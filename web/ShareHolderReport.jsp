<%@taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer"%>
<%@page import="java.util.Date,java.text.*,com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.* " contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="util" scope="session" class="batbsms.Utility"></jsp:useBean>
<html>
<head>
<title>Certificate Printing</title>
</head>
<body bgcolor="#ffffff">
<h1></h1>
<%
  String scope = util.changeIfNullnTrim( String.valueOf(request.getParameter("printrecords")));
  String type = util.changeIfNullnTrim(String.valueOf(request.getParameter("reporttype")));
  String sharesgreater = util.changeIfNullnTrim(String.valueOf(request.getParameter("sharesgreaterthan")));
  String sharesless = util.changeIfNullnTrim(String.valueOf(request.getParameter("shareslessthan")));
  String thecategory = util.changeIfNullnTrim(String.valueOf(request.getParameter("SelectCategory")));

  String reporturl = "";

  if (scope.equals("printname") && type.equals("SMStype"))
  {
    reporturl = "/CR_Reports/Folio_Share_Holder_Report/Folio_ShareHolders_ReportOrderByName.rpt";
  }
  else if (scope.equals("printname") && type.equals("CDBLtype"))
  {
    reporturl = "/CR_Reports/BO_Share_Holder_Report/BO_ShareHolders_Report_OrderByName.rpt";
  }
  else if (scope.equals("printshares") && type.equals("SMStype"))
  {
    reporturl = "/CR_Reports/Folio_Share_Holder_Report/Folio_ShareHolders_ReportOrderByShare.rpt";
  }
  else if (scope.equals("printshares") && type.equals("CDBLtype"))
  {
    reporturl = "/CR_Reports/BO_Share_Holder_Report/BO_ShareHolders_Report_OrderByShares.rpt";
  }
  else if (scope.equals("printcategory") && type.equals("SMStype"))
  {
    if (thecategory.equalsIgnoreCase("All"))
    {
     reporturl = "/CR_Reports/Folio_Share_Holder_Report/Folio_ShareHolders_ReportOrderByCategory_All.rpt";
    }
    else
    {
     reporturl = "/CR_Reports/Folio_Share_Holder_Report/Folio_ShareHolders_ReportOrderByCategory.rpt";
    }
  }
  else if (scope.equals("printcategory") && type.equals("CDBLtype"))
  {
    if (thecategory.equalsIgnoreCase("All"))
    {
      reporturl = "/CR_Reports/BO_Share_Holder_Report/BO_ShareHolders_Report_OrderByCatagory_ALL.rpt";
    }
    else
    {
      reporturl = "/CR_Reports/BO_Share_Holder_Report/BO_ShareHolders_Report_OrderByCatagory.rpt";
    }
  }

  if (!reporturl.equals("")) {
     session.setAttribute("reportSource", null);
     session.setAttribute("refreshed", null);
  }

   Object reportSource = session.getAttribute("reportSource");
   if (reportSource == null)
   {
   try{
     IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
     reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
     session.setAttribute("reportSource", reportSource);
   }catch (Exception e) {}
   }
     CrystalReportViewer viewer= new CrystalReportViewer();
     try{
    viewer.setReportSource(reportSource);
     }catch (Exception e) {}
  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setOwnForm(true);


  // Passing parameters
  ParameterField param1 = new ParameterField();
  param1.setReportName("");
  param1.setName("ParamGreaterThan");

  ParameterField param2 = new ParameterField();
  param2.setReportName("");
  param2.setName("ParamLessThan");

  ParameterField param3 = new ParameterField();
  param3.setReportName("");
  param3.setName("ParamClass");

  Values vals1 = new Values();
  ParameterFieldDiscreteValue val1 = new ParameterFieldDiscreteValue();
  val1.setValue(sharesgreater);
  vals1.add(val1);


  Values vals2 = new Values();
  ParameterFieldDiscreteValue val2 = new ParameterFieldDiscreteValue();
  val2.setValue(sharesless);
  vals2.add(val2);

  Values vals3 = new Values();
  ParameterFieldDiscreteValue val3 = new ParameterFieldDiscreteValue();
  val3.setValue(thecategory);
  vals3.add(val3);

  param1.setCurrentValues(vals1);
  param2.setCurrentValues(vals2);
  param3.setCurrentValues(vals3);



  Fields fields = new Fields();
  fields.add(param1);
  fields.add(param2);
  fields.add(param3);


  viewer.setParameterFields(fields);

  if (session.getAttribute("refreshed") == null)
  {
    viewer.refresh();
    session.setAttribute("refreshed", "true");
  }

   try
   {
      viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);
   }
   catch(Exception e){System.out.println(e.getMessage());}

%>
</body>
</html>
