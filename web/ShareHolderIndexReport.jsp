<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : March 2006
'Page Purpose  : Passes Parameters for Share Holder Index Report.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

 boolean isConnect = cm.connect();
 if(isConnect==false)
 {
   %>
    <jsp:forward page="ErrorMsg.jsp" >
    <jsp:param name="ErrorTitle" value="Connection Failure" />
    <jsp:param name="ErrorHeading" value="Connection Problem" />
    <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
    </jsp:forward>
  <%
 }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Printed Share Holder Index Report')";
  boolean ub = cm.procedureExecute(ulog);

  if (isConnect)
  {
   cm.takeDown();
  }
%>

<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page
import="java.util.Date,java.text.*,com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
Right Share Allotment List
</title>
</head>
<body bgcolor="#ffffff">
<h1>
</h1>
<%
  String PrintScope = String.valueOf(request.getParameter("printrecords"));
  String DivType = String.valueOf(request.getParameter("reporttype"));
  String FStart = String.valueOf(request.getParameter("foliostart"));
  String FEnd = String.valueOf(request.getParameter("folioend"));
  String DivOrder = String.valueOf(request.getParameter("orderby"));

  if (String.valueOf(FStart).equals("null"))
   FStart = "";
  if (String.valueOf(FEnd).equals("null"))
   FEnd = "";

  String reporturl = "";

  if (DivType.equalsIgnoreCase("SMStype"))
  {
   if (DivOrder.equalsIgnoreCase("byname"))
   {
     if (PrintScope.equalsIgnoreCase("printall"))
     {
       reporturl = "/CR_Reports/Folio_Share_Holder's_Index/Folio_ShareHoldersIndexByName&All.rpt";
     }
     else if (PrintScope.equalsIgnoreCase("rangebyfolio"))
     {
       reporturl = "/CR_Reports/Folio_Share_Holder's_Index/Folio_ShareHoldersIndexByName&Range.rpt";
     }
   }
   else if (DivOrder.equalsIgnoreCase("byfolio"))
   {
     if (PrintScope.equalsIgnoreCase("printall"))
     {
       reporturl = "/CR_Reports/Folio_Share_Holder's_Index/Folio_ShareHoldersIndexByFolio&All.rpt";
     }
     else if (PrintScope.equalsIgnoreCase("rangebyfolio"))
     {
       reporturl = "/CR_Reports/Folio_Share_Holder's_Index/Folio_ShareHoldersIndexByFolio&Range.rpt";
     }
   }
  }
  else if (DivType.equalsIgnoreCase("CDBLtype"))
  {
   if (DivOrder.equalsIgnoreCase("byname"))
   {
     if (PrintScope.equalsIgnoreCase("printall"))
     {
       reporturl = "/CR_Reports/BO_Share_Holder's_Index/BO_ShareHoldersIndexOrderByName&All.rpt";
     }
     else if (PrintScope.equalsIgnoreCase("rangebyfolio"))
     {
       reporturl = "/CR_Reports/BO_Share_Holder's_Index/BO_ShareHoldersIndexOrderByName&Range.rpt";
     }
   }
   else if (DivOrder.equalsIgnoreCase("byfolio"))
   {
     if (PrintScope.equalsIgnoreCase("printall"))
     {
       reporturl = "/CR_Reports/BO_Share_Holder's_Index/BO_ShareHoldersIndexOrderByBOID&All.rpt";
     }
     else if (PrintScope.equalsIgnoreCase("rangebyfolio"))
     {
       reporturl = "/CR_Reports/BO_Share_Holder's_Index/BO_ShareHoldersIndexOrderByBOID&Range.rpt";
     }
   }
  }

    if (!reporturl.equals("")) {
    session.setAttribute("reportSource", null);
    session.setAttribute("refreshed", null);
  }

Object reportSource = session.getAttribute("reportSource");
if (reportSource == null)
{
try{
  IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
  reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
  session.setAttribute("reportSource", reportSource);
}catch (Exception e) {}
}
  CrystalReportViewer viewer= new CrystalReportViewer();
  try{
    viewer.setReportSource(reportSource);
  }catch (Exception e) {}

  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setOwnForm(true);


// Passing parameters

ParameterField param1 = new ParameterField();
param1.setReportName("");
param1.setName("ParamGreaterThan");

Values vals1 = new Values();
ParameterFieldDiscreteValue val1 = new ParameterFieldDiscreteValue();
val1.setValue(FStart);
vals1.add(val1);

ParameterField param2 = new ParameterField();
param2.setReportName("");
param2.setName("ParamLessThan");

Values vals2 = new Values();
ParameterFieldDiscreteValue val2 = new ParameterFieldDiscreteValue();
val2.setValue(FEnd);
vals2.add(val2);

try {
// DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
// Date d = df.parse(DateDec);
// val1.setValue(d);
// vals1.add(val1);
} catch(Exception e) {
e.printStackTrace();
}

param1.setCurrentValues(vals1);
param2.setCurrentValues(vals2);

Fields fields = new Fields();
fields.add(param1);
fields.add(param2);

viewer.setParameterFields(fields);

if (session.getAttribute("refreshed") == null)
{
viewer.refresh();
session.setAttribute("refreshed", "true");
}


try
{
viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);
}
catch(Exception e){System.out.println(e.getMessage());}

%>
</body>
</html>

