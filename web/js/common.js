var newWindow = null;
var nArray = new Array();
var count = 0;
// for batlcexport
var Clientadd = new Array();
var Clienttel = new Array();
var Clientfax = new Array();
var Clientemail = new Array();
var Notifyadd = new Array();
var Consigneeadd = new Array();
var CurrencyP1 = new Array();
var CurrencyP2 = new Array();
var ExpBankAddArr = new Array();
var TsoadValueArr = new Array();
var RelationshipManArr = new Array();
var AccountNameArr = new Array();
var ExpBankBdtAccNumArr = new Array();
var ExpBankUsdAccNumArr = new Array();
var SwiftCodeArr = new Array();

//var ImpBankArr=new Array();
var ImpBankAddArr = new Array();
var ImpBankAccNumArr = new Array();
var BankIdCodeArr = new Array();
//Masud: (05 Jan)
//***********************************
//Function to add options in a list box field
function AddFromTo(FromFld, ToFld) {

    if (FromFld.selectedIndex != -1) {
        for (var i = 0; i < FromFld.options.length; ++i) {
            if (FromFld.options[i].selected) {
                var Flag = false;
                var selectionText = FromFld.options[i].text;
                var selectionValue = FromFld.options[i].value;
                FromFld.options[i].selected = false;
                if (ToFld.options.length == 0) {
                    ToFld.options[ToFld.options.length] = new Option(FromFld.options[i].text, FromFld.options[i].value);
                } else {
                    for (var t = 0; t < ToFld.options.length; ++t) {
                        if (ToFld.options[t].value == selectionText || ToFld.options[t].value == selectionValue) {
                            Flag = true;
                        }
                    }
                    if (!Flag) {
                        ToFld.options[ToFld.options.length] = new Option(FromFld.options[i].text, FromFld.options[i].value);
                    }
                }
            }
        }
    }

}
///Function to remove options from a list box
function RemoveClick(field) {
    if (field.length != 0) {
        if (field.selectedIndex != -1) {
            for (var i = 0; i < field.options.length; ++i) {
                if (field.options[i].selected) {
                    field.options[i] = null;
                    i = i - 1;
                }
            }
        }
    }
}
///Function to validate List Box fields
function ListBoxValidate(MyFld, msg) {
    var f = document.forms[0];
    if (f.elements[MyFld].options.length == 0) {
        count = count + 1;
        nArray[count] = msg;
    }
}
function AddSelUser(ThatVal, ThatText) {
    var TargetFld = document.forms[0].elements[FieldName]
    TargetFld.options.length = 0;

    for (var i = 0; i < ThatVal.length; i++) {
        if (ThatVal[i] != "") {
            TargetFld.options[TargetFld.options.length] = new Option(ThatText[i], ThatVal[i]);
        }
    }
}
//*********************************

//////function Open New window

function ONW(windowName, TU, width, height, status, scrollbars, resizable) {

    var left = (screen.width - width) / 2;
    var top = (screen.height - height) / 2;

    if (!newWindow || newWindow.closed) {
        newWindow = window.open(TU, windowName, 'top=' + top + ',left=' + left + ',width=' + width + ',height=' + height + ',toolbar=0,location=0,directories=0,status=' + status + ',menubar=0,scrollbars=' + scrollbars + ',resizable=' + resizable, true);

    } else {
        newWindow.focus();
    }
}

//////////////TRIM THE TEXT

var frameName = 'TopFrame'; // this is used by following 2 functions
//  this is the name of the frame where two functions
//  1. storeValue() 2. getValue() in included in the
//  JS Header of the designated page or form

function storeCurrentURL() { //frameName is where storeValue() 								   //fn available
    var frameObj = eval('top.' + frameName);
    if (frameObj) {
        frameObj.storeValue(location.href);
    }
}
//#################################################################################
function closeCurrenDoc() {
    var frameObj = eval('top.' + frameName);
    if (frameObj) {
        location = frameObj.getValue();
    }
}
//#################################################################################
function closeCurrenDoc() {
    var frameObj = eval('top.' + frameName);
    if (frameObj) {
        location = frameObj.getValue();
    }
}
//#################################################################################
function closeCurrenDocForCall(URLStr) {
    var frameObj = eval('top.' + frameName);
    if (frameObj.getValue() == "") {
        location = URLStr;
    } else {
        location = frameObj.getValue();
    }
}
//#################################################################################
function cancelCurrenDocForCall(URLStr) {
    location = URLStr;
}

//#################################################################################

function replaceSubstring(source, what, withWhat) {
    var idx = 0;
    while ((idx = source.indexOf(what)) != -1) {
        var left = source.substring(0, idx);
        var right = source.substring(idx + what.length);
        source = left + withWhat + right;
    }
    return source;
}

//#################################################################################
function trim(str) {
    fixedTrim = "";
    lastCh = " ";

    for (x = 0; x < str.length; x++)
    {
        strch = str.charAt(x);
        if ((strch != " ") || (lastCh != " "))
        {
            fixedTrim += strch;
        }
        lastCh = strch;
    }

    if (fixedTrim.charAt(fixedTrim.length - 1) == " ")
    {
        fixedTrim = fixedTrim.substring(0, fixedTrim.length - 1);
    }

    return fixedTrim;
}



//#################################################################################
function ShowAllAlertMsg() {
    var msg = "";
    for (i = 0; i <= count; i++) {
        if (nArray[i] == null) {
        } else {
            msg = msg + "<br>" + nArray[i];
        }
    }

    msg = msg.replace("'", "''");
    msg = msg.replace("&", "$$");
    msg = msg.replace("?", "##");

    var ht = 170 + 15 * (count + 1) + 65;

    var left = (screen.width - 500) / 2;
    var top = (screen.height - ht) / 2;
    count = 0;
    //window.showModalDialog("/batbsms/js/allmessage.jsp?mymsg="+msg,'','dialogHeight:'+ht+'px;dialogWidth:540px;dialogLeft:'+left+';dialogTop:'+top+';center:yes; status:no; scroll:no;');
    if (!newWindow || newWindow.closed) {
        newWindow = window.open("", "Error", "left=" + left + ",top=" + top + ",height=" + ht + ",width=500");

        msgHead1 = "  Warning !"
        msgHead2 = "The document you have submitted is not complete. The mandatory field(s) which have not been specified are listed below."
        msgHead3 = "Please press OK button to return to the form and specify the value(s)."
        OKButton = "<br><br><center><input name='b1' tabindex='0' type ='button' onClick='window.close();' VALUE='OK' STYLE='letter-spacing:1px;font-family:tahoma; font-size:9pt; background-color:#FFFFFF'>"

        newWindow.document.write('<HTML><HEAD><TITLE>' + msgHead1 + '</TITLE></HEAD>\n');
        newWindow.document.write('<BODY BGCOLOR="#E8F3FD">\n');
        newWindow.document.write("<B><Font Face=Verdana><font color = FF0000>" + msgHead1 + "</font></b>\n");
        newWindow.document.write("<hr>\n");
        newWindow.document.write("<Font Face=Verdana><FONT SIZE=2 color = 0A2769>" + msgHead2 + "</font>\n");
        newWindow.document.write("</br>\n");
        newWindow.document.write("<B><Font Face=Verdana color = FF0000 SIZE=2>" + msg + "</font></B>\n");
        newWindow.document.write("<p>");
        newWindow.document.write("</b><Font Face=Verdana><FONT SIZE=2><font color = 0A2769>" + msgHead3 + "</font></b>");

        newWindow.document.write(OKButton);

        newWindow.document.write('</BODY></HTML>');
        newWindow.document.close();
        newWindow.focus();
    } else {
        newWindow.focus();
    }
}
//#################################################################################

//#################################################################################
function ShowAgenda(mycontext) {
    var msg = "";
    for (i = 0; i <= count; i++) {
        if (nArray[i] == null) {
        } else {
            msg = msg + "<br>" + nArray[i];
        }
    }

    msg = msg.replace("'", "''");
    msg = msg.replace("&", "$$");
    msg = msg.replace("?", "##");

    var ht = 340;

    var left = (screen.width - 500) / 2;
    var top = (screen.height - ht) / 2;
    count = 0;
    //alert(mycontext+"/js/message.jsp?mymsg="+msg);
    window.showModalDialog(mycontext + "/js/message.jsp?mymsg=" + msg, '', 'dialogHeight:' + ht + 'px;dialogWidth:540px;dialogLeft:' + left + ';dialogTop:' + top + ';center:yes; status:no; scroll:yes;');


    /* if (!newWindow || newWindow.closed) {
     newWindow = window.open("","Error","left="+left+",top="+top+",height="+ht+",width=500");
     
     msgHead1 = "  Agenda Description !"
     OKButton = "<br><br><center><input name='b1' tabindex='0' type ='button' onClick='window.close();' VALUE='OK' STYLE='letter-spacing:1px;font-family:tahoma; font-size:9pt; background-color:#FFFFFF'>"
     
     newWindow.document.write('<HTML><HEAD><TITLE>'+msgHead1+'</TITLE></HEAD>\n');
     newWindow.document.write('<BODY BGCOLOR="#E8F3FD">\n');
     newWindow.document.write("<B><Font Face=Verdana><font color = FF0000>"+msgHead1+"</font></b>\n");
     newWindow.document.write("<hr>\n");
     newWindow.document.write("<B><Font Face=Verdana color=#0A2769 SIZE=2>"+msg+"</font></B>\n");
     newWindow.document.write("\n");
     
     newWindow.document.write(OKButton);
     
     newWindow.document.write('</BODY></HTML>');
     newWindow.document.close();
     newWindow.focus();
     }else{
     newWindow.focus();
     }*/
}
//#################################################################################

//#################################################################################
function SelectValidate(MyFld, MyMsg) {
    var Fld;
    var selectedIndexVal = window.document.forms[0].elements[MyFld].selectedIndex;
    if (selectedIndexVal == 0 || selectedIndexVal == -1) { //If the field is not Blank and first Entry is not selected
        count = count + 1;
        nArray[count] = MyMsg;
    }
}

function SelectValidateNotFirstEntry(MyFld, MyMsg) {
    var Fld;
    var selectedIndexVal = window.document.forms[0].elements[MyFld].selectedIndex;
    if (selectedIndexVal == -1) { //If the field is not Blank and first Entry is not selected
        count = count + 1;
        nArray[count] = MyMsg;
    }
}

//#################################################################################
function BlankValidate(FldName, Msg) {
    var val;
    val = trim(window.document.forms[0].elements[FldName].value);
    window.document.forms[0].elements[FldName].value = val;
    if (val === "") {
        count = count + 1;
        nArray[count] = Msg;
    }
}

//#################################################################################
function ValidateTextMinLength(FldName, MinLength, Msg) {
    var val;
    val = trim(window.document.forms[0].elements[FldName].value);
    window.document.forms[0].elements[FldName].value = val;
    if (val.length < MinLength) {
        count = count + 1;
        nArray[count] = Msg;
    }
}

function CheckValidate(Msg1)
{
    count = count + 1;
    nArray[count] = Msg1;
}

//#################################################################################
function keypressOnDoubleFld() {
    if ((event.keyCode == 47) || (event.keyCode < 46) || (event.keyCode > 57)) { //Numbers
        event.returnValue = false
    }
}

//#################################################################################
/*function keypresssubmit() {
 if (event.keyCode==13)  {
 SubmitThis();
 }
 }*/
//#################################################################################
function keypressOnNumberFld() {
    if ((event.keyCode < 48) || (event.keyCode > 57)) { //Numbers
        event.returnValue = false
    }
}


//#################################################################################
function PasswordCheck(Fld1, Fld2, Msg) {
    var val1, val2;
    val1 = trim(window.document.forms[0].elements[Fld1].value);
    val2 = trim(window.document.forms[0].elements[Fld2].value);

    if (val1 != val2) {
        count = count + 1;
        nArray[count] = Msg;
    }
}


///////////////////////////////////////////////////////////////////////////

function showPrintPreview(linkToOpen) {
    var width = 700;
    var height = 680;
    var left = (screen.width - width) / 2;
    var top = (screen.height - height - 150) / 2;

    wind = window.open(linkToOpen, 'newWin', 'top=' + top + ',left=' + left + ',width=' + width + ',height=' + height + ',toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0', true);
    wind.focus()
}

function showPrintPreview1(linkToOpen) {
    var width = 800;
    var height = 680;
    var left = (screen.width - width) / 2;
    var top = (screen.height - height - 150) / 2;

    wind = window.open(linkToOpen, 'newWin', 'top=' + top + ',left=' + left + ',width=' + width + ',height=' + height + ',toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0', true);
    wind.focus()
}

function showPrintPreview2(linkToOpen) {
    var width = 970;
    var height = 680;
    var left = (screen.width - width) / 2;
    var top = (screen.height - height - 150) / 2;

    wind = window.open(linkToOpen, 'newWin', 'top=' + top + ',left=' + left + ',width=' + width + ',height=' + height + ',toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0', true);
    wind.focus()
}

function showPrintPreview3(linkToOpen) {
    var width = 900;
    var height = 680;
    var left = (screen.width - width) / 2;
    var top = (screen.height - height) / 2;

    wind = window.open(linkToOpen, 'newWin', 'top=' + top + ',left=' + left + ',width=' + width + ',height=' + height + ',toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0', true);
    wind.focus()
}

function showPrintPreview4(linkToOpen) {
    var width = 1000;
    var height = 680;
    var left = (screen.width - width) / 2;
    var top = (screen.height - height - 150) / 2;

    wind = window.open(linkToOpen, 'newWin', 'top=' + top + ',left=' + left + ',width=' + width + ',height=' + height + ',toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0', true);
    wind.focus()
}

function showPrintPreview5(linkToOpen) {
    var width = 1000;
    var height = 680;
    var left = (screen.width - width) / 2;
    var top = (screen.height - height - 150) / 2;

    wind = window.open(linkToOpen, 'newWin', 'top=' + top + ',left=' + left + ',width=' + width + ',height=' + height + ',toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0', true);
    wind.focus()
}

function showPrintPreview6(linkToOpen) {
    var width = 1010;
    var height = 540;
    var left = ((screen.width - width) / 2) - 5;
    var top = (screen.height - height - 15) / 2;
    wind = window.open(linkToOpen, 'newWin', 'top=' + top + ',left=' + left + ',width=' + width + ',height=' + height + ',toolbar=0,location=0,directories=0,status=0,menubar=1,scrollbars=1,resizable=1', true);
    wind.focus();
}

//////////////////////////////
// for batlcexport

//Replace character
function replaceChars(mainStr, out, add) {
    temp = "" + mainStr; // temporary holder
    while (temp.indexOf(out) > -1) {
        pos = temp.indexOf(out);
        temp = "" + (temp.substring(0, pos) + add +
                temp.substring((pos + out.length), temp.length));
    }
    return temp;
}
//Calculate Outstanding
function caltaka() {
    var callform = document.all["Pymt_Rcvd_Amnt_Usd"].form.name;
    var TotalOutStanding;
    var myPymtRcvdAmntUsd = replaceChars(document.all["Pymt_Rcvd_Amnt_Usd"].value, ",", "");
    var myPymtRcvdExchgRate = replaceChars(document.all["Pymt_Rcvd_Exchg_Rate"].value, ",", "");
    var myPymtRcvdAmntBdt = replaceChars(document.all["Pymt_Rcvd_Amnt_Bdt"].value, ",", "");
    var myTotalFob = replaceChars(document.all['Total_Fob'].value, ",", "");
    var myamountrec = replaceChars(document.all["Amount_Rcvd"].value, ",", "");
    var mybankcharges = replaceChars(document.all["Bank_Charges"].value, ",", "");
    if (Number(mybankcharges) < 0) {
        mybankcharges = 0;
    }
    //var myAmountReceiptDate=document.all["Amount_Receipt_Date"].value;
    var myPymtRcvdDate = document.all["Pymt_Rcvd_Date"].value;
    TotalOutStanding = 0;
    if (Number(myamountrec) > 0) {
        if (Number(myPymtRcvdExchgRate) > 0) {// && myAmountReceiptDate.length>0
            if (callform == 'frmLcexporttt') {//for TT
                if (myPymtRcvdDate.length > 0) {
                    document.all["Pymt_Rcvd_Amnt_Usd"].value = (Number(myamountrec) / 2).toFixed(2);
                    document.all["Pymt_Rcvd_Amnt_Bdt"].value = (Number(Number(myamountrec) / 2) * Number(myPymtRcvdExchgRate)).toFixed(2);
                    TotalOutStanding = Number(myTotalFob) - Number(myamountrec);
                    TotalOutStanding = Number(TotalOutStanding) - Number(mybankcharges);
                } else {
                    document.all["Pymt_Rcvd_Amnt_Usd"].value = "";
                    document.all["Pymt_Rcvd_Amnt_Bdt"].value = "";
                    TotalOutStanding = Number(myTotalFob) - Number(mybankcharges);
                }
            } else {//for LC & DP
                if (Number(myPymtRcvdAmntUsd) > 0 && myPymtRcvdDate.length > 0) {
                    TotalOutStanding = Number(myTotalFob) - Number(Number(myamountrec) + Number(myPymtRcvdAmntUsd) + Number(mybankcharges));
                } else {
                    //document.all["Pymt_Rcvd_Amnt_Usd"].value="";
                    TotalOutStanding = Number(myTotalFob) - Number(Number(myamountrec) + Number(mybankcharges));
                }
                document.all["Pymt_Rcvd_Amnt_Bdt"].value = (Number(myamountrec) * Number(myPymtRcvdExchgRate)).toFixed(2);
            }
            if (TotalOutStanding < 0) {
                count = count + 1;
                nArray[count] = 'Invalid input.  Outstanding Balance can not be negative !';
                if ((myPymtRcvdAmntUsd.length <= 0) || (Number(myPymtRcvdAmntUsd) <= Number(myamountrec))) {
                    if (document.all["Treasuryspan"].style.display == 'none') {
                        document.all["Treasuryspan"].style.display = ''
                        document.all["Amount_Rcvd"].focus();
                    }
                } else {
                    if (document.all["Treasuryspan"].style.display == 'none') {
                        document.all["Treasuryspan"].style.display = ''
                        document.all["Pymt_Rcvd_Amnt_Usd"].focus();
                    }
                }
                ShowAllAlertMsg();
                return false;
            } else
                this.document.all['Outstanding_Balance'].value = TotalOutStanding.toFixed(2);
        } else {//noexchenge rate or received Date
            if (callform == 'frmLcexporttt') {//for TT
                document.all["Pymt_Rcvd_Amnt_Usd"].value = "";
                document.all["Pymt_Rcvd_Amnt_Bdt"].value = "";
                TotalOutStanding = Number(myTotalFob) - Number(mybankcharges);
            } else {//for LC & DP
                if (Number(myPymtRcvdAmntUsd) > 0 && myPymtRcvdDate.length > 0) {
                    TotalOutStanding = Number(myTotalFob) - Number(myPymtRcvdAmntUsd) - Number(mybankcharges);
                } else {
                    //document.all["Pymt_Rcvd_Amnt_Usd"].value="";
                    TotalOutStanding = Number(myTotalFob) - Number(mybankcharges);
                }
                document.all["Pymt_Rcvd_Amnt_Bdt"].value = "";
            }
            if (TotalOutStanding < 0) {
                count = count + 1;
                nArray[count] = 'Invalid input. Outstanding Balance can not be negative !';
                if ((myPymtRcvdAmntUsd.length <= 0) || (Number(myPymtRcvdAmntUsd) <= Number(myamountrec))) {
                    if (document.all["Treasuryspan"].style.display == 'none') {
                        document.all["Treasuryspan"].style.display = ''
                        document.all["Amount_Rcvd"].focus();
                    }
                } else {
                    if (document.all["Treasuryspan"].style.display == 'none') {
                        document.all["Treasuryspan"].style.display = ''
                        document.all["Pymt_Rcvd_Amnt_Usd"].focus();
                    }
                }
                ShowAllAlertMsg();
                return false;
            } else if (TotalOutStanding > 0)
                this.document.all['Outstanding_Balance'].value = TotalOutStanding.toFixed(2);
        }
    } else {//no BDT Amount Received
        if (callform == 'frmLcexporttt') {//for TT
            document.all["Pymt_Rcvd_Amnt_Usd"].value = "";
            document.all["Pymt_Rcvd_Amnt_Bdt"].value = "";
            TotalOutStanding = Number(myTotalFob) - Number(mybankcharges);
        } else {//for LC & DP
            if (Number(myPymtRcvdAmntUsd) > 0 && myPymtRcvdDate.length > 0) {
                TotalOutStanding = Number(myTotalFob) - Number(myPymtRcvdAmntUsd) - Number(mybankcharges);
            } else {
                //document.all["Pymt_Rcvd_Amnt_Usd"].value="";
                TotalOutStanding = Number(myTotalFob) - Number(mybankcharges);
            }
            document.all["Pymt_Rcvd_Amnt_Bdt"].value = "";
        }

        if (TotalOutStanding < 0) {
            count = count + 1;
            nArray[count] = 'Invalid input.  Outstanding Balance can not be negative !';
            document.all["Pymt_Rcvd_Exchg_Rate"].value = "";
            if ((myPymtRcvdAmntUsd.length <= 0) || (Number(myPymtRcvdAmntUsd) <= Number(myamountrec))) {
                if (document.all["Treasuryspan"].style.display == 'none') {
                    document.all["Treasuryspan"].style.display = ''
                    document.all["Amount_Rcvd"].focus();
                }
            } else {
                if (document.all["Treasuryspan"].style.display == 'none') {
                    document.all["Treasuryspan"].style.display = ''
                    document.all["Pymt_Rcvd_Amnt_Usd"].focus();
                }
            }
            ShowAllAlertMsg();
            return false;
        } else if (TotalOutStanding > 0)
            this.document.all['Outstanding_Balance'].value = TotalOutStanding.toFixed(2);
        else
            this.document.all['Outstanding_Balance'].value = "";
    }

    mysetcomma('Pymt_Rcvd_Amnt_Bdt');
    mysetcomma('Pymt_Rcvd_Amnt_Usd');
    mysetcomma('Outstanding_Balance');
    mysetcomma('Bank_Charges');
}
//Calculate Cashincentive
function clacashinc() {
    var Totalcashinc;
    var myPercashincrate = replaceChars(document.all["Per_Of_Cash_Inc"].value, ",", "");
    var myTotalFobcashinc = replaceChars(document.all['Total_Fob'].value, ",", "");
    var myotherchargecashinc = replaceChars(document.all['Total_Charges'].value, ",", "");
    if (Number(myotherchargecashinc) <= 0)
        myotherchargecashinc = 0;
    if (Number(myTotalFobcashinc) > 0 && Number(myPercashincrate) > 0) {
        myTotalFobcashinc = Number(myTotalFobcashinc) - Number(myotherchargecashinc);
        document.all['Cash_Incentive_Amount'].value = ((Number(myPercashincrate) * Number(myTotalFobcashinc)) / 100).toFixed(2);
    } else {
        document.all['Cash_Incentive_Amount'].value = "";
    }
    mysetcomma('Cash_Incentive_Amount');
}
//Calculate Pending Cashincentive
function clacashincpending() {
    var myAuditedcashinc = replaceChars(document.all["Audited_Cash_Inc_Amt"].value, ",", "");
    var myReccashincamt = replaceChars(document.all['Cash_Incentive_Rec_Amount'].value, ",", "");
    var myCashIncAmt = replaceChars(document.all['Cash_Incentive_Amount'].value, ",", "");
    if (Number(myCashIncAmt) > 0) {
        if (Number(Number(myReccashincamt) - Number(myAuditedcashinc)) > 0) {
            count = count + 1;
            nArray[count] = 'Cash Incentive Pending Amount can not be negative !';
            if (document.all["Cashinsspan"].style.display == 'none') {
                document.all["Cashinsspan"].style.display = ''
                document.all["Cash_Incentive_Rec_Amount"].focus();
            }
            ShowAllAlertMsg();
            return false;
        } else {
            if (Number(myAuditedcashinc) > 0 && Number(myReccashincamt) > 0) {
                if (Number(Number(myReccashincamt) - Number(myAuditedcashinc)) == 0)
                    document.all['Cash_Incentive_Pending'].value = "";
                else
                    document.all['Cash_Incentive_Pending'].value = (Number(myAuditedcashinc) - Number(myReccashincamt)).toFixed(2);
            } else if (Number(myAuditedcashinc) > 0) {
                document.all['Cash_Incentive_Pending'].value = Number(myAuditedcashinc).toFixed(2);
            } else {
                document.all['Cash_Incentive_Pending'].value = "";
            }
            mysetcomma('Cash_Incentive_Pending');
        }
    } else {
        document.all["Audited_Cash_Inc_Amt"].value = "";
    }
}

//Chenge Client Infomation
function ChangeClientInfo(myid) {
    var Sclientindex = document.all[myid].selectedIndex;
    if (Sclientindex == 0) {
        document.all["Sold_By_Add"].value = "";
        document.all["Sold_By_Tel"].value = "";
        document.all["Sold_By_Fax"].value = "";
        document.all["Sold_By_Email"].value = "";
    } else {
        document.all["Sold_By_Add"].value = replaceChars(Clientadd[Sclientindex], "<br>", "\r\n");
        document.all["Sold_By_Tel"].value = Clienttel[Sclientindex];
        document.all["Sold_By_Fax"].value = Clientfax[Sclientindex];
        document.all["Sold_By_Email"].value = Clientemail[Sclientindex];
    }
}

//Chenge Consignee Infomation
function ChangeconsigneeInfo(myid) {
    var Sconsigneeindex = document.all[myid].selectedIndex;
    if (Sconsigneeindex == 0) {
        document.forms[0].Consignee_Add.value = "";
    } else {
        document.forms[0].Consignee_Add.value = replaceChars(Consigneeadd[Sconsigneeindex], "<br>", "\r\n");
    }
}
//Chenge Notify Party Infomation
function ChangenotifyInfo(myid) {
    var Snotifyindex = document.all[myid].selectedIndex;
    if (Snotifyindex == 0) {
        document.all["Notify_Party_Add"].value = "";
    } else {
        document.all["Notify_Party_Add"].value = replaceChars(Notifyadd[Snotifyindex], "<br>", "\r\n");
    }
}
//function for convertion Number to word

function numberinword(mynumberobj, mywordobj) {
    var mytotalfob = new String(document.all[mynumberobj].value);
    mytotalfob = replaceChars(mytotalfob, ",", "");
    //var mytotalfob= document.all[mynumberobj].value;
    var intpart = 0;
    var mytotalfobward = "";
    var Unitindex = 0;
    if (document.all["Unit_Price_C1"].selectedIndex != 0) {
        Unitindex = document.all["Unit_Price_C1"].selectedIndex;
    }
    if (Unitindex != 0 && mytotalfob.valueOf() > '0.00') {
        var Mylen = mytotalfob.indexOf('.', 0);
        if (Mylen != -1) {
            intpart = NumtoWord(mytotalfob.substr(0, Mylen), 0);
            //for dot part
            var startpos = mytotalfob.indexOf('.', 0) + 1;
            Mylen = mytotalfob.length - mytotalfob.indexOf('.', 0) - 1;
            var mydotpart = mytotalfob.substr(startpos, Mylen);
            if (mydotpart.length == 1) {
                mydotpart = mydotpart + '0';//00
            }/*
             else if(mydotpart.length==2){
             mydotpart=mydotpart+'00';
             }
             else if(mydotpart.length==3){
             mydotpart=mydotpart+'0';
             }*/
            if (mydotpart == '0' || mydotpart == '00') {//|| mydotpart=='0000'
                //total Number
                mytotalfobward = CurrencyP1[Unitindex] + ' ' + intpart + ' Only';
            } else {
                var dotpart = NumtoWord(mydotpart, 0);
                //total Number
                if (intpart == ' zero') {
                    mytotalfobward = dotpart + ' ' + CurrencyP2[Unitindex] + ' Only';
                } else {
                    mytotalfobward = CurrencyP1[Unitindex] + ' ' + intpart + ' and ' + dotpart + ' ' + CurrencyP2[Unitindex] + ' Only';
                }
            }
        } else {
            intpart = NumtoWord(mytotalfob, 0);
            //total Number
            mytotalfobward = CurrencyP1[Unitindex] + ' ' + intpart + ' Only';
        }
        //Capitalized first character
        //var firstchar=mytotalfobward.substr(1,1);
        //var Mytotalfobward=firstchar.toUpperCase() + mytotalfobward.substr(2,(mytotalfobward.length-2));
        var Mytotalfobward = mytotalfobward
        if (mytotalfobward.length <= 255) {
            document.all[mywordobj].value = Mytotalfobward;
        } else {
            if (document.all["Shippingmarkspan"].style.display == 'none') {
                document.all["Shippingmarkspan"].style.display = ''
                document.all[mynumberobj].focus();
            }
            count = count + 1;
            nArray[count] = 'Total fob amount in words contain more than 255 character';
            ShowAllAlertMsg();
            return false;
        }
    } else {
        document.all[mywordobj].value = "";
    }
}
/*
 Num2Word, ouputs written number in human language format
 ARGUMENTS:
 -----------
 Nbr: num, the number to be worded. This number is converted into an integer.
 Crd: bol, flags whether output is cardinal or ordinal number. Cardinal is used when describing the order of items, like "first" or "second" place.
 */
function NumtoWord(num, fmt) {
//_ arguments
    num = Math.round(num).toString(); // round value
    if (num == 0)
        return ' zero'; // if number given is 0, return and exit
//_ locals
    // word numbers
    var wnums = [['Hundred', 'Thousand', 'Million', 'Billion', 'Trillion', 'Zillion'], ['One', 'First', 'Ten', '', 'Th'], ['Two', 'Second', 'Twen', 0, 0], ['Three', 'Third', 'Thir', 0, 0], ['Four', 'Fourth', 0, 0, 0], ['Five', 'Fifth', 'Fif', 0, 0], ['Six', 'Sixth', 0, 0, 0], ['Seven', 'Seventh', 0, 0, 0], ['Eight', 'Eighth', 'Eigh', 0, 0], ['Nine', 'Ninth', 0, 0, 0], ['Ten', ], ['Eleven', ], ['Twelve', 'Twelfth'], ['Thirteen', ], ['Fourteen', ], ['Fifteen', ], ['Sixteen', ], ['Seventeen', ], ['Eighteen', ], ['Nineteen', ]];
    // digits outside triplets
    var dot = (num.length % 3) ? num.length % 3 : 3;
    // number of triplets in number
    var sets = Math.ceil(num.length / 3);
    // result string, word as number
    var rslt = '';
//_ convert every three digits
    for (var i = 0; i < sets; i++) { // for each set of triplets
        // capture set of numbers up to three digits
        var subt = num.substring((!i) ? 0 : dot + (i - 1) * 3, (!i) ? dot : dot + i * 3);
        if (subt != 0) { // if value of set is not 0...
            var hdec = (subt.length > 2) ? subt.charAt(0) : 0; // get hundreds digit
            var ddec = subt.substring(Math.max(subt.length - 2, 0), subt.length); // get double digits
            var odec = subt.charAt(subt.length - 1); // get one's digit
            // hundreds digit
            if (hdec != 0)
                rslt += ' ' + wnums[hdec][0] + ' Hundred ' + ((fmt && ddec == 0) ? 'Th' : ''); //-hundred
            // add double digit
            if (ddec < 20 && 9 < ddec) { // if less than 20...
                // add double digit word
                rslt += ' ' + ((fmt) ? ((wnums[ddec][1]) ? wnums[ddec][1] : wnums[ddec][0] + 'Th') : wnums[ddec][0]);
            } else { // otherwise, add words for each digit...
                // add "and" for last set without a tens digits
                if ((0 < hdec || 1 < sets) && i + 1 == sets && 0 < ddec && ddec < 10)
                    rslt += 'and ';
                // tens digit
                if (19 < ddec)
                    rslt += wnums[ddec.charAt(0)][(wnums[ddec.charAt(0)][2]) ? 2 : 0] + ((i + 1 == sets && odec == 0 && fmt) ? 'Tieth' : 'ty') + ((0 < odec) ? ' ' : ' ');//? '-'
                // one's digit
                if (0 < odec)
                    rslt += wnums[odec][(i + 1 == sets && fmt) ? 1 : 0];
            }
            // add place for set
            if (i + 1 < sets)
                rslt += ' ' + wnums[0][sets - i - 1] + ' '; // if not last set, add place
        } else if (i + 1 == sets && fmt) { // otherwise, if this set is zero, is the last set of the loop, and the format (fmt) is cardinal
            rslt += 'th'; // add cardinal "th"
        }
    }
//_ return result
    return rslt
}

// function for calculate total Quantity
function calTotalQuentity() {
    var quantity1;
    var quantity2;
    var quantity3;
    var quantity4;
    var Totalquantitycal;
    quantity1 = replaceChars(document.all["Quantity1"].value, ",", "");
    quantity2 = replaceChars(document.all["Quantity2"].value, ",", "");
    quantity3 = replaceChars(document.all["Quantity3"].value, ",", "");
    quantity4 = replaceChars(document.all["Quantity4"].value, ",", "");
    quantity5 = replaceChars(document.all["Quantity5"].value, ",", "");
    quantity6 = replaceChars(document.all["Quantity6"].value, ",", "");
    quantity7 = replaceChars(document.all["Quantity7"].value, ",", "");
    quantity8 = replaceChars(document.all["Quantity8"].value, ",", "");
    Totalquantitycal = 0;
    if (quantity1 > 0)
        Totalquantitycal = Number(Totalquantitycal) + Number(quantity1);
    if (quantity2 > 0)
        Totalquantitycal = Number(Totalquantitycal) + Number(quantity2);
    if (quantity3 > 0)
        Totalquantitycal = Number(Totalquantitycal) + Number(quantity3);
    if (quantity4 > 0)
        Totalquantitycal = Number(Totalquantitycal) + Number(quantity4);
    if (quantity5 > 0)
        Totalquantitycal = Number(Totalquantitycal) + Number(quantity5);
    if (quantity6 > 0)
        Totalquantitycal = Number(Totalquantitycal) + Number(quantity6);
    if (quantity7 > 0)
        Totalquantitycal = Number(Totalquantitycal) + Number(quantity7);
    if (quantity8 > 0)
        Totalquantitycal = Number(Totalquantitycal) + Number(quantity8);
    document.all["Total_Quantity"].value = Totalquantitycal.toFixed(2);
    calTotalFOB();
    mysetcomma('Total_Fob');
    caltaka();
    clacashinc();
}
//Calculate other charges
function calOthercharges() {
    var Totalamountcal;
    var othercharge;
    var storagecharge;
    var financecharge;
    var freightcharge;
    othercharge = replaceChars(document.all["Other_Charge"].value, ",", "");
    storagecharge = replaceChars(document.all["Storage_Charge"].value, ",", "");
    financecharge = replaceChars(document.all["Finance_Charge"].value, ",", "");
    freightcharge = replaceChars(document.all["Freight_Charge"].value, ",", "");
    Totalamountcal = 0;
    if (othercharge > 0)
        Totalamountcal = Number(Totalamountcal) + Number(othercharge);
    if (storagecharge > 0)
        Totalamountcal = Number(Totalamountcal) + Number(storagecharge);
    if (financecharge > 0)
        Totalamountcal = Number(Totalamountcal) + Number(financecharge);
    if (freightcharge > 0)
        Totalamountcal = Number(Totalamountcal) + Number(freightcharge);
    if (Totalamountcal > 0) {
        document.all["Total_Charges"].value = Totalamountcal.toFixed(2);
        mysetcomma('Total_Charges');
    } else
        document.all["Total_Charges"].value = "";
}

//Calculate Total FOB
function calTotalFOB() {
    var amount1;
    var amount2;
    var amount3;
    var amount4;
    var Totalamountcal;
    var quantity1;
    var quantity2;
    var quantity3;
    var quantity4;
    var othercharge;
    var Totalquantitycal;
    var storagecharge;
    var financecharge;
    var freightcharge;
    quantity1 = replaceChars(document.all["Quantity1"].value, ",", "");
    quantity2 = replaceChars(document.all["Quantity2"].value, ",", "");
    quantity3 = replaceChars(document.all["Quantity3"].value, ",", "");
    quantity4 = replaceChars(document.all["Quantity4"].value, ",", "");
    amount1 = replaceChars(document.all["Unit_Price_A1"].value, ",", "");
    amount2 = replaceChars(document.all["Unit_Price_A2"].value, ",", "");
    amount3 = replaceChars(document.all["Unit_Price_A3"].value, ",", "");
    amount4 = replaceChars(document.all["Unit_Price_A4"].value, ",", "");
    quantity5 = replaceChars(document.all["Quantity5"].value, ",", "");
    quantity6 = replaceChars(document.all["Quantity6"].value, ",", "");
    quantity7 = replaceChars(document.all["Quantity7"].value, ",", "");
    quantity8 = replaceChars(document.all["Quantity8"].value, ",", "");
    amount5 = replaceChars(document.all["Unit_Price_A5"].value, ",", "");
    amount6 = replaceChars(document.all["Unit_Price_A6"].value, ",", "");
    amount7 = replaceChars(document.all["Unit_Price_A7"].value, ",", "");
    amount8 = replaceChars(document.all["Unit_Price_A8"].value, ",", "");
    othercharge = replaceChars(document.all["Other_Charge"].value, ",", "");
    storagecharge = replaceChars(document.all["Storage_Charge"].value, ",", "");
    financecharge = replaceChars(document.all["Finance_Charge"].value, ",", "");
    freightcharge = replaceChars(document.all["Freight_Charge"].value, ",", "");
    Totalamountcal = 0;
    if (amount1 > 0 && quantity1 > 0)
        Totalamountcal = Number(Totalamountcal) + (Number(amount1) * Number(quantity1));
    if (amount2 > 0 && quantity2 > 0)
        Totalamountcal = Number(Totalamountcal) + (Number(amount2) * Number(quantity2));
    if (amount3 > 0 && quantity3 > 0)
        Totalamountcal = Number(Totalamountcal) + (Number(amount3) * Number(quantity3));
    if (amount4 > 0 && quantity4 > 0)
        Totalamountcal = Number(Totalamountcal) + (Number(amount4) * Number(quantity4));
    if (amount5 > 0 && quantity5 > 0)
        Totalamountcal = Number(Totalamountcal) + (Number(amount5) * Number(quantity5));
    if (amount6 > 0 && quantity6 > 0)
        Totalamountcal = Number(Totalamountcal) + (Number(amount6) * Number(quantity6));
    if (amount7 > 0 && quantity7 > 0)
        Totalamountcal = Number(Totalamountcal) + (Number(amount7) * Number(quantity7));
    if (amount8 > 0 && quantity8 > 0)
        Totalamountcal = Number(Totalamountcal) + (Number(amount8) * Number(quantity8));
    if (othercharge > 0)
        Totalamountcal = Number(Totalamountcal) + Number(othercharge);
    if (storagecharge > 0)
        Totalamountcal = Number(Totalamountcal) + Number(storagecharge);
    if (financecharge > 0)
        Totalamountcal = Number(Totalamountcal) + Number(financecharge);
    if (freightcharge > 0)
        Totalamountcal = Number(Totalamountcal) + Number(freightcharge);

    document.all["Total_Fob"].value = Totalamountcal.toFixed(2);
    numberinword('Total_Fob', 'Total_Fob_Words');
    caltaka();
    clacashinc();
}

// function for check valid number input
function checkvalidnumber(checkvalue) {
    var checkme;
    //var Totalquantitycal;
    checkme = document.all[checkvalue].value;
    if (checkme.length <= 0) {
        document.all[checkvalue].value = '';
    }
    if (checkme.indexOf(".") != checkme.lastIndexOf(".")) {
        count = 1;
        nArray[count] = 'Invalid Number in the ' + checkvalue + ' Field.';
        document.all[checkvalue].focus();
        ShowAllAlertMsg();
    } else {
        if (checkvalue == 'Quantity1' || checkvalue == 'Quantity2' || checkvalue == 'Quantity3' || checkvalue == 'Quantity4' || checkvalue == 'Quantity5' || checkvalue == 'Quantity6' || checkvalue == 'Quantity7' || checkvalue == 'Quantity8') {
            calTotalQuentity();
        } else if (checkvalue == 'Unit_Price_A1' || checkvalue == 'Unit_Price_A2' || checkvalue == 'Unit_Price_A3' || checkvalue == 'Unit_Price_A4' || checkvalue == 'Unit_Price_A5' || checkvalue == 'Unit_Price_A6' || checkvalue == 'Unit_Price_A7' || checkvalue == 'Unit_Price_A8' || checkvalue == 'Other_Charge' || checkvalue == 'Storage_Charge' || checkvalue == 'Finance_Charge' || checkvalue == 'Freight_Charge') {
            calTotalFOB();
        }
    }
}
//For set comma on the field value
function mysetcomma(setcommaobj) {
    var setcommavalue = "";
    var numcom = 0;
    var currvalue = "";
    var mydotpart = "";
    var currvaluereal = 0
    currvalue = document.all[setcommaobj].value;
    currvalue = replaceChars(currvalue, ",", "");
    if (currvalue.length > 0)
        currvaluereal = parseInt(currvalue, 10);
    if (currvaluereal > 0) {//00
        var mylen = currvalue.indexOf('.', 0);
        if (mylen != -1) {
            numcom = Number(currvalue.substr(0, mylen));
            //for dot part
            var startpos = currvalue.indexOf('.', 0) + 1;
            mylen = currvalue.length - currvalue.indexOf('.', 0) - 1;
            mydotpart = currvalue.substr(startpos, mylen);
            if (mydotpart.length == 1) {
                mydotpart = mydotpart + '0';//00
            }
        } else {
            numcom = Number(currvalue);
        }
    }
    if (numcom > 0) {
        numcom = Math.round(numcom).toString(); // round value
        // digits outside triplets
        var dotcomm = (numcom.length % 3) ? numcom.length % 3 : 3;
        // number of triplets in number
        var setscom = Math.ceil(numcom.length / 3);
        //_ convert every three digits
        for (var i = 0; i < setscom; i++) { // for each set of triplets
            // capture set of numbers up to three digits
            var subtcom = numcom.substring((!i) ? 0 : dotcomm + (i - 1) * 3, (!i) ? dotcomm : dotcomm + i * 3);
            setcommavalue = setcommavalue + subtcom + ',';
        }
        if (mydotpart.length > 1) {
            document.all[setcommaobj].value = setcommavalue.substr(0, (setcommavalue.length - 1)) + '.' + mydotpart;
        } else {
            document.all[setcommaobj].value = setcommavalue.substr(0, (setcommavalue.length - 1));//+'.0000'
        }
    } else {
        document.all[setcommaobj].value = currvalue;
    }
}

//for Negotiating Documents
function checkempty(checkobj) {
    var myvalue = document.all[checkobj].value;
    if (isNaN(myvalue) == true) {
        document.all[checkobj].value = "";
    }
}
//for Set comma on load
function setcommaonload() {
    //For Quantity
    mysetcomma('Quantity1');
    mysetcomma('Quantity2');
    mysetcomma('Quantity3');
    mysetcomma('Quantity4');
    mysetcomma('Quantity5');
    mysetcomma('Quantity6');
    mysetcomma('Quantity7');
    mysetcomma('Quantity8');
    //for unit price
    mysetcomma('Total_Quantity');
    mysetcomma('Unit_Price_A1');
    mysetcomma('Unit_Price_A2');
    mysetcomma('Unit_Price_A3');
    mysetcomma('Unit_Price_A4');
    mysetcomma('Unit_Price_A5');
    mysetcomma('Unit_Price_A6');
    mysetcomma('Unit_Price_A7');
    mysetcomma('Unit_Price_A8');
    mysetcomma('Total_Fob');
    mysetcomma('Other_Charge');
    mysetcomma('Total_Gross_Weight');
    //for Payment Information
    mysetcomma('Amount_Rcvd');
    mysetcomma('Pymt_Rcvd_Amnt_Usd');
    mysetcomma('Pymt_Rcvd_Exchg_Rate');
    mysetcomma('Pymt_Rcvd_Amnt_Bdt');
    mysetcomma('Outstanding_Balance');
    //for Cash Incentive Information
    mysetcomma('Cash_Incentive_Amount');
    mysetcomma('Cash_Incentive_Rec_Amount');
    mysetcomma('Per_Of_Cash_Inc');
    mysetcomma('Freight_Charge');
    mysetcomma('Finance_Charge');
    mysetcomma('Storage_Charge');
    mysetcomma('Applied_Cash_Inc');
    mysetcomma('Audited_Cash_Inc_Amt');
    //mysetcomma('Cash_Incentive_Pending');
    mysetcomma('Bank_Charges');
    calOthercharges();
    //shiftcurrunit();
}
//for Remove comma before submit
function removecomma() {
    //For Quantity
    document.all["Quantity1"].value = replaceChars(document.all["Quantity1"].value, ",", "");
    document.all["Quantity2"].value = replaceChars(document.all["Quantity2"].value, ",", "");
    document.all["Quantity3"].value = replaceChars(document.all["Quantity3"].value, ",", "");
    document.all["Quantity4"].value = replaceChars(document.all["Quantity4"].value, ",", "");
    document.all["Quantity5"].value = replaceChars(document.all["Quantity5"].value, ",", "");
    document.all["Quantity6"].value = replaceChars(document.all["Quantity6"].value, ",", "");
    document.all["Quantity7"].value = replaceChars(document.all["Quantity7"].value, ",", "");
    document.all["Quantity8"].value = replaceChars(document.all["Quantity8"].value, ",", "");
    document.all["Total_Quantity"].value = replaceChars(document.all["Total_Quantity"].value, ",", "");
    //for unit price
    document.all['Unit_Price_A1'].value = replaceChars(document.all["Unit_Price_A1"].value, ",", "");
    document.all['Unit_Price_A2'].value = replaceChars(document.all["Unit_Price_A2"].value, ",", "");
    document.all['Unit_Price_A3'].value = replaceChars(document.all["Unit_Price_A3"].value, ",", "");
    document.all['Unit_Price_A4'].value = replaceChars(document.all["Unit_Price_A4"].value, ",", "");
    document.all['Unit_Price_A5'].value = replaceChars(document.all["Unit_Price_A5"].value, ",", "");
    document.all['Unit_Price_A6'].value = replaceChars(document.all["Unit_Price_A6"].value, ",", "");
    document.all['Unit_Price_A7'].value = replaceChars(document.all["Unit_Price_A7"].value, ",", "");
    document.all['Unit_Price_A8'].value = replaceChars(document.all["Unit_Price_A8"].value, ",", "");
    document.all['Total_Fob'].value = replaceChars(document.all["Total_Fob"].value, ",", "");
    //for other charge
    document.all['Freight_Charge'].value = replaceChars(document.all["Freight_Charge"].value, ",", "");
    document.all['Finance_Charge'].value = replaceChars(document.all["Finance_Charge"].value, ",", "");
    document.all['Storage_Charge'].value = replaceChars(document.all["Storage_Charge"].value, ",", "");
    document.all['Other_Charge'].value = replaceChars(document.all["Other_Charge"].value, ",", "");
    document.all['Total_Gross_Weight'].value = replaceChars(document.all["Total_Gross_Weight"].value, ",", "");

    //for Payment Information
    document.all['Amount_Rcvd'].value = replaceChars(document.all["Amount_Rcvd"].value, ",", "");
    document.all['Pymt_Rcvd_Amnt_Usd'].value = replaceChars(document.all["Pymt_Rcvd_Amnt_Usd"].value, ",", "");
    document.all['Pymt_Rcvd_Exchg_Rate'].value = replaceChars(document.all["Pymt_Rcvd_Exchg_Rate"].value, ",", "");
    document.all['Pymt_Rcvd_Amnt_Bdt'].value = replaceChars(document.all["Pymt_Rcvd_Amnt_Bdt"].value, ",", "");
    document.all['Outstanding_Balance'].value = replaceChars(document.all["Outstanding_Balance"].value, ",", "");
    //for Cash Incentive Information
    document.all['Per_Of_Cash_Inc'].value = replaceChars(document.all["Per_Of_Cash_Inc"].value, ",", "");
    document.all['Cash_Incentive_Rec_Amount'].value = replaceChars(document.all["Cash_Incentive_Rec_Amount"].value, ",", "");
    document.all['Cash_Incentive_Amount'].value = replaceChars(document.all["Cash_Incentive_Amount"].value, ",", "");
    document.all['Applied_Cash_Inc'].value = replaceChars(document.all["Applied_Cash_Inc"].value, ",", "");
    document.all['Audited_Cash_Inc_Amt'].value = replaceChars(document.all["Audited_Cash_Inc_Amt"].value, ",", "");
    //document.all['Cash_Incentive_Pending'].value=replaceChars(document.all["Cash_Incentive_Pending"].value,",","");
    document.all['Bank_Charges'].value = replaceChars(document.all["Bank_Charges"].value, ",", "");

}

//Chenge Export Bank Infomation
function ChangeExportBankInfo(myebid) {
    var Sclientindex = document.all[myebid].selectedIndex;
    if (Sclientindex == 0) {
        document.all["Exp_Bank_Add"].value = "";
        document.all["Tsoad"].value = "";
        document.all["Relationship_Man"].value = "";
        document.all["Account_Name"].value = "";
        document.all["Exp_Bank_Bdt_Acc_Num"].value = "";
        document.all["Exp_Bank_Usd_Acc_Num"].value = "";
        document.all["Swift_Code"].value = "";
    } else {
        document.all["Exp_Bank_Add"].value = replaceChars(ExpBankAddArr[Sclientindex], "<br>", "\r\n");
        document.all["Tsoad"].value = TsoadValueArr[Sclientindex];
        document.all["Relationship_Man"].value = RelationshipManArr[Sclientindex];
        document.all["Account_Name"].value = replaceChars(AccountNameArr[Sclientindex], "<br>", "\r\n");
        document.all["Exp_Bank_Bdt_Acc_Num"].value = ExpBankBdtAccNumArr[Sclientindex];
        document.all["Exp_Bank_Usd_Acc_Num"].value = ExpBankUsdAccNumArr[Sclientindex];
        document.all["Swift_Code"].value = SwiftCodeArr[Sclientindex];
    }
}
//Chenge Importer Bank Infomation
function ChangeImportBankInfo(myebid) {
    var Sclientindex = document.all[myebid].selectedIndex;
    if (Sclientindex == 0) {
        document.all["Imp_Bank_Add"].value = "";
        document.all["Imp_Bank_Acc_Num"].value = "";
        document.all["Bank_Id_Code"].value = "";
    } else {
        document.all["Imp_Bank_Add"].value = replaceChars(ImpBankAddArr[Sclientindex], "<br>", "\r\n");
        document.all["Imp_Bank_Acc_Num"].value = ImpBankAccNumArr[Sclientindex];
        document.all["Bank_Id_Code"].value = BankIdCodeArr[Sclientindex];
    }
}

function CheckLength(Objname, maxLength, errMessage) {
    var Checktxt = document.all[Objname].value;
    var Mymaxlength = Number(maxLength);
    if (Checktxt.length > Mymaxlength) {
        count = count + 1;
        nArray[count] = errMessage;
    }
}

function shiftcurrunit() {
    document.all["Pymt_Rcvd_Cur_Lbl"].value = document.all["Unit_Price_C1"].value;//.selectedIndex;
    document.all["Amount_Rcvd_Curr"].value = document.all["Unit_Price_C1"].value;
}
