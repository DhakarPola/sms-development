<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : May 2007
'Page Purpose  : Printed Remat Cirtificate
'******************************************************************************************************************************************
*/
</script>

<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page
import="com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<jsp:useBean id="util" scope="session" class="batbsms.Utility"></jsp:useBean>
<html>
<head>
<title>Remat Certificate Printing</title>
</head>
<body bgcolor="#ffffff">
<h1></h1>
<%
  String PrintStyle = util.changeIfNullnTrim( String.valueOf(request.getParameter("printStyle")));
  String CertificateNo = util.changeIfNullnTrim(String.valueOf(request.getParameter("certificateno")));
  String CertificateFrom = util.changeIfNullnTrim(String.valueOf(request.getParameter("certificateFrom")));
  String CertificateTo = util.changeIfNullnTrim(String.valueOf(request.getParameter("certificateTo")));

  String reporturl = "";

  if (PrintStyle.equals("on"))
      reporturl = "/CR_Reports/Print_remat_certificate/ByCertificateNum.rpt";
  else if (PrintStyle.equals("range"))
      reporturl = "/CR_Reports/Print_remat_certificate/ByRange.rpt";

  if (!reporturl.equals(""))
  {
     session.setAttribute("reportSource", null);
     session.setAttribute("refreshed", null);
  }

   Object reportSource = session.getAttribute("reportSource");
   if (reportSource == null)
   {
   try{
     IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
     reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
     session.setAttribute("reportSource", reportSource);
   }catch (Exception e) {}
   }
     CrystalReportViewer viewer= new CrystalReportViewer();
     try
     {
       viewer.setReportSource(reportSource);
     }
     catch (Exception e) {}
  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setOwnForm(true);

  // Passing parameters
  ParameterField param1 = new ParameterField();
  param1.setReportName("");
  param1.setName("certNo");

  ParameterField param2 = new ParameterField();
  param2.setReportName("");
  param2.setName("certNoFrom");

  ParameterField param3 = new ParameterField();
  param3.setReportName("");
  param3.setName("certNoTo");


  Values vals1 = new Values();
  ParameterFieldDiscreteValue val1 = new ParameterFieldDiscreteValue();
  val1.setValue(CertificateNo);
  vals1.add(val1);


  Values vals2 = new Values();
  ParameterFieldDiscreteValue val2 = new ParameterFieldDiscreteValue();
  val2.setValue(CertificateFrom);
  vals2.add(val2);

  Values vals3 = new Values();
  ParameterFieldDiscreteValue val3 = new ParameterFieldDiscreteValue();
  val3.setValue(CertificateTo);
  vals3.add(val3);

  param1.setCurrentValues(vals1);
  param2.setCurrentValues(vals2);
  param3.setCurrentValues(vals3);


  Fields fields = new Fields();
  fields.add(param1);
  fields.add(param2);
  fields.add(param3);

  viewer.setParameterFields(fields);

  if (session.getAttribute("refreshed") == null)
  {
    viewer.refresh();
    session.setAttribute("refreshed", "true");
  }
   try
   {
      viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);
   }
   catch(Exception e){System.out.println(e.getMessage());
 }
%>
</body>
</html>
