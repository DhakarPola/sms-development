<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : February 2006
'Page Purpose  : Passes Parameters for Right Share Invitition Letter Printing.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}
%>

<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<%@page
import="java.util.Date,java.text.*,com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
Right Share Invitition Letter
</title>
</head>
<body bgcolor="#ffffff">
<h1>
</h1>
<%
  String DateDec = String.valueOf(request.getParameter("dateselect"));
  String RightType = String.valueOf(request.getParameter("righttype"));
  String PrintScope = String.valueOf(request.getParameter("printscope"));
  String ForBNo = String.valueOf(request.getParameter("foliono"));

  if (String.valueOf(ForBNo).equals("null"))
   ForBNo = "";

  String reporturl = "";

  if (RightType.equalsIgnoreCase("SMStype"))
  {
    if (PrintScope.equalsIgnoreCase("printsingle"))
    {
     reporturl = "/CR_Reports/Folio_Right_Letter/Folio_RightLetterByFolio.rpt";
    }
    else if (PrintScope.equalsIgnoreCase("printall"))
    {
      reporturl = "/CR_Reports/Folio_Right_Letter/Folio_RightLetterNotPrinted.rpt";
    }
  }
  else if (RightType.equalsIgnoreCase("CDBLtype"))
  {
    if (PrintScope.equalsIgnoreCase("printsingle"))
    {
      reporturl = "/CR_Reports/BO_Right_Letter/BO_RightLetterByBO.rpt";
    }
    else if (PrintScope.equalsIgnoreCase("printall"))
    {
      reporturl = "/CR_Reports/BO_Right_Letter/BO_RightLetterByALL.rpt";
    }
  }

  if (!reporturl.equals("")) {
    session.setAttribute("reportSource", null);
    session.setAttribute("refreshed", null);
  }

Object reportSource = session.getAttribute("reportSource");
if (reportSource == null)
{
try{
  IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
  reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
  session.setAttribute("reportSource", reportSource);
}catch (Exception e) {}
}
  CrystalReportViewer viewer= new CrystalReportViewer();
  try{
    viewer.setReportSource(reportSource);
  }catch (Exception e) {}

  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setOwnForm(true);

// Passing parameters

ParameterField param1 = new ParameterField();
param1.setReportName(""); // must always be set, even if just to ""
param1.setName("ParamDeclareDate");

Values vals1 = new Values();
ParameterFieldDiscreteValue val1 = new ParameterFieldDiscreteValue();
val1.setValue(DateDec);
vals1.add(val1);

ParameterField param2 = new ParameterField();
param2.setReportName(""); // must always be set, even if just to ""
param2.setName("ParamID");

Values vals2 = new Values();
ParameterFieldDiscreteValue val2 = new ParameterFieldDiscreteValue();
val2.setValue(ForBNo);
vals2.add(val2);

try {
 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
 Date d = df.parse(DateDec);
 val1.setValue(d);
 vals1.add(val1);
} catch(Exception e) {
e.printStackTrace();
}

param1.setCurrentValues(vals1);
param2.setCurrentValues(vals2);

Fields fields = new Fields();
fields.add(param1);
fields.add(param2);

viewer.setParameterFields(fields);

if (session.getAttribute("refreshed") == null)
{
viewer.refresh();
session.setAttribute("refreshed", "true");
}


try
{
viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);
}
catch(Exception e){System.out.println(e.getMessage());}

%>
</body>
</html>

