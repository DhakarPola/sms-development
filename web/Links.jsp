<script>
    /*
     '******************************************************************************************************************************************
     'Script Author : Renad Hakim
     'Creation Date : October 2005
     'Page Purpose  : Page that uses an applet to show links for the user based on the user's role.
     '******************************************************************************************************************************************
     */
</script>

<%@ page errorPage="CommonError.jsp" %>
<% if (String.valueOf(session.getAttribute("UserName")).equals("null")) {
%>
<script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
    }%>

<html>
    <head>
        <title>
            Menu- Contents
        </title>
        <style>
            <!--

            #foldheader{
                cursor:pointer;
                cursor:hand;
                font-family: Arial;
                font-weight:bold;
                list-style-image:url(images/fold.gif);
                margin-left:-25px;
                left:0px;
                font-size: 11px;
            }

            #foldsubheader{
                cursor:pointer;
                cursor:hand;
                font-family: Arial;
                font-weight:bold;
                list-style-image:url(images/fold.gif);
                margin-left:-25px;
                left:0px;
                font-size: 10px;
            }

            #foldinglist{
                list-style-image:url(images/list.gif);
                margin-left:-30px;
                font-family: Arial;
            }



            #openlist{
                list-style-image:url(images/list.gif);
                margin-left:10px;
                font-weight:bold;
                font-family: Arial;
                font-size: 10px;
            }

            .head {
                font-family: Arial;
                font-size: 9px;
                font-style: normal;
            }

            a {
                font-family: Arial;
                font-size: 9px;
                font-style: normal;
                color: #000000;
                text-decoration: none;
            }
            -->
        </style>
        <script language="JavaScript1.2">
<!--

            //Smart Folding Menu tree- By Dynamic Drive (rewritten 03/03/02)
            //For full source code and more DHTML scripts, visit http://www.dynamicdrive.com
            //This credit MUST stay intact for use

            var head = "display:''"
            img1 = new Image()
            img1.src = "images/fold.gif"
            img2 = new Image()
            img2.src = "images/open.gif"

            var ns6 = document.getElementById && !document.all
            var ie4 = document.all && navigator.userAgent.indexOf("Opera") == -1

            var openmenu;
            var openmenutop;
            var iscall = "Not";
            var curTransfer;
            var menuTransfer;
            var ClickMenuItem = 0;
            var PreviousMenuItem = "";

            function checkcontained(e)            {
                var iscontained = 0;
                cur = ns6 ? e.target : event.srcElement;
                i = 0;
                if ((cur.id == "foldheader") || (cur.id == "foldsubheader")){
                    iscontained = 1
                } else
                    while (ns6 && cur.parentNode || (ie4 && cur.parentElement))                    {
                        if (cur.id == "foldheader" || cur.id == "foldsubheader" || cur.id == "foldinglist" || cur.id == "openlist")                        {
                            iscontained = (cur.id == "foldheader") ? 1 : 0
                            var menucontent = event.srcElement;
                            if (ClickMenuItem > 0)                            {
                                PreviousMenuItem.style.color = "#000000";
                            }
                            menucontent.style.color = "red";
                            PreviousMenuItem = menucontent;
                            ClickMenuItem = 1;
                            break;
                        }
                        cur = ns6 ? cur.parentNode : cur.parentElement;
                    }

                if (iscontained)                {
                    var foldercontent = ns6 ? cur.nextSibling.nextSibling : cur.all.tags("UL")[0]
                    if (iscall == "Yes" && foldercontent.name != openmenu.name)                    {
                        if ((cur.name == 'liSTransfer' || cur.name == 'liNTransfer' || cur.name == 'subimp6' || cur.name == 'subimp1' || cur.name == 'subimp2' || cur.name == 'subimp3' || cur.name == 'subimp4' || cur.name == 'folist' || cur.name == 'complist' || cur.name == 'liBankRecon' || cur.name == 'liAccountMerg' || cur.name == 'liMerger' || cur.name == 'shlist') && openmenutop.name == 'liTransfer')                        {
                            curTransfer = openmenutop;
                            menuTransfer = openmenu;
                            //(openmenutop.name=='liSTransfer' || openmenutop.name=='liNTransfer')
                            if (openmenutop.name != 'liTransfer')                            {
                                if (openmenu.style.display == "none")                                {
                                    openmenu.style.display = "";
                                    openmenutop.style.listStyleImage = "url(images/open.gif)";
                                } else                                {
                                    openmenu.style.display = "none";
                                    openmenutop.style.listStyleImage = "url(images/fold.gif)";
                                }
                            }
                        } else if ((cur.name == 'liSTransfer' || cur.name == 'liNTransfer' || cur.name == 'subimp1' || cur.name == 'subimp6' || cur.name == 'subimp2' || cur.name == 'subimp3' || cur.name == 'subimp4' || cur.name == 'folist' || cur.name == 'complist' || cur.name == 'liBankRecon' || cur.name == 'liAccountMerg' || cur.name == 'liMerger' || cur.name == 'shlist') && openmenutop.name == 'liImport')                        {
                            curTransfer = openmenutop;
                            menuTransfer = openmenu;
                            //(openmenutop.name=='liSTransfer' || openmenutop.name=='liNTransfer')
                            if (openmenutop.name != 'liImport')                            {
                                if (openmenu.style.display == "none")                                {
                                    openmenu.style.display = "";
                                    openmenutop.style.listStyleImage = "url(images/open.gif)";
                                } else                                {
                                    openmenu.style.display = "none";
                                    openmenutop.style.listStyleImage = "url(images/fold.gif)";
                                }
                            }
                        } else if ((cur.name == 'liSTransfer' || cur.name == 'liNTransfer' || cur.name == 'subimp1' || cur.name == 'subimp6' || cur.name == 'subimp2' || cur.name == 'subimp3' || cur.name == 'subimp4' || cur.name == 'folist' || cur.name == 'complist' || cur.name == 'liBankRecon' || cur.name == 'liAccountMerg' || cur.name == 'liMerger' || cur.name == 'shlist') && openmenutop.name == 'liFolio')                        {
                            curTransfer = openmenutop;
                            menuTransfer = openmenu;
                            //(openmenutop.name=='liSTransfer' || openmenutop.name=='liNTransfer')
                            if (openmenutop.name != 'liFolio')                            {
                                if (openmenu.style.display == "none")                                {
                                    openmenu.style.display = "";
                                    openmenutop.style.listStyleImage = "url(images/open.gif)";
                                } else                                {
                                    openmenu.style.display = "none";
                                    openmenutop.style.listStyleImage = "url(images/fold.gif)";
                                }
                            }
                        } else if ((cur.name == 'liSTransfer' || cur.name == 'liNTransfer' || cur.name == 'subimp1' || cur.name == 'subimp6' || cur.name == 'subimp2' || cur.name == 'subimp3' || cur.name == 'subimp4' || cur.name == 'folist' || cur.name == 'complist' || cur.name == 'liBankRecon' || cur.name == 'liAccountMerg' || cur.name == 'liMerger' || cur.name == 'shlist') && openmenutop.name == 'liComplaint')                        {
                            curTransfer = openmenutop;
                            menuTransfer = openmenu;
                            //(openmenutop.name=='liSTransfer' || openmenutop.name=='liNTransfer')
                            if (openmenutop.name != 'liComplaint')                            {
                                if (openmenu.style.display == "none")                                {
                                    openmenu.style.display = "";
                                    openmenutop.style.listStyleImage = "url(images/open.gif)";
                                } else                                {
                                    openmenu.style.display = "none";
                                    openmenutop.style.listStyleImage = "url(images/fold.gif)";
                                }
                            }
                        } else if ((cur.name == 'liSTransfer' || cur.name == 'liNTransfer' || cur.name == 'subimp1' || cur.name == 'subimp6' || cur.name == 'subimp2' || cur.name == 'subimp3' || cur.name == 'subimp4' || cur.name == 'folist' || cur.name == 'complist' || cur.name == 'liBankRecon' || cur.name == 'liAccountMerg' || cur.name == 'liMerger' || cur.name == 'shlist') && openmenutop.name == 'liBankAcc')                        {
                            curTransfer = openmenutop;
                            menuTransfer = openmenu;
                            //(openmenutop.name=='liSTransfer' || openmenutop.name=='liNTransfer')
                            if (openmenutop.name != 'liBankAcc')                            {
                                if (openmenu.style.display == "none")                                {
                                    openmenu.style.display = "";
                                    openmenutop.style.listStyleImage = "url(images/open.gif)";
                                } else                                {
                                    openmenu.style.display = "none";
                                    openmenutop.style.listStyleImage = "url(images/fold.gif)";
                                }
                            }
                        } else if ((cur.name == 'liSTransfer' || cur.name == 'liNTransfer' || cur.name == 'subimp1' || cur.name == 'subimp6' || cur.name == 'subimp2' || cur.name == 'subimp3' || cur.name == 'subimp4' || cur.name == 'folist' || cur.name == 'complist' || cur.name == 'liBankRecon' || cur.name == 'liAccountMerg' || cur.name == 'liMerger' || cur.name == 'shlist') && openmenutop.name == 'liReports')
                        {
                            curTransfer = openmenutop;
                            menuTransfer = openmenu;
                            //(openmenutop.name=='liSTransfer' || openmenutop.name=='liNTransfer')
                            if (openmenutop.name != 'liReports')
                            {
                                if (openmenu.style.display == "none")
                                {
                                    openmenu.style.display = "";
                                    openmenutop.style.listStyleImage = "url(images/open.gif)";
                                } else
                                {
                                    openmenu.style.display = "none";
                                    openmenutop.style.listStyleImage = "url(images/fold.gif)";
                                }
                            }
                        } else if (((cur.name == 'liSTransfer' || cur.name == 'liNTransfer' || cur.name == 'subimp1' || cur.name == 'subimp6' || cur.name == 'subimp2' || cur.name == 'subimp3' || cur.name == 'subimp4' || cur.name == 'folist' || cur.name == 'complist' || cur.name == 'liBankRecon' || cur.name == 'liAccountMerg' || cur.name == 'liMerger' || cur.name == 'shlist') && openmenutop.name != 'liTransfer'))
                        {
                            if (openmenu.style.display == "none")
                            {
                                openmenu.style.display = "";
                                openmenutop.style.listStyleImage = "url(images/open.gif)";
                            } else
                            {
                                openmenu.style.display = "none";
                                openmenutop.style.listStyleImage = "url(images/fold.gif)";
                            }
                        } else
                        {
                            if (openmenutop.name == 'liNTransfer' || openmenutop.name == 'liSTransfer' || openmenutop.name == 'subimp1' || cur.name == 'subimp6' || openmenutop.name == 'subimp2' || openmenutop.name == 'subimp3' || cur.name == 'subimp4' || openmenutop.name == 'folist' || openmenutop.name == 'complist' || cur.name == 'liBankRecon' || cur.name == 'liAccountMerg' || cur.name == 'liMerger' || cur.name == 'shlist')
                            {
                                if (openmenu.style.display == "none")
                                {
                                    openmenu.style.display = "";
                                    openmenutop.style.listStyleImage = "url(images/open.gif)";
                                } else
                                {
                                    openmenu.style.display = "none";
                                    openmenutop.style.listStyleImage = "url(images/fold.gif)";
                                }

                                if (menuTransfer.style.display == "none")
                                {
                                    menuTransfer.style.display = "";
                                    curTransfer.style.listStyleImage = "url(images/open.gif)";
                                } else
                                {
                                    menuTransfer.style.display = "none";
                                    curTransfer.style.listStyleImage = "url(images/fold.gif)";
                                }
                            } else
                            {
                                if (openmenu.style.display == "none")
                                {
                                    openmenu.style.display = "";
                                    openmenutop.style.listStyleImage = "url(images/open.gif)";
                                } else
                                {
                                    openmenu.style.display = "none";
                                    openmenutop.style.listStyleImage = "url(images/fold.gif)";
                                }
                            }
                        }
                    }

                    if (foldercontent.style.display == "none")
                    {
                        foldercontent.style.display = "";
                        cur.style.listStyleImage = "url(images/open.gif)";
                        openmenu = foldercontent;
                        openmenutop = cur;
                        iscall = "Yes";
                    } else
                    {
                        foldercontent.style.display = "none";
                        cur.style.listStyleImage = "url(images/fold.gif)";
                        iscall = "Not";
                        openmenu = '';
                        openmenutop = '';
                    }
                }
            }

            if (ie4 || ns6)
                document.onclick = checkcontained
-->
            </script>
        <SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/tabber-minimized.js"></SCRIPT>
    <SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/tabber.js"></SCRIPT>
    <LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/tab.css" TYPE="text/css"></LINK>

</head>
<body bgcolor=#FFFFFF topmargin="0" leftmargin="2" background="images/leftBG.gif">

    <font face="Arial" class="head">
    <br>
    <br>
    </font>

    <div class="tabber">

        <div class="tabbertab">
            <h2>General</h2>
            <p>
            <ul>
                <li id="foldheader" name="liSystemAD">System</li>
                <ul id="foldinglist" name="Sysadmin" style="display:none" style=&{head};>
                    <li><a href="<%=request.getContextPath()%>/jsp/Users/AllUsers.jsp" target="rightFrame">Stand-alone Users</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Users/AdUserSelection.jsp" target="rightFrame">Users</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Bank/AllBanks.jsp" target="rightFrame">Bank</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Accounts/AccountConfig.jsp" target="rightFrame">Accounts</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Signatures/AllSignatures.jsp" target="rightFrame">Signatures</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Category/AllCategories.jsp" target="rightFrame">Categories</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Nominee/AllNominees.jsp" target="rightFrame">Dividend Groups</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Others/Status.jsp" target="rightFrame">Status</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Users/Configuration.jsp" target="rightFrame">Configuration</a></li>
                </ul>

                <li id="foldheader" name="liFolio">Folio</li>
                <ul id="foldinglist" name="Folio" style="display:none" style=&{head};>
                    <li id="foldsubheader" name="folist" style="margin-left:0px">Folio List</li>
                    <ul id="foldinglist" name="FL" style="display:none" style=&{head};>
                        <li><a href="<%=request.getContextPath()%>/jsp/Folio/AllPermanentFolios.jsp" target="rightFrame">by Folio No.</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/Folio/FoliobyName.jsp" target="rightFrame">by Name</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/Nominee/ViewbyNominee.jsp?nomineeno=0&StartValue=0&EndValue=0" target="rightFrame">by Dividend Group</a></li>
                    </ul>
                    <li><a href="<%=request.getContextPath()%>/jsp/Folio/freezefolio.jsp" target="rightFrame">Freeze Folio</a></li>
                    <!--<li><a href="//request.getContextPath()/jsp/Folio/NewCertificate_Upload.jsp" target="rightFrame">Upload New Certificate</a></li>-->
                    <li><a href="<%=request.getContextPath()%>/jsp/Folio/AllFolios.jsp" target="rightFrame">New Folio</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Folio/AcceptFolios.jsp" target="rightFrame">Folio Acceptence/Rejection</a></li>
                </ul>

                <li id="foldheader" name="liCertificate">Certificate</li>
                <ul id="foldinglist" name="CertificateS" style="display:none" style=&{head};>
                    <li><a href="<%=request.getContextPath()%>/jsp/Certificate/CertificateEditView.jsp" target="rightFrame">Certificate Edit</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Certificate/NonPrintedCertificatesView.jsp" target="rightFrame">Not Printed Certificates</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Certificate/PrintCertificate.jsp" target="rightFrame">Print Certificates</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Certificate/CertificateLock.jsp" target="rightFrame">Lock Certificates</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Certificate/LockList.jsp" target="rightFrame">List of Locked Certificates</a></li>
                    <li><a href="<%=request.getContextPath()%>/TransferList.jsp" target="rightFrame">List of Transferred Certificates</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Certificate/CertificateInformationView.jsp" target="rightFrame">Certificate Information</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Others/VerifySignature.jsp" target="rightFrame">Certificate Verification</a></li>
                </ul>

                <li id="foldheader" name="liTransfer">Share Transfer</li>
                <ul id="foldinglist" name="Transfer" style="display:none" style=&{head};>
                    <li id="foldsubheader" name="liNTransfer" style="margin-left:0px">Normal Transfer</li>
                    <ul id="foldinglist" name="NTransfer" style="display:none" style=&{head};>
                        <li><a href="<%=request.getContextPath()%>/jsp/Transfer/SimpleTransfer.jsp" target="rightFrame">Simple</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/Transfer/MultipleTransfer.jsp" target="rightFrame">Multiple</a></li>
                    </ul>
                    <li id="foldsubheader" name="liSTransfer" style="margin-left:0px">Split</li>
                    <ul id="foldinglist" name="STransfer" style="display:none" style=&{head};>
                        <li><a href="<%=request.getContextPath()%>/jsp/Transfer/NormalSplitting.jsp" target="rightFrame">Simple</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/Transfer/MultipleSplitting.jsp" target="rightFrame">Multiple</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/Transfer/AmalgamatedSplitting.jsp" target="rightFrame">Amalgamated</a></li>
                    </ul>
                    <li><a href="<%=request.getContextPath()%>/jsp/Transfer/Consolidation.jsp" target="rightFrame">Consolidation</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Transfer/Amalgamation.jsp" target="rightFrame">Amalgamation</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Transfer/AcceptTransfer.jsp" target="rightFrame">Accept Transfers</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Transfer/LastTransfers.jsp" target="rightFrame">Last Accepted Transfers</a></li>
                </ul>

                <li id="foldheader" name="liImport">Import from CDBL</li>
                <ul id="foldinglist" name="Impfc1" style="display:none" style=&{head};>
                    <li id="foldsubheader" name="subimp4" style="margin-left:0px">Demat Requests</li>
                    <ul id="foldinglist" name="DR" style="display:none" style=&{head};>
                        <li><a href="<%=request.getContextPath()%>/jsp/CDBL/DReqImport.jsp" target="rightFrame">Import</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/CDBL/DematRequests.jsp" target="rightFrame">View</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/CDBL/DRCheck.jsp" target="rightFrame">Check</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/CDBL/DRReference.jsp" target="rightFrame">Reference List</a></li>
                    </ul>
                    <li id="foldsubheader" name="subimp1" style="margin-left:0px">Demat Confirmations</li>
                    <ul id="foldinglist" name="DC" style="display:none" style=&{head};>
                        <li><a href="<%=request.getContextPath()%>/jsp/CDBL/DCImport.jsp" target="rightFrame">Import</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/CDBL/DematConfirmation.jsp" target="rightFrame">View</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/CDBL/DCNotReconciled.jsp" target="rightFrame">Not Reconciled</a></li>
                        <li><a href="<%=request.getContextPath()%>/DematReport.jsp" target="rightFrame">Report</a></li>
                    </ul>
                    <li id="foldsubheader" name="subimp2" style="margin-left:0px">BO Holding</li>
                    <ul id="foldinglist" name="BH" style="display:none" style=&{head};>
                        <li><a href="<%=request.getContextPath()%>/jsp/CDBL/BOHolding.jsp" target="rightFrame">Import</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/CDBL/ViewBOHolding.jsp" target="rightFrame">View</a></li>
                    </ul>
                    <li><a href="<%=request.getContextPath()%>/jsp/CDBL/DematRegisters.jsp" target="rightFrame">Demat Register</a></li>
                    <li id="foldsubheader" name="subimp3" style="margin-left:0px">BO Address</li>
                    <ul id="foldinglist" name="BA" style="display:none" style=&{head};>
                        <li><a href="<%=request.getContextPath()%>/jsp/CDBL/BOAddress.jsp" target="rightFrame">Import</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/CDBL/ViewBOAddress.jsp" target="rightFrame">Reconciliation</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/CDBL/viewAddress.jsp" target="rightFrame">View</a></li>
                    </ul>
                </ul>
                <li id="foldheader" name="liDemat">Demat Overlap Solution</li>
                <ul id="foldinglist" name="Demat" style="display:none" style=&{head};>
                    <li><a href="<%=request.getContextPath()%>/jsp/Overlap/RegbyFolio.jsp" target="rightFrame">Check Register</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Overlap/SolvedOverlaps.jsp" target="rightFrame">Solved Overlaps</a></li>
                    <li><a href="<%=request.getContextPath()%>/OverlapReport.jsp" target="rightFrame">Overlap Report</a></li>
                </ul>

                <li id="foldheader" name="liRemat">Rematerialization</li>
                <ul id="foldinglist" name="Remat" style="display:none" style=&{head};>
                    <li><a href="<%=request.getContextPath()%>/jsp/Remat/EnterRemats.jsp" target="rightFrame">Enter Remat Info.</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Remat/AcceptRemats.jsp" target="rightFrame">Accept Remats</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Remat/AllRemats.jsp" target="rightFrame">All Accepted Remats</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Remat/LastRemats.jsp" target="rightFrame">Last Accepted Remats</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Remat/RematCertificates.jsp" target="rightFrame">Remat Certificates</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Remat/PrintCertificates.jsp" target="rightFrame">Print Remat Certificates</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Remat/RematSummary.jsp" target="rightFrame">Remat Summary</a></li>
                </ul>

            </ul>
            </p>
        </div>


        <div class="tabbertab">
            <h2>Dividend</h2>
            <p>
            <ul>
                <li id="foldheader" name="liDividend">Dividend</li>
                <ul id="foldinglist" name="Dividend" style="display:none" style=&{head};>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/DeclareDividend.jsp" target="rightFrame">Declare</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/AcceptDividend.jsp" target="rightFrame">Accept</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/SetLetterCheque.jsp" target="rightFrame">Letter Cheque</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/SetGroupLetterCheque.jsp" target="rightFrame">Group Letter Cheque</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/SetDividendGroupList.jsp" target="rightFrame">Dividend Group List</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/DividendReconciliation.jsp" target="rightFrame">Reconciliation</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/DividendDereconcile.jsp" target="rightFrame">De-Reconcile</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/DividendWarrantManagement.jsp" target="rightFrame">Duplicate Warrant Mgt.</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/DuplicateWarrantList.jsp" target="rightFrame">Duplicate Warrant List</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/DividendInquiry.jsp" target="rightFrame">Dividend Inquiry</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/DividendMemo.jsp" target="rightFrame">Dividend Memo</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/AllEarningPartial.jsp" target="rightFrame">Earning Per Share</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/DividendArchieve.jsp" target="rightFrame">Dividend History</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/ChaqueIssue/SearchChaque.jsp" target="rightFrame">Search & Issue Cheque</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/Tax/SearchDividend.jsp" target="rightFrame">Dividend Tax Edit</a></li>
                </ul>

                <li id="foldheader" name="liDividendReport">Dividend Report</li>
                <ul id="foldinglist" name="DividendReport" style="display:none" style=&{head};>
                    <li><a href="<%=request.getContextPath()%>/jsp/ChaqueIssue/SetDividendCheque.jsp" target="rightFrame">Devidend Cheque Report</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/DividendCollectionInfo.jsp" target="rightFrame">Dividend Collection Info.</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/DividendReturnReport.jsp" target="rightFrame">Dividend Return Report</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/SetDividendPrelist.jsp" target="rightFrame">Print Pre-List</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/SetDividendPostlist.jsp" target="rightFrame">Print Post-List</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/ExportDividendManagement.jsp" target="rightFrame">Export Dividend Info</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Dividend/IndiDividendStatus.jsp" target="rightFrame">Dividend Reports (Individual)</a></li>
                </ul>


                <li id="foldheader" name="liOmnibus">Group BO Dividend</li>
                <ul id="foldinglist" name="Omnibus" style="display:none" style=&{head};>
                    <li><a href="<%=request.getContextPath()%>/jsp/Omnibus/AllOmnibus.jsp" target="rightFrame">Omnibus List</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Omnibus/SDividendList.jsp" target="rightFrame">Migrate to Omnibus</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Omnibus/RDividendList.jsp" target="rightFrame">Omnibus Accept</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Omnibus/group/SetOmnibusGroupList.jsp" target="rightFrame">Group  Omnibus</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Omnibus/group/reportOmnibus.jsp" target="rightFrame">Omnibus Report</a></li>
                </ul>

                <li id="foldheader" name="liMargin">Margin Dividend</li>
                <ul id="foldinglist" name="Margins" style="display:none" style=&{head};>
                    <li><a href="<%=request.getContextPath()%>/jsp/Margin/AllMargin.jsp" target="rightFrame">Margin List</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Margin/SDividendList.jsp" target="rightFrame">Migrate to Margin</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Margin/RDividendList.jsp" target="rightFrame">Margin Accept</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Margin/group/SetMarginGroupList.jsp" target="rightFrame">Group  Margin</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Margin/group/reportMargin.jsp" target="rightFrame">Margin Report</a></li>
                </ul>
            </ul>
            </p>
        </div>

        <div class="tabbertab">
            <h2>Others</h2>
            <p>
            <ul>
                <li id="foldheader" name="liBankAcc">Bank Accounts</li>
                <ul id="foldinglist" name="BankAcc" style="display:none" style=&{head};>
                    <li id="foldsubheader" name="liBankRecon" style="margin-left:0px">Bank Reconciliation</li>
                    <ul id="foldinglist" name="BankRecon" style="display:none" style=&{head};>
                        <li><a href="<%=request.getContextPath()%>/jsp/Dividend/SetBankRecon.jsp?account=0" target="rightFrame">Reconcile</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/Dividend/SetBankLetter.jsp?account=0&sig=0&sigid=0&letter=0&bank=0" target="rightFrame">Letter to Bank</a></li>
                    </ul>
                    <li id="foldsubheader" name="liAccountMerg" style="margin-left:0px">Merge Bank Accounts</li>
                    <ul id="foldinglist" name="AccountMerg" style="display:none" style=&{head};>
                        <li><a href="<%=request.getContextPath()%>/jsp/Dividend/MergeAccounts.jsp" target="rightFrame">Merge Bank Accounts</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/Bank/OffsetAmount.jsp" target="rightFrame">Offset Amount</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/Bank/SetAccountReport.jsp" target="rightFrame">Bank Account Report</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/Bank/ClaimedUnclaimedReport.jsp" target="rightFrame">Claimed/Unclaimed Report</a></li>
                    </ul>
                </ul>

                <li id="foldheader" name="liBonus">Bonus Share</li>
                <ul id="foldinglist" name="Bonus" style="display:none" style=&{head};>
                    <li><a href="<%=request.getContextPath()%>/jsp/Bonus/DeclareBonus.jsp" target="rightFrame">Declare</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Bonus/SetBonusPrelist.jsp" target="rightFrame">Print Pre-List</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Bonus/ImportBonus.jsp" target="rightFrame">Import Data from CDBL</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Bonus/CDBLReconciliation.jsp" target="rightFrame">Reconcile Data with CDBL</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Bonus/AcceptBonus.jsp" target="rightFrame">Accept</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Bonus/SetBonusPostlist.jsp" target="rightFrame">Print Allotment List</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Bonus/BonusLetterPrint.jsp" target="rightFrame">Print Letter to Shareholder</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Bonus/ViewBonusCertificates.jsp" target="rightFrame">Bonus Certificates</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Bonus/BonusCertificates.jsp" target="rightFrame">Print Bonus Certificates</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Bonus/BonusLetterCDBL.jsp" target="rightFrame">Print Letter to CDBL</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Bonus/ViewSellPriceInfo.jsp" target="rightFrame">Fraction Share Selling Price</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Bonus/SetBonusFractionList.jsp" target="rightFrame">Bonus Fraction List</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Bonus/BonusLetterandCheque.jsp" target="rightFrame">Fraction Letter & Cheque</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Bonus/BonusReconciliation.jsp" target="rightFrame">Reconciliation</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Bonus/BonusArchieve.jsp" target="rightFrame">Bonus Share History</a></li>
                </ul>

                <li id="foldheader" name="liRight">Right Share</li>
                <ul id="foldinglist" name="RightS" style="display:none" style=&{head};>
                    <li><a href="<%=request.getContextPath()%>/jsp/Right/DeclareRight.jsp" target="rightFrame">Declare</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Right/SetRightPrelist.jsp" target="rightFrame">Print Pre-List</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Right/AcceptRight.jsp" target="rightFrame">Accept</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Right/RightAllotmentList.jsp" target="rightFrame">Allotment List</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Right/RightLetterPrint.jsp" target="rightFrame">Print Letter to Shareholder</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Right/RightReconciliation.jsp" target="rightFrame">Acceptance Reconciliation</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Right/SetRightPostlist.jsp" target="rightFrame">Reconciled Allotment List</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Right/ViewRightCertificates.jsp" target="rightFrame">Right Certificates</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Right/RightCertificates.jsp" target="rightFrame">Print Right Certificates</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Right/RightLetterCDBL.jsp" target="rightFrame">Print Letter to CDBL</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Right/SellingPriceInfo.jsp" target="rightFrame">Fraction Share Selling Price</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Right/SetFractionList.jsp" target="rightFrame">Fraction List</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Right/RightLetterandCheque.jsp" target="rightFrame">Fraction Letter and Cheque</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Right/RightPaymentReconciliation.jsp" target="rightFrame">Payment Reconciliation</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Right/RightArchieve.jsp" target="rightFrame">Right Share History</a></li>
                </ul>



                <li id="foldheader" name="liAGM">AGM Management</li>
                <ul id="foldinglist" name="AGM" style="display:none" style=&{head};>
                    <li><a href="<%=request.getContextPath()%>/jsp/AGM/GenerateList.jsp" target="rightFrame">Generate AGM List</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/AGM/SetAttendanceList.jsp" target="rightFrame">Print Invitation List</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/AGM/SetInvitationLetter.jsp" target="rightFrame">Print Invitation Letter</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/AGM/SetAGMAttendanceProxy.jsp" target="rightFrame">AGM Attendance</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/AGM/SetAttendanceReport.jsp" target="rightFrame">Attendance Report</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/AGM/ShareholderReport.jsp" target="rightFrame">Shareholder Report</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/AGM/SetProxyView.jsp" target="rightFrame">Proxy</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/AGM/BoWithFolioList.jsp" target="rightFrame">BO Having Folio</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/AGM/MulBoSingleFolioList.jsp" target="rightFrame">Multiple BO Having Single Folio</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/AGM/MulFolioSingleBoList.jsp" target="rightFrame">Multiple Folio Having Single BO</a></li>
                </ul>
                <li id="foldheader" name="liPOLL">POLL Management</li>
                <ul id="foldinglist" name="POLL" style="display:none" style=&{head};>
                    <li><a href="<%=request.getContextPath()%>/jsp/POLL/GeneratePollList.jsp" target="rightFrame">Generate Poll List</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/POLL/setPoll.jsp" target="rightFrame">Poll</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/POLL/pollResult.jsp" target="rightFrame">Poll Result</a></li>
                </ul>
                <li id="foldheader" name="liComplaint">Complaint Register</li>
                <ul id="foldinglist" name="Complaint" style="display:none" style=&{head};>
                    <li id="foldsubheader" name="complist" style="margin-left:0px">Complaints</li>
                    <ul id="foldinglist" name="CL" style="display:none" style=&{head};>
                        <li><a href="<%=request.getContextPath()%>/jsp/Complaint/ComplaintbyReference.jsp" target="rightFrame">by Reference</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/Complaint/ComplaintbyDate.jsp" target="rightFrame">by Date</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/Complaint/ComplaintbyFolio.jsp" target="rightFrame">by Folio No./BO ID</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/Complaint/ComplaintbyName.jsp" target="rightFrame">by Name</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/Complaint/ComplaintbyStatus.jsp" target="rightFrame">by Status</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/Complaint/ComplaintbyPerson.jsp" target="rightFrame">by Person Assigned</a></li>
                    </ul>
                    <li><a href="<%=request.getContextPath()%>/jsp/Complaint/LodgeComplaint.jsp" target="rightFrame">Lodge Complaint</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Complaint/SetComplaintSummary.jsp" target="rightFrame">Complaint Summary</a></li>
                </ul>

                <li id="foldheader" name="liSMarket">Share Market</li>
                <ul id="foldinglist" name="SMarket" style="display:none" style=&{head};>
                    <li><a href="<%=request.getContextPath()%>/jsp/Others/MarketSharePrice.jsp?StartValue=1&EndValue=0" target="rightFrame">Market Share Price</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Others/MarketComparison.jsp" target="rightFrame">Market Comparison</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Others/MarketReport.jsp" target="rightFrame">Market Report</a></li>
                </ul>

                <li id="foldheader" name="liLetters">Letters</li>
                <ul id="foldinglist" name="Letters" style="display:none" style=&{head};>
                    <li><a href="<%=request.getContextPath()%>/jsp/Others/OthersLetterEdit.jsp" target="rightFrame">Letter Edit</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Others/OthersCircularLetter.jsp" target="rightFrame">Circular Letter</a></li>
                </ul>

                <li id="foldheader" name="liReports">Reports</li>
                <ul id="foldinglist" name="Reports" style="display:none" style=&{head};>
                    <li id="foldsubheader" name="shlist" style="margin-left:0px">Shareholding Reports</li>
                    <ul id="foldinglist" name="SR" style="display:none" style=&{head};>
                        <li><a href="<%=request.getContextPath()%>/jsp/Reports/ShareholdingReport.jsp" target="rightFrame">Shareholding Report</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/Reports/ExportShareholding.jsp" target="rightFrame">Export Shareholding Data</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/Reports/ShareCapitalReport.jsp" target="rightFrame">Share Capital Report</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/Reports/ExportShareCapital.jsp" target="rightFrame">Export Share Capital Data</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/Reports/MonthlySharholdingData.jsp" target="rightFrame">Monthly Shareholding Data</a></li>
                        <li><a href="<%=request.getContextPath()%>/jsp/Reports/ExportMonthlySharholdingData.jsp" target="rightFrame">Export Monthly Data</a></li>
                    </ul>
                    <li><a href="<%=request.getContextPath()%>/jsp/Reports/AnnualReport.jsp" target="rightFrame">Annual Return</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Reports/ShareHolderIndex.jsp" target="rightFrame">Share Holder's Index</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Reports/ShareRegister.jsp" target="rightFrame">Share Register</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Reports/StockExchange.jsp" target="rightFrame">Stock Exchange Report</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Reports/ShareHolderReport.jsp" target="rightFrame">Share Holders' Report</a></li>
                    <li><a href="<%=request.getContextPath()%>/jsp/Reports/ComplianceReport.jsp" target="rightFrame">SEC Compliance Report</a></li>

                    <li><a href="<%=request.getContextPath()%>/ShareBlockAllReport.jsp" target="rightFrame">Share Block All</a></li>
                    <li><a href="<%=request.getContextPath()%>/ShareBlockBOReport.jsp" target="rightFrame">Share Block BO</a></li>
                    <li><a href="<%=request.getContextPath()%>/ShareBlockFollioReport.jsp" target="rightFrame">SEC Compliance Report</a></li>

                </ul>
                </p>
        </div>
    </div>
    <ul>

        <li id="openlist" style="margin-left:-25px"><a href="<%=request.getContextPath()%>/SMSUserGuide.pdf" target="rightFrame" style="margin-left:0px">User Manual</a></li>
        <li id="openlist" style="margin-left:-25px"><a href="<%=request.getContextPath()%>/jsp/Users/ChangePassword.jsp" target="rightFrame" style="margin-left:0px">Change Password</a></li>
        <li id="openlist" style="margin-left:-25px"><a href="<%=request.getContextPath()%>/jsp/logout.jsp" target="rightFrame" style="margin-left:0px">Logout</a></li>



</body>
</html>
