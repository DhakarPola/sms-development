<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Updated By    : Renad Hakim
'Creation Date : November 2006
'Page Purpose  : Passes Parameters for AGM Invitation Letter Printing.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

 boolean isConnect = cm.connect();
 if(isConnect==false)
 {
   %>
    <jsp:forward page="ErrorMsg.jsp" >
    <jsp:param name="ErrorTitle" value="Connection Failure" />
    <jsp:param name="ErrorHeading" value="Connection Problem" />
    <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
    </jsp:forward>
  <%
 }

%>

<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<%@page
import="java.util.Date,java.text.*,com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
AGM Invitation Letter
</title>
</head>
<body bgcolor="#ffffff">
<h1>
</h1>
<%
    //Value of parameter received from page SetInvitationLetter.jsp
  String DivType = String.valueOf(request.getParameter("dividendtype"));
  String DateDec = String.valueOf(request.getParameter("dateselect"));
  String letter_id=String.valueOf(request.getParameter("lettertype"));
  String DivOrder = String.valueOf(request.getParameter("orderby"));
  String PRecords = String.valueOf(request.getParameter("printrecords"));
  String ForBNo = String.valueOf(request.getParameter("sfoliono"));
  String FStart = String.valueOf(request.getParameter("foliostart"));
  String FEnd = String.valueOf(request.getParameter("folioend"));
  String sigid = String.valueOf(request.getParameter("sigselect"));

  String test="0";

  if (String.valueOf(DivType).equals("null"))
   DivType = "";
  if (String.valueOf(DateDec).equals("null"))
   DateDec = "";
  if (String.valueOf(letter_id).equals("null"))
   letter_id = "";
  if (String.valueOf(DivOrder).equals("null"))
   DivOrder = "";
  if (String.valueOf(PRecords).equals("null"))
   PRecords = "";
  if (String.valueOf(FStart).equals("null"))
   FStart = "";
  if (String.valueOf(FEnd).equals("null"))
   FEnd = "";
  if (String.valueOf(ForBNo).equals("null"))
  ForBNo = "";
  if (String.valueOf(sigid).equals("null"))
   sigid = "";
   if (String.valueOf(DateDec).equals("null"))
   DateDec = "";



  String reporturl = "";

  if (DivType.equalsIgnoreCase("SMStype"))
  {
   if (PRecords.equalsIgnoreCase("singlebyfolio"))
   {
     reporturl ="";
     reporturl ="/CR_Reports/AGMInvitation_Letter/CDBL_SINGLE_BOID.rpt";
   }
  else if (PRecords.equalsIgnoreCase("notprinted"))
   {
     if (DivOrder.equalsIgnoreCase("byname"))
     {
         reporturl ="";
         reporturl ="/CR_Reports/AGMInvitation_Letter/SMS_ALL_NAME.rpt";
      }
     else if (DivOrder.equalsIgnoreCase("byfolio"))
     {
         reporturl ="";
        reporturl ="/CR_Reports/AGMInvitation_Letter/SMS_ALL_BOID.rpt";
     }
   }
   else if (PRecords.equalsIgnoreCase("rangebyfolio"))
   {
     if (DivOrder.equalsIgnoreCase("byname"))
     {
         reporturl ="";
         reporturl ="/CR_Reports/AGMInvitation_Letter/SMS_RANGE_NAME.rpt";
     }
     else if (DivOrder.equalsIgnoreCase("byfolio"))
     {
         reporturl ="";
         reporturl ="/CR_Reports/AGMInvitation_Letter/SMS_RANGE_BOID.rpt";
     }
   }
  }
  else if (DivType.equalsIgnoreCase("CDBLtype"))
  {
   if (PRecords.equalsIgnoreCase("singlebyfolio"))
   {
     reporturl ="";
    reporturl ="/CR_Reports/AGMInvitation_Letter/CDBL_SINGLE_BOID.rpt";
  }
   else if (PRecords.equalsIgnoreCase("notprinted"))
   {
     if (DivOrder.equalsIgnoreCase("byname"))
     {
         reporturl ="/CR_Reports/AGMInvitation_Letter/CDBL_ALL_NAME.rpt";
     }
     else if (DivOrder.equalsIgnoreCase("byfolio"))
     {
         reporturl ="";
         reporturl ="/CR_Reports/AGMInvitation_Letter/CDBL_ALL_BOID.rpt";
     }
   }
   else if (PRecords.equalsIgnoreCase("rangebyfolio"))
   {
     if (DivOrder.equalsIgnoreCase("byname"))
     {
         reporturl ="";
          reporturl ="/CR_Reports/AGMInvitation_Letter/CDBL_RANGE_NAME.rpt";
     }
     else if (DivOrder.equalsIgnoreCase("byfolio"))
     {
       //Add the report name
         reporturl ="";
        reporturl ="/CR_Reports/AGMInvitation_Letter/CDBL_RANGE_BOID.rpt";
     }
   }
  }


  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Printed AGM Invitation Letter')";
 try
{
  boolean ub = cm.procedureExecute(ulog);
}
catch(Exception e)
{
  test="1";
}

  if (!reporturl.equals("")) {
    //session.setAttribute("reportSource", null);
    session.setAttribute("reportSource", null);
    session.setAttribute("refreshed", null);
  }

Object reportSource = session.getAttribute("reportSource");
if (reportSource == null)
{
try{
  IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
  reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
  session.setAttribute("reportSource", reportSource);
}
catch (Exception e)
{
  test="2";
}
}
  CrystalReportViewer viewer= new CrystalReportViewer();
  try{
    viewer.setReportSource(reportSource);
  }catch (Exception e) {}
  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  //viewer.setPrintMode(CrPrintMode.PDF);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setReuseParameterValuesOnRefresh(true);
// Passing parameters

ParameterField param1 = new ParameterField();
param1.setReportName("");
param1.setName("ddate");

Values vals1 = new Values();
ParameterFieldDiscreteValue val1 = new ParameterFieldDiscreteValue();
val1.setValue(DateDec);
vals1.add(val1);

ParameterField param2 = new ParameterField();
param2.setReportName("");
param2.setName("lid");

Values vals2 = new Values();
ParameterFieldDiscreteValue val2 = new ParameterFieldDiscreteValue();
val2.setValue(letter_id);
vals2.add(val2);

ParameterField param3 = new ParameterField();
param3.setReportName("");
param3.setName("sigid");

Values vals3 = new Values();
ParameterFieldDiscreteValue val3 = new ParameterFieldDiscreteValue();
val3.setValue(sigid);
vals3.add(val3);

ParameterField param4 = new ParameterField();
param4.setReportName("");
param4.setName("pBOID");

Values vals4 = new Values();
ParameterFieldDiscreteValue val4 = new ParameterFieldDiscreteValue();
val4.setValue(ForBNo);
vals4.add(val4);

ParameterField param5 = new ParameterField();
param5.setReportName("");
param5.setName("sBOID");

Values vals5 = new Values();
ParameterFieldDiscreteValue val5 = new ParameterFieldDiscreteValue();
val5.setValue(FStart);
vals5.add(val5);

ParameterField param6 = new ParameterField();
param6.setReportName("");
param6.setName("eBOID");

Values vals6 = new Values();
ParameterFieldDiscreteValue val6 = new ParameterFieldDiscreteValue();
val6.setValue(FEnd);
vals6.add(val6);

try {
 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
 Date d = df.parse(DateDec);
 val1.setValue(d);
 vals1.add(val1);
} catch(Exception e)
{
  test="3";
e.printStackTrace();
}


param1.setCurrentValues(vals1);
param2.setCurrentValues(vals2);
param3.setCurrentValues(vals3);
param4.setCurrentValues(vals4);
param5.setCurrentValues(vals5);
param6.setCurrentValues(vals6);

Fields fields = new Fields();
fields.add(param1);
fields.add(param2);
fields.add(param3);
fields.add(param4);
fields.add(param5);
fields.add(param6);

viewer.setParameterFields(fields);

if (session.getAttribute("refreshed") == null)
{
viewer.refresh();
session.setAttribute("refreshed", "true");
}

//viewer.refresh();

try
{
viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);
}
catch(Exception e)
{
  //System.out.println(e.toString());
  test="4";
}

  if (isConnect)
  {
    try
    {
      cm.takeDown();
    }
    catch(Exception e)
    {
      //error
    }
  }
%>
</body>
</html>

