<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : March 2006
'Page Purpose  : Passes Parameters for Dividend Postlist Printing.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

 boolean isConnect = cm.connect();
 if(isConnect==false)
 {
   %>
    <jsp:forward page="ErrorMsg.jsp" >
    <jsp:param name="ErrorTitle" value="Connection Failure" />
    <jsp:param name="ErrorHeading" value="Connection Problem" />
    <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
    </jsp:forward>
  <%
 }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Printed Dividend Postlist')";
  boolean ub = cm.procedureExecute(ulog);

  if (isConnect)
  {
   cm.takeDown();
  }
%>

<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page
import="java.util.Date,java.text.*,com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
Dividend Postlist
</title>
</head>
<body bgcolor="#ffffff">
<h1>
</h1>
<%
  String DateDec = String.valueOf(request.getParameter("dateselect"));
  String DivType = String.valueOf(request.getParameter("dividendtype"));
  String DivOrder = String.valueOf(request.getParameter("orderby"));
  String Div_GT = String.valueOf(request.getParameter("divgr"));
  String Div_LT = String.valueOf(request.getParameter("divls"));


  String reporturl = "";

  if (DivType.equalsIgnoreCase("SMStype"))
  {
   if (DivOrder.equalsIgnoreCase("byname"))
   {
     reporturl = "/CR_Reports/FOLIO_Div_PostLists/FolioDivPostlist_Order_by_Name.rpt";
   }
   else if (DivOrder.equalsIgnoreCase("bydividend"))
   {
     reporturl = "/CR_Reports/FOLIO_Div_PostLists/FolioDivPostlist_Order_by_Dividend.rpt";
   }
   else if (DivOrder.equalsIgnoreCase("byfolio"))
   {
     reporturl = "/CR_Reports/FOLIO_Div_PostLists/FolioDivPostlist_Order_by_FolioNo.rpt";
   }
  }
  else if (DivType.equalsIgnoreCase("CDBLtype"))
  {
   if (DivOrder.equalsIgnoreCase("byname"))
   {
     reporturl = "/CR_Reports/BO_Div_PostLists/BO DivPostList_Order_By_Name.rpt";
   }
   else if (DivOrder.equalsIgnoreCase("bydividend"))
   {
     reporturl = "/CR_Reports/BO_Div_PostLists/BO DivPostList_Order_By_Dividend.rpt";
   }
   else if (DivOrder.equalsIgnoreCase("byfolio"))
   {
     reporturl = "/CR_Reports/BO_Div_PostLists/BO DivPostList_Order_By_BOID.rpt";
   }
  }

  if (!reporturl.equals("")) {
    session.setAttribute("reportSource", null);
    session.setAttribute("refreshed", null);
  }

Object reportSource = session.getAttribute("reportSource");
if (reportSource == null)
{
try{
  IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
  reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
  session.setAttribute("reportSource", reportSource);
}catch (Exception e) {}
}
  CrystalReportViewer viewer= new CrystalReportViewer();
  try{
    viewer.setReportSource(reportSource);
  }catch (Exception e) {}

  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setOwnForm(true);

// Passing parameters

ParameterField param1 = new ParameterField();
param1.setReportName(""); // must always be set, even if just to ""
param1.setName("DivGreaterThan_Param");

ParameterField param2 = new ParameterField();
param2.setReportName("");
param2.setName("DivLessThan_Param");

ParameterField param3 = new ParameterField();
param3.setReportName("");
param3.setName("DecDate_Param");

Values vals1 = new Values();
ParameterFieldDiscreteValue val1 = new ParameterFieldDiscreteValue();
val1.setValue(Div_GT);
vals1.add(val1);

Values vals2 = new Values();
ParameterFieldDiscreteValue val2 = new ParameterFieldDiscreteValue();
val2.setValue(Div_LT);
vals2.add(val2);

Values vals3 = new Values();
ParameterFieldDiscreteValue val3 = new ParameterFieldDiscreteValue();

try {
 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
 Date d = df.parse(DateDec);
 val3.setValue(d);
 vals3.add(val3);
} catch(Exception e) {
e.printStackTrace();
}

param1.setCurrentValues(vals1);
param2.setCurrentValues(vals2);
param3.setCurrentValues(vals3);

Fields fields = new Fields();
fields.add(param1);
fields.add(param2);
fields.add(param3);

viewer.setParameterFields(fields);

if (session.getAttribute("refreshed") == null)
{
viewer.refresh();
session.setAttribute("refreshed", "true");
}


try
{
viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);
}
catch(Exception e){System.out.println(e.getMessage());}
%>
</body>
</html>

