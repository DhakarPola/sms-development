
<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page
import="com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
Bonus Prelist
</title>
</head>
<body bgcolor="#ffffff">
<h1>
</h1>
<%
  String DivType = String.valueOf(request.getParameter("dividendtype"));
  String DivOrder = String.valueOf(request.getParameter("orderby"));
  String reporturl = "";

if (DivType.equalsIgnoreCase("SMStype")){
   if (DivOrder.equalsIgnoreCase("byname")) {
     reporturl = "/CR_Reports/FOLIO_Bonus_PreLists/Folio_BonusPreListOrderByName.rpt";
   } else if (DivOrder.equalsIgnoreCase("byfolio")){
     reporturl = "/CR_Reports/FOLIO_Bonus_PreLists/Folio_BonusPreListOrderByFolio.rpt";
   }
} else if (DivType.equalsIgnoreCase("CDBLtype")){
   if (DivOrder.equalsIgnoreCase("byname")) {
     reporturl = "/CR_Reports/BO_Bonus_PreLists/BO_BonusPreListOrderByName.rpt";
   } else if (DivOrder.equalsIgnoreCase("byfolio")){
     reporturl = "/CR_Reports/BO_Bonus_PreLists/BO_BonusPreListOrderByBOID.rpt";
   }
}

  if (!reporturl.equals("")) {
    session.setAttribute("reportSource", null);
    session.setAttribute("refreshed", null);
  }

Object reportSource = session.getAttribute("reportSource");
if (reportSource == null)
{
try{
  IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
  reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
  session.setAttribute("reportSource", reportSource);
}catch (Exception e) {}
}
  CrystalReportViewer viewer= new CrystalReportViewer();
  try{
    viewer.setReportSource(reportSource);
  }catch (Exception e) {}

  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setOwnForm(true);

if (session.getAttribute("refreshed") == null)
{
viewer.refresh();
session.setAttribute("refreshed", "true");
}

try
{
viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);
}
catch(Exception e){System.out.println(e.getMessage());}
%>
</body>
</html>
