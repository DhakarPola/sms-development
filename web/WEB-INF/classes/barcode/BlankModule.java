package barcode;
import barcode.output.AbstractOutput;
public class BlankModule extends Module {

        /**
         * Constructs a new BlankModule with the specified width.
         * @param width The width of the module in bar widths.
         */
        public BlankModule(int width) {
                super(new int[] {width});
        }

        /**
         * Draws the module to a barcode output.
         * @param output The output to draw to
         * @param x The starting X co-ordinate
         * @param y The starting Y co-ordinate
         * @return The total width drawn
         */
        protected double draw(AbstractOutput output, double x, double y) {
                output.setupForBlankDrawing();
                double sum = super.draw(output, x, y);
                output.teardownFromBlankDrawing();
                return sum;
        }

        /**
         * Returns the symbol that this module encodes.
         * @return A blank string
         */
        public String getSymbol() {
                return "";
        }
}
