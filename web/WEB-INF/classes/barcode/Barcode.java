package barcode;
import barcode.env.EnvironmentFactory;
import barcode.output.AbstractOutput;
import barcode.output.GraphicsOutput;
import barcode.output.SVGOutput;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

public abstract class Barcode extends JComponent {

        private static final String [] UNITS = {"in", "px", "cm", "mm"};
        private static final double DEFAULT_BAR_HEIGHT = 50;

        protected String data;
        protected boolean drawingText;
        protected double barWidth = 2;
        protected double barHeight;
        private Font font;
        private Dimension size;
        private int x;
        private int y;
        private int resolution = -1;
        private String mSVGUnits;
        private double mSVGScalar;

        protected Barcode(String data) throws BarcodeException {
                if (data == null || data.length() == 0) {
                        throw new BarcodeException("Data to encode cannot be empty");
                }
                this.data = data;
                double minHeight = calculateMinimumBarHeight(getResolution());
                if (minHeight > 0) {
                        this.barHeight = minHeight;
                } else {
                        this.barHeight = Barcode.DEFAULT_BAR_HEIGHT;
                }
                this.font = EnvironmentFactory.getEnvironment().getDefaultFont();
                this.drawingText = true;
                setBackground(Color.white);
                setForeground(Color.black);
                setOpaque(true);
                setSVGScalar(1.0 / 128, "in");
                invalidateSize();
        }

        /**
         * Returns the data that the barcode is coding for.
         * @return The barcode raw data
         */
        public String getData() {
                return data;
        }

        /**
         * Sets the font to use when drawing the barcode data string underneath the barcode.
         * <p/> Note that changing this setting after a barcode has been drawn will invalidate the
         * component and may force a refresh.
         * @param font The font to use
         */
        public void setFont(Font font) {
                this.font = font;
                invalidateSize();
        }

        /**
         * Indicates whether the barcode data should be shown as a string underneath the
         * barcode or not.
         * <p/> Note that changing this setting after a barcode has been drawn will invalidate the
         * component and may force a refresh.
         * @param drawingText True if the text should be shown, false if not
         */
        public void setDrawingText(boolean drawingText) {
                this.drawingText = drawingText;
                invalidateSize();
        }

        /**
         * Indicates whether the barcode is drawing a text label underneath the barcode or not.
         * @return True if the text is drawn, false otherwise
         */
        public boolean isDrawingText() {
                return drawingText;
        }

        /**
         * Sets the desired bar width for the barcode. This is the width (in pixels) of the
         * thinnest bar in the barcode. Other bars will change their size relative to this.
         * <p/> Note that changing this setting after a barcode has been drawn will invalidate the
         * component and may force a refresh.
         * @param barWidth The desired width of the thinnest bar in pixels
         */
        public void setBarWidth(double barWidth) {
                if (barWidth >= 1) {
                        this.barWidth = barWidth;
                } else {
                        this.barWidth = 1;
                }
                invalidateSize();
        }

        /**
         * Sets the desired height for the bars in the barcode (in pixels). Note that some
         * barcode implementations will not allow the height to go below a minimum size. This
         * is not the height of the component as a whole, as it does not specify the height of
         * any text that may be drawn and does not include borders.
         * <p/> Note that changing this setting after a barcode has been drawn will invalidate the
         * component and may force a refresh.
         * @param barHeight The desired height of the barcode bars in pixels
         */
        public void setBarHeight(double barHeight) {
                // There is a minimum bar height that we must enforce
                if (barHeight > calculateMinimumBarHeight(getResolution())) {
                        this.barHeight = barHeight;
                        invalidateSize();
                }
        }

        /**
         * Sets the desired output resolution for the barcode. This method should
         * be used in cases where the barcode is either being outputted to a device
         * other than the screen, or the barcode is being generated on a headless
         * machine (e.g. a rack mounted server) and the screen resolution cannot be
         * determined. Note that is the barcode is generated in either of these situations
         * and this method has not been called, the resolution is assumed to be 72 dots
         * per inch.
         * @param resolution The desired output resolution (in dots per inch)
         */
        public void setResolution(int resolution) {
                if (resolution > 0) {
                        this.resolution = resolution;
                        double newHeight = calculateMinimumBarHeight(getResolution());
                        if (newHeight > this.barHeight) {
                                this.barHeight = newHeight;
                        }
                        invalidateSize();
                }
        }

        /**
         * From {@link javax.swing.JComponent JComponent}.
         * @return The X co-ordinate of the component's origin
         */
        public int getX() {
                return x;
        }

        /**
         * From {@link javax.swing.JComponent JComponent}.
         * @return The Y co-ordinate of the component's origin
         */
        public int getY() {
                return y;
        }

        /**
         * From {@link javax.swing.JComponent JComponent}.
         * @return The width of this component
         */
        public int getWidth() {
                return (int) getActualSize().getWidth();
        }

        /**
         * From {@link javax.swing.JComponent JComponent}.
         * @return The height of this component
         */
        public int getHeight() {
                return (int) getActualSize().getHeight();
        }

        /**
         * From {@link javax.swing.JComponent JComponent}.
         * @return The bounds of this component
         */
        public Rectangle getBounds() {
                return getBounds(new Rectangle());
        }

        /**
         * From {@link javax.swing.JComponent JComponent}.
         * @param rv The rectangle to set the bounds on
         * @return The updated rv
         */
        public Rectangle getBounds(Rectangle rv) {
                rv.setBounds(getX(), getY(), (int) getActualSize().getWidth() + getX(),
                                         (int) getActualSize().getHeight() + getY());
                return rv;
        }

        /**
         * From {@link javax.swing.JComponent JComponent}.
         * @return The preferred size of this component
         */
        public Dimension getPreferredSize() {
                return getActualSize();
        }

        /**
         * From {@link javax.swing.JComponent JComponent}.
         * @return The minimum size of this component
         */
        public Dimension getMinimumSize() {
                return getActualSize();
        }

        /**
         * From {@link javax.swing.JComponent JComponent}.
         * @return The maximum size of this component
         */
        public Dimension getMaximumSize() {
                return getActualSize();
        }

        /**
         * From {@link javax.swing.JComponent JComponent}.
         * @return The actual size of this component
         */
        public Dimension getSize() {
                return getActualSize();
        }

        /**
         * Renders this <code>Barcode</code> at the specified location in
         * the specified {@link java.awt.Graphics2D Graphics2D} context.
         * The origin of the layout is placed at x,&nbsp;y.  Rendering may touch
         * any point within <code>getBounds()</code> of this position.  This
         * leaves the <code>g2</code> unchanged.
         *
         * @param g The graphics context
         * @param x The horizontal value of the upper left co-ordinate of the bounding box
         * @param y The vertical value of the upper left co-ordinate of the bounding box
         */
        public void draw(Graphics2D g, int x, int y) {
                this.x = x;
                this.y = y;
                size = internalDraw(g, x, y, true);
        }

        protected abstract double getBarcodeWidth(int resolution);
        protected abstract Module[] encodeData();
        protected abstract Module calculateChecksum();
        protected abstract Module getPreAmble();
        protected abstract Module getPostAmble();

        /**
         * Returns the text that will be displayed underneath the barcode (if requested).
         * @return The text label for the barcode
         */
        protected String getLabel() {
                return data;
        }

        protected double calculateMinimumBarHeight(int resolution) {
                return 0;
        }

        /**
         * From {@link javax.swing.JComponent JComponent}.
         * @param g The graphics to paint the component onto
         */
        protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                Insets insets = getInsets();
                draw((Graphics2D) g, insets.left, insets.top);
        }

        protected int getResolution() {
                if (resolution > 0) {
                        return resolution;
                }
                return EnvironmentFactory.getEnvironment().getResolution();
        }

        private Dimension internalDraw(Graphics2D g, int x, int y, boolean paint) {
                Color c = g.getColor();
                double currentX = x;
                AbstractOutput drawingEnv = new GraphicsOutput(g, font, barWidth, barHeight,
                                                                                                           paint, getForeground(), getBackground());

                if (paint) {
                        // don't like this, as it is recursive (currently)
                        Dimension dim = getActualSize();
                        drawingEnv.beginDraw(dim.width, dim.height);
                }

                currentX += drawModule(getPreAmble(), drawingEnv, currentX, y);
                Module[] modules = encodeData();
                for (int i = 0; i < modules.length; i++) {
                        Module module = modules[i];
                        currentX += drawModule(module, drawingEnv, currentX, y);
                }

                currentX += drawModule(calculateChecksum(), drawingEnv, currentX, y);
                currentX += drawModule(getPostAmble(), drawingEnv, currentX, y);
                double currentY = barHeight + y;

                if (drawingText) {
                        currentY += drawTextLabel(drawingEnv, x, (int) currentY, currentX);
                }

                if (paint) {
                        drawingEnv.endDraw();
                }

                g.setColor(c);
                return new Dimension((int) (currentX - x), (int) (currentY) - y);
        }

        private void invalidateSize() {
                size = null;
        }

        private Dimension getActualSize() {
                if (size == null) {
                        size = calculateSize();
                }
                return size;
        }

        private Dimension calculateSize() {
                return internalDraw((Graphics2D) new BufferedImage(1000, 1000,
                                                                                         BufferedImage.TYPE_BYTE_GRAY).getGraphics(), 0, 0, false);
        }

        protected double drawModule(Module module, AbstractOutput params, double x, double y) {
                if (module == null) {
                        return 0;
                }
                return module.draw(params, x, y);
        }

        protected String beautify(String s) {
                StringBuffer buf = new StringBuffer();
                StringCharacterIterator iter = new StringCharacterIterator(s);
                for (char c = iter.first(); c != CharacterIterator.DONE; c = iter.next()) {
                        if (Character.isDefined(c) && !Character.isISOControl(c)) {
                                buf.append(c);
                        }
                }
                return buf.toString();
        }


        public String getSVG() {
                java.io.StringWriter writer = new java.io.StringWriter();
                outputSVG(writer);
                return writer.toString();
        }

        public void outputSVG(java.io.Writer out) {
                internalOutputSVG(out, 0, 0);
        }

        public void setSVGScalar(double scalar, String units) {
                String options = "";
                boolean found = false;

                for (int ii = 0; !found && ii < UNITS.length; ii++) {
                        if (units.equals(UNITS[ii])) {
                                found = true;
                        }
                        if (ii != 0) {
                                options += ", ";
                        }
                        options += UNITS[ii];
                }

                if (!found) {
                        throw new IllegalArgumentException("SVG Units must be one of " + options);
                }

                mSVGScalar = scalar;
                mSVGUnits = units;
        }

        // this needs to fold into internalDraw() above, once i get all the specific stuff out of the way.
        private Dimension internalOutputSVG(java.io.Writer out, int x, int y) {
                double currentX = x;
                double originalBarWidth = barWidth;
                boolean wasDrawingText = drawingText;
                AbstractOutput params;

                drawingText = false; // FIX
                // restore to single pixels, which we multiply by our "units"
                setBarWidth(1.0);

                params = new SVGOutput(out, font, barHeight,
                                                           getForeground(), getBackground(), mSVGScalar, mSVGUnits);

                Dimension dim = getActualSize(); //NB: Currently calling internalDraw(), which isn't really
                //  what we need.  need to unify our dimensions with the dpi system used herein.

                // write our opening tag...
                params.beginDraw(dim.width, dim.height);
                currentX += drawModule(getPreAmble(), params, currentX, y);
                Module[] modules = encodeData();
                for (int i = 0; i < modules.length; i++) {
                        Module module = modules[i];
                        currentX += drawModule(module, params, currentX, y);
                }

                currentX += drawModule(calculateChecksum(), params, currentX, y);
                currentX += drawModule(getPostAmble(), params, currentX, y);
                double currentY = barHeight + y;

                if (drawingText) {
                        currentY += drawTextLabel(params, x, (int) currentY, currentX);
                }

                // now our closing tag..
                params.endDraw();

                // restore the bar width
                setBarWidth(originalBarWidth);
                drawingText = wasDrawingText;

                return new Dimension((int) (currentX - x), (int) (currentY) - y);
        }

        private double drawTextLabel(AbstractOutput params, double x, double y, double width) {
                String s = beautify(getLabel());
                return params.drawText(s, x, y, width);
        }
}
