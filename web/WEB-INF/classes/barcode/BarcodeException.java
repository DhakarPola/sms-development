package barcode;

public class BarcodeException extends Exception {

        /**
         * Constructs a new barcode exception with the specified error message.
         * @param msg The error message
         */
        public BarcodeException(String msg) {
                super(msg);
        }
}
