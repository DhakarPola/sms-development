package barcode.linear.codebar;

import barcode.Module;

import java.util.Map;
import java.util.HashMap;

/**
 * Codabar barcode module definitions.
 *
 * @author <a href="mailto:ian@thoughtworks.net">Ian Bourke</a>
 */
final class ModuleFactory {
	private static final Map SET = new HashMap();

	static {
		init();
	}

	///CLOVER:OFF
	/**
	 * No public access.
	 */
	private ModuleFactory() {
	}
	///CLOVER:ON

	/**
	 * Initialise the module definitions.
	 */
	private static void init() {
		SET.put("0", new Module(new int[] {1, 1, 1, 1, 1, 2, 2}));
		SET.put("1", new Module(new int[] {1, 1, 1, 1, 2, 2, 1}));
		SET.put("2", new Module(new int[] {1, 1, 1, 2, 1, 1, 2}));
		SET.put("3", new Module(new int[] {2, 2, 1, 1, 1, 1, 1}));
		SET.put("4", new Module(new int[] {1, 1, 2, 1, 1, 2, 1}));
		SET.put("5", new Module(new int[] {2, 1, 1, 1, 1, 2, 1}));
		SET.put("6", new Module(new int[] {1, 2, 1, 1, 1, 1, 2}));
		SET.put("7", new Module(new int[] {1, 2, 1, 1, 2, 1, 1}));
		SET.put("8", new Module(new int[] {1, 2, 2, 1, 1, 1, 1}));
		SET.put("9", new Module(new int[] {2, 1, 1, 2, 1, 1, 1}));
		SET.put("-", new Module(new int[] {1, 1, 1, 2, 2, 1, 1}));
		SET.put("$", new Module(new int[] {1, 1, 2, 2, 1, 1, 1}));
		SET.put(":", new Module(new int[] {2, 1, 1, 1, 2, 1, 2}));
		SET.put("/", new Module(new int[] {2, 1, 2, 1, 1, 1, 2}));
		SET.put(".", new Module(new int[] {2, 1, 2, 1, 2, 1, 1}));
		SET.put("+", new Module(new int[] {1, 1, 2, 2, 2, 2, 2}));
		SET.put("A", new Module(new int[] {1, 1, 2, 2, 1, 2, 1}));
		SET.put("B", new Module(new int[] {1, 1, 1, 2, 1, 2, 2}));
		SET.put("C", new Module(new int[] {1, 2, 1, 2, 1, 1, 2}));
		SET.put("D", new Module(new int[] {1, 1, 1, 2, 2, 2, 1}));
	}

	/**
	 * Returns the module that represents the specified character.
	 * @param key The data character to get the encoding module for
	 * @return The module that encodes the given char
	 */
	public static Module getModule(String key) {
		Module module = null;
		module = (Module) SET.get(key);
		module.setSymbol(key);
		return module;
	}

	/**
	 * Indicates whether the given character is valid for this barcode or not.
	 * This basically just checks to see whether the key is in the list of
	 * encoded characters.
	 * @param key The key to check for validity
	 * @return True if the key is valid, false otherwise
	 */
	public static boolean isValid(String key) {
		return SET.containsKey(key);
	}
}
