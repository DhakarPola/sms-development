package barcode.linear.code128;

import barcode.BarcodeException;
import barcode.CompositeModule;
import barcode.Module;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

/**
 * An implementation of the UCC 128 and EAN 128 code formats. These are almost identical
 * to the vanilla Code 128 format, but they are encoded in character set C and include the
 * FNC1 character at the start. In addition, an Application Identifier must be provided
 * that identifies the application domain of the barcode. Please see the convenienve methods
 * on BarcodeFactory that provide application domain specific instances of this barcode type.
 *
 * @author <a href="mailto:ian@thoughtworks.net">Ian Bourke</a>
 */
public class UCCEAN128Barcode extends Code128Barcode {
	/**
	 * SSCC-18 application identifier.
	 */
	public static final String SSCC_18_AI = "00";
	/**
	 * SCC-14 shipping code application identifier.
	 */
	public static final String SCC_14_AI = "01";
	/**
	 * Global Trade Item Number application identifier.
	 */
	public static final String GTIN_AI = SCC_14_AI;
	/**
	 * EAN 128 application identifier for all EAN 128 formats.
	 */
	public static final String EAN128_AI = "01";
	/**
	 * Shipment Identification Number application identifier.
	 */
	public static final String SHIPMENT_ID_AI = "402";
	/**
	 * US Postal service application identifier for all USPS formats.
	 */
	public static final String USPS_AI = "420";

    private final String applicationIdentifier;
	private String originalData;
	private final boolean includeCheckDigit;

	/**
	 * Creates a new UCC/EAN 128 barcode with the given application identifier and
	 * data to encode. The AI will be prepended to the data (which also has a mod 10
	 * check digit appended) before encoding, and will appear in parentheses in the
	 * printed label underneath the barcode. A mod 10 check digit will be generated.
	 * @param applicationIdentifier The application identifier for this barcode
	 * @param data The data to encode
	 * @throws BarcodeException If the data to be encoded is invalid
	 */
	public UCCEAN128Barcode(String applicationIdentifier, String data) throws BarcodeException {
		this(applicationIdentifier, data, true);
	}

	/**
	 * Creates a new UCC/EAN 128 barcode with the given application identifier and
	 * data to encode. The AI will be prepended to the data (which also has a mod 10
	 * check digit appended) before encoding, and will appear in parentheses in the
	 * printed label underneath the barcode.
	 * @param applicationIdentifier The application identifier for this barcode
	 * @param data The data to encode
	 * @param includeCheckDigit specifies whether a mod 10 check digit should be generated or not
	 * @throws BarcodeException If the data to be encoded is invalid
	 */
	public UCCEAN128Barcode(String applicationIdentifier, String data, boolean includeCheckDigit) throws BarcodeException {
		super(FNC_1 + applicationIdentifier + data + getMod10CheckDigit(data, includeCheckDigit), C);
		if (applicationIdentifier == null || applicationIdentifier.length() == 0) {
			throw new IllegalArgumentException("Application Identifier must be provided");
		}
		this.applicationIdentifier = applicationIdentifier;
		this.originalData = data;
		this.includeCheckDigit = includeCheckDigit;
	}

	/**
	 * Returns the pre-amble for this barcode type. This is basically
	 * a specific instance of the Code 128 barcode that always uses
	 * a C start char and FNC1 char in succession.
	 * @return The pre-amble module
	 */
	protected Module getPreAmble() {
		CompositeModule module = new CompositeModule();
		module.add(QUIET_SECTION);
		module.add(START_C);
		return module;
	}

	/**
	 * Returns the text to be displayed underneath the barcode.
	 * @return The text that the barcode represents
	 */
	protected String getLabel() {
		return '(' + applicationIdentifier + ") " + originalData + getMod10CheckDigit(originalData, includeCheckDigit);
	}

	/**
	 * Generates a mod 10 check digit for the barcode data (ignoring the app id).
	 * @param data The data to generate the check digit for
	 * @param calculate Whether the check digit should actually be calculated or not
	 * @return The check digit (or "" if not calculated)
	 */
	static String getMod10CheckDigit(String data, boolean calculate) {
		if (!calculate) {
			return "";
		}

		Accumulator sum = new Accumulator(START_INDICES[C]);
		Accumulator index = new Accumulator(1);
		CharBuffer buf = new CharBuffer(BUF_SIZES[C]);
		StringCharacterIterator iter = new StringCharacterIterator(data);
		for (char c = iter.first(); c != CharacterIterator.DONE; c = iter.next()) {
			buf.addChar(c);
			if (buf.isFull()) {
				int code = ModuleFactory.getIndex(buf.toString(), C);
				sum.add(code * index.getValue());
				index.increment();
				buf.clear();
			}
		}
		return String.valueOf(sum.getValue() % 10);
	}
}
