package barcode.linear.code128;

final class Accumulator {
	private int value;

	Accumulator(int startingValue) {
		this.value = startingValue;
	}

	int getValue() {
		return value;
	}

	void add(int value) {
		this.value += value;
	}

	void increment() {
		value += 1;
	}
}
