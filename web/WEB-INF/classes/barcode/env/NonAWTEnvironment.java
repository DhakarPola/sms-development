package barcode.env;
import java.awt.*;
public class NonAWTEnvironment implements Environment {
        private final int resolution;

    /**
     * Constructs the environment with the mandatory output resolution.
     * The resolution must be specified because this environment cannot
     * look it up anywhere.
     * @param resolution The output resolution
     */
        public NonAWTEnvironment(int resolution) {
                this.resolution = resolution;
        }

    /**
         * Returns the specified resolution for
         * outputting barcodes.
         * @return The resolution for the environment
         */
        public int getResolution() {
                return resolution;
        }

    /**
     * Returns the default font for the environment.
     * @return null
     */
        public Font getDefaultFont() {
                return null;
        }
}
