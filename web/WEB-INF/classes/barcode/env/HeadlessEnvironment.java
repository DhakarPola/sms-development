package barcode.env;
import java.awt.*;
public final class HeadlessEnvironment implements Environment {
        /**
         * The default output resolution (in DPI) for barcodes in headless mode.
         */
        public static final int DEFAULT_RESOLUTION = 72;

        /**
         * Returns the environment determined resolution for
         * outputting barcodes.
         * @return The resolution for the environment
         */
        public int getResolution() {
                return DEFAULT_RESOLUTION;
        }

    /**
     * Returns the default font for the environment.
     * @return null
     */
    public Font getDefaultFont() {
        return null;
    }
}
