package barcode;
import barcode.twod.pdf417.PDF417Barcode;
public final class BarcodeFactory {

        /**
         * You can't construct one of these.
         */
        private BarcodeFactory() {
        }
        /**
         * Creates a PDF417 two dimensional barcode.
         * @param data The data to encode
         * @return The barcode
         * @throws BarcodeException If the data to be encoded is invalid
         */
        public static Barcode createPDF417(String data) throws BarcodeException {
                return new PDF417Barcode(data);
        }

}
