/*
'******************************************************************************************************************************************
'Author : Fahmi Sarker
'Creation Date : January 2005
'Purpose  : Provides Utility functions
'******************************************************************************************************************************************
*/
package batbsms;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utility
{
    conBean con;

    public Utility()
    {
        con = new conBean();
    }

    /**
     * Changes a date from a user- specified source format to a user- specified
     * destination format
     *
     * @param date String
     * @param sourceformat String
     * @param destinationformat String
     * @return String
     */
    public String changeDateFormat(String date, String sourceformat, String destinationformat)
    {
        String sdate = new String(date)  ;
        SimpleDateFormat sim = new SimpleDateFormat(sourceformat);

        Date datum = new Date();

         try
         {
             datum = sim.parse(sdate);
         }
         catch(ParseException e)
         {
             System.out.println("Invalid Date Parser Exception (Possible: Invalid Source Date Format)");
         }
         SimpleDateFormat dmj= new SimpleDateFormat(destinationformat);
         String thisDate = dmj.format(datum);

         return thisDate.toString();

    }


    /**
     * Changes a string to "" if it is null
     *
     * @param str String
     * @return String
     */
    public String changeIfNull(String str)
    {
        if(str == null || str.equals("null"))
            return "";
        else
            return str;

    }


    /**
    * Changes a string to "" if it is null and trims leading and trailing
    * whitespaces.
    *
    * @param str String
    * @return String
    */
   public String changeIfNullnTrim(String str)
   {
       if(str == null || str.equals("null"))
           return "";
       else
           return str.trim();

   }


    /**
     * checks if a value already exists in database
     *
     * @param tablename String
     * @param fieldname String
     * @param value String
     */
    public boolean alreadyExistsInDB(String tablename,String fieldname, String value)
    {
              String query = "SELECT * FROM " + tablename + " WHERE " + fieldname + " = '" + value +"'";
              value = value.trim();

              int no_of_rows=0;
              boolean isConnect=false;
              try
              {
                  isConnect = con.connect();
                  no_of_rows = con.queryExecuteCount(query);
               }
               catch(Exception ex)
               {
                   System.out.println("Could not connect : "+ ex.getMessage() );
               }
               finally
               {
                   if (isConnect)
                   {   try{
                           con.takeDown();
                       }
                       catch(Exception ex){;}
                   }
               }
               if(no_of_rows>0)
                   return true;
               else
                return false;
    }

    /**
     * Used only for debugging: Outputs two strings surrounding with "'" in
     * order to compare the two.
     *
     * @param str1 String
     * @param str2 String
     */
    public void outputStrings(String str1, String str2)
    {
        System.out.println("'"+str1+"' : '"+str2+"'");
    }
}
