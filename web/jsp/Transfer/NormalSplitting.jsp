<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : October 2005
'Page Purpose  : Adds a new Simple Splitting Information.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Simple Share Splitting</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function formconfirm()
{
  if (confirm("Do you want to do the mentioned Simple Splitting?"))
  {
    return true;
  }
}

function addprimaryshares(checkvalue1,checkvalue2,checkvalue3)
{
  var checkme1;
  var checkme2;
  var ptotal;
  checkme1 = document.all[checkvalue1].value;
  checkme2 = document.all[checkvalue2].value;

  if((checkme1.length > 0)&&(checkme2.length > 0))
  {
    if ((checkme2 - checkme1) > -1)
    {
      ptotal = checkme2 - checkme1 + 1;
      document.all[checkvalue3].value = ptotal;
    }
  }
}

function setbarcode()
{
  var val1 = document.all["certificateno"].value;

  if (val1.length > 0)
  {
   location = "<%=request.getContextPath()%>/jsp/Transfer/PutBarcode.jsp?pgname=normalsplitting&certno=" + val1;
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('sfolio','- Folio (Option must be entered)');
  BlankValidate('certificateno','- Certificate No. (Option must be entered)');

  var val17;
  var val18;
  var val19;

  val19 = 0;

  val17  = trim(window.document.all["distinctionstart"].value);
  val18  = trim(window.document.all["distinctionend"].value);

  if ((val17 == "") || (val18 == ""))
  {
    count = count + 1;
    nArray[count] = '- Distinctions (Option must be entered)';
    val19++;
  }

  if (val19 == 0)
  {
    BlankValidate('tnofshares','- Distinctions are Incorrect');
  }

  BlankValidate('proposedunits','- Proposed Unit (Option must be entered)');

  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

  String dist1from = String.valueOf(request.getParameter("dist1from"));
  String dist1to = String.valueOf(request.getParameter("dist1to"));
  String HolderFolio = String.valueOf(request.getParameter("hfolio"));
  String Certificate1No = String.valueOf(request.getParameter("cert1no"));

  int tnofshares = 0;
  String Stnofshares = "";

  if (String.valueOf(dist1from).equals("null"))
    dist1from = "";
  if (String.valueOf(dist1to).equals("null"))
    dist1to = "";
  if (String.valueOf(HolderFolio).equals("null"))
    HolderFolio = "";
  if (String.valueOf(Certificate1No).equals("null"))
    Certificate1No = "";

  if (dist1from.length() > 0)
  {
    tnofshares = Integer.parseInt(dist1to) - Integer.parseInt(dist1from) + 1;
  }

  if (tnofshares == 0)
  {
   Stnofshares = "";
  }
  else
  {
   Stnofshares = String.valueOf(tnofshares);
  }

%>

<form action="SaveNormalSplitting.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Simple Share Splitting</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Folio</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="sfolio" class="SL2TextField" value="<%=HolderFolio%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Certificate No.</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="certificateno" class="SL2TextField" onkeypress="keypressOnNumberFld()" value="<%=Certificate1No%>">
          <br>
          <img name="B3" src="<%=request.getContextPath()%>/images/btnSearch.gif"  onclick="setbarcode()" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnSearchR.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnSearch.gif'">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Distinctions</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left">
         <table width="75%"  border="0" cellpadding="2">
           <tr>
            <td width="30%" align="left"><div align="left" class="style8">
              <b>From </b></div>
            </td>
            <td width="70%" align="left"><div align="left" class="style8">
              <input type="text" name="distinctionstart" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distinctionstart','distinctionend','tnofshares')" value="<%=dist1from%>"></div>
            </td>
           </tr>
           <tr>
            <td width="30%" align="left"><div align="left" class="style8">
              <b>To </b></div>
            </td>
            <td width="70%" align="left"><div align="left" class="style8">
              <input type="text" name="distinctionend" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distinctionstart','distinctionend','tnofshares')" value="<%=dist1to%>"></div>
            </td>
           </tr>
        </table>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Total Shares</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="tnofshares" class="SL2TextField" onkeypress="keypressOnNumberFld()" onfocus="this.blur()" value="<%=Stnofshares%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;
          Split&nbsp;
          </div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left" class="style8">
            <input type="text" name="proposedunits" class="SL2TextField" onkeypress="keypressOnNumberFld()">
            &nbsp;<b>(Proposed Unit)</b>
            </div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="10" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <td width="50%" align="right">
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
      </td>
        <td width="50%">
	          <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
      </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
