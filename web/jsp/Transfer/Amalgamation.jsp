<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : October 2005
'Page Purpose  : Adds a new Amalgamation Information.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Amalgamation</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function formconfirm()
{
  if (confirm("Do you want to do the mentioned Amalgamation?"))
  {
    return true;
  }
}

function addprimaryshares(checkvalue1,checkvalue2,checkvalue3)
{
  var checkme1;
  var checkme2;
  var ptotal;
  checkme1 = document.all[checkvalue1].value;
  checkme2 = document.all[checkvalue2].value;

  if((checkme1.length > 0)&&(checkme2.length > 0))
  {
    if ((checkme2 - checkme1) > -1)
    {
      ptotal = checkme2 - checkme1 + 1;
      document.all[checkvalue3].value = ptotal;
      addshares(checkvalue3);
    }
  }
}

function addshares(checkvalue)
{
  var checkme;
  checkme = document.all[checkvalue].value;

  if(checkme.length <= 0)
  {
    document.all[checkvalue].value='';
  }

  var quantity1;
  var quantity2;
  var quantity3;
  var quantity4;
  var Totalquantitycal;

  quantity1 = replaceChars(document.all["nofshares1"].value,",","");
  quantity2 = replaceChars(document.all["nofshares2"].value,",","");
  quantity3 = replaceChars(document.all["nofshares3"].value,",","");
  quantity4 = replaceChars(document.all["nofshares4"].value,",","");

  Totalquantitycal = 0;

  if(quantity1 > 0)
   Totalquantitycal = Number(Totalquantitycal) + Number(quantity1);
  if(quantity2 > 0)
   Totalquantitycal = Number(Totalquantitycal) + Number(quantity2);
  if(quantity3 > 0)
   Totalquantitycal = Number(Totalquantitycal) + Number(quantity3);
  if(quantity4 > 0)
   Totalquantitycal = Number(Totalquantitycal) + Number(quantity4);

  document.all["tnofshares"].value = Totalquantitycal;
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('sfolio','- Folio (Option must be entered)');

  val1  = trim(window.document.all["distfrom1"].value);
  val2  = trim(window.document.all["distto1"].value);
  val3  = trim(window.document.all["nofshares1"].value);
  val4  = trim(window.document.all["distfrom2"].value);
  val5  = trim(window.document.all["distto2"].value);
  val6  = trim(window.document.all["nofshares2"].value);
  val7  = trim(window.document.all["distfrom3"].value);
  val8  = trim(window.document.all["distto3"].value);
  val9  = trim(window.document.all["nofshares3"].value);
  val10 = trim(window.document.all["distfrom4"].value);
  val11 = trim(window.document.all["distto4"].value);
  val12 = trim(window.document.all["nofshares4"].value);

  val13 = trim(window.document.all["certificateno1"].value);
  val14 = trim(window.document.all["certificateno2"].value);
  val15 = trim(window.document.all["certificateno3"].value);
  val16 = trim(window.document.all["certificateno4"].value);

  var rowcounter;
  rowcounter = 0;

  if (val3 != "")
  {
    rowcounter++;
  }
  if (val6 != "")
  {
    rowcounter++;
  }
  if (val9 != "")
  {
    rowcounter++;
  }
  if (val12 != "")
  {
    rowcounter++;
  }

  if ((val1 == "") && (val2 == "") && (val3 == "") && (val4 == "") && (val5 == "") && (val6 == "") && (val7 == "") && (val8 == "") && (val9 == "") && (val10 == "") && (val11 == "") && (val12 == "") && (val13 == "") && (val14 == "") && (val15 == "") && (val16 == ""))
   {
     count = count + 1;
     nArray[count] = '- At least one Certificate must be entered';
     rowcounter = 0;
   }
  else
  {
    if ((val1 != "") || (val2 != "") || (val3 != "") || (val13 != ""))
    {
      if ((val1 == "") || (val2 == "") || (val3 == "") || (val13 == ""))
      {
       count = count + 1;
       nArray[count] = '- Please Compelete Certificate 1';
       rowcounter = 0;
      }
    }
    if ((val4 != "") || (val5 != "") || (val6 != "") || (val14 != ""))
    {
      if ((val4 == "") || (val5 == "") || (val6 == "") || (val14 == ""))
      {
       count = count + 1;
       nArray[count] = '- Please Compelete Certificate 2';
       rowcounter = 0;
      }
    }
    if ((val7 != "") || (val8 != "") || (val9 != "") || (val15 != ""))
    {
      if ((val7 == "") || (val8 == "") || (val9 == "") || (val15 == ""))
      {
       count = count + 1;
       nArray[count] = '- Please Compelete Certificate 3';
       rowcounter = 0;
      }
    }
    if ((val10 != "") || (val11 != "") || (val12 != "") || (val16 != ""))
    {
      if ((val10 == "") || (val11 == "") || (val12 == "") || (val16 == ""))
      {
       count = count + 1;
       nArray[count] = '- Please Compelete Certificate 4';
       rowcounter = 0;
      }
    }
  }

  if (rowcounter < 2)
  {
     count = count + 1;
     nArray[count] = '- There should be 2 or more Distinctions';
  }

  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
%>

<form action="SaveAmalgamation.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Amalgamation</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Folio</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="sfolio" class="SL2TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Amalgamate</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left">

         <table width="100%"  border="0" cellpadding="2">
           <tr>
            <td width="25%" align="center"><div align="center" class="style8"><b>Certificate No.</b></div></th>
            <td width="25%" align="center"><div align="center" class="style8"><b>Dist. From</b></div></th>
            <td width="25%" align="center"><div align="center" class="style8"><b>Dist. To</b></div></td>
            <td width="25%" align="center"><div align="center" class="style8"><b>No. of Shares</b></div></td>
           </tr>
           <tr>
            <td width="25%" align="center"><div align="center" class="style8"><input type="text" name="certificateno1" class="SL67TextField" onkeypress="keypressOnNumberFld()"></div></td>
            <td width="25%" align="center"><div align="center" class="style8"><input type="text" name="distfrom1" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom1','distto1','nofshares1')"></div></td>
            <td width="25%" align="center"><div align="center" class="style8"><input type="text" name="distto1" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom1','distto1','nofshares1')"></div></td>
            <td width="25%" align="center"><div align="center" class="style8"><input type="text" name="nofshares1" class="SL67TextField"  onkeypress="keypressOnNumberFld()" onfocus="this.blur()"></div></td>
           </tr>
           <tr>
            <td width="25%" align="center"><div align="center" class="style8"><input type="text" name="certificateno2" class="SL67TextField" onkeypress="keypressOnNumberFld()"></div></td>
            <td width="25%" align="center"><div align="center" class="style8"><input type="text" name="distfrom2" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom2','distto2','nofshares2')"></div></td>
            <td width="25%" align="center"><div align="center" class="style8"><input type="text" name="distto2" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom2','distto2','nofshares2')"></div></td>
            <td width="25%" align="center"><div align="center" class="style8"><input type="text" name="nofshares2" class="SL67TextField"  onkeypress="keypressOnNumberFld()" onfocus="this.blur()"></div></td>
           </tr>
           <tr>
            <td width="25%" align="center"><div align="center" class="style8"><input type="text" name="certificateno3" class="SL67TextField" onkeypress="keypressOnNumberFld()"></div></td>
            <td width="25%" align="center"><div align="center" class="style8"><input type="text" name="distfrom3" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom3','distto3','nofshares3')"></div></td>
            <td width="25%" align="center"><div align="center" class="style8"><input type="text" name="distto3" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom3','distto3','nofshares3')"></div></td>
            <td width="25%" align="center"><div align="center" class="style8"><input type="text" name="nofshares3" class="SL67TextField"  onkeypress="keypressOnNumberFld()" onfocus="this.blur()"></div></td>
           </tr>
           <tr>
            <td width="25%" align="center"><div align="center" class="style8"><input type="text" name="certificateno4" class="SL67TextField" onkeypress="keypressOnNumberFld()"></div></td>
            <td width="25%" align="center"><div align="center" class="style8"><input type="text" name="distfrom4" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom4','distto4','nofshares4')"></div></td>
            <td width="25%" align="center"><div align="center" class="style8"><input type="text" name="distto4" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom4','distto4','nofshares4')"></div></td>
            <td width="25%" align="center"><div align="center" class="style8"><input type="text" name="nofshares4" class="SL67TextField"  onkeypress="keypressOnNumberFld()" onfocus="this.blur()"></div></td>
           </tr>
        </table>

        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Total Shares</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="tnofshares" class="SL2TextField" onfocus="this.blur()">
        </div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="10" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <td width="50%" align="right">
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
      </td>
        <td width="50%">
	          <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
      </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
