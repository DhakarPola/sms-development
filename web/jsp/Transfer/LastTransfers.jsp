<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : November 2005
'Page Purpose  : View showing the last Accepted Transfers.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<%@ page language="java" import="java.sql.*,java.util.Vector,java.util.Date,java.text.SimpleDateFormat,java.math.*" %>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
.style19
{
	color: #0044B0;
	font-size: 16px;
	font-weight:bold;
}
.style20
{
	color: #0044B0;
	font-size: 11px;
	font-weight:bold;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Last Accepted Transfer</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    document.all.dprint1.style.display = ''
    window.print();
    document.all.dprint.style.display = '';
    document.all.dprint1.style.display = 'none';
  }
 }
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style13 {font-size: 11px}
.style15 {
	font-size: 16px;
	font-weight: bold;
}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
     Date d = new Date();
     int thisyear = d.getYear();
     thisyear = thisyear + 1900;
     int thismonth = d.getMonth();
     thismonth = thismonth + 1;
     int thisday = d.getDate();
     int thisday1 = d.getDay();
     int thishour = d.getHours();
     String Sthishour = "";
     int thisminute = d.getMinutes();
     String Sthisminute = "";
     String ampm = "";
     if (thisminute > 9)
     {
       Sthisminute = String.valueOf(thisminute);
     }
     else
     {
       Sthisminute = "0" + String.valueOf(thisminute);
     }
     if (thishour > 11)
     {
       ampm = "PM";
       if (thishour > 12)
       {
        thishour = thishour - 12;
        Sthishour = "0" + String.valueOf(thishour);
       }
       else
       {
        Sthishour = String.valueOf(thishour);
       }
     }
     else
     {
       ampm = "AM";
       if (thishour < 10)
       {
         if (thishour == 0)
         {
           Sthishour = "12";
         }
         else
         {
           Sthishour = "0" + String.valueOf(thishour);
         }
       }
       else
       {
         Sthishour = String.valueOf(thishour);
       }
     }
     String wday = "";
     String thismonth1 = "";
     if (thisday1 == 1)
       wday = "Monday";
     else if (thisday1 == 2)
       wday = "Tuesday";
     else if (thisday1 == 3)
       wday = "Wednesday";
     else if (thisday1 == 4)
       wday = "Thursday";
     else if (thisday1 == 5)
       wday = "Friday";
     else if (thisday1 == 6)
       wday = "Saturday";
     else if (thisday1 == 0)
       wday = "Sunday";

     if (thismonth == 1)
       thismonth1 = "January";
     else if (thismonth == 2)
       thismonth1 = "February";
     else if (thismonth == 3)
       thismonth1 = "March";
     else if (thismonth == 4)
       thismonth1 = "April";
     else if (thismonth == 5)
       thismonth1 = "May";
     else if (thismonth == 6)
       thismonth1 = "June";
     else if (thismonth == 7)
       thismonth1 = "July";
     else if (thismonth == 8)
       thismonth1 = "August";
     else if (thismonth == 9)
       thismonth1 = "September";
     else if (thismonth == 10)
       thismonth1 = "October";
     else if (thismonth == 11)
       thismonth1 = "November";
     else if (thismonth == 12)
       thismonth1 = "December";

     String formdate = wday + ", " + thismonth1 + " " + thisday + ", " + thisyear + ", " + Sthishour + ":" + Sthisminute + " " + ampm;

    String query1 = "SELECT * FROM LAST_TRANSFER_VIEW ORDER BY CERTIFICATE_NO";
    cm.queryExecute(query1);

    String bfolio = "";
    String bname = "";
    String ttype = "";
    String typetransfer = "";
    String certificateno = "";
    String distfrom1 = "";
    String distto1 = "";
    String distfrom2 = "";
    String distto2 = "";
    String distfrom3 = "";
    String distto3 = "";
    String distfrom4 = "";
    String distto4 = "";
    int totalshares = 0;
    int subtotalshares = 0;
%>
  <span class="style7">
  <form name="FormToSubmit" method="GET" action="RejectPendingTransfers.jsp">
  <SPAN id="dprint">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
  </SPAN>
    <center>
      <span id="dprint1" style="display:none">
	  <div class="style19">BRITISH AMERICAN TOBACCO BANGLADESH</div>
	  <BR>
      <div class="style20">SHARE MANAGEMENT SYSTEM - LIST OF PROCESSED TRANSACTIONS</div><br>
        <table width="100%"  border="0" cellpadding="0">
          <tr>
            <td width="50%" align="left" class="style20"><%=formdate%></td>
            <td width="50%" align="right" class="style20">&nbsp;</td>
          </tr>
        </table>
	  </span>
    </center>
  <table width="100%" BORDER=1  cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Last Accepted Transfers</center></td></tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="13%" class="style9"><div align="center">Buyer Folio</div></td>
    <td width="23%"><div align="center" class="style9">
      <div align="center">Buyer Name</div>
    </div></td>
    <td width="13%"><div align="center" class="style9">
      <div align="center">Certificate No.</div>
    </div></td>
    <td width="24%"><div align="center" class="style9">
      <div align="center">Distinctions</div>
    </div></td>
    <td width="8%"><div align="center" class="style9">
      <div align="center">Shares</div>
    </div></td>
    <td width="19%"><div align="center" class="style9">
      <div align="center">Type of Transfer</div>
    </div></td>
  </tr>
  <div align="left">
     <%
    while(cm.toNext())
     {
       subtotalshares = 0;
       bfolio = cm.getColumnS("BUYER_FOLIO");
       bname = cm.getColumnS("BUYER_NAME");
       ttype = cm.getColumnS("TYPE_OF_TRANSFER");
       certificateno = cm.getColumnS("CERTIFICATE_NO");
       distfrom1 = cm.getColumnS("DIST_FROM1");
       distto1 = cm.getColumnS("DIST_TO1");
       distfrom2 = cm.getColumnS("DIST_FROM2");
       distto2 = cm.getColumnS("DIST_TO2");
       distfrom3 = cm.getColumnS("DIST_FROM3");
       distto3 = cm.getColumnS("DIST_TO3");
       distfrom4 = cm.getColumnS("DIST_FROM4");
       distto4 = cm.getColumnS("DIST_TO4");

       if (String.valueOf(bfolio).equals("null"))
         bfolio = "";
       if (String.valueOf(bname).equals("null"))
         bname = "";
       if (String.valueOf(ttype).equals("null"))
         ttype = "";
       if (String.valueOf(certificateno).equals("null"))
         certificateno = "";
       if (String.valueOf(distfrom1).equals("null"))
         distfrom1 = "";
       if (String.valueOf(distto1).equals("null"))
         distto1 = "";
       if (String.valueOf(distfrom2).equals("null"))
         distfrom2 = "";
       if (String.valueOf(distto2).equals("null"))
         distto2 = "";
       if (String.valueOf(distfrom3).equals("null"))
         distfrom3 = "";
       if (String.valueOf(distto3).equals("null"))
         distto3 = "";
       if (String.valueOf(distfrom4).equals("null"))
         distfrom4 = "";
       if (String.valueOf(distto4).equals("null"))
         distto4 = "";

       if (ttype.equalsIgnoreCase("T"))
       {
         typetransfer = "Simple Transfer";
       }
       else if (ttype.equalsIgnoreCase("B"))
       {
         typetransfer = "Bonus";
       }
       else if (ttype.equalsIgnoreCase("M"))
       {
         typetransfer = "Multiple Transfer/Split";
       }
       else if (ttype.equalsIgnoreCase("R"))
       {
         typetransfer = "Right";
       }
       else if (ttype.equalsIgnoreCase("C"))
       {
         typetransfer = "Consolidation";
       }
       else if (ttype.equalsIgnoreCase("S"))
       {
         typetransfer = "Simple Split";
       }
       else if (ttype.equalsIgnoreCase("N"))
       {
         typetransfer = "New";
       }
       else if (ttype.equalsIgnoreCase("P"))
       {
         typetransfer = "Amalgamated Split";
       }
       else if (ttype.equalsIgnoreCase("G"))
       {
         typetransfer = "Amalgamation";
       }

       if (!bname.equals("null"))
       {
         %>
    </div>
  <tr bgcolor="#E8F3FD">
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=bfolio%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="left">&nbsp;<%=bname%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=certificateno%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">
               <%
                  int distcounter = 0;
                  if ((!distfrom1.equalsIgnoreCase("0")) && (!distfrom1.equalsIgnoreCase("")) && (!String.valueOf(distfrom1).equals("null")))
                  {
                    totalshares = totalshares + Integer.parseInt(distto1) - Integer.parseInt(distfrom1) + 1;
                    subtotalshares = subtotalshares + Integer.parseInt(distto1) - Integer.parseInt(distfrom1) + 1;
                    if (distcounter > 0)
                    {
                     %>
                        <br><%=distfrom1%> - <%=distto1%>
                     <%
                    }
                    else
                    {
                     %>
                        <%=distfrom1%> - <%=distto1%>
                     <%
                    }
                    distcounter++;
                  }
                  if ((!distfrom2.equalsIgnoreCase("0")) && (!distfrom2.equalsIgnoreCase("")) && (!String.valueOf(distfrom2).equals("null")))
                  {
                    totalshares = totalshares + Integer.parseInt(distto2) - Integer.parseInt(distfrom2) + 1;
                    subtotalshares = subtotalshares + Integer.parseInt(distto2) - Integer.parseInt(distfrom2) + 1;
                    if (distcounter > 0)
                    {
                     %>
                        <br><%=distfrom2%> - <%=distto2%>
                     <%
                    }
                    else
                    {
                     %>
                        <%=distfrom2%> - <%=distto2%>
                     <%
                    }
                    distcounter++;
                  }
                  if ((!distfrom3.equalsIgnoreCase("0")) && (!distfrom3.equalsIgnoreCase("")) && (!String.valueOf(distfrom3).equals("null")))
                  {
                    totalshares = totalshares + Integer.parseInt(distto3) - Integer.parseInt(distfrom3) + 1;
                    subtotalshares = subtotalshares + Integer.parseInt(distto3) - Integer.parseInt(distfrom3) + 1;
                    if (distcounter > 0)
                    {
                     %>
                        <br><%=distfrom3%> - <%=distto3%>
                     <%
                    }
                    else
                    {
                     %>
                        <%=distfrom3%> - <%=distto3%>
                     <%
                    }
                    distcounter++;
                  }
                  if ((!distfrom4.equalsIgnoreCase("0")) && (!distfrom4.equalsIgnoreCase("")) && (!String.valueOf(distfrom4).equals("null")))
                  {
                    totalshares = totalshares + Integer.parseInt(distto4) - Integer.parseInt(distfrom4) + 1;
                    subtotalshares = subtotalshares + Integer.parseInt(distto4) - Integer.parseInt(distfrom4) + 1;
                    if (distcounter > 0)
                    {
                     %>
                        <br><%=distfrom4%> - <%=distto4%>
                     <%
                    }
                    else
                    {
                     %>
                        <%=distfrom4%> - <%=distto4%>
                     <%
                    }
                    distcounter++;
                  }
               %>
             </div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=subtotalshares%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=typetransfer%>&nbsp;</div>
           </div></td>
    </tr>
             <%
         }
     }
     %>
    </table>
    <table width="100%"  border="0" cellpadding="0">
      <tr>
        <td width="50%" align="left" class="style20"></td>
        <td width="50%" align="right" class="style20"><br>Total Transferred Shares: <%=totalshares%></td>
      </tr>
    </table>
</form>
     <%
   if (isConnect)
   {
     cm.takeDown();
   }
  %>
</body>
</html>
