<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : October 2005
'Page Purpose  : Updates a Simple Splitting Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update Simple Splitting</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String SellerFolio = String.valueOf(request.getParameter("sfolio"));
  String CertificateNo = String.valueOf(request.getParameter("certificateno"));
  String DistFrom1 = String.valueOf(request.getParameter("distinctionstart"));
  String DistTo1 = String.valueOf(request.getParameter("distinctionend"));
  String TotalShares = String.valueOf(request.getParameter("tnofshares"));
  String TransferID = String.valueOf(request.getParameter("transferid"));
  String ProposedUnits = String.valueOf(request.getParameter("proposedunits"));

  String SplitCheck = "T";
  String AmalCheck = "F";
  int errorcounter = 0;

  SellerFolio=cm.replace(SellerFolio,"'","''");
  CertificateNo=cm.replace(CertificateNo,"'","''");
  DistFrom1=cm.replace(DistFrom1,"'","''");
  DistTo1=cm.replace(DistTo1,"'","''");

  int iProposedUnits = Integer.parseInt(ProposedUnits);
  int iTotalShares = Integer.parseInt(TotalShares);
  int iDistFrom1 = Integer.parseInt(DistFrom1);
  int iDistTo1 = Integer.parseInt(DistTo1);

  if (iProposedUnits >= iTotalShares)
  {
    errorcounter++;
    %>
     <script language="javascript">
      alert("Total No. of Shares is less than the Proposed Split Unit. Cannot perform Split.");
      history.go(-1);
     </script>
    <%
  }

  if (iDistFrom1 >= iDistTo1)
  {
    errorcounter++;
    %>
     <script language="javascript">
      alert("Invalid Distinctions. Please Check!");
      history.go(-1);
     </script>
    <%
  }

  String query10 = "SELECT * FROM SCRIPTS_VIEW WHERE LOWER(CERTIFICATE_NO) = LOWER('" + CertificateNo + "')";
  int nofscripts = cm.queryExecuteCount(query10);

  if (nofscripts > 1)
   {
     errorcounter++;
     %>
       <script language="javascript">
        alert("More than one Distinctions exist for the Certificate. Cannot perform Split!");
        history.go(-1);
       </script>
     <%
   }

  String FolioHolder = "";
  String isValid = "";
  String isLocked = "";
  String isActive = "";
  String isConsolidated = "";
  String TypeofTransfer = "S";

  String query2 = "SELECT * FROM SHAREHOLDER_VIEW WHERE lower(FOLIO_NO)=lower('"+SellerFolio+"')";
  cm.queryExecute(query2);
  int sellerexists = cm.queryExecuteCount(query2);

  String SellerName = "";

  if (sellerexists > 0)
  {
   SellerName = cm.getColumnS("NAME");
   isActive = cm.getColumnS("ACTIVE");

   if (String.valueOf(isActive).equals("null"))
     isActive = "";
   if (String.valueOf(SellerName).equals("null"))
     SellerName = "";

   if (isActive.equalsIgnoreCase("F"))
   {
        errorcounter++;
       %>
        <script language="javascript">
         alert("The Seller Folio is not Active!");
         history.go(-1);
        </script>
       <%
   }

   String query3 = "SELECT * FROM CERTIFICATE_VIEW WHERE lower(CERTIFICATE_NO)=lower('"+CertificateNo+"')";
   cm.queryExecute(query3);
   int certificateexists = cm.queryExecuteCount(query3);

   if (certificateexists > 0)
    {
         FolioHolder = cm.getColumnS("HOLDER_FOLIO");
         isValid = cm.getColumnS("VALIDITY");
         isLocked = cm.getColumnS("LOCK");
         isConsolidated = cm.getColumnS("CONSOLIDATED");

         if (String.valueOf(FolioHolder).equals("null"))
         {
           FolioHolder = "";
         }
         if (String.valueOf(isValid).equals("null"))
         {
           isValid = "";
         }
         if (String.valueOf(isLocked).equals("null"))
         {
           isLocked = "";
         }
         if (String.valueOf(isConsolidated).equals("null"))
         {
           isConsolidated = "";
         }

      if (!FolioHolder.equalsIgnoreCase(SellerFolio))
      {
        errorcounter++;
       %>
        <script language="javascript">
         alert("The Certificate does not belong to the Folio!");
         history.go(-1);
        </script>
       <%
      }

      if (isValid.equalsIgnoreCase("F"))
      {
        errorcounter++;
       %>
        <script language="javascript">
         alert("The Certificate is no longer Valid!");
         history.go(-1);
        </script>
       <%
      }

      if (isLocked.equalsIgnoreCase("T"))
      {
        errorcounter++;
       %>
        <script language="javascript">
         alert("The Certificate is locked!");
         history.go(-1);
        </script>
       <%
      }

      if (isConsolidated.equalsIgnoreCase("T"))
      {
        errorcounter++;
       %>
        <script language="javascript">
         alert("The Certificate is Consolidated!");
         history.go(-1);
        </script>
       <%
      }

     String query6 = "SELECT * FROM SCRIPTS_VIEW WHERE LOWER(CERTIFICATE_NO) = LOWER('" + CertificateNo + "') AND LOWER(DIST_FROM) = LOWER('"+DistFrom1+ "') AND LOWER(DIST_TO) = LOWER('"+DistTo1+ "')";
     int dist1exists = cm.queryExecuteCount(query6);

     if (dist1exists < 1)
     {
      errorcounter++;
      %>
       <script language="javascript">
        alert("Distinction is not valid. Please Check!");
        history.go(-1);
       </script>
      <%
     }
    }
   else
   {
        errorcounter++;
    %>
     <script language="javascript">
       alert("Certificate Does Not Exist!");
       history.go(-1);
     </script>
    <%
   }
  }
  else
  {
        errorcounter++;
  %>
  <script language="javascript">
    alert("Folio Does Not Exist!");
    history.go(-1);
  </script>
  <%
  }

  int duplicateexists = 0;
  int temp1 = 0;

  String query4m = "SELECT * FROM TRANSFER_T_VIEW WHERE (REC_NO <> "+TransferID+") AND (LOWER(TYPE_OF_TRANSFER)=LOWER('M') AND ((OLD_CERTIFICATE_FROM <= " + CertificateNo + ") AND (OLD_CERTIFICATE_TO >= " + CertificateNo + ")))";
  temp1 = cm.queryExecuteCount(query4m);
  duplicateexists = duplicateexists + temp1;
  String query4a = "SELECT * FROM TRANSFER_T_VIEW WHERE (REC_NO <> "+TransferID+") AND (((lower(OLD_CERTIFICATE_FROM)=lower('"+CertificateNo+"')) OR (lower(XCERTIFICATE1)=lower('"+CertificateNo+"')) OR (lower(XCERTIFICATE2)=lower('"+CertificateNo+"')) OR (lower(XCERTIFICATE3)=lower('"+CertificateNo+"')) OR (lower(XCERTIFICATE4)=lower('"+CertificateNo+"'))))";
  temp1 = cm.queryExecuteCount(query4a);
  duplicateexists = duplicateexists + temp1;

  if (duplicateexists > 0)
  {
    errorcounter++;
    %>
     <script language="javascript">
      alert("The Certificate is already in the Transfer/Splitting Process!");
      history.go(-1);
     </script>
  <%
  }

  String nsimplesplitting = "call UPDATE_TRANSFER_T(" + TransferID + ", '" + SellerFolio + "', '" + SellerName + "', '" + SellerFolio + "', '" + SellerName + "', '" + TypeofTransfer + "', '" + TotalShares + "','" + CertificateNo + "','" + ProposedUnits + "','" + SplitCheck + "','" + AmalCheck + "','" + DistFrom1 + "','" + DistTo1 + "','','','','','','')";

  if (errorcounter == 0)
  {
    boolean b = cm.procedureExecute(nsimplesplitting);
  }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Edited Simple Share Splitting Request')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  alert("The Splitting Request has been Updated");
  location = "<%=request.getContextPath()%>/jsp/Transfer/AcceptTransfer.jsp";
</script>
</body>
</html>
