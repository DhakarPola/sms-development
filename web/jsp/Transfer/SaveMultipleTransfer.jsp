<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : November 2005
'Page Purpose  : Saves Multiple Transfer Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Multiple Transfer</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String SellerFolio = String.valueOf(request.getParameter("sfolio"));
  String BuyerFolio = String.valueOf(request.getParameter("bfolio"));
  String CertificateStart = String.valueOf(request.getParameter("certificatestart"));
  String CertificateEnd = String.valueOf(request.getParameter("certificateend"));
  String DistinctionStart = String.valueOf(request.getParameter("distinctionstart"));
  String DistinctionEnd = String.valueOf(request.getParameter("distinctionend"));
  String TotalShares = String.valueOf(request.getParameter("tnofshares"));
  String ProposedUnits = String.valueOf(request.getParameter("proposedunits"));
  String SplitCheck = "";
  String AmalCheck = "F";
  int errorcounter = 0;

  SellerFolio=cm.replace(SellerFolio,"'","''");
  BuyerFolio=cm.replace(BuyerFolio,"'","''");
  CertificateStart=cm.replace(CertificateStart,"'","''");
  CertificateEnd=cm.replace(CertificateEnd,"'","''");
  DistinctionStart=cm.replace(DistinctionStart,"'","''");
  DistinctionEnd=cm.replace(DistinctionEnd,"'","''");
  TotalShares=cm.replace(TotalShares,"'","''");
  ProposedUnits=cm.replace(ProposedUnits,"'","''");

  int DistinctionLength = DistinctionStart.length();

  if (DistinctionLength != DistinctionEnd.length())
  {
     errorcounter++;
     %>
        <script language="javascript">
         alert("Invalid Distinction Numbers!");
         history.go(-1);
        </script>
     <%
  }

  int iCertificateStart = Integer.parseInt(CertificateStart);
  int iCertificateEnd = Integer.parseInt(CertificateEnd);
  int iDistinctionStart = Integer.parseInt(DistinctionStart);
  int iDistinctionEnd = Integer.parseInt(DistinctionEnd);

  if (String.valueOf(ProposedUnits).equals("null"))
    ProposedUnits = "";

  if (ProposedUnits.equalsIgnoreCase(""))
  {
    SplitCheck = "F";
  }
  else
  {
    SplitCheck = "T";
  }

  if (iCertificateStart >= iCertificateEnd)
  {
     errorcounter++;
     %>
        <script language="javascript">
         alert("Invalid Certificate Number!");
         history.go(-1);
        </script>
     <%
  }

  if (iDistinctionStart >= iDistinctionEnd)
  {
     errorcounter++;
     %>
        <script language="javascript">
         alert("Invalid Distinction Number!");
         history.go(-1);
        </script>
     <%
  }

  String FolioHolder = "";
  String isValid = "";
  String isLocked = "";
  String isActive = "";
  String TypeofTransfer = "M";

  int startofdistinction = 0;
  int endofdistinction = 0;
  int certcounter = 0;
  int lastdistinctionend = -1;
  int thisdistinctionstart = 0;
  int sellerexists = 0;
  int buyerexists = 0;

  String query2 = "SELECT * FROM SHAREHOLDER_VIEW WHERE lower(FOLIO_NO)=lower('"+SellerFolio+"')";
  cm.queryExecute(query2);
  sellerexists = cm.queryExecuteCount(query2);

  String SellerName = "";

  if (sellerexists > 0)
  {
   SellerName = cm.getColumnS("NAME");
   isActive = cm.getColumnS("ACTIVE");

   if (String.valueOf(isActive).equals("null"))
     isActive = "";
   if (String.valueOf(SellerName).equals("null"))
     SellerName = "";

   if (isActive.equalsIgnoreCase("F"))
   {
        errorcounter++;
       %>
        <script language="javascript">
         alert("The Seller Folio is not Active!");
         history.go(-1);
        </script>
       <%
   }

   for (int i=iCertificateStart;i<iCertificateEnd+1;i++)
   {
   String SCertificate = String.valueOf(i);
   int LenSCertificate = SCertificate.length();
//   int DifInLen = DistinctionLength - LenSCertificate;
   int DifInLen = 9 - LenSCertificate;

   if (DifInLen > 0)
   {
     for (int j=0;j<DifInLen;j++)
     {
       SCertificate = "0" + SCertificate;
     }
   }

   String query3 = "SELECT * FROM CERTIFICATE_VIEW WHERE lower(CERTIFICATE_NO)=lower('"+SCertificate+"')";
   cm.queryExecute(query3);
   int certificateexists = cm.queryExecuteCount(query3);

   if (certificateexists > 0)
    {
         FolioHolder = cm.getColumnS("HOLDER_FOLIO");
         isValid = cm.getColumnS("VALIDITY");
         isLocked = cm.getColumnS("LOCK");

         if (String.valueOf(FolioHolder).equals("null"))
         {
           FolioHolder = "";
         }
         if (String.valueOf(isValid).equals("null"))
         {
           isValid = "";
         }
         if (String.valueOf(isLocked).equals("null"))
         {
           isLocked = "";
         }

      if (!FolioHolder.equalsIgnoreCase(SellerFolio))
      {
        errorcounter++;
       %>
        <script language="javascript">
         alert("One or more Certificates do not belong to the Seller Folio!");
         history.go(-1);
        </script>
       <%
      }

      if (isValid.equalsIgnoreCase("F"))
      {
        errorcounter++;
       %>
        <script language="javascript">
         alert("One or more Certificates are no longer Valid!");
         history.go(-1);
        </script>
       <%
      }

      if (isLocked.equalsIgnoreCase("T"))
      {
        errorcounter++;
       %>
        <script language="javascript">
         alert("One or more Certificates are locked!");
         history.go(-1);
        </script>
       <%
      }

      String query10 = "SELECT * FROM SCRIPTS_VIEW WHERE lower(CERTIFICATE_NO)=lower('"+SCertificate+"')";
      int moreecriptsexist = cm.queryExecuteCount(query10);

      if (moreecriptsexist > 1)
      {
       errorcounter++;
       %>
        <script language="javascript">
         alert("One or more Certificates have multiple Distinctions. Amalgamate First!");
         history.go(-1);
        </script>
       <%
      }

      String query6 = "SELECT * FROM SCRIPTS_VIEW WHERE (" + iDistinctionStart + " <= DIST_FROM AND " + iDistinctionEnd + " >= DIST_TO) AND lower(CERTIFICATE_NO)=lower('"+SCertificate+"')";
      int dist1exists = cm.queryExecuteCount(query6);
//      cm.queryExecute(query6);

      if (dist1exists < 1)
      {
       errorcounter++;
       %>
        <script language="javascript">
         alert("Distinctions are not valid. Please Check!");
         history.go(-1);
        </script>
       <%
      }
      else
      {
        thisdistinctionstart = cm.getColumnI("DIST_FROM");
        if (certcounter == 0)
        {
          startofdistinction = thisdistinctionstart;
        }
        if (lastdistinctionend > -1)
        {
          if (thisdistinctionstart != lastdistinctionend + 1)
          {
           errorcounter++;
           %>
            <script language="javascript">
             alert("Distinctions are not consecutive. Please Check!");
             history.go(-1);
            </script>
           <%
          }
        }
        endofdistinction = cm.getColumnI("DIST_TO");
        lastdistinctionend = endofdistinction;
      }
    }
   else
   {
        errorcounter++;
    %>
     <script language="javascript">
//       alert("Seller Folio Does Not Exist!");
       alert("One or more Certificates Do Not Exist!");
       history.go(-1);
     </script>
    <%
   }
   certcounter++;
  }
  //End of for Loop

    if ((startofdistinction != iDistinctionStart) || (endofdistinction != iDistinctionEnd))
    {
        errorcounter++;
    %>
     <script language="javascript">
       alert("Distinctions are not valid. Please Check!");
       history.go(-1);
     </script>
    <%

    }
  }
  else if (sellerexists == 0)
  {
        errorcounter++;
  %>
  <script language="javascript">
    alert("Seller Folio Does Not Exist!");
    history.go(-1);
  </script>
  <%
  }

  String query1 = "SELECT * FROM SHAREHOLDER_VIEW WHERE lower(FOLIO_NO)=lower('"+BuyerFolio+"')";

  cm.queryExecute(query1);
  buyerexists = cm.queryExecuteCount(query1);

  String BuyerName = "";
  String isBuyerActive = "";

  if (buyerexists > 0)
  {
       BuyerName = cm.getColumnS("NAME");
       isBuyerActive = cm.getColumnS("ACTIVE");

       if (String.valueOf(BuyerName).equals("null"))
         BuyerName = "";
       if (String.valueOf(isBuyerActive).equals("null"))
         isBuyerActive = "";

       if (isBuyerActive.equalsIgnoreCase("F"))
       {
        errorcounter++;
       %>
        <script language="javascript">
         alert("The Buyer Folio is not Active!");
         history.go(-1);
        </script>
       <%
       }
  }
  else
  {
        errorcounter++;
  %>
  <script language="javascript">
    alert("Buyer Folio Does Not Exist!");
    history.go(-1);
  </script>
  <%
  }

  int duplicateexists = 0;
  int temp1 = 0;

  String query4x = "SELECT * FROM TRANSFER_T_VIEW WHERE (((XCERTIFICATE1 >= " + iCertificateStart + ") AND (XCERTIFICATE1 <= " + iCertificateEnd + ")) OR ((XCERTIFICATE2 >= " + iCertificateStart + ") AND (XCERTIFICATE2 <= " + iCertificateEnd + ")) OR ((XCERTIFICATE3 >= " + iCertificateStart + ") AND (XCERTIFICATE3 <= " + iCertificateEnd + ")) OR ((XCERTIFICATE4 >= " + iCertificateStart + ") AND (XCERTIFICATE4 <= " + iCertificateEnd + ")))";
  temp1 = cm.queryExecuteCount(query4x);
  duplicateexists = duplicateexists + temp1;

  String query4s = "SELECT * FROM TRANSFER_T_VIEW WHERE LOWER(TYPE_OF_TRANSFER) <> LOWER('"+TypeofTransfer+"') AND ((OLD_CERTIFICATE_FROM >= " + iCertificateStart + ") AND (OLD_CERTIFICATE_FROM <= " + iCertificateEnd + "))";
  temp1 = cm.queryExecuteCount(query4s);
  duplicateexists = duplicateexists + temp1;

  String query4 = "SELECT * FROM TRANSFER_T_VIEW WHERE LOWER(TYPE_OF_TRANSFER)=LOWER('"+TypeofTransfer+"') AND (((OLD_CERTIFICATE_FROM >= " + iCertificateStart + ") AND (OLD_CERTIFICATE_FROM <= " + iCertificateEnd + ")) OR ((OLD_CERTIFICATE_TO >= " + iCertificateStart + ") AND (OLD_CERTIFICATE_TO <= " + iCertificateEnd + ")))";
  temp1 = cm.queryExecuteCount(query4);
  duplicateexists = duplicateexists + temp1;

  if (duplicateexists > 0)
  {
        errorcounter++;
    %>
     <script language="javascript">
      alert("One or more Certificates are already in the Transfer Process!");
      history.go(-1);
     </script>
  <%
  }

  String nmultipletransfer = "call ADD_MULTIPLE_TRANSFER_T('" + SellerFolio + "', '" + SellerName + "', '" + BuyerFolio + "', '" + BuyerName + "', '" + TypeofTransfer + "', '" + TotalShares + "','" + ProposedUnits + "','" + CertificateStart + "','" + CertificateEnd + "','" + SplitCheck + "','" + AmalCheck + "','" + DistinctionStart + "','" + DistinctionEnd + "')";

  if (errorcounter == 0)
  {
    boolean b = cm.procedureExecute(nmultipletransfer);
  }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added Multiple Share Transfer Request')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  alert("The Transfer Request has been Entered");
  location = "<%=request.getContextPath()%>/jsp/Transfer/MultipleTransfer.jsp";
</script>
</body>
</html>
