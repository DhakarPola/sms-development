<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : October 2005
'Page Purpose  : Form for the Administrator to Accept Transfers.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Accept Transfer</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

function toacceptedtransfers()
{
  location = "<%=request.getContextPath()%>/jsp/Transfer/SaveAcceptedTransfers.jsp"
}

 function askreject()
 {
  if (confirm("Do you want to Reject the Selected Transfers?"))
  {
    SubmitThis();
  }
 }

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String query1 = "SELECT * FROM TRANSFER_T_VIEW";
    cm.queryExecute(query1);

    String sfolio = "";
    String sname = "";
    String bfolio = "";
    String bname = "";
    String ttype = "";
    String typetransfer = "";
    int nofshares = 0;
    String certificateno = "";
    String certificateend = "";
    String xcer1 = "";
    String xcer2 = "";
    String xcer3 = "";
    String xcer4 = "";
    String dest = "";

    int transferid = 0;
    int rowcounter = 0;
%>
  <span class="style7">
  <form name="FormToSubmit" method="GET" action="RejectPendingTransfers.jsp">
  <SPAN id="dprint">
    <img name="B4" src="<%=request.getContextPath()%>/images/btnAcceptAll.gif" onclick="toacceptedtransfers()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAcceptAllR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAcceptAll.gif'">
    <img name="B6" src="<%=request.getContextPath()%>/images/btnReject.gif" onclick="askreject()" onMouseOver="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnRejectR.gif'" onMouseOut="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnReject.gif'">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
  </SPAN>
  <table width="100%" BORDER=1  cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Transfer Requests</center></td></tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="12%" class="style9"><div align="center">Seller Folio</div></td>
    <td width="24%"><div align="center" class="style9">
      <div align="center">Seller Name</div>
    </div></td>
    <td width="14%"><div align="center" class="style9">
      <div align="center">Certificate No.</div>
    </div></td>
    <td width="12%"><div align="center" class="style9">
      <div align="center">Buyer Folio</div>
    </div></td>
    <td width="24%"><div align="center" class="style9">
      <div align="center">Buyer Name</div>
    </div></td>
    <td width="7%"><div align="center" class="style9">
      <div align="center">Type</div>
    </div></td>
    <td width="7%"><div align="center" class="style9">
      <div align="center">Shares</div>
    </div></td>
  </tr>
  <div align="left">
     <%
    while(cm.toNext())
     {
       rowcounter++;
       sfolio = cm.getColumnS("SELLER_FOLIO");
       sname = cm.getColumnS("SELLER_NAME");
       bfolio = cm.getColumnS("BUYER_FOLIO");
       bname = cm.getColumnS("BUYER_NAME");
       ttype = cm.getColumnS("TYPE_OF_TRANSFER");
       certificateno = cm.getColumnS("OLD_CERTIFICATE_FROM");
       transferid = cm.getColumnI("REC_NO");
       nofshares = cm.getColumnI("NO_OF_SHARES");
       certificateend = cm.getColumnS("OLD_CERTIFICATE_TO");
       xcer1 = cm.getColumnS("XCERTIFICATE1");
       xcer2 = cm.getColumnS("XCERTIFICATE2");
       xcer3 = cm.getColumnS("XCERTIFICATE3");
       xcer4 = cm.getColumnS("XCERTIFICATE4");

       if (String.valueOf(certificateend).equals("null"))
         certificateend = "";

       if (ttype.equalsIgnoreCase("T"))
       {
         typetransfer = "Simple Transfer";
         dest = request.getContextPath() + "/jsp/Transfer/EditSimpleTransfer.jsp";
       }
       else if (ttype.equalsIgnoreCase("B"))
       {
         typetransfer = "Bonus";
         dest = request.getContextPath() + "/jsp/Transfer/EditBonusTransfer.jsp";
       }
       else if (ttype.equalsIgnoreCase("M"))
       {
         if (sfolio.equalsIgnoreCase(bfolio))
         {
           typetransfer = "Multiple Split";
           dest = request.getContextPath() + "/jsp/Transfer/EditMultipleSplitting.jsp";
         }
         else
         {
           typetransfer = "Multiple Transfer";
           dest = request.getContextPath() + "/jsp/Transfer/EditMultipleTransfer.jsp";
         }
       }
       else if (ttype.equalsIgnoreCase("R"))
       {
         typetransfer = "Right";
         dest = request.getContextPath() + "/jsp/Transfer/EditRightTransfer.jsp";
       }
       else if (ttype.equalsIgnoreCase("C"))
       {
         typetransfer = "Consolidation";
         dest = request.getContextPath() + "/jsp/Transfer/EditConsolidation.jsp";
       }
       else if (ttype.equalsIgnoreCase("S"))
       {
         typetransfer = "Simple Split";
         dest = request.getContextPath() + "/jsp/Transfer/EditNormalSplitting.jsp";
       }
       else if (ttype.equalsIgnoreCase("N"))
       {
         typetransfer = "New";
         dest = request.getContextPath() + "/jsp/Transfer/EditNewTransfer.jsp";
       }
       else if (ttype.equalsIgnoreCase("P"))
       {
         typetransfer = "Amalgamated Split";
         dest = request.getContextPath() + "/jsp/Transfer/EditAmalgamatedSplitting.jsp";
       }
       else if (ttype.equalsIgnoreCase("G"))
       {
         typetransfer = "Amalgamation";
         dest = request.getContextPath() + "/jsp/Transfer/EditAmalgamation.jsp";
       }

       if (!sname.equals("null"))
       {
         %>
    </div>
  <tr bgcolor="#E8F3FD">
           <td><div align="center" class="style10">
             <div align="left" class="style10">
               <span class="style13">
               <input type="checkbox" name="rejectbox<%=rowcounter%>" value="<%=transferid%>">
               <a HREF="<%=dest%>?tid1=<%=transferid%>"><%=sfolio%></a>
               </span></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="left">&nbsp;<%=sname%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
            <div align="center">
             <%
               if (ttype.equalsIgnoreCase("G") || ttype.equalsIgnoreCase("C"))
               {
                 %>
                   <%=xcer1%>
                     <%
                       if (!xcer2.equalsIgnoreCase("0"))
                       {
                         %>
                           ,&nbsp;<%=xcer2%>
                         <%
                       }
                     %>
                     <%
                       if (!xcer3.equalsIgnoreCase("0"))
                       {
                         %>
                           ,&nbsp;<%=xcer3%>
                         <%
                       }
                     %>
                     <%
                       if (!xcer4.equalsIgnoreCase("0"))
                       {
                         %>
                           ,&nbsp;<%=xcer4%>
                         <%
                       }
               }
               else
               {
                 %>
                   <%=certificateno%>&nbsp;
                 <%
                 if (!certificateend.equalsIgnoreCase(""))
                 {
                   %>
                     -&nbsp;<%=certificateend%>
                   <%
                 }
               }
             %>
             </div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=bfolio%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="left">&nbsp;&nbsp;<%=bname%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=typetransfer%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=nofshares%>&nbsp;</div>
           </div></td>
         </tr>
             <%
         }
     }
     %>
       </form>
     <%
   if (isConnect)
   {
     cm.takeDown();
   }
  %>
</body>
</html>
