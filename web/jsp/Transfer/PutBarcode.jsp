<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : June 2006
'Page Purpose  : Puts in Barcode Info.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.util.Date,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Put Barcode</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();

   if (isConnect==false)
   {
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
   }

  String CertificateNo = String.valueOf(request.getParameter("certno"));
  String PageName = String.valueOf(request.getParameter("pgname"));
  int distfrom = 0;
  int distto = 0;
  String[][] distarray = new String[4][2];
  int rowcounter = 0;
  String holderfolio = "";

  distarray[0][0] = "";
  distarray[0][1] = "";
  distarray[1][0] = "";
  distarray[1][1] = "";
  distarray[2][0] = "";
  distarray[2][1] = "";
  distarray[3][0] = "";
  distarray[3][1] = "";

  String query3 = "SELECT * FROM SCRIPTS WHERE CERTIFICATE_NO = '" + CertificateNo + "'";
  cm.queryExecute(query3);

   while(cm.toNext())
     {
       distfrom = cm.getColumnI("DIST_FROM");
       distto = cm.getColumnI("DIST_TO");

       if (rowcounter < 4)
       {
        distarray[rowcounter][0] = String.valueOf(distfrom);
        distarray[rowcounter][1] = String.valueOf(distto);
       }
       rowcounter++;
     }

  String query31 = "SELECT * FROM CERTIFICATE WHERE CERTIFICATE_NO = '" + CertificateNo + "'";
  cm.queryExecute(query31);

   while(cm.toNext())
   {
     holderfolio = cm.getColumnS("HOLDER_FOLIO");
   }

   if (isConnect)
   {
     cm.takeDown();
   }

  if (PageName.equalsIgnoreCase("simpletransfer"))
  {
  %>
  <script language="javascript">
    location = "<%=request.getContextPath()%>/jsp/Transfer/SimpleTransfer.jsp?dist1from=<%=distarray[0][0]%>&dist1to=<%=distarray[0][1]%>&dist2from=<%=distarray[1][0]%>&dist2to=<%=distarray[1][1]%>&dist3from=<%=distarray[2][0]%>&dist3to=<%=distarray[2][1]%>&dist4from=<%=distarray[3][0]%>&dist4to=<%=distarray[3][1]%>&hfolio=<%=holderfolio%>&cert1no=<%=CertificateNo%>";
  </script>
  <%
  }
  else if (PageName.equalsIgnoreCase("normalsplitting"))
  {
  %>
  <script language="javascript">
    location = "<%=request.getContextPath()%>/jsp/Transfer/NormalSplitting.jsp?dist1from=<%=distarray[0][0]%>&dist1to=<%=distarray[0][1]%>&dist2from=<%=distarray[1][0]%>&dist2to=<%=distarray[1][1]%>&dist3from=<%=distarray[2][0]%>&dist3to=<%=distarray[2][1]%>&dist4from=<%=distarray[3][0]%>&dist4to=<%=distarray[3][1]%>&hfolio=<%=holderfolio%>&cert1no=<%=CertificateNo%>";
  </script>
  <%
  }
  else if (PageName.equalsIgnoreCase("amalgamsplit"))
  {
  %>
  <script language="javascript">
    location = "<%=request.getContextPath()%>/jsp/Transfer/AmalgamatedSplitting.jsp?dist1from=<%=distarray[0][0]%>&dist1to=<%=distarray[0][1]%>&dist2from=<%=distarray[1][0]%>&dist2to=<%=distarray[1][1]%>&dist3from=<%=distarray[2][0]%>&dist3to=<%=distarray[2][1]%>&dist4from=<%=distarray[3][0]%>&dist4to=<%=distarray[3][1]%>&hfolio=<%=holderfolio%>&cert1no=<%=CertificateNo%>";
  </script>
  <%
  }
%>
</body>
</html>
