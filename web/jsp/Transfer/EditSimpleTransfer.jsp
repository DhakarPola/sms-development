<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : November 2005
'Page Purpose  : Edits a new Simple Transfer.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Simple Transfer</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function formconfirm()
{
  if (confirm("Do you want to Update the Transfer Information?"))
  {
    return true;
  }
}

function canwesplit()
{
  if (document.forms[0].cansplit.checked)
  {
    document.all.punits.style.display = ''
  }
  else
  {
    document.all.punits.style.display = 'none'
  }
}

function addprimaryshares(checkvalue1,checkvalue2,checkvalue3)
{
  var checkme1;
  var checkme2;
  var ptotal;
  checkme1 = document.all[checkvalue1].value;
  checkme2 = document.all[checkvalue2].value;

  if((checkme1.length > 0)&&(checkme2.length > 0))
  {
    if ((checkme2 - checkme1) > -1)
    {
      ptotal = checkme2 - checkme1 + 1;
      document.all[checkvalue3].value = ptotal;
      addshares(checkvalue3);
    }
  }
}

function addshares(checkvalue)
{
  var checkme;
  checkme = document.all[checkvalue].value;

  if(checkme.length <= 0)
  {
    document.all[checkvalue].value='';
  }

  var quantity1;
  var quantity2;
  var quantity3;
  var quantity4;
  var Totalquantitycal;

  quantity1 = replaceChars(document.all["nofshares1"].value,",","");
  quantity2 = replaceChars(document.all["nofshares2"].value,",","");
  quantity3 = replaceChars(document.all["nofshares3"].value,",","");
  quantity4 = replaceChars(document.all["nofshares4"].value,",","");

  Totalquantitycal = 0;

  if(quantity1 > 0)
   Totalquantitycal = Number(Totalquantitycal) + Number(quantity1);
  if(quantity2 > 0)
   Totalquantitycal = Number(Totalquantitycal) + Number(quantity2);
  if(quantity3 > 0)
   Totalquantitycal = Number(Totalquantitycal) + Number(quantity3);
  if(quantity4 > 0)
   Totalquantitycal = Number(Totalquantitycal) + Number(quantity4);

  document.all["tnofshares"].value = Totalquantitycal;
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('sfolio','- Seller Folio (Option must be entered)');
  BlankValidate('certificateno','- Certificate no. (Option must be entered)');
  BlankValidate('bfolio','- Buyer Folio (Option must be entered)');

  var val1;
  var val2;
  var val3;
  var val4;
  var val5;
  var val6;
  var val7;
  var val8;
  var val9;
  var val10;
  var val11;
  var val12;
  var val13;
  var val14;

  val13  = trim(window.document.all["sfolio"].value);
  val14  = trim(window.document.all["bfolio"].value);

  if ((val13 != "") && (val14 != ""))
  {
    if (val13 == val14)
     {
       count = count + 1;
       nArray[count] = '- Buyer and Seller Folios are same';
     }
  }

  val1  = trim(window.document.all["distfrom1"].value);
  val2  = trim(window.document.all["distto1"].value);
  val3  = trim(window.document.all["nofshares1"].value);
  val4  = trim(window.document.all["distfrom2"].value);
  val5  = trim(window.document.all["distto2"].value);
  val6  = trim(window.document.all["nofshares2"].value);
  val7  = trim(window.document.all["distfrom3"].value);
  val8  = trim(window.document.all["distto3"].value);
  val9  = trim(window.document.all["nofshares3"].value);
  val10 = trim(window.document.all["distfrom4"].value);
  val11 = trim(window.document.all["distto4"].value);
  val12 = trim(window.document.all["nofshares4"].value);

  if (((val1 == "") && (val2 == "") && (val3 == "")) && ((val4 == "") && (val5 == "") && (val6 == "")) && ((val7 == "") && (val8 == "") && (val9 == "") &&((val10 == "") && (val11 == "") && (val12 == ""))))
   {
     count = count + 1;
     nArray[count] = '- At least one Distinction must be entered';
   }
  else
  {
    if ((val1 != "") || (val2 != "") || (val3 != ""))
    {
      if ((val1 == "") || (val2 == "") || (val3 == ""))
      {
       count = count + 1;
       nArray[count] = '- Distinction 1 is Incomplete or Invalid';
      }
    }
    if ((val4 != "") || (val5 != "") || (val6 != ""))
    {
      if ((val4 == "") || (val5 == "") || (val6 == ""))
      {
       count = count + 1;
       nArray[count] = '- Distinction 2 is Incomplete or Invalid';
      }
    }
    if ((val7 != "") || (val8 != "") || (val9 != ""))
    {
      if ((val7 == "") || (val8 == "") || (val9 == ""))
      {
       count = count + 1;
       nArray[count] = '- Distinction 3 is Incomplete or Invalid';
      }
    }
    if ((val10 != "") || (val11 != "") || (val12 != ""))
    {
      if ((val10 == "") || (val11 == "") || (val12 == ""))
      {
       count = count + 1;
       nArray[count] = '- Distinction 4 is Incomplete or Invalid';
      }
    }
  }

  if (document.forms[0].cansplit.checked)
  {
    BlankValidate('proposedunits','- Proposed Unit (Option must be entered)');
  }

  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF" onload="canwesplit()">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

  String ThisID = String.valueOf(request.getParameter("tid1"));
  int iThisID = Integer.parseInt(ThisID);

  String query1 = "SELECT * FROM TRANSFER_T_VIEW WHERE REC_NO = " + iThisID;
  cm.queryExecute(query1);

  String sfolio = "";
  String certificateno = "";
  String distfrom1 = "";
  String distto1 = "";
  String distfrom2 = "";
  String distto2 = "";
  String distfrom3 = "";
  String distto3 = "";
  String distfrom4 = "";
  String distto4 = "";
  int nofshares1 = 0;
  int nofshares2 = 0;
  int nofshares3 = 0;
  int nofshares4 = 0;
  int tnofshares = 0;
  String bfolio = "";
  String issplit = "";
  int proposedunits = 0;

  String Snofshares1 = "";
  String Snofshares2 = "";
  String Snofshares3 = "";
  String Snofshares4 = "";

  while(cm.toNext())
   {
     sfolio = cm.getColumnS("SELLER_FOLIO");
     bfolio = cm.getColumnS("BUYER_FOLIO");
     certificateno = cm.getColumnS("OLD_CERTIFICATE_FROM");
     distfrom1 = cm.getColumnS("DIST_FROM1");
     distto1 = cm.getColumnS("DIST_TO1");
     distfrom2 = cm.getColumnS("DIST_FROM2");
     distto2 = cm.getColumnS("DIST_TO2");
     distfrom3 = cm.getColumnS("DIST_FROM3");
     distto3 = cm.getColumnS("DIST_TO3");
     distfrom4 = cm.getColumnS("DIST_FROM4");
     distto4 = cm.getColumnS("DIST_TO4");
     tnofshares = cm.getColumnI("NO_OF_SHARES");
     issplit = cm.getColumnS("SPLIT");

     if (issplit.equalsIgnoreCase("T"))
     {
       proposedunits = cm.getColumnI("PROPOSED_UNITS");
     }

     if (String.valueOf(sfolio).equals("null"))
       sfolio = "";
     if (String.valueOf(bfolio).equals("null"))
       bfolio = "";
     if (String.valueOf(certificateno).equals("null"))
       certificateno = "";
     if (String.valueOf(distfrom1).equals("null"))
       distfrom1 = "";
     if (String.valueOf(distfrom2).equals("null"))
       distfrom2 = "";
     if (String.valueOf(distfrom3).equals("null"))
       distfrom3 = "";
     if (String.valueOf(distfrom4).equals("null"))
       distfrom4 = "";
     if (String.valueOf(distto1).equals("null"))
       distto1 = "";
     if (String.valueOf(distto2).equals("null"))
       distto2 = "";
     if (String.valueOf(distto3).equals("null"))
       distto3 = "";
     if (String.valueOf(distto4).equals("null"))
       distto4 = "";

     if (!distfrom1.equalsIgnoreCase(""))
     {
       int temp1 = 0;
       temp1 = Integer.parseInt(distto1);
       nofshares1 = temp1;
       temp1 = Integer.parseInt(distfrom1);
       nofshares1 = nofshares1 - temp1 + 1;
     }
     if (!distfrom2.equalsIgnoreCase(""))
     {
       int temp2 = 0;
       temp2 = Integer.parseInt(distto2);
       nofshares2 = temp2;
       temp2 = Integer.parseInt(distfrom2);
       nofshares2 = nofshares2 - temp2 + 1;
     }

     if (!distfrom3.equalsIgnoreCase(""))
     {
       int temp3 = 0;
       temp3 = Integer.parseInt(distto3);
       nofshares3 = temp3;
       temp3 = Integer.parseInt(distfrom3);
       nofshares3 = nofshares3 - temp3 + 1;
     }
     if (!distfrom4.equalsIgnoreCase(""))
     {
       int temp4 = 0;
       temp4 = Integer.parseInt(distto4);
       nofshares4 = temp4;
       temp4 = Integer.parseInt(distfrom4);
       nofshares4 = nofshares4 - temp4 + 1;
     }

     if (nofshares1 > 0)
     {
       Snofshares1 = String.valueOf(nofshares1);
     }
     if (nofshares2 > 0)
     {
       Snofshares2 = String.valueOf(nofshares2);
     }
     if (nofshares3 > 0)
     {
       Snofshares3 = String.valueOf(nofshares3);
     }
     if (nofshares4 > 0)
     {
       Snofshares4 = String.valueOf(nofshares4);
     }
  }

%>

<form action="UpdateSimpleTransfer.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Edit Simple Transfer</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Seller Folio</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="sfolio" value="<%=sfolio%>" class="SL2TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Old Certificate No.</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="certificateno" value="<%=certificateno%>" class="SL2TextField" onkeypress="keypressOnNumberFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Distinction(s)</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left">

         <table width="75%"  border="0" cellpadding="2">
           <tr>
            <td width="33%" align="center"><div align="center" class="style8"><b>From</b></div></th>
            <td width="33%" align="center"><div align="center" class="style8"><b>To</b></div></td>
            <td width="33%" align="center"><div align="center" class="style8"><b>No. of Shares</b></div></td>
           </tr>
           <tr>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="distfrom1" value="<%=distfrom1%>" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom1','distto1','nofshares1')"></div></td>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="distto1" value="<%=distto1%>" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom1','distto1','nofshares1')"></div></td>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="nofshares1" value="<%=Snofshares1%>" class="SL67TextField"  onkeypress="keypressOnNumberFld()" onfocus="this.blur()"></div></td>
           </tr>
           <tr>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="distfrom2" value="<%=distfrom2%>" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom2','distto2','nofshares2')"></div></td>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="distto2" value="<%=distto2%>" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom2','distto2','nofshares2')"></div></td>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="nofshares2" value="<%=Snofshares2%>" class="SL67TextField"  onkeypress="keypressOnNumberFld()" onfocus="this.blur()"></div></td>
           </tr>
           <tr>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="distfrom3" value="<%=distfrom3%>" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom3','distto3','nofshares3')"></div></td>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="distto3" value="<%=distto3%>" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom3','distto3','nofshares3')"></div></td>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="nofshares3" value="<%=Snofshares3%>" class="SL67TextField"  onkeypress="keypressOnNumberFld()" onfocus="this.blur()"></div></td>
           </tr>
           <tr>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="distfrom4" value="<%=distfrom4%>" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom4','distto4','nofshares4')"></div></td>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="distto4" value="<%=distto4%>" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom4','distto4','nofshares4')"></div></td>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="nofshares4" value="<%=Snofshares4%>" class="SL67TextField"  onkeypress="keypressOnNumberFld()" onfocus="this.blur()"></div></td>
           </tr>
        </table>

        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Total Shares to be Transferred </div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="tnofshares" value="<%=tnofshares%>" class="SL2TextField" onfocus="this.blur()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Buyer Folio </div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="bfolio" value="<%=bfolio%>" class="SL2TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;
          Split&nbsp;
          <%
            if (issplit.equalsIgnoreCase("T"))
            {
            %>
               <input type="checkbox" name="cansplit" checked="checked" value="checkbox" onclick="canwesplit()">
            <%
            }
            else
            {
            %>
               <input type="checkbox" name="cansplit" value="checkbox" onclick="canwesplit()">
            <%
            }
          %>
          </div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left" class="style8">
          <%
            if (issplit.equalsIgnoreCase("T"))
            {
            %>
             <SPAN id="punits">
               <input type="text" name="proposedunits" value="<%=proposedunits%>" class="SL2TextField" onkeypress="keypressOnNumberFld()">
               &nbsp;<b>(Proposed Unit)</b>
             </span>
            <%
            }
            else
            {
            %>
            <SPAN id="punits" style="display:none">
              <input type="text" name="proposedunits" value="<%=proposedunits%>" class="SL2TextField" onkeypress="keypressOnNumberFld()">
              &nbsp;<b>(Proposed Unit)</b>
            </span>
            <%
            }
          %>
            </div></td>
      </tr>

  </table>

  <!--HIDDEN FIELDS-->
  <input type="hidden" name="transferid" value="<%=ThisID%>">

  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="34%" scope="row">
            <div align="right">
                   <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="history.go(-1)" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
            </div></th>
        <td width="33%" align="center">
                  <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
        </td>
        <td width="33%">
	          <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
        </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
