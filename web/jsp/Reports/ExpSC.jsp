<script>
/*
'******************************************************************************************************************************************
'Script Author : Mohammad Ashraful Islam
'Creation Date : December 2006
'Page Purpose  : Exports Share Capital Report to Excel Format.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Market Comparison</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<%@ page language="java" import="org.apache.poi.hssf.usermodel.*,java.util.Vector,java.io.*,java.util.*,java.util.List,java.util.Vector" %>
<script language="javascript">
<!--
//Execute while click on Submit
function SubmitThis() {
  count = 0;

  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
   String TheDate = "";
   TheDate = String.valueOf(request.getParameter("dtselect"));
   String yearselect1 =TheDate.substring(6,10);

int yearselect=0;
yearselect=Integer.parseInt(yearselect1);
yearselect=yearselect-1;
String years="";
years=String.valueOf(yearselect);

String TheTotalSH = String.valueOf(request.getParameter("totalsh"));
String TheTotalTK = String.valueOf(request.getParameter("totaltk"));
String TheOtherSH = String.valueOf(request.getParameter("othersh"));
String TheOtherTK = String.valueOf(request.getParameter("othertk"));

int TSH=0;
int TTK=0;
int OSH=0;
int OTK=0;

TSH=Integer.parseInt(TheTotalSH);
TTK=Integer.parseInt(TheTotalTK);
OSH=Integer.parseInt(TheOtherSH);
OTK=Integer.parseInt(TheOtherTK);

    String query11 = "SELECT NAME,PERCENTAGE FROM SHAREHOLDING_REPORT WHERE NAME NOT LIKE 'Others' ORDER BY PERCENTAGE DESC";
    cm.queryExecute(query11);

     %>
      <script language="javascript">
       // alert("No Records with this combination exist in the Database!\nPlease check again!");
       // history.go(-1);
       </script>
     <%

          String shname = "";
          double shperc = 0;

          HSSFWorkbook wb = new HSSFWorkbook();
          HSSFSheet sheet = wb.createSheet("Share Capital Report");
          // Create a row and put some cells in it. Rows are 0 based.
           HSSFRow row = sheet.createRow((short)0);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue("");
           row.createCell((short)2).setCellValue("");
           row.createCell((short)3).setCellValue("");
           row.createCell((short)4).setCellValue("");
           row.createCell((short)5).setCellValue("");
           row.createCell((short)6).setCellValue(years);

           row = sheet.createRow((short)1);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)6).setCellValue("---------------");

           row = sheet.createRow((short)2);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue("ShareCapital");
           row.createCell((short)6).setCellValue("Taka");

          row = sheet.createRow((short)3);
          row.createCell((short)0).setCellValue("");
          row.createCell((short)1).setCellValue("Authorised:");
          row.createCell((short)6).setCellValue("---------------");

          row = sheet.createRow((short)4);
          row.createCell((short)0).setCellValue("");
          row.createCell((short)1).setCellValue(TheTotalSH);
          row.createCell((short)2).setCellValue("ordinary shares of Tk 10 each");
          row.createCell((short)6).setCellValue(TheTotalTK);
          //row.createCell((short)7).setCellValue("");

          row = sheet.createRow((short)5);
          row.createCell((short)0).setCellValue("");
          row.createCell((short)6).setCellValue("=========");

          row = sheet.createRow((short)6);
          row.createCell((short)0).setCellValue("");
          row.createCell((short)1).setCellValue("Issued, subscribed and paid up:");
          row.createCell((short)6).setCellValue("");

          row = sheet.createRow((short)7);
          row.createCell((short)0).setCellValue("");
          row.createCell((short)1).setCellValue("Issued for cash - ");
          row.createCell((short)6).setCellValue("");

          row = sheet.createRow((short)8);
          row.createCell((short)0).setCellValue("");
          row.createCell((short)1).setCellValue("3 ordinary shares of Tk 10 each");
          row.createCell((short)6).setCellValue("30");

          row = sheet.createRow((short)9);
          row.createCell((short)0).setCellValue("");
          row.createCell((short)1).setCellValue("");

          row = sheet.createRow((short)10);
          row.createCell((short)0).setCellValue("");
          row.createCell((short)1).setCellValue("Issued for cosideration other than cash - ");

          row = sheet.createRow((short)11);
          row.createCell((short)0).setCellValue("");
          row.createCell((short)1).setCellValue(TheOtherSH);
          row.createCell((short)2).setCellValue("ordinary shares of Tk 10 each");
          row.createCell((short)6).setCellValue(TheOtherTK);
          //row.createCell((short)7).setCellValue("");

          row = sheet.createRow((short)12);
          row.createCell((short)0).setCellValue("");
          row.createCell((short)6).setCellValue("---------------");


          row = sheet.createRow((short)13);
          row.createCell((short)0).setCellValue("");
          row.createCell((short)6).setCellValue(TheTotalTK);
          //row.createCell((short)7).setCellValue("");

          row = sheet.createRow((short)14);
          row.createCell((short)0).setCellValue("");
          row.createCell((short)6).setCellValue("=========");

          row = sheet.createRow((short)15);
          row.createCell((short)0).setCellValue("");
          row.createCell((short)1).setCellValue("");



          row = sheet.createRow((short)16);
          row.createCell((short)0).setCellValue("");
          row.createCell((short)1).setCellValue("The composition of shareholders");
          row.createCell((short)6).setCellValue(years);

          row = sheet.createRow((short)17);
          row.createCell((short)0).setCellValue("");
          row.createCell((short)6).setCellValue("---------------");

          row = sheet.createRow((short)18);
          row.createCell((short)0).setCellValue("");
          row.createCell((short)1).setCellValue("");


          double totalperc=0;
          int i=19;
          while(cm.toNext())
          {

            shname = cm.getColumnS("NAME");
            shperc = cm.getColumnF("PERCENTAGE");
            shperc = bcal.roundtovalue(shperc,2);





            totalperc=totalperc+shperc;

            HSSFRow rowi = sheet.createRow((short)i);

            rowi.createCell((short)0).setCellValue("");
            rowi.createCell((short)1).setCellValue(shname);
            rowi.createCell((short)6).setCellValue(shperc);
            rowi.createCell((short)7).setCellValue("%");
            i++;
             }

double otherperc=100-totalperc;
otherperc = bcal.roundtovalue(otherperc,2);
totalperc=totalperc+otherperc;
            row = sheet.createRow((short)25);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("Others");
            row.createCell((short)6).setCellValue(otherperc);
            row.createCell((short)7).setCellValue("%");

            row = sheet.createRow((short)26);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)6).setCellValue("---------------");

            row = sheet.createRow((short)27);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)6).setCellValue(totalperc);
            row.createCell((short)7).setCellValue("%");

            row = sheet.createRow((short)28);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)6).setCellValue("=========");

            row = sheet.createRow((short)29);
          row.createCell((short)0).setCellValue("");
          row.createCell((short)1).setCellValue("");


          row = sheet.createRow((short)30);
          row.createCell((short)0).setCellValue("");
          row.createCell((short)1).setCellValue("");


double totalsh1=0;
double totalsh2=0;
double totalsh3=0;
double totalsh4=0;
double totalsh5=0;
double totalsh6=0;
double totalsh7=0;
double totalsh8=0;
double totalsh9=0;
double totalsh10=0;

int noholder1=0;
int noholder2=0;
int noholder3=0;
int noholder4=0;
int noholder5=0;
int noholder6=0;
int noholder7=0;
int noholder8=0;
int noholder9=0;
int noholder10=0;

String query2="SELECT * FROM SHARE_CAPITAL_VIEW";
cm.queryExecute(query2);

while (cm.toNext())
{
totalsh1=cm.getColumnI("TOTAL1");
noholder1=cm.getColumnI("COUNT1");
totalsh2=cm.getColumnI("TOTAL2");
noholder2=cm.getColumnI("COUNT2");
totalsh3=cm.getColumnI("TOTAL3");
noholder3=cm.getColumnI("COUNT3");
totalsh4=cm.getColumnI("TOTAL4");
noholder4=cm.getColumnI("COUNT4");
totalsh5=cm.getColumnI("TOTAL5");
noholder5=cm.getColumnI("COUNT5");
totalsh6=cm.getColumnI("TOTAL6");
noholder6=cm.getColumnI("COUNT6");
totalsh7=cm.getColumnI("TOTAL7");
noholder7=cm.getColumnI("COUNT7");
totalsh8=cm.getColumnI("TOTAL8");
noholder8=cm.getColumnI("COUNT8");
totalsh9=cm.getColumnI("TOTAL9");
noholder9=cm.getColumnI("COUNT9");
totalsh10=cm.getColumnI("TOTAL10");
noholder10=cm.getColumnI("COUNT10");
}

double totalshare=totalsh1+totalsh2+totalsh3+totalsh4+totalsh5+totalsh6+totalsh7+totalsh8+totalsh9+totalsh10;
int totalholder=noholder1+noholder2+noholder3+noholder4+noholder5+noholder6+noholder7+noholder8+noholder9+noholder10;

double percsh1=totalsh1/TSH*100;
double percsh2=totalsh2/TSH*100;
double percsh3=totalsh3/TSH*100;
double percsh4=totalsh4/TSH*100;
double percsh5=totalsh5/TSH*100;
double percsh6=totalsh6/TSH*100;
double percsh7=totalsh7/TSH*100;
double percsh8=totalsh8/TSH*100;
double percsh9=totalsh9/TSH*100;
double percsh10=totalsh10/TSH*100;

percsh1 = bcal.roundtovalue(percsh1,2);
percsh2 = bcal.roundtovalue(percsh2,2);
percsh3 = bcal.roundtovalue(percsh3,2);
percsh4 = bcal.roundtovalue(percsh4,2);
percsh5 = bcal.roundtovalue(percsh5,2);
percsh6 = bcal.roundtovalue(percsh6,2);
percsh7 = bcal.roundtovalue(percsh7,2);
percsh8 = bcal.roundtovalue(percsh8,2);
percsh9 = bcal.roundtovalue(percsh9,2);
percsh10 = bcal.roundtovalue(percsh10,2);

double percshtotal=percsh1+percsh2+percsh3+percsh4+percsh5+percsh6+percsh7+percsh8+percsh9+percsh10;
percshtotal = bcal.roundtovalue(percshtotal,2);

            row = sheet.createRow((short)31);
            row.createCell((short)0).setCellValue("A distribution schedule of the above shares is given below as required by listing rules:");
            row.createCell((short)10).setCellValue("");

            row = sheet.createRow((short)32);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("");


            row = sheet.createRow((short)33);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("");

            row = sheet.createRow((short)34);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("");
            row.createCell((short)5).setCellValue("No. of");
            row.createCell((short)6).setCellValue("Total no.");
            row.createCell((short)7).setCellValue("Total");

            row = sheet.createRow((short)35);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("No. of shares");
            row.createCell((short)2).setCellValue("");
            row.createCell((short)5).setCellValue("holders");
            row.createCell((short)6).setCellValue("of shares");
            row.createCell((short)7).setCellValue("% holdings");

            row = sheet.createRow((short)36);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)5).setCellValue("---------------");
            row.createCell((short)6).setCellValue("---------------");
            row.createCell((short)7).setCellValue("---------------");


            row = sheet.createRow((short)37);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("Less than");
            row.createCell((short)2).setCellValue("500");
            row.createCell((short)3).setCellValue("shares");
            row.createCell((short)4).setCellValue("");
            row.createCell((short)5).setCellValue(noholder1);
            row.createCell((short)6).setCellValue(totalsh1);
            row.createCell((short)7).setCellValue(percsh1);
            row.createCell((short)8).setCellValue("%");

            row = sheet.createRow((short)38);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("500 to");
            row.createCell((short)2).setCellValue("5,000");
            row.createCell((short)3).setCellValue("shares");
            row.createCell((short)4).setCellValue("");
            row.createCell((short)5).setCellValue(noholder2);
            row.createCell((short)6).setCellValue(totalsh2);
            row.createCell((short)7).setCellValue(percsh2);
            row.createCell((short)8).setCellValue("%");

            row = sheet.createRow((short)39);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("5,001 to");
            row.createCell((short)2).setCellValue("10,000");
            row.createCell((short)3).setCellValue("shares");
            row.createCell((short)4).setCellValue("");
            row.createCell((short)5).setCellValue(noholder3);
            row.createCell((short)6).setCellValue(totalsh3);
            row.createCell((short)7).setCellValue(percsh3);
            row.createCell((short)8).setCellValue("%");

            row = sheet.createRow((short)40);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("10,001 to");
            row.createCell((short)2).setCellValue("20,000");
            row.createCell((short)3).setCellValue("shares");
            row.createCell((short)4).setCellValue("");
            row.createCell((short)5).setCellValue(noholder4);
            row.createCell((short)6).setCellValue(totalsh4);
            row.createCell((short)7).setCellValue(percsh4);
            row.createCell((short)8).setCellValue("%");

            row = sheet.createRow((short)41);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("20,001 to");
            row.createCell((short)2).setCellValue("30,000");
            row.createCell((short)3).setCellValue("shares");
            row.createCell((short)4).setCellValue("");
            row.createCell((short)5).setCellValue(noholder5);
            row.createCell((short)6).setCellValue(totalsh5);
            row.createCell((short)7).setCellValue(percsh5);
            row.createCell((short)8).setCellValue("%");

            row = sheet.createRow((short)42);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("30,001 to");
            row.createCell((short)2).setCellValue("40,000");
            row.createCell((short)3).setCellValue("shares");
            row.createCell((short)4).setCellValue("");
            row.createCell((short)5).setCellValue(noholder6);
            row.createCell((short)6).setCellValue(totalsh6);
            row.createCell((short)7).setCellValue(percsh6);
            row.createCell((short)8).setCellValue("%");

            row = sheet.createRow((short)43);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("40,001 to");
            row.createCell((short)2).setCellValue("50,000");
            row.createCell((short)3).setCellValue("shares");
            row.createCell((short)4).setCellValue("");
            row.createCell((short)5).setCellValue(noholder7);
            row.createCell((short)6).setCellValue(totalsh7);
            row.createCell((short)7).setCellValue(percsh7);
            row.createCell((short)8).setCellValue("%");

            row = sheet.createRow((short)44);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("50,001 to");
            row.createCell((short)2).setCellValue("1,00,000");
            row.createCell((short)3).setCellValue("shares");
            row.createCell((short)4).setCellValue("");
            row.createCell((short)5).setCellValue(noholder8);
            row.createCell((short)6).setCellValue(totalsh8);
            row.createCell((short)7).setCellValue(percsh8);
            row.createCell((short)8).setCellValue("%");

            row = sheet.createRow((short)45);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("1,00,001 to");
            row.createCell((short)2).setCellValue("1,000,000");
            row.createCell((short)3).setCellValue("shares");
            row.createCell((short)4).setCellValue("");
            row.createCell((short)5).setCellValue(noholder9);
            row.createCell((short)6).setCellValue(totalsh9);
            row.createCell((short)7).setCellValue(percsh9);
            row.createCell((short)8).setCellValue("%");

            row = sheet.createRow((short)46);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("Over");
            row.createCell((short)2).setCellValue("1,000,000");
            row.createCell((short)3).setCellValue("shares");
            row.createCell((short)4).setCellValue("");
            row.createCell((short)5).setCellValue(noholder10);
            row.createCell((short)6).setCellValue(totalsh10);
            row.createCell((short)7).setCellValue(percsh10);
            row.createCell((short)8).setCellValue("%");

            row = sheet.createRow((short)47);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("");
            row.createCell((short)5).setCellValue("---------------");
            row.createCell((short)6).setCellValue("---------------");
            row.createCell((short)7).setCellValue("---------------");

            row = sheet.createRow((short)48);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("");

            row = sheet.createRow((short)49);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("Total");
            row.createCell((short)5).setCellValue(totalholder);
            row.createCell((short)6).setCellValue(totalshare);
            row.createCell((short)7).setCellValue(percshtotal);

            row = sheet.createRow((short)50);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("");
            row.createCell((short)5).setCellValue("=========");
            row.createCell((short)6).setCellValue("=========");
            row.createCell((short)7).setCellValue("=========");

// Write the output to a file
             String savepath="";
             ServletContext sc = getServletConfig().getServletContext();
          //Code for creating a folder
           String path=getServletContext().getRealPath("/");
           File f1 = new File(path + "/Excel Export");
           if(!f1.exists())
           {
                 f1.mkdir();
           }
           savepath = sc.getRealPath("/") +"Excel Export/"+"ShareCapitalReport.xls";

                FileOutputStream fileOut = new FileOutputStream(savepath);
                wb.write(fileOut);
                fileOut.close();
                fileOut=null;
                sheet=null;
                wb=null;
         //End Excel Export
        String tempURL =  request.getContextPath() +"/Excel Export/"+"ShareCapitalReport.xls";

        %>
<script language="javascript">
    showPrintPreview6('<%=tempURL%>');
    location = "<%=request.getContextPath()%>/jsp/Reports/ExportShareCapital.jsp";
</script>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
