<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Updated by    : Mohammad ashraful Islam in December 2006
'Updated by    : kamruzzaman
'Creation Date : September 2005
'Page Purpose  : Generating Month Shareholding Data.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Generate - Monthly Shareholding Data</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
function tonewdata()
{
  if(confirm("Do You really want to generate Monthly Shareholding data?"))
  {
    location = "<%=request.getContextPath()%>/jsp/Reports/NewMonthlySh.jsp"
  }
}
</script>

<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String query1 = "SELECT DISTINCT DEC_DATE FROM MONTHLYSHAREHOLDING_VIEW ORDER BY DEC_DATE DESC";

    int count=cm.queryExecuteCount(query1);
    cm.queryExecute(query1);
    cm.toNext();

    int c=0;
    String decdate="";
    String periodfrom = "";

    decdate = cm.getColumnDT("DEC_DATE");

%>
  <span class="style7">
  <form name="FormToSubmit" method="GET" action="RejectDeclaredDividends.jsp">
   <img name="B4" src="<%=request.getContextPath()%>/images/btnNew.gif" onclick="tonewdata()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNewOn.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNew.gif'">
  <table width="100%" BORDER=1  cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Generate - Monthly Shareholding Data</center></td></tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="34%" class="style9"><div align="center">Date</div></td>
    <td width="33%"><div align="center" class="style9">
      <div align="center">Period From</div>
    </div></td>
    <td width="33%"><div align="center" class="style9">
      <div align="center">Period To</div>
    </div></td>
  </tr>
  <div align="left">
     <%
       while (cm.toNext() && c<count)
       {
         periodfrom=cm.getColumnDT("DEC_DATE");
         %>
    </div>
  <tr bgcolor="#E8F3FD">
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=decdate%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=periodfrom%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">&nbsp;&nbsp;<%=decdate%>&nbsp;</div>
           </div></td>
         </tr>
             <%
         decdate=periodfrom;
           c++;
        }
               periodfrom="";
         %>

  <tr bgcolor="#E8F3FD">
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=decdate%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=periodfrom%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">&nbsp;&nbsp;<%=decdate%>&nbsp;</div>
           </div></td>
         </tr>
         </table>
       </form>
     <%
   if (isConnect)
   {
     cm.takeDown();
   }
  %>
</body>
</html>
