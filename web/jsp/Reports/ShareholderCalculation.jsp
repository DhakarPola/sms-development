<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : December 2006
'Page Purpose  : For Calculation of Shareholder Report
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Sahre Calculation</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   if(isConnect==false||isConnect1==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
     String selectDate = String.valueOf(request.getParameter("dateselect"));
     String report=String.valueOf(request.getParameter("report"));
     String perS=String.valueOf(request.getParameter("pshare"));
     //System.out.println("perCent= "+perS);
     double perD=Double.parseDouble(perS);
     //System.out.println("perD= "+perD);

     String deletetable="call DELETE_SH_RPT()";
    // System.out.println(deletetable);
     boolean b=cm.procedureExecute(deletetable);
    // System.out.println("delet="+b);

     double bo_total=0;
     double sh_total=0;
     double totalshare=0.0;
     double share=0.0;
     double percentage=0.0;
     double s=0.0;
     double ts=0.0;
     double  less10_total=0.0;
     double totalshare1=0.0;
     int count=0;
     int Intshare=0;
     String Stotalshare="";
     int totals=0;
     int t1=0;
     int t2=0;
     boolean b1=false;
     double temptotal=0.0;

     String Name="";
     String folio_no="";
     String shares="";
     String percent="";
     String category="";
     String stop="No";
     String setup="";

     String query="select sum(CURRBAL) total1 from BOHOLDING";
     cm.queryExecute(query);
     cm.toNext();
     bo_total=cm.getColumnF("total1");
     t1=cm.getColumnI("total1");
     String query1="select sum(TOTAL_SHARE) total2 from SHAREHOLDER where ACTIVE='T'";
     cm.queryExecute(query1);
     cm.toNext();
     sh_total=cm.getColumnF("total2");
     t2=cm.getColumnI("total2");
     totalshare = bo_total + sh_total;

     totals=t1+t2;

     Stotalshare=String.valueOf(totals);

     //System.out.println("totaSahre="+totalshare);
     totalshare1=totalshare/perD;
     //System.out.println("totalShare1= "+totalshare1);

     String query2="select sum(TOTAL_SHARE) total3 from SHAREHOLDER where TOTAL_SHARE< " + totalshare1 + " and ACTIVE='T'";
     cm.queryExecute(query2);
     cm.toNext();
     less10_total=cm.getColumnF("total3");
    // System.out.println(less10_total);

     String selectquery="select  NAME,FOLIO_NO,TOTAL_SHARE  from shareholder  order by TOTAL_SHARE DESC";
     //System.out.println(selectquery);
     cm.queryExecute(selectquery);

     count=0;
     double share_withoutOthers=0.0;
     int total_share_temp=0;
     while((cm.toNext())&& (stop.equals("No")))
     {
       Name=String.valueOf(cm.getColumnS("NAME"));
       folio_no=String.valueOf(cm.getColumnS("FOLIO_NO"));
       share=cm.getColumnF("TOTAL_SHARE");
       Intshare=cm.getColumnI("TOTAL_SHARE");
       percentage=(share/totalshare)*100.0;
       percentage=bcal.roundtovalue(percentage,2);
       if(folio_no.equals("26000010"))
       {
         category="Parent";
       }
       else if(percentage>=perD)
       {
         category="Greater then 10";
         //System.out.println("inside");
       }
       else
       {
         category="Less then 10";
         count++;
         if(count==5)
         {
           Name="Others";
           stop="yes";
           less10_total=totalshare-share_withoutOthers;
           //System.out.println("share_withoutOthers= "+share_withoutOthers);
           //Intshare=(int)less10_total;
           percentage=(less10_total/totalshare)*100.0;
           percentage=bcal.roundtovalue(percentage,2);
           Intshare=totals-total_share_temp;
          }
       }
       if(count<=4)
       {
          share_withoutOthers=share_withoutOthers+share;
          total_share_temp=total_share_temp+Intshare;
       }
       Name=cm.replace(Name,"'","''");
       folio_no=cm.replace(folio_no,"'","''");
       Name=cm.replace(Name,"'","''");

       shares=String.valueOf(Intshare);
       percent=String.valueOf(percentage);

       shares=cm.replace(shares,"'","''");

       if(!folio_no.equals("null"))
       {
         if(Name.equals("null"))
         Name="";
         if(shares.equals("null"))
         shares="";
         if(percent.equals("null"))
         percent="";
         if(category.equals("null"))
         category="";
         Name=Name.trim();
         folio_no=folio_no.trim();
         shares=shares.trim();
         percent=percent.trim();
         category=category.trim();

         setup="call CREATE_SH_RPT('"+ Name +"','"+ folio_no +"','"+ shares +"','"+ percent +"','" + category +"')";
         b1=cm1.procedureExecute(setup);
        // System.out.println(setup);
        // System.out.println(b1);
       }
     }
     if(isConnect)
     {
       cm.takeDown();
     }
     if(isConnect1)
     {
       cm1.takeDown();
     }
   %>

<script language="javascript">
   <%
   if(report.equals("1"))
   {
     %>

     location = "<%=request.getContextPath()%>/ExportSharholderReport.jsp?selectdate=<%=selectDate%>&percent=<%=perS%>";

    <%
   }
   else if(report.equals("2"))
   {
   %>

     location = "<%=request.getContextPath()%>/jsp/Reports/seeShareholderReport.jsp?total_share=<%=Stotalshare%>&percent=<%=perS%>";

     <%
    }
   %>
</script>

</body>
</html>
