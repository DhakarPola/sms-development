<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : August 2006
'Page Purpose  : Populates the Share Register Temp Table.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.util.Date,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="cm2"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Share Register Temp</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   boolean isConnect2 = cm2.connect();
   if((isConnect==false) || (isConnect1==false) || (isConnect2==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

  String FolioNo = String.valueOf(request.getParameter("sfoliono"));
  String altdate = "";
  String certno = "";
  int nofshares = 0;
  int distfrom = 0;
  int distto = 0;
  String buyerfolio = "";
  String thiscertno = "";

  if (String.valueOf(FolioNo).equals("null"))
   FolioNo = "";

  String sreg = "call DELETE_SHARE_REGISTER()";
  boolean ub = cm.procedureExecute(sreg);

  String query3 = "SELECT * FROM TRANSFER_VIEW WHERE SELLER_FOLIO = '" + FolioNo + "'";
  cm.queryExecute(query3);

   while(cm.toNext())
   {
     buyerfolio = cm.getColumnS("BUYER_FOLIO");
     certno = cm.getColumnS("CERTIFICATE_NO");
     altdate = cm.getColumnDT("TRANSFER_DATE");

     if (certno.length() > 9)
     {
       StringTokenizer CertSTK = new StringTokenizer(certno,",");

       while (CertSTK.hasMoreTokens())
       {
         thiscertno = CertSTK.nextToken();
         thiscertno = thiscertno.trim();

         if (thiscertno.length() > 8)
         {
           String query3B = "SELECT * FROM SCRIPTS_VIEW WHERE CERTIFICATE_NO = '" + thiscertno + "'";
           cm1.queryExecute(query3B);

           while(cm1.toNext())
           {
            distfrom = cm1.getColumnI("DIST_FROM");
            distto = cm1.getColumnI("DIST_TO");

            nofshares = distto - distfrom + 1;

            String sregister = "call ADD_SHARE_REGISTER('" + thiscertno + "', '" + buyerfolio + "', '" + altdate + "', " + distfrom + ", " + distto + "," + nofshares + ")";
            boolean bB2 = cm2.procedureExecute(sregister);
           }
         }
       }
     }
     else
     {
       String query3A = "SELECT * FROM SCRIPTS_VIEW WHERE CERTIFICATE_NO = '" + certno + "'";
       cm1.queryExecute(query3A);

       while(cm1.toNext())
       {
         distfrom = cm1.getColumnI("DIST_FROM");
         distto = cm1.getColumnI("DIST_TO");

         nofshares = distto - distfrom + 1;

         String sregister = "call ADD_SHARE_REGISTER('" + certno + "', '" + buyerfolio + "', '" + altdate + "', " + distfrom + ", " + distto + "," + nofshares + ")";
         boolean bB2 = cm2.procedureExecute(sregister);
       }
     }
   }

   if (isConnect)
   {
     cm.takeDown();
     cm1.takeDown();
     cm2.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/ShareRegisterReport.jsp?sfoliono=<%=FolioNo%>";
</script>
</body>
</html>
