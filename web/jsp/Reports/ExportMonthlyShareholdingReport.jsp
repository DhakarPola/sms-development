<script>
/*
'******************************************************************************************************************************************
'Script Author : Md.Kamruzzaman
'Creation Date : December 2006
'Page Purpose  : Exports Monthly Shareholding Report to Excel Format.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cms"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Monthly Shareholding Report</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<%@ page language="java" import="java.lang.Math.*,org.apache.poi.hssf.usermodel.*,java.util.Vector,java.io.*,java.util.*,java.util.List,java.util.Vector" %>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1=cms.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
           String name="";
           String folio="";
           int totalshare=0;
           int totalshare_p=0;
           double percent=0.0;
           int totalshare_l=0;
           int totalshare_pl=0;
           double percentl=0.0;
           String name_l="";
           int less_count=0;
           int totalshare_less10=0;
           int totalshare_less10_p=0;
           String folio_l="";
           double totalpercentage=0.0;


           String q1="select to_char(sysdate,'fmddth Month YYYY') d from dual";
           cm.queryExecute(q1);
           cm.toNext();
           String sysdate=cm.getColumnS("d");
           //System.out.println(sysdate);
           String to_date=request.getParameter("dateselect_to");
           String from_date=request.getParameter("datselect_from");
           //System.out.println("report page");
           //System.out.println(to_date);
           //System.out.println(from_date);
            int givenPercent=0;
            String per="select distinct percent_sh  from MONTHLYSHAREHOLDING where DEC_DATE=TO_DATE('" + to_date + "','DD/MM/YYYY')";
            try
            {
              cm.queryExecute(per);
              cm.toNext();
              givenPercent=cm.getColumnI("percent_sh");
            }
            catch(Exception e){}

           String q2="select to_char(TO_DATE('"+ to_date +"','DD/MM/YYYY'),'fmdd Month YYYY') d1 from dual";
           cm.queryExecute(q2);
           cm.toNext();
           String t_date=cm.getColumnS("d1");

           String q3="select to_char(TO_DATE('"+ from_date +"','DD/MM/YYYY'),'fmddth Month YYYY') d2 from dual";
           cm.queryExecute(q3);
           cm.toNext();
           String f_date=cm.getColumnS("d2");


           HSSFWorkbook wb = new HSSFWorkbook();
           HSSFSheet sheet = wb.createSheet("Monthly Shareholding Report");
          // Create a row and put some cells in it. Rows are 0 based.
           int i=1;
           HSSFRow row = sheet.createRow((short)i);

           row.createCell((short)0).setCellValue(sysdate);
           i+=3;

           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("The Executive Director");
           i++;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("Securities and Exchange Commission");
           i++;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("Jiban Bima Tower (15, 16 & 20 Flr)");
           i++;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("10 Dilkusha C/A, Dhaka-1000");
           i++;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("Fax : 9563721");
           i+=2;

           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("Dear Sir,");
           i+=2;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("Subject: Shareholding Position on "+t_date);
           i+=2;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("Please be Referred to your letter SEC/SRMID/2004-08/1116-239 dated "+f_date+", the share");
           i++;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("holding position of our Company is mentioned bellow:");
           i+=3;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("1 Shareholders Position on "+t_date);
           i++;
           q1="select DISTINCT GRAND_TOTAL from MONTHLYSHAREHOLDING_VIEW where DEC_DATE=TO_DATE('" + to_date +"','DD/MM/YYYY')";
           //System.out.println("q1= "+q1);
           cm.queryExecute(q1);
           cm.toNext();
           int share=cm.getColumnI("GRAND_TOTAL");
           //System.out.println("share="+share);

         // double share_d=cm.getColumnF("GRAND_TOTAL");
           int capital=share*10;

           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("2 Paip-up Capital (Tk.) "+capital);
           i++;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("3 Total Number of Shares "+share);
           i+=2;

           //data for parent
           String parent="select * from MONTHLYSHAREHOLDING_VIEW where DEC_DATE=TO_DATE('"+ to_date +"','DD/MM/YYYY') and FOLIO_NO='26000010'";
          //String parent="select * from MONTHLYSHAREHOLDING_VIEW where DEC_DATE=TO_DATE('"+ to_date +"','DD/MM/YYYY') and TOTAL_SHARE=max(TOTAL_SHARE)";
          //System.out.println("parent="+parent);
           cm.queryExecute(parent);
           cm.toNext();
           name=cm.getColumnS("SH_NAME");
           name=name.trim();
           folio=cm.getColumnS("FOLIO_NO");
           folio=folio.trim();
           totalshare=cm.getColumnI("TOTAL_SHARE");
           String stotalshare=String.valueOf(totalshare);
           String parent_p="select * from MONTHLYSHAREHOLDING_VIEW where DEC_DATE=TO_DATE('"+ from_date +"','DD/MM/YYYY') and FOLIO_NO='" + folio + "'";
           //System.out.println("parent_p= "+parent_p);
           cm.queryExecute(parent_p);
           cm.toNext();
           totalshare_p=cm.getColumnI("TOTAL_SHARE");
           String spercent="";
           String stotalshare_p=String.valueOf(totalshare_p);
           if(totalshare_p>0)
           {
              double temp_dif=(double)(totalshare-totalshare_p);
              if(temp_dif<0.0)
              temp_dif=temp_dif*(-1);
              percent=(double)(temp_dif/totalshare_p)*100.0;
              percent=bcal.roundtovalue(percent,2);
              spercent=String.valueOf(percent);
           }
           else
           spercent="-";

             row=sheet.createRow((short)i);
             row.createCell((short)0).setCellValue("4 Particulars of Sponsor holding Shares");
             row.createCell((short)6).setCellValue(to_date);
             row.createCell((short)8).setCellValue(from_date);
             row.createCell((short)9).setCellValue("Change");
             row.createCell((short)11).setCellValue("Remarks");
             i++;
             row=sheet.createRow((short)i);
             row.createCell((short)0).setCellValue("    SI.");
             row.createCell((short)1).setCellValue("Folio No.");
             row.createCell((short)2).setCellValue("Name of the Sponsor");
             row.createCell((short)6).setCellValue("No.of shares ");
             row.createCell((short)8).setCellValue("No.of shares ");
             row.createCell((short)9).setCellValue("   %");
             row.createCell((short)10).setCellValue("(As per SEC/SRMID/2000-953/313/Admin-06)");
             i++;
             if(stotalshare.equals("0"))
             stotalshare="-";
             if(stotalshare_p.equals("0"))
             stotalshare_p="-";
             if(spercent.equals("0.0"))
             spercent="-";
             row=sheet.createRow((short)i);
             row.createCell((short)0).setCellValue("     1");
             row.createCell((short)1).setCellValue("26000010");
             row.createCell((short)2).setCellValue(name+" (not in CDS)");
             row.createCell((short)6).setCellValue(stotalshare);
             row.createCell((short)8).setCellValue(stotalshare_p);
             row.createCell((short)9).setCellValue(spercent);
             row.createCell((short)10).setCellValue("As per Company's Share Register");
             i++;
             row=sheet.createRow((short)i);
             row.createCell((short)0).setCellValue("");
             row.createCell((short)1).setCellValue("");
             row.createCell((short)4).setCellValue("   Sub Total of A");
             row.createCell((short)6).setCellValue(stotalshare);
             row.createCell((short)8).setCellValue(stotalshare_p);
             row.createCell((short)9).setCellValue(spercent);
             row.createCell((short)10).setCellValue("");

           i+=2;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("5 Particulars of "+givenPercent+"% or above "+givenPercent+"% holding shares");
           row.createCell((short)6).setCellValue(to_date);
           row.createCell((short)8).setCellValue(from_date);
           row.createCell((short)9).setCellValue("Change");
           row.createCell((short)11).setCellValue("Remarks");
           i++;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("  SI.");
           row.createCell((short)1).setCellValue("Folio/BO");
           row.createCell((short)2).setCellValue("Name of the shareholders");
           row.createCell((short)6).setCellValue("No.of shares ");
           row.createCell((short)8).setCellValue("No.of shares ");
           row.createCell((short)9).setCellValue("   %");
           row.createCell((short)10).setCellValue("(As per Substantial Acquisition Rule 2002)");
           i+=2;
           //dynamic
           int serial=1;
           String Spercent="";
           String more10="select *  from MONTHLYSHAREHOLDING_VIEW where DEC_DATE=TO_DATE('"+ to_date +"','DD/MM/YYYY') and FOLIO_NO not like '26000010'";
           //String more10="select *  from MONTHLYSHAREHOLDING_VIEW where DEC_DATE=TO_DATE('"+ to_date +"','DD/MM/YYYY') and TOTAL_SHARE!=max(TOTAL_SHARE)";
           //System.out.println("more10= "+more10);
           try
           {
             cm.queryExecute(more10);
             while( cm.toNext())
             {
               name_l=cm.getColumnS("SH_NAME");
               name_l=name_l.trim();
               folio_l=cm.getColumnS("FOLIO_NO");
               folio_l=folio_l.trim();
               totalshare_l=cm.getColumnI("TOTAL_SHARE");
               totalshare_less10=totalshare_less10+totalshare_l;
               String more10p="select TOTAL_SHARE from MONTHLYSHAREHOLDING_VIEW where DEC_DATE=TO_DATE('"+ from_date +"','DD/MM/YYYY') and FOLIO_NO='"+ folio_l+"'";
               //System.out.println("more10p= "+more10p);
               int found=0;
               found=cms.queryExecuteCount(more10p);
               //System.out.println("found= "+found);
               //System.out.println("After more10p= "+more10p);
               totalshare_pl=0;
               percentl=0.0;
               if(found>0)
               {
                 cms.queryExecute(more10p);
                 cms.toNext();
                 totalshare_pl=cms.getColumnI("TOTAL_SHARE");
              }
              //System.out.println("totalshare_pl= "+totalshare_pl);
              totalshare_less10_p=totalshare_less10_p+totalshare_pl;
             // System.out.println("totalshare_less10_p= "+totalshare_less10_p);
              double temp_dif=(double)(totalshare_l-totalshare_pl);
              if(temp_dif<0.0)
              temp_dif=temp_dif*(-1);
              if(totalshare_pl!=0)
              percentl=(double)(temp_dif/totalshare_pl)*100.0;
              percentl=bcal.roundtovalue(percentl,2);
              totalpercentage=totalpercentage+percentl;
              Spercent=String.valueOf(percentl);

               String Sserial=String.valueOf(serial);
               String Stotalshare_l=String.valueOf(totalshare_l);
               String Stotalshare_pl=String.valueOf(totalshare_pl);
               if(Stotalshare_l.equals("0"))
               Stotalshare_l="-";
               if(Stotalshare_pl.equals("0"))
               Stotalshare_pl="-";
               if(Spercent.equals("0.0"))
               Spercent="-";
               row=sheet.createRow((short)i);
               row.createCell((short)0).setCellValue(Sserial);
               row.createCell((short)1).setCellValue(folio_l);
               row.createCell((short)2).setCellValue(name_l+" (including CDS)");
               row.createCell((short)6).setCellValue(Stotalshare_l);
               row.createCell((short)8).setCellValue(Stotalshare_pl);
               row.createCell((short)9).setCellValue(Spercent);
               row.createCell((short)10).setCellValue("CDBL+Company's share Register");
               i++;
               serial++;
             }
           }catch(Exception e){}
             //String Stotalpercentage=String.valueOf(totalpercentage);
             double total_percentage=0.0;
             if(totalshare_less10_p!=0)
             {
               double temp_diff=(double)(totalshare_less10-totalshare_less10_p);
               if(temp_diff<0)
               temp_diff=temp_diff*(-1);
               total_percentage=(double)(temp_diff/totalshare_less10_p)*100.0;
               total_percentage=bcal.roundtovalue(total_percentage,2);
             }
             String Stotalpercentage=String.valueOf(total_percentage);
             if(Stotalpercentage.equals("0.0"))
             Stotalpercentage="-";
             String Stotalshare_less10=String.valueOf(totalshare_less10);
             String Stotalshare_less10_p=String.valueOf(totalshare_less10_p);

             row=sheet.createRow((short)i);
             row.createCell((short)0).setCellValue("");
             row.createCell((short)1).setCellValue("");
             row.createCell((short)4).setCellValue("    Sub Total of B");
             row.createCell((short)6).setCellValue(Stotalshare_less10);
             row.createCell((short)8).setCellValue(Stotalshare_less10_p);
             row.createCell((short)9).setCellValue(Stotalpercentage);
             row.createCell((short)10).setCellValue("");
             i+=3;
             row=sheet.createRow((short)i);
             row.createCell((short)0).setCellValue("Yours Faithfully,");
             i++;
             row=sheet.createRow((short)i);
             row.createCell((short)0).setCellValue("British American Tobacco Bangladesh Co. Ltd.");
             i+=4;
             row=sheet.createRow((short)i);
             row.createCell((short)0).setCellValue("Md. Azizur Rahman FCS");
             i++;
             row=sheet.createRow((short)i);
             row.createCell((short)0).setCellValue("Deputy Company Secretary");
             i++;

          // Write the output to a file
             String savepath="";
             ServletContext sc = getServletConfig().getServletContext();

             //Code for creating a folder
           String path=getServletContext().getRealPath("/");
           File f1 = new File(path + "/Excel Export");
           if(!f1.exists())
           {
                 f1.mkdir();
           }
           savepath = sc.getRealPath("/") +"Excel Export/"+"MonthlyShareholdingReport.xls";

                FileOutputStream fileOut = new FileOutputStream(savepath);
                wb.write(fileOut);
                fileOut.close();
                fileOut=null;
                sheet=null;
                wb=null;
         //End Excel Export
        String tempURL =  request.getContextPath() +"/Excel Export/"+"MonthlyShareholdingReport.xls";
        %>
 <script language="javascript">
     showPrintPreview6('<%=tempURL%>');
    location = "<%=request.getContextPath()%>/jsp/Reports/ExportMonthlySharholdingData.jsp";
 </script>

<%
   if (isConnect)
   {
     cm.takeDown();
     cms.takeDown();
   }
%>
</body>
</html>
