<script>
/*
'******************************************************************************************************************************************
'Script Author : Mohammad Ashraful Islam
'Creation Date : December 2006
'Page Purpose  : Share Capital Calculation.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}
%>
<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Share Capital Calculation</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>
<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
String ShareCategory="None";

String DateSelect = String.valueOf(request.getParameter("dateselect"));
DateSelect=cm.replace(DateSelect,"'","''");

String query2="SELECT A.*,B.* FROM (SELECT SUM(TOTAL_SHARE) T1 FROM SHAREHOLDER WHERE ACTIVE='T') A, (SELECT SUM(CURRBAL) T2 FROM BOHOLDING) B";
cm.queryExecute(query2);
cm.toNext();

double total1=cm.getColumnI("T1");
double total2=cm.getColumnI("T2");

double grandtotalSH=total1+total2;
double grandtotalTK=grandtotalSH*10;

double ordinarySH=3;
double ordinaryTK=30;

double otherthanSH=grandtotalSH-ordinarySH;
double otherthanTK=grandtotalTK-ordinaryTK;

String query1 = "SELECT NAME,FOLIO_NO,MAX(TOTAL_SHARE) TOTAL1 FROM SHAREHOLDER GROUP BY NAME,FOLIO_NO ORDER BY TOTAL1 DESC";
cm.queryExecute(query1);

int i=0;

double sharesum[];
sharesum=new double[100];

String[] shname;
shname=new String[100];

String[] foliono;
foliono=new String[100];

while(cm.toNext() && i<=5)
{
shname[i]=cm.getColumnS("NAME");
sharesum[i]=cm.getColumnI("TOTAL1");
foliono[i]=cm.getColumnS("FOLIO_NO");
shname[i]=cm.replace(shname[i],"'","''");
i++;
}

String query4 = "SELECT BOSHORTNAME,BOID,MAX(CURRBAL) TOTAL2 FROM BOHOLDING GROUP BY BOSHORTNAME,BOID HAVING MAX(CURRBAL)>'" + sharesum[5] + "'ORDER BY TOTAL2 DESC";

int count=0;
count=cm.queryExecuteCount(query4);

cm.queryExecute(query4);

int j=0;

while(cm.toNext() && j<count)
{
shname[j+6]=cm.getColumnS("BOSHORTNAME");
sharesum[j+6]=cm.getColumnI("TOTAL2");
foliono[j+6]=cm.getColumnS("BOID");
shname[j+6]=cm.replace(shname[j+6],"'","''");
j++;
}

int a=0;
int b=0;
double temp=0;
int x=6+count;

String temp1="";
String temp2="";

for(a=0;a<x;a++)
{
for(b=0;b<x-a;b++)
{
if(sharesum[b]<sharesum[b+1])
{
temp=sharesum[b];
sharesum[b]=sharesum[b+1];
sharesum[b+1]=temp;

temp1=shname[b];
shname[b]=shname[b+1];
shname[b+1]=temp1;

temp2=foliono[b];
foliono[b]=foliono[b+1];
foliono[b+1]=temp2;
}
}
}

int m=0;
int z=0;

double shareperc[];
shareperc=new double[10];

for(z=0;z<6;z++)
{
shareperc[z]=(sharesum[z]/grandtotalSH)*100;
shareperc[z]=cm1.roundtovalue(shareperc[z],2);
}

shname[6]="Others";
sharesum[6]=grandtotalSH-(sharesum[0]+sharesum[1]+sharesum[2]+sharesum[3]+sharesum[4]+sharesum[5]);
shareperc[6]=100-(shareperc[0]+shareperc[1]+shareperc[2]+shareperc[3]+shareperc[4]+shareperc[5]);
shareperc[6]=cm1.roundtovalue(shareperc[6],2);
foliono[6]="N/A";

String deltable = "call DELETE_SH_RPT()";
boolean ub = cm.procedureExecute(deltable);

int GrandtotalSH=(int) grandtotalSH;
int GrandtotalTK=(int) grandtotalTK;
int OrdinarySH=(int) ordinarySH;
int OrdinaryTK=(int) ordinaryTK;
int OtherthanSH=(int) otherthanSH;
int OtherthanTK=(int) otherthanTK;

for(m=0;m<7;m++)
{
String addtable = "call CREATE_SH_RPT('" + shname[m] + "','" + foliono[m] + "','" + sharesum[m] + "','" + shareperc[m] + "','" + ShareCategory + "')";
boolean ub1 = cm.procedureExecute(addtable);
}
String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Generated Share Capital Report')";
boolean ub2 = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
<%

String locator = String.valueOf(request.getParameter("sharecapital"));
if(locator.equals("1"))
{
  %>
location = "<%=request.getContextPath()%>/jsp/Reports/ExpSC.jsp?dtselect=<%=DateSelect%>&totalsh=<%=GrandtotalSH%>&totaltk=<%=GrandtotalTK%>&othersh=<%=OtherthanSH%>&othertk=<%=OtherthanTK%>";
<%
}
else {
%>
location = "<%=request.getContextPath()%>/SCReport.jsp?dtselect=<%=DateSelect%>";
<%
}
%>
</script>
</body>
</html>
