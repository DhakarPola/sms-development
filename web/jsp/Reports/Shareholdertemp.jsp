<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : December 2006
'Page Purpose  : Temporary Monthly shareholding.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<%@ page language="java" import="org.apache.poi.hssf.usermodel.*,java.util.Vector,java.io.*,java.util.*,java.util.List,java.util.Vector" %>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Shareholder temp</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
     int count=0;
     String to_date=String.valueOf(request.getParameter("dateselect_to"));
     String from_date=String.valueOf(request.getParameter("dateselect_from"));
     String q1= "Select sysdate from dual where TO_DATE('"+ from_date +"','DD/MM/YYYY')>TO_DATE('"+ to_date +"','DD/MM/YYYY')";
     cm.queryExecute(q1);
     while(cm.toNext())
     {
       count++;
     }
    if(to_date.equals(from_date))
     {
     %>
       <script language="javascript">
       alert("Date To and Date From cann't be same.");
       location = "<%=request.getContextPath()%>/jsp/Reports/ExportMonthlySharholdingData.jsp";
       </script>
     <%
     }
      else if(count>0)
     {
     %>
       <script language="javascript">
       alert("Date To cann't before Date From");
       location = "<%=request.getContextPath()%>/jsp/Reports/ExportMonthlySharholdingData.jsp";
       </script>
     <%
     }
     else
     {
     %>
       <script language="javascript">
     location = "<%=request.getContextPath()%>/jsp/Reports/ExportMonthlyShareholdingReport.jsp?dateselect_to=<%=to_date%>&datselect_from=<%=from_date%>";
     </script>
     <%
     }
  %>
</body>
</html>
