<script>
/*
'******************************************************************************************************************************************
'Script Author : Md.Kamruzzaman
'Creation Date : December 2006
'Page Purpose  : Selects Date For Export Monthly Data
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Shareholding Report</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
//Execute while click on Submit
function SubmitThis() {
  count = 0;
  SelectValidate('dateselect_to','-  Date To (Option must be entered)');
  SelectValidate('dateselect_from','-  Date From (Option must be entered)');
  if (count == 0)
  {
    if (confirm("Do you want to generate Monthly Shareholding Report?"))
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
    int count=1;
    String tempdate="";
    String thedate="";
    String query1="";
    String selected_todate="";
    String selected_fromdate="";
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

  %>
<form action="<%=request.getContextPath()%>/jsp/Reports/Shareholdertemp.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Monthly Shareholding Report</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date To</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="dateselect_to" class="SL1TextFieldListBox">
            <option>--- Please Select ---</option>
            <%
             thedate = "";

              query1 = "SELECT DISTINCT DEC_DATE FROM MONTHLYSHAREHOLDING_VIEW ORDER BY DEC_DATE DESC";
              cm.queryExecute(query1);

              while(cm.toNext())
              {

                 thedate = cm.getColumnDT("DEC_DATE");
                 if(count==1)
                 {
                   selected_todate=thedate;
                   count++;
                    %>
                   <option value="<%=thedate%>" selected="<%=selected_todate%>"><%=thedate%></option>
                   <%
                 }
                 else
                 {
                   count++;
                   %>
                   <option value="<%=thedate%>"><%=thedate%></option>
                   <%
                   }
              }
            %>
         </select>
        </div></td>
      </tr>
       <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date From</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="dateselect_from" class="SL1TextFieldListBox">
         <option>--- Please Select ---</option>
            <%
              query1 = "SELECT DISTINCT DEC_DATE FROM MONTHLYSHAREHOLDING_VIEW  ORDER BY DEC_DATE DESC";
              cm.queryExecute(query1);
              count=1;
              while(cm.toNext())
              {
                 thedate = cm.getColumnDT("DEC_DATE");
                 if(count==2)
                 {
                   selected_fromdate=thedate;
                   count++;
                    %>
                   <option value="<%=thedate%>" selected="<%=selected_fromdate%>"><%=thedate%></option>
                  <%
                 }
                 else
                 {
                   count++;
                   %>
                   <option value="<%=thedate%>"><%=thedate%></option>
                   <%
                   }
              }
            %>
         </select>
        </div></td>
      </tr>
  </table>
  <br>
      </div>

      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="50%" scope="row">
            <div align="right">
	      <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
            </div></th>
        <td width="50%" align="left">
 	      <img name="B1" src="<%=request.getContextPath()%>/images/btnOK.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOKOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOK.gif'">
      </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
