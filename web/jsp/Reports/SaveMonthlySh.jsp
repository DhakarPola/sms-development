<script>
/*
'******************************************************************************************************************************************
'Script Author : Mohammad Ashraful Islam
'Updated by    : Md. Kamruzzaman
'Creation Date : December 2006
'Page Purpose  : Save new Monthly Shareholding Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>

<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Monthly Shareholding Report</title>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();

   if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
     String foliono="";
     String compname="";
     int totalshare=0;

     int c=0;

     String decdate = String.valueOf(request.getParameter("DecDate"));
     String percent_sh = String.valueOf(request.getParameter("pshare"));
     double percent_shD=Double.parseDouble(percent_sh);
     //System.out.println("percent_shD= "+percent_shD);

     String query1 = "SELECT DISTINCT DEC_DATE FROM MONTHLYSHAREHOLDING_VIEW WHERE DEC_DATE >= TO_DATE('" +decdate+ "','DD/MM/YYYY')";
     c=cm.queryExecuteCount(query1);

     if (c>0)
     {
       %>
       <script language="javascript">
       alert("Your entry date is invalid.Please try again with another date.");
       location = "<%=request.getContextPath()%>/jsp/Reports/NewMonthlySh.jsp";
       </script>
       <%
     }

     else
    {
      //String query2="SELECT SUM(TOTAL_SHARE) T1 FROM SHAREHOLDER WHERE ACTIVE='T'";
      String query2="SELECT A.*,B.* FROM (SELECT SUM(TOTAL_SHARE) T1 FROM SHAREHOLDER WHERE ACTIVE='T') A, (SELECT SUM(CURRBAL) T2 FROM BOHOLDING) B";
      cm.queryExecute(query2);
      cm.toNext();

      double total1=0;
      total1=cm.getColumnI("T1");
/*
      String query3="SELECT SUM(CURRBAL) T2 FROM BOHOLDING";
      cm.queryExecute(query3);
      cm.toNext();
*/
      double total2=0;
      total2=cm.getColumnI("T2");

      double grandtotalSH=total1+total2;

      double tenperc=(grandtotalSH*percent_shD)/100.0;
      //System.out.println("tenperc= "+tenperc);

     String query4 = "SELECT FOLIO_NO,NAME,TOTAL_SHARE FROM SHAREHOLDER_VIEW WHERE ACTIVE='T' AND TOTAL_SHARE BETWEEN '"+tenperc+"' AND '"+grandtotalSH+"' ORDER BY TOTAL_SHARE DESC";
     cm.queryExecute(query4);
     //System.out.println("query4= "+query4);
     while(cm.toNext())
              {
                foliono = cm.getColumnS("FOLIO_NO").trim();
                compname = cm.getColumnS("NAME").trim();
                totalshare=cm.getColumnI("TOTAL_SHARE");
                if (String.valueOf(foliono).equals("null"))
                foliono = "";
                if (String.valueOf(compname).equals("null"))
                compname = "";
                compname=cm.replace(compname,"'","''");
              String mnt = "call ADD_MONTHLYSH('" + foliono + "', '" + compname + "', '" + decdate + "', '" + totalshare + "', '" + grandtotalSH + "','" + percent_sh + "')";
              //System.out.println("mnt="+mnt);
              boolean b = cm1.procedureExecute(mnt);

             }

     String boid="";
     String boname="";
     int total=0;

     String query5 = "SELECT * FROM BOHOLDING_VIEW WHERE CURRBAL BETWEEN '"+tenperc+"' AND '"+grandtotalSH+"' ORDER BY CURRBAL DESC";
     cm.queryExecute(query5);
    //System.out.println("query5= "+query5);
    while(cm.toNext())
              {

                boid = cm.getColumnS("BOID").trim();
                boname = cm.getColumnS("BOSHORTNAME").trim();
                total=cm.getColumnI("CURRBAL");
                //System.out.println("Before boname= "+boname);

                if (String.valueOf(boid).equals("null"))
                     boid = "";
                 if (String.valueOf(boname).equals("null"))
                     boname = "";
                //System.out.println("mid boname= "+boname);
                boname=cm.replace(boname,"'","''");
                //System.out.println("after boname= "+boname);
               String mntsh = "call ADD_MONTHLYSH('" + boid + "', '" + boname + "', '" + decdate + "', '" + total + "', '" + grandtotalSH + "','" + percent_sh + "')";
               //System.out.println("mntsh="+mntsh);
               boolean b = cm1.procedureExecute(mntsh);
              //System.out.println("last boname= "+boname);
             }
           }
    String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added Monthly Shareholding Information')";
    boolean ub = cm.procedureExecute(ulog);

    if (isConnect)
   {
     cm.takeDown();
     cm1.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Reports/MonthlySharholdingData.jsp";
  </script>
</body>
</html>
