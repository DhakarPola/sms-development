<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.io.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*"%>
<jsp:useBean id="cm"  class="batbsms.conBean" />
<%

     boolean isConnect = cm.connect();
     if(isConnect==false){
       %>
       <jsp:forward page="ErrorMsg.jsp" >
         <jsp:param name="ErrorTitle" value="Connection Failure" />
         <jsp:param name="ErrorHeading" value="Connection Problem" />
         <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
       </jsp:forward>
       <%
       }
  %>
<html>
<head>
<title>
OpenAttatchment
</title>
</head>
<body bgcolor="#ffffff">
<h1>
   <%
   String Com_Id=request.getParameter("ATTACH_ID");
   int Com_Id_int = Integer.parseInt(Com_Id);
   String QueryAttch="select FILENAME from COMP_ATTACH where ATTACH_ID="+Com_Id_int;

   Connection conn=null;
   OutputStream o=null;
   byte[] imgData2=null;
   String query1 = "SELECT ATTACHMENTS FROM COMP_ATTACH WHERE ATTACH_ID = " + Com_Id_int;
   String fileName="";
   try
   {
     cm.queryExecute(QueryAttch);
     while(cm.toNext())
     {
       fileName=cm.getColumnS("FILENAME");
     }
   }
   catch (Exception e)
   {
   }

   String fileType = fileName.substring(fileName.indexOf(".")+1,fileName.length());
   if (fileType.trim().equalsIgnoreCase("txt"))
   {
     response.setContentType( "text/plain" );
   }
   else if (fileType.trim().equalsIgnoreCase("doc"))
   {
     response.setContentType( "application/msword" );
   }
   else if (fileType.trim().equalsIgnoreCase("xls"))
   {
     response.setContentType( "application/vnd.ms-excel" );
   }
   else if (fileType.trim().equalsIgnoreCase("pdf"))
   {
     response.setContentType( "application/pdf" );
   }
   else if (fileType.trim().equalsIgnoreCase("ppt"))
   {
     response.setContentType( "application/ppt" );
   }
   else
   {
     response.setContentType( "application/octet-stream" );
   }

   try
   {

     conn = cm.getConnection();

     conn.setAutoCommit (false);
     // get the image from the database
     response.setHeader("Content-Disposition","+fileName+; filename=\""+fileName+"\"");
     response.setHeader("cache-control", "no-cache");
     response.setHeader("cache-control", "must-revalidate");
     imgData2 =  cm.getPhoto(conn, query1);

     ServletOutputStream outs = response.getOutputStream();
     outs.write(imgData2);
     outs.flush();
     outs.close();
   }
   catch (Exception e)
   {
     conn=null;
   }
   conn=null;

   if (isConnect)
   {
     try
     {
       cm.takeDown();
     }
     catch (Exception e)
     {

     }
   }

    %>
</h1>
</body>
</html>
