<script>
/*
'******************************************************************************************************************************************
'Script Author : Mohammad Ashraful Islam
'Creation Date : November 2006
'Page Purpose  : View of all the complaint by Name
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style19 {color: #0A2769; font-weight: bold; }
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Name</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--

function goNextPrevious(pstart,pend,foliono)
{
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Complaint/ComplaintbyName.jsp?StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;
  thisurl = thisurl + "&searchName="+foliono;
  location = thisurl;
}

 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   int com_state=0;
   int COUNT;
   int MAX_RECORDS=20;
   String com_name=request.getParameter("searchName");

   String com_no="";
   String csubject = "";
   String cdate = "";
   String cfolio = "";
   String cshareholder = "";
   String ccomplaint = "";
   String cassigned = "";
   String ctime = "";
   String cstatus="";
   String ccomment="";
   String dest="";
 //Used for NEXT/PREVIOUS
    String SStartingValue = String.valueOf(request.getParameter("StartValue"));
    String SEndingValue = String.valueOf(request.getParameter("EndValue"));


    if(SStartingValue == null || SStartingValue.equals("") || SStartingValue.equals("null")) SStartingValue="1";
    if(SEndingValue == null || SEndingValue.equals("") || SEndingValue.equals("null")) SEndingValue="0";

    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    int Chunk = MAX_RECORDS;
    EndingValue = StartingValue + Chunk - 1;
    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;

    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }

    if (String.valueOf(com_name).equals("null"))
      com_name = "";
   String query1;

    if (com_name.length() < 1)
   {
     query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (select * from COMPLAINTS order by LOWER(SH_NAME)) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;
   }
   else
   {
     query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (select * from COMPLAINTS where SH_NAME LIKE '%" + com_name + "%' order by lower(SH_NAME)) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;

   }

   //System.out.println(query1);

   cm.queryExecute(query1);
   COUNT=0;
   while(cm.toNext())
   {
     COUNT++;
   }
   cm.queryExecute(query1);

%>
  <span class="style7">
  <form method="GET" action="ComplaintbyName.jsp">
  <SPAN id="dprint">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
  </SPAN>
  <table width="100%" BORDER=1  cellpadding="0" bordercolor="0044B0" style="border-collapse: collapse" bordercolor="#06689E">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7" height="30"><center>Complaints - by Name</center></td></tr>
  <tr>
  	<td>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0">
			<tr bgcolor="#E8F3FD">
				<td width="10%"></td>
				<td width="10%"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goNextPrevious(<%=pStartingValue%>,<%=pEndingValue%>,'<%=com_name%>')"></td>
				<td width="29%"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNextPrevious(<%=nStartingValue%>,<%=nEndingValue%>,'<%=com_name%>')"></td>
				<td width="12%" class="style19">Name:</td>
				<td width="19%"><input name="searchName" type="text" >
                                        <input type="hidden" name="hcom" value="<%=com_name%>"/>
                                </td>
				<td width="20%"><img src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="SubmitThis()"></td>
			</tr>
                </table>
	</td>
  </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="0044B0">
  <tr bgcolor="#0044B0">
    <td width="8%" class="style9" align="center">Ref. No.</td>
    <td width="8%" class="style9" align="center">Date</td>
    <td width="16%" class="style9" align="center">Folio No/BO ID</td>
    <td width="19%"><div align="center" class="style9">
      <div align="center">Name</div>
    </div></td>
    <td width="20%"><div align="center" class="style9">
      <div align="center">Subject</div>
    </div></td>
    <td width="19%"><div align="center" class="style9">
      <div align="center">Assigned Person</div>
    </div></td>
    <td width="10%"><div align="center" class="style9">
      <div align="center">Status</div>
    </div></td>
  </tr>
  <div align="left">
     <%
     com_state=0;
     int counter1=0;
    while(cm.toNext() && COUNT!=0)
     {
       com_state++;
       counter1++;
       com_no=cm.getColumnS("COM_ID");
       csubject =cm.getColumnS("SUBJECT");
       cdate = cm.getColumnDT("L_DATE");
       cfolio = cm.getColumnS("BOFOLIO");
       cshareholder =cm.getColumnS("SH_NAME");
       ccomplaint =cm.getColumnS("COMPLAINT");
       cassigned =cm.getColumnS("ASS_PERSON");
       ctime = cm.getColumnS("TIMELINE");
       cstatus=cm.getColumnS("STATUS");
       ccomment=cm.getColumnS("COMMENTS");

       dest = request.getContextPath() + "/jsp/Complaint/EditComplaint.jsp";

       if (!com_name.equals("null"))
       {
         if (String.valueOf(ccomplaint).equals("null"))
           ccomplaint = "";
         if (String.valueOf(ccomment).equals("null"))
           ccomment = "";
         if (String.valueOf(cshareholder).equals("null"))
           cshareholder = "";
         if (String.valueOf(ctime).equals("null"))
           ctime = "";
         if (String.valueOf(cassigned).equals("null"))
           cassigned = "";
         %>
    </div>
    <tr bgcolor="#E8F3FD">
           <td ><div align="center" class="style10">
             <div align="center" class="style10">
               <span class="style13">
                  <a HREF="<%=dest%>?fno=<%=com_no%>&dbstatus=actual"><%=com_no%></a>
               &nbsp;
               <%
               if(com_state==COUNT)
               {
               %>
                 <input type="hidden" name="svalue" value="<%=StartingValue%>">
                 <input type="hidden" name="evalue" value="<%=EndingValue%>">
              <%}%>
               </span></div>
             </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center"><%=cdate %></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center"><%=cfolio%></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div>&nbsp;<%=cshareholder%></div>
           </div></td>
           <td class="style13"><div align="justify" class="style12">
             <div>&nbsp;<%=csubject%></div>
           </div></td>
           <td class="style13"><div align="justify" class="style12">
             <div>&nbsp;<%=cassigned %></div>
           </div></td>
           <td class="style13"><div align="justify" class="style12">
             <div>&nbsp;<%=cstatus%></div>
           </div></td>
    </tr>
         <div align="left" class="style13">
             <%
         }
     }

   if (isConnect)
   {
     cm.takeDown();
   }
  %>
         </div>
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD" ><center>

</body>
</html>
