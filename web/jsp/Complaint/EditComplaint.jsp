<script>
/*
'******************************************************************************************************************************************
'Script Author : Md.Kamruzzaman
'Updated By    : Renad Hakim
'Creation Date : November 2006
'Page Purpose  : Form to Edit Complaint
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }


   //String refid="1";
  String refid=request.getParameter("fno");
   int refid_int = Integer.parseInt(refid);

   String csubject = "";
   String cdate = "";
   String cfolio = "";
   String cshareholder = "";
   String ccomplaint = "";
   String cassigned = "";
   String ctime = "";
   String cstatus="";
   String ccomment="";
   int rowcounter1 = 0;
   String query1 = "SELECT * FROM COMPLAINTS WHERE COM_ID =" + refid_int;
   cm.queryExecute(query1);

   while(cm.toNext())
     {
       csubject =cm.getColumnS("SUBJECT");
       cdate = cm.getColumnDT("L_DATE");
       cfolio = cm.getColumnS("BOFOLIO");
       cshareholder =cm.getColumnS("SH_NAME");
       ccomplaint =cm.getColumnS("COMPLAINT");
       cassigned =cm.getColumnS("ASS_PERSON");
       ctime = cm.getColumnS("TIMELINE");
       cstatus=cm.getColumnS("STATUS");
       ccomment=cm.getColumnS("COMMENTS");

       if (String.valueOf(cshareholder).equals("null"))
        cshareholder  = "";
       if (String.valueOf(cassigned).equals("null"))
         cassigned = "";
       if (String.valueOf(ctime).equals("null"))
         ctime = "";
       if (String.valueOf(ccomment).equals("null"))
         ccomment = "";
     }
%>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit Complaint</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">

<!--
function gotoedit()
{
  //location = "<%//=request.getContextPath()%>/jsp/Complaint/ComplaintbyReference.jsp";
  history.go(-1)
}
function formconfirm()
{
  if (confirm("Do you want to change this information."))
  {
    return true;
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('bsubject','- Subject of Complaint (Option must be entered)');
  BlankValidate('bdate','- Date of Complaint (Option must be entered)');
  BlankValidate('bfolio','- Folio no./BO ID (Option must be entered)');
  BlankValidate('bcomplaint','- Complaints (Option must be entered)');
  BlankValidate('bstatus','- Status (Option must be entered)');

  if (count == 0)
  {
    if (formconfirm())
    {
      var regexp = /\\/g;
      document.forms[0].filepath.value = document.forms[0].filepath.value.replace(regexp,"\\\\");
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
.style12 {
	color: #000066;
	font-size: 11px;
}
.style19 {
	color: #FFFFFF;
	font-weight: bold;
}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">

<form action="UpdateComplaint.jsp" method="post" name="FileForm" enctype="multipart/form-data">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Edit Complaint</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="85%"  border="0" cellpadding="5">
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Subject</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="bsubject" class="SL68TextField" value="<%= csubject %>">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date</div></th>
        <td>:</td>
        <td><div align="left">
         <input name="bdate" type=text id="bdate" value="<%= cdate %>" maxlength="10" class="SL77TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('bdate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Folio No./BO ID</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="bfolio" class="SL68TextField" value="<%= cfolio %>">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Shareholder Name</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="bshare" class="SL68TextField" value="<%= cshareholder %>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Complaint</div></th>
        <td valign="top">:</td>
        <td><div align="left">
          <textarea name="bcomplaint" cols="2" wrap="VIRTUAL" class="ML10TextField" ><%= ccomplaint %></textarea>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Assigned Person</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="bassign" class="SL68TextField" value="<%= cassigned %>">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Timeline</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="btime" class="SL68TextField" value="<%= ctime %>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="54%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Status</div></th>
        <td width="1%">:</td>
        <td><div align="left">
        <select name="bstatus" class="SL2TextFieldListBox">
         <%
        String select1=cstatus;
        %>
         <option>--- Please Select ---</option>
            <option value="Recieved" <%if(select1.equalsIgnoreCase("Recieved")){%>Selected<%}%>>Recieved</option>
            <option value="In Progress" <%if(select1.equalsIgnoreCase("In Progress")){%>Selected<%}%>>In Progress</option>
            <option value="On Hold" <%if(select1.equalsIgnoreCase("On Hold")){%>Selected<%}%>>On Hold</option>
            <option value="Completed" <%if(select1.equalsIgnoreCase("Completed")){%>Selected<%}%>>Completed</option>
            <option value="Rejected" <%if(select1.equalsIgnoreCase("Rejected")){%>Selected<%}%>>Rejected</option>
        </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Comments</div></th>
        <td valign="top">:</td>
        <td><div align="left">
          <textarea name="bcomment" cols="2" wrap="VIRTUAL" class="ML10TextField" ><%= ccomment %></textarea>
        </div></td>
      </tr>
      <tr>
        <th scope="row"width="54%"><div align="left" class="style8">&nbsp;&nbsp;Attachments </div></th>
            <td width="1%">:</td>

            <td width="45%"><input name="filepath" type="file" id="filepath" class="SL68TextField"></td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;Attachment Description</div></th>
        <td valign="top">:</td>
        <td><div align="left">
          <textarea name="AttComment" id="AttComment" cols="2" wrap="VIRTUAL" class="ML10TextField" ></textarea>
        </div></td>
      </tr>

      <tr>
      <th scope="row" width="54%" valign="top"><div align="left" valign="top" class="style8">&nbsp;&nbsp;Attachments List</div></th>
      <td valign="top"  width="1%">:</td>
        <td  width="44%">
      <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="0044B0">
      <tr bgcolor="#0044B0">
         <td class="style19" align="center" width="12%">Sl No.</td>
         <td class="style19" align="center" width="35%">File Name</td>
         <td class="style19" align="center" width="40%">Description</td>
         <td class="style19" align="center" width="13%">Delete</td>
      </tr>
      <%
      query1 = "SELECT ATTACH_ID,FILENAME,DESCRIPTION FROM COMP_ATTACH WHERE COMP_ID = " + refid_int + "ORDER BY ATTACH_ID";
      try{
        cm.queryExecute(query1);
        String DescAtt="";
        int Arowcount=0;
        while(cm.toNext())
        {
          rowcounter1++;
          DescAtt=cm.getColumnS("DESCRIPTION");
          if (String.valueOf(DescAtt).equals("null"))
          {
            DescAtt = "";
          }
          %>
          <tr>
            <td align="center" class="style12"><%=++Arowcount%></td>
            <td class="style12"><a href="OpenAttatchment.jsp?ATTACH_ID=<%=cm.getColumnS("ATTACH_ID")%>" target="_new"><%=cm.getColumnS("FILENAME")%></a></td>
            <td class="style12"><%=DescAtt%></td>
            <td align="center"><input type="checkbox" name="selectingbox<%=rowcounter1%>" value="<%=cm.getColumnS("ATTACH_ID")%>"></td>
          </tr>
          <%
          }
        }
        catch (Exception ex){
        }
      %>
      </table>
      </td>
      </tr>

  </table>

     <!-- Hidden Field -->
  <input type="hidden" name="bcomid" value="<%=refid%>"/>

  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="10" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <td width="50%" align="right">
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
      </td>
        <td width="50%">
	          <img name="B2" src="<%=request.getContextPath()%>/images/btnClose.gif" onclick="gotoedit()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
      </td>
      </tr>
</table>
<input type="hidden" name="rowcounter1" id="rowcounter1" value="<%=rowcounter1%>">
</form>
<%
if (isConnect)
{
  try{
    cm.takeDown();
  }
  catch (Exception ex){
  }
}

%>
</body>
</html>
