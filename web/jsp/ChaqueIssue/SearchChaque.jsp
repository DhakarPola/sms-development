<script>
/*
'******************************************************************************************************************************************
'Script Author : Rahat Uddin
'Creation Date : Octoberber 2011
'Page Purpose  : View of all the Chaque Information
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style19 {color: #0A2769; font-weight: bold; }
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Folios</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--

function goNextPrevious(pstart,pend,foliono)
{
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Folio/AllPermanentFolios.jsp?StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;
  thisurl = thisurl + "&searchFolio="+foliono;
  location = thisurl;
}

 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  //alert('count='+count);
  var searchFolio=document.getElementById("searchFolio").value;
  var chequeNo=document.getElementById("chequeNo").value;
  if (count == 0){
    if(searchFolio.length >7){
      document.forms[0].action="SearchChaque.jsp";
    }
    else if(chequeNo.length >2){
      document.forms[0].action="EditChaque.jsp";
    }
    //document.forms[0].submit();
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   int folio_state=0;
   int COUNT;
   int MAX_RECORDS=20;
   String folio_no=request.getParameter("searchFolio");
   String folioname = "";
   String folioaddress = "";
   String folioaddress2 = "";
   String folioaddress3 = "";
   String folioaddress4 = "";
   String dest = "";
   int totalshares = 0;



    if (String.valueOf(folio_no).equals("null"))
      folio_no = "1";

   String query1;

/*   if(folio_no==null)
   {
     folio_no="1";
   }
*/




   if (folio_no.length() <= 8)
   {
      query1 = "select * from shareholder_view WHERE folio_no='"+folio_no+"'";
   }
   else
   {
      //query1 = "Select bov.boid,bov.BOSHORTNAME,boa.ADDRESS from boholding_view bov, boaddress_all_view boa where bov.boid= boa.bo_id  and BO_ID='"+folio_no+"'";
      query1 = "Select * from boaddress_all_view where  BO_ID='"+folio_no+"'";
   }

   //System.out.println(query1);
   cm.queryExecute(query1);


%>
  <span class="style7">
  <form method="POST" action="SearchChaque.jsp" onsubmit="SubmitThis()">
  <table width="100%" BORDER=1  cellpadding="0" bordercolor="0044B0" style="border-collapse: collapse" bordercolor="#06689E">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7" height="30"><center>Folio/BO Information</center></td></tr>
  <tr>
  	<td>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0">
			<tr bgcolor="#E8F3FD">
				<td width="10%"></td>
				<td width="12%" class="style19">Enter Folio/BO No</td>
                                <td width="2%" class="style19">:</td>
				<td width="19%"><input id="searchFolio" name="searchFolio" type="text" value="">
                                        <input type="hidden" name="hfolio" value="<%=folio_no%>"/>
                                </td>
				<td width="20%"></td>
			</tr>
                        <tr bgcolor="#E8F3FD">
                          <td width="10%"></td>
                          <td width="12%" class="style19"></td>
                          <td width="2%" class="style19"></td>
                          <td width="19%" align="center">OR</td>
                          <td width="20%"></td>

			</tr>
                        <tr bgcolor="#E8F3FD">
                          <td width="10%"></td>
                          <td width="12%" class="style19">Cheque No</td>
                          <td width="2%" class="style19">:</td>
                          <td width="19%"><input id="chequeNo" name="chequeNo" type="text" value="">
                          </td>
                          <td width="20%"></td>
			</tr>
                        <tr bgcolor="#E8F3FD">
                          <td width="10%"></td>
                          <td width="12%" class="style19"></td>
                          <td width="2%" class="style19"></td>
                          <td width="19%" align="center" height="35pt" valign="middle">
                            <input type="submit" value="" style="background: white url('<%=request.getContextPath()%>/images/btnSearch.gif') no-repeat;width: 63px; height:23px;border:0px;"/>
                          </td>
                          <td width="20%"></td>

			</tr>
                </table>
	</td>
  </tr>
  <tr>
  	<td>
      <%
       folio_state=0;
       while(cm.toNext())
       {
         folio_state++;
         if (folio_no.length() <= 8){
           folio_no=cm.getColumnS("FOLIO_NO");
           folioname = cm.getColumnS("NAME");
           folioaddress = cm.getColumnS("ADDRESS1");
           folioaddress2 = cm.getColumnS("ADDRESS2");
           folioaddress3 = cm.getColumnS("ADDRESS3");
           folioaddress4 = cm.getColumnS("ADDRESS4");
         }
         else{

           folio_no=cm.getColumnS("BO_ID");
           //folioname = cm.getColumnS("BOSHORTNAME");
           folioname = cm.getColumnS("NAME_FIRSTHOLDER");
           folioaddress = cm.getColumnS("ADDRESS");
         /*folioaddress2 = cm.getColumnS("ADDRESS2");
         folioaddress3 = cm.getColumnS("ADDRESS3");
         folioaddress4 = cm.getColumnS("ADDRESS4");*/
         }
         dest = request.getContextPath() + "/jsp/ChaqueIssue/EditChaque.jsp";

         if (!folio_no.equals("null"))
         {
           if (String.valueOf(folioname).equals("null"))
           folioname = "";
           if (String.valueOf(folioaddress).equals("null"))
           folioaddress = "";
           if (String.valueOf(folioaddress2).equals("null"))
           folioaddress2 = "";
           if (String.valueOf(folioaddress3).equals("null"))
           folioaddress3 = "";
           if (String.valueOf(folioaddress4).equals("null"))
           folioaddress4 = "";

           folioaddress = folioaddress + folioaddress2 + folioaddress3 + folioaddress4;

         %>
          <table width="100%"  border="0" cellpadding="0" cellspacing="0">
            <tr bgcolor="#E8F3FD">
                <td width="7%"></td>
                <td width="15%" class="style19">Folio/BO No:</td>
                <td width="3%" align=center>:</td>
                <td width="75%"><%=folio_no%></td>
            </tr>
            <tr bgcolor="#E8F3FD">
                <td ></td>
                <td  class="style19">Name</td>
                <td align=center>:</td>
                <td><%=folioname%></td>
            </tr>
            <tr bgcolor="#E8F3FD">
                <td ></td>
                <td class="style19">Address</td>
                <td align=center>:</td>
                <td ><%=folioaddress%></td>
            </tr>

          </table>
	</td>
  </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="0044B0">
    <tr bgcolor="#E8F3FD">
    <td  height="20pt"><a href="NewChaque.jsp?foliono=<%=folio_no%>"><img border=0 src="<%=request.getContextPath()%>/images/btnNew.gif"></a></td>
    </tr>
  </table>
     <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="0044B0">
       <tr bgcolor="#0044B0">
         <td width="10%"><div align="center" class="style9">
           <div align="center">Chaque ID</div>
</div></td>
<td width="20%"><div align="center" class="style9">
  <div align="center">Account No.</div>
</div></td>
<td width="20%"><div align="center" class="style9">
  <div align="center">Chaque No.</div>
</div></td>

<td width="10%"><div align="center" class="style9">
  <div align="center"> Issue Date</div>
</div></td>
<td width="10%"><div align="center" class="style9">
  <div align="center"> Amount </div>
</div></td>
<td width="11%"><div align="center" class="style9">
  <div align="center">Status</div>
</div></td>
       </tr>
  <%
  query1 = "Select chaque_id, chaque_no, coll_status, to_char(issue_date,'dd/mm/yyyy') as issue_date, folio_bo,nvl(ACCOUNT_NO,'Not Available') as ACCOUNT_NO,TOTAL_AMOUNT from issued_chaque where folio_bo='"+folio_no+"'";
/*query1 = "SELECT FOLIO_BO,issue_date,warrant_no FROM (select di.folio_no FOLIO_BO, di.issue_date, di.warrant_no  from dividend DI where di.folio_no='"+folio_no+"' and di.remarks is null and collected='F' ";
 query1 = query1 + " UNION ";
 query1 = query1 + " SELECT bo.bo_id FOLIO_BO, bo.issue_date, bo.warrant_no  FROM bodividend BO WHERE  BO.bo_id='"+folio_no+"' and bo.remarks is null and collected='F') ";
 query1 = query1 + " MINUS ";
 query1 = query1+ " (Select warr_chaque.FOLIO_BO, warr_chaque.ISSUE_DATE, warr_chaque.warrent_no from warr_chaque, issued_chaque where warr_chaque.folio_bo='"+folio_no+"' and issued_chaque.chaque_id=warr_chaque.chaque_id and issued_chaque.coll_status='F') ORDER BY ISSUE_DATE ";
 */
   //System.out.println(query1);

   cm.queryExecute(query1);
   while(cm.toNext())
   {
     folio_no=cm.getColumnS("FOLIO_NO");
     String collStatus=cm.getColumnS("coll_status");
     if (collStatus.equalsIgnoreCase("T")){
       collStatus="Collected";
     }
      else if (collStatus.equalsIgnoreCase("F")){
        collStatus="Uncollected";
    }
    else if (collStatus.equalsIgnoreCase("C")){
      collStatus="Canceled";
    }
    String totalAmount=cm.getColumnS("TOTAL_AMOUNT");
    if(totalAmount.indexOf(".")==totalAmount.length()-2)
        totalAmount=totalAmount+"0";
    else  if(totalAmount.indexOf(".")==-1) {
      totalAmount=totalAmount+".00";
    }
     %>


       <tr bgcolor="#E8F3FD">
         <td ><div align="center" class="style10">
           <div align="center" class="style10">
             <span class="style13">
               <a HREF="<%=dest%>?chaid=<%=cm.getColumnS("chaque_id")%>&foliono=<%=folio_no%>"><%=cm.getColumnS("chaque_id")%></a>
               &nbsp;

             </span></div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">&nbsp;&nbsp;<a HREF="<%=dest%>?chaid=<%=cm.getColumnS("chaque_id")%>&foliono=<%=folio_no%>"><%=cm.getColumnS("ACCOUNT_NO")%></a></div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">&nbsp;&nbsp;<a HREF="<%=dest%>?chaid=<%=cm.getColumnS("chaque_id")%>&foliono=<%=folio_no%>"><%=cm.getColumnS("chaque_no")%></a></div>
</div></td>
<td class="style13"><div align="justify" class="style12">
  <div align="center"><%=cm.getColumnS("issue_date")%></div>
</div></td>
<td class="style13"><div align="justify" class="style12">
  <div align="right"><%=totalAmount%></div>
</div></td>
<td class="style13"><div align="center" class="style12">
  <div align="center"><%=collStatus%></div>
</div></td>
       </tr>
       <div align="left" class="style13">
       <%
           }//End of Chaque while
         }

     }

   if (isConnect)
   {
       cm.takeDown();
   }
  %>
         </div>
         </table>
  </form>

</body>
</html>
