<script>
/*
'******************************************************************************************************************************************
'Script Author : Rahat Uddin
'Creation Date : Octoberber 2011
'Page Purpose  : View of all the Chaque Information
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style19 {color: #0A2769; font-weight: bold; }
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Folios</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--

function goNextPrevious(pstart,pend,foliono)
{
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Folio/AllPermanentFolios.jsp?StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;
  thisurl = thisurl + "&searchFolio="+foliono;
  location = thisurl;
}

 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   int folio_state=0;
   int COUNT;
   int MAX_RECORDS=20;
   String chaqueid=request.getParameter("chaqueid");
   String folio_no=request.getParameter("Folio_Bo");

   String chaqueno=request.getParameter("chaqueno");
   String totalamount=request.getParameter("totalamount");
   String ChaqueStatus=request.getParameter("ChaqueStatus");
   String issuedate=request.getParameter("issuedate");
   String accountNo=request.getParameter("account");

   String folioname = "";
   String folioaddress = "";
   String folioaddress2 = "";
   String folioaddress3 = "";
   String folioaddress4 = "";
   String dest = "";
   int totalshares = 0;



    if (String.valueOf(chaqueid).equals("null"))
      chaqueid = "0";

   String query1;
   query1 = "Update issued_chaque set  chaque_no='"+chaqueno+"', coll_status='"+ChaqueStatus+"', issue_date=to_Date('"+issuedate+"','dd/mm/yyyy'), folio_bo='"+folio_no+"',Total_amount="+totalamount+",ACCOUNT_NO='"+accountNo+"' where chaque_id="+chaqueid;
   //System.out.println(query1);
   cm.queryExecute(query1);


   System.out.println("-----------"+ChaqueStatus);
  if(ChaqueStatus.equalsIgnoreCase("T")){
    String rowcounter = String.valueOf(request.getParameter("rowcounter1"));
    String wrrent="";
    String wrrent_return="";
    int irowcounter = Integer.parseInt(rowcounter);
    query1="delete WARR_CHAQUE where chaque_id="+chaqueid;
    cm.queryExecute(query1);
    cm.connCommit();
    for (int i=1;i<irowcounter+1;i++)
    {
      wrrent="selectingbox"+ String.valueOf(i);
      wrrent_return=String.valueOf(request.getParameter(wrrent));
      if (!String.valueOf(wrrent_return).equals("null"))
      {
        query1="Insert into WARR_CHAQUE (CHAQUE_ID,FOLIO_BO,ISSUE_DATE,WARRENT_NO) values ("+chaqueid+",'"+folio_no+"',to_date('"+wrrent_return.substring(0,wrrent_return.indexOf("^"))+"','dd/mm/yyyy'),'"+wrrent_return.substring(wrrent_return.indexOf("^")+1)+"')";
        cm.queryExecute(query1);
        if (folio_no.length() <= 8)
        {
          query1 = "Update DIVIDEND set COLLECTED='T',SELECTED='T' where FOLIO_NO='"+folio_no+"' and ISSUE_DATE=to_date('"+wrrent_return.substring(0,wrrent_return.indexOf("^"))+"','dd/mm/yyyy') and WARRANT_NO='"+wrrent_return.substring(wrrent_return.indexOf("^")+1)+"'";
        }
        else
        {
          query1 = "Update BODIVIDEND set COLLECTED='T',SELECTED='T' where BO_ID='"+folio_no+"' and ISSUE_DATE=to_date('"+wrrent_return.substring(0,wrrent_return.indexOf("^"))+"','dd/mm/yyyy') and WARRANT_NO='"+wrrent_return.substring(wrrent_return.indexOf("^")+1)+"'";
        }

        //System.out.println(query1);
        cm.queryExecute(query1);
      }
      else {
      wrrent="warrantval"+ String.valueOf(i);
      wrrent_return=String.valueOf(request.getParameter(wrrent));
       if (folio_no.length() <= 8)
        {
          query1 = "Update DIVIDEND set COLLECTED='F',SELECTED='F' where FOLIO_NO='"+folio_no+"' and ISSUE_DATE=to_date('"+wrrent_return.substring(0,wrrent_return.indexOf("^"))+"','dd/mm/yyyy') and WARRANT_NO='"+wrrent_return.substring(wrrent_return.indexOf("^")+1)+"'";
        }
        else
        {
          query1 = "Update BODIVIDEND set COLLECTED='F',SELECTED='F' where BO_ID='"+folio_no+"' and ISSUE_DATE=to_date('"+wrrent_return.substring(0,wrrent_return.indexOf("^"))+"','dd/mm/yyyy') and WARRANT_NO='"+wrrent_return.substring(wrrent_return.indexOf("^")+1)+"'";
        }
        //System.out.println(query1);
        cm.queryExecute(query1);
      }
    }
  }
  else if(ChaqueStatus.equalsIgnoreCase("F")) {
    String rowcounter = String.valueOf(request.getParameter("rowcounter1"));
    String wrrent="";
    String wrrent_return="";
    int irowcounter = Integer.parseInt(rowcounter);
    query1="delete WARR_CHAQUE where chaque_id="+chaqueid;
    //System.out.println(query1);
    cm.queryExecute(query1);
    cm.connCommit();
    for (int i=1;i<irowcounter+1;i++)
    {
      wrrent="selectingbox"+ String.valueOf(i);
      wrrent_return=String.valueOf(request.getParameter(wrrent));
      if (!String.valueOf(wrrent_return).equals("null"))
      {

        query1="Insert into WARR_CHAQUE (CHAQUE_ID,FOLIO_BO,ISSUE_DATE,WARRENT_NO) values ("+chaqueid+",'"+folio_no+"',to_date('"+wrrent_return.substring(0,wrrent_return.indexOf("^"))+"','dd/mm/yyyy'),'"+wrrent_return.substring(wrrent_return.indexOf("^")+1)+"')";
        System.out.println(query1);
        cm.queryExecute(query1);

      }

    }

  }
  else if(ChaqueStatus.equalsIgnoreCase("C")) {
    String rowcounter = String.valueOf(request.getParameter("rowcounter1"));
    String wrrent="";
    String wrrent_return="";
    int irowcounter = Integer.parseInt(rowcounter);
    query1="delete WARR_CHAQUE where chaque_id="+chaqueid;
    //System.out.println(query1);
    cm.queryExecute(query1);
    cm.connCommit();
    for (int i=1;i<irowcounter+1;i++)
    {
      wrrent="selectingbox"+ String.valueOf(i);
      wrrent_return=String.valueOf(request.getParameter(wrrent));
      if (!String.valueOf(wrrent_return).equals("null"))
      {

        query1="Insert into WARR_CHAQUE (CHAQUE_ID,FOLIO_BO,ISSUE_DATE,WARRENT_NO) values ("+chaqueid+",'"+folio_no+"',to_date('"+wrrent_return.substring(0,wrrent_return.indexOf("^"))+"','dd/mm/yyyy'),'"+wrrent_return.substring(wrrent_return.indexOf("^")+1)+"')";
        System.out.println(query1);
        cm.queryExecute(query1);

        if (folio_no.length() <= 8)
        {
          query1 = "Update DIVIDEND set COLLECTED='F',SELECTED='F' where FOLIO_NO='"+folio_no+"' and ISSUE_DATE=to_date('"+wrrent_return.substring(0,wrrent_return.indexOf("^"))+"','dd/mm/yyyy') and WARRANT_NO='"+wrrent_return.substring(wrrent_return.indexOf("^")+1)+"'";
          System.out.println(query1);
        }
        else
        {
          query1 = "Update BODIVIDEND set COLLECTED='F',SELECTED='F' where BO_ID='"+folio_no+"' and ISSUE_DATE=to_date('"+wrrent_return.substring(0,wrrent_return.indexOf("^"))+"','dd/mm/yyyy') and WARRANT_NO='"+wrrent_return.substring(wrrent_return.indexOf("^")+1)+"'";
          System.out.println(query1);
        }
        cm.queryExecute(query1);

      }
      else {
      wrrent="warrantval"+ String.valueOf(i);
      wrrent_return=String.valueOf(request.getParameter(wrrent));
       if (folio_no.length() <= 8)
        {
          query1 = "Update DIVIDEND set COLLECTED='F',SELECTED='F' where FOLIO_NO='"+folio_no+"' and ISSUE_DATE=to_date('"+wrrent_return.substring(0,wrrent_return.indexOf("^"))+"','dd/mm/yyyy') and WARRANT_NO='"+wrrent_return.substring(wrrent_return.indexOf("^")+1)+"'";
          System.out.println("2. "+query1);
        }
        else
        {
          query1 = "Update BODIVIDEND set COLLECTED='F',SELECTED='F' where BO_ID='"+folio_no+"' and ISSUE_DATE=to_date('"+wrrent_return.substring(0,wrrent_return.indexOf("^"))+"','dd/mm/yyyy') and WARRANT_NO='"+wrrent_return.substring(wrrent_return.indexOf("^")+1)+"'";
          System.out.println("2.. "+query1);
        }
        //System.out.println(query1);
        cm.queryExecute(query1);
      }

    }

  }

  if (isConnect)
   {
     cm.takeDown();
   }
%>
  <span class="style7">
  <script language="javascript">
     alert("Cheque Information Updated");
     location = "<%=request.getContextPath()%>/jsp/ChaqueIssue/SearchChaque.jsp";
  </script>

</body>
</html>
