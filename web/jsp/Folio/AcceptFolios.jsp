<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Updated By    : Renad Hakim
'Creation Date : November 2005
'Page Purpose  : View of all the Folios
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Folios</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--

function rejectFolio()
{
  if(confirm("Do you want to permanently store the folio information?"))
  {
    reject_query = "";
    for(i=1;i<=document.forms[0].COUNTER.value;i++ )
    {

      reject_query = reject_query  +  "folioid"+i+"="+document.all["folioid"+i].value+"&"+  "foliostatus"+i+"="+document.all["foliostatus"+i].value+"&";
    }
    reject_query = reject_query + "COUNTER="+document.forms[0].COUNTER.value;
    location = "<%=request.getContextPath()%>/jsp/Folio/SaveFolioPermanent.jsp?"+reject_query;
  }
}

 function askdelete()
 {
  if (confirm("Do you want to permanently delete the folio information?"))
  {
   document.forms[0].submit();
  }
 }

//Execute while click on Submit
function SubmitThis() {
 count = 0;
 if (count == 0){
document.forms[0].submit();
   }
else{
 ShowAllAlertMsg();
 return false;
 }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String query1 = "SELECT * FROM SHAREHOLDER_TEMP_VIEW ORDER BY SUBSTR(FOLIO_NO,3,6)"; //UPPER(FOLIO_NO)";
    cm.queryExecute(query1);
    int counter;
    String folio_no="";
    String folioname = "";
    String folioaddress = "";
    String folioaddress2 = "";
    String folioaddress3 = "";
    String folioaddress4 = "";
    String joint="";
    String dest = "";

%>
  <span class="style7">
  <form method="GET" action="RejectFolio.jsp">
  <img name="B4" src="<%=request.getContextPath()%>/images/btnAcceptAll.gif" onclick="rejectFolio()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAcceptAllR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAcceptAll.gif'">
  <img name="B6" src="<%=request.getContextPath()%>/images/btnReject.gif" onclick="askdelete()" onMouseOver="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnRejectR.gif'" onMouseOut="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnReject.gif'">
  <table width="100%" BORDER=1  cellpadding="5" bordercolor="#06689E" style="border-collapse: collapse">
  <!--DWLayoutTable-->
  <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Folio Information</center></td></tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#06689E">
  <tr bgcolor="#0044B0" >
  	<td width="12%" class="style9"><div align="center">Status</div></td>
    <td width="12%" class="style9"><div align="center">Folio No</div></td>
    <td width="35%"><div align="center" class="style9">
      <div align="center">Name</div>
    </div></td>
    <td width="41%"><div align="center" class="style9">
      <div align="center">Address</div>
    </div></td>
  </tr>
  <div align="left">
     <%
     counter=0;
    while(cm.toNext())
     {
       counter++;
       folio_no=cm.getColumnS("FOLIO_NO");

       folioname = cm.getColumnS("NAME");
       folioaddress = cm.getColumnS("ADDRESS1");
       folioaddress2 = cm.getColumnS("ADDRESS2");
       folioaddress3 = cm.getColumnS("ADDRESS3");
       folioaddress4 = cm.getColumnS("ADDRESS4");
       joint = String.valueOf(cm.getColumnI("JOINT"));

       dest = request.getContextPath() + "/jsp/Folio/EditFolio.jsp";

       if (!folio_no.equals("null"))
       {
         if (String.valueOf(folioname).equals("null"))
           folioname = "";
         if (String.valueOf(folioaddress).equals("null"))
           folioaddress = "";
         if (String.valueOf(folioaddress2).equals("null"))
           folioaddress2 = "";
         if (String.valueOf(folioaddress3).equals("null"))
           folioaddress3 = "";
         if (String.valueOf(folioaddress4).equals("null"))
           folioaddress4 = "";

         folioaddress = folioaddress + folioaddress2 + folioaddress3 + folioaddress4;
         %>
    </div>
    <tr bgcolor="#E8F3FD">
           <td class="style13"><div align="center" class="style12">
             <div align="center">
               <input name="foliostatus<%=counter%>" type="checkbox" ></div>
               <input name="folioid<%=counter%>" type="hidden" value="<%=folio_no%>">
               <input name="joint<%=counter%>" type="hidden" value="<%=joint%>">
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=folio_no%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="left">&nbsp;&nbsp;<%=folioname%>
             </div>
           </div></td>
           <td class="style13"><div align="justify" class="style12">
             <div align="left"><%=folioaddress%></div>
           </div></td>
         </tr>
         <div align="left" class="style13">
             <%
         }
     }
   if (isConnect)
   {
     cm.takeDown();
   }
  %>
         </div>
         <input name="COUNTER" type="hidden" value="<%=counter%>">
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>

</body>
</html>
