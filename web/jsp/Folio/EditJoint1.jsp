
<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Updated By    : Renad Hakim
'Creation Date : December 2005
'Page Purpose  : Adds first Joint Share Holder .
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.io.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*"%>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {
        color: #0A2769;
        font-weight: bold;
	font-size: 13px;
        }
.style12 {color: #FFFFFF}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Add First Joint Shareholder Details</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function editSignatureWindow()
{
  	window.open("EditJoint1Signature.jsp","","HEIGHT=350,WIDTH=500")
}
function editPhotoWindow()
{
        window.open("EditJoint1Photo.jsp","","HEIGHT=350,WIDTH=500")
}
function getFolio()
{
  return window.opener.document.forms[0].foliono.value
}
//Execute while click on Submit
function SubmitThis() {


  if(document.forms[0].jointname.value == null || document.forms[0].jointname.value == "" )
  {
    alert("Please enter Joint Holder's name");
  }
  else
  {
    //Enter the values in NewFolio.jsp hidden fields

    window.opener.document.forms[0].jointprefix.value = document.forms[0].jointprefix.value
    window.opener.document.forms[0].jointname.value = document.forms[0].jointname.value
    window.opener.document.forms[0].jointrelation.value = document.forms[0].jointrelation.value
    window.opener.document.forms[0].jointfather.value = document.forms[0].jointfather.value
    window.opener.document.forms[0].jointoccupation.value = document.forms[0].jointoccupation.value
    window.opener.document.forms[0].jointtelephone.value = document.forms[0].jointtelephone.value
    window.opener.document.forms[0].jointfax.value = document.forms[0].jointfax.value
    window.opener.document.forms[0].jointemail.value = document.forms[0].jointemail.value
    window.opener.document.forms[0].jointaddress.value = document.forms[0].jointaddress.value
    window.opener.document.forms[0].jointcontactperson.value = document.forms[0].jointcontactperson.value
    window.opener.document.forms[0].jointremarks.value = document.forms[0].jointremarks.value
    window.opener.document.forms[0].joint1signature.value = document.forms[0].joint1signature.value
    window.opener.document.forms[0].joint1photo.value = document.forms[0].joint1photo.value
    window.opener.document.forms[0].joint1photostatus.value = document.forms[0].joint1photostatus.value
    window.opener.document.forms[0].joint1signaturestatus.value = document.forms[0].joint1signaturestatus.value


    self.close()

  }
}
//-->
</script>
<style type="text/css">
<!--
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String foliono = request.getParameter("hfoliono");
    String dbstatus = request.getParameter("dbstatus");

    foliono=  foliono.trim();
    dbstatus = dbstatus.trim();

    String tablename="";

    String jointprefix ="";
    String jointname = "";
    String jointrelation = "";
    String jointfather = "";
    String jointoccupation = "";
    String jointtelephone = "";
    String jointfax = "";
    String jointemail = "";
    String jointaddress = "";
    String jointaddress2 = "";
    String jointaddress3 = "";
    String jointaddress4 = "";
    String jointcontactperson = "";
    String jointremarks = "";
    String jointsignature ="";
    String jointphoto = "";

    if(dbstatus!=null && dbstatus.equals("actual"))
    tablename = "JOINT_N_VIEW";
  else
    tablename = "JOINT_N_TEMP_VIEW";

      String queryJoint1 = "SELECT * FROM " +tablename +" WHERE FOLIO_NO = '" + foliono + "' AND JOINT = 1" ;


     cm.queryExecute(queryJoint1);
      while(cm.toNext())
        {
          jointprefix=cm.getColumnS("PREFIX");
          jointname= cm.getColumnS("NAME");
          jointrelation = cm.getColumnS("RELATION");
          jointfather = cm.getColumnS("FATHER");
          jointoccupation = cm.getColumnS("OCCUPATION");
          jointtelephone = cm.getColumnS("TELEPHONE");
          jointfax = cm.getColumnS("FAX");
          jointemail = cm.getColumnS("EMAIL");
          jointaddress= cm.getColumnS("ADDRESS1");

          jointcontactperson = cm.getColumnS("CONTACTPERSON");
          jointremarks = cm.getColumnS("REMARKS");


          if (String.valueOf(jointprefix).equals("null"))
          jointprefix = "";
            if (String.valueOf(jointname).equals("null"))
           jointname= "";
            if (String.valueOf(jointrelation).equals("null"))
           jointrelation= "";
            if (String.valueOf(jointfather).equals("null"))
           jointfather= "";
            if (String.valueOf(jointoccupation).equals("null"))
           jointoccupation= "";

            if (String.valueOf(jointtelephone).equals("null"))
           jointtelephone= "";
            if (String.valueOf(jointfax).equals("null"))
           jointfax= "";
            if (String.valueOf(jointemail).equals("null"))
           jointemail= "";
            if (String.valueOf(jointaddress).equals("null"))
           jointaddress= "";
            if (String.valueOf(jointremarks).equals("null"))
           jointremarks= "";
            if (String.valueOf(jointcontactperson).equals("null"))
           jointcontactperson= "";

           jointname = jointname.trim();
        }
%>

<form action="EditJoint1.jsp" method="get" name="FileForm">

  <span class="style7">
  <table width="100%"  border="1" bordercolor="#06689E" style="border-collapse: collapse" cellpadding="0">
    <tr>
      <td><table width="100%"  border="0" cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
        <tr bgcolor="#E8F3FD">
          <td bgcolor="#0044B0" class="style7">Joint One Details</td>
          <td bgcolor="#0044B0" class="style7">&nbsp;</td>
          <td bgcolor="#0044B0" class="style7">&nbsp;</td>
          </tr>
       <tr bgcolor="#E8F3FD">
          <td width="44%" class="style8"><br>Prefix</td>
          <td width="1%"><br>:</td>
          <td width="55%"><br><input name="jointprefix" type="text" class="SL2TextField" value="<%=jointprefix%>"></td>
       </tr>
       <tr bgcolor="#E8F3FD">
         <td class="style8">Joint Name</td>
         <td>:</td>
         <td><input name="jointname" type="text" class="SL2TextField" value="<%=jointname%>"></td>
        </tr>
       <tr bgcolor="#E8F3FD">
         <td class="style8">Relation</td>
          <td>:</td>
          <td>
            <select name="jointrelation" class="SL1TextFieldListBox">
              <%
                 jointrelation = jointrelation.trim();
                 String values1[] = {"--Please Select--","S/O","W/O","D/O"};
                 for(int i=0;i<values1.length;i++)
                  {
                    if(jointrelation.equals(values1[i]))
                   {
                     %>
                     <option value="<%=values1[i]%>" selected="selected"><%=values1[i]%></option>
                       <%
                                        }
                                        else
                                        {
                                        %>
                                        <option value="<%=values1[i]%>" ><%=values1[i]%></option>
                                        <%
                                        }
                                      }
                                  %>
                                </select>
                            </td>
                          </tr>
                         <tr bgcolor="#E8F3FD">
                            <td class="style8">Father/Husband</td>
                            <td>:</td>
                            <td><input name="jointfather" type="text" class="SL2TextField" value="<%=jointfather%>"></td>
                          </tr>
                         <tr bgcolor="#E8F3FD">
                            <td class="style8">Occupation</td>
                            <td>:</td>
                            <td><input name="jointoccupation" type="text" class="SL2TextField" value="<%=jointoccupation%>"></td>
                          </tr>
                         <tr bgcolor="#E8F3FD">
                            <td class="style8">Telephone</td>
                            <td>:</td>
                            <td><input name="jointtelephone"  class="SL2TextField" type="text" value="<%=jointtelephone%>"></td>
                          </tr>
                         <tr bgcolor="#E8F3FD">
                           <td class="style8">Fax</td>
                            <td>:</td>
                            <td><input name="jointfax"  class="SL2TextField" type="text" value="<%=jointfax%>"></td>
                         </tr>
                         <tr bgcolor="#E8F3FD">
                           <td class="style8">E-mail</td>
                           <td>:</td>
                           <td><input name="jointemail" class="SL2TextField" type="text" value="<%=jointemail%>"></td>
                         </tr>
                         <tr bgcolor="#E8F3FD">
                           <td class="style8" valign="top">Address</td>
                           <td valign="top">:</td>
                           <td valign="top"><textarea name="jointaddress" class="ML9TextField"><%=jointaddress%></textarea></td>
                         </tr>
                         <tr bgcolor="#E8F3FD">
                           <td class="style8">Contact Person</td>
                           <td>:</td>
                           <td><input name="jointcontactperson" type="text" class="SL2TextField" value="<%=jointcontactperson%>"></td>
                         </tr>
                         <tr bgcolor="#E8F3FD">
                           <td class="style8" valign="top">Remarks</td>
                           <td valign="top">:</td>
                           <td valign="top"><textarea name="jointremarks" class="ML9TextField"><%=jointremarks%></textarea></td>
                         </tr>
                         <tr bgcolor="#E8F3FD">
                           <td valign="top" class="style8">Signature (*.gif)</td>
                           <td valign="top">:</td>
                                    <%
                                         if(dbstatus!=null && dbstatus.equals("actual"))
                                            tablename = "JOINT_N_VIEW";
                                          else
                                            tablename = "JOINT_N_TEMP_VIEW";

                                        String selectQuery="SELECT SIGNATURE FROM " + tablename + " WHERE folio_no = '" + foliono + "'  AND JOINT=1 ";
                                        byte[] imgData =  null;


                                        if(!jointname.equals(""))
                                         {
                                           imgData =  cm.getPhoto( cm.getConnection(), selectQuery  );
                                           if(imgData.length == 0)
                                            imgData = null;
                                         }

                                        %>
                                    <td><table width="149">
                                      <tr><td width="41">
                                      <%
                                         if(imgData != null)
                                         {
                                      %>
                                        <img alt="Joint One Signature" src="image.jsp?selectQuery=<%=selectQuery%>" /></td>
                                        <%
                                         }
                                         else
                                         {
                                         %>
                                         <b class="style8">(Signature Unavailable)</b>
                                           <%
                                           }
                                         %>
                                           <td width="96">
<!--                                             <input type="button" name="ButtonSignature" value="Edit" onClick="editSignatureWindow()">-->
                                           &nbsp;<img name="B4" src="<%=request.getContextPath()%>/images/btnEdit.gif"  onclick="editSignatureWindow()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnEditR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnEdit.gif'">
                                           <input type="hidden" name="joint1signature">
                                           <input type="hidden" name="joint1signaturestatus" value="edit"/>
                                                    </td>
                                      </tr></table></td>
                         </tr>
                         <tr bgcolor="#E8F3FD">
                            <td valign="top" class="style8">Photo (*.gif)</td>
                             <td valign="top">:</td>
                                  <%
                                         if(dbstatus!=null && dbstatus.equals("actual"))
                                            tablename = "JOINT_N_VIEW";
                                          else
                                            tablename = "JOINT_N_TEMP_VIEW";

                                         selectQuery="SELECT PHOTO FROM " + tablename +" WHERE folio_no = '" + foliono + "'  AND JOINT=1 ";

                                        imgData = null;
                                         if(!jointname.equals(""))
                                         {
                                           imgData =  cm.getPhoto( cm.getConnection(), selectQuery  );
                                           if(imgData.length == 0)
                                            imgData = null;
                                         }

                                  %>
                                  <td width="78%"><table width="149">
                                    <tr><td width="51">
                                    <%
                                       if(imgData != null)
                                       {
                                    %>
                                      <img alt="Share Holder Photo" src="image.jsp?selectQuery=<%=selectQuery%>" /></td>
                                      <%
                                       }
                                       else
                                       {
                                       %>
                                       <b class="style8">(Photo Unavailable)</b>
                                         <%
                                         }
                                       %>
                                         <td width="86">
<!--                                           <input type="button" name="ButtonPhoto" value="Edit" onClick="editPhotoWindow()">-->
                                           &nbsp;<img name="B5" src="<%=request.getContextPath()%>/images/btnEdit.gif"  onclick="editPhotoWindow()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnEditR.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnEdit.gif'">
                                         <input type="hidden" name="joint1photo">
                                         <input type="hidden" name="joint1photostatus" value="edit"/>
                                        </td>
                  </tr></table></td>
                         </tr>
    <table width="100%" BORDER=0  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
      <tr bgcolor="#E8F3FD">
        <td align="center" valign="top"><br>
 		<img name="B3" src="<%=request.getContextPath()%>/images/btnCancel.gif"  onclick="self.close()" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnCancelOn.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnCancel.gif'">
        </td>
        <td align="center" valign="top"><br>
                <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
        </td>
        <td align="center" valign="top"><br>
	       <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
               <br>&nbsp;
      </td>
      </tr>
</table>

			</table>

                        </td>
                    </tr>

  </table>
  </span>
  <br>
      </div>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>


