
<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Creation Date : December 2005
'Page Purpose  : Adds Second Joint Share Holder .
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {
        color: #0A2769;
        font-weight: bold;
	font-size: 13px;
        }
.style12 {color: #FFFFFF}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Add Second Joint Shareholder Details</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function openSignatureWindow()
{
	window.open("UploadJoint2Signature.jsp","","HEIGHT=350,WIDTH=500")
}
function openPhotoWindow()
{
	window.open("UploadJoint2Photo.jsp","","HEIGHT=350,WIDTH=500")
}

//Execute while click on Submit
function SubmitThis() {

  //BlankValidate('joint2name','- Joint Holder''s Name (Option must be entered)');

  //Enter the values in NewFolio.jsp hidden fields
  if(document.forms[0].joint2name.value == null || document.forms[0].joint2name.value == "" )
  {
    alert("Please enter Joint Holder's Name");
  }
  else
  {
   window.opener.document.forms[0].joint2prefix.value = document.forms[0].joint2prefix.value
   window.opener.document.forms[0].joint2name.value = document.forms[0].joint2name.value
   window.opener.document.forms[0].joint2relation.value = document.forms[0].joint2relation.value
   window.opener.document.forms[0].joint2father.value = document.forms[0].joint2father.value
   window.opener.document.forms[0].joint2occupation.value = document.forms[0].joint2occupation.value
   window.opener.document.forms[0].joint2telephone.value = document.forms[0].joint2telephone.value
   window.opener.document.forms[0].joint2fax.value = document.forms[0].joint2fax.value
   window.opener.document.forms[0].joint2email.value = document.forms[0].joint2email.value
   window.opener.document.forms[0].joint2address.value = document.forms[0].joint2address.value
   window.opener.document.forms[0].joint2contactperson.value = document.forms[0].joint2contactperson.value
   window.opener.document.forms[0].joint2remarks.value = document.forms[0].joint2remarks.value
   window.opener.document.forms[0].joint2signature.value = document.forms[0].joint2signature.value
   window.opener.document.forms[0].joint2photo.value = document.forms[0].joint2photo.value

  self.close()
  }
}
//-->
</script>
<style type="text/css">
<!--
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
%>

<form action="" method="post" name="FileForm">

  <span class="style7">
  <table width="100%"  border="1" bordercolor="#06689E" style="border-collapse: collapse" cellpadding="0">
    <tr>
      <td><table width="100%"  border="0" cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
        <tr bgcolor="#E8F3FD">
          <td bgcolor="#0044B0" width="44%" class="style7">Joint Two Details</td>
          <td bgcolor="#0044B0" width="1%" class="style7">&nbsp;</td>
          <td bgcolor="#0044B0" width="55%" class="style7">&nbsp;</td>
          </tr>
               <tr bgcolor="#E8F3FD">
                <td class="style8">Prefix</td>
                <td>:</td>
                <td><input name="joint2prefix" type="text" class="SL2TextField"></td>
              </tr>
              <tr bgcolor="#E8F3FD">
                <td  class="style8">Joint Name</td>
                <td>:</td>
                <td><input name="joint2name" type="text" class="SL2TextField"></td>
              </tr>
              <tr bgcolor="#E8F3FD">
                <td  class="style8">Relation</td>
                <td>:</td>
                <td><select name="joint2relation">
                  <option value="S/O">S/O</option>
                  <option value="W/O">W/O</option>
                  <option value="D/O">D/O</option>
                                </select></td>
              </tr>
              <tr bgcolor="#E8F3FD">
                <td  class="style8">Father/Husband</td>
                <td>:</td>
                <td><input name="joint2father" type="text" class="SL2TextField"></td>
              </tr>
              <tr bgcolor="#E8F3FD">
                <td  class="style8">Occupation</td>
                <td>:</td>
                <td><input name="joint2occupation" type="text" class="SL2TextField"></td>
              </tr>
              <tr bgcolor="#E8F3FD">
                <td  class="style8">Telephone</td>
                <td>:</td>
                <td><input name="joint2telephone" type="text" class="SL2TextField" ></td>
              </tr>
              <tr bgcolor="#E8F3FD">
                <td  class="style8">Fax</td>
                <td>:</td>
                <td><input name="joint2fax" type="text" class="SL2TextField"></td>
              </tr>
              <tr bgcolor="#E8F3FD">
                <td  class="style8">E-mail</td>
                <td>:</td>
                <td><input name="joint2email" type="text" class="SL2TextField"></td>
              </tr>
              <tr bgcolor="#E8F3FD">
                <td  class="style8" valign="top">Address</td>
                <td valign="top">:</td>
                <td><textarea name="joint2address" class="ML9TextField"></textarea></td>
              </tr>
              <tr bgcolor="#E8F3FD">
                <td  class="style8">Contact Person</td>
                <td>:</td>
                <td><input name="joint2contactperson" type="text" class="SL2TextField"></td>
              </tr>
              <tr bgcolor="#E8F3FD">
                <td  class="style8">Remarks</td>
                <td valign="top">:</td>
                <td><textarea name="joint2remarks" class="ML9TextField"></textarea></td>
              </tr>
              <tr bgcolor="#E8F3FD">
                <td  class="style8" valign="top">Signature (*.gif)</td>
                <td valign="top">:</td>
                <td><input name="joint2signature" type="text" class="SL2TextField" readonly="readonly">
<!--					<input name="btnSignature" type="button" value="Upload" onclick="openSignatureWindow()">-->
                                        <br><img name="B4" src="<%=request.getContextPath()%>/images/btnUpload.gif"  onclick="openSignatureWindow()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnUploadR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnUpload.gif'">
				</td>
              </tr>
              <tr bgcolor="#E8F3FD">
                <td  class="style8">Photo (*.gif)</td>
                <td valign="top">:</td>
                <td><input name="joint2photo" type="text" class="SL2TextField" readonly="readonly">
<!--					<input name="btnPhoto" type="button" value="Upload" onclick="openPhotoWindow()">-->
                                        <br><img name="B5" src="<%=request.getContextPath()%>/images/btnUpload.gif"  onclick="openPhotoWindow()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnUploadR.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnUpload.gif'">
				</td>
              </tr>
            </table>
      <table width="100%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr bgcolor="#E8F3FD">
        <td align="center">
 		<img name="B3" src="<%=request.getContextPath()%>/images/btnCancel.gif"  onclick="self.close()" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnCancelOn.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnCancel.gif'">
        </td>
        <td align="center">
                <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
        </td>
        <td align="center">
	       <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
      </td>
      </tr>
</table>

            </td>
		  </tr>
  </table>
  </span>
  <br>
      </div>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>

