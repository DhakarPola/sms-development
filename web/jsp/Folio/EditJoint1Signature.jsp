<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Updated By    : Renad Hakim
'******************************************************************************************************************************************
*/
</script>


<%@ page language="java" import="oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*,org.apache.commons.fileupload.*,java.io.*,java.util.*,java.util.List,java.util.Vector,java.util.Date,java.text.SimpleDateFormat" %>
<html>
<title>Upload Joint 1 Signature</title>
<head>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function formconfirm()
{
  if (confirm("Please Upload"))
  {
    return true;
  }
}

function confirmDelete()
{
  if (confirm("Do you really want to delete the existing signature?"))
  {
    return true;
  }
}

function confirmReplace()
{
  if (confirm("Do you really want to replace the existing signature?"))
  {
    return true;
  }
}

//Execute while click on Submit
function SubmitThis() {


  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
function deleteSignature()
{
  if(confirmDelete())
  {
    window.opener.document.forms[0].joint1signaturestatus.value = "delete"
    self.close()
  }
}
function attachPicture()
{
  if(confirmReplace())
  {
    var regexp = /\\/g;
    window.opener.document.forms[0].joint1signature.value = document.forms[0].path.value.replace(regexp,"\\\\");
    window.opener.document.forms[0].joint1signaturestatus.value = "add"
    self.close()
  }
}
function viewPreview()
{
    var regexp = /\\/g;
    window.opener.document.forms[0].joint1signature.value = document.forms[0].path.value.replace(regexp,"\\\\");
}
function closeSig()
{
  self.close();
}

//-->
</script>

<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}

.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {
        color: #0A2769;
        font-weight: bold;
	font-size: 13px;
        }
.style12 {color: #FFFFFF}
-->
</style>
</head>
<body bgcolor="#ffffff">
<form action="EditJoint1Signature.jsp" method="post" enctype="multipart/form-data"  name="newUploadSignatureForm">
  <table width="100%"  border="1" bordercolor="#06689E" style="border-collapse: collapse" cellpadding="0">
    <tr>
      <td><table width="100%"  border="0" cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
        <tr bgcolor="#E8F3FD">
          <td width="44%" bgcolor="#0044B0" class="style7">Upload User Signature</td>
          <td width="1%" bgcolor="#0044B0" class="style7">&nbsp;</td>
          <td width="55%%" bgcolor="#0044B0" class="style7">&nbsp;</td>
          </tr>
       <tr bgcolor="#E8F3FD">
          <td class="style8">Upload New Signature</td>
          <td>:</td>
          <td><input name="signaturepath" type="file" id="signaturepath"  class="SL74TextField"></td>
       </tr>
      <%
        String CurrentFile = "";
        String fname ="";

        String signaturepath=null;


        DiskFileUpload u1 = new DiskFileUpload();

        System.out.println(request.getContentLength());
        if(request.getContentLength()>0)
        {
          List items1 = u1.parseRequest(request);

          Iterator itr1 = items1.iterator();

          File savedFile=null ;
          File fullFile=null ;

          List items = items1;
          Iterator itr = items.iterator();

          FileItem item = (FileItem) itr.next();

          while(itr1.hasNext())
          {
            FileItem item1 = (FileItem) itr1.next();
            String fieldName = item1.getFieldName();
            if(fieldName.equals("signaturepath"))
            {
              CurrentFile = item1.getName();
              fullFile  = new File(item1.getName());
              fname =  fullFile.getName();
              item = item1;
              signaturepath =fullFile.getPath();
            }
          }//end of while
        }
        if(signaturepath !=null)
        {
        %>
        <tr bgcolor="#E8F3FD">
	<td valign="top" class="style8">Preview</td>
          <td valign="top">:</td>
        <td valign="top"><img name="imgsignature" alt="" src="<%=signaturepath%>"/>
        </td>
        </tr>
        <%
            }
        %>
  <table width="100%" BORDER=0  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
    <tr bgcolor="#E8F3FD">
          <td align="center">
            <input type="hidden" name="path" value="<%=signaturepath%>"/>
            <input type="hidden" name="signaturestatus" />
            <img  name = "B1" src="<%=request.getContextPath()%>/images/btnUpload.gif" onclick="submit()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUploadR.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpload.gif'" />
            &nbsp;&nbsp;
            <img  name = "B2" src="<%=request.getContextPath()%>/images/btnAdd.gif"  onclick="attachPicture()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnAddOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnAdd.gif'"/>
            &nbsp;&nbsp;
            <img  name = "B3" src="<%=request.getContextPath()%>/images/btnDelete.gif" onclick="deleteSignature()" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnDeleteOn.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnDelete.gif'"/>
            &nbsp;&nbsp;
            <img name="B4" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="closeSig()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnClose.gif'"/>
          </td>
    </tr>

  </table>
      </table></td>
    </tr>
  </table>
</form>
</body>
</html>
