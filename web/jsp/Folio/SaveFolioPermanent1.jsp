<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Creation Date : November 2005
'Page Purpose  : Save new Folio.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.text.DateFormat"%>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Folio</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
     String count_s=request.getParameter("COUNTER");
     int COUNT = Integer.parseInt(count_s) ;

for(int i=1;i<=COUNT;i++)
{

    String foliono=request.getParameter("folioid" + String.valueOf(i)) ;
    String foliostatus= request.getParameter("foliostatus" + String.valueOf(i));


    // Variable declaration and initialization for Shareholder
    int ijoint=0;
    String category = "";
    String prefix = "";
    String shareholdername = "";
    String surname = "";
    String relation = "";
    String father = "";
    String resident = "";
    String nationality = "";
    String occupation = "";
    String address1 = "";
    String address2 = "";
    String zipcode = "";
    String telephone = "";
    String cellphone = "";
    String email = "";
    String fax = "";
    String address3 = "";
    String address4 = "";
    String accountno = "";
    String bankname = "";
    String bankbranch = "";
    String tin = "";
    String taxfreeshare = "";
    String entrydate = "";
    String nomineeno = "";
    String jointno="";
    String total_shares = "";


    // Variable declaration and initialization for Joint 1
     String jointprefix ="";
     String jointname ="";
     String jointrelation ="";
     String jointfather ="";
     String jointoccupation ="";
     String jointtelephone ="";
     String jointfax ="";
     String jointemail ="";
     String jointaddress ="";
     String jointaddress2 ="";
     String jointaddress3 ="";
     String jointaddress4 ="";
     String jointcontactperson ="";
     String jointremarks ="";


    // Variable declaration and initialization for Joint 1
     String joint2prefix ="";
     String joint2name ="";
     String joint2relation ="";
     String joint2father ="";
     String joint2occupation ="";
     String joint2telephone ="";
     String joint2fax ="";
     String joint2email ="";
     String joint2address ="";
     String joint2address2 ="";
     String joint2address3 ="";
     String joint2address4 ="";
     String joint2contactperson ="";
     String joint2remarks ="";



    if(foliostatus==null)
    foliostatus="";


    //Retrives shareholder information from Shareholder_temp table
    String queryFolio = "SELECT * FROM SHAREHOLDER_TEMP_VIEW WHERE FOLIO_NO = '" + foliono+"'";

    cm.queryExecute(queryFolio);
    while (cm.toNext())
    {

      jointno  = String.valueOf(cm.getColumnS("JOINT"));

      category = cm.getColumnS("CLASS");
      prefix = cm.getColumnS("PREFIX");
      shareholdername = cm.getColumnS("NAME");

      surname = cm.getColumnS("SURNAME");
      relation = cm.getColumnS("RELATION");
      father = cm.getColumnS("FATHER");
      resident = cm.getColumnS("RESIDENT");
      nationality = cm.getColumnS("NATIONALITY");
      occupation = cm.getColumnS("OCCUPATION");
      address1 = cm.getColumnS("ADDRESS1");
      address2 = cm.getColumnS("ADDRESS2");
      zipcode = cm.getColumnS("ZIP");


      telephone = cm.getColumnS("TELEPHONE");
      cellphone = cm.getColumnS("CELLPHONE");
      email = cm.getColumnS("EMAIL");
      fax = cm.getColumnS("FAX");

      address3 = cm.getColumnS("ADDRESS3");
      address4 = cm.getColumnS("ADDRESS4");
      accountno = cm.getColumnS("ACCOUNT_NO");
      bankname = cm.getColumnS("BANK_NAME");
      bankbranch = cm.getColumnS("BANK_BRANCH");
      tin = cm.getColumnS("TIN");
      taxfreeshare = cm.getColumnS("TAX_FREE_SHARE");
      entrydate = cm.getColumnS("ENTRY_DATE");
      nomineeno = cm.getColumnS("NOMINEE_NO");
      total_shares = cm.getColumnS("TOTAL_SHARE");

       if(entrydate==null || entrydate.equals("null"))
           entrydate="null";
       else
          entrydate = "'"+entrydate+"'";

       if(taxfreeshare==null || taxfreeshare.equals("null"))
        taxfreeshare="0";

      if(total_shares==null || total_shares.equals("null"))
        total_shares="0";

      if(entrydate==null || entrydate.equals("null"))
          jointno ="0";

    }//end of inner while




      //Adds Shareholder information to SHAREHOLDER table
      String nfolio = "call ADD_SHAREHOLDER('" + foliono  + "', '" + prefix  + "', '" + shareholdername   + "', '" + surname + "', '" + relation + "', '" + father + "', '" + address1 + "', '" +address2 + "', '" + address3 +  "', '" + address4  + "', '" +zipcode    + "', '" + occupation  + "', " + jointno + ", '" + category  + "', '" +  accountno + "', '" +  bankname  + "', '" +  bankbranch  + "', "+total_shares+" , " +  taxfreeshare  + ", 'F' ,'F', '" + resident   + "', '" + nationality  + "', '"  +nomineeno + "', 'T', " + entrydate +  ", null, null,'" + telephone + "', '" + cellphone + "', '" + email + "', '" + fax + "', '" + tin   +"', null )";
      boolean b = cm.procedureExecute(nfolio);


      //Retrieves Joint Holder 1 information from temporary table
      String queryJoint1 = "SELECT * FROM JOINT_N_TEMP_VIEW WHERE FOLIO_NO = '" + foliono + "' AND JOINT = 1" ;
      cm.queryExecute(queryJoint1);
      while(cm.toNext())
        {
          jointprefix=cm.getColumnS("PREFIX");
          jointname= cm.getColumnS("NAME");
          jointrelation = cm.getColumnS("RELATION");
          jointfather = cm.getColumnS("FATHER");
          jointoccupation = cm.getColumnS("OCCUPATION");
          jointtelephone = cm.getColumnS("TELEPHONE");
          jointfax = cm.getColumnS("FAX");
          jointemail = cm.getColumnS("EMAIL");
          jointaddress= cm.getColumnS("ADDRESS1");
          jointaddress2= cm.getColumnS("ADDRESS2");
          jointaddress3= cm.getColumnS("ADDRESS3");
          jointaddress4= cm.getColumnS("ADDRESS4");
          jointcontactperson = cm.getColumnS("CONTACTPERSON");
          jointremarks = cm.getColumnS("REMARKS");

          if (String.valueOf(jointprefix).equals("null"))
          jointprefix = "";
            if (String.valueOf(jointname).equals("null"))
           jointname= "";
            if (String.valueOf(jointrelation).equals("null"))
           jointrelation= "";
            if (String.valueOf(jointfather).equals("null"))
           jointfather= "";
            if (String.valueOf(jointoccupation).equals("null"))
           jointoccupation= "";

            if (String.valueOf(jointtelephone).equals("null"))
           jointtelephone= "";
            if (String.valueOf(jointfax).equals("null"))
           jointfax= "";
            if (String.valueOf(jointemail).equals("null"))
           jointemail= "";
            if (String.valueOf(jointaddress).equals("null"))
           jointaddress= "";
            if (String.valueOf(jointaddress2).equals("null"))
           jointaddress2= "";
            if (String.valueOf(jointremarks).equals("null"))
           jointremarks= "";
           if (String.valueOf(jointaddress3).equals("null"))
           jointaddress3 = "";
           if (String.valueOf(jointaddress4).equals("null"))
           jointaddress4= "";
            if (String.valueOf(jointcontactperson).equals("null"))
           jointcontactperson= "";

        }

      //Retrieves Joint Holder 2 information from temporary table
      String queryJoint2 = "SELECT * FROM JOINT_N_TEMP_VIEW WHERE FOLIO_NO = '" + foliono + "' AND JOINT = 2" ;
      cm.queryExecute(queryJoint2);
      while(cm.toNext())
        {
          joint2prefix=cm.getColumnS("PREFIX");
          joint2name= cm.getColumnS("NAME");
          joint2relation = cm.getColumnS("RELATION");
          joint2father = cm.getColumnS("FATHER");
          joint2occupation = cm.getColumnS("OCCUPATION");
          joint2telephone = cm.getColumnS("TELEPHONE");
          joint2fax = cm.getColumnS("FAX");
          joint2email = cm.getColumnS("EMAIL");
          joint2address= cm.getColumnS("ADDRESS1");
          joint2address2= cm.getColumnS("ADDRESS2");
          joint2address3= cm.getColumnS("ADDRESS3");
          joint2address4= cm.getColumnS("ADDRESS4");
          joint2contactperson = cm.getColumnS("CONTACTPERSON");
          joint2remarks = cm.getColumnS("REMARKS");

          if (String.valueOf(joint2prefix).equals("null"))
          joint2prefix = "";
            if (String.valueOf(joint2name).equals("null"))
           joint2name= "";
            if (String.valueOf(joint2relation).equals("null"))
           joint2relation= "";
            if (String.valueOf(joint2father).equals("null"))
           joint2father= "";
            if (String.valueOf(joint2occupation).equals("null"))
           joint2occupation= "";

            if (String.valueOf(joint2telephone).equals("null"))
           joint2telephone= "";
            if (String.valueOf(joint2fax).equals("null"))
           joint2fax= "";
            if (String.valueOf(joint2email).equals("null"))
           joint2email= "";
            if (String.valueOf(joint2address).equals("null"))
           joint2address= "";
            if (String.valueOf(joint2address2).equals("null"))
           joint2address2= "";
            if (String.valueOf(joint2remarks).equals("null"))
           joint2remarks= "";
           if (String.valueOf(joint2address3).equals("null"))
           joint2address3 = "";
           if (String.valueOf(joint2address4).equals("null"))
           joint2address4= "";
            if (String.valueOf(joint2contactperson).equals("null"))
           joint2contactperson= "";

        }


    if(!jointname.equals(""))
    {
      String procJoint=" call ADD_JOINT_N('" + foliono  + "', 1, '" + jointprefix+"' , '" + jointname + "', '" + jointrelation  + "', '" + jointfather + "', '" + jointaddress  + "', '" +jointaddress2  + "', '" + jointaddress3 + " ', '" + jointaddress4 + " ', '" + zipcode    + "', '" + occupation  + "', null,'"+  jointtelephone+ "','"+jointemail +"','"+jointfax+"','"+jointcontactperson+"','"+jointremarks+"', null)";
      boolean c = cm.procedureExecute(procJoint);

    }

    if(!joint2name.equals(""))
    {
       String procJoint2=" call ADD_JOINT_N('" + foliono  + "', 2 , '"+joint2prefix+ "' , '" + joint2name + "', '" + joint2relation  + "', '" + joint2father + "', '" + joint2address  + "', '" + joint2address2 + "', '" + joint2address3 + " ', '" + joint2address4 + " ', '" + zipcode    + "', '" + occupation   + "', null,'"+  joint2telephone+ "','"+joint2email +"','"+joint2fax+"','"+joint2contactperson+"','"+jointremarks+"', null)";
       boolean d = cm.procedureExecute(procJoint2);
    }


    //Transfer Signature & Photo
    String updateSignQuery = " call TRANSFER_SIGNATURE('"+foliono+"')";
    cm.procedureExecute(updateSignQuery);





    // Delete Shareholder from Temporary table
    String dfolio = "call DELETE_SHAREHOLDER_TEMP('" + foliono + "')";
    boolean b1 = cm.procedureExecute(dfolio);

    // Delete Joint Holder from Temporary table
    String deleteJoint="call DELETE_JOINT_N_TEMP('" + foliono + "')";
    boolean e = cm.procedureExecute(deleteJoint);

     if(resident == null)
       resident="F";
    else
       resident="T";


    if(category=="null")
       category="";
}//end of outer while

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added Folio Information')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript" >
  location = "<%=request.getContextPath()%>/jsp/Folio/AcceptFolios.jsp";
</script>
</body>
</html>
