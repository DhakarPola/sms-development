<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi
'Updated By    : Renad Hakim
'Updated By    : Md. Kamruzzaman
'Creation Date : October 2005
'Page Purpose  : Updates folio information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<%@ page language="java" import="oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*,org.apache.commons.fileupload.*,java.io.*,java.util.*,java.util.List,java.util.Vector,java.util.Date,java.text.SimpleDateFormat" %>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update Bank Information</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

     String foliono = String.valueOf(request.getParameter("foliono"));


     String category = String.valueOf(request.getParameter("category")).trim();
     String ndetails = String.valueOf(request.getParameter("ndetails"));
     String prefix = String.valueOf(request.getParameter("prefix"));
     String shareholdername = String.valueOf(request.getParameter("shareholdername"));
     String surname = String.valueOf(request.getParameter("surname"));
     String relation = String.valueOf(request.getParameter("relation"));
     String father = String.valueOf(request.getParameter("father"));
     String resident = String.valueOf(request.getParameter("resident"));
     String active = String.valueOf(request.getParameter("activity"));
     String nationality = String.valueOf(request.getParameter("nationality"));
     String occupation = String.valueOf(request.getParameter("occupation"));
     String address1 = String.valueOf(request.getParameter("address1"));
     String address2 = "";//String.valueOf(request.getParameter("address2"));
     String zipcode = String.valueOf(request.getParameter("zipcode"));
     String telephone = String.valueOf(request.getParameter("telephone"));
     String cellphone = String.valueOf(request.getParameter("cellphone"));
     String email = String.valueOf(request.getParameter("email"));
     String fax = String.valueOf(request.getParameter("fax"));
     String address3 = "";//String.valueOf(request.getParameter("address3"));
     String address4 = "";//String.valueOf(request.getParameter("address4"));
     String accountno = String.valueOf(request.getParameter("accountno"));
     String bankname = String.valueOf(request.getParameter("bankname"));
     String bankbranch = String.valueOf(request.getParameter("bankbranch"));
     String tin = String.valueOf(request.getParameter("tin"));
	 
     String nid = String.valueOf(request.getParameter("nid"));  // Addded on 01 10 2014
     String routingno = String.valueOf(request.getParameter("routingno")); // Addded on 01 10 2014
	 	 
     String taxfreeshare = String.valueOf(request.getParameter("taxfreeshare"));
     String entrydate = String.valueOf(request.getParameter("entrydate"));
     String nomineeno = String.valueOf(request.getParameter("nomineeno"));
     String jointno=String.valueOf(request.getParameter("joint"));
     String signature = String.valueOf(request.getParameter("signature"));
     String signaturestatus = String.valueOf(request.getParameter("signaturestatus"));
     String photo = String.valueOf(request.getParameter("photo"));
     String total_shares = String.valueOf(request.getParameter("totalshares"));
     String photostatus = String.valueOf(request.getParameter("photostatus"));
     String dbstatus = request.getParameter("dbstatus");
     String sisliable = String.valueOf(request.getParameter("boxliable"));
     String sissptax = String.valueOf(request.getParameter("boxsptax"));
     //signature=cm.replace(signature,"^","//");
    //signature="D:/BBIntranet/images/1.gif";
//System.out.println(signature);
//signature="D:/mybosig/"+signature;

signature=signature;
System.out.println(signature);
     String jointprefix = String.valueOf(request.getParameter("jointprefix"));
     String jointname = String.valueOf(request.getParameter("jointname"));

     String jointrelation = String.valueOf(request.getParameter("jointrelation"));
     String jointfather = String.valueOf(request.getParameter("jointfather"));
     String jointoccupation = String.valueOf(request.getParameter("jointoccupation"));
     String jointtelephone = String.valueOf(request.getParameter("jointtelephone"));
     String jointfax = String.valueOf(request.getParameter("jointfax"));
     String jointemail = String.valueOf(request.getParameter("jointemail"));
     String jointaddress = String.valueOf(request.getParameter("jointaddress"));
     String jointaddress2 = "";
     String jointaddress3 = "";
     String jointaddress4 = "";
     String jointcontactperson = String.valueOf(request.getParameter("jointcontactperson"));
     String jointremarks = String.valueOf(request.getParameter("jointremarks"));
     String jointsignature = String.valueOf(request.getParameter("joint1signature"));
     String jointphoto = String.valueOf(request.getParameter("joint1photo"));
     String jointphotostatus = String.valueOf(request.getParameter("joint1photostatus"));
     String jointsignaturestatus = String.valueOf(request.getParameter("joint1signaturestatus"));



     String joint2prefix = String.valueOf(request.getParameter("joint2prefix"));
     String joint2name = String.valueOf(request.getParameter("joint2name"));
     String joint2relation = String.valueOf(request.getParameter("joint2relation"));
     String joint2father = String.valueOf(request.getParameter("joint2father"));
     String joint2occupation = String.valueOf(request.getParameter("joint2occupation"));
     String joint2telephone = String.valueOf(request.getParameter("joint2telephone"));
     String joint2fax = String.valueOf(request.getParameter("joint2fax"));
     String joint2email = String.valueOf(request.getParameter("joint2email"));
     String joint2address = String.valueOf(request.getParameter("joint2address"));
     String joint2address2 = "";
     String joint2address3 = "";
     String joint2address4 = "";
     String joint2contactperson = String.valueOf(request.getParameter("joint2contactperson"));
     String joint2remarks = String.valueOf(request.getParameter("joint2remarks"));
     String joint2signature = String.valueOf(request.getParameter("joint2signature"));
     String joint2photo = String.valueOf(request.getParameter("joint2photo"));
     String joint2photostatus = String.valueOf(request.getParameter("joint2photostatus"));
     String joint2signaturestatus = String.valueOf(request.getParameter("joint2signaturestatus"));
     String specilaTaxVal = String.valueOf(request.getParameter("spTax"));

     if(specilaTaxVal.equals("null")||specilaTaxVal.equals(""))
     {
       specilaTaxVal="0";
     }

      signature = cm.replace(signature,"'","''");
      taxfreeshare = cm.replace(taxfreeshare,"'","''");
      entrydate = cm.replace(entrydate,"'","''");
      foliono = cm.replace(foliono,"'","''");
      ndetails = cm.replace(ndetails,"'","''");
      category = cm.replace(category,"'","''");
      prefix =cm.replace(prefix,"'","''");
      shareholdername = cm.replace(shareholdername,"'","''");
      surname = cm.replace(surname,"'","''");
      relation = cm.replace(relation,"'","''");
      father = cm.replace(father,"'","''");
      resident = cm.replace(resident,"'","''");
      active = cm.replace(active,"'","''");
      nationality =cm.replace(nationality,"'","''");
      occupation = cm.replace(occupation,"'","''");
      address1 = cm.replace(address1,"'","''");
      address2 = cm.replace(address2,"'","''");
      zipcode = cm.replace(zipcode,"'","''");
      telephone = cm.replace(telephone,"'","''");
      cellphone = cm.replace(cellphone,"'","''");
      email = cm.replace(email,"'","''");
      fax = cm.replace(fax,"'","''");
      address3 = cm.replace(address3,"'","''");
      address4 = cm.replace(address4,"'","''");
      accountno = cm.replace(accountno,"'","''");
      bankname = cm.replace(bankname,"'","''");
      bankbranch = cm.replace(bankbranch,"'","''");
      tin = cm.replace(tin,"'","''");
	  nid = cm.replace(nid,"'","''");
	  routingno = cm.replace(routingno,"'","''");
      taxfreeshare = cm.replace(taxfreeshare,"'","''");
      entrydate = cm.replace(entrydate,"'","''");
      nomineeno = cm.replace(nomineeno,"'","''");


      jointprefix = cm.replace(jointprefix,"'","''");
      jointname = cm.replace(jointname,"'","''");
      jointrelation = cm.replace(jointrelation,"'","''");
      jointfather = cm.replace(jointfather,"'","''");
      jointoccupation = cm.replace(jointoccupation,"'","''");
      jointtelephone = cm.replace(jointtelephone,"'","''");
      jointfax = cm.replace(jointfax,"'","''");
      jointemail = cm.replace(jointemail,"'","''");
      jointaddress = cm.replace(jointaddress,"'","''");
      jointaddress2 = cm.replace(jointaddress2,"'","''");
      jointaddress3 = cm.replace(jointaddress3,"'","''");
      jointaddress4 = cm.replace(jointaddress4,"'","''");
      jointcontactperson = cm.replace(jointcontactperson,"'","''");
      jointremarks = cm.replace(jointremarks,"'","''");



      joint2prefix = cm.replace(joint2prefix,"'","''");
      joint2name = cm.replace(joint2name,"'","''");
      joint2relation = cm.replace(joint2relation,"'","''");
      joint2father = cm.replace(joint2father,"'","''");
      joint2occupation = cm.replace(joint2occupation,"'","''");
      joint2telephone = cm.replace(joint2telephone,"'","''");
      joint2fax = cm.replace(joint2fax,"'","''");
      joint2email = cm.replace(joint2email,"'","''");
      joint2address = cm.replace(joint2address,"'","''");
      joint2address2 = cm.replace(joint2address2,"'","''");
      joint2address3 = cm.replace(joint2address3,"'","''");
      joint2address4 = cm.replace(joint2address4,"'","''");
      joint2contactperson = cm.replace(joint2contactperson,"'","''");
      joint2remarks = cm.replace(joint2remarks,"'","''");

      if(prefix.equals("--Please Select--"))
      prefix="";
      if(relation.equals("--Please Select--"))
      relation="";

    if (String.valueOf(ndetails).equals("null"))
     ndetails = "";

  int no_of_joints =0;
  if(!jointname.equals(""))
    no_of_joints++;
  if(!joint2name.equals(""))
    no_of_joints++;

    if(!resident.equalsIgnoreCase("T"))
       resident="F";
    else
       resident="T";

    if(!active.equalsIgnoreCase("T"))
       active="F";
    else
       active="T";

    if(!sisliable.equalsIgnoreCase("T"))
       sisliable="F";
    else
       sisliable="T";

    //System.out.println("sissptax= "+sissptax);
    if(String.valueOf(sissptax).equals("on")||String.valueOf(sissptax).equals("T"))
       sissptax="T";
    else
       sissptax="F";

    if(category=="null")
       category="";

    String updateFolio = "";
    String updateJoint1="";
    String updateJoint2="";

    if(signaturestatus==null) signaturestatus="";
    if(photostatus==null) photostatus="";

    String proc_name="";

    /*System.out.println("signaturestatus : "+signaturestatus);
    System.out.println("photostatus : "+photostatus);
    System.out.println("jointsignaturestatus : "+jointsignaturestatus);
    System.out.println("jointphotostatus : "+jointphotostatus);
    System.out.println("joint2signature : "+joint2signature);
    System.out.println("joint2photostatus : "+joint2photostatus);*/

    if(sissptax.equals("T"))
    {
      String seletSpTax="Select * from special_tax where folio_no='" + foliono + "'";
      String inserSpTax="insert into special_tax  values ('" + foliono + "','" + specilaTaxVal + "')";
      String updateSpTax="update special_tax set amount='" + specilaTaxVal + "' where folio_no='" + foliono + "'";
      try
      {
       int found=cm.queryExecuteCount(seletSpTax);
       //System.out.println("found= "+found);
       if(found>0)
       {
         cm.queryExecute(updateSpTax);
       }
       else
       {
         cm.queryExecute(inserSpTax);
       }
      }catch(Exception e){}
    }
    else if(sissptax.equals("F"))
    {
      String deletSpTax="delete from special_tax where folio_no='" + foliono + "'";
       try
      {
      cm.queryExecute(deletSpTax);
      }catch(Exception e){}
    }

    if(dbstatus!=null && dbstatus.equals("actual"))
      proc_name="";
    else
      proc_name="_TEMP";


      if((signaturestatus.equals("add") && photostatus.equals("add")) || (signaturestatus.equals("delete") && photostatus.equals("delete")))
          updateFolio = "call UPDATE_SHAREHOLDER"+proc_name +"('" + foliono  + "', '" + prefix  + "', '" + shareholdername   + "', '" + surname + "', '" + relation + "', '" + father + "', '" + address1 + "', '" +address2 + "', '" + address3 +  "', '" + address4  + "', '" +zipcode    + "', '" + occupation  + "', " + no_of_joints + ", '" + category  + "', '" +  accountno + "', '" +  bankname  + "', '" +  bankbranch  + "', "+total_shares +" , '" +  taxfreeshare  + "', '" + sisliable + "' ,'" + sissptax + "', '" + resident   + "', '" + nationality  + "', '"  +nomineeno + "', '" + active +  "', '" + entrydate +  "', null, null,'" + telephone + "', '" + cellphone + "', '" + email + "', '" + fax + "', '" + tin   +"',null,'" + ndetails + "', '" + nid + "', '" + routingno +"')";
      else
      if((signaturestatus.equals("add") && photostatus.equals("edit"))  || (signaturestatus.equals("delete") && photostatus.equals("edit")))
          updateFolio = "call UPDATE_SHAREHOLDER"+proc_name +"_S('" + foliono  + "', '" + prefix  + "', '" + shareholdername   + "', '" + surname + "', '" + relation + "', '" + father + "', '" + address1 + "', '" +address2 + "', '" + address3 +  "', '" + address4  + "', '" +zipcode    + "', '" + occupation  + "', " + no_of_joints + ", '" + category  + "', '" +  accountno + "', '" +  bankname  + "', '" +  bankbranch  + "', "+total_shares+" , '" +  taxfreeshare  + "', '" + sisliable + "' ,'" + sissptax + "', '" + resident   + "', '" + nationality  + "', '"  +nomineeno + "', '" + active +  "', '" + entrydate +  "', null, null,'" + telephone + "', '" + cellphone + "', '" + email + "', '" + fax + "', '" + tin   +" ','" + ndetails + "', '" + nid + "', '" + routingno  +"')";
      else
      if((signaturestatus.equals("edit") && photostatus.equals("add")) || (signaturestatus.equals("edit") && photostatus.equals("delete")))
          updateFolio = "call UPDATE_SHAREHOLDER"+proc_name +"_P('" + foliono  + "', '" + prefix  + "', '" + shareholdername   + "', '" + surname + "', '" + relation + "', '" + father + "', '" + address1 + "', '" +address2 + "', '" + address3 +  "', '" + address4  + "', '" +zipcode    + "', '" + occupation  + "', " + no_of_joints + ", '" + category  + "', '" +  accountno + "', '" +  bankname  + "', '" +  bankbranch  + "', "+total_shares+" , '" +  taxfreeshare  + "', '" + sisliable + "' ,'" + sissptax + "', '" + resident   + "', '" + nationality  + "', '"  +nomineeno + "', '" + active +  "', '" + entrydate +  "', null,'" + telephone + "', '" + cellphone + "', '" + email + "', '" + fax + "', '" + tin   +" ','" + ndetails + "', '" + nid + "', '" + routingno   +"')";
      else
          updateFolio = "call UPDATE_SHAREHOLDER"+proc_name +"_EMPTY('" + foliono  + "', '" + prefix  + "', '" + shareholdername   + "', '" + surname + "', '" + relation + "', '" + father + "', '" + address1 + "', '" +address2 + "', '" + address3 +  "', '" + address4  + "', '" +zipcode    + "', '" + occupation  + "', " + no_of_joints + ", '" + category  + "', '" +  accountno + "', '" +  bankname  + "', '" +  bankbranch  + "', "+total_shares+" , '" +  taxfreeshare  + "', '" + sisliable + "' ,'" + sissptax + "', '" + resident   + "', '" + nationality  + "', '"  +nomineeno + "', '" + active +  "', '" + entrydate +  "', null,'" + telephone + "', '" + cellphone + "', '" + email + "', '" + fax + "', '" + tin   +" ','" + ndetails + "', '" + nid + "', '" + routingno   +"')";



      if(jointsignaturestatus.equals("add") && jointphotostatus.equals("add") || (jointsignaturestatus.equals("delete") && jointphotostatus.equals("delete")))
      updateJoint1=" call UPDATE_JOINT_N"+proc_name +"('" + foliono  + "', 1, '" + jointprefix+"' , '" + jointname + "', '" + jointrelation  + "', '" + jointfather + "', '" + jointaddress  + "', '" +jointaddress2  + "', '" + jointaddress3 + " ', '" + jointaddress4 + " ', '" + zipcode    + "', '" + occupation  + "', null,'"+  jointtelephone+ "','"+jointemail +"','"+jointfax+"','"+jointcontactperson+"',"+ null+")";
      else
      if(jointsignaturestatus.equals("add") && jointphotostatus.equals("edit")  || (jointsignaturestatus.equals("delete") && jointphotostatus.equals("edit")))
      updateJoint1=" call UPDATE_JOINT_N"+proc_name +"_S('" + foliono  + "', 1, '" + jointprefix+"' , '" + jointname + "', '" + jointrelation  + "', '" + jointfather + "', '" + jointaddress  + "', '" +jointaddress2  + "', '" + jointaddress3 + " ', '" + jointaddress4 + " ', '" + zipcode    + "', '" + occupation  + "', null,'"+  jointtelephone+ "','"+jointemail +"','"+jointfax+"','"+jointcontactperson+"')";
      else
      if(jointsignaturestatus.equals("edit") && jointphotostatus.equals("add") || (jointsignaturestatus.equals("edit") && jointphotostatus.equals("delete")))
      updateJoint1=" call UPDATE_JOINT_N"+proc_name +"_P('" + foliono  + "', 1, '" + jointprefix+"' , '" + jointname + "', '" + jointrelation  + "', '" + jointfather + "', '" + jointaddress  + "', '" +jointaddress2  + "', '" + jointaddress3 + " ', '" + jointaddress4 + " ', '" + zipcode    + "', '" + occupation  + "','"+  jointtelephone+ "','"+jointemail +"','"+jointfax+"','"+jointcontactperson +"',"+ null+")";
      else
      updateJoint1=" call UPDATE_JOINT_N"+proc_name +"_EMPTY('" + foliono  + "', 1, '" + jointprefix+"' , '" + jointname + "', '" + jointrelation  + "', '" + jointfather + "', '" + jointaddress  + "', '" +jointaddress2  + "', '" + jointaddress3 + " ', '" + jointaddress4 + " ', '" + zipcode    + "', '" + occupation  + "','"+  jointtelephone+ "','"+jointemail +"','"+jointfax+"','"+jointcontactperson+"')";


      if(joint2signaturestatus.equals("add") && joint2photostatus.equals("add") || (joint2signaturestatus.equals("delete") && joint2photostatus.equals("delete")))
      updateJoint2=" call UPDATE_JOINT_N"+proc_name +"('" + foliono  + "', 2, '" + joint2prefix+"' , '" + joint2name + "', '" + joint2relation  + "', '" + joint2father + "', '" + joint2address  + "', '" +joint2address2  + "', '" + joint2address3 + " ', '" + joint2address4 + " ', '" + zipcode    + "', '" + occupation  + "', null,'"+  joint2telephone+ "','"+joint2email +"','"+joint2fax+"','"+joint2contactperson+"',"+ null+")";
      else
      if(joint2signaturestatus.equals("add") && joint2photostatus.equals("edit")  || (joint2signaturestatus.equals("delete") && joint2photostatus.equals("edit")))
      updateJoint2=" call UPDATE_JOINT_N"+proc_name +"_S('" + foliono  + "', 2, '" + joint2prefix+"' , '" + joint2name + "', '" + joint2relation  + "', '" + joint2father + "', '" + joint2address  + "', '" +joint2address2  + "', '" + joint2address3 + " ', '" + joint2address4 + " ', '" + zipcode    + "', '" + occupation  + "', null,'"+  joint2telephone+ "','"+joint2email +"','"+joint2fax+"','"+joint2contactperson+"')";
      else
      if(joint2signaturestatus.equals("edit") && joint2photostatus.equals("add") || (joint2signaturestatus.equals("edit") && joint2photostatus.equals("delete")))
      updateJoint2=" call UPDATE_JOINT_N"+proc_name +"_P('" + foliono  + "', 2, '" + joint2prefix+"' , '" + joint2name + "', '" + joint2relation  + "', '" + joint2father + "', '" + joint2address  + "', '" +joint2address2  + "', '" + joint2address3 + " ', '" + joint2address4 + " ', '" + zipcode    + "', '" + occupation  + "','"+  joint2telephone+ "','"+joint2email +"','"+joint2fax+"','"+joint2contactperson +"',"+ null+")";
      else
      updateJoint2=" call UPDATE_JOINT_N"+proc_name +"_EMPTY('" + foliono  + "', 2, '" + joint2prefix+"' , '" + joint2name + "', '" + joint2relation  + "', '" + joint2father + "', '" + joint2address  + "', '" +joint2address2  + "', '" + joint2address3 + " ', '" + joint2address4 + " ', '" + zipcode    + "', '" + occupation  + "','"+  joint2telephone+ "','"+joint2email +"','"+joint2fax+"','"+joint2contactperson+"')";


   /*
      if(signaturestatus.equals("add") && photostatus.equals("add"))
          updateFolio = "call UPDATE_SHAREHOLDER_TEMP('" + foliono  + "', '" + prefix  + "', '" + shareholdername   + "', '" + surname + "', '" + relation + "', '" + father + "', '" + address1 + "', '" +address2 + "', '" + address3 +  "', '" + address4  + "', '" +zipcode    + "', '" + occupation  + "', " + no_of_joints + ", '" + category  + "', '" +  accountno + "', '" +  bankname  + "', '" +  bankbranch  + "', "+total_shares+" , '" +  taxfreeshare  + "', 'F' ,'F', '" + resident   + "', '" + nationality  + "', '"  +nomineeno + "', 'F', '" + entrydate +  "', null, null,'" + telephone + "', '" + cellphone + "', '" + email + "', '" + fax + "', '" + tin   +"',null)";
      else
      if(signature.equals("add") && photostatus.equals("edit"))
          updateFolio = "call UPDATE_SHAREHOLDER_TEMP_S('" + foliono  + "', '" + prefix  + "', '" + shareholdername   + "', '" + surname + "', '" + relation + "', '" + father + "', '" + address1 + "', '" +address2 + "', '" + address3 +  "', '" + address4  + "', '" +zipcode    + "', '" + occupation  + "', " + no_of_joints + ", '" + category  + "', '" +  accountno + "', '" +  bankname  + "', '" +  bankbranch  + "', "+total_shares+" , '" +  taxfreeshare  + "', 'F' ,'F', '" + resident   + "', '" + nationality  + "', '"  +nomineeno + "', 'F', '" + entrydate +  "', null, null,'" + telephone + "', '" + cellphone + "', '" + email + "', '" + fax + "', '" + tin   +" ')";
      else
      if(signature.equals("edit") && photostatus.equals("add"))
          updateFolio = "call UPDATE_SHAREHOLDER_TEMP_P('" + foliono  + "', '" + prefix  + "', '" + shareholdername   + "', '" + surname + "', '" + relation + "', '" + father + "', '" + address1 + "', '" +address2 + "', '" + address3 +  "', '" + address4  + "', '" +zipcode    + "', '" + occupation  + "', " + no_of_joints + ", '" + category  + "', '" +  accountno + "', '" +  bankname  + "', '" +  bankbranch  + "', "+total_shares+" , '" +  taxfreeshare  + "', 'F' ,'F', '" + resident   + "', '" + nationality  + "', '"  +nomineeno + "', 'F', '" + entrydate +  "', null,'" + telephone + "', '" + cellphone + "', '" + email + "', '" + fax + "', '" + tin   +" ')";
      else
          updateFolio = "call UPDATE_SHAREHOLDER_TEMP_EMPTY('" + foliono  + "', '" + prefix  + "', '" + shareholdername   + "', '" + surname + "', '" + relation + "', '" + father + "', '" + address1 + "', '" +address2 + "', '" + address3 +  "', '" + address4  + "', '" +zipcode    + "', '" + occupation  + "', " + no_of_joints + ", '" + category  + "', '" +  accountno + "', '" +  bankname  + "', '" +  bankbranch  + "', "+total_shares+" , '" +  taxfreeshare  + "', 'F' ,'F', '" + resident   + "', '" + nationality  + "', '"  +nomineeno + "', 'F', '" + entrydate +  "', null,'" + telephone + "', '" + cellphone + "', '" + email + "', '" + fax + "', '" + tin   +" ')";


    if(signaturestatus.equals("add") && photostatus.equals("add"))
      updateJoint1=" call UPDATE_JOINT_N_TEMP('" + foliono  + "', 1, '" + jointprefix+"' , '" + jointname + "', '" + jointrelation  + "', '" + jointfather + "', '" + jointaddress  + "', '" +jointaddress2  + "', '" + jointaddress3 + " ', '" + jointaddress4 + " ', '" + zipcode    + "', '" + occupation  + "', null,'"+  jointtelephone+ "','"+jointemail +"','"+jointfax+"','"+jointcontactperson+"',"+ null+")";
      else
      if(signature.equals("add") && photostatus.equals("edit"))
      updateJoint1=" call UPDATE_JOINT_N_TEMP_S('" + foliono  + "', 1, '" + jointprefix+"' , '" + jointname + "', '" + jointrelation  + "', '" + jointfather + "', '" + jointaddress  + "', '" +jointaddress2  + "', '" + jointaddress3 + " ', '" + jointaddress4 + " ', '" + zipcode    + "', '" + occupation  + "', null,'"+  jointtelephone+ "','"+jointemail +"','"+jointfax+"','"+jointcontactperson+"')";
      else
      if(signature.equals("edit") && photostatus.equals("add"))
      updateJoint1=" call UPDATE_JOINT_N_TEMP_P('" + foliono  + "', 1, '" + jointprefix+"' , '" + jointname + "', '" + jointrelation  + "', '" + jointfather + "', '" + jointaddress  + "', '" +jointaddress2  + "', '" + jointaddress3 + " ', '" + jointaddress4 + " ', '" + zipcode    + "', '" + occupation  + "','"+  jointtelephone+ "','"+jointemail +"','"+jointfax+"','"+jointcontactperson +"',"+ null+"')";
      else
      updateJoint1=" call UPDATE_JOINT_N_TEMP_EMPTY('" + foliono  + "', 1, '" + jointprefix+"' , '" + jointname + "', '" + jointrelation  + "', '" + jointfather + "', '" + jointaddress  + "', '" +jointaddress2  + "', '" + jointaddress3 + " ', '" + jointaddress4 + " ', '" + zipcode    + "', '" + occupation  + "','"+  jointtelephone+ "','"+jointemail +"','"+jointfax+"','"+jointcontactperson+"')";


      if(signaturestatus.equals("add") && photostatus.equals("add"))
      updateJoint2=" call UPDATE_JOINT_N_TEMP('" + foliono  + "', 2, '" + jointprefix+"' , '" + jointname + "', '" + jointrelation  + "', '" + jointfather + "', '" + jointaddress  + "', '" +jointaddress2  + "', '" + jointaddress3 + " ', '" + jointaddress4 + " ', '" + zipcode    + "', '" + occupation  + "', null,'"+  jointtelephone+ "','"+jointemail +"','"+jointfax+"','"+jointcontactperson+"',"+ null+")";
      else
      if(signature.equals("add") && photostatus.equals("edit"))
      updateJoint2=" call UPDATE_JOINT_N_TEMP_S('" + foliono  + "', 2, '" + jointprefix+"' , '" + jointname + "', '" + jointrelation  + "', '" + jointfather + "', '" + jointaddress  + "', '" +jointaddress2  + "', '" + jointaddress3 + " ', '" + jointaddress4 + " ', '" + zipcode    + "', '" + occupation  + "', null,'"+  jointtelephone+ "','"+jointemail +"','"+jointfax+"','"+jointcontactperson+"')";
      else
      if(signature.equals("edit") && photostatus.equals("add"))
      updateJoint2=" call UPDATE_JOINT_N_TEMP_P('" + foliono  + "', 2, '" + jointprefix+"' , '" + jointname + "', '" + jointrelation  + "', '" + jointfather + "', '" + jointaddress  + "', '" +jointaddress2  + "', '" + jointaddress3 + " ', '" + jointaddress4 + " ', '" + zipcode    + "', '" + occupation  + "','"+  jointtelephone+ "','"+jointemail +"','"+jointfax+"','"+jointcontactperson +"',"+ null+"')";
      else
      updateJoint2=" call UPDATE_JOINT_N_TEMP_EMPTY('" + foliono  + "', 2, '" + jointprefix+"' , '" + jointname + "', '" + jointrelation  + "', '" + jointfather + "', '" + jointaddress  + "', '" +jointaddress2  + "', '" + jointaddress3 + " ', '" + jointaddress4 + " ', '" + zipcode    + "', '" + occupation  + "','"+  jointtelephone+ "','"+jointemail +"','"+jointfax+"','"+jointcontactperson+"')";
    */

   //System.out.println(updateFolio);
   boolean b = cm.procedureExecute(updateFolio);

   int joint1count=0;
   int joint2count=0;

   String checkJoint1Query = "SELECT COUNT(*) AS TOTAL FROM JOINT_N"+proc_name + " WHERE FOLIO_NO='"+ foliono+"' AND JOINT= 1";
   String checkJoint2Query = "SELECT COUNT(*) AS TOTAL FROM JOINT_N"+proc_name + " WHERE FOLIO_NO='"+ foliono+"' AND JOINT= 2";

   cm.queryExecute(checkJoint1Query);
   while(cm.toNext()){joint1count = cm.getColumnI("TOTAL");}


   cm.queryExecute(checkJoint2Query);
   while(cm.toNext()){joint2count = cm.getColumnI("TOTAL");}


   if(joint1count <= 0)
      updateJoint1=" call ADD_JOINT_N_TEMP('" + foliono  + "', 1, '" + jointprefix+"' , '" + jointname + "', '" + jointrelation  + "', '" + jointfather + "', '" + jointaddress  + "', '" +jointaddress2  + "', '" + jointaddress3 + " ', '" + jointaddress4 + " ', '" + zipcode    + "', '" + occupation  + "', null,'"+  jointtelephone+ "','"+jointemail +"','"+jointfax+"','"+jointcontactperson+"','"+jointremarks+"', null)";

   if(joint2count <= 0)
      updateJoint2=" call ADD_JOINT_N_TEMP('" + foliono  + "', 2 , '"+joint2prefix+ "' , '" + joint2name + "', '" + joint2relation  + "', '" + joint2father + "', '" + joint2address  + "', '" + joint2address2 + "', '" + joint2address3 + " ', '" + joint2address4 + " ', '" + zipcode    + "', '" + occupation   + "', null,'"+  joint2telephone+ "','"+joint2email +"','"+joint2fax+"','"+joint2contactperson+"','"+joint2remarks+"', null)";



   if(!jointname.equals(""))
   {
         //System.out.println(updateJoint1);
         boolean c = cm.procedureExecute(updateJoint1);
   }
   if(!joint2name.equals(""))
   {
         //System.out.println(updateJoint2);
         boolean d = cm.procedureExecute(updateJoint2);
   }

try
{
  //INSERTS JOINT 2 SIGNATURE INTO DATABASE
   if(joint2signature!=null && !joint2signature.equals("") && !(joint2signaturestatus.equals("delete") || joint2signaturestatus.equals("edit")))
 {
        BLOB blob;
      PreparedStatement pstmt;
      cm.connect();
      Connection connection = cm.getConnection() ;

      String selectQuery="";
      if(dbstatus!=null && dbstatus.equals("actual"))
      {
        selectQuery="SELECT * FROM JOINT_N where folio_no = ?  AND JOINT=2 for update";
      }
      else
      {
        selectQuery="SELECT * FROM JOINT_N_TEMP where folio_no = ? AND JOINT=2 for update";
      }

      pstmt = connection.prepareStatement(selectQuery);

      pstmt.setString(1, foliono);
      connection.setAutoCommit(false);

      ResultSet rset=pstmt.executeQuery();
      rset.next();



        blob = ((OracleResultSet) rset).getBLOB("SIGNATURE");
        OutputStream os = blob.getBinaryOutputStream();


        byte[] chunk = new byte[blob.getChunkSize()];
        int i = -1;


        File fullFile = new File(joint2signature);

        FileInputStream is = new FileInputStream(fullFile );
        while ((i = is.read(chunk)) != -1) {
            os.write(chunk, 0, i);

        }

        is.close();
        os.close();

        pstmt.close();
        connection.commit();
        connection.setAutoCommit(true);
        //cm.takeDown();
 }



 //This section inserts joint 1 signature into database
  if(jointsignature!=null && !jointsignature.equals("") && !(jointsignaturestatus.equals("delete") || jointsignaturestatus.equals("edit")))
 {
        BLOB blob;
      PreparedStatement pstmt;
      cm.connect();
      Connection connection = cm.getConnection() ;

      String selectQuery="";
      if(dbstatus!=null && dbstatus.equals("actual"))
      {
        selectQuery="SELECT * FROM JOINT_N where folio_no = ?  AND JOINT=1 for update";
      }
      else
      {
        selectQuery="SELECT * FROM JOINT_N_TEMP where folio_no = ? AND JOINT=1 for update";
      }

      pstmt = connection.prepareStatement(selectQuery);

      pstmt.setString(1, foliono);
      connection.setAutoCommit(false);

      ResultSet rset=pstmt.executeQuery();
      rset.next();



        blob = ((OracleResultSet) rset).getBLOB("SIGNATURE");
        OutputStream os = blob.getBinaryOutputStream();


        byte[] chunk = new byte[blob.getChunkSize()];
        int i = -1;


        File fullFile = new File(jointsignature);

        FileInputStream is = new FileInputStream(fullFile );
        while ((i = is.read(chunk)) != -1) {
            os.write(chunk, 0, i);

        }

        is.close();
        os.close();

        pstmt.close();
        connection.commit();
        connection.setAutoCommit(true);
        //cm.takeDown();
 }
System.out.println("Ok");
//This section inserts signature into database
 if (signature!=null && !signature.equals("") && !(signaturestatus.equals("delete") || signaturestatus.equals("edit")))
  {
System.out.println("Ok1");
    BLOB blob;
    PreparedStatement pstmt;
    cm.connect();
    Connection connection = cm.getConnection() ;

    String selectQuery="";
    if(dbstatus!=null && dbstatus.equals("actual"))
    {
      selectQuery="SELECT * FROM shareholder where folio_no = ? for update";
    }
    else
    {
      selectQuery="SELECT * FROM shareholder_temp where folio_no = ? for update";
    }

    pstmt = connection.prepareStatement(selectQuery);

    pstmt.setString(1, foliono);
    connection.setAutoCommit(false);

    ResultSet rset=pstmt.executeQuery();
    rset.next();



        blob = ((OracleResultSet) rset).getBLOB("SIGNATURE");
        OutputStream os = blob.getBinaryOutputStream();


        byte[] chunk = new byte[blob.getChunkSize()];
        int i = -1;


        File fullFile = new File(signature);

        FileInputStream is = new FileInputStream(fullFile );
        while ((i = is.read(chunk)) != -1) {
            os.write(chunk, 0, i);

        }

        is.close();
        os.close();

        pstmt.close();
        connection.commit();
        connection.setAutoCommit(true);
        //cm.takeDown();
  }

//This section inserts signature into SHAREHOLDER database
  if (photo!=null && !photo.equals("") && !(photostatus.equals("delete") || photostatus.equals("edit")))
  {

    BLOB blob;
    PreparedStatement pstmt;
    cm.connect();
    Connection connection = cm.getConnection() ;

    String selectQuery="";
    if(dbstatus!=null && dbstatus.equals("actual"))
    {
      selectQuery="SELECT * FROM shareholder where folio_no = ? for update";
    }
    else
    {
      selectQuery="SELECT * FROM shareholder_temp where folio_no = ? for update";
    }

    pstmt = connection.prepareStatement(selectQuery);

    pstmt.setString(1, foliono);
    connection.setAutoCommit(false);

    ResultSet rset=pstmt.executeQuery();
    rset.next();


        //Use the OracleDriver resultset, we take the blob locator
        blob = ((OracleResultSet) rset).getBLOB("PHOTO");
        OutputStream os = blob.getBinaryOutputStream();

        //Read the file by chuncks and insert them in the Blob. The chunk size come from the blob
        byte[] chunk = new byte[blob.getChunkSize()];
        int i = -1;


        File fullFile = new File(photo);

        FileInputStream is = new FileInputStream(fullFile );
        while ((i = is.read(chunk)) != -1) {
            os.write(chunk, 0, i); //Write the chunk

        }

        is.close();
        os.close();
        //Close the statement and commit
        pstmt.close();
        connection.commit();
        connection.setAutoCommit(true);
        //cm.takeDown();
  }


//This section inserts joint 1 photo into  database
  if (jointphoto!=null && !jointphoto.equals("") && !(jointphotostatus.equals("delete") || jointphotostatus.equals("edit")))
  {
    //Insert Blob into database
    BLOB blob;
    PreparedStatement pstmt;
    cm.connect();
    Connection connection = cm.getConnection() ;

    String selectQuery="";
    if(dbstatus!=null && dbstatus.equals("actual"))
    {
      selectQuery="SELECT * FROM JOINT_N where folio_no = ? AND JOINT=1 for update";
    }
    else
    {
      selectQuery="SELECT * FROM JOINT_N_TEMP where folio_no = ? AND JOINT=1 for update";
    }

    pstmt = connection.prepareStatement(selectQuery);

    pstmt.setString(1, foliono);
    connection.setAutoCommit(false);

    ResultSet rset=pstmt.executeQuery();
    rset.next();


        //Use the OracleDriver resultset, we take the blob locator
        blob = ((OracleResultSet) rset).getBLOB("PHOTO");
        OutputStream os = blob.getBinaryOutputStream();

        //Read the file by chuncks and insert them in the Blob. The chunk size come from the blob
        byte[] chunk = new byte[blob.getChunkSize()];
        int i = -1;


        File fullFile = new File(jointphoto);

        FileInputStream is = new FileInputStream(fullFile );
        while ((i = is.read(chunk)) != -1) {
            os.write(chunk, 0, i); //Write the chunk

        }

        is.close();
        os.close();
        //Close the statement and commit
        pstmt.close();
        connection.commit();
        connection.setAutoCommit(true);
        //cm.takeDown();
  }//end of joint 1 photo


//This section inserts joint 2 photo into  database
  if (joint2photo!=null && !joint2photo.equals("") && !(joint2photostatus.equals("delete") || joint2photostatus.equals("edit")))
  {

    BLOB blob;
    PreparedStatement pstmt;
    cm.connect();
    Connection connection = cm.getConnection() ;

    String selectQuery="";
    if(dbstatus!=null && dbstatus.equals("actual"))
    {
      selectQuery="SELECT * FROM JOINT_N where folio_no = ? AND JOINT=2 for update";
    }
    else
    {
      selectQuery="SELECT * FROM JOINT_N_TEMP where folio_no = ? AND JOINT=2 for update";
    }

    pstmt = connection.prepareStatement(selectQuery);

    pstmt.setString(1, foliono);
    connection.setAutoCommit(false);

    ResultSet rset=pstmt.executeQuery();
    rset.next();


        //Use the OracleDriver resultset, we take the blob locator
        blob = ((OracleResultSet) rset).getBLOB("PHOTO");
        OutputStream os = blob.getBinaryOutputStream();

        //Read the file by chuncks and insert them in the Blob. The chunk size come from the blob
        byte[] chunk = new byte[blob.getChunkSize()];
        int i = -1;


        File fullFile = new File(joint2photo);

        FileInputStream is = new FileInputStream(fullFile );
        while ((i = is.read(chunk)) != -1) {
            os.write(chunk, 0, i); //Write the chunk

        }

        is.close();
        os.close();
        //Close the statement and commit
        pstmt.close();
        connection.commit();
        connection.setAutoCommit(true);
//        cm.takeDown();
  }

}
catch(Exception ex)
{
     ex.printStackTrace();
}

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Edited Folio Information')";
  boolean ub = cm.procedureExecute(ulog);

 if (isConnect)
  {
    cm.takeDown();
  }
%>
<script language="javascript">
<%

if(dbstatus!=null && dbstatus.equals("actual"))
{
  %>
  location = "<%=request.getContextPath()%>/jsp/Folio/AllPermanentFolios.jsp";

  <%
}
  else{
    %>
  location = "<%=request.getContextPath()%>/jsp/Folio/AllFolios.jsp";
    <%
  }
%>
</script>
</body>
</html>
