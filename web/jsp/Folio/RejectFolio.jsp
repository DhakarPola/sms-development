<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Creation Date : November 2005
'Page Purpose  : Deletes list of selected folios Folio.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Delete Folio</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
     String count_s=request.getParameter("COUNTER");
     int COUNT = Integer.parseInt(count_s) ;

 String deleteJointQuery;

for(int i=1;i<=COUNT;i++)
{
  String foliono=request.getParameter("folioid" + String.valueOf(i)) ;
  String foliostatus= request.getParameter("foliostatus" + String.valueOf(i));
  String dfolioQuery = "call DELETE_SHAREHOLDER_TEMP('" + foliono + "')";

  if(foliostatus!=null)
  {
    boolean b = cm.procedureExecute(dfolioQuery);
    deleteJointQuery="call DELETE_JOINT_N_TEMP('" + foliono + "')";
    System.out.println(deleteJointQuery);
    boolean c = cm.procedureExecute(deleteJointQuery);

  }
}

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Deleted Folio Information')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Folio/AcceptFolios.jsp";
</script>
</body>
</html>
