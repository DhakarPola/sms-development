<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Creation Date : October 2005
'Updated By    : Renad Hakim
'Updated By    : Kamruzzaman
'Page Purpose  : Adds a new Folio.
'
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.io.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*"%>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<jsp:useBean id="util" class="batbsms.Utility"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
</LINK><%if (String.valueOf(session.getAttribute("UserName")).equals("null")) {%>
<script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%}%>

<html>
<head>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datepicker.js"></SCRIPT>
<style type="text/css">
  <!--
    body {
    background-color: #FFFFFF;
    }
    body,td,th {
    color: #000000;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
    }
  -->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Add New Folio</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function formconfirm()
{
  if (confirm("Do you want to enter the mentioned Folio Information?"))
  {
    return true;
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;

 //Blank Field Validation

 BlankValidate('foliono','- Folio No (Option must be entered)');
 BlankValidate('shareholdername','-  Share Holder Name(Option must be entered)');
  if(document.forms[0].boxsptax.checked==true)
    {
      BlankValidate('spTax','- Special Tax Value (Option must be entered)');
    }

  if(document.forms[0].foliono.value.length == 8)
  {
    if (count == 0)
    {
      if (formconfirm())
      {
        document.forms[0].submit();
      }
    }
    else
    {
      ShowAllAlertMsg();
      return false;
    }
  }
  else
  {
    alert("Folio Number should be 8 characters long");
  }
}

function openSignWindow()
{
	window.open("UploadSignature.jsp","","HEIGHT=350,WIDTH=500")
}
function openPhotoWindow()
{
  	window.open("UploadPhoto.jsp","","HEIGHT=350,WIDTH=500")
}
function openNomineeWindow()
{
	window.open("NomineeList.jsp","","HEIGHT=500,WIDTH=500,SCROLLBAR=YES")
}
function openJoint1()
{
	window.open("Joint1.jsp","","HEIGHT=680,WIDTH=500");
}
function openJoint2()
{
	window.open("Joint2.jsp","","HEIGHT=680,WIDTH=500");
}

function showText()
{
    if(document.forms[0].boxsptax.checked==true)
    {
      document.forms[0].spTax.style.display='';
    }
    else
    {
      document.forms[0].spTax.style.display="none";
    }
}

//-->
</script>
<style type="text/css">
<!--
    .style7 {
    color: #FFFFFF;
    font-weight: bold;
    font-size: 13px;
    }
    .style8 {color: #0A2769;
            font-weight: bold;}
    .style9 {color: #0A2769; font-weight: bold; }
.style12 {color: #FFFFFF}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
%>
<form action="SaveFolio.jsp" method="get"   name="newFolioForm">
<span class="style7">
<table width="100%" border="1" cellspacing="0" cellpadding="0" style="border-collapse:collapse" bordercolor="#0044B0" bgcolor="#E8F3FD">
  <tr>
    <td>
	<table width="100%" border="0" cellspacing="0" cellpadding="5" >
      <tr>
        <td bgcolor="#0044B0" class="style7" height="30">Share Holder's Personal Information</td>
      </tr>
  <tr bgcolor="#E8F3FD">
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
	<table width="100%" height="100%"  border="0" cellpadding="5">
      <tr>
        <td width="32%" class="style8">Folio</td>
        <td width="1%">:</td>
        <td width="67%"><input name="foliono" type="text"  maxlength="8" class="SL2TextField"></td>

        </tr>
      <tr>
        <td class="style8">Category</td>
        <td>:</td>
        <td>
          <select name="category" class="SL1TextFieldListBox">
          <%

           String categoryQuery="SELECT \"CATEGORY\" FROM CATEGORY_VIEW";
           ResultSet rs= cm.queryExecuteGetResultSet(categoryQuery);
           while(rs.next())
           {
          %>
             <option value="<%= cm.getColumnS("CATEGORY") %>" ><%= cm.getColumnS("CATEGORY") %></option>
          <%
             }
          %>
        </select></td>
        </tr>
      <tr>
        <td class="style8">Prefix</td>
        <td>:</td>
        <td><select name="prefix" class="SL1TextFieldListBox">
          <option value="none" selected="selected">--</option>
          <option value="Mr.">Mr.</option>
          <option value="Mrs.">Mrs.</option>
          <option value="MISS">MISS</option>
          <option value="MS.">MS.</option>
          <option value="DR.">DR.</option>
        </select></td>
        </tr>
      <tr>
        <td height="37" class="style8">Name</td>
        <td>:</td>
        <td><input name="shareholdername" type="text"  class="SL2TextField"></td>
        </tr>
      <tr>
        <td class="style8">Surname</td>
        <td>:</td>
        <td><input name="surname" type="text" class="SL2TextField"></td>
      </tr>
      <tr>
        <td class="style8">Relation</td>
        <td>:</td>
        <td>
          <select name="relation"  class="SL1TextFieldListBox">
            <option value="none" selected="selected">--</option>
            <option value="S/O">S/O</option>
            <option value="W/O">W/O</option>
            <option value="D/O">D/O</option>
          </select></td>
        </tr>
      <tr>
        <td class="style8">Father/Husband</td>
        <td>:</td>
        <td><input name="father" type="text"  class="SL2TextField"></td>
        </tr>
      <tr>
        <td class="style8">Resident</td>
        <td>:</td>
        <td><input name="resident" type="checkbox"  checked>
        </td>
        </tr>
      <tr>
        <td class="style8">Active</td>
        <td>:</td>
        <td><input name="boxactive" type="checkbox"  checked>
        </td>
        </tr>
      <tr>
        <td class="style8">Nationality</td>
        <td>:</td>
        <td><input name="nationality" type="text" value="Bangladeshi" class="SL2TextField">
        </tr>
      <tr>
        <td class="style8">Occupation</td>
        <td>:</td>
        <td><input name="occupation" type="text" class="SL2TextField"></td>
        </tr>
      <tr>
        <td class="style8" valign="top">Address</td>
        <td valign="top">:</td>
        <td><textarea name="address1" class="ML9TextField"></textarea></td>
        </tr>
      <tr>
        <td class="style8">Zip Code</td>
        <td>:</td>
        <td><input name="zipcode" type="text"  class="SL2TextField" onkeypress="keypressOnNumberFld()"></td>
        </tr>
      <tr>
        <td class="style8">Telephone</td>
        <td>:</td>
        <td><input name="telephone" type="text" class="SL2TextField" onkeypress="keypressOnNumberFld()"></td>
        </tr>
      <tr>
        <td class="style8">Mobile</td>
        <td>:</td>
        <td><input name="cellphone" type="text" class="SL2TextField" onkeypress="keypressOnNumberFld()"></td>
        </tr>
      <tr>
        <td class="style8">E-mail</td>
        <td>:</td>
        <td><input name="email" type="text" class="SL2TextField"></td>
        </tr>
      <tr>
        <td class="style8">Fax</td>
        <td>:</td>
        <td><input name="fax" type="text" class="SL2TextField"></td>
      </tr>
	<tr>
         <td class="style8">Total Shares</td>
        <td>:</td>
		<td><input name="totalshares" type="text" class="SL2TextField" value="0"></td>
	  </tr>

      <tr>
        <td class="style8" valign="top">Signature (*.gif)</td>
        <td valign="top">:</td>
	<td><input name="signature" type="text" id="signature" class="SL2TextField" readonly="readonly">
<!--		<input name="btnSignature" type="button" value="Upload" onclick="openSignWindow()">-->
            <br><img name="BA" src="<%=request.getContextPath()%>/images/btnUpload.gif" onclick="openSignWindow()" onMouseOver="document.forms[0].BA.src = '<%=request.getContextPath()%>/images/btnUploadR.gif'" onMouseOut="document.forms[0].BA.src = '<%=request.getContextPath()%>/images/btnUpload.gif'">

        </td>
      </tr>
    <tr>
		<tr>
            <td width="32%" class="style8" valign="top">Photo (*.gif)</td>
            <td width="1%" valign="top">:</td>
            	<td width="67%">
                  <input name="photo" type="text"  id="photo" class="SL2TextField" readonly="readonly">
<!--                  <input name="btnPhoto" type="button" value="Upload" onclick="openPhotoWindow()">                </td>-->
                      <br><img name="BB" src="<%=request.getContextPath()%>/images/btnUpload.gif" onclick="openPhotoWindow()" onMouseOver="document.forms[0].BB.src = '<%=request.getContextPath()%>/images/btnUploadR.gif'" onMouseOut="document.forms[0].BB.src = '<%=request.getContextPath()%>/images/btnUpload.gif'">
        <br><br>&nbsp;
              </td>
                </tr>
        </table>
          <tr>
              <td bgcolor="#0044B0" class="style7" height="30">Personal Details</td>
          </tr>
                    <tr>
                      <td colspan="2">

                        <table width="100%" border="0" cellpadding="5">
                          <tr>
                <td width="33%" class="style8">Account No.</td>
                 <td width="1%">:</td>
                <td width="67%"><input name="accountno" type="text" class="SL2TextField" ></td>
              </tr>
              <tr>
                <td class="style8">Bank</td>
                 <td>:</td>
                <td><input name="bankname" type="text" class="SL2TextField"></td>
              </tr>
              <tr>
                <td class="style8">Branch</td>
                 <td>:</td>
                <td><input name="bankbranch" type="text" class="SL2TextField"></td>
              </tr>
              <tr>
                <td class="style8">TIN</td>
                 <td>:</td>
                <td><input name="tin" type="text" class="SL2TextField" onkeypress="keypressOnNumberFld()">
                <!-- Joint 1 hidden fields-->
                  <input type="hidden" name="jointprefix" />
                  <input type="hidden" name="jointname" />
                  <input type="hidden" name="jointrelation" />
                  <input type="hidden" name="jointfather" />
                  <input type="hidden" name="jointoccupation" />
                  <input type="hidden" name="jointtelephone" />
                  <input type="hidden" name="jointfax" />
                  <input type="hidden" name="jointemail" />
                  <input type="hidden" name="jointaddress" />
                  <input type="hidden" name="jointcontactperson" />
                  <input type="hidden" name="jointremarks" />
                  <input type="hidden" name="joint1signature" />
                  <input type="hidden" name="joint1photo" />
                <!-- End of Joint 1 fields-->
                <!-- Joint 2 hidden fields-->
                  <input type="hidden" name="joint2prefix" />
                  <input type="hidden" name="joint2name" />
                  <input type="hidden" name="joint2relation" />
                  <input type="hidden" name="joint2father" />
                  <input type="hidden" name="joint2occupation" />
                  <input type="hidden" name="joint2telephone" />
                  <input type="hidden" name="joint2fax" />
                  <input type="hidden" name="joint2email" />
                  <input type="hidden" name="joint2address" />
                  <input type="hidden" name="joint2contactperson" />
                  <input type="hidden" name="joint2remarks" />
                  <input type="hidden" name="joint2signature" />
                  <input type="hidden" name="joint2photo" />
                <!-- End of Joint 2 fields-->

                </td>
              </tr>
              <tr>
                <td class="style8">Tax Free Share</td>
                 <td>:</td>
                <td><input name="taxfreeshare" type="text" class="SL2TextField" onkeypress="keypressOnNumberFld()"></td>
              </tr>
              <tr>
                <td class="style8">Liable to Tax</td>
                <td>:</td>
                <td><input name="boxliable" type="checkbox"  checked>
                </td>
              </tr>
              <tr>
                <td class="style8">Special Tax</td>
                <td>:</td>
                <td><input name="boxsptax" type="checkbox" onclick="showText()">&nbsp;<input id="spTax" type="text" name="spTax" class="SLSPTAXTextField" style="display:none" onkeypress="keypressOnDoubleFld()"/>
                </td>
              </tr>
              <tr>
                <td class="style8">Entry Date</td>
                 <td>:</td>
                <td><input name="entrydate" type="text"  class="SL2TextField">
                <!-- Date Picker  -->
                <!--<a href="javascript:NewCal('CallReceived','ddmmyyyy',false,12)"><img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date"></a>-->
                <a href="javascript:show_calendar('newFolioForm.entrydate');"  onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"><img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date"></a>                </td>
              </tr>
              <tr>
                <td class="style8" valign="top">Dividend Group No.</td>
                 <td valign="top">:</td>
                <td valign="middle"><input name="nomineeno" type="text" class="SL2TextField" readonly="readonly" >
                                <br><img  src="<%=request.getContextPath()%>/images/btnSelect.gif" name="B5" onclick="openNomineeWindow()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnSelectOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnSelect.gif'">				</td>
              </tr>
              <tr>
                <td class="style8" valign="top">Nominee Details</td>
                <td valign="top">:</td>
                <td><textarea name="ndetails" class="ML9TextField"></textarea></td>
              </tr>
            </table></td>
          </tr>
	<tr>
	<td>
	 <table width="100%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B" bgcolor="#E8F3FD">
      <tr>
        <td align="center"><label>
         <!-- <input type="button" name="Joint1" value="Joint1" onClick="openJoint1()">-->
            <img src="<%=request.getContextPath()%>/images/btnJointOne.gif" name="BC" onclick="openJoint1()" onMouseOver="document.forms[0].BC.src = '<%=request.getContextPath()%>/images/btnJointOneR.gif'" onMouseOut="document.forms[0].BC.src = '<%=request.getContextPath()%>/images/btnJointOne.gif'">
        </label></td>
        <td align="center"><label>
<!--          <input type="button" name="Joint2"  value="Joint2" onClick="openJoint2()">-->
            <img src="<%=request.getContextPath()%>/images/btnJointTwo.gif" name="BD" onclick="openJoint2()" onMouseOver="document.forms[0].BD.src = '<%=request.getContextPath()%>/images/btnJointTwoR.gif'" onMouseOut="document.forms[0].BD.src = '<%=request.getContextPath()%>/images/btnJointTwo.gif'">
        </label></td>
        <td align="center">
 		    <img src="<%=request.getContextPath()%>/images/btnBack.gif" name="B3" onclick="history.go(-1)" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnBackOn.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnBack.gif'">
        </td>
        <td align="center">
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
      	</td>
        <td align="center">
	          <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
      	</td>
      </tr>
	</table>
	</td>
	</tr>

        </table></td>
		</tr>
    </table>
</table>
</form>
<%
if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
