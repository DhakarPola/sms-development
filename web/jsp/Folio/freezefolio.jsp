<script>
/*
'******************************************************************************************************************************************
'Script Author : Hasanul Azaz Aman
'Creation Date : December 2009
'Page Purpose  : Freeze the folio
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style19 {color: #0A2769; font-weight: bold; }
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Folios</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  var a=document.forms[0].searchFolio.value.length;

    if (a == 8){
      document.forms[0].submit();
    }
    else{
      alert('Please enter valid Folio Number!');
      return false;
    }


}

 function askfreeze()
 {
    var a=document.forms[0].Cfoliono.value.length;
    if(a>0){
      if (confirm("Do you want to Freeze the Folio Information?"))
      {
        document.forms[0].action='FreezeSaveDelete.jsp';
        document.forms[0].submit();
      }
    }
      else{alert('Please enter valid Folio Number!');}

 }
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String folio_no = String.valueOf(request.getParameter("searchFolio"));
    //System.out.println("folio_no="+folio_no);
    String query="select * from shareholder_view where FOLIO_NO='"+folio_no+"'";
   //----New
   String query1="select * from Certificate_view where HOLDER_FOLIO='"+folio_no+"'";
   //System.out.println("quary="+query1);

%>
  <span class="style7">

  <form method="POST" action="freezefolio.jsp">
  <% if(folio_no.length()>=0)  {
          %>
          <input type="hidden" name="Cfoliono" id="Cfoliono" value="<%=folio_no%>"/>
          <%
          }
    %>
   <table width="100%" BORDER=0  cellpadding="0" bordercolor="0044B0" style="border-collapse: collapse" bordercolor="#06689E">
  <!--DWLayoutTable-->
  <tr>
    <td  bgcolor="#0044B0" class="style7" height="10"><center>Search by Folio</center></td><td bgcolor="#0044B0" class="style7" height="30">&nbsp;</td>
  </tr>
  <tr>
     <td width="100%" align="right">Folio No :&nbsp;<input name="searchFolio" type="text"/>&nbsp;
     <img name="B1" src="<%=request.getContextPath()%>/images/btnSearch.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSearchR.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSearch.gif'"/>&nbsp;
     </td>
  </tr>
  </table>

<table width="100%" border="0"  style="border-collapse: collapse" bordercolor="0044B0">
  <tr>
  <td  bgcolor="#0044B0" class="style7" height="30"><center>Shareholder Summary</center></td>
  </tr>
</table>

  <%
  try {
    cm.queryExecute(query);
    while(cm.toNext()) {
      String folio=cm.getColumnS("FOLIO_NO");
      System.out.println("FOLIO_NO="+folio);
      String name=cm.getColumnS("NAME");
      String resident=cm.getColumnS("RESIDENT");
      String nationality=cm.getColumnS("NATIONALITY");
      String address=cm.getColumnS("ADDRESS1");
      String totalShare=cm.getColumnS("TOTAL_SHARE");
      String active=cm.getColumnS("ACTIVE");
      //System.out.println("active="+active);
      if(name==null||name.equals("null")) name="";
      if(resident==null||resident.equals("null")) resident="";
      if(nationality==null||nationality.equals("null")) nationality="";
      if(address==null||address.equals("null")) address="";
      if(totalShare==null||totalShare.equals("null")) totalShare="0";
      if(active==null) active="F";

      %>

  <table width="60%" border="0"  style="border-collapse: collapse" bordercolor="0044B0">

    <tr >
    <td align="left" ><div  class="style13">Folio No</div></td><td>:</td>
   <td  class="style13"><%=folio %></td>
   </tr>
   <tr>
    <td align="left"><div  class="style13">
      Name
    </div></td><td>:</td>
    <td  class="style13" >
          <%=name %></td>
    </tr>
    <tr>
     <td align="left"><div  class="style13">
      Resident
    </div></td><td>:</td>
    <td  class="style13" ><%=resident %></td>
    </tr>
    <tr>
     <td align="left"><div  class="style13">
     Nationality
    </div></td><td>:</td>
    <td  class="style13" ><%=nationality %></td>
    </tr>
    <tr>
    <td align="left"><div  class="style13">
      Address
    </div></td><td>:</td>
    <td  class="style13" ><%=address  %></td>
    </tr>
    <tr>
    <td align="left"><div  class="style13">
      Total Shares
    </div></td><td>:</td>
    <td  class="style13"><%=totalShare  %></td>
  </tr>
  <tr>
   <td align="left"><div  class="style13">
      Freez Folio
    </div></td><td>:</td>
    <td  class="style13">
      <input type="checkbox" name="active" id="active" value="F" <%if(active.equals("F")){%>checked<%}%>/>
    </td>
  </tr>
  <tr>
   <td align="left"><div  class="style13">
      <img name="B110" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="askfreeze()" onMouseOver="document.forms[0].B110.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B110.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'"/>
    </div></td><td></td>
    <td  class="style13">
    </td>
  </tr>

      <%
      }
    }catch(Exception e){}
  %>

  </table>

<br />
  <table width="100%" BORDER=0  cellpadding="0" bordercolor="0044B0" style="border-collapse: collapse" bordercolor="#06689E">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7" height="30">&nbsp;</td>
    <td  bgcolor="#0044B0" class="style7" height="30"><center>Certificate Details</center></td><td bgcolor="#0044B0" class="style7" height="30">&nbsp;</td>
  </tr>
  </table>
<br />

  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="0044B0">
  <tr bgcolor="#0044B0">
    <td  class="style9" align="center">Certificate No</td>

    <td><div align="center" class="style9">
      <div align="center">Certificate Issue Date</div>
    </div></td>
     <td width="32%"><div align="center" class="style9">
      <div align="center">Valid</div>
    </div></td>

  </tr>
  <%
 try {
     cm.queryExecute(query1);
     while(cm.toNext()) {
     String certifNo=cm.getColumnS("CERTIFICATE_NO");
     String issueDate=cm.getColumnDT("CERTIFICATE_ISSUE_DATE");
     String validity=cm.getColumnS("VALIDITY");

     if(certifNo==null||certifNo.equals("null")) certifNo="";
     if(issueDate==null||issueDate.equals("null")) issueDate="";
     if(validity==null||validity.equals("null")) validity="";

     if(validity.equals("T")){
      validity="Yes";
     }
     else {
       validity="No";
     }

     %>
      <tr >
        <td width="8%" class="style13" align="center"><%=certifNo %></td>
        <td width="24%">
          <div align="center" class="style13">
            <div align="center"><%=issueDate %></div>
          </div>
       </td>
       <td width="32%">
         <div align="center" class="style13">
             <div align="center"><%=validity %></div>
         </div>
       </td>
      </tr>
      <%
      }
    }catch(Exception e){}
  %>
  </table>
 <%
  try {
    if (isConnect)  {
      cm.takeDown();
    }
  }catch(Exception e){}
  %>

  </form>
</body>
</html>
