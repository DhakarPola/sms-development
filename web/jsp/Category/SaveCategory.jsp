<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Creation Date : November 2005
'Page Purpose  : Save new Category.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Category</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String category = String.valueOf(request.getParameter("category"));
//  String phonecode = String.valueOf(request.getParameter("phonecode"));
  String code = String.valueOf(request.getParameter("code"));
  int countCode = 0;

  category=cm.replace(category,"'","''");
//  phonecode=cm.replace(phonecode,"'","''");
  code=cm.replace(code,"'","''");


  String countCategoryQuery = "SELECT * FROM CATEGORY WHERE CODE = '"+code+"'";
  countCode = cm.queryExecuteCount(countCategoryQuery);
  cm.queryExecute(countCategoryQuery);

 if(countCode == 0)
  {
    String addCategoryQuery = "call ADD_CATEGORY('" + category + "', '', '" + code + "')";
    boolean b = cm.procedureExecute(addCategoryQuery);
  }
 else
 {
     %>
       <script language="javascript">
        alert("A Category with the same Code exists!");
        history.go(-1);
       </script>
     <%
 }

/*
'******************************************************************************************************************************************
'Updated By    : Renad Hakim
'Date          : March 2005
'******************************************************************************************************************************************
*/

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added Category Information')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Category/AllCategories.jsp";
</script>
</body>
</html>
