<%@ page isErrorPage="true" %>
<html>
<body bgcolor="#ffffff">

<h1>Error page</h1>

<br>An error occured in the system. The error message is: <br>
<%= exception.getMessage() %><br>
<pre><font color="white"><%
 java.io.CharArrayWriter cw = new java.io.CharArrayWriter();
 java.io.PrintWriter pw = new java.io.PrintWriter(cw,true);
 exception.printStackTrace(pw);
 out.println(cw.toString());
 %></font></pre>
<br></body>
</html>
