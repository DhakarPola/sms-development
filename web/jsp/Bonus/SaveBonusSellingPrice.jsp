<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : January 2006
'Page Purpose  : Updates Fractional Bonus Share Sales Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.util.Date,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update Fractional Bonus Share Price</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String BankID = String.valueOf(request.getParameter("bankidentification"));
  String SPrice = String.valueOf(request.getParameter("priceamount"));
  String BankName = String.valueOf(request.getParameter("bnkname"));
  String BankBranch = String.valueOf(request.getParameter("bchname"));
  String BankAddress = String.valueOf(request.getParameter("bnkaddress"));
  String AccountNo = String.valueOf(request.getParameter("bnkacc"));
  String DecDate = String.valueOf(request.getParameter("decdate"));

  StringTokenizer BankSTK = new StringTokenizer(BankName,"%");
  BankName = BankSTK.nextToken();

  String nbonus = "call UPDATE_BONUS_FRACTION_INFO('" + DecDate + "', " + SPrice + ", '" + BankName + "','" + BankBranch + "','" + BankAddress + "','" + AccountNo + "'," + BankID + ")";
  boolean b = cm.procedureExecute(nbonus);

  String forbno = "";
  double fractionshare = 0;
  double fractionamount = 0;
  double sellingprice = Double.parseDouble(SPrice);
  String warrantno = "";

  String query1 = "SELECT * FROM BONUS_VIEW WHERE DATE_DEC = TO_DATE('" + DecDate + "','DD/MM/YYYY')";
  cm.queryExecute(query1);

   while(cm.toNext())
   {
     forbno = cm.getColumnS("FOLIO_NO");
     fractionshare = cm.getColumnF("FRACTION_SHARE");

     warrantno = cm1.LastWarrant();
     fractionamount = fractionshare * sellingprice;
     fractionamount = bcal.roundtovalue(fractionamount,2);

     nbonus = "call UPDATE_BONUS_FRACTION_AMOUNT('" + DecDate + "', '" + forbno + "', " + fractionamount + ", '" + warrantno + "')";
     b = cm1.procedureExecute(nbonus);
   }

  query1 = "SELECT * FROM BOBONUS_VIEW  WHERE DATE_DEC = TO_DATE('" + DecDate + "','DD/MM/YYYY')";
  cm.queryExecute(query1);

   while(cm.toNext())
   {
     forbno = cm.getColumnS("BO_ID");
     fractionshare = cm.getColumnF("FRACTION_SHARE");

     warrantno = cm1.LastWarrant();
     fractionamount = fractionshare * sellingprice;
     fractionamount = bcal.roundtovalue(fractionamount,2);

     nbonus = "call UPDATE_BOBONUS_FRACTION_AMOUNT('" + DecDate + "', '" + forbno + "', " + fractionamount + ", '" + warrantno + "')";
     b = cm1.procedureExecute(nbonus);
   }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added Information Regarding Sales Price of Fractional Bonus Shares')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
     cm1.takeDown();
   }
%>
<script language="javascript">
  alert("Fractional Bonus Sharing Sales Information Updated!");
  location = "<%=request.getContextPath()%>/jsp/Bonus/ViewSellPriceInfo.jsp";
</script>
</body>
</html>
