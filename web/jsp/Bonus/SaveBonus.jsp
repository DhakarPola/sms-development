<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : October 2005
'Page Purpose  : Saves Bonus Declaration.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@ page language="java" import="java.sql.*,java.util.Vector,java.util.Date,java.text.SimpleDateFormat,java.math.*" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="cm2"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Bonus</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   boolean isConnect2 = cm2.connect();
   if((isConnect==false) || (isConnect1==false) || (isConnect2==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

     Date d = new Date();
     int thisyear = d.getYear();
     thisyear = thisyear - 100;
     String Sthisyear = String.valueOf(thisyear);

     if (Sthisyear.length() < 3)
     {
      int lendiff = 3 - Sthisyear.length();
      for (int ii=0;ii<lendiff;ii++)
      {
       Sthisyear = "0" + Sthisyear;
      }
     }

     String formdate = "BONUS" + Sthisyear;

   String DateDec = "";
   String RecDate = "";
   String decby = "";
   int hratio = 0;
   int bratio = 0;
   int totalbonusshares = 0;
   int totalfractionshares = 0;
   int totalshares = 0;
   int proposedunit = 0;
   boolean b;

   String ndividend2B = "call DELETE_LAST_BOBONUS()";
   b = cm.procedureExecute(ndividend2B);

   String ndividend2C = "call DELETE_LAST_BONUS()";
   b = cm.procedureExecute(ndividend2C);

   String query1 = "SELECT * FROM BONUSDECLARATION_T_VIEW";
   int nofbonus = cm1.queryExecuteCount(query1);
   if (nofbonus == 0)
   {
     %>
       <script language="javascript">
        alert("Declare a Bonus First!");
        history.go(-1);
       </script>
     <%
   }

   cm.queryExecute(query1);

   while(cm.toNext())
   {
     DateDec = cm.getColumnDT("DATE_DEC");
     RecDate = cm.getColumnDT("REC_DATE");
     hratio = cm.getColumnI("HOLDER_RATIO");
     bratio = cm.getColumnI("BONUS_RATIO");
     totalbonusshares = cm.getColumnI("TOTAL_BONUS_SHARES");
     totalfractionshares = cm.getColumnI("TOTAL_FRACTION_SHARES");
     totalshares = cm.getColumnI("TOTAL_SHARES");
     proposedunit = cm.getColumnI("PROPOSED_UNIT");
     decby = cm.getColumnS("DEC_BY");

     String ndividend1 = "call ADD_BONUSDECLARATION('" + DateDec + "','" + RecDate + "', " + hratio + ", " + bratio + ", '" + formdate + "', " + totalbonusshares + ", " + totalfractionshares + ",'','','','',''," + totalshares + "," + proposedunit + ",'" + decby + "')";
     b = cm1.procedureExecute(ndividend1);
   }

   String ndividend2 = "call DELETE_BONUSDECLARATION_T()";
   b = cm.procedureExecute(ndividend2);



   String query2 = "SELECT * FROM BONUS_T_VIEW";
   cm.queryExecute(query2);

   String foliono = "";
   String boid = "";
   String decdate = "";
   int totalshare = 0;
   int bonusshare = 0;
   double fractionshare = 0;
   String bonuscertificateno = "";
   int distfrom = 0;
   int distto = 0;

   while(cm.toNext())
   {
     foliono = cm.getColumnS("FOLIO_NO");
     decdate = cm.getColumnDT("DATE_DEC");
     totalshare = cm.getColumnI("TOTAL_SHARE");
     bonusshare = cm.getColumnI("BONUS_SHARE");
     fractionshare = cm.getColumnF("FRACTION_SHARE");

     bonuscertificateno = cm1.LastCertificate();
     distfrom = 0;
     distto = 0;

     if (bonusshare >= 1)
     {
       distfrom = cm1.LastDistinction();
       distto = distfrom + bonusshare - 1;
     }

     String sdividend3 = "call ADD_BONUS('" + foliono + "', '" + decdate + "', " + totalshare + ", " + bonusshare + ", " + fractionshare + ",'','" + bonuscertificateno + "'," + distfrom + "," + distto + ",'','F','F','F','F')";
     b = cm1.procedureExecute(sdividend3);

     String sdividend3a = "call ADD_LAST_BONUS('" + foliono + "', '" + decdate + "', " + totalshare + ", " + bonusshare + ", " + fractionshare + ",'','" + bonuscertificateno + "'," + distfrom + "," + distto + ",'','F','F','F','F')";
     b = cm1.procedureExecute(sdividend3a);

     if (bonusshare >= 1)
     {
       String addcert1 = "call ADD_CERTIFICATE('" + bonuscertificateno + "', '" + foliono + "','T','F','F','F','F','F','')";
       b = cm1.procedureExecute(addcert1);

       String addcertb1 = "call ADD_CERTIFICATE_BONUS('" + bonuscertificateno + "', '" + foliono + "', '" + DateDec + "')";
       b = cm1.procedureExecute(addcertb1);

       String adddist1 = "call ADD_DISTINCTION(" + distfrom + ", " + distto + ",'" + bonuscertificateno + "')";
       b = cm1.procedureExecute(adddist1);

       String sbalance = "call ADD_SHARE_BALANCE('" + foliono + "', " + bonusshare + ")";
       b = cm1.procedureExecute(sbalance);

       String addtransfer = "call ADD_TRANSFER('" + formdate + "', '" + foliono + "', '" + bonuscertificateno + "', 'B', '" + bonuscertificateno + "','" + distfrom + "','" + distto + "'," + bonusshare + ")";
       b = cm1.procedureExecute(addtransfer);
     }

//     System.out.println("SMS: " + sbalance);
   }

   String addcert1 = "call ADD_SHAREHOLDER_BONUS('" + formdate + "','Mr.','Company Secretary','Sir','Bangladesh Tobacco Company',0,'D'," + totalfractionshares + ",0,'T','F','T','Multinational','0','T','" + DateDec + "')";
   b = cm1.procedureExecute(addcert1);

   String comsec1 = "call ADD_BONUS_FRACTION('" + formdate + "', '" + DateDec + "', 0, " + totalfractionshares + ", 0,'','',null,null,'','F','F','F','F')";
   b = cm1.procedureExecute(comsec1);

   if (proposedunit == 0)
   {
     bonuscertificateno = cm1.LastCertificate();
     distfrom = cm1.LastDistinction();
     distto = distfrom + totalfractionshares - 1;

     String uptrans = "call UPDATE_BONUSTRANSFER('" + bonuscertificateno + "', '" + formdate + "')";
     b = cm1.procedureExecute(uptrans);

     String addcert2 = "call ADD_CERTIFICATE('" + bonuscertificateno + "', '" + formdate + "','T','F','F','F','F','F','Bonus Shares')";
     b = cm1.procedureExecute(addcert2);

     String addcertb2 = "call ADD_CERTIFICATE_BONUS('" + bonuscertificateno + "', '" + formdate + "', '" + DateDec + "')";
     b = cm1.procedureExecute(addcertb2);

     String adddist2 = "call ADD_DISTINCTION(" + distfrom + ", " + distto + ",'" + bonuscertificateno + "')";
     b = cm1.procedureExecute(adddist2);
   }
  else
  {
    double chunks = totalfractionshares/proposedunit;
    distfrom = cm1.LastDistinction();
    int thisdistfrom = distfrom;
    int thisdistto = 0;
    int thistotal = 0;

    for (double i1=chunks;i1>0;i1=i1-1)
    {
     thisdistto = thisdistfrom + proposedunit - 1;
     thistotal = thisdistto - thisdistfrom + 1;
     bonuscertificateno = cm1.LastCertificate();

     String addcert3 = "call ADD_CERTIFICATE('" + bonuscertificateno + "', '" + formdate + "','T','F','F','F','F','F','Bonus Shares')";
     b = cm1.procedureExecute(addcert3);

     String addcertb3 = "call ADD_CERTIFICATE_BONUS('" + bonuscertificateno + "', '" + formdate + "', '" + DateDec + "')";
     b = cm1.procedureExecute(addcertb3);

     String adddist3 = "call ADD_DISTINCTION(" + thisdistfrom + ", " + thisdistto + ",'" + bonuscertificateno + "')";
     b = cm1.procedureExecute(adddist3);

     thisdistfrom = thisdistfrom + proposedunit;
    }
     String uptrans = "call UPDATE_BONUSTRANSFER('" + bonuscertificateno + "', '" + formdate + "')";
     b = cm1.procedureExecute(uptrans);
  }


   String ndividend4 = "call DELETE_BONUS_T()";
   b = cm.procedureExecute(ndividend4);

   String query3 = "SELECT * FROM BOBONUS_T_VIEW";
   cm.queryExecute(query3);

   while(cm.toNext())
   {
     boid = cm.getColumnS("BOID");
     decdate = cm.getColumnDT("DATE_DEC");
     totalshare = cm.getColumnI("TOTAL_SHARE");
     bonusshare = cm.getColumnI("BONUS_SHARE");
     fractionshare = cm.getColumnF("FRACTION_SHARE");

     String sdividend4 = "call ADD_BOBONUS('" + boid + "', '" + decdate + "', " + totalshare + ", " + bonusshare + ", " + fractionshare + ",'','','F','F','F','F')";
     b = cm1.procedureExecute(sdividend4);

     String sdividend4a = "call ADD_LAST_BOBONUS('" + boid + "', '" + decdate + "', " + totalshare + ", " + bonusshare + ", " + fractionshare + ",'','','F','F','F','F')";
     b = cm1.procedureExecute(sdividend4a);

     if (bonusshare >= 1)
     {
       String sbalance1 = "call ADD_BOSHARE_BALANCE('" + boid + "', " + bonusshare + ")";
       b = cm1.procedureExecute(sbalance1);
     }
   }

   String ndividend5 = "call DELETE_BOBONUS_T()";
   b = cm.procedureExecute(ndividend5);

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Accepted Bonus Declaration')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
     cm1.takeDown();
     cm2.takeDown();
   }
%>
<script language="javascript">
  alert("Bonus Accepted!");
  location = "<%=request.getContextPath()%>/jsp/Bonus/SetBonusPostlist.jsp";
</script>
</body>
</html>
