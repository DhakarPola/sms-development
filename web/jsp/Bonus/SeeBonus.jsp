<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : January 2006
'Page Purpose  : Shows Bonus Share Information.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Bonus Share Information</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   String DecDate = request.getParameter("declarationdate");
   int hratio = 0;
   int bratio = 0;
   String bonusfolio = "";
   int totalshares = 0;
   int totalbonusshares = 0;
   int totalfractionshares = 0;
   String Sproposedunit = "";
   String sellingprice = "";
   String bankname = "";
   String bankbranch = "";
   String bankaddress = "";
   String accountno = "";
   String decby = "";
   String recdate = "";
   int distfrom = 0;
   int distto = 0;
   String certfrom = "";
   String certto = "";

   String query21 = "SELECT MIN(CERTIFICATE_NO) as MINCER FROM CERTIFICATE_BONUS_VIEW WHERE DATE_DEC = TO_DATE('" + DecDate + "','DD/MM/YYYY')";
   cm.queryExecute(query21);
   cm.toNext();
   certfrom = cm.getColumnS("MINCER");

   query21 = "SELECT MAX(CERTIFICATE_NO) as MAXCER FROM CERTIFICATE_BONUS_VIEW WHERE DATE_DEC = TO_DATE('" + DecDate + "','DD/MM/YYYY')";
   cm.queryExecute(query21);
   cm.toNext();
   certto = cm.getColumnS("MAXCER");

   query21 = "SELECT * FROM BONUSDECLARATION_VIEW WHERE DATE_DEC = TO_DATE('" + DecDate + "','DD/MM/YYYY')";
   cm.queryExecute(query21);

   while(cm.toNext())
     {
       hratio = cm.getColumnI("HOLDER_RATIO");
       bratio = cm.getColumnI("BONUS_RATIO");
       bonusfolio = cm.getColumnS("BONUS_FOLIO");
       totalshares = cm.getColumnI("TOTAL_SHARES");
       totalbonusshares = cm.getColumnI("TOTAL_BONUS_SHARES");
       totalfractionshares = cm.getColumnI("TOTAL_FRACTION_SHARES");
       sellingprice = cm.getColumnS("SELLING_PRICE");
       bankname = cm.getColumnS("BANK_NAME");
       bankbranch = cm.getColumnS("BANK_BRANCH");
       bankaddress = cm.getColumnS("BANK_ADDRESS");
       accountno = cm.getColumnS("ACCOUNT_NO");
       Sproposedunit = cm.getColumnS("PROPOSED_UNIT");
       decby = cm.getColumnS("DEC_BY");
       recdate = cm.getColumnDT("REC_DATE");

       if (String.valueOf(sellingprice).equals("null"))
         sellingprice = "";
       if (String.valueOf(Sproposedunit).equals("null"))
         Sproposedunit = "";
       if (String.valueOf(bonusfolio).equals("null"))
         bonusfolio = "";
       if (String.valueOf(bankname).equals("null"))
         bankname = "";
       if (String.valueOf(bankbranch).equals("null"))
         bankbranch = "";
       if (String.valueOf(bankaddress).equals("null"))
         bankaddress = "";
       if (String.valueOf(accountno).equals("null"))
         accountno = "";
       if (String.valueOf(decby).equals("null"))
         decby = "";
       if (String.valueOf(recdate).equals("null"))
         recdate = "";
       if (String.valueOf(certfrom).equals("null"))
         certfrom = "";
       if (String.valueOf(certto).equals("null"))
         certto = "";

       distfrom = totalshares + 1;
       distto = totalshares + totalbonusshares;
     }

%>

<form action="UpdateTempBonus.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Bonus Share Information</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date of Declaration</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=DecDate%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Declared By</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=decby%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Record Date</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=recdate%></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Share Holder : Bonus Share Ratio</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style9"><%=hratio%>&nbsp;:&nbsp;<%=bratio%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Total Shares</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=totalshares%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Total Bonus Shares</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=totalbonusshares%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Total Fraction Shares</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=totalfractionshares%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Certificates</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=certfrom%> - <%=certto%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Distinctions</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=distfrom%> - <%=distto%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bonus Folio</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=bonusfolio%></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Split Size&nbsp;
          </div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left" class="style9">
            <%=Sproposedunit%>
            </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Selling Price</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=sellingprice%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Name</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=bankname%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Branch</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=bankbranch%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Address</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=bankaddress%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Account No.</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=accountno%></div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="10" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="100%" scope="row">
            <div align="center">
                   <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="history.go(-1)" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
            </div></th>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
