<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. kamruzzaman
'Creation Date : August 2007
'Page Purpose  : Generating Poll List.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>AGM History</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function tonewdata()
{
  location = "<%=request.getContextPath()%>/jsp/POLL/NewPoll.jsp"
}

//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
.style15{

        }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
      if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

     String decdate = "";

     String query = "SELECT DISTINCT POLL_DATE FROM POLL_DECLARE_VIEW ORDER BY POLL_DATE DESC";
     try
     {
     cm.queryExecute(query);
   %>

     <span class="style7">
     <form name="FormToSubmit" method="GET" action="NewPoll.jsp">
     <img name="B4" src="<%=request.getContextPath()%>/images/btnNew.gif" onclick="tonewdata()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNewOn.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNew.gif'">
     <table width="100%" BORDER=1  cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
     <!--DWLayoutTable-->
     <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>POLL History</center></td></tr>
     </table>
     <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
     <tr bgcolor="#0044B0">
     <td width="32%" class="style9"><div align="center">POLL Date</div></td>
     <td width="17%"><div align="center" class="style9">
     <div align="center">Agenda 1</div>
     </div></td>
     <td width="17%"><div align="center" class="style9">
     <div align="center">Agenda 2</div>
     </div></td>
     <td width="17%"><div align="center" class="style9">
     <div align="center">Agenda 3</div>
     </div></td>
     <td width="17%"><div align="center" class="style9">
     <div align="center">Agenda 4</div>
     </div></td>
     </tr>
     <div align="left">
     <%
     while (cm.toNext())
     {
     decdate = cm.getColumnDT("POLL_DATE");
     String query1 = "SELECT * FROM POLL_CONFIG_VIEW WHERE POLL_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY')";
     cm1.queryExecute(query1);
     cm1.toNext();
     String agenda1=cm1.getColumnS("AGENDA1");
     String agenda2=cm1.getColumnS("AGENDA2");
     String agenda3=cm1.getColumnS("AGENDA3");
     String agenda4=cm1.getColumnS("AGENDA4");
     %>
     </div>
     <tr bgcolor="#E8F3FD">
     <td class="style13"><div align="left" class="style12">
     <div align="center"><%=decdate%>&nbsp;</div>
     </div></td>
     <td width="17%"><div align="center" class="style9">
     <div align="center"><%if(agenda1.equals("T")){%><img name="B2" src="<%=request.getContextPath()%>/images/icoCheck.gif"><%}else{%><img name="B3" src="<%=request.getContextPath()%>/images/icoCross.gif"><%} %></div>
     </div></td>
     <td width="17%"><div align="center" class="style9">
     <div align="center"><%if(agenda2.equals("T")){%><img name="B2" src="<%=request.getContextPath()%>/images/icoCheck.gif"><%}else{%><img name="B3" src="<%=request.getContextPath()%>/images/icoCross.gif"><%} %></div>
     </div></td>
     <td width="17%"><div align="center" class="style9">
     <div align="center"><%if(agenda3.equals("T")){%><img name="B2" src="<%=request.getContextPath()%>/images/icoCheck.gif"><%}else{%><img name="B3" src="<%=request.getContextPath()%>/images/icoCross.gif"><%} %></div>
     </div></td>
     <td width="17%"><div align="center" class="style9">
     <div align="center"><%if(agenda4.equals("T")){%><img name="B2" src="<%=request.getContextPath()%>/images/icoCheck.gif"><%}else{%><img name="B3" src="<%=request.getContextPath()%>/images/icoCross.gif"><%} %></div>
     </div></td>
     </tr>
     <%
     }
    }catch(Exception e){}
     %>
    </form>
    <%
     if (isConnect)
   {
     try
     {
       cm.takeDown();
       cm1.takeDown();
     }catch(Exception e){}
   }
   %>
</body>
</html>
