<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : November 2006
'Page Purpose  : Add a new Poll Date.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Add Poll Date</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function formconfirm()
{
  if (confirm("Do you want to enter the mentioned POLL Information?"))
  {
    return true;
  }
}

function showAgenda(agenda)
{
  var mes="";
  if(agenda=='1')
  {
    mes="Resolved that the Reports of Directors and the Balance sheet and Profit and Loss Account for the year ended 31 December 2006, and the notes thereto and Report of the Directors and Auditors\u2019 Reports thereon,  be and are hereby adopted.";
  }
  else if(agenda=='2')
  {
    mes="That a Dividend of Tk.3.00 per share of Tk.10.00 each, on the issued and paid up ordinary shares of the Company, as recommended by the Directors, be approved and that the Directors and/or the Secretary of the Company be and are hereby authorised to pay the same accordingly, to the shareholders whose names appear on the Share Register or/and Depository Resister of the Company at 10 May 2007.";
  }
  else if(agenda=='3')
  {
    mes="That Messrs. Md. Ziaul Haque Khondker and Mr. M. A. Mokaddem retire from the Board by rotation, Mr. Nicholas S. Hales and Md. Mahbubur Rahman, who were appointed to the Board since the last Annual General Meeting, also retires. Messrs. Md. Ziaul Haque Khondker, Mr. M. A. Mokaddem, Mr. Nicholas S. Hales and Md. Mahbubur Rahman, being eligible, be and are hereby elected Directors of the Company. In accordance with the Articles of Association, the term of office of Mr. M. A. Mokaddem will be upto the conclusion of the next Annual General Meeting, since he have reached an age beyond 65";
  }
  else if(agenda=='4')
  {
    mes="That Messrs. Rahman Rahman Huq be and are hereby appointed Auditors for the Company's Financial Year 2007 at a remuneration of Tk. 650,000/=(Taka Six Lac  fifty thousand) plus VAT";
  }
  count=0;
  if(mes!="")
  {
    count++;
    nArray[count]=mes;
  }
  if(count!=0)
  {
    ShowAgenda('<%=request.getContextPath()%>');
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('polldate','- AGM Date (Option must be entered)');
  var test=0;

  if(document.forms[0].check1.checked)
  {
    test=1;
  }
  if(document.forms[0].check2.checked)
  {
    test=1;
  }
  if(document.forms[0].check3.checked)
  {
    test=1;
  }
  if(document.forms[0].check4.checked)
  {
    test=1;
  }
  if(test==0)
  {
    count = count + 1;
    nArray[count] = '- Choose at least one Agenda for a poll.';
  }
  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
    else{
   ShowAllAlertMsg();
   return false;
   }
}
function toagmlist()
{
  location = "<%=request.getContextPath()%>/jsp/POLL/GeneratePollList.jsp"
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
  .link{cursor:hand;
           color:#0A2769;
         }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
%>

<form action="SavePoll.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Add Poll Information</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;POLL Date</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
        <select name="polldate">
        <option>--Please Select--</option>
        <%
        String agm_date="select distinct AGMDATE from AGMAttendance_view order by AGMDATE desc";
        try
        {
          cm.queryExecute(agm_date);
          while(cm.toNext())
          {
            String a_date=cm.getColumnDT("AGMDATE");
            %>
            <option value="<%=a_date%>"><%=a_date%></option>
            <%
          }
        }catch(Exception e){}
        %>
         </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;</div></th>
        <td width="1%"></td>
        <td width="55%"><div align="left">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;</div></th>
        <td width="1%"></td>
        <td width="55%"><div align="left"><font size="+1" color="#0A2769">Poll Agenda</font>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;</div></th>
        <td width="1%"></td>
        <td width="55%"><div align="left">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;<a onclick="showAgenda('1')" class="link"><u>Agenda 1</u></a></div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left"><input type="checkbox" name="check1" value="1" />
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;<a onclick="showAgenda('2')" class="link"><u>Agenda 2</u></a></div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left"><input  name="check2" type="checkbox" value="2" />
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;<a onclick="showAgenda('3')" class="link"><u>Agenda 3</u></a></div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left"><input name="check3" type="checkbox" value="3" />
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;<a onclick="showAgenda('4')" class="link"><u>Agenda 4</u></a></div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left"><input name="check4" type="checkbox" value="4" />
        </div></td>
      </tr>
  </table>
  <br>
  </div>
  <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
  <tr>
  <th width="34%" scope="row">
  <div align="right">
  <img name="B3" src="<%=request.getContextPath()%>/images/btnBack.gif"  onclick="toagmlist()" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnBackOn.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnBack.gif'">
  </div></th>
  <td width="33%" align="center">
  <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
  </td>
  <td width="33%">
  <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
  </td>
  </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
