<script>
/*
'******************************************************************************************************************************************
'Script Author :Md. Kamruzzaman
'Creation Date : November 2006
'Page Purpose  : Update Attendance.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update Attendance</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String agm_date=request.getParameter("agmdate");
  String boid = String.valueOf(request.getParameter("Vboid"));
  String Fldname = "";
  String updatequery = "";
    String BoxValue = String.valueOf(request.getParameter("AStatus"));
    BoxValue=cm.replace(BoxValue,"'","''");

    if (String.valueOf(BoxValue).equals("null"))
    {
      BoxValue = "F";
    }
    if (!BoxValue.equalsIgnoreCase(""))
    {
		//updatequery="UPDATE AGMATTENDANCE SET IS_PRESENT='"+BoxValue+"' where boid='" + boid + "'and AGMDATE=TO_DATE('" + agm_date + "','DD/MM/YYYY')";
                updatequery="call UPDATE_AGMATTENDANCE ('" + BoxValue +"','" + agm_date + "','" + boid + "')";
                //cm.queryExecute(updatequery);
                boolean b = cm.procedureExecute(updatequery);
    }

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  //alert("Attendance Done!");
  location = "<%=request.getContextPath()%>/jsp/AGM/AGMAttendance.jsp?dateselect=<%=agm_date%>&searchFolio=<%=boid%>";
</script>
</body>
</html>
