<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : August 2007
'Page Purpose  : Saves Bank Account Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Bank Account Info.</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String CompBankID = String.valueOf(request.getParameter("compbnkid"));
  String CompBankAcc = String.valueOf(request.getParameter("compbnkacc"));
  String CurBankID = String.valueOf(request.getParameter("curbnkid"));
  String CurBankAcc = String.valueOf(request.getParameter("curbnkacc"));
  String UncBankID = String.valueOf(request.getParameter("uncbnkid"));
  String UncBankAcc = String.valueOf(request.getParameter("uncbnkacc"));

  CompBankAcc=cm.replace(CompBankAcc,"'","''");
  CurBankAcc=cm.replace(CurBankAcc,"'","''");
  UncBankAcc=cm.replace(UncBankAcc,"'","''");

  String query2 = "SELECT * FROM BANK_ACCOUNTS_VIEW";
  int countrecords = cm.queryExecuteCount(query2);

  if (countrecords == 0)
  {
    String nacc = "call ADD_BANK_ACCOUNTS('" + CompBankAcc + "', '" + CurBankAcc + "', '" + UncBankAcc + "', " + CompBankID + ", " + CurBankID + "," + UncBankID + ")";
    boolean b = cm.procedureExecute(nacc);
  }
  else
  {
    String nacc = "call UPDATE_BANK_ACCOUNTS('" + CompBankAcc + "', '" + CurBankAcc + "', '" + UncBankAcc + "', " + CompBankID + ", " + CurBankID + "," + UncBankID + ")";
    boolean b = cm.procedureExecute(nacc);
  }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added Bank Account Information')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  alert("Bank Account Information Updated.");
  location = "<%=request.getContextPath()%>/jsp/Accounts/AccountConfig.jsp";
</script>
</body>
</html>
