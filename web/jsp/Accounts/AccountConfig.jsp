<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : August 2007
'Page Purpose  : Sets Accounts Configuration.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.util.Date,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<jsp:useBean id="cal" class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>

<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.imagehand{
  cursor:hand;
  color:#FFFFFF;
 }

-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Accounts Configuration</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function formconfirm()
{
  if (confirm("Do you want to enter the mentioned Accounts Configuration?\nThis will overwrite the present configuration."))
  {
    return true;
  }
}

function showfields(id1,id2)
{
  if (document.all[id1].style.display=='none')
  {
   document.all[id1].style.display = '';
   document.all[id2].src = '<%=request.getContextPath()%>/images/twisteeDown.gif';
  }
  else
  {
   document.all[id1].style.display = 'none';
   document.all[id2].src = '<%=request.getContextPath()%>/images/twisteeUp.gif';
  }
}

function setbankvalues(value1,value2)
{
  var val1 = value1;
  var val2 = value1.indexOf("%");
  var val3 = val1.substring(0,val2);
  var val4 = value1.indexOf("$");
  var val5 = val1.substring(val2 + 1,val4);
  var val10 = value1.indexOf("?");
  var val11 = val1.substring(val4 + 1,val10);
  var val6 = val1.substring(val10 + 1,val1.length);

  var val7 = value2 + "bchname";
  var val8 = value2 + "bnkaddress";
  var val9 = value2 + "bnkid";

  document.all[val7].value = val5;
  document.all[val8].value = val11;
  document.all[val9].value = val6;
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  SelectValidate('compbnkname','- Company Bank Name (Option must be entered)');
  SelectValidate('curbnkname','- Current Dividend Bank Name (Option must be entered)');
  SelectValidate('uncbnkname','- Unclaimed Dividend Bank Name (Option must be entered)');
  BlankValidate('compbnkacc','- Company Account No. (Option must be entered)');
  BlankValidate('curbnkacc','- Current Dividend Account No. (Option must be entered)');
  BlankValidate('uncbnkacc','- Unclaimed Dividend Account No. (Option must be entered)');

  val1  = trim(window.document.all["compbnkacc"].value);
  val2  = trim(window.document.all["curbnkacc"].value);
  val3  = trim(window.document.all["uncbnkacc"].value);

  if ((val1 != "") && (val2 != "") && (val3 != ""))
   {
     if (val1 == val2)
     {
       count = count + 1;
       nArray[count] = '- Company Account & CDA Cannot be Same';
     }

     if (val1 == val3)
     {
       count = count + 1;
       nArray[count] = '- Company Account and UDA Cannot be Same';
     }

     if (val2 == val3)
     {
       count = count + 1;
       nArray[count] = '- CDA & UDA Cannot be Same';
     }
   }

  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   String compacc = "";
   int compbankid = 0;
   String compbankbranch = "";
   String compbankaddress = "";
   String uncacc = "";
   int uncbankid = 0;
   String uncbankbranch = "";
   String uncbankaddress = "";
   String curacc = "";
   int curbankid = 0;
   String curbankbranch = "";
   String curbankaddress = "";

   String query4m = "SELECT * FROM BANK_ACCOUNTS_VIEW";
   cm.queryExecute(query4m);

   while(cm.toNext())
   {
     compbankid = cm.getColumnI("COMPANY_BANK_ID");
     compacc = cm.getColumnS("COMPANY_ACCOUNT");
     uncbankid = cm.getColumnI("UNCLAIMED_BANK_ID");
     uncacc = cm.getColumnS("UNCLAIMED_ACCOUNT");
     curbankid = cm.getColumnI("CURRENT_BANK_ID");
     curacc = cm.getColumnS("CURRENT_ACCOUNT");
   }

%>

<form action="SaveAccountInfo.jsp" method="post" name="FileForm">

  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Accounts Configuration</td></tr>

  <tr bgcolor="#005FFB">
    <td width="5%" height="20">&nbsp;&nbsp;
    <img name="DivInfoimg" id="DivInfo" class="DivInfoimg" src="<%=request.getContextPath()%>/images/twisteeUp.gif" onclick="showfields('DivInfospan','DivInfoimg');">
    <strong>
    <a class="imagehand" onclick="showfields('DivInfospan','DivInfoimg');">&nbsp;&nbsp;Company Account</a>
    </strong>
    </td>
  </tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <div align="left">
 <SPAN id="DivInfospan" name="DivInfospan" style="display:none">
  <br>
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Name</div></th>
        <td>:</td>
        <td><div align="left">
         <select name="compbnkname" class="SL2TextFieldListBox" onchange="setbankvalues(document.all.compbnkname.value,'comp')">
            <option>--- Please Select ---</option>
            <%
              int bid1 = 0;
              String bname1 = "";
              String bbranch1 = "";
              String baddress1 = "";

              String query2 = "SELECT * FROM BANK_VIEW ORDER BY UPPER(BANK_NAME)";
              cm.queryExecute(query2);

              while(cm.toNext())
              {
                 bid1 = cm.getColumnI("BANK_ID");
                 bname1 = cm.getColumnS("BANK_NAME");
                 bbranch1 = cm.getColumnS("BANK_BRANCH");
                 baddress1 = cm.getColumnS("BANK_ADDRESS");
                 String combo1 = bname1 + "%" + bbranch1 + "$" + baddress1 + "?" + bid1;

                 if (bid1 == compbankid)
                 {
                   compbankbranch = bbranch1;
                   compbankaddress = baddress1;
                   %>
                   <option selected="selected" value="<%=combo1%>"><%=bname1%></option>
                   <%
                 }
                 else
                 {
                   %>
                   <option value="<%=combo1%>"><%=bname1%></option>
                   <%
                 }
              }
            %>
         </select>
         <input type="hidden" name="compbnkid" value="<%=compbankid%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Branch Name</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" value="<%=compbankbranch%>" name="compbchname" class="SL68TextField" onfocus="this.blur()">
        </div></td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Address</div></th>
        <td valign="top">:</td>
        <td><div align="left">
          <textarea name="compbnkaddress" cols="20" rows="3" wrap="VIRTUAL" id="compbnkaddress" class="ML10TextField" onfocus="this.blur()"><%=compbankaddress%></textarea>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Account No.</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" value="<%=compacc%>" name="compbnkacc" class="SL68TextField">
        </div></td>
      </tr>
  </table>
  <br>
  </span>


  <tr bgcolor="#005FFB">
    <td width="5%" height="20">&nbsp;&nbsp;
    <img name="TaxInfoimg" id="TaxInfo" class="TaxInfoimg" src="<%=request.getContextPath()%>/images/twisteeUp.gif" onclick="showfields('TaxInfospan','TaxInfoimg');">
    <strong>
    <a class="imagehand" onclick="showfields('TaxInfospan','TaxInfoimg');">&nbsp;&nbsp;Current Dividend Account (CDA)</a>
    </strong>
    </td>
  </tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <div align="left">
 <SPAN id="TaxInfospan" name="TaxInfospan" style="display:none">
  <br>
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Name</div></th>
        <td>:</td>
        <td><div align="left">
         <select name="curbnkname" class="SL2TextFieldListBox" onchange="setbankvalues(document.all.curbnkname.value,'cur')">
            <option>--- Please Select ---</option>
            <%
              int bid2 = 0;
              String bname2 = "";
              String bbranch2 = "";
              String baddress2 = "";

              String query3 = "SELECT * FROM BANK_VIEW ORDER BY UPPER(BANK_NAME)";
              cm.queryExecute(query3);

              while(cm.toNext())
              {
                 bid2 = cm.getColumnI("BANK_ID");
                 bname2 = cm.getColumnS("BANK_NAME");
                 bbranch2 = cm.getColumnS("BANK_BRANCH");
                 baddress2 = cm.getColumnS("BANK_ADDRESS");
                 String combo2 = bname2 + "%" + bbranch2 + "$" + baddress2 + "?" + bid2;

                 if (bid2 == curbankid)
                 {
                   curbankbranch = bbranch2;
                   curbankaddress = baddress2;
                   %>
                   <option selected="selected" value="<%=combo2%>"><%=bname2%></option>
                   <%
                 }
                 else
                 {
                   %>
                   <option value="<%=combo2%>"><%=bname2%></option>
                   <%
                 }
              }
            %>
         </select>
         <input type="hidden" name="curbnkid"  value="<%=curbankid%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Branch Name</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" value="<%=curbankbranch%>" name="curbchname" class="SL68TextField" onfocus="this.blur()">
        </div></td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Address</div></th>
        <td valign="top">:</td>
        <td><div align="left">
          <textarea name="curbnkaddress" cols="20" rows="3" wrap="VIRTUAL" id="curbnkaddress" class="ML10TextField" onfocus="this.blur()"><%=curbankaddress%></textarea>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Account No.</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text"  value="<%=curacc%>" name="curbnkacc" class="SL68TextField">
        </div></td>
      </tr>
  </table>
  <br>
  </span>


  <tr bgcolor="#005FFB">
    <td width="5%" height="20">&nbsp;&nbsp;
    <img name="BankInfoimg" id="BankInfo" class="BankInfoimg" src="<%=request.getContextPath()%>/images/twisteeUp.gif" onclick="showfields('BankInfospan','BankInfoimg');">
    <strong>
    <a class="imagehand" onclick="showfields('BankInfospan','BankInfoimg');">&nbsp;&nbsp;Unclaimed Dividend Account (UDA)</a>
    </strong>
    </td>
  </tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
 <SPAN id="BankInfospan" name="BankInfospan" style="display:none">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Name</div></th>
        <td>:</td>
        <td><div align="left">
         <select name="uncbnkname" class="SL2TextFieldListBox" onchange="setbankvalues(document.all.uncbnkname.value,'unc')">
            <option>--- Please Select ---</option>
            <%
              int bid3 = 0;
              String bname3 = "";
              String bbranch3 = "";
              String baddress3 = "";

              String query4 = "SELECT * FROM BANK_VIEW ORDER BY UPPER(BANK_NAME)";
              cm.queryExecute(query4);

              while(cm.toNext())
              {
                 bid3 = cm.getColumnI("BANK_ID");
                 bname3 = cm.getColumnS("BANK_NAME");
                 bbranch3 = cm.getColumnS("BANK_BRANCH");
                 baddress3 = cm.getColumnS("BANK_ADDRESS");
                 String combo3 = bname3 + "%" + bbranch3 + "$" + baddress3 + "?" + bid3;

                 if (bid3 == uncbankid)
                 {
                   uncbankbranch = bbranch3;
                   uncbankaddress = baddress3;

                   %>
                    <option selected="selected" value="<%=combo3%>"><%=bname3%></option>
                   <%
                 }
                 else
                 {
                   %>
                    <option value="<%=combo3%>"><%=bname3%></option>
                   <%
                 }
              }
            %>
         </select>
         <input type="hidden" name="uncbnkid"  value="<%=uncbankid%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Branch Name</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" value="<%=uncbankbranch%>" name="uncbchname" class="SL68TextField" onfocus="this.blur()">
        </div></td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Address</div></th>
        <td valign="top">:</td>
        <td><div align="left">
          <textarea name="uncbnkaddress" cols="20" rows="3" wrap="VIRTUAL" id="uncbnkaddress" class="ML10TextField" onfocus="this.blur()"><%=uncbankaddress%></textarea>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Account No.</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" value="<%=uncacc%>" name="uncbnkacc" class="SL68TextField">
        </div></td>
      </tr>
  </table>
  <br>
  </span>


      </div>
      <table width="100%" BORDER=0  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
      <tr bgcolor="#E8F3FD">
        <th width="50%" scope="row" >
            <div align="right">
              <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
            </div></th>
        <td width="50%" >
	      <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
      </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
