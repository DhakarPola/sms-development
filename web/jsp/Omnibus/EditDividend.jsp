<script>
/*
'******************************************************************************************************************************************
'Script Author : Jahirul Islam
'Creation Date : 26th February' 2013
'Page Purpose  : Edit Omnibus Dividend.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Dividend List</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<script language="javascript">
<!--


function gotoback()
{
  location = "<%=request.getContextPath()%>/jsp/Omnibus/SDividendList.jsp";
}




function SubmitThis()
{
  count = 0;
  /*BlankValidate('mrecondate','- Collection Date (Option must be entered)');*/

  if (count == 0)
  {
    document.forms[0].action = "AddDividendforOmnibus.jsp";
    document.forms[0].submit();
  }
  else
  {
   ShowAllAlertMsg();
   return false;
  }
}




 function askdelete()
 {
  if (confirm("Do you want to Delete this BO?"))
  {
   document.forms[0].submit();
  }
 }



//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style17 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

  //System.out.println("---------------DividendforOmnibusCalc.jsp--------");
  String decdate = String.valueOf(request.getParameter("dateselect"));
  //System.out.println("---------------DividendforOmnibusCalc.jsp----decdate----"+decdate);
  String oid = String.valueOf(request.getParameter("omnibusName"));
  //System.out.println("---------------DividendforOmnibusCalc.jsp----oid----"+oid);
  String SStartingValue = String.valueOf(request.getParameter("oStartValue"));
  //System.out.println("---------------DividendforOmnibusCalc.jsp----SStartingValue----"+SStartingValue);
  String SEndingValue = String.valueOf(request.getParameter("oEndValue"));
  //System.out.println("---------------DividendforOmnibusCalc.jsp----SEndingValue----"+SEndingValue);


    int ioid = Integer.parseInt(oid);
    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    int Chunk = 20;
//    StartingValue = StartingValue + 1;
    EndingValue = StartingValue + Chunk - 1;

    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;
    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }


  //String cate="SELECT * FROM BODIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + decdate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' ORDER BY BO_ID";
  //cm.queryExecute(cate);
  //cm.toNext();




%>






  <form method="GET" action="DeleteBoDividend.jsp">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Dividend for Omnibus</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date of Declaration</div></th>
        <td width="1%">:</td>
        <td>
          <div align="left">
          <input type="text" name="ddate" value="<%=decdate%>" class="SL2TextField" readonly="readonly">
          </div>
        </td>
      </tr>

      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Omnibus</div></th>
        <td width="1%">:</td>
        <td>
          <div align="left">
          <%
              String oname = "";

              String query2 = "SELECT NAME FROM OMNIBUS_CONFIG WHERE REC_ID="+oid;
              cm.queryExecute(query2);

              while(cm.toNext())
              {
                oname=cm.getColumnS("NAME");

            %>
          <input type="text" name="oname" value="<%=oname%>" class="SL2TextField" readonly="readonly">
            <%
              }
            %>
          </div>
        </td>
      </tr>

  </table>
  </div>
  </center>
  </td>
  </tr>
  </table>


  <div align="left" ><br /></div>

  <SPAN id="dprint">
    <img name="B4" src="<%=request.getContextPath()%>/images/btnAdd.gif" onclick="SubmitThis()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAddOn.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAdd.gif'">
    <img name="B6" src="<%=request.getContextPath()%>/images/btnDelete.gif" onclick="askdelete()" onMouseOver="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnDeleteOn.gif'" onMouseOut="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnDelete.gif'">
    <img name="B7" src="<%=request.getContextPath()%>/images/btnClose.gif" onclick="gotoback()" onMouseOver="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
  </SPAN>


  <span class="style7">
  <table width="100%" BORDER=0  cellpadding="0" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td height="30" bgcolor="#0044B0" class="style17" valign="middle"><center>BO Dividend For Omnibus</center></td></tr>
  </table>
  <%
  int rowcounter1 = 0;
  String oboid="";
  String owarrentNo="";
  String odividend="";
  //String oselected="";



  //String cate="SELECT * FROM BODIVIDEND_VIEW WHERE WARRANT_NO NOT IN (((SELECT WARRANT_NO FROM OMNIBUS_BO_DIVIDEND_T WHERE ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY') AND STATUS=UPPER('S')) UNION (SELECT WARRENT_NO FROM OMNIBUS_BO_DIVIDEND WHERE ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY')))) AND ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' ORDER BY BO_ID";
  String cate="SELECT * FROM BODIVIDEND WHERE WARRANT_NO IN (((SELECT WARRANT_NO FROM OMNIBUS_BO_DIVIDEND_T WHERE OMNIBUS_DIV_ID=(SELECT REC_ID FROM OMNIBUS_CONFIG_DIVIDEND WHERE OMNIBUS_ID="+oid+" AND DIVIDEND_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')) AND OMNIBUS_AC_ID="+oid+" AND ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY')))) AND ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY')  AND LOCKED = 'F' ORDER BY BO_ID";
  //System.out.println("------------cate------->"+cate);
  int fcc = cm.queryExecuteCount(cate);


  if (nStartingValue > (fcc-Chunk+1))
    {
      nStartingValue = fcc - Chunk + 1;
    }
    if (nEndingValue > fcc)
    {
      nEndingValue = fcc;
    }
    if (nStartingValue < 1)
    {
      nStartingValue = 1;
    }
    if (nEndingValue < Chunk)
    {
      nEndingValue = Chunk;
    }

    //String query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM BODIVIDEND_VIEW WHERE WARRANT_NO NOT IN (((SELECT WARRANT_NO FROM OMNIBUS_BO_DIVIDEND_T WHERE ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY') AND STATUS=UPPER('S')) UNION (SELECT WARRENT_NO FROM OMNIBUS_BO_DIVIDEND WHERE ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY')))) AND ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' ORDER BY BO_ID) div where rownum <= " + EndingValue + ") where rnum >= " + StartingValue + "";
    String query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM BODIVIDEND WHERE WARRANT_NO IN (((SELECT WARRANT_NO FROM OMNIBUS_BO_DIVIDEND_T WHERE OMNIBUS_DIV_ID=(SELECT REC_ID FROM OMNIBUS_CONFIG_DIVIDEND WHERE OMNIBUS_ID="+oid+" AND DIVIDEND_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')) AND OMNIBUS_AC_ID="+oid+" AND ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY')))) AND ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY')  AND LOCKED = 'F' ORDER BY BO_ID) div)"; // where rownum <= " + EndingValue + ") where rnum >= " + StartingValue + "";
    System.out.println("----calc--------query1------->"+query1);
    cm.queryExecute(query1);


  %>

  <table width="100%"  border="0" cellpadding="0" cellspacing="0" bordercolor="#0044B0">
      <tr bgcolor="#E8F3FD">
	<td><div></div></td>
      </tr>
  </table>


  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="25%" class="style17">
      <div align="center">BO ID</div>
    </td>
    <td width="25%"><div align="center" class="style17">
      <div align="center">Warrant No.</div>
    </div></td>
    <td width="25%"><div align="center" class="style17">
      <div align="center">Dividend</div>
    </div></td>
  </tr>

  <div>
  <%


  while(cm.toNext())
  {
    rowcounter1++;
    oboid=cm.getColumnS("BO_ID");
    owarrentNo=cm.getColumnS("WARRANT_NO");
    odividend=cm.getColumnS("DIVIDEND");
    //oselected=cm.getColumnS("OMNIBUS_SELECT");

       if (String.valueOf(oboid).equals("null"))
         oboid = "";
       if (String.valueOf(owarrentNo).equals("null"))
         owarrentNo = "";
       if (String.valueOf(odividend).equals("null"))
         odividend = "";
       //if (String.valueOf(oselected).equals("null"))
         //oselected = "";


    //System.out.println("------0-calc--oselected--------"+oselected);
  %>

  <tr bgcolor="#E8F3FD">
    <td width="25%" class="style9">
      <div align="center" class="style9">
      <div align="left" class="style9">
      <label><input type="radio" checked="checked" name="divtOWno" value="<%=owarrentNo%>"></label>
      &nbsp;&nbsp;&nbsp<%=oboid%>&nbsp;&nbsp;
      </div>
      </div>

    </td>
    <td width="25%">
      <div align="center" class="style9">
      <div align="center"><%=owarrentNo%>&nbsp;</div>
      </div>
    </td>
    <td width="25%">
      <div align="center" class="style9">
      <div align="center"><%=odividend%>&nbsp;</div>
      </div>
    </td>

  </tr>
  </div>
  <%
  }
  %>


  </table>


  <!--HIDDEN FIELDS-->

            <input type="hidden" name="ddate" value="<%=decdate%>">
            <input type="hidden" name="oStartValue" value="1">
            <input type="hidden" name="oEndValue" value="0">
            <input type="hidden" name="oid" value="<%=oid%>">




<%






  /*




  String addmemo = "call ADD_DIVIDEND_MEMO(UPPER('"+ memoname[x] +"'),'"+memotype[x]+"','" + totalSH[x] + "','" + totaldiv[x] + "','" + totaltax[x] + "','"+memoid+"')";
  boolean add = cm.procedureExecute(addmemo);
  }



   */


  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Calculated Dividend for Omnibus')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }

%>


</body>
</html>
