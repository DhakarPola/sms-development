<script>
/*
'******************************************************************************************************************************************
'Script Author : Hasanul Azaz Aman
'Creation Date : Feb 2013
'Page Purpose  : Shows  List By Group.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="cm2"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

 String DateDec = String.valueOf(request.getParameter("dateselect"));
%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
.style19 {
	color: #0A2769;
	font-weight: bold;
}
.style20
{
	color: #0044B0;
	font-size: 11px;
	font-weight:bold;
}

-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Omnibus Group List</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

function goPrevious(pnomno,pstart,pend)
{
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Omnibus/group/ViewOmnibusbyNominee.jsp?nomineeno=" + pnomno;
  thisurl = thisurl + "&StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;
  thisurl = thisurl + "&dateselect=" + "<%=DateDec%>";

  location = thisurl;
}

function gosearch(sdate,stype,sstart,send)
{
  var list =  document.forms[0].nomineeinfo;
  location = list.options[list.selectedIndex].value;
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style17 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   boolean isConnect2 = cm2.connect();
   if((isConnect==false) || (isConnect1==false) || (isConnect2==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String SNomineeNo = String.valueOf(request.getParameter("nomineeno"));
    String SStartingValue = String.valueOf(request.getParameter("StartValue"));
    String SEndingValue = String.valueOf(request.getParameter("EndValue"));

    int NomineeNo = 0;
    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    int Chunk = 20;
    EndingValue = StartingValue + Chunk - 1;

    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;

    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }

    NomineeNo = Integer.parseInt(SNomineeNo);
    //String query1cc = "SELECT * FROM SHAREHOLDER_VIEW WHERE NOMINEE_NO = '" + NomineeNo + "' AND ACTIVE = 'T' ORDER BY SUBSTR(FOLIO_NO,3,6)"; //FOLIO_NO";
    String query1cc="SELECT BODIVIDEND.BO_ID,BODIVIDEND.NAME_FIRSTHOLDER,BODIVIDEND.WARRANT_NO,BODIVIDEND.DIVIDEND,BODIVIDEND.total_Share,OMNIBUS_CONFIG_DIVIDEND.group_warrant_no FROM BODIVIDEND_EXCEL_VIEW BODIVIDEND,OMNIBUS_BO_DIVIDEND,OMNIBUS_CONFIG_DIVIDEND where OMNIBUS_BO_DIVIDEND.OMNIBUS_AC_ID="+NomineeNo+" and bodividend.warrant_no=OMNIBUS_BO_DIVIDEND.warrent_no and OMNIBUS_CONFIG_DIVIDEND.rec_id=OMNIBUS_BO_DIVIDEND.OMNIBUS_DIV_ID";

    int fcc = cm.queryExecuteCount(query1cc);

    if (nStartingValue > (fcc-Chunk+1))
    {
      nStartingValue = fcc - Chunk + 1;
    }
    if (nEndingValue > fcc)
    {
      nEndingValue = fcc;
    }
    if (nStartingValue < 1)
    {
      nStartingValue = 1;
    }
    if (nEndingValue < Chunk)
    {
      nEndingValue = Chunk;
    }

    //String query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM SHAREHOLDER_VIEW WHERE NOMINEE_NO = '" + NomineeNo + "' AND ACTIVE = 'T' ORDER BY SUBSTR(FOLIO_NO,3,6)) div where rownum <= " + EndingValue + ") where rnum >= " + StartingValue + "";
    String query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT BODIVIDEND.BO_ID,BODIVIDEND.NAME_FIRSTHOLDER,BODIVIDEND.WARRANT_NO,BODIVIDEND.DIVIDEND,BODIVIDEND.total_Share,OMNIBUS_CONFIG_DIVIDEND.group_warrant_no FROM BODIVIDEND_EXCEL_VIEW BODIVIDEND,OMNIBUS_BO_DIVIDEND,OMNIBUS_CONFIG_DIVIDEND where OMNIBUS_BO_DIVIDEND.OMNIBUS_AC_ID="+NomineeNo+" and bodividend.warrant_no=OMNIBUS_BO_DIVIDEND.warrent_no and OMNIBUS_CONFIG_DIVIDEND.rec_id=OMNIBUS_BO_DIVIDEND.OMNIBUS_DIV_ID) div where rownum <= " + EndingValue + ") where rnum >= " + StartingValue + "";
    // String query1cc="SELECT BODIVIDEND.BO_ID,BODIVIDEND.NAME_FIRSTHOLDER,BODIVIDEND.WARRANT_NO,BODIVIDEND.DIVIDEND,BODIVIDEND.total_Share,OMNIBUS_CONFIG_DIVIDEND.group_warrant_no FROM BODIVIDEND_EXCEL_VIEW BODIVIDEND,OMNIBUS_BO_DIVIDEND,OMNIBUS_CONFIG_DIVIDEND where OMNIBUS_BO_DIVIDEND.OMNIBUS_AC_ID="+NomineeNo+" and bodividend.bo_id=OMNIBUS_BO_DIVIDEND.bo_id and OMNIBUS_CONFIG_DIVIDEND.omnibus_id=OMNIBUS_BO_DIVIDEND.OMNIBUS_DIV_ID"
    cm.queryExecute(query1);

    String foliono = "";
    String folioname = "";
    int totalshares = 0;
    int gtotalshares = 0;
    double inddividend = 0;
    double ginddividend = 0;
    String iwarrantno = "";
    String gwarrantno = "";
    int grandTotalShare = 0;
    double grandTotalDividend = 0;
    int rowcounter1 = 0;
%>
  <span class="style7">
  <form method="GET" action="ViewOmnibusbyNominee.jsp">
  <SPAN id="dprint">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
  </SPAN>
  <table width="100%" BORDER=0  cellpadding="0" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td height="30" bgcolor="#0044B0" class="style17" valign="middle"><center>Omnibus Group List</center></td></tr>
  </table>
  <table width="100%"  border="0" cellpadding="0" cellspacing="0" bordercolor="#0044B0">
      <tr bgcolor="#E8F3FD">
	<td width="10%" style="border-left: solid 1px #0044B0;">&nbsp;</td>
	<td width="10%"></td>
	<td width="22%"></td>
	<td width="19%" height="35" class="style19" align="right">Omnibus Group :&nbsp;&nbsp;&nbsp;</td>
	<td width="19%">
          <select name="nomineeinfo" class="SL2TextFieldListBox" onChange="gosearch()">
          <option value="<%=request.getContextPath()%>/jsp/Omnibus/group/ViewOmnibusbyNominee.jsp?nomineeno=0&StartValue=0&EndValue=0&dateselect=<%=DateDec%>">--- Please Select ---</option>
            <%
              String thenomineeno = "";
              String thenomineename = "";

              String query1n = "select * from OMNIBUS_CONFIG ORDER BY NAME ASC";
              cm1.queryExecute(query1n);




              while(cm1.toNext())
              {

                 thenomineeno = cm1.getColumnS("REC_ID");

                 thenomineename = cm1.getColumnS("NAME");


				 if (String.valueOf(thenomineeno).equals("null"))
                  thenomineeno = "";

                 if (SNomineeNo.equalsIgnoreCase(thenomineeno))
                 {
                  //  System.out.println("Come here-----------------------------------"+SNomineeNo);
                  String query1nb = "SELECT BODIVIDEND.BO_ID,BODIVIDEND.NAME_FIRSTHOLDER,BODIVIDEND.WARRANT_NO,BODIVIDEND.DIVIDEND,BODIVIDEND.total_Share,OMNIBUS_CONFIG_DIVIDEND.group_warrant_no FROM BODIVIDEND_EXCEL_VIEW BODIVIDEND,OMNIBUS_BO_DIVIDEND,OMNIBUS_CONFIG_DIVIDEND where OMNIBUS_BO_DIVIDEND.OMNIBUS_AC_ID="+SNomineeNo+" and  bodividend.warrant_no=OMNIBUS_BO_DIVIDEND.warrent_no and OMNIBUS_CONFIG_DIVIDEND.rec_id=OMNIBUS_BO_DIVIDEND.OMNIBUS_DIV_ID AND OMNIBUS_BO_DIVIDEND.ISSUE_DATE = TO_DATE('" + DateDec + "','DD/MM/YYYY') Union SELECT BODIVIDEND.BO_ID,BODIVIDEND.NAME_FIRSTHOLDER,BODIVIDEND.WARRANT_NO,BODIVIDEND.DIVIDEND,BODIVIDEND.total_Share,OMNIBUS_CONFIG_DIVIDEND.group_warrant_no FROM BODIVIDEND_EXCEL_VIEW BODIVIDEND,OMNIBUS_BO_DIVIDEND_T,OMNIBUS_CONFIG_DIVIDEND where OMNIBUS_BO_DIVIDEND_T.OMNIBUS_AC_ID="+SNomineeNo+" and bodividend.warrant_no=OMNIBUS_BO_DIVIDEND_T.warrant_no and OMNIBUS_CONFIG_DIVIDEND.rec_id=OMNIBUS_BO_DIVIDEND_T.OMNIBUS_DIV_ID AND OMNIBUS_BO_DIVIDEND_T.ISSUE_DATE = TO_DATE('" + DateDec + "','DD/MM/YYYY')";
                  System.out.println(query1nb);
                  cm2.queryExecute(query1nb);

                  while(cm2.toNext())
                  {

                   gwarrantno = cm2.getColumnS("WARRANT_NO");
                   ginddividend = cm2.getColumnF("DIVIDEND");
                   gtotalshares = cm2.getColumnI("TOTAL_SHARE");

                   if (String.valueOf(gwarrantno).equals("null"))
                    gwarrantno = "";

                   ginddividend = bcal.roundtovalue(ginddividend,2);
                  }

                 %>
                   <option value="<%=request.getContextPath()%>/jsp/Omnibus/group/ViewOmnibusbyNominee.jsp?nomineeno=<%=thenomineeno%>&StartValue=0&EndValue=0&dateselect=<%=DateDec%>" selected="selected"><%=thenomineename%></option>
                 <%
                 }
                 else
                 {
                 %>
                   <option value="<%=request.getContextPath()%>/jsp/Omnibus/group/ViewOmnibusbyNominee.jsp?nomineeno=<%=thenomineeno%>&StartValue=0&EndValue=0&dateselect=<%=DateDec%>"><%=thenomineename%></option>
                 <%
                 }
              }
            %>
         </select>
        </td>
      </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="10%" class="style9"><div align="center">BO No.</div></td>
    <td width="32%" class="style9"><div align="center">Name</div></td>
    <td width="12%" class="style9"><div align="center">Total Shares</div></td>
    <td width="16%" class="style9"><div align="center">Dividend</div></td>
    <td width="15%" class="style9"><div align="center">Warrant No.</div></td>
    <td width="15%" class="style9"><div align="center">Group Warrant No.</div></td>
  </tr>

  <div align="left">
     <%
    if (!SNomineeNo.equalsIgnoreCase("0"))
    {
   /* while(cm.toNext())
     {
       rowcounter1++;
       foliono = cm.getColumnS("FOLIO_NO");
       folioname = cm.getColumnS("NAME");

       if (String.valueOf(foliono).equals("null"))
         foliono = "";
       if (String.valueOf(folioname).equals("null"))
         folioname = "";
*/
       //String query1na = "SELECT * FROM DIVIDEND_VIEW WHERE FOLIO_NO = '" + foliono + "' AND ISSUE_DATE = TO_DATE('" + DateDec + "','DD/MM/YYYY') ORDER BY SUBSTR(FOLIO_NO,3,6)";
       //String query1na = "select d.WARRANT_NO,d.DIVIDEND,d.folio_no,d.total_Share,S_H.* from dividend d,(SELECT * FROM SHAREHOLDER_VIEW WHERE NOMINEE_NO = '"+SNomineeNo+"') S_H where d.folio_no=s_H.folio_no and d.ISSUE_DATE = TO_DATE('" + DateDec + "','DD/MM/YYYY') ORDER BY SUBSTR(d.FOLIO_NO,3,6)";
      // String query1na = "SELECT BODIVIDEND.BO_ID,BODIVIDEND.NAME_FIRSTHOLDER,BODIVIDEND.WARRANT_NO,BODIVIDEND.DIVIDEND,BODIVIDEND.total_Share,OMNIBUS_CONFIG_DIVIDEND.group_warrant_no FROM BODIVIDEND_EXCEL_VIEW BODIVIDEND,OMNIBUS_BO_DIVIDEND,OMNIBUS_CONFIG_DIVIDEND where OMNIBUS_BO_DIVIDEND.OMNIBUS_AC_ID="+SNomineeNo+" and bodividend.warrant_no=OMNIBUS_BO_DIVIDEND.warrent_no and OMNIBUS_CONFIG_DIVIDEND.rec_id=OMNIBUS_BO_DIVIDEND.OMNIBUS_DIV_ID AND OMNIBUS_BO_DIVIDEND.ISSUE_DATE = TO_DATE('" + DateDec + "','DD/MM/YYYY')";
     String query1na = "SELECT BODIVIDEND.BO_ID,BODIVIDEND.NAME_FIRSTHOLDER,BODIVIDEND.WARRANT_NO,BODIVIDEND.DIVIDEND,BODIVIDEND.total_Share,OMNIBUS_CONFIG_DIVIDEND.group_warrant_no FROM BODIVIDEND_EXCEL_VIEW BODIVIDEND,OMNIBUS_BO_DIVIDEND,OMNIBUS_CONFIG_DIVIDEND where OMNIBUS_BO_DIVIDEND.OMNIBUS_AC_ID="+SNomineeNo+" and bodividend.warrant_no=OMNIBUS_BO_DIVIDEND.warrent_no and OMNIBUS_CONFIG_DIVIDEND.rec_id=OMNIBUS_BO_DIVIDEND.OMNIBUS_DIV_ID AND OMNIBUS_BO_DIVIDEND.ISSUE_DATE = TO_DATE('" + DateDec + "','DD/MM/YYYY') Union SELECT BODIVIDEND.BO_ID,BODIVIDEND.NAME_FIRSTHOLDER,BODIVIDEND.WARRANT_NO,BODIVIDEND.DIVIDEND,BODIVIDEND.total_Share,OMNIBUS_CONFIG_DIVIDEND.group_warrant_no FROM BODIVIDEND_EXCEL_VIEW BODIVIDEND,OMNIBUS_BO_DIVIDEND_T,OMNIBUS_CONFIG_DIVIDEND where OMNIBUS_BO_DIVIDEND_T.OMNIBUS_AC_ID="+SNomineeNo+" and bodividend.warrant_no=OMNIBUS_BO_DIVIDEND_T.warrant_no and OMNIBUS_CONFIG_DIVIDEND.rec_id=OMNIBUS_BO_DIVIDEND_T.OMNIBUS_DIV_ID AND OMNIBUS_BO_DIVIDEND_T.ISSUE_DATE = TO_DATE('" + DateDec + "','DD/MM/YYYY')";


       ///System.out.println("Second Query-------------------------------"+query1na);
       cm2.queryExecute(query1na);

       while(cm2.toNext())
       {

         rowcounter1++;
       foliono = cm2.getColumnS("BO_ID");
       folioname = cm2.getColumnS("NAME_FIRSTHOLDER");

       if (String.valueOf(foliono).equals("null"))
         foliono = "";
       if (String.valueOf(folioname).equals("null"))
         folioname = "";



         totalshares = cm2.getColumnI("TOTAL_SHARE");
         inddividend = cm2.getColumnF("DIVIDEND");
         iwarrantno = cm2.getColumnS("WARRANT_NO");

         inddividend = bcal.roundtovalue(inddividend,2);

         gwarrantno = cm2.getColumnS("group_warrant_no");
         grandTotalShare+=totalshares;
         grandTotalDividend+=inddividend;

         if (String.valueOf(gwarrantno).equals("null"))
         gwarrantno= "";

      // }
     %>
    </div>
  <tr bgcolor="#E8F3FD">
            <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=foliono%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="left">&nbsp;<%=folioname%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=totalshares%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=inddividend%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=iwarrantno%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=gwarrantno%>&nbsp;</div>
           </div></td>
         </tr>
         <div align="left" class="style13">
             <%
         }
    }
  if (isConnect)
   {
     cm.takeDown();
     cm1.takeDown();
     cm2.takeDown();
   }
  %>
         </div>
    <%
     if (gtotalshares > 0)
     {
     %>
     <table width="100%"  border="0" cellpadding="0">
       <tr>
         <td width="50%" align="left" class="style20"><br>Total Shares: <%=grandTotalShare%></td>
         <td width="50%" align="right" class="style20"><br>Total Dividend: <%=grandTotalDividend%></td>
       </tr>
     </table>
     <%
     }
    %>
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>

</body>
</html>
