<script>
/*
'******************************************************************************************************************************************
'Script Author : Jahirul Islam
'Creation Date : 14th February' 2013
'Page Purpose  : Save new omnibus.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Omnibus</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String OmnibusName = String.valueOf(request.getParameter("uname"));
  String OmnibusAddress = String.valueOf(request.getParameter("uaddress"));
  String OmnibusContact = String.valueOf(request.getParameter("ucontact"));
  String OmnibusAc = String.valueOf(request.getParameter("uAc"));
  String OmnibusStatus = String.valueOf(request.getParameter("ustatus"));
	String OmnibusAcName        = String.valueOf(request.getParameter("uAc_Name"));
	String OmnibusAcBank        = String.valueOf(request.getParameter("uAc_Bank"));	
	String OmnibusAcBankBranch        = String.valueOf(request.getParameter("uAc_Bank_Branch"));	

     
  //System.out.println("---------------OmnibusAddress-------------"+OmnibusAddress);
  //System.out.println("----------------OmnibusContact------------"+OmnibusContact);
  //System.out.println("--------------OmnibusStatus--------------"+OmnibusStatus);

	OmnibusName 	   	     =cm.replace(OmnibusName,"'","''");
	OmnibusAddress 		 	 =cm.replace(OmnibusAddress,"'","''");
	OmnibusContact 			 =cm.replace(OmnibusContact,"'","''");
	OmnibusStatus 			 =cm.replace(OmnibusStatus,"'","''");
	OmnibusAc 				 =cm.replace(OmnibusAc ,"'","''");
	OmnibusAcName             =cm.replace(marginAcName ,"'","''");
	OmnibusAcBank             =cm.replace(marginAcBank ,"'","''");
	OmnibusAcBankBranch       =cm.replace(marginAcBankBranch ,"'","''");

System.out.println("-------------OmnibusAc ---------------"+OmnibusAc );
  String SQL = "SELECT * FROM OMNIBUS_CONFIG WHERE UPPER(NAME)=UPPER('"+OmnibusName+"')";

  int Count = cm.queryExecuteCount(SQL);

  if (Count > 0)
  {
    %>
       <script language="javascript">
         alert("Omnibus name already exists!!!");
         history.go(-1);
       </script>
    <%
  }
  else{
    String nuser = "call ADD_OMNIBUS('" + OmnibusName + "', '" + OmnibusAddress + "','" + OmnibusContact + "','" + OmnibusStatus+ "','" + OmnibusAc+ "','"+ OmnibusAcName + "','"+ OmnibusAcBank+"','"+OmnibusAcBankBranch)";
    //String nuser = "INSERT INTO OMNIBUS_CONFIG(REC_ID, NAME, ADDRESS, CONTACT, STATUS) VALUES ((SELECT MAX(REC_ID)+1 FROM OMNIBUS_CONFIG), '" + OmnibusName + "', '" + OmnibusAddress + "', '" + OmnibusContact + "', '" + OmnibusAc+ "')";
    //System.out.println("--------------nuser--------------"+nuser);
    boolean b = cm.procedureExecute(nuser);
    //System.out.println("------save--------b--------------"+b);
    String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added a Omnibus')";
    boolean ub = cm.procedureExecute(ulog);
  }


   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Omnibus/AllOmnibus.jsp";
</script>
</body>
</html>
