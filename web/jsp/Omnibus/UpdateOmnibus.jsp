<script>
/*
'******************************************************************************************************************************************
'Script Author : Jahirul Islam
'Creation Date : 14th February' 2013
'Page Purpose  : Saves update of Omnibus information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update User</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String uOmnibusName = String.valueOf(request.getParameter("uname"));
  String uOmnibusAddress = String.valueOf(request.getParameter("uaddress"));
  String uOmnibusContact = String.valueOf(request.getParameter("ucontact"));
  String uOmnibusAc = String.valueOf(request.getParameter("uAc"));
  String uOmnibusStatus = String.valueOf(request.getParameter("ustatus"));
  String uOmnibusID = String.valueOf(request.getParameter("fomnibusid"));
  
  	String uOmnibusRoutingNo        = String.valueOf(request.getParameter("uRouting_No"));
    String uOmnibusAcBank        = String.valueOf(request.getParameter("uAc_Bank"));	
    String uOmnibusAcBankBranch        = String.valueOf(request.getParameter("uAc_Bank_Branch"));	

  //System.out.println("-------u------OmnibusName---------------"+uOmnibusName);
  //System.out.println("------u---------OmnibusAddress-------------"+uOmnibusAddress);
  //System.out.println("------u----------OmnibusContact------------"+uOmnibusContact);
  //System.out.println("------u--------OmnibusStatus--------------"+uOmnibusStatus);
  //System.out.println("-------u-------OmnibusID--------------"+uOmnibusID);

  int uiOmnibusID=Integer.parseInt(uOmnibusID);
  uOmnibusName=cm.replace(uOmnibusName,"'","''");
  uOmnibusAddress=cm.replace(uOmnibusAddress,"'","''");
  uOmnibusContact=cm.replace(uOmnibusContact,"'","''");
  uOmnibusStatus=cm.replace(uOmnibusStatus,"'","''");
  	uOmnibusRoutingNo             =cm.replace(uOmnibusRoutingNo ,"'","''");
	uOmnibusAcBank             =cm.replace(uOmnibusAcBank ,"'","''");
	uOmnibusAcBankBranch       =cm.replace(uOmnibusAcBankBranch ,"'","''");


  String SQL = "SELECT * FROM OMNIBUS_CONFIG WHERE UPPER(NAME)=UPPER('"+uOmnibusName+"')  AND REC_ID<>"+uOmnibusID;

  int Count = cm.queryExecuteCount(SQL);

  if (Count > 0)
  {
    %>
       <script language="javascript">
         alert("Omnibus name already exists!!!");
         history.go(-1);
       </script>
    <%
  }
  else{


    String uomnibus = "call UPDATE_OMNIBUS(" + uiOmnibusID + ",'" + uOmnibusName + "', '" + uOmnibusAddress + "','" + uOmnibusContact + "','" + uOmnibusStatus + "','" + uOmnibusAc + "','"+ uOmnibusRoutingNo + "','"+ uOmnibusAcBank + "','"+uOmnibusAcBankBranch+"')";
    boolean b = cm.procedureExecute(uomnibus);
    //System.out.println("------a--------b--------------"+b+" ------ "+uomnibus);
    String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Updated Omnibus')";
    boolean ub = cm.procedureExecute(ulog);
    //System.out.println("-------b-------b--------------"+b);
    //System.out.println("-------ub-------ub--------------"+ub);

  }


 if (isConnect)
  {
    cm.takeDown();
  }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Omnibus/AllOmnibus.jsp";
</script>
</body>
</html>
