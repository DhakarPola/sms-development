<script>
/*
'******************************************************************************************************************************************
'Script Author : Jahirul Islam
'Creation Date : 14th February' 2013
'Page Purpose  : Adds a new Omnibus.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Add Omnibus</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function formconfirm()
{
  if (confirm("Do you want to enter the mentioned Omnibus Information?"))
  {
    return true;
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('uname','- Name (Option must be entered)');
  /*
  BlankValidate('uloginid','- Login ID (Option must be entered)');
  SelectValidate('urole','- User Role (Option must be entered)');
  BlankValidate('upass','- Password (Option must be entered)');
  BlankValidate('ucpass','- Confirm Password (Option must be entered)');

  if (document.all.upass.value != document.all.ucpass.value)
  {
    count++;
    nArray[count] = '- Passwords do no match!';
  }
  */
  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
%>

<form action="SaveOmnibus.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Add Omnibus</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Name</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="uname" class="SL2TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Address </div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="uaddress" class="SL2TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Contact </div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="ucontact" class="SL2TextField">
        </div></td>
      </tr>

     <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;A/C No </div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="uAc" class="SL2TextField">
        </div></td>
      </tr>
	   <tr>
		  <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Routing No </div></th>
		  <td>:</td>
		  <td><div align="left">
			<input type="text" name="uRouting_No" class="SL2TextField">
		  </div></td>
		</tr>
	   <tr>
		  <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank </div></th>
		  <td>:</td>
		  <td><div align="left">
			<input type="text" name="uAc_Bank" class="SL2TextField">
		  </div></td>
		</tr>
	   <tr>
		  <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Branch </div></th>
		  <td>:</td>
		  <td><div align="left">
			<input type="text" name="uAc_Bank_Branch" class="SL2TextField">
		  </div></td>
		</tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Status </div></th>
        <td>:</td>
        <td><div align="left">
          <input type="radio" id="uactive" name="ustatus" value="ACTIVE" checked="checked" ><label for="uactive">&nbsp;&nbsp;ACTIVE&nbsp;&nbsp;</label>
          <input type="radio" id="uinactive" name="ustatus" value="INACTIVE" ><label for="uinactive">&nbsp;&nbsp;INACTIVE&nbsp;&nbsp;</label>
        </div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="34%" scope="row">
            <div align="right">
 		    <img name="B3" src="<%=request.getContextPath()%>/images/btnBack.gif"  onclick="history.go(-1)" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnBackOn.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnBack.gif'">
            </div></th>
        <td width="33%" align="center">
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
      </td>
        <td width="33%">
	          <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
      </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
