<script>
/*
'******************************************************************************************************************************************
'Script Author : Jahirul Islam
'Creation Date : 14th February' 2013
'Page Purpose  : Deletes a omnibus.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Delete Omnibus</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String OmnibusID = String.valueOf(request.getParameter("domnibus"));
  //System.out.println("--------------OmnibusID--------------"+OmnibusID);
  //String domnibus1 = "call DELETE_OMNIBUS('" + OmnibusID + "')";
  String domnibus1 = "UPDATE OMNIBUS_CONFIG SET STATUS = UPPER('INACTIVE') WHERE REC_ID="+OmnibusID;
  //System.out.println("--------------domnibus1--------------"+domnibus1);

  //boolean b = cm.procedureExecute(domnibus1);
  cm.queryExecute(domnibus1);

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Inactive Omnibus')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Omnibus/AllOmnibus.jsp";
</script>
</body>
</html>
