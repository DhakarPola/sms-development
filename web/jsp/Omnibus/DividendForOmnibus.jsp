<script>
/*
'******************************************************************************************************************************************
'Script Author : Jahirul Islam
'Creation Date : 15th February' 2013
'Page Purpose  : Selects Criteria for Omnibus Dividend.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Dividend for Omnibus</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
//Execute while click on Submit
function SubmitThis() {
  count = 0;
  SelectValidate('dateselect1','- Declaration Date (Option must be entered)');
  if (count == 0)
  {
    document.forms[0].submit();
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}

function toback()
{
  /*history.go(-1)*/
  location = "<%=request.getContextPath()%>/jsp/Omnibus/SDividendList.jsp"
}

function showMessage()
{
alert('please select Date of Declaration!')
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
%>

<form action="<%=request.getContextPath()%>/jsp/Omnibus/DividendForOmnibus2.jsp" method="post" name="FileForm">
  <table width="100%" BORDER=0  cellpadding="1" style="border-collapse: collapse" bordercolor="#EAC06B">
  <td width="100%" scope="row"><div align="left">
 	<img name="B3" src="<%=request.getContextPath()%>/images/btnBack.gif"  onclick="toback()" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnBackOn.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnBack.gif'">
  </div></td>
  </table>

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Dividend for Omnibus</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date of Declaration</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="dateselect1" class="SL1TextFieldListBox"">
            <option>--- Please Select ---</option>
            <%
              String thedate = "";

              String query1 = "SELECT DISTINCT ISSUE_DATE FROM DIVIDEND_VIEW ORDER BY ISSUE_DATE DESC";

              cm.queryExecute(query1);

              while(cm.toNext())
              {
                 thedate = cm.getColumnDT("ISSUE_DATE");
                 %>
                   <option value="<%=thedate%>"><%=thedate%></option>
                 <%
              }
            %>
         </select>
        </div></td>
      </tr>
  </table>
  <br>
</div>
<table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="50%" scope="row">
            <div align="right">
	      <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
            </div>
        </th>
        <td width="50%" align="left">
 	      <img name="B1" src="<%=request.getContextPath()%>/images/btnOK.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOKOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOK.gif'">
        </td>
      </tr>
</table>



        <!--HIDDEN FIELDS-->
        <input type="hidden" name="oStartValue" value="1">
        <input type="hidden" name="oEndValue" value="0">



</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
