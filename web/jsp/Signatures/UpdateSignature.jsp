<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Updated by    : Fahmi Sarker
'Creation Date : October 2005
'Page Purpose  : Saves update of Signature information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@ page language="java" import="oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*,org.apache.commons.fileupload.*,java.io.*,java.util.*,java.util.List,java.util.Vector,java.util.Date,java.text.SimpleDateFormat" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update Signature</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String SigName = String.valueOf(request.getParameter("sname"));
  String SigRank = String.valueOf(request.getParameter("srank"));
  String signature = String.valueOf(request.getParameter("signature"));
  String SigID = String.valueOf(request.getParameter("fsigid"));
  String signaturestatus = String.valueOf(request.getParameter("signaturestatus"));
   if(signaturestatus==null) signaturestatus="";
  SigName=cm.replace(SigName,"'","''");
  SigRank=cm.replace(SigRank,"'","''");
  signature=cm.replace(signature,"'","''");
  SigID=cm.replace(SigID,"'","''");
  signaturestatus=cm.replace(signaturestatus,"'","''");



  String uuser="";


  System.out.println(uuser);
  System.out.println("signaturestatus : "+signaturestatus);

  if(signaturestatus.equals("add") || signaturestatus.equals("delete"))
     uuser = "call UPDATE_SIGNATURE('" + SigID + "','" + SigName + "', '" + SigRank + "',null)";
  else
     uuser = "call UPDATE_SIGNATURE_EMPTY('" + SigID + "','" + SigName + "', '" + SigRank + "')";

  boolean b = cm.procedureExecute(uuser);



if(signaturestatus.equals("delete") || signaturestatus.equals("edit"))
 {
   ;
 }
 else
 if (signature!=null && !signature.equals(""))//This section inserts signature into database
  {
    //Insert Blob into database
    BLOB blob;
    PreparedStatement pstmt;
    cm.connect();
    Connection connection = cm.getConnection() ;

    String selectQuery="";

    selectQuery="SELECT * FROM SIGNATURE where SIG_ID = ? for update";

    pstmt = connection.prepareStatement(selectQuery);

    pstmt.setString(1, SigID);
    connection.setAutoCommit(false);

    ResultSet rset=pstmt.executeQuery();
    rset.next();


        //Use the OracleDriver resultset, we take the blob locator
        blob = ((OracleResultSet) rset).getBLOB("SIGN");
        OutputStream os = blob.getBinaryOutputStream();

        //Read the file by chuncks and insert them in the Blob. The chunk size come from the blob
        byte[] chunk = new byte[blob.getChunkSize()];
        int i = -1;

        System.out.println("SIGNATURE : "+signature);
        File fullFile = new File(signature);

        FileInputStream is = new FileInputStream(fullFile );
        while ((i = is.read(chunk)) != -1) {
            os.write(chunk, 0, i); //Write the chunk
            System.out.print('.'); // print progression
        }

        is.close();
        os.close();
        //Close the statement and commit
        pstmt.close();
        connection.commit();
        connection.setAutoCommit(true);
        cm.takeDown();
  }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Edited Signature Information')";
  boolean ub = cm.procedureExecute(ulog);

 if (isConnect)
  {
    cm.takeDown();
  }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Signatures/AllSignatures.jsp";
</script>
</body>
</html>
