<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : October 2005
'Page Purpose  : Save new Signature.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@ page language="java" import="oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*,org.apache.commons.fileupload.*,java.io.*,java.util.*,java.util.List,java.util.Vector,java.util.Date,java.text.SimpleDateFormat,oracle.jdbc.driver.*" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Signature</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

  String SigName = String.valueOf(request.getParameter("sname"));
  String SigRank = String.valueOf(request.getParameter("srank"));
  String signature = String.valueOf(request.getParameter("signature"));
  String sig_id = "";



  String nsig = "call ADD_SIGNATURE('" + SigName + "', '" + SigRank + "', null)";
  String findLastRecordQuery = "select SIG_ID from signature where NAME='" +SigName
                              + "' AND RANK='"+SigRank+"'";


  try
  {


  boolean bResult = cm.procedureExecute(nsig);
  System.out.println(nsig);
  cm.queryExecuteGetResultSet(findLastRecordQuery);
  System.out.println(findLastRecordQuery);

  while(cm.toNext())
  {
    sig_id= cm.getColumnS("SIG_ID");
  }



//This section inserts signature into database
 if (signature!=null && !signature.equals(""))
  {
    //Insert Blob into database
    BLOB blob;
    PreparedStatement pstmt;

    cm.connect();
    Connection connection = cm.getConnection() ;

    String selectQuery="SELECT * FROM SIGNATURE where sig_id = '"+ sig_id+"' for update";
    pstmt = connection.prepareStatement(selectQuery);

    //pstmt.setString(1, sig_id);
    connection.setAutoCommit(false);


    ResultSet rset=pstmt.executeQuery();
    rset.next();

        //Use the OracleDriver resultset, we take the blob locator
        blob = ((OracleResultSet) rset).getBLOB("SIGN");
        OutputStream os = blob.getBinaryOutputStream();
        //Read the file by chuncks and insert them in the Blob. The chunk size come from the blob
        byte[] chunk = new byte[blob.getChunkSize()];
        int i = -1;

        System.out.println("SIGNATURE : "+signature);
        File fullFile = new File(signature);

        FileInputStream is = new FileInputStream(fullFile );
        while ((i = is.read(chunk)) != -1) {
            os.write(chunk, 0, i); //Write the chunk
            System.out.print('.'); // print progression
        }

        is.close();
        os.close();
        //Close the statement and commit
        pstmt.close();
        connection.commit();
        connection.setAutoCommit(true);

  }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added Signature Information')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
  }
  catch(Exception ex)
  {
    System.out.println("Error Message : "+ex.getMessage());
      StackTraceElement[] st  = ex.getStackTrace();
      for(int i=0;i<st.length;i++)
         System.out.println(st[i]);
  }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Signatures/AllSignatures.jsp";
</script>
</body>
</html>
