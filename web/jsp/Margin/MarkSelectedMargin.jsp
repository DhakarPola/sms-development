<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Zahidul Islam
'Creation Date : 12th January' 2016
'Page Purpose  : Marks the selected BO Numbers for Margin.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null")){ %>
    <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<% } %>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Mark Selected BO for Margin</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF"><%
    boolean isConnect       = cm.connect();
    if(isConnect==false){ %>
        <jsp:forward page="ErrorMsg.jsp" >
          <jsp:param name="ErrorTitle" value="Connection Failure" />
          <jsp:param name="ErrorHeading" value="Connection Problem" />
          <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
        </jsp:forward> <%
    }


    String rowcounter                   = String.valueOf(request.getParameter("rowcounter1"));
    String DivDate                      = String.valueOf(request.getParameter("ddate"));
    String Warrantno                    = String.valueOf(request.getParameter("searchWarrant"));
    String Chunk                        = String.valueOf(request.getParameter("pchunk"));
    String Destination                  = String.valueOf(request.getParameter("pstatus"));
    String ForW                         = String.valueOf(request.getParameter("forw"));
    String oid                          = String.valueOf(request.getParameter("oid"));
    int ioid                            = Integer.parseInt(oid);

  //System.out.println("---------------MarkSelectedMargin.jsp----rowcounter----"+rowcounter);
  //System.out.println("---------------MarkSelectedMargin.jsp----DivDate----"+DivDate);
  //System.out.println("---------------MarkSelectedMargin.jsp----Warrantno----"+Warrantno);
  //System.out.println("---------------MarkSelectedMargin.jsp----Chunk----"+Chunk);
  //System.out.println("------1---------MarkSelectedMargin.jsp----Destination----"+Destination);
  //System.out.println("---------------MarkSelectedMargin.jsp----ForW----"+ForW);
  //System.out.println("---------------MarkSelectedMargin.jsp----oid----"+oid);

    int iChunk                          = Integer.parseInt(Chunk);
    Warrantno                           =cm.replace(Warrantno,"'","''");

    if (String.valueOf(Warrantno).equals("null")){
        Warrantno                       = "";
    }


    if (!Warrantno.equalsIgnoreCase("")){
        String ifwarexists                = "";
        String countfolios                = "";


        if (ForW.equalsIgnoreCase("obo")){
         //ifwarexists = "SELECT * FROM BODIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' AND BO_ID = '"+ Warrantno+"'";
            ifwarexists                 ="SELECT * FROM BODIVIDEND WHERE WARRANT_NO NOT IN (((SELECT WARRANT_NO FROM MARGIN_BO_DIVIDEND_T WHERE ISSUE_DATE = TO_DATE('"+DivDate+"','DD/MM/YYYY') AND STATUS=UPPER('S')) UNION (SELECT WARRENT_NO FROM MARGIN_BO_DIVIDEND WHERE ISSUE_DATE = TO_DATE('"+DivDate+"','DD/MM/YYYY')))) AND ISSUE_DATE = TO_DATE('"+DivDate+"','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' AND BO_ID = '"+ Warrantno+"' ORDER BY BO_ID";
         //System.out.println("---m---obo------ifwarexists-----"+ifwarexists);
        }else if (ForW.equalsIgnoreCase("warrant")){
         //ifwarexists = "SELECT * FROM BODIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' AND WARRANT_NO = '"+ Warrantno+"'";
            ifwarexists                 ="SELECT * FROM BODIVIDEND WHERE WARRANT_NO NOT IN (((SELECT WARRANT_NO FROM MARGIN_BO_DIVIDEND_T WHERE ISSUE_DATE = TO_DATE('"+DivDate+"','DD/MM/YYYY') AND STATUS=UPPER('S')) UNION (SELECT WARRENT_NO FROM MARGIN_BO_DIVIDEND WHERE ISSUE_DATE = TO_DATE('"+DivDate+"','DD/MM/YYYY')))) AND ISSUE_DATE = TO_DATE('"+DivDate+"','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' AND WARRANT_NO = '"+ Warrantno+"' ORDER BY BO_ID";
         //System.out.println("----m--warrant------ifwarexists-----"+ifwarexists);
        }

        //countfolios = "SELECT * FROM BODIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F'";
        countfolios                     ="SELECT * FROM BODIVIDEND WHERE WARRANT_NO NOT IN (((SELECT WARRANT_NO FROM MARGIN_BO_DIVIDEND_T WHERE ISSUE_DATE = TO_DATE('"+DivDate+"','DD/MM/YYYY') AND STATUS=UPPER('S')) UNION (SELECT WARRENT_NO FROM MARGIN_BO_DIVIDEND WHERE ISSUE_DATE = TO_DATE('"+DivDate+"','DD/MM/YYYY')))) AND ISSUE_DATE = TO_DATE('"+DivDate+"','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' ORDER BY BO_ID";
        //System.out.println("---m---countfolios-----"+countfolios);


        int cWAR                        = cm.queryExecuteCount(ifwarexists);

        if (cWAR > 0){
            Destination                 = request.getContextPath() + "/jsp/Margin/DividendforMarginCalc.jsp?dateselect=";
            Destination                 = Destination + DivDate + "&oStartValue=";
            int tcount                  = cm.queryExecuteCount(countfolios);

            String getWarrant           = "";


            if (ForW.equalsIgnoreCase("obo")) {
                getWarrant              = "SELECT rnum FROM (SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM BODIVIDEND WHERE WARRANT_NO NOT IN (((SELECT WARRANT_NO FROM MARGIN_BO_DIVIDEND_T WHERE ISSUE_DATE = TO_DATE('"+DivDate+"','DD/MM/YYYY') AND STATUS=UPPER('S')) UNION (SELECT WARRENT_NO FROM MARGIN_BO_DIVIDEND WHERE ISSUE_DATE = TO_DATE('"+DivDate+"','DD/MM/YYYY')))) AND ISSUE_DATE = TO_DATE('"+DivDate+"','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' ORDER BY BO_ID) div where rownum <= "+tcount+") where rnum >= 1) WHERE BO_ID = '"+Warrantno+"'";
                //System.out.println("----m--obo-----------"+getWarrant);
            }else if (ForW.equalsIgnoreCase("warrant")){
                getWarrant              = "SELECT rnum FROM (SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM BODIVIDEND WHERE WARRANT_NO NOT IN (((SELECT WARRANT_NO FROM MARGIN_BO_DIVIDEND_T WHERE ISSUE_DATE = TO_DATE('"+DivDate+"','DD/MM/YYYY') AND STATUS=UPPER('S')) UNION (SELECT WARRENT_NO FROM MARGIN_BO_DIVIDEND WHERE ISSUE_DATE = TO_DATE('"+DivDate+"','DD/MM/YYYY')))) AND ISSUE_DATE = TO_DATE('"+DivDate+"','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' ORDER BY BO_ID) div where rownum <= "+tcount+") where rnum >= 1) WHERE WARRANT_NO = '"+Warrantno+"'";
             //System.out.println("---m---warrant-----------"+getWarrant);
            }

            cm.queryExecute(getWarrant);
            cm.toNext();
            String srr                  = cm.getColumnS("rnum");
            int rr                      = Integer.parseInt(srr);
            int err                     = rr + iChunk;
            Destination                 = Destination + rr + "&oEndValue=" + err ;
        }
        Destination                     = Destination + "&marginName=" + ioid;
        //System.out.println("---m---Destination-----------"+Destination);
    }


    String validating1                  ="SELECT REC_ID FROM MARGIN_CONFIG_DIVIDEND WHERE MARGIN_ID="+ioid+" AND DIVIDEND_DATE=TO_DATE('"+DivDate+"','DD/MM/YYYY')";
    //System.out.println("-----------M--------------------validating1="+validating1);
    int validity1                       =cm.queryExecuteCount(validating1);
    if(validity1<1){
      String addMarginConfigDividend    ="INSERT INTO MARGIN_CONFIG_DIVIDEND (REC_ID,MARGIN_ID,DIVIDEND_DATE,STATUS) VALUES ((SELECT NVL(MAX(rec_id),0)+1 FROM MARGIN_CONFIG_DIVIDEND),"+ioid+",TO_DATE('"+DivDate+"', 'DD/MM/YYYY'),'TEMP')";
      //System.out.println("-----------M--------------------aocd=");
      cm.queryExecute(addMarginConfigDividend);
      //System.out.println("-----------M--------------------addMarginConfigDividend="+addMarginConfigDividend);
    }

    String Fldname                      = "";
    int irowcounter                     = Integer.parseInt(rowcounter);

    for (int i=1;i<irowcounter+1;i++){
        Fldname                         = "selectingbox" + String.valueOf(i);
        String BoxValue                 = String.valueOf(request.getParameter(Fldname));

        BoxValue                        =cm.replace(BoxValue,"'","''");

        if (String.valueOf(BoxValue).equals("null")){
            BoxValue                    = "";
        }
        if (!BoxValue.equalsIgnoreCase("")){
          String marksel                = "";
          String marksel2               = "";
          //System.out.println("-----------M--------------------BoxValue="+BoxValue);
          //System.out.println("-----------M--------------------DivDate="+DivDate);
          //System.out.println("-----------M--------------------ioid="+ioid);
          //marksel = "call MARK_SELECTED_BODIVIDEND('" + BoxValue + "','" + ColDate + "')";
          //marksel="INSERT INTO MARGIN_CONFIG_DIVIDEND (REC_ID,MARGIN_ID,DIVIDEND_DATE,STATUS) VALUES ((SELECT NVL(MAX(rec_id),0)+1 FROM MARGIN_CONFIG_DIVIDEND),"+ioid+",TO_DATE('"+DivDate+"', 'DD/MM/YYYY'),'TEMP')";
          marksel                       ="UPDATE BODIVIDEND SET MARGIN_SELECT = 'T' WHERE WARRANT_NO = '"+BoxValue+"' AND ISSUE_DATE = TO_DATE('"+DivDate+"','DD/MM/YYYY')";

          //System.out.println("-----------M--------------------marksel="+marksel);
          //boolean b = cm.procedureExecute(marksel);
          cm.queryExecute(marksel);
          //System.out.println("-----------M--------------------b=");


          String validating2            ="SELECT REC_ID FROM MARGIN_BO_DIVIDEND_T WHERE MARGIN_DIV_ID=(SELECT REC_ID FROM MARGIN_CONFIG_DIVIDEND WHERE MARGIN_ID="+ioid+" AND DIVIDEND_DATE=TO_DATE('"+DivDate+"','DD/MM/YYYY')) AND MARGIN_AC_ID="+ioid+" AND WARRANT_NO='"+BoxValue+"' AND ISSUE_DATE=TO_DATE('"+DivDate+"','DD/MM/YYYY')";
          //System.out.println("-----------M--------------------validating2="+validating2);
          int validity2                 =cm.queryExecuteCount(validating2);
          if(validity2<1){
            marksel2                    ="INSERT INTO MARGIN_BO_DIVIDEND_T (REC_ID,MARGIN_DIV_ID,MARGIN_AC_ID,BO_ID,WARRANT_NO,DIV_DATE,ISSUE_DATE,STATUS) VALUES ((SELECT NVL(MAX(rec_id),0)+1 FROM MARGIN_BO_DIVIDEND_T),(SELECT REC_ID FROM MARGIN_CONFIG_DIVIDEND WHERE MARGIN_ID="+ioid+" AND DIVIDEND_DATE=TO_DATE('"+DivDate+"','DD/MM/YYYY')),"+ioid+",(SELECT BO_ID FROM BODIVIDEND WHERE WARRANT_NO='"+BoxValue+"' AND ISSUE_DATE=TO_DATE('"+DivDate+"','DD/MM/YYYY')),'"+BoxValue+"',SYSDATE,TO_DATE('"+DivDate+"','DD/MM/YYYY'),'T')";
            //System.out.println("-----------M--------------------marksel2="+marksel2);
            cm.queryExecute(marksel2);
            //System.out.println("-----------M--------------------b2=");
          }
        }
    }

    if (isConnect){
        cm.takeDown();
    }

    if (Destination.equalsIgnoreCase("close")){ %>
        <script language="javascript">
            location = "<%=request.getContextPath()%>/jsp/Home.jsp";
        </script> <%
    }else{
        //System.out.println("-------2--------MarkSelectedMargin.jsp----Destination----"+Destination); %>
        <script language="javascript">
            location = "<%=Destination%>";
        </script> <%
    } %>
</body>
</html>
