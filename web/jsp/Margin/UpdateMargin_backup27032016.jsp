<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Zahidul Islam
'Creation Date : 12th January' 2016
'Page Purpose  : Saves update of Margin information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null")){ %>
    <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<% } %>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update User</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
    boolean isConnect = cm.connect();
    if(isConnect==false){  %>
    <jsp:forward page="ErrorMsg.jsp" >
        <jsp:param name="ErrorTitle" value="Connection Failure" />
        <jsp:param name="ErrorHeading" value="Connection Problem" />
        <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
    </jsp:forward>
    <% }
    
    String marginName         = String.valueOf(request.getParameter("uname"));
    String marginAddress      = String.valueOf(request.getParameter("uaddress"));
    String marginContact      = String.valueOf(request.getParameter("ucontact"));
    String marginAc           = String.valueOf(request.getParameter("uAc"));
    String marginStatus       = String.valueOf(request.getParameter("ustatus"));
    String marginID           = String.valueOf(request.getParameter("fmarginid"));
	String marginAcName        = String.valueOf(request.getParameter("uAc_Name"));
    String marginAcBank        = String.valueOf(request.getParameter("uAc_Bank"));	
    String marginAcBankBranch        = String.valueOf(request.getParameter("uAc_Bank_Branch"));	

    //System.out.println("-------u------OmnibusName---------------"+marginName);
    //System.out.println("------u---------OmnibusAddress-------------"+marginAddress);
    //System.out.println("------u----------OmnibusContact------------"+marginContact);
    //System.out.println("------u--------OmnibusStatus--------------"+marginStatus);
    //System.out.println("-------u-------OmnibusID--------------"+marginID);

    int uiMarginID          =Integer.parseInt(marginID);
    marginName              =cm.replace(marginName,"'","''");
    marginAddress           =cm.replace(marginAddress,"'","''");
    marginContact           =cm.replace(marginContact,"'","''");
    marginStatus            =cm.replace(marginStatus,"'","''");
	marginAcName             =cm.replace(marginAcName ,"'","''");
	marginAcBank             =cm.replace(marginAcBank ,"'","''");
	marginAcBankBranch       =cm.replace(marginAcBankBranch ,"'","''");


    String SQL      = "SELECT * FROM MARGIN_CONFIG WHERE UPPER(NAME)=UPPER('"+marginName+"')  AND REC_ID<>"+marginID;
    int Count       = cm.queryExecuteCount(SQL);

    if (Count > 0){ %>
        <script language="javascript">
            alert("Margin name already exists!!!");
            history.go(-1);
        </script><%  
    }else{
		
		String umargin   = "call UPDATE_MARGIN(" + uiMarginID + ",'" + marginName + "', '" + marginAddress + "','" + marginContact + "','" + marginStatus + "','" + marginAc + "','"+marginAcName+"','"+marginAcBank+"','"+marginAcBankBranch+ "')";
		
		// Line commented by Zahid 25 February 2016 
        // String umargin      = "call UPDATE_MARGIN(" + uiMarginID + ",'" + marginName + "', '" + marginAddress + "','" + marginContact + "','" + marginStatus + "','" + marginAc + "')";
        //System.out.println("-------u-------OmnibusID--------------"+umargin);
        boolean b           = cm.procedureExecute(umargin);

        String ulog         = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Updated Margin')";
        boolean ub          = cm.procedureExecute(ulog);
    }
    
    if (isConnect){
        cm.takeDown();
    }
%>
<script language="javascript">
    location = "<%=request.getContextPath()%>/jsp/Margin/AllMargin.jsp";
</script>
</body>
</html>
