<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Zahidul Islam
'Creation Date : 12th January' 2016
'Page Purpose  : Edits margin Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null")){ %>
    <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<% } %>

<html>
<head>
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

    String marginName       	= request.getParameter("uname1");
    String marginId         	= request.getParameter("uid1");
    int marginIdParsed      	= Integer.parseInt(marginId);
    String marginAddress    	= "";
    String marginContact    	= "";
    String marginAc         	= "";
    String marginStatus     	= "";
	String marginRoutingNo     	= ""; 
    String marginAcBank        	= "";
    String marginAcBankBranch  	= "";
	
    
	/* 										Commented by ZAHID for special characer problem */
	
	
	//String query1           = "SELECT * FROM MARGIN_CONFIG WHERE REC_ID = " + marginIdParsed + " AND NAME = '"+marginName+ "'";
	
	
	
	
	String query1           = "SELECT * FROM MARGIN_CONFIG WHERE REC_ID = " + marginIdParsed;
    cm.queryExecute(query1);

    while(cm.toNext()){
		marginName 			= cm.getColumnS("NAME");
        marginAddress   	= cm.getColumnS("ADDRESS");
        marginContact   	= cm.getColumnS("CONTACT");
        marginAc        	= cm.getColumnS("AC_NO");
        marginStatus    	= cm.getColumnS("STATUS");
		marginRoutingNo 	= cm.getColumnS("ROUTING_NO");
		marginAcBank    	=  cm.getColumnS("BANK_NAME");
		marginAcBankBranch 	= cm.getColumnS("BANK_BRANCH");
		
		

        if (String.valueOf(marginAddress).equals("null"))
            marginAddress  = "";
        if (String.valueOf(marginContact).equals("null"))
            marginContact  = "";
        if (String.valueOf(marginAc ).equals("null"))
            marginAc       = "";
        if (String.valueOf(marginStatus).equals("null"))
            marginStatus   = "INACTIVE";
		
		if (String.valueOf(marginRoutingNo ).equals("null"))
			marginRoutingNo       = "";
		if (String.valueOf(marginAcBank ).equals("null"))
			marginAcBank       = "";
		if (String.valueOf(marginAcBankBranch ).equals("null"))
			marginAcBankBranch       = "";		
    }
%>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit margin</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function gotoMargin(){
    location = "<%=request.getContextPath()%>/jsp/Margin/AllMargin.jsp";
}

function gotoDelete(){
    if (confirm("Do you want to delete the margin?")){
        location = "<%=request.getContextPath()%>/jsp/Margin/DeleteMargin.jsp?cusid2=<%=marginId%>";
  }
}

function formConfirm(){
    if (confirm("Do you want to change the Margin Information?")){
        return true;
    }
}

//Execute while click on Submit
function SubmitThis() {
    count = 0;
    BlankValidate('uname','- Name (Option must be entered)');
    /*
    BlankValidate('uloginid','- Login ID (Option must be entered)');
    BlankValidate('upass','- Password (Option must be entered)');
    BlankValidate('ucpass','- Confirm Password (Option must be entered)');

    if (document.all.upass.value != document.all.ucpass.value)
    {
      count++;
      nArray[count] = '- Passwords do no match!';
    }
    */

    if (count == 0){
        if (formConfirm()){
            document.forms[0].submit();
        }
    }else{
        ShowAllAlertMsg();
        return false;
    }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">

<form action="UpdateMargin.jsp" method="get" enctype="multipart/form-data" name="FileForm">

    <span class="style7">
    <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
    <!--DWLayoutTable-->
    <tr><td bgcolor="#0044B0" class="style7">margin Information</td></tr>
    <tr bgcolor="#E8F3FD" >
        <td height="100%" bgcolor="#E8F3FD"  ><center>
        <br>
        <div align="left">
            <table width="80%"  border="0" cellpadding="5">
                <tr>
                    <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Name</div></th>
                    <td width="1%">:</td>
                    <td width="55%"><div align="left">
                      <input type="text" name="uname" value="<%=marginName%>" class="SL2TextField">
                      </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Address</div></th>
                    <td>:</td>
                    <td><div align="left">
                      <input type="text" name="uaddress" value="<%=marginAddress%>" class="SL2TextField" >
                    </div></td>
                </tr>
                <tr>
                    <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Contact</div></th>
                    <td>:</td>
                    <td><div align="left">
                      <input type="text" name="ucontact" value="<%=marginContact%>" class="SL2TextField" >
                    </div></td>
                </tr>

               <tr>
                    <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;A/C No</div></th>
                    <td>:</td>
                    <td><div align="left">
                      <input type="text" name="uAc" value="<%=marginAc%>" class="SL2TextField" >
                    </div></td>
                </tr>
		<tr>
		  <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Routing No </div></th>
		  <td>:</td>
		  <td><div align="left">
			<input type="text" name="uRouting_No" value="<%=marginRoutingNo%>" class="SL2TextField">
		  </div></td>
		</tr>
               <tr>
                  <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank </div></th>
                  <td>:</td>
                  <td><div align="left">
                    <input type="text" name="uAc_Bank" value="<%=marginAcBank%>" class="SL2TextField">
                  </div></td>
                </tr>
               <tr>
                  <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Branch </div></th>
                  <td>:</td>
                  <td><div align="left">
                    <input type="text" name="uAc_Bank_Branch" value="<%=marginAcBankBranch%>" class="SL2TextField">
                  </div></td>
                </tr>	
                <tr>
                    <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Status </div></th>
                    <td>:</td>
                    <td>
                        <div align="left"><%  
                            if (marginStatus.equalsIgnoreCase("ACTIVE")){ %>
                                <input type="radio" id="uactive" name="ustatus" value="ACTIVE" checked="checked" ><label for="uactive">&nbsp;&nbsp;ACTIVE&nbsp;&nbsp;</label>
                                <input type="radio" id="uinactive" name="ustatus" value="INACTIVE" ><label for="uinactive">&nbsp;&nbsp;INACTIVE&nbsp;&nbsp;</label><% 
                            }else if (marginStatus.equalsIgnoreCase("INACTIVE")){ %>
                                <input type="radio" id="uactive" name="ustatus" value="ACTIVE" ><label for="uactive">&nbsp;&nbsp;ACTIVE&nbsp;&nbsp;</label>
                                <input type="radio" id="uinactive" name="ustatus" value="INACTIVE" checked="checked" ><label for="uinactive">&nbsp;&nbsp;INACTIVE&nbsp;&nbsp;</label><% 
                            } %>
                        </div>
                    </td>
                </tr>
            </table>

            <!--HIDDEN FIELDS-->
            <input type="hidden" name="fmarginid" value="<%=marginId%>">

            <br>
        </div>
        <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
            <tr>
                <th width="43%" scope="row">
                    <div align="right">
                        <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
                    </div>
                </th>
                <td width="57%">
                    <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="gotoMargin()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
                </td>
            </tr>
        </table>
</form>
<%  if (isConnect){
        cm.takeDown();
    }
%>
</body>
</html>
