<script>
/*
'******************************************************************************************************************************************
'Script Author : Rahat
'Creation Date : September 2010
'Page Purpose  : Offset Amount.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cms"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Offset Account </title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0)
  {
    document.forms[0].submit();
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
function gotobank()
{
  history.go(-1);
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
     double offsetamount=0.0;
     String offsetamountS="0.0";
     String entry_date="";
     String description="";
     String query="select * from OFFSETAMOUNT where ISACTIVE='T'";
     //System.out.println(query);
     try
     {
       cm.queryExecute(query);
       cm.toNext();
       offsetamount=cm.getColumnF("AMOUNT");
       offsetamount=cms.roundtovalue(offsetamount,2);
       description=cm.getColumnS("DESCRIPTION");
       if(String.valueOf(description).equals("null"))
       description="";
       offsetamountS=String.valueOf(offsetamount);
       if(offsetamountS.indexOf(".")==-1)
          offsetamountS=offsetamountS+".00";
       else if (offsetamountS.indexOf(".")==offsetamountS.length()-2) {
         offsetamountS=offsetamountS+"0";
       }
     }
     catch(Exception e){}
     //System.out.println("merged= "+merged);
     //System.out.println("merge_date= "+merge_date);

%>

<form action="<%=request.getContextPath()%>/jsp/Bank/SaveOffsetAmount.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse:collapse; border-color:#FFFFFF;" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Offset Imformation</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="93%"  BORDER=1  cellpadding="0" style="border-collapse: collapse" bordercolor="#06689E">
  	<tr>
        <td scope="row" width="18%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
        <td width="3%" align="center">&nbsp;</td>
        <td width="41%"><div align="center" class="style8"><b>New Value</b></div></td>
        <td width="38%"><div align="center" class="style8"><b>Old Value </b></div></td>
      <tr>
        <td><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Description</div></td>
        <td align="center">:</td>
        <td><div align="left">&nbsp;
          <textarea name="description" cols="20" rows="3" wrap="VIRTUAL" id="description" class="ML12TextField"></textarea>
        </div></td>
		<td><div align="left">&nbsp;&nbsp;<%=description%></div></td>
      </tr>
        <tr>
          <td scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Amount</div></td>
          <td align="center" >:</td>
          <td><div align="left">&nbsp;
         <input type="text" name="offsetamount" id="offsetamount"class="SL2TextField" onkeypress="keypressOnDoubleFld()" value="">
        </div></td>
        <td><div align="left">&nbsp;&nbsp;<%=offsetamountS%></div></td>
        </tr>

  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
      <td width="43%" scope="row">
        <div align="right">
		  <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
        </div></td>
        <td width="57%">
	          <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="gotobank()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
        </td>
      </tr>
</table>
</td>
</tr>
</table>
</form>
<%
   if (isConnect)
   {
     try
     {
       cm.takeDown();
     }catch(Exception e){}
   }
%>
</body>
</html>
