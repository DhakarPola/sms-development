<script>
/*
'******************************************************************************************************************************************
'Script Author : Md.Kamruzzaman
'Creation Date : August 2007
'Page Purpose  : Edit Merge account.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Merge Account </title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0)
  {
    var answer = confirm ("Do really want to Marge this Account");
    if (answer){
    document.forms[0].submit();
    }
    else{
       return false;
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
function gotobank()
{
  history.go(-1);
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
     String dec_date="";
     dec_date=String.valueOf(request.getParameter("decdate"));
     String merge_date="";
     String merged="";
     String query="select MERGED_DATE,ACC_MERGED from DIVIDENDDECLARATION_VIEW where DATE_DEC=TO_DATE('" + dec_date + "','DD/MM/YYYY')";
     //System.out.println(query);
     try
     {
       cm.queryExecute(query);
       cm.toNext();
       merge_date=cm.getColumnDT("MERGED_DATE");
       merged=cm.getColumnS("ACC_MERGED");
       if(String.valueOf(merge_date).equals("null"))
       merge_date="";
       if(String.valueOf(merged).equals("null"))
       merged="";
     }
     catch(Exception e){}
     //System.out.println("merged= "+merged);
     //System.out.println("merge_date= "+merge_date);

%>

<form action="<%=request.getContextPath()%>/jsp/Bank/SaveMarge.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Edit Merge Accounts</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Dividend Declaration Date</div></th>
        <th width="1%">:</th>
        <th><div align="left" class="style8"><%=dec_date%></div></th>
      </tr>
      <%if(merged.equalsIgnoreCase("Y"))
      { %>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Merged Date</div></th>
        <th width="1%">:</th>
        <th><div align="left" class="style8"><%=merge_date%></div></th>
      </tr>
      <%
        }
        else
        {
        %>
        <tr>
          <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Account Merged</div></th>
          <th width="1%">:</th>
          <th><div align="left"><input type="checkbox" name="marged" value="Y"/></div></th>
        </tr>
        <%
          }
       %>

  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
      <%if(!merged.equalsIgnoreCase("Y"))
      {
     %>
      <th width="43%" scope="row">
        <div align="right">
          <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
        </div></th>
        <td width="57%">
	          <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="gotobank()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
        </td>
         <%
        }
        else
        {
          %>
          <td width="100%"><div align="center">
            <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="gotobank()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
          </div></td>
        <%
          }
        %>
      </tr>
</table>
<input type="hidden" name="decdate" value="<%=dec_date%>"/>
</form>
<%
   if (isConnect)
   {
     try
     {
       cm.takeDown();
     }catch(Exception e){}
   }
%>
</body>
</html>
