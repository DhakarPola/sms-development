<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : October 2005
'Page Purpose  : Saves update of bank information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update Bank Information</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

  String BankName = String.valueOf(request.getParameter("bname"));
  String BranchName = String.valueOf(request.getParameter("brname"));
  String BankAddress = String.valueOf(request.getParameter("baddress"));
  String BankContact = String.valueOf(request.getParameter("bcontact"));
  String BankTelephone = String.valueOf(request.getParameter("btel"));
  String BankMobile = String.valueOf(request.getParameter("bmob"));
  String BankEmail = String.valueOf(request.getParameter("bfax"));
  String BankFax = String.valueOf(request.getParameter("bemail"));
  String BankID = String.valueOf(request.getParameter("fbankid"));

  BankName=cm.replace(BankName,"'","''");
  BranchName=cm.replace(BranchName,"'","''");
  BankAddress=cm.replace(BankAddress,"'","''");
  BankContact=cm.replace(BankContact,"'","''");
  BankTelephone=cm.replace(BankTelephone,"'","''");
  BankMobile=cm.replace(BankMobile,"'","''");
  BankEmail=cm.replace(BankEmail,"'","''");
  BankFax=cm.replace(BankFax,"'","''");

  String ubank = "call UPDATE_BANK('" + BankID + "','" + BankName + "','" + BranchName + "', '" + BankAddress + "','" + BankContact + "','" + BankTelephone + "','" + BankMobile + "','" + BankFax + "','" + BankEmail + "')";
  boolean b = cm.procedureExecute(ubank);

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Edited Bank Information')";
  boolean ub = cm.procedureExecute(ulog);

 if (isConnect)
  {
    cm.takeDown();
  }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Bank/AllBanks.jsp";
</script>
</body>
</html>
