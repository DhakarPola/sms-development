<script>
/*
'******************************************************************************************************************************************
'Script Author : Aminul Islam
'Creation Date : December 2005
'Page Purpose  : add all the BO Address import [if all are reconciled] in to BOADDRESS table.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   String TotalCountQuery="";
   String rnum="1";

   TotalCountQuery="SELECT BO_ID FROM BOADDRESSTEMPNEW_VIEW ";
   TotalCountQuery += "WHERE (RESIDENT is null) OR (LIABLE_TO_TAX is null) OR (CLASS is null)";
   int Count=cm.queryExecuteCount(TotalCountQuery);
   if (Count > 0) {
    %>
       <script language="javascript">
         alert("Reconciliation is not complete.\nIn order to accept the BO Address Import,\nall the BO Account should be reconciled first !!!");
         history.go(-1);
       </script>
    <%
  } else {
      String SQL = "call ADD_BOADDRESS()";
      cm.connAutoCommit(false);
      boolean b = cm.procedureExecute(SQL);
      cm.connCommit();
      cm.connAutoCommit(true);
    %>
       <script language="javascript">
          location="<%=request.getContextPath()%>/jsp/CDBL/ViewBOAddress.jsp"
       </script>
    <%
  }

/*
'******************************************************************************************************************************************
'Updated By    : Renad Hakim
'Date          : March 2005
'******************************************************************************************************************************************
*/

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Saved BO Address Imports')";
  boolean ub = cm.procedureExecute(ulog);

  if (isConnect) {
    cm.takeDown();
  }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/CDBL/ViewBOAddress.jsp";
</script>
</body>
</html>

