<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : July 2007
'Page Purpose  : Edit BOAddress Info
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cms"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}
%>

<html>
<head>
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cms.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   String boid=String.valueOf(request.getParameter("boid"));

  String shName="";
  String nameOfContact="";
  String address="";
  String city="";
  String state="";
  String postCode="";
  String country="";
  String phone="";
  String fax="";
  String email="";
  String taxFreeShare="";
  String liableToTax="";
  String specialTax="";
  String resident="";
  String nationality="";
  String clas="";
  String pfBDate="";
  String specialTaxVal="";


   String query1 = "SELECT * FROM BOADDRESS_ALL_VIEW WHERE BO_ID = '" + boid + "'";
   System.out.println("query1="+query1);
   try
   {
     cm.queryExecute(query1);
     while(cm.toNext())
     {
       shName=cm.getColumnS("NAME_FIRSTHOLDER");
       nameOfContact=cm.getColumnS("NAME_CONTACT_PERSON");
       address=cm.getColumnS("ADDRESS");
       city=cm.getColumnS("CITY");
       state=cm.getColumnS("STATE");
       postCode=cm.getColumnS("POSTAL_CODE");
       country=cm.getColumnS("COUNTRY");
       phone=cm.getColumnS("PHONE");
       fax=cm.getColumnS("FAX");
       email=cm.getColumnS("EMAIL");
       taxFreeShare=cm.getColumnS("TAX_FREE_SHARE");
       liableToTax=cm.getColumnS("LIABLE_TO_TAX");
       specialTax=cm.getColumnS("SPECIAL_TAX");
       resident=cm.getColumnS("RESIDENT");
       nationality=cm.getColumnS("NATIONALITY");
       clas=cm.getColumnS("CLASS");
       pfBDate=cm.getColumnDT("PF_BDATE");
       if(shName==null)
       shName="";
       if(nameOfContact==null)
       nameOfContact="";
       if(address==null)
       address="";
       if(city==null)
       city="";
       if(state==null)
       state="";
       if(postCode==null)
       postCode="";
       if(country==null)
       country="";
       if(phone==null)
       phone="";
       if(fax==null)
       fax="";
       if(email==null)
       email="";
       if(taxFreeShare==null)
       taxFreeShare="";
       if(String.valueOf(liableToTax).equals("T"))
       liableToTax="T";
       else
       liableToTax="F";

        if(String.valueOf(specialTax).equals("T"))
       specialTax="T";
       else
       specialTax="F";

       if(String.valueOf(resident).equals("T"))
       resident="T";
       else
       resident="F";


       /*if(specialTax==null)
       specialTax="F";
       if(resident==null)
       resident="F";*/

       if(nationality==null)
       nationality="";
       if(clas==null)
       clas="";
       if(pfBDate==null)
       pfBDate="";
       if(specialTax.equals("T"))
       {
         String sp_q="select * from special_Tax where folio_no='" + boid + "'";
         //System.out.println("sp_q= "+sp_q);
         cms.queryExecute(sp_q);
         cms.toNext();
         specialTaxVal=cms.getColumnS("AMOUNT");
       }
     }
   }
   catch(Exception e){}
%>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit BOAddress</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--



function formconfirm()
{
  if (confirm("Do you want to change the BOAddress Information?"))
  {
    return true;
  }
}

function showText()
{
    if(document.forms[0].specialtax.checked==true)
    {
      document.forms[0].spTax.style.display='';
    }
    else
    {
      document.forms[0].spTax.style.display="none";
    }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('shname','- Shareholder Name (Option must be entered)');
  BlankValidate('address','- Shareholder Address (Option must be entered)');
  BlankValidate('city','- City (Option must be entered)');
  BlankValidate('state','- State (Option must be entered)');
  if(document.forms[0].specialtax.checked==true)
    {
      BlankValidate('spTax','- Special Tax Value (Option must be entered)');
    }
  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">

<form action="UpdateBOAddress.jsp" method="get" enctype="multipart/form-data" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Edit BOAddress Information</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;BOID</div></th>
        <td width="1%">:</td>
        <th scope="row" width="55%"><div align="left" class="style8"><%=boid%></div></th>
      </tr>

      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Shareholder Name</div></th>
        <td width="1%">:</td>
        <th scope="row" width="55%"><div align="left" class="style8"><%=shName%></div></th>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Name of Contact Person</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="contname" class="SL2TextField" value="<%=nameOfContact%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Address:</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left">
           <textarea name="address" cols="20" rows="3" id="address" class="ML9TextField"><%=address%></textarea>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;City</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="city" class="SL2TextField" value="<%=city%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;State</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="state" class="SL2TextField" value="<%=state%>">
        </div></td>
      </tr>
       <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Postal Code</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="postalcode" class="SL2TextField" value="<%=postCode%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Country</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="country" class="SL2TextField" value="<%=country%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Phone</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="phone" class="SL2TextField" value="<%=phone%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Fax</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="fax" class="SL2TextField" value="<%=fax%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Email</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="email" class="SL2TextField" value="<%=email%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Tax Free Share</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="taxfreeshare" class="SL2TextField" value="<%=taxFreeShare%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Liable to Tax</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <%
            if(liableToTax.equalsIgnoreCase("T"))
            {
             %>
             <input name="liabletotax" type="checkbox" checked="checked" value="T">
              <%
            }
            else
              {
               %>
                <input name="liabletotax" type="checkbox" value="T">
                 <%
               }
            %>
      </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Special Tax</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
         <%
                            if(specialTax.equalsIgnoreCase("T"))
                            {
                              %>
                              <input name="specialtax" type="checkbox" onclick="showText()" checked="checked" value="T">&nbsp;<input id="spTax" type="text" name="spTax" class="SLSPTAXTextField" onkeypress="keypressOnDoubleFld()" value="<%=specialTaxVal%>"/>
                              <%
                            }
                            else
                            {
                              %>
                              <input name="specialtax" type="checkbox" onclick="showText()">&nbsp;<input id="spTax" type="text" name="spTax" class="SLSPTAXTextField" style="display:none" onkeypress="keypressOnDoubleFld()" value="<%=specialTaxVal%>"/>
                              <%
                            }
                            %>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Resident</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
        <%
                      resident = resident.trim();
                      if(resident.equalsIgnoreCase("T"))
                      {
                      %>
                      <input name="resident" type="checkbox" checked="checked" value="T">
                      <%
                      }
                      else
                      {
                      %>
                        <input name="resident" type="checkbox" value="T">
                      <%
                      }
            %>
      </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Nationality</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="nationality" class="SL2TextField" value="<%=nationality%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Class</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="clas" class="SL2TextField" value="<%=clas%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date</div></th>
        <td width="1%">:</td>
        <th scope="row" width="55%"><div align="left" class="style8"><%=pfBDate%></div></th>
      </tr>
  </table>
 </div>
  <!--HIDDEN FIELDS-->
  <input type="hidden" name="boid" value="<%=boid%>">
  <input type="hidden" name="shname" value="<%=shName%>">
  <input type="hidden" name="pfBDate" value="<%=pfBDate%>">
  <br>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="43%" scope="row">
            <div align="right">
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
            </div></th>
        <td width="57%">
	          <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="history.go(-1);" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
        </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     try
     {
       cm.takeDown();
       cms.takeDown();
     }
     catch(Exception e){}
   }
%>
</body>
</html>
