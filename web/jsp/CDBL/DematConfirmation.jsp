<script>
/*
'******************************************************************************************************************************************
'Script Author : Aminul Islam
'Updated By    : Renad Hakim
'Creation Date : December 2005
'Page Purpose  : View of all the Demat Confirmation import
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Demat Confirmation Import</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

function closedocument()
{
  location = "<%=request.getContextPath()%>/jsp/Home.jsp";
}

//Displays the previous list of folio
function goPrevious()
{
  var f=document.forms[0];
  if (f.FirstCount.value>1) {
    var FC=parseInt(f.FirstCount.value) - parseInt(f.maxCount.value);
    var LC=parseInt(f.FirstCount.value)-1;
    if (LC<1) LC=1;
    if (FC<1) {
      FC=1;
      LC=f.maxCount.value;
    }
    location = "<%=request.getContextPath()%>/jsp/CDBL/DematConfirmation.jsp?prev=true&FirstCount="+FC+"&LastCount="+LC;
  }
}
function goNext()
{
  var f=document.forms[0];
  if (f.LastCount.value<f.TotalRows.value) {
    var FC=parseInt(f.LastCount.value)+1;
    var LC=parseInt(f.LastCount.value)+parseInt(f.maxCount.value);
    if (LC>parseInt(f.TotalRows.value)) LC=parseInt(f.TotalRows.value);
    location = "<%=request.getContextPath()%>/jsp/CDBL/DematConfirmation.jsp?next=true&FirstCount="+FC+"&LastCount="+LC;
  }
}

var FldName = null;
function loadAccept(){
   var TU = "<%=request.getContextPath()%>/jsp/CDBL/ConfirmAcceptDC.jsp";
   var width = 380;
   var height = 130;
   ONW('AcceptDC',TU, width, height, 0, 0, 0);
}

function SubmitTo()
{
  document.forms[0].action = "SaveAcceptDC.jsp";
  document.forms[0].submit();
}

//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
.style19 {
	color: #0A2769;
	font-weight: bold;
}
.style17 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   String previous = "";
   String next = "";

   previous= request.getParameter("prev");
   next= request.getParameter("next");

   int folio_state=0;
   int COUNT;
   int MAX_RECORDS=25;
   String folio_no=request.getParameter("searchFolio");
   String FirstCount = request.getParameter("FirstCount");
   String LastCount=request.getParameter("LastCount");

   String folioname = "";
   String certNo = "";
   String pfBDate = "";
   int total = 0;
   String boid = "";
   int dist_from = 0;
   int dist_to = 0;
   int intFC=0;
   int intLC=0;
   String dest = "";
   String TotalCountQuery="";
   String rnum="1";


   TotalCountQuery="select * from BODEMATCONFIRMATIONTEMP_VIEW";
   int TotalRows= cm.queryExecuteCount(TotalCountQuery);

   String query1;

   if(((folio_no==null) && (previous==null) && (next==null)) || ((folio_no!=null) && (previous!=null) && (next!=null))) {
     if (folio_no==null) {
       folio_no="00000000";
     }
     FirstCount="1";
     LastCount=String.valueOf(MAX_RECORDS);
     query1 = "select * from (";
     query1 += "select DC. BOID, DC.SHR_NAME, DC.FOLIO_NUMB, DC.CERTIFICATE_NUMB, DC.START_DIST_NUMB,";
     query1 += "DC.END_DIST_NUMB, DC.TOTAL, DC.PF_BDATE,rownum rnum";
     query1 += " FROM BODEMATCONFIRMATIONTEMP_VIEW DC)";
     query1 += " WHERE (FOLIO_NUMB>='"+folio_no+"') and rownum between 1 and "+String.valueOf(MAX_RECORDS);
   } else {
     query1 = "select * from ";
     query1 += "(SELECT DC. BOID, DC.SHR_NAME, DC.FOLIO_NUMB, DC.CERTIFICATE_NUMB, DC.START_DIST_NUMB,";
     query1 += "DC.END_DIST_NUMB, DC.TOTAL, DC.PF_BDATE, rownum rnum";
     query1 += " FROM BODEMATCONFIRMATIONTEMP_VIEW DC";
     query1 += " WHERE rownum<="+LastCount+" ORDER BY DC.FOLIO_NUMB,DC.CERTIFICATE_NUMB)";
     query1 += " where rnum >="+FirstCount;
   }

 COUNT = cm.queryExecuteCount(query1);
 cm.queryExecute(query1);

%>
  <span class="style7">
  <form method="GET" action="DematConfirmation.jsp">
  <SPAN id="dprint">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnAccept.gif" onclick="loadAccept();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnAcceptR.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnAccept.gif'">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
    <img name="B7" src="<%=request.getContextPath()%>/images/btnClose.gif" onclick="closedocument()" onMouseOver="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
  </SPAN>
  <table width="100%" BORDER=1  cellpadding="0" bordercolor="#0044B0" style="border-collapse: collapse">
  <!--DWLayoutTable-->
  <tr><td height="30" bgcolor="#0044B0" class="style17" valign="middle"><center>Demat Confirmation Imports</center></td></tr>
  <tr>
  	<td>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0">
			<tr bgcolor="#E8F3FD">
				<td width="10%">
                                </td>
				<td width="10%"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goPrevious()"></td>
				<td width="29%"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNext()"></td>
				<td width="12%" height="35" class="style19">Folio No:</td>
				<td width="19%"><input name="searchFolio" type="text"></td>
				<td width="20%"><img  name="B4" src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="submit()"  onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnSearchR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnSearch.gif'"></td>
			</tr>
                </table>
	</td>
  </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#06689E">
    <td width="13%" class="style9" bgcolor="#0044B0"><div align="center">BO No.</div></td>
    <td width="9%" class="style9" bgcolor="#0044B0"><div align="center">Folio No.</div></td>
    <td width="26%" class="style9" bgcolor="#0044B0"><div align="center">Shareholder Name</div></td>
    <td width="12%" class="style9" bgcolor="#0044B0"><div align="center">Certificate No.</div></td>
    <td width="11%" class="style9" bgcolor="#0044B0"><div align="center">Dist. From</div></td>
    <td width="11%" class="style9" bgcolor="#0044B0"><div align="center">Dist. To</div></td>
    <td width="9%" class="style9" bgcolor="#0044B0"><div align="center">Shares</div></td>
    <td width="9%" class="style9" bgcolor="#0044B0"><div align="center">PF BDate</div></td>
  </tr>
  <div align="left">
     <%
    folio_state=0;

    while(cm.toNext())
     {
       folio_state++;
       folio_no=cm.getColumnS("FOLIO_NUMB");
       certNo = cm.getColumnS("CERTIFICATE_NUMB");
       folioname = cm.getColumnS("SHR_NAME");
       total = cm.getColumnI("TOTAL");
       boid = cm.getColumnS("BOID");
       dist_from = cm.getColumnI("START_DIST_NUMB");
       dist_to = cm.getColumnI("END_DIST_NUMB");
       pfBDate = cm.getColumnS("PF_BDATE");
       rnum = cm.getColumnS("rnum");
       if(folio_state==1) {
         FirstCount=rnum;
       }
       if (!folio_no.equals("null"))
       {
         if (String.valueOf(folioname).equals("null"))
           folioname = "";
         if (String.valueOf(certNo ).equals("null"))
           certNo = "";
         %>
    </div>
  <tr bgcolor="#E8F3FD">
           <td class="style13"><div align="left" class="style12">
             <div align="center"><%=boid%></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center"><%=folio_no%></div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="left">&nbsp;&nbsp;<%=folioname%></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center"><%=certNo%></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center"><%=dist_from%></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center"><%=dist_to%></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center"><%=total%></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center"><%=pfBDate%></div>
           </div></td>
         </tr>
             <%
         }
     }
     LastCount=rnum;
   if (isConnect)
   {
     cm.takeDown();
   }
  %>
     <input  type="hidden" name="prev" value="false">
     <input  type="hidden" name="next" value="false">
     <input type="hidden" name="FirstCount" value="<%=FirstCount%>"/>
     <input type="hidden" name="LastCount" value="<%=LastCount%>"/>
     <input type="hidden" name="TotalRows" value="<%=TotalRows%>"/>
     <input type="hidden" name="maxCount" value="<%=MAX_RECORDS%>"/>
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD" ><center>

</body>
</html>
