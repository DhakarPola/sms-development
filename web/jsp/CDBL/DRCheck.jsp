<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : July 2007
'Page Purpose  : Check Demat Request import of Last Date
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cms"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Check Demat Requests</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--

function closedocument()
{
  location = "<%=request.getContextPath()%>/jsp/Home.jsp";
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
.style19 {
	color: #0A2769;
	font-weight: bold;
}
.style17 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cms.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

    String q="call DELETE_CHECK_DEMAT_REQUEST()";
    try
    {
      cm.procedureExecute(q);
    }catch(Exception e){}
   String maxDate="";
   String Query="select * from DEMAT_REQUEST_VIEW where PF_BDATE=(select max(PF_BDATE) from DEMAT_REQUEST_VIEW)";
   try
   {
   cm.queryExecute(Query);
   cm.toNext();
   maxDate=cm.getColumnDT("PF_BDATE");
   }
   catch(Exception e){}


   String shName="";
   String certNo="";
   String distStart="";
   String distEnd="";
   String folio="";
   String boid="";
   String status="";
   int valid=0;
   int invalid=0;

   try
   {
     cm.queryExecute(Query);
     while(cm.toNext())
     {
       shName=cm.getColumnS("SH_NAME").trim();
       certNo=cm.getColumnS("CERTIFICATE_NO");
       distStart=cm.getColumnS("DIST_FROM");
       distEnd=cm.getColumnS("DIST_TO");
       folio=cm.getColumnS("FOLIO_NO");
       boid=cm.getColumnS("BO_ID");

       status="";
       String shareholder="select NAME from shareholder where FOLIO_NO='" + folio + "'";
       cms.queryExecute(shareholder);
       cms.toNext();
       String shareholderName=cms.getColumnS("NAME").trim();

       /* remove prefix from name */
       String tempName[]=shName.split(" ");
       String tempshName="";
       if(tempName[0].equalsIgnoreCase("Mr.")||tempName[0].equalsIgnoreCase("Mrs.")||tempName[0].equalsIgnoreCase("Miss")||tempName[0].equalsIgnoreCase("Dr.")||tempName[0].equalsIgnoreCase("Ms."))
       {
         for(int i=1;i<tempName.length;i++)
         tempshName=tempshName+tempName[i]+" ";
         tempshName=tempshName.trim();
       }
       else
       tempshName=shName;

       String tempSName[]=shareholderName.split(" ");
       String tempshareholderName="";
       if(tempSName[0].equalsIgnoreCase("Mr.")||tempSName[0].equalsIgnoreCase("Mrs.")||tempSName[0].equalsIgnoreCase("Miss")||tempSName[0].equalsIgnoreCase("Dr.")||tempSName[0].equalsIgnoreCase("Ms."))
       {
         for(int i=1;i<tempName.length;i++)
         tempshareholderName=tempshareholderName+tempSName[i]+" ";
         tempshareholderName=tempshareholderName.trim();
       }
       else
       tempshareholderName=shareholderName;


       if(!tempshName.equalsIgnoreCase(tempshareholderName))
       status="Folio number and Shareholder name mismatch.";

       String cert="select HOLDER_FOLIO from certificate where CERTIFICATE_NO='" + certNo + "'";
      cms.queryExecute(cert);
       cms.toNext();
       String folioNo=cms.getColumnS("HOLDER_FOLIO");
       if(!folioNo.equals(folio))
       status="Folio number and Certificate number mismatch.";

       String script="select * from scripts where CERTIFICATE_NO='" + certNo + "'";
       cms.queryExecute(script);
       cms.toNext();
       String distFrom=cms.getColumnS("DIST_FROM");
       String distTo=cms.getColumnS("DIST_TO");
       if(!distFrom.equals(distStart))
       status="Distinction From is Invalid.";
       if(!distTo.equals(distEnd))
       status="Distinction To is Invalid.";

       if(status.equals(""))
       {
         valid++;
         status="valid";
       }
       else
       invalid++;

     String iq="call ADD_CHECK_DEMAT_REQUEST('" + boid + "','" + shName + "','" + folio + "','" + certNo + "','" + distStart + "','" + distEnd + "','" + status + "')";
     cms.procedureExecute(iq);
     }
   }catch(Exception e){}

   String dest=request.getContextPath()+"/DRCheckReports.jsp";

%>
  <span class="style7">
  <form method="GET">
  <SPAN id="dprint">
    <img name="B7" src="<%=request.getContextPath()%>/images/btnClose.gif" onclick="closedocument()" onMouseOver="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
  </SPAN>
  <table width="100%" BORDER=1  cellpadding="0" bordercolor="#0044B0" style="border-collapse: collapse">
  <!--DWLayoutTable-->
  <tr><td height="30" bgcolor="#0044B0" class="style17" valign="middle"><center>Check Demat Requests of Date (<%=maxDate%>)</center></td></tr>
  <tr>
  	<td>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0">
			<tr bgcolor="#E8F3FD">
				<td width="10%">&nbsp;
                                </td>
                        </tr>
                </table>
	</td>
  </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#06689E">
    <td width="50%" class="style9" bgcolor="#0044B0"><div align="center">Check Status</div></td>
    <td width="50%" class="style9" bgcolor="#0044B0"><div align="center">No. of Record(s)</div></td>
  </tr>
  <div align="left">
    </div>
  <tr bgcolor="#E8F3FD">
           <td width="50%" class="style13"><div align="center" class="style12">
            <div align="center"><a HREF="<%=dest%>?status=valid&rdate=<%=maxDate%>">Valid</div>
            </div></td>
           <td width="50%" class="style13"><div align="center" class="style12">
           <div align="center"><%=valid%></div>
           </div></td>
  </tr>
  <tr bgcolor="#E8F3FD">
           <td width="50%" class="style13"><div align="center" class="style12">
            <div align="center"><a HREF="<%=dest%>?status=invalid&rdate=<%=maxDate%>">Invalid</div>
            </div></td>
           <td width="50%" class="style13"><div align="center" class="style12">
           <div align="center"><%=invalid%></div>
           </div></td>
  </tr>
  </table>
<%
String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Check Demat Requests')";
  try
  {
    boolean ub = cm.procedureExecute(ulog);
  }
  catch(Exception e){}

   if (isConnect)
   {
     try
     {
     cm.takeDown();
     cms.takeDown();
     }
     catch(Exception e){}
   }
  %>
  </form>
  <tr bgcolor="#E8F3FD" >
  <td height="100%" bgcolor="#E8F3FD" ><center>
</body>
</html>
