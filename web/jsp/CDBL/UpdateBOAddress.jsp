<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : July 2007
'Page Purpose  : update of BOAddress information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update BOAddress Information</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

    String boid=String.valueOf(request.getParameter("boid"));
    String shName=String.valueOf(request.getParameter("shname"));
    String nameOfContact=String.valueOf(request.getParameter("contname"));
    String address=String.valueOf(request.getParameter("address"));
    String city=String.valueOf(request.getParameter("city"));
    String state=String.valueOf(request.getParameter("state"));
    String postCode=String.valueOf(request.getParameter("postalcode"));
    String country=String.valueOf(request.getParameter("country"));
    String phone=String.valueOf(request.getParameter("phone"));
    String fax=String.valueOf(request.getParameter("fax"));
    String email=String.valueOf(request.getParameter("email"));
    String taxFreeShare=String.valueOf(request.getParameter("taxfreeshare"));
    String liableToTax=String.valueOf(request.getParameter("liabletotax"));
    String specialTax=String.valueOf(request.getParameter("specialtax"));
    String resident=String.valueOf(request.getParameter("resident"));
    String nationality=String.valueOf(request.getParameter("nationality"));
    String clas=String.valueOf(request.getParameter("clas"));
    String pfBDate=String.valueOf(request.getParameter("pfBDate"));
    String specilaTaxVal=String.valueOf(request.getParameter("spTax"));
	
	String tin=String.valueOf(request.getParameter("tin"));
	String nid=String.valueOf(request.getParameter("nid"));
	String routingno=String.valueOf(request.getParameter("routingno"));


	   if (String.valueOf(nid).equals("null"))
          nid = "";
	   if (String.valueOf(routingno).equals("null"))
          routingno = "";
	   if (String.valueOf(tin).equals("null"))
          tin = ""; 
System.out.println("routingno= "+routingno);
     if(shName.equals("null")||shName==null)
       shName="";
       if(nameOfContact.equals("null")||nameOfContact==null)
       nameOfContact="";
       if(address.equals("null")||address==null)
       address="";
       if(city.equals("null")||city==null)
       city="";
       if(state.equals("null")||state==null)
       state="";
       if(postCode.equals("null")||postCode==null)
       postCode="";
       if(country.equals("null")||country==null)
       country="";
       if(phone.equals("null")||phone==null)
       phone="";
       if(fax.equals("null")||fax==null)
       fax="";
       if(email.equals("null")||email==null)
       email="";
       if(taxFreeShare.equals("null")||taxFreeShare==null)
       taxFreeShare="";
       if(liableToTax.equals("null")||liableToTax==null)
       liableToTax="";
       if(specialTax.equals("null")||specialTax==null)
       specialTax="";
       if(resident.equals("null")||resident==null)
       resident="";
       if(nationality.equals("null")||nationality==null)
       nationality="";
       if(clas.equals("null")||clas==null)
       clas="";
       if(pfBDate.equals("null")||pfBDate==null)
       pfBDate="";

      if(!resident.equalsIgnoreCase("T"))
       resident="F";
    else
       resident="T";

    if(!liableToTax.equalsIgnoreCase("T"))
       liableToTax="F";
    else
       liableToTax="T";

    //System.out.println("specialTax= "+specialTax);
    if(String.valueOf(specialTax).equalsIgnoreCase("on")||String.valueOf(specialTax).equals("T"))
       specialTax="T";
    else
       specialTax="F";
    //System.out.println("specialTax after= "+specialTax);
   String foliono=boid;

    if(specialTax.equals("T"))
    {
      String seletSpTax="Select * from special_tax where folio_no='" + foliono + "'";
      String inserSpTax="insert into special_tax  values ('" + foliono + "','" + specilaTaxVal + "')";
      String updateSpTax="update special_tax set amount='" + specilaTaxVal + "' where folio_no='" + foliono + "'";
      try
      {
       int found=cm.queryExecuteCount(seletSpTax);
       //System.out.println("found= "+found);
       if(found>0)
       {
         cm.queryExecute(updateSpTax);
       }
       else
       {
         cm.queryExecute(inserSpTax);
       }
      }catch(Exception e){}
    }
    else if(specialTax.equals("F"))
    {
      String deletSpTax="delete from special_tax where folio_no='" + foliono + "'";
       try
      {
      cm.queryExecute(deletSpTax);
      }catch(Exception e){}
    }

    String uboaddress="call UPDATEBOADDRESS('"+ boid +"','"+ shName +"','"+ nameOfContact +"','"+ address +"','"+ city +"','"+ state +"','"+ postCode +"','"+ country +"','"+ phone +"','"+ fax +"','"+ email +"','"+ taxFreeShare +"','"+ liableToTax +"','"+ specialTax +"','"+ resident +"','"+ nationality +"','"+ clas +"','"+ pfBDate +"','"+ tin +"','"+ nid +"','"+ routingno+"')";

    String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Edit BOAddress')";
    System.out.println(uboaddress);
    try
    {
      boolean b=cm.procedureExecute(uboaddress);
      boolean u = cm.procedureExecute(ulog);
    }
    catch(Exception e){}
    if (isConnect)
    {
      try
      {
        cm.takeDown();
      }
      catch(Exception e){}
    }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/CDBL/viewAddress.jsp";
</script>
</body>
</html>
