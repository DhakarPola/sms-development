<script type="">
/*
'******************************************************************************************************************************************
'Script Author : Ehsan Al Baqui
'Creation Date : December 2005
'Page Purpose  : Save of all the BO Address Import
'******************************************************************************************************************************************
*/
</script>
<%@page errorPage="CommonError.jsp"%>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<%@page language="java" import="oracle.jdbc.driver.*,java.sql.*,org.apache.commons.fileupload.*,java.io.*,java.util.*,java.util.List,java.util.Vector,java.util.Date,java.text.SimpleDateFormat"%>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css">
</LINK><%if (String.valueOf(session.getAttribute("UserName")).equals("null")) {%>
<script type="">top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%}%>
<%
  boolean isConnect = cm.connect();
  if (isConnect == false) {
%>
<jsp:forward page="ErrorMsg.jsp">
  <jsp:param name="ErrorTitle" value="Connection Failure"/>
  <jsp:param name="ErrorHeading" value="Connection Problem"/>
  <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator"/>
</jsp:forward>
<%
  } else {
    String CurrentFile = "";
    String fname = "";
    String fieldName;
    String filepath = null;
    String txtTableName = "";
    String txtRemarks = "";
    String realFilePath = "";
    String urlpath = "";
    String folderpath = "";
    DiskFileUpload u1 = new DiskFileUpload();
    List items = u1.parseRequest(request);
    Iterator itr1 = items.iterator();
    FileItem item;
    boolean isFileExists = false;
    while (itr1.hasNext()) {
      item = (FileItem) itr1.next();
      fieldName = item.getFieldName();
      if (fieldName.equals("filepath")) {
        CurrentFile = item.getName();
        File fullFile = new File(item.getName());
        realFilePath = fullFile.getAbsolutePath();
        fname = fullFile.getName();
        if (!CurrentFile.equals("")) {
          //Code for creating a folder
          String path = getServletContext().getRealPath("/");
          File f1 = new File(path + "/import");
          if (!f1.exists()) {
            f1.mkdir();
          }
          File f2 = new File(path + "/import/BOADDRESSTEMP");
          if (!f2.exists()) {
            f2.mkdir();
          }
          urlpath = path + "import\\BOADDRESSTEMP\\" + fname;
          folderpath = path + "/import/BOADDRESSTEMP/";
          File savedFile = new File(folderpath, fullFile.getName());
          item.write(savedFile);
          isFileExists = savedFile.isFile();
        }
      }
      else if (fieldName.equals("txtTableName")) {
        txtTableName = item.getString();
      }
      else if (fieldName.equals("txtRemarks")) {
        txtRemarks = item.getString();
      }
    }
    if (isFileExists) {
      System.out.println("--A");
      String Msg = cm.importBOAddress(realFilePath, "BOAddress", txtTableName, txtRemarks, "Import", new Date(), urlpath);
      System.out.println("--B");
      File f = new File(urlpath);
      f.delete();
      if (Msg.equals("")) {
        %>
            <script language="javascript">
             location = "<%=request.getContextPath()%>/jsp/CDBL/ViewBOAddress.jsp";
            </script>
    <%} else {%>
            <script language="javascript">
             location = "<%=request.getContextPath()%>/jsp/CDBL/BOAddress.jsp?msg=<%=Msg%>";
            </script>
    <%
      }
  } else {
%>
         <script language="javascript">
             location = "<%=request.getContextPath()%>/jsp/CDBL/BOAddress.jsp?msg=FileNotFound";
         </script>
<%
  }
      cm.takeDown();
      }
%>
