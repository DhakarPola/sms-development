<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : June 2007
'Page Purpose  : Import the Demat Request from the attached .mdb file.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cms"  class="batbsms.conBean"/>
<%@ page language="java" import="oracle.jdbc.driver.*,java.sql.*,org.apache.commons.fileupload.*,java.io.*,java.util.*,java.util.List,java.util.Vector,java.util.Date,java.text.SimpleDateFormat" %>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cms.connect();
   if(isConnect==false){
%>
     <jsp:forward page="ErrorMsg.jsp" >
     <jsp:param name="ErrorTitle" value="Connection Failure" />
     <jsp:param name="ErrorHeading" value="Connection Problem" />
     <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
<%
    } else {
        String CurrentFile = "";
        String fname ="";
        String fieldName;
        String filepath=null;
        String txtTableName="";
        String txtRemarks="";
        String realFilePath="";
        String urlpath ="";
        String folderpath="";


        DiskFileUpload u1 = new DiskFileUpload();
        List items = u1.parseRequest(request);
        Iterator itr1 = items.iterator();
        FileItem item;
        boolean isFileExists=false;

         while(itr1.hasNext()) {
            item = (FileItem) itr1.next();
            fieldName = item.getFieldName();
            if(fieldName.equals("filepath")) {
                CurrentFile = item.getName();
                File fullFile  = new File(item.getName());
                realFilePath=fullFile.getAbsolutePath();
                fname =  fullFile.getName();
                if (!CurrentFile.equals("")){
                  //Code for creating a folder
                  String path=getServletContext().getRealPath("/");
                  File f1 = new File(path + "/import");
                  if(!f1.exists()){
                    f1.mkdir();
                  }
                  File f2 = new File(path + "/import/DC");
                  if(!f2.exists()){
                    f2.mkdir();
                  }

                  urlpath = path + "import\\DC\\" + fname;
                  folderpath = path + "/import/DC/";
                  File savedFile = new File(folderpath,fullFile.getName());
                  item.write(savedFile);
                  isFileExists= savedFile.isFile();
                }
            } else if (fieldName.equals("txtTableName")) {
                txtTableName=item.getString();
            } else if (fieldName.equals("txtRemarks")) {
                txtRemarks=item.getString();
            }
         }

        if(isFileExists){
             String failure=cm.importDematRequest(realFilePath,"Demat Requests",txtTableName,txtRemarks,"Import", new Date(), urlpath);
             File f = new File(urlpath);
             f.delete();
             if (failure.equals("")) {
             String maxRfId="select nvl(max(REF_NO),0)as refId from DEMAT_REQUEST_VIEW";
             String distRefNo="select distinct DE_NO,PF_BDATE as PD from DEMAT_REQUEST where REF_NO=0";
            int maxId=0;
            try
            {
              cm.queryExecute(maxRfId);
              cm.toNext();
              maxId=cm.getColumnI("refId");
              cm.queryExecute(distRefNo);
              while(cm.toNext())
              {
                maxId++;
                String De_No=cm.getColumnS("DE_NO");
                String pDate=cm.getColumnDT("PD");
                String update="update demat_request set ref_no=" + maxId + " where DE_NO=" + De_No + " and PF_BDATE=TO_DATE('" + pDate + "','DD/MM/YYYY')";
                cms.queryExecute(update);
                String totalshare_q="select nvl(sum(DR_QTY),0)as TS from demat_request where ref_no=" + maxId + "";
                //System.out.println("totalshare_q="+totalshare_q);
                cms.queryExecute(totalshare_q);
                cms.toNext();
                String totalshare=cms.getColumnS("TS");
                String des="select distinct BO_ID,FOLIO_NO,SH_NAME,PF_BDATE,DE_NO  from demat_request where ref_no=" + maxId + "";
                cms.queryExecute(des);
                cms.toNext();
                String rec_date=cms.getColumnDT("PF_BDATE");
                String folio_no=cms.getColumnS("FOLIO_NO");
                String sname=cms.getColumnS("SH_NAME");
                String boid=cms.getColumnS("BO_ID");
                String DENO=cms.getColumnS("DE_NO");
                //System.out.println("rec_date= "+rec_date);
                //System.out.println("folio_no= "+folio_no);
                //System.out.println("sname= "+sname);
                //System.out.println("boid= "+boid);
                //System.out.println("DENO= "+DENO);
                String insert="call ADD_DEMAT_REGISTER('" + maxId + "','" + rec_date + "','" + sname + "','" + folio_no + "','" + boid + "','" + DENO + "','" + totalshare + "')";
                //System.out.println("insert="+insert);
                boolean b1=cms.procedureExecute(insert);
              }
            }
            catch(Exception e){}

             %>
             <script language="javascript">
             location = "<%=request.getContextPath()%>/jsp/CDBL/DematRequests.jsp";
             </script>
           <%} else {%>
             <script language="javascript">
             location = "<%=request.getContextPath()%>/jsp/CDBL/DReqImport.jsp?msg=<%=failure%>";
             </script>
           <%
            }
        } else { %>
             <script language="javascript">
             location = "<%=request.getContextPath()%>/jsp/CDBL/DReqImport.jsp?msg=FileNotFound";
             </script>
      <%}
  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Imported Demat Requests')";
  try
  {
    boolean ub = cm.procedureExecute(ulog);

    cm.takeDown();
    cms.takeDown();
  }catch(Exception e){}
   }   %>
