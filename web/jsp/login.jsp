<script>
    /*
     '******************************************************************************************************************************************
     'Script Author : Renad Hakim
     'Creation Date : October 2005
     'Page Purpose  : Logs in an user.
     '******************************************************************************************************************************************
     */
</script>

<%@page language="java" import="java.sql.*,java.util.Vector,java.security.MessageDigest,java.util.Collections,java.io.*"%>
<%@page errorPage="CommonError.jsp"%>
<jsp:useBean id="ad" scope="session" class="batbsms.MSADConnection"/>
<jsp:useBean id="connection" scope="page" class="batbsms.conBean"/>
<jsp:useBean id="UserDAO" scope="page" class="batbsms.UserDAO"/>
<jsp:useBean id="UserModel" scope="page" class="batbsms.UserModel"/>
<jsp:useBean id="Authentication" scope="page" class="batbsms.Authentication"/>
<html>
    <head>
        <title>Thanks for the request</title>
        <LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css">
    </head>
    <body bgcolor="#ffffff" topmargin="0" marginheight="0">
        <%
            boolean check = false; //to check the execution status
            String Status = "";
            String SQL = "";
            String UserID = String.valueOf(request.getParameter("UserID"));
            String UserType = String.valueOf(request.getParameter("UserType"));
            String Password = String.valueOf(request.getParameter("Password"));
            boolean IsAdministrator = false;
            boolean IsSupervisor = false;
            boolean IsOperator = false;
            String CurrUserRole = "";
            String CurrUserName = "";

            
            if (Authentication.checkUserId(UserID)) {
                UserModel = UserDAO.getUserByName(UserID);
                Authentication.authError = false;
                if ( !UserModel.getLockOut()) {
                    if (UserModel.getLoginAttempt() > 3) {
                        Authentication.doSetLockout(UserModel.getLoginId(),UserModel.getLockOut());
                        Authentication.authError = true;
                        Authentication.authMessageTitle ="Locked";
                        Authentication.authMessageHeading ="Locked";
                        Authentication.authMessage ="5 Error : You might have been Locked, Please contact the System Administrator";

                    } else {
                        if (Authentication.passwordValidation(Password, UserModel.getUserPass())) {
                            if(!Authentication.checkUserDisabled(UserModel.getLoginId())){                      // user not disabled 
                                if(!Authentication.checkUserInactivity(UserModel.getLoginId())) {               // User active 
                                    if(Authentication.firstTimeLogin(UserModel.getLoginId())){                  // First time login 
                                        //Authentication.passwordChange(UserModel.getLoginId(), UserModel.getUserPass());
                                        //Authentication.passwordChange(UserModel.getLoginId(), UserModel.getUserPass());
                                        %>
                                            <script language="JavaScript">
                                                location = '<%=request.getContextPath()%>/jsp/Users/ChangePassword.jsp';
                                            </script><
                                        <%
                                        
                                    }else{      // not first time login 
                                        if( Authentication.concurrentLogin(UserModel.getLoginId())){            // already logged in using this user pass  
                                            Authentication.authError = true;
                                            Authentication.authMessageTitle ="Already Loggedin";
                                            Authentication.authMessageHeading ="Already Loggedin";
                                            Authentication.authMessage ="6 Error : You Already Logged into system from another terminal , Please contact the System Administrator";
                                        }else{                  // no one already logged in 
                                            if( Authentication.passwordExpiration(UserModel.getLoginId(), UserModel.getPasswordExpireTime())){  // Password Expired 
                                                %>
                                                    <script language="JavaScript">
                                                        location = '<%=request.getContextPath()%>/jsp/Users/ChangePassword.jsp';
                                                    </script><
                                                <%
                                                 
                                            }else{ // Password Not Expired
                                            
                                            }

                                        }
                                    }
                                }else{      // user inactive for 45 days 
                                    Authentication.disableUser(UserModel.getLoginId(),UserModel.getDisabled());     // Disable user
                                    Authentication.authError = true;
                                    Authentication.authMessageTitle ="User disabled";
                                    Authentication.authMessageHeading ="User disabled";
                                    Authentication.authMessage ="6 Error : User disabled for inactivity, Please contact the System Administrator";
                                }
                            }else{          // user disabled 
                                Authentication.authError = true;
                                Authentication.authMessageTitle ="User disabled";
                                Authentication.authMessageHeading ="User disabled";
                                Authentication.authMessage ="6 Error : User disabled, Please contact the System Administrator";
                            }
                        } else {
                            Authentication.increaseLoginAttempt(UserID,UserModel.getLoginAttempt());
                            Authentication.authError = true;
                            Authentication.authMessageTitle ="User / Password wrong";
                            Authentication.authMessageHeading ="User / Password wrong";
                            Authentication.authMessage ="9 Error : User / Password wrong, Please contact the System Administrator";
                        }
                    }
                } else {                                                              // Acount locked out 
                    if (Authentication.lockoutEndTimeOver(UserModel.getLockOutEndTime())) {
                        Authentication.doResetLockout(UserModel.getLoginId(),UserModel.getLockOut());
                        if (Authentication.passwordValidation(Password, UserModel.getUserPass())) {
                            
                        } else {
                            Authentication.increaseLoginAttempt(UserID,UserModel.getLoginAttempt());
                            Authentication.authError = true;
                            Authentication.authMessageTitle ="User / Password Might be wrong";
                            Authentication.authMessageHeading ="User / Password Might be wrong";
                            Authentication.authMessage ="1 Error : User / Password Might be wrong, Please contact the System Administrator";                            
                        }
                    } else {
                        Authentication.authError = true;
                        Authentication.authMessageTitle ="Locked";
                        Authentication.authMessageHeading ="Locked";
                        Authentication.authMessage ="2 Error : You might have to wait for lockout time, Please contact the System Administrator";                        
                    }
                }
            } else { 
                Authentication.authError = true;
                Authentication.authMessageTitle ="Not registered";
                Authentication.authMessageHeading ="Not registered";
                Authentication.authMessage ="3 Error : You might have to be registered with this system, Please contact the System Administrator";  
            }
            
            if(Authentication.authError){
                %>
                <jsp:forward page="ErrorMsg.jsp">
                    <jsp:param name="ErrorTitle" value="<%=Authentication.authMessageTitle %>"/>
                    <jsp:param name="ErrorHeading" value="<%=Authentication.authMessageHeading %>"/>
                    <jsp:param name="ErrorMsg" value="<%=Authentication.authMessage %>"/>
                </jsp:forward>
                <%
            }

            try {
                MessageDigest md = MessageDigest.getInstance("MD5");
                md.update(Password.getBytes());
                byte[] bytes = md.digest();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < bytes.length; i++) {
                    sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
                }
                Password = sb.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (UserType.equals("NetworkUser")) {
                boolean isConnected2AD = ad.connect();
                //try to connect if failed, redirect to error page
                if (isConnected2AD == false) {
                    if (ad.isNetworkExist) {
                        ad.takeDown();
                    }
        %>
        <jsp:forward page="ErrorMsg.jsp">
            <jsp:param name="ErrorTitle" value="Connection Failure"/>
            <jsp:param name="ErrorHeading" value="Connection Problem"/>
            <jsp:param name="ErrorMsg" value="Error : Network Connection Failed, Please contact the System Administrator"/>
        </jsp:forward>
        <%
            }

            // checking if current user is a valid user in the Active Directory
            check = ad.isValidUser(UserID, Password);
            //if username is not a valid user, then redirect to error page
            if (check == false) {
                ad.takeDown();
        %>
        <jsp:forward page="ErrorMsg.jsp">
            <jsp:param name="ErrorTitle" value="Invalid username and password"/>
            <jsp:param name="ErrorHeading" value="Invalid username and password"/>
            <jsp:param name="ErrorMsg" value="2 User and Password do not match, Please try another one"/>
        </jsp:forward>
        <%
            }
            boolean isConnected2sql = connection.connect();
            //try to connect if failed, redirect to error page
            if (isConnected2sql == false) {
                connection.takeDown();
        %>
        <jsp:forward page="ErrorMsg.jsp">
            <jsp:param name="ErrorTitle" value="Connection Failure"/>
            <jsp:param name="ErrorHeading" value="Connection Problem"/>
            <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator"/>
        </jsp:forward>
        <%
            }

            Vector AllUsers = new Vector();
            AllUsers = ad.getGroups();

            //Getting all the groups of user
            String[] AllGroups = ad.getGroupsOfUser(UserID);

            SQL = "SELECT distinct user_role FROM AD_ROLE_VIEW WHERE lower(login_id)=lower('" + UserID + "')";

            for (int i = 0; i < AllGroups.length; i++) {
                if (!AllGroups[i].equals("")) {
                    SQL = SQL + " OR login_id='" + AllGroups[i] + "'";
                }
            }

            int Count = connection.queryExecuteCount(SQL);

            if (Count > 0) {
                connection.queryExecute(SQL);

                while (connection.toNext()) {
                    CurrUserRole = connection.getColumnS("user_role");
                    CurrUserName = connection.getColumnS("NAME");
                    if (CurrUserRole.equalsIgnoreCase("Administrator")) {
                        IsAdministrator = true;
                    } else if (CurrUserRole.equalsIgnoreCase("Supervisor")) {
                        IsSupervisor = true;
                    } else if (CurrUserRole.equalsIgnoreCase("Operator")) {
                        IsOperator = true;
                    }

                }
            } else {
                connection.takeDown();
                ad.takeDown();
        %>
        <jsp:forward page="ErrorMsg.jsp">
            <jsp:param name="ErrorTitle" value="Configuration Failure"/>
            <jsp:param name="ErrorHeading" value="User Group Configuration Problem"/>
            <jsp:param name="ErrorMsg" value="Error : User is not authenticated for this system, Please contact the System Administrator"/>
        </jsp:forward>
        <%
            }

        } else {
            boolean isConnected2sql = connection.connect();

            //try to connect if failed, redirect to error page
            if (isConnected2sql == false) {
                connection.takeDown();
        %>
        <jsp:forward page="ErrorMsg.jsp">
            <jsp:param name="ErrorTitle" value="Connection Failure"/>
            <jsp:param name="ErrorHeading" value="Connection Problem"/>
            <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator"/>
        </jsp:forward>
        <%
            }

            SQL = "SELECT * FROM ROLE_VIEW WHERE lower(login_id)=lower('" + UserID + "') AND user_pass ='" + Password + "'";
            int Count = connection.queryExecuteCount(SQL);

            if (Count > 0) {
                connection.queryExecute(SQL);

                while (connection.toNext()) {
                    CurrUserRole = connection.getColumnS("user_role");
                    CurrUserName = connection.getColumnS("NAME");
                    if (CurrUserRole.equalsIgnoreCase("Administrator")) {
                        IsAdministrator = true;
                    } else if (CurrUserRole.equalsIgnoreCase("Supervisor")) {
                        IsSupervisor = true;
                    } else if (CurrUserRole.equalsIgnoreCase("Operator")) {
                        IsOperator = true;
                    }
                }

            } else {
                connection.takeDown();
        %>
        <jsp:forward page="ErrorMsg.jsp">
            <jsp:param name="ErrorTitle" value="Invalid username and password"/>
            <jsp:param name="ErrorHeading" value="Invalid username and password"/>
            <jsp:param name="ErrorMsg" value="1 User and Password do not match, Please try another one"/>
        </jsp:forward>
        <%
                }
            }
            session.setAttribute("UserName", UserID);
            session.setAttribute("FullName", CurrUserName);

            if (IsAdministrator) {
                session.setAttribute("UserRole", "Administrator");
            } else if (IsSupervisor) {
                session.setAttribute("UserRole", "Supervisor");
            } else if (IsOperator) {
                session.setAttribute("UserRole", "Operator");
            }

            session.setMaxInactiveInterval( 5*60);

            String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Logged In')";
            boolean ub = connection.procedureExecute(ulog);

            connection.takeDown();
        %>
        <script language="JavaScript">
            location = '<%=request.getContextPath()%>/main.jsp';
        </script></body>
</html>
