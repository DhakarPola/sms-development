<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : June 2006
'Page Purpose  : Comparison Report of Market Share Price.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Market Comparison</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
//Execute while click on Submit
function SubmitThis() {
  count = 0;

  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

    String MarketName = String.valueOf(request.getParameter("marketselect"));

    if (MarketName.equalsIgnoreCase("Others"))
    {
      MarketName = String.valueOf(request.getParameter("omname"));
    }

    String StartDate = String.valueOf(request.getParameter("datefrom"));
    String EndDate = String.valueOf(request.getParameter("dateto"));

    int countrecords = 0;

    String query1 = "SELECT * FROM SHARE_PRICE_VIEW WHERE MARKET = '" + MarketName + "'";
    countrecords = cm.queryExecuteCount(query1);

    if (countrecords == 0)
    {
     %>
       <script language="javascript">
        alert("No Market with this name exists in the Database!\nPlease check again!");
        history.go(-1);
       </script>
     <%
    }

    countrecords = 0;

    query1 = "SELECT * FROM SHARE_PRICE_VIEW WHERE MARKET = '" + MarketName + "' AND ENTRY_DATE >= TO_DATE('" + StartDate + "','DD/MM/YYYY') AND ENTRY_DATE <= TO_DATE('" + EndDate + "','DD/MM/YYYY')";
    countrecords = cm.queryExecuteCount(query1);

    if (countrecords == 0)
    {
     %>
       <script language="javascript">
        alert("No Records with this combination exist in the Database!\nPlease check again!");
        history.go(-1);
       </script>
     <%
    }

    double movement = 0;
    double maxprice = 0;
    double minprice = 0;
    double avgprice = 0;
    double maxindex = 0;
    double minindex = 0;
    double avgindex = 0;
    double indexmovement = 0;

    query1 = "SELECT AVG(to_Number(AVG_PRICE)) as TUNIT FROM SHARE_PRICE_VIEW WHERE MARKET = '" + MarketName + "' AND ENTRY_DATE >= TO_DATE('" + StartDate + "','DD/MM/YYYY') AND ENTRY_DATE <= TO_DATE('" + EndDate + "','DD/MM/YYYY')";
    cm.queryExecute(query1);
    cm.toNext();
    avgprice = cm.getColumnF("TUNIT");

    query1 = "SELECT MAX(to_Number(MAX_PRICE)) as TMAX FROM SHARE_PRICE_VIEW WHERE MARKET = '" + MarketName + "' AND ENTRY_DATE >= TO_DATE('" + StartDate + "','DD/MM/YYYY') AND ENTRY_DATE <= TO_DATE('" + EndDate + "','DD/MM/YYYY')";
    cm.queryExecute(query1);
    cm.toNext();
    maxprice = cm.getColumnF("TMAX");

    query1 = "SELECT MIN(to_Number(MIN_PRICE)) as TMIN FROM SHARE_PRICE_VIEW WHERE MARKET = '" + MarketName + "' AND ENTRY_DATE >= TO_DATE('" + StartDate + "','DD/MM/YYYY') AND ENTRY_DATE <= TO_DATE('" + EndDate + "','DD/MM/YYYY')";
    cm.queryExecute(query1);
    cm.toNext();
    minprice = cm.getColumnF("TMIN");

    query1 = "SELECT SUM(to_Number(MOVEMENT)) as TMOV FROM SHARE_PRICE_VIEW WHERE MARKET = '" + MarketName + "' AND ENTRY_DATE >= TO_DATE('" + StartDate + "','DD/MM/YYYY') AND ENTRY_DATE <= TO_DATE('" + EndDate + "','DD/MM/YYYY')";
    cm.queryExecute(query1);
    cm.toNext();
    movement = cm.getColumnF("TMOV");

    query1 = "SELECT AVG(to_Number(SHARE_INDEX)) as TAINDEX FROM SHARE_PRICE_VIEW WHERE MARKET = '" + MarketName + "' AND ENTRY_DATE >= TO_DATE('" + StartDate + "','DD/MM/YYYY') AND ENTRY_DATE <= TO_DATE('" + EndDate + "','DD/MM/YYYY')";
    cm.queryExecute(query1);
    cm.toNext();
    avgindex = cm.getColumnF("TAINDEX");

    query1 = "SELECT MAX(to_Number(SHARE_INDEX)) as TMXINDEX FROM SHARE_PRICE_VIEW WHERE MARKET = '" + MarketName + "' AND ENTRY_DATE >= TO_DATE('" + StartDate + "','DD/MM/YYYY') AND ENTRY_DATE <= TO_DATE('" + EndDate + "','DD/MM/YYYY')";
    cm.queryExecute(query1);
    cm.toNext();
    maxindex = cm.getColumnF("TMXINDEX");

    query1 = "SELECT MIN(to_Number(SHARE_INDEX)) as TMNINDEX FROM SHARE_PRICE_VIEW WHERE MARKET = '" + MarketName + "' AND ENTRY_DATE >= TO_DATE('" + StartDate + "','DD/MM/YYYY') AND ENTRY_DATE <= TO_DATE('" + EndDate + "','DD/MM/YYYY')";
    cm.queryExecute(query1);
    cm.toNext();
    minindex = cm.getColumnF("TMNINDEX");

    query1 = "SELECT SUM(to_Number(INDEX_MOVEMENT)) as TIMOV FROM SHARE_PRICE_VIEW WHERE MARKET = '" + MarketName + "' AND ENTRY_DATE >= TO_DATE('" + StartDate + "','DD/MM/YYYY') AND ENTRY_DATE <= TO_DATE('" + EndDate + "','DD/MM/YYYY')";
    cm.queryExecute(query1);
    cm.toNext();
    indexmovement = cm.getColumnF("TIMOV");

    movement = bcal.roundtovalue(movement,2);
    maxprice = bcal.roundtovalue(maxprice,2);
    minprice = bcal.roundtovalue(minprice,2);
    avgprice = bcal.roundtovalue(avgprice,2);
    maxindex = bcal.roundtovalue(maxindex,2);
    minindex = bcal.roundtovalue(minindex,2);
    avgindex = bcal.roundtovalue(avgindex,2);
    indexmovement = bcal.roundtovalue(indexmovement,2);
%>

<form action="UpdateSharePrice.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Market Comparison</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Market</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8"><b><%=MarketName%></b></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Period From</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8"><b><%=StartDate%></b></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Period To</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8"><b><%=EndDate%></b></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Average Price</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8"><b><%=avgprice%></b></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Maximum Price</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8"><b><%=maxprice%></b></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Minimum Price</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8"><b><%=minprice%></b></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Price Movement</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8"><b><%=movement%></b></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Average Index</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8"><b><%=avgindex%></b></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Maximum Index</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8"><b><%=maxindex%></b></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Minimum Index</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8"><b><%=minindex%></b></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Index Movement</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8"><b><%=indexmovement%></b></div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="100%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="100%" scope="row">
            <div align="center">
 		<img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="history.go(-1)" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
            </div>
        </th>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
