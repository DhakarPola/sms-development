<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Updated by    : Md. Kamruzzaman
'Creation Date : February 2006
'Page Purpose  : Save New Share Price Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Share Price Information</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

  String SStartingValue = String.valueOf(request.getParameter("StartValue"));
  String SEndingValue = String.valueOf(request.getParameter("EndValue"));

  String Market = String.valueOf(request.getParameter("marketselect"));
  String OtherMarket = String.valueOf(request.getParameter("omname"));
  String EntryDate = String.valueOf(request.getParameter("entrydate"));
  String UnitPrice = String.valueOf(request.getParameter("uprice"));
  String Movement = String.valueOf(request.getParameter("movement"));
  String MaxPrice = String.valueOf(request.getParameter("maxprice"));
  String MinPrice = String.valueOf(request.getParameter("minprice"));
  String AvgPrice = String.valueOf(request.getParameter("avgprice"));
  String IIndex = String.valueOf(request.getParameter("iiindex"));
  String IMovement = String.valueOf(request.getParameter("imovement"));
  String shareTraded = String.valueOf(request.getParameter("shareTraded"));

  OtherMarket=cm.replace(OtherMarket,"'","''");
  Movement=cm.replace(Movement,"'","''");
  IIndex=cm.replace(IIndex,"'","''");
  IMovement=cm.replace(IMovement,"'","''");

  if (String.valueOf(OtherMarket).equals("null"))
  {
    OtherMarket = "";
  }
  if (!OtherMarket.equalsIgnoreCase(""))
  {
    Market = OtherMarket;
  }

  double iUnitPrice = Double.parseDouble(UnitPrice);
  double iMovement = Double.parseDouble(Movement);
  double iMaxPrice = Double.parseDouble(MaxPrice);
  double iMinPrice = Double.parseDouble(MinPrice);
  double iAvgPrice = Double.parseDouble(AvgPrice);
  double iIIndex = Double.parseDouble(IIndex);
  double iIMovement = Double.parseDouble(IMovement);

  iUnitPrice = bcal.roundtovalue(iUnitPrice,2);
  iMovement = bcal.roundtovalue(iMovement,2);
  iMaxPrice = bcal.roundtovalue(iMaxPrice,2);
  iMinPrice = bcal.roundtovalue(iMinPrice,2);
  iAvgPrice = bcal.roundtovalue(iAvgPrice,2);
  iIIndex = bcal.roundtovalue(iIIndex,2);
  iIMovement = bcal.roundtovalue(iIMovement,2);

   if(String.valueOf(shareTraded).equals("null"))
   shareTraded="0";

  String nprice = "SELECT * FROM SHARE_PRICE_VIEW WHERE MARKET = '" + Market + "' AND ENTRY_DATE = TO_DATE('" + EntryDate + "','DD/MM/YYYY')";
  int recordexists = cm.queryExecuteCount(nprice);

  if (recordexists > 0)
  {
   %>
    <script language="javascript">
     alert("The Record already exists!");
     </script>
   <%
  }
  else
  {
    nprice = "call ADD_SHARE_PRICE('" + Market + "', '" + EntryDate + "', " + iUnitPrice + ", " + iMovement + ", " + iMaxPrice + ", " + iMinPrice + "," + iAvgPrice + "," + iIIndex + "," + iIMovement + ",'" + shareTraded + "')";
    boolean b = cm.procedureExecute(nprice);
  }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added Share Price Information')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Others/MarketSharePrice.jsp?StartValue=<%=SStartingValue%>&EndValue=<%=SEndingValue%>";
</script>
</body>
</html>
