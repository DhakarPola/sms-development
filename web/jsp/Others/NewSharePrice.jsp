<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Updated by    : Md. Kamruzzaman
'Creation Date : February 2006
'Page Purpose  : Adds a Share Price Information.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>New Share Price Information</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function formconfirm()
{
  if (confirm("Do you want to enter the mentioned Information?"))
  {
    return true;
  }
}

function setmarketvalues(mvalue)
{
  if (mvalue == "Others")
  {
   document.all.marketspan1.style.display = '';
   document.all.marketspan2.style.display = '';
   document.all.marketspan3.style.display = '';
  }
  else
  {
   document.all.marketspan1.style.display = 'none';
   document.all.marketspan2.style.display = 'none';
   document.all.marketspan3.style.display = 'none';
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  SelectValidate('marketselect','- Market Name (Option must be entered)');
  if (document.all.marketselect.value == "Others")
  {
    BlankValidate('omname','- New Market Name (Option must be entered)');
  }

  BlankValidate('entrydate','- Entry Date (Option must be entered)');
  BlankValidate('uprice','- Unit Price (Option must be entered)');
  BlankValidate('movement','- Movement (Option must be entered)');
  BlankValidate('maxprice','- Maximum Price (Option must be entered)');
  BlankValidate('minprice','- Minimum Price (Option must be entered)');
  BlankValidate('avgprice','- Avgerage Price (Option must be entered)');
  BlankValidate('iiindex','- Index (Option must be entered)');
  BlankValidate('imovement','- Index Movement (Option must be entered)');
  BlankValidate('shareTraded','- No. Share Traded (Option must be entered)');

  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

    String SStartingValue = String.valueOf(request.getParameter("StartValue"));
    String SEndingValue = String.valueOf(request.getParameter("EndValue"));
%>

<form action="SaveSharePrice.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">New Share Price Information</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Market</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
         <select name="marketselect" class="SL1TextFieldListBox" onchange="setmarketvalues(document.all.marketselect.value)">
            <option>--- Please Select ---</option>
            <option value="CDBL">CDBL</option>
            <option value="CSE">CSE</option>
            <option value="DSE">DSE</option>
            <option value="SEC">SEC</option>
            <option value="Others">Others</option>
         </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8"><SPAN id="marketspan1" style="display:none">&nbsp;&nbsp;&nbsp;&nbsp;Enter Market Name</span>
        </div></th>
        <td><SPAN id="marketspan2" style="display:none">:</span></td>
        <td><div align="left">
          <SPAN id="marketspan3" style="display:none"><input type="text" name="omname" class="SL2TextField"></span>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date</div></th>
        <td width="1%" valign="top">:</td>
        <td><div align="left">
         <input name="entrydate" type=text id="entrydate" value="" maxlength="10" class="SL2TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('entrydate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Unit Price</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="uprice" class="SL2TextField" onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Movement</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="movement" class="SL2TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Maximum Price</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="maxprice" class="SL2TextField" onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Minimum Price</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="minprice" class="SL2TextField" onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Average Price</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="avgprice" class="SL2TextField" onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Index</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="iiindex" class="SL2TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Index Movement</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="imovement" class="SL2TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;No. of Share Traded</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="shareTraded" class="SL2TextField" onkeypress="keypressOnNumberFld()">
        </div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="34%" scope="row">
            <div align="right">
 		    <img name="B3" src="<%=request.getContextPath()%>/images/btnBack.gif"  onclick="history.go(-1)" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnBackOn.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnBack.gif'">
            </div></th>
        <td width="33%" align="center">
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
      </td>
        <td width="33%">
	          <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
      </td>
      </tr>
</table>

   <!--HIDDEN FIELDS-->
    <input type="hidden" name="StartValue" value="<%=SStartingValue%>">
    <input type="hidden" name="EndValue" value="<%=SEndingValue%>">

</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
