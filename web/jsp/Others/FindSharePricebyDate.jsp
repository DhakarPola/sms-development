<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : February 2006
'Page Purpose  : Finds Market Share Information by Date.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Find Market Share Price by Date</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String EntryDate = String.valueOf(request.getParameter("EDate"));
  String Chunk = String.valueOf(request.getParameter("ChunkSize"));
  String StartValue = String.valueOf(request.getParameter("StartingValue"));
  String EndValue = String.valueOf(request.getParameter("EndingValue"));

  int iChunk = Integer.parseInt(Chunk);

  String S1 = "SELECT * FROM SHARE_PRICE_VIEW";
  int countsprecords = cm.queryExecuteCount(S1);
  String S2 = "SELECT * FROM SHARE_PRICE_VIEW WHERE ENTRY_DATE = TO_DATE('" + EntryDate + "','DD/MM/YYYY')";
  int countsprecords1 = cm.queryExecuteCount(S2);

  if (countsprecords1 > 0)
  {
   String fdate = "SELECT rnum FROM (SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM SHARE_PRICE_VIEW ORDER BY ENTRY_DATE DESC) div where rownum <= " + countsprecords + ") where rnum >= 1) WHERE ENTRY_DATE = TO_DATE('" + EntryDate + "','DD/MM/YYYY')";
   cm.queryExecute(fdate);
   cm.toNext();
   int RowNumber = cm.getColumnI("rnum");

   int SValue = RowNumber;
   int EValue = RowNumber + iChunk;

   if (EValue > countsprecords)
   {
     EValue = countsprecords;
   }

   StartValue = String.valueOf(SValue);
   EndValue = String.valueOf(EValue);
  }

  if (isConnect)
   {
     cm.takeDown();
   }
%>

 <script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Others/MarketSharePrice.jsp?StartValue=<%=StartValue%>&EndValue=<%=EndValue%>";
 </script>

</body>
</html>
