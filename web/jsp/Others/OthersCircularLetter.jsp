<script>
/*
'******************************************************************************************************************************************
'Script Author : Masud Ahmad
'Updated By    : Renad Hakim
'Creation Date : Jan 2006
'Page Purpose  : Circular Letter
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Circular Letter</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
//Execute while click on Submit
function SubmitThis() {
  count = 0;
  SelectValidate('LetterTemplate','- Letter Template (Option must be entered)');
  SelectValidate('CLOfCategory','- Shareholder Category (Option must be entered)');
  BlankValidate('CLGreaterThan','- Having Shares Greater Than (Option must be entered)');
  BlankValidate('CLLessThan','- Having Shares Less Than (Option must be entered)');
  BlankValidate('CLFrom','- Folio Greater Than (Option must be entered)');
  BlankValidate('CLTo','- Folio Less Than (Option must be entered)');

  if (count == 0)
  {
    document.forms[0].submit();
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
%>

<form action="ExportCircularLetter.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Curcular Letter</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Type</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left" class="style9">
            <input name="Type" type="radio" value="SMS" checked="checked">&nbsp;SMS<br>
            <input name="Type" type="radio" value="CDBL">&nbsp;CDBL
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Letter Template</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <select name="LetterTemplate" class="SL1TextFieldListBox" >
            <option>--- Please Select ---</option>
            <%
              String LName = "";

              String query1 = "SELECT LETTER_NAME FROM LETTER ORDER BY LETTER_ID";
              cm.queryExecute(query1);

              while(cm.toNext())
              {
                 LName = cm.getColumnS("LETTER_NAME");
                 %>
            <option value="<%=LName%>"><%=LName%></option>
                 <%
              }
            %>
          </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Send to Shareholders of Category</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <select name="CLOfCategory" class="SL1TextFieldListBox">
          <option selected>--- Please Select ---</option>
          <%
              String theCategory = "";

              String query2 = "SELECT CATEGORY,CODE FROM CATEGORY ORDER BY CATEGORY";
              cm.queryExecute(query2);

              while(cm.toNext())
              {
                 theCategory = cm.getColumnS("CATEGORY");

                 %>
          <option value="<%=cm.getColumnS("CODE")%>"><%=theCategory%></option>
          <%
              }
            %>
        </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Nationality</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left" class="style9">
            <input name="Nationality" type="radio" value="All" checked="checked">&nbsp;All<br>
            <input name="Nationality" type="radio" value="Bangladeshi">&nbsp;Bangladeshi<br>
            <input name="Nationality" type="radio" value="Not Bangladeshi">&nbsp;Not Bangladeshi
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Residence</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left" class="style9">
            <input name="Residence" type="radio" value="All" checked="checked">&nbsp;All<br>
            <input name="Residence" type="radio" value="Resident">&nbsp;Resident<br>
            <input name="Residence" type="radio" value="Non Resident">&nbsp;Non Resident
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Having Shares</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left">
         <table width="75%"  border="0" cellpadding="2">
           <tr>
            <td width="30%" align="left"><div align="left" class="style8">
              <b>From </b></div>
            </td>
            <td width="70%" align="left"><div align="left" class="style8">
              <input type="text" name="CLGreaterThan" class="SL67TextField" onkeypress="keypressOnNumberFld()" value="1"></div>
            </td>
           </tr>
           <tr>
            <td width="30%" align="left"><div align="left" class="style8">
              <b>To </b></div>
            </td>
            <td width="70%" align="left"><div align="left" class="style8">
              <input type="text" name="CLLessThan" class="SL67TextField" onkeypress="keypressOnNumberFld()" value="60000000"></div>
            </td>
           </tr>
        </table>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Having Folio</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left">
         <table width="75%"  border="0" cellpadding="2">
           <tr>
            <td width="30%" align="left"><div align="left" class="style8">
              <b>From </b></div>
            </td>
            <td width="70%" align="left"><div align="left" class="style8">
              <input type="text" name="CLFrom" class="SL67TextField" onkeypress="keypressOnNumberFld()"></div>
            </td>
           </tr>
           <tr>
            <td width="30%" align="left"><div align="left" class="style8">
              <b>To </b></div>
            </td>
            <td width="70%" align="left"><div align="left" class="style8">
              <input type="text" name="CLTo" class="SL67TextField" onkeypress="keypressOnNumberFld()"></div>
            </td>
           </tr>
        </table>
        </div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="10" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <td width="50%" align="right">
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnExport.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnExportR.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnExport.gif'">
      </td>
        <td width="50%">
	          <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
      </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
