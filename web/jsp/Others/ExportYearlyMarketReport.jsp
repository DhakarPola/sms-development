<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : August 2007
'Page Purpose  : Exports Yearly Share Market Report to Excel Format.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cms"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Yearly Market Report</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<%@ page language="java" import="org.apache.poi.hssf.usermodel.*,java.util.Vector,java.io.*,java.util.*,java.util.List,java.util.Vector" %>
<script language="javascript">
<!--
//Execute while click on Submit
function SubmitThis() {
  count = 0;

  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cms.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String dyear = String.valueOf(request.getParameter("dyear"));

    String earningPartial="";
    String q1="select amount from view_earning_partial where dec_year='" + dyear + "'";
    try
    {
      cm.queryExecute(q1);
      cm.toNext();
      earningPartial=cm.getColumnS("amount");
    }
    catch(Exception e){}
    String StartDate = "1/1/"+dyear;
    String EndDate = "31/12/"+dyear;
    //System.out.println(StartDate);
    //System.out.println(EndDate);

      // Add Excel Export Code Here.
	  //String Marketselect = String.valueOf(request.getParameter("marketselect"));

          HSSFWorkbook wb = new HSSFWorkbook();
          HSSFSheet sheet = wb.createSheet("Yearly Market Report");


            // Create a row and put some cells in it. Rows are 0 based.
            HSSFRow row = sheet.createRow((short)0);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("Yearly Market Report");
            row = sheet.createRow((short)1);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("Period From : "+StartDate);

            row = sheet.createRow((short)2);
            row.createCell((short)0).setCellValue("");
            row.createCell((short)1).setCellValue("Period To : "+EndDate);

            row = sheet.createRow((short)3);
            row.createCell((short)0).setCellValue("");

            String MARKET = "";
            String ENTRY_DATE = "";
            double UNIT_PRICE = 0;
            double MOVEMENT = 0;
            double MAX_PRICE = 0;
            double MIN_PRICE = 0;
            double AVG_PRICE = 0;
            double SHARE_INDEX =0;
            double INDEX_MOVEMENT = 0;


            row = sheet.createRow((short)6);
            HSSFRow row1 = sheet.createRow((short)5);

            String market_q="SELECT Distinct MARKET FROM SHARE_PRICE_View order by UPPER(MARKET)";
            int numOfMarket=cm.queryExecuteCount(market_q);
            for(int l=0;l<numOfMarket;l++)
            {
              row.createCell((short)(10*l+0)).setCellValue("");
              row.createCell((short)(10*l+1)).setCellValue("Entry Date");
              //row.createCell((short)(10*l+2)).setCellValue("Unit Price");
              row.createCell((short)(10*l+2)).setCellValue("Movement");
              row.createCell((short)(10*l+3)).setCellValue("Max Price");
              row.createCell((short)(10*l+4)).setCellValue("Min Price");
              row.createCell((short)(10*l+5)).setCellValue("Avg Price");
              row.createCell((short)(10*l+6)).setCellValue("Share Index");
              row.createCell((short)(10*l+7)).setCellValue("Avg Index");
              row.createCell((short)(10*l+8)).setCellValue("Index Movement");
              row.createCell((short)(10*l+9)).setCellValue("Eearning Partial");
            }

            try
            {
              cm.queryExecute(market_q);
              int loop=0;
              while(cm.toNext())
              {
                String market_Name=cm.getColumnS("MARKET");
                row1.createCell((short)(10*loop+4)).setCellValue("Market : "+market_Name);
                loop++;
              }
            }catch(Exception e){}

            // Create a cell and put a value in it.

            cm.queryExecute(market_q);
            int loop=0;
            int P_numberOfDate=0;
            int i=7;
            int rowCount=0;
            HSSFRow rowi[] = new HSSFRow[50];
            while(cm.toNext())
            {
             String market_Name=cm.getColumnS("MARKET");
             String avgIndex="SELECT nvl(sum(SHARE_INDEX),0)as totalIndex FROM SHARE_PRICE_View where MARKET='"+market_Name+"' and (ENTRY_DATE BETWEEN TO_DATE('"+StartDate+"', 'DD/MM/YYYY') AND TO_DATE('"+EndDate+"', 'DD/MM/YYYY')) Order By ENTRY_DATE";


             String SelectQuery="SELECT MARKET,ENTRY_DATE,UNIT_PRICE,\"MOVEMENT\",MAX_PRICE,MIN_PRICE,AVG_PRICE,SHARE_INDEX,INDEX_MOVEMENT FROM SHARE_PRICE_View where MARKET='"+market_Name+"' and (ENTRY_DATE BETWEEN TO_DATE('"+StartDate+"', 'DD/MM/YYYY') AND TO_DATE('"+EndDate+"', 'DD/MM/YYYY')) Order By ENTRY_DATE";
             int numberOfDate=cms.queryExecuteCount(SelectQuery);

             double avg_index=0.0;
             String avg_indexS="";
             String testtwo="";
             try
             {
               cms.queryExecute(avgIndex);
               cms.toNext();
               double totalShareIndex=cms.getColumnF("totalIndex");
               if(numberOfDate!=0)
               {
                 avg_index=totalShareIndex/(double)numberOfDate;
                 avg_index=bcal.roundtovalue(avg_index,2);
                 avg_indexS=String.valueOf(avg_index);
                 //System.out.println("avg_indexS= "+avg_indexS);
                 for (int j=0; j<avg_indexS.length(); j++)
                 {
                   testtwo = avg_indexS.charAt(j) + testtwo;
                 }
           //to add 0 ifone decimal place is found
                  char c=testtwo.charAt(2);
                   if(c!='.')
                    avg_indexS=avg_indexS+"0";
               }
             }
             catch(Exception e){}
             //System.out.println("numberOfDate= "+numberOfDate);
             if(numberOfDate>P_numberOfDate)
             {
               for(int j=P_numberOfDate;j<numberOfDate;j++)
               {
                 rowi[rowCount] = sheet.createRow((short)i);
                 i++;
                // System.out.println("rowCount= "+rowCount);
                 rowCount++;
               }
               P_numberOfDate=numberOfDate;
             }
             rowCount=P_numberOfDate;
            cms.queryExecute(SelectQuery);
            int setRow=0;
            while(cms.toNext())
            {
              MARKET = cms.getColumnS("MARKET");
              ENTRY_DATE = cms.getColumnDT("ENTRY_DATE");
              UNIT_PRICE= cms.getColumnF("UNIT_PRICE");
              MOVEMENT = cms.getColumnF("MOVEMENT");
              MAX_PRICE= cms.getColumnF("MAX_PRICE");
              MIN_PRICE= cms.getColumnF("MIN_PRICE");
              AVG_PRICE = cms.getColumnF("AVG_PRICE");
              SHARE_INDEX = cms.getColumnF("SHARE_INDEX");
              INDEX_MOVEMENT = cms.getColumnF("INDEX_MOVEMENT");


              rowi[setRow].createCell((short)(10*loop+0)).setCellValue("");
              rowi[setRow].createCell((short)(10*loop+1)).setCellValue(ENTRY_DATE);
              //rowi[setRow].createCell((short)(10*loop+2)).setCellValue(UNIT_PRICE);
              rowi[setRow].createCell((short)(10*loop+2)).setCellValue(MOVEMENT);
              rowi[setRow].createCell((short)(10*loop+3)).setCellValue(MAX_PRICE);
              rowi[setRow].createCell((short)(10*loop+4)).setCellValue(MIN_PRICE);
              rowi[setRow].createCell((short)(10*loop+5)).setCellValue(AVG_PRICE);
              rowi[setRow].createCell((short)(10*loop+6)).setCellValue(SHARE_INDEX);
              rowi[setRow].createCell((short)(10*loop+7)).setCellValue(avg_indexS);
              rowi[setRow].createCell((short)(10*loop+8)).setCellValue(INDEX_MOVEMENT);
              rowi[setRow].createCell((short)(10*loop+9)).setCellValue(earningPartial);
              setRow++;
             }
             loop++;
            }

             // Write the output to a file
             String savepath="";
             ServletContext sc = getServletConfig().getServletContext();
          //Code for creating a folder
           String path=getServletContext().getRealPath("/");
           File f1 = new File(path + "/Excel Export");
           if(!f1.exists())
           {
                 f1.mkdir();
           }
           savepath = sc.getRealPath("/") +"Excel Export/"+"MarketReport.xls";

                FileOutputStream fileOut = new FileOutputStream(savepath);
                wb.write(fileOut);
                fileOut.close();
                fileOut=null;
                sheet=null;
                wb=null;
         //End Excel Export
        String tempURL =  request.getContextPath() +"/Excel Export/"+"MarketReport.xls";
        %>
<script language="javascript">
    showPrintPreview6('<%=tempURL%>');
    location = "<%=request.getContextPath()%>/jsp/Others/YearlyMarketReport.jsp";
</script>



<form action="SeeMarketReport.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Generate Market Report</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">


      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Market</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8"><b></b></div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="100%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="100%" scope="row">
            <div align="center">
 		<img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="history.go(-1)" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
            </div>
        </th>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
     cms.takeDown();
   }
%>
</body>
</html>
