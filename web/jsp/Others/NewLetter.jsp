<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : December 2005
'Page Purpose  : Enters New Letter Information.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}
%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>New Letter</title>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('lettername','- Name of Letter (Option must be entered)');
  BlankValidate('lettersubject','- Subject (Option must be entered)');
  BlankValidate('letterbody','- Body (Option must be entered)');
  SelectValidate('sigselect','- Signature (Option must be entered)');
  if (count == 0)
  {
    document.forms[0].submit();
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
%>

<form action="SaveLetter.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">New Letter</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Name of Letter</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="lettername" class="SL68TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Subject</div></th>
        <td valign="top">:</td>
        <td><div align="left">
          <textarea name="lettersubject" cols="2" wrap="VIRTUAL" class="ML10TextField" ></textarea>
        </div></td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Body</div></th>
        <td valign="top">:</td>
        <td><div align="left">
          <textarea name="letterbody" cols="2" wrap="VIRTUAL" class="ML10TextField" ></textarea>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Signature</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="sigselect" class="SL2TextFieldListBox">
            <option>--- Please Select ---</option>
            <%
              String signame = "";
              String sigid = "";
              String sigrank = "";

              String query2 = "SELECT * FROM SIGNATURE_VIEW ORDER BY SIG_ID";
              cm.queryExecute(query2);

              while(cm.toNext())
              {
                 signame = cm.getColumnS("NAME");
                 sigid = cm.getColumnS("SIG_ID");
                 sigrank = cm.getColumnS("RANK");
                 %>
                   <option value="<%=sigid%>"><%=signame%> (<%=sigrank%>)</option>
                 <%
              }
            %>
         </select>
        </div></td>
      </tr>
  </table>
  <br>
      </div>

      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="34%" scope="row">
            <div align="right">
 		    <img name="B3" src="<%=request.getContextPath()%>/images/btnBack.gif"  onclick="history.go(-1)" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnBackOn.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnBack.gif'">
            </div></th>
        <td width="33%" align="center">
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
      </td>
        <td width="33%">
	          <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
      </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
