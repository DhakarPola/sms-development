<script>
/*
'******************************************************************************************************************************************
'Script Author : Masud Ahmad
'Creation Date : Feb 2006
'Page Purpose  : Edits Signature Verification Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.io.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*"%>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<jsp:useBean id="util" class="batbsms.Utility"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css">
</LINK><%if (String.valueOf(session.getAttribute("UserName")).equals("null")) {%>
<script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%}%>
<html>
<head>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datepicker.js"></SCRIPT>
<script language="javascript">
<!--
function editSignatureWindow()
{
  	window.open("EditSignature.jsp","","HEIGHT=350,WIDTH=500")
}
function editPhotoWindow()
{
        window.open("EditPhoto.jsp","","HEIGHT=350,WIDTH=500")
}
function openNomineeWindow()
{
	window.open("NomineeList.jsp","","HEIGHT=500,WIDTH=500,SCROLLBAR=YES")
}
function openJoint1Edit()
{

      var windowname = "EditJoint1.jsp?hfoliono=" + document.forms[0].hfoliono.value+" &dbstatus="+document.forms[0].dbstatus.value;

      var win = window.open(windowname,"","HEIGHT=700,WIDTH=500");

      return win;
}
function openJoint2Edit()
{

      var windowname = "EditJoint2.jsp?hfoliono=" + document.forms[0].hfoliono.value+" &dbstatus="+document.forms[0].dbstatus.value;

      var win = window.open(windowname,"","HEIGHT=700,WIDTH=500");

      return win;
}

//-->
</script>
<%
  boolean isConnect = cm.connect();
  if (isConnect == false) {
%>
<jsp:forward page="ErrorMsg.jsp">
  <jsp:param name="ErrorTitle" value="Connection Failure"/>
  <jsp:param name="ErrorHeading" value="Connection Problem"/>
  <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator"/>
</jsp:forward>
<%
  }
  String sfolioID = request.getParameter("fno");

  int joint=0;

  String category = "";
  String prefix = "";
  String shareholdername = "";
  String surname = "";
  String relation = "";
  String father = "";
  String resident = "";
  String nationality = "";
  String occupation = "";
  String address1 = "";
  String address2 = "";
  String zipcode = "";
  String telephone = "";
  String cellphone = "";
  String email = "";
  String fax = "";
  String address3 = "";
  String address4 = "";
  String accountno = "";
  String bankname = "";
  String bankbranch = "";
  String tin = "";
  String taxfreeshare = "";
  String entrydate = "";
  String nomineeno = "";
  String dbstatus = request.getParameter("dbstatus");
  String tablename="";
  int total_shares=0;


  String jointprefix ="";
  String jointname = "";
  String jointrelation = "";
  String jointfather = "";
  String jointoccupation = "";
  String jointtelephone = "";
  String jointfax = "";
  String jointemail = "";
  String jointaddress = "";
  String jointaddress2 = "";
  String jointaddress3 = "";
  String jointaddress4 = "";
  String jointcontactperson = "";
  String jointremarks = "";



  String joint2prefix ="";
  String joint2name = "";
  String joint2relation = "";
  String joint2father = "";
  String joint2occupation = "";
  String joint2telephone = "";
  String joint2fax = "";
  String joint2email = "";
  String joint2address = "";
  String joint2address2 = "";
  String joint2address3 = "";
  String joint2address4 = "";
  String joint2contactperson = "";
  String joint2remarks = "";


  String queryFolio;//SHAREHOLDER_TEMP


    tablename = "SHAREHOLDER";

 queryFolio = "SELECT * FROM " + tablename + " WHERE FOLIO_NO = '" + sfolioID+"'";

  cm.queryExecute(queryFolio);
  while (cm.toNext()) {
    joint  = cm.getColumnI("JOINT");

    category = cm.getColumnS("CLASS");
    prefix = cm.getColumnS("PREFIX");
    shareholdername = cm.getColumnS("NAME");
    surname = cm.getColumnS("SURNAME");
    relation = cm.getColumnS("RELATION");
    father = cm.getColumnS("FATHER");
    resident = cm.getColumnS("RESIDENT");
    nationality = cm.getColumnS("NATIONALITY");
    occupation = cm.getColumnS("OCCUPATION");
    address1 = cm.getColumnS("ADDRESS1");
    address2 = cm.getColumnS("ADDRESS2");
    zipcode = cm.getColumnS("ZIP");


    telephone = cm.getColumnS("TELEPHONE");
    cellphone = cm.getColumnS("CELLPHONE");
    email = cm.getColumnS("EMAIL");
    fax = cm.getColumnS("FAX");


    address3 = cm.getColumnS("ADDRESS3");
    address4 = cm.getColumnS("ADDRESS4");
    accountno = cm.getColumnS("ACCOUNT_NO");
    bankname = cm.getColumnS("BANK_NAME");
    bankbranch = cm.getColumnS("BANK_BRANCH");
    tin = cm.getColumnS("TIN");
    taxfreeshare = cm.getColumnS("TAX_FREE_SHARE");
    entrydate = cm.getColumnS("ENTRY_DATE");

    total_shares = cm.getColumnI("TOTAL_SHARE");

    if(entrydate!=null)
    {
      entrydate = util.changeDateFormat(entrydate,"MM/dd/yyyy h:m:s","dd/MM/yyyy");
    }


    nomineeno = cm.getColumnS("NOMINEE_NO");
    tablename="";


    if (String.valueOf(category).equals("null"))
          category = "";
    if (String.valueOf(prefix).equals("null"))
          prefix = "";
    if (String.valueOf(shareholdername).equals("null"))
         shareholdername  = "";
    if (String.valueOf(surname).equals("null"))
          surname = "";
    if (String.valueOf(relation).equals("null"))
          relation = "";
    if (String.valueOf(father).equals("null"))
          father = "";
    if (String.valueOf(resident).equals("null"))
           resident= "";
    if (String.valueOf(nationality).equals("null"))
         nationality  = "";
    if (String.valueOf(occupation).equals("null"))
        occupation   = "";
    if (String.valueOf(address1).equals("null"))
         address1  = "";
    if (String.valueOf(zipcode).equals("null"))
          zipcode = "";
    if (String.valueOf(telephone).equals("null"))
          telephone = "";
    if (String.valueOf(accountno).equals("null"))
          accountno = "";
    if (String.valueOf(fax).equals("null"))
          fax = "";
    if (String.valueOf(taxfreeshare).equals("null"))
          taxfreeshare = "";
    if (String.valueOf(bankname).equals("null"))
          bankname = "";
    if (String.valueOf(entrydate).equals("null"))
          entrydate = "";
    if (String.valueOf(nomineeno).equals("null"))
          nomineeno = "";
    if (String.valueOf(bankbranch).equals("null"))
          bankbranch = "";
    if (String.valueOf(cellphone).equals("null"))
          cellphone = "";
   if (String.valueOf(email).equals("null"))
          email = "";
   if (String.valueOf(tin).equals("null"))
          tin = "";
  if (String.valueOf(address2).equals("null"))
          address2 = "";
  if (String.valueOf(address3).equals("null"))
          address3 = "";
  if (String.valueOf(address4).equals("null"))
          address4 = "";

   address1 = address1.trim() + "\n" +address2.trim() + "\n" + address3.trim() + "\n" + address4.trim();

   //************Trims whitespaces************

        category = category.trim();
        prefix = prefix.trim();
        shareholdername  = shareholdername.trim();
        surname = surname.trim();
        relation = relation.trim();
        father = father.trim();
        resident= resident.trim();
        nationality  = nationality.trim();
        occupation   = occupation.trim();
        address1  = address1.trim();
        zipcode = zipcode.trim();
        telephone = telephone.trim();
        accountno = accountno.trim();
        fax = fax.trim();
        taxfreeshare = taxfreeshare.trim();
        bankname = bankname.trim();
        entrydate = entrydate.trim();
        nomineeno = nomineeno.trim();
        bankbranch = bankbranch.trim();
        cellphone = cellphone.trim();
        email = email.trim();
        tin = tin.trim();

   //***********************************************
  }

%>
<style type="text/css">
  <!--
    body {
    background-color: #FFFFFF;
    }
    body,td,th {
    color: #000000;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
    }
  -->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit Bank</title>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css">
</LINK><script language="javascript">
<!--
function gotofolio()
{
  location = "<%=request.getContextPath()%>/jsp/Folio/AllFolios.jsp";
}
function gotoPermanentFolios()
{
    location = "<%=request.getContextPath()%>/jsp/Folio/AllPermanentFolios.jsp";
}

function gotodelete()
{
  if (confirm("Do you want to delete the Folio Information?"))
  {
    location = "<%=request.getContextPath()%>/jsp/Folio/DeleteFolio.jsp?bid2=<%=sfolioID%>";
  }
}

function formconfirm()
{
  if (confirm("Do you want to change the Folio Information?"))
  {
    return true;
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
   if(document.forms[0].foliono.value.length == 8)
  {
    if (count == 0)
    {
      if (formconfirm())
      {
        document.forms[0].submit();
      }
    }
    else{
      ShowAllAlertMsg();
      return false;
    }
  }
  else
  {
    alert("Folio Number should be 8 characters long");
  }
}

//-->
</script>
<style type="text/css">
  <!--
    .style7 {
    color: #FFFFFF;
    font-weight: bold;
    font-size: 13px;
    }
    .style8 {color: #0A2769;
            font-weight: bold;}
    .style9 {color: #0A2769; font-weight: bold; }
  -->
</style>
</head>
<body TEXT="000000" BGCOLOR="FFFFFF">
<form action="UpdateFolio.jsp" method="get" enctype="multipart/form-data" name="FileForm">

  <span class="style7">
   <table width="100%" BORDER=1 cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#0044B0" bgcolor="#E8F3FD">
   <tr><td >
    <table width="100%" BORDER=0 cellpadding="5"  bordercolor="#0044B0" >
      <tr>
        <td bgcolor="#0044B0" class="style7">Signature Verification
        </td>
      </tr>
      <tr bgcolor="#E8F3FD">
        <td height="100%" bgcolor="#E8F3FD"><table width="100%" height="100%" border="0" cellpadding="5">
              <tr>
                <td width="22%" class="style8">Folio: </td>
                <td width="78%"><input name="foliono" type="text" maxlength="8" class="SL2TextField" value="<%=sfolioID%>" readonly="readonly">
                    <input type="hidden" name="dbstatus" value="<%=dbstatus%>"/>
                    <input type="hidden" name = "hfoliono" value="<%=sfolioID%>"/>

                      <!-- Joint 1 hidden fields-->
                  <input type="hidden" name="jointprefix" />
                  <input type="hidden" name="jointname"  />
                  <input type="hidden" name="jointrelation" />
                  <input type="hidden" name="jointfather" />
                  <input type="hidden" name="jointoccupation" />
                  <input type="hidden" name="jointtelephone" />
                  <input type="hidden" name="jointfax" />
                  <input type="hidden" name="jointemail" />
                  <input type="hidden" name="jointaddress" />
                  <input type="hidden" name="jointcontactperson" />
                  <input type="hidden" name="jointremarks" />
                  <input type="hidden" name="joint1signature" />
                  <input type="hidden" name="joint1photo" />
                  <input  type="hidden" name="joint1photostatus"/>
                  <input  type="hidden" name="joint1signaturestatus"/>

                <!-- End of Joint 1 fields-->
                <!-- Joint 2 hidden fields-->
                  <input type="hidden" name="joint2prefix" />
                  <input type="hidden" name="joint2name"  />
                  <input type="hidden" name="joint2relation" />
                  <input type="hidden" name="joint2father" />
                  <input type="hidden" name="joint2occupation" />
                  <input type="hidden" name="joint2telephone" />
                  <input type="hidden" name="joint2fax" />
                  <input type="hidden" name="joint2email" />
                  <input type="hidden" name="joint2address" />
                  <input type="hidden" name="joint2contactperson" />
                  <input type="hidden" name="joint2remarks" />
                  <input type="hidden" name="joint2signature" />
                  <input type="hidden" name="joint2photo" />
                  <input  type="hidden" name="joint2photostatus"/>
                  <input  type="hidden" name="joint2signaturestatus"/>
                <!-- End of Joint 2 fields-->
                </td>
              </tr>
              <tr>
                <td height="37"  class="style8">Name:</td>
                <td><input name="shareholdername" type="text" class="SL2TextField" value="<%=shareholdername%>"></td>
              </tr>
              <tr>
                <td class="style8"><span class="style9">Signature:</span></td>
                 <%
                   if(dbstatus!=null && dbstatus.equals("actual"))
                      tablename = "SHAREHOLDER";
                   else
                      tablename = "SHAREHOLDER_TEMP";

                  byte[] imgData =null;

                  String selectQuery="SELECT SIGNATURE FROM " + tablename + " WHERE folio_no = '"+sfolioID +"'";
                  try
                  {
                    imgData =  cm.getPhoto( cm.getConnection(), selectQuery  );
                  }
                  catch(Exception ex)
                  {
                        ;
                  }

                %>
                <td><table width="149">
                  <tr>
                    <td width="50"><%
                     if(imgData.length!=0)
                     {
                  %>
                        <img alt="Share Holder Signature" src="image.jsp?selectQuery=<%=selectQuery%>" /></td>
                    <%
                     }
                     else
                     {
                     %>
                    <b class="style9">(Signature Unavailable)</b>
                    <%
                       }
                     %>
                    <td width="87"><!--<input type="button" name="ButtonSignature" value="Edit" onClick="editSignatureWindow()">-->
                        <img name="B6" src="<%=request.getContextPath()%>/images/btnEdit.gif"  onclick="editSignatureWindow()" onMouseOver="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnEditR.gif'" onMouseOut="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnEdit.gif'">
                        <input type="hidden" name="signature">
                        <input type="hidden" name="signaturestatus" value="edit"/>
                    </td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td class="style8"><span class="style9">Verified:</span></td>
                <%

                      tablename = "SHAREHOLDER";

                  imgData =null;

                  selectQuery="SELECT SIGNATURE FROM " + tablename + " WHERE folio_no = '"+sfolioID +"'";
                  try
                  {
                    imgData =  cm.getPhoto( cm.getConnection(), selectQuery  );
                  }
                  catch(Exception ex)
                  {
                        ;
                  }

                %>
                <td><%
                      resident = resident.trim();
                      if(resident.equalsIgnoreCase("T"))
                      {
                      %>
                  <input name="resident" type="checkbox"  checked>
                  <%
                      }
                      else
                      {
                      %>
                  <input name="resident" type="checkbox"  >
                  <%
                      }
                      %></td>
              </tr>
                <tr>

                </tr>
              <tr>
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="2" rowspan="18"><table width="18%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
                  <tr>
                    <th width="50%" height="60" scope="row"> <div align="right"> <img name="B2" src="<%=request.getContextPath()%>/images/btnSubmit.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'"> </div></th>
                    <td><div align="right"> <img name="B3" src="<%=request.getContextPath()%>/images/btnClose.gif" onclick="CLClose()" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnClose.gif'"> </div>
                  </tr>
                </table></td>
              </tr>
            </table>
            <tr>
              <td><!--bordercolor="#EAC06B"-->              </td>
        </tr>
    </table>
    </td></tr>
    </table>
</form>
<%
  if (isConnect) {
    cm.takeDown();
  }
%>
</body>
</html>
