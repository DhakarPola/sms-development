<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Updated by    : Md. Kamruzzaman
'Creation Date : February 2006
'Page Purpose  : Edits a Share Price Information.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Share Price Information</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function formconfirm()
{
  if (confirm("Do you want to Update the mentioned Information?"))
  {
    return true;
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('uprice','- Unit Price (Option must be entered)');
  BlankValidate('movement','- Movement (Option must be entered)');
  BlankValidate('maxprice','- Maximum Price (Option must be entered)');
  BlankValidate('minprice','- Minimum Price (Option must be entered)');
  BlankValidate('avgprice','- Avgerage Price (Option must be entered)');
  BlankValidate('iiindex','- Index (Option must be entered)');
  BlankValidate('imovement','- Index Movement (Option must be entered)');

  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

    String MarketName = String.valueOf(request.getParameter("marketname"));
    String EntryDate = String.valueOf(request.getParameter("entrydate"));

    double uprice = 0;
    double movement = 0;
    double maxprice = 0;
    double minprice = 0;
    double avgprice = 0;
    double dindex = 0;
    double indexmovement = 0;
    String sharetraded="0";

    String query1 = "SELECT * FROM SHARE_PRICE_VIEW WHERE MARKET = '" + MarketName + "' AND ENTRY_DATE = TO_DATE('" + EntryDate + "','DD/MM/YYYY')";

    try
    {
      cm.queryExecute(query1);

      while(cm.toNext())
      {
        uprice = cm.getColumnF("UNIT_PRICE");
        movement = cm.getColumnF("MOVEMENT");
        maxprice = cm.getColumnF("MAX_PRICE");
        minprice = cm.getColumnF("MIN_PRICE");
        avgprice = cm.getColumnF("AVG_PRICE");
        dindex = cm.getColumnF("SHARE_INDEX");
        indexmovement = cm.getColumnF("INDEX_MOVEMENT");
        sharetraded = cm.getColumnS("SHARE_TRADED");

        uprice = bcal.roundtovalue(uprice,2);
        movement = bcal.roundtovalue(movement,2);
        maxprice = bcal.roundtovalue(maxprice,2);
        minprice = bcal.roundtovalue(minprice,2);
        avgprice = bcal.roundtovalue(avgprice,2);
        dindex = bcal.roundtovalue(dindex,2);
        indexmovement = bcal.roundtovalue(indexmovement,2);

       if(String.valueOf(sharetraded).equals("null"))
       sharetraded="0";
      }
    }catch(Exception e){}
%>

<form action="UpdateSharePrice.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Share Price Information</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Market</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8"><b><%=MarketName%></b></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8"><b><%=EntryDate%></b></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Unit Price</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="uprice" class="SL2TextField" value="<%=uprice%>" onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Movement</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="movement" value="<%=movement%>" class="SL2TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Maximum Price</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="maxprice" class="SL2TextField" value="<%=maxprice%>" onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Minimum Price</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="minprice" class="SL2TextField" value="<%=minprice%>" onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Average Price</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="avgprice" class="SL2TextField" value="<%=avgprice%>" onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Index</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" value="<%=dindex%>" name="iiindex" class="SL2TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Index Movement</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="imovement" value="<%=indexmovement%>" class="SL2TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;No. of Share Traded</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="shareTraded" value="<%=sharetraded%>" class="SL2TextField" onkeypress="keypressOnNumberFld()">
        </div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="43%" scope="row">
            <div align="right">
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
            </div></th>
        <td width="57%">
	          <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="history.go(-1)" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
        </td>
      </tr>
</table>

 <!--HIDDEN FIELDS-->
  <input type="hidden" name="marketname1" value="<%=MarketName%>">
  <input type="hidden" name="entrydate1" value="<%=EntryDate%>">

</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
