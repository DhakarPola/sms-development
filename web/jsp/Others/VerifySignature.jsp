<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : June 2006
'Page Name     : Certificate Verification
'Page Purpose  : Verifies Certificates with Signature.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Certificate Verification</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--

function goNextPrevious(pstart,pend,foliono)
{

  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Others/VerifySignature.jsp?StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;
  thisurl = thisurl + "&searchFolio="+foliono;
  location = thisurl;

}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}

 function putrecondate()
 {
   //check1++;
 }

 function closedocument()
{
	history.back();
}

function toreclaccept()
{
  count = 0;

  if (count == 0)
  {
    document.forms[0].action = "SaveMCVerification.jsp";
    document.forms[0].submit();
  }
  else
  {
   ShowAllAlertMsg();
   return false;
  }
}

//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
    .style19 {
	color: #0A2769;
	font-weight: bold;
    }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   int folio_state=0;
   int COUNT;
   int MAX_RECORDS=20;
   String cer_no=request.getParameter("searchFolio");
   String folioname = "";
   String foliono = "";
   String folioaddress = "";
   String dest = "";
   String VStatus = "";
   int rowcounter1 = 0;

 //Used for NEXT/PREVIOUS
    String SStartingValue = String.valueOf(request.getParameter("StartValue"));
    String SEndingValue = String.valueOf(request.getParameter("EndValue"));


    if(SStartingValue == null || SStartingValue.equals("") || SStartingValue.equals("null")) SStartingValue="1";
    if(SEndingValue == null || SEndingValue.equals("") || SEndingValue.equals("null")) SEndingValue="0";

    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    int Chunk = MAX_RECORDS;
    EndingValue = StartingValue + Chunk - 1;
    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;

    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }

    if (String.valueOf(cer_no).equals("null"))
      cer_no = "1";

   String query1;

   if (cer_no.length() != 9)
   {
     query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (select * from CERTIFICATE_VERIFICATION_VIEW order by CERTIFICATE_NO) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;
   }
   else
   {
     query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (select * from CERTIFICATE_VERIFICATION_VIEW where (CERTIFICATE_NO >= '" + cer_no + "') order by CERTIFICATE_NO) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;
   }

//           query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (select * from SIG_VERIFY_VIEW where ( cer_no >= '"+cer_no+"') order by SUBSTR(cer_no,3,6)) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;


   cm.queryExecute(query1);

   COUNT=0;
   while(cm.toNext())
   {
     COUNT++;
   }
   cm.queryExecute(query1);


%>
  <span class="style7">
  <form method="GET" action="VerifySignature.jsp">
	<SPAN id="dprint">
    <img name="B4" src="<%=request.getContextPath()%>/images/btnAccept.gif" onclick="toreclaccept()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAcceptR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAccept.gif'">
    <img name="B7" src="<%=request.getContextPath()%>/images/btnClose.gif" onclick="closedocument()" onMouseOver="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
  </SPAN>
  <input type="hidden" name="ssvalue" value="<%=StartingValue%>">
  <input type="hidden" name="eevalue" value="<%=EndingValue%>">
  <!--System.out.println("1 = " + StartingValue + " " + EndingValue);-->

  <table width="100%" BORDER=1  cellpadding="0" style="border-collapse: collapse" bordercolor="#06689E">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7" height="30"><center>Certificate Verification</center></td></tr>
	  <tr>
		<td>
			<table width="100%"  border="0" cellpadding="0" cellspacing="0">
				<tr bgcolor="#E8F3FD">
					<td width="10%"></td>
					<td width="10%"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goNextPrevious(<%=pStartingValue%>,<%=pEndingValue%>,<%=cer_no%>)"></td>
					<td width="29%"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNextPrevious(<%=nStartingValue%>,<%=nEndingValue%>,<%=cer_no%>)"></td>
					<td width="12%" class="style19">Certificate No:</td>
					<td width="19%"><input name="searchFolio" type="text" >
											<input type="hidden" name="hfolio" value="<%=cer_no%>" onkeypress="keypressOnNumberFld()" maxlength="9">
									</td>
					<td width="20%"><img src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="SubmitThis()"></td>
				</tr>
			 </table>
		</td>
	  </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="0044B0">
	  <tr bgcolor="#0044B0">
			<td width="17%" class="style9" align="center">Certificate No</td>
			<td width="17%" class="style9" align="center">Folio No</td>
			<td width="32%"><div align="center" class="style9">
			  <div align="center">Name</div>
			</div></td>
			<td width="17%"><div align="center" class="style9">
			  <div align="center">Status</div>
			</div></td>
			<td width="17%"><div align="center" class="style9">
			  <div align="center">Verify</div>
			</div></td>
	  </tr>
  <div align="left">
     <%
     folio_state=0;
    while(cm.toNext() && COUNT!=0)
     {
	   rowcounter1++;
       folio_state++;
       cer_no=cm.getColumnS("CERTIFICATE_NO");
       foliono=cm.getColumnS("FOLIO_NO");
       folioname = cm.getColumnS("NAME");
       VStatus = cm.getColumnS("VERIFICATION_STATUS");
       dest = request.getContextPath() + "/jsp/Others/EditVerifySignature.jsp";

       if (!cer_no.equals("null"))
       {
         if (String.valueOf(folioname).equals("null"))
           folioname = "";
         if (String.valueOf(foliono).equals("null"))
           foliono = "";
         if (String.valueOf(VStatus ).equals("null"))
           VStatus = "F";
         %>
    </div>
    <tr bgcolor="#E8F3FD">
           <td ><div align="center" class="style10">
             <div align="center" class="style10">
               <span class="style13">
                  <a HREF="<%=dest%>?fno=<%=cer_no%>&dbstatus=actual"><%=cer_no%></a>

               &nbsp;
               <%
               if(folio_state==COUNT)
               {
               %>
                 <input type="hidden" name="svalue" value="<%=StartingValue%>">
                 <input type="hidden" name="evalue" value="<%=EndingValue%>">
              <%}%>
               </span></div>
             </div></td>
           <td ><div align="center" class="style10">
             <div align="center" class="style10">
               <span class="style13">
                  <a HREF="<%=dest%>?fno=<%=cer_no%>&dbstatus=actual"><%=foliono%></a>
               &nbsp;
               </span></div>
             </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="left"><a HREF="<%=dest%>?fno=<%=cer_no%>&dbstatus=actual">&nbsp;&nbsp;&nbsp;<%=folioname%>&nbsp;</div>
           </div></td>



		   <td class="style13"><div align="center" class="style12">
             <div align="center"><%
              if (VStatus.equals("T"))
              {
              %>
              <img name="B2" src="<%=request.getContextPath()%>/images/icoCheck.gif">
              <%
               }
                else
                {
                %>
                <img name="B3" src="<%=request.getContextPath()%>/images/icoCross.gif">
                <%
              }
              %>
             </div>
           </div></td>
		   <td class="style13"><div align="center" class="style12">
		   <%
               if (!VStatus.equalsIgnoreCase("T"))
               {
               %>
			   <input type="checkbox" name="selectingbox<%=rowcounter1%>" value="<%=cer_no%>" onclick="putrecondate()">
               <%
               }
             %>

		   </td>

    </tr>
             <%
         }
     }
%>
</table>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
  %>
		  <!--HIDDEN FIELDS-->
            <input type="hidden" name="rowcounter1" value="<%=rowcounter1%>">
  </form>
</body>
</html>
