<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : October 2005
'Page Purpose  : Logs in an user.
'******************************************************************************************************************************************
*/
</script>

<%@page language="java" import="java.sql.*,java.util.Vector,java.security.MessageDigest,java.util.Collections,java.io.*"%>
<%@page errorPage="CommonError.jsp"%>
<jsp:useBean id="ad" scope="session" class="batbsms.MSADConnection"/>
<jsp:useBean id="cm" scope="page" class="batbsms.conBean"/>
<jsp:useBean id="cal" scope="page" class="batbsms.batCalculations"/>
<html>
<head>
<title>Thanks for the request</title>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css">
</head>
<body bgcolor="#ffffff" topmargin="0" marginheight="0">
<%
    boolean check             = false; //to check the execution status
    String Status             = "";
    String SQL                = "";
    String UserID             = String.valueOf(request.getParameter("UserID"));
    String UserType           = String.valueOf(request.getParameter("UserType"));
    String Password           = String.valueOf(request.getParameter("Password"));
    boolean IsAdministrator   =false;
    boolean IsSupervisor      =false;
    boolean IsOperator        =false;
    String CurrUserRole       = "";
    String CurrUserName       = "";

    try {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(Password.getBytes());
        byte[] bytes = md.digest();
        StringBuilder sb = new StringBuilder();
        for(int i=0; i< bytes.length ;i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        Password = sb.toString();
    }catch (Exception e){
        e.printStackTrace();
    }


if (UserType.equals("NetworkUser")) {
  boolean isConnected2AD = ad.connect();
  //try to connect if failed, redirect to error page
  if (isConnected2AD == false) {
    if (ad.isNetworkExist) {
      ad.takeDown();
    }
%>
<jsp:forward page="ErrorMsg.jsp">
  <jsp:param name="ErrorTitle" value="Connection Failure"/>
  <jsp:param name="ErrorHeading" value="Connection Problem"/>
  <jsp:param name="ErrorMsg" value="Error : Network Connection Failed, Please contact the System Administrator"/>
</jsp:forward>
<%
  }

  // checking if current user is a valid user in the Active Directory
  check = ad.isValidUser(UserID, Password);
  //if username is not a valid user, then redirect to error page
  if (check == false) {
    ad.takeDown();
%>
<jsp:forward page="ErrorMsg.jsp">
  <jsp:param name="ErrorTitle" value="Invalid username and password"/>
  <jsp:param name="ErrorHeading" value="Invalid username and password"/>
  <jsp:param name="ErrorMsg" value="User and Password do not match, Please try another one"/>
</jsp:forward>
<%
  }
  boolean isConnected2sql = cm.connect();
  //try to connect if failed, redirect to error page
  if (isConnected2sql == false) {
    cm.takeDown();
%>
<jsp:forward page="ErrorMsg.jsp">
  <jsp:param name="ErrorTitle" value="Connection Failure"/>
  <jsp:param name="ErrorHeading" value="Connection Problem"/>
  <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator"/>
</jsp:forward>
<%
  }

  Vector AllUsers = new Vector();
  AllUsers = ad.getGroups();

  //Getting all the groups of user
  String[] AllGroups = ad.getGroupsOfUser(UserID);

  SQL = "SELECT distinct user_role FROM AD_ROLE_VIEW WHERE lower(login_id)=lower('"+UserID+"')";

  for (int i = 0; i < AllGroups.length; i++) {
    if (!AllGroups[i].equals(""))
       SQL=SQL+" OR login_id='"+AllGroups[i]+"'";
  }

  int Count = cm.queryExecuteCount(SQL);

  if (Count > 0) {
    cm.queryExecute(SQL);

    while (cm.toNext()) {
      CurrUserRole=cm.getColumnS("user_role");
      CurrUserName=cm.getColumnS("NAME");
      if (CurrUserRole.equalsIgnoreCase("Administrator")) {
        IsAdministrator=true;
      } else if(CurrUserRole.equalsIgnoreCase("Supervisor")){
        IsSupervisor=true;
       } else if (CurrUserRole.equalsIgnoreCase("Operator")){
        IsOperator=true;
      }

    }
  } else {
    cm.takeDown();
    ad.takeDown();
%>
<jsp:forward page="ErrorMsg.jsp">
  <jsp:param name="ErrorTitle" value="Configuration Failure"/>
  <jsp:param name="ErrorHeading" value="User Group Configuration Problem"/>
  <jsp:param name="ErrorMsg" value="Error : User is not authenticated for this system, Please contact the System Administrator"/>
</jsp:forward>
<%
  }

} else {
  boolean isConnected2sql = cm.connect();

  //try to connect if failed, redirect to error page
  if (isConnected2sql == false) {
    cm.takeDown();
%>
<jsp:forward page="ErrorMsg.jsp">
  <jsp:param name="ErrorTitle" value="Connection Failure"/>
  <jsp:param name="ErrorHeading" value="Connection Problem"/>
  <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator"/>
</jsp:forward>
<%
  }

  SQL = "SELECT * FROM ROLE_VIEW WHERE lower(login_id)=lower('"+UserID+"') AND user_pass ='" + Password + "'";

  int Count = cm.queryExecuteCount(SQL);

  if (Count > 0) {
    cm.queryExecute(SQL);

    while (cm.toNext()) {
      CurrUserRole=cm.getColumnS("user_role");
      CurrUserName=cm.getColumnS("NAME");
      if (CurrUserRole.equalsIgnoreCase("Administrator")) {
        IsAdministrator=true;
      } else if(CurrUserRole.equalsIgnoreCase("Supervisor")){
        IsSupervisor=true;
      } else if(CurrUserRole.equalsIgnoreCase("Operator")){
        IsOperator=true;
      }
    }

  } else {
    cm.takeDown();
%>
<jsp:forward page="ErrorMsg.jsp">
  <jsp:param name="ErrorTitle" value="Invalid username and password"/>
  <jsp:param name="ErrorHeading" value="Invalid username and password"/>
  <jsp:param name="ErrorMsg" value="User and Password do not match, Please try another one"/>
</jsp:forward>
<%
  }
}
  session.setAttribute("UserName", UserID);
  session.setAttribute("FullName", CurrUserName);

  if (IsAdministrator)
    session.setAttribute("UserRole", "Administrator");
  else if(IsSupervisor)
    session.setAttribute("UserRole", "Supervisor");
  else if(IsOperator)
    session.setAttribute("UserRole", "Operator");

  session.setMaxInactiveInterval(30 * 60);

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Logged In')";
  boolean ub = cm.procedureExecute(ulog);

  cm.takeDown();
%>
<script language="JavaScript">
	location = '<%=request.getContextPath()%>/main.jsp';
</script></body>
</html>
