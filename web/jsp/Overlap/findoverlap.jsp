<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : June 2005
'Page Purpose  : List of Solved Overlaps
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
.style21 {
	color: #0044B0;
	font-size: 11px;
	font-weight:bold;
}
.styhand{
	text-decoration:underline;
	cursor:hahand;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Find Overlaps</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>

<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
    boolean isConnect1 = cm1.connect();

   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%  }%>
      <table>
       <tr>
          <td>Slno</td>
          <td>Loop count</td>
           <td>CERTIFICATE_NO</td>
           <td>DIST_FROM</td>
           <td>DIST_TO</td>
         </tr>
     <%

     String SStartingValue = String.valueOf(request.getParameter("StartValue"));
     String SEndingValue = String.valueOf(request.getParameter("EndValue"));
     int StartingValue = Integer.parseInt(SStartingValue);
     int EndingValue = Integer.parseInt(SEndingValue);

     //String selectsql="SELECT  c.HOLDER_FOLIO,c.CERTIFICATE_NO,s.DIST_FROM,s.DIST_TO,(s.DIST_TO - s.DIST_FROM +1) AS  NO_OF_SHARES FROM SCRIPTS s,CERTIFICATE c WHERE (c.CERTIFICATE_NO = s.CERTIFICATE_NO  AND c.VALIDITY='T' and c.HOLDER_FOLIO=19000833)";
     String selectsql="SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT  c.HOLDER_FOLIO,c.CERTIFICATE_NO,s.DIST_FROM,s.DIST_TO,(s.DIST_TO - s.DIST_FROM +1) AS  NO_OF_SHARES FROM SCRIPTS s,CERTIFICATE c WHERE (c.CERTIFICATE_NO = s.CERTIFICATE_NO  AND c.VALIDITY='T' and c.HOLDER_FOLIO='19000833')) div WHERE rownum<="+EndingValue+") WHERE  rnum>="+StartingValue;
     String selectsql1="";
     String Cur_Holder_folio="19000833";
     String CERTIFICATE_NO="";
     long DIST_FROM=0;
     long DIST_TO=0;
     long TotalCount=0;
     long slno=0;
     String total_share="";
     cm.queryExecute(selectsql);

            while(cm.toNext())
            {
              TotalCount++;
              CERTIFICATE_NO = cm.getColumnS("CERTIFICATE_NO");
              DIST_FROM = Long.parseLong(cm.getColumnS("DIST_FROM"));
              DIST_TO = Long.parseLong(cm.getColumnS("DIST_TO"));
              total_share = cm.getColumnS("NO_OF_SHARES");
              selectsql1="SELECT  c.CERTIFICATE_NO,s.DIST_FROM,s.DIST_TO FROM SCRIPTS s,CERTIFICATE c WHERE (c.CERTIFICATE_NO = s.CERTIFICATE_NO  AND c.VALIDITY='T' and c.HOLDER_FOLIO<>'19000833' and s.dist_from BETWEEN " + DIST_FROM + " and "+DIST_TO+" ) ";
               selectsql1=selectsql1+ " union ";
               selectsql1=selectsql1+ " SELECT  c.CERTIFICATE_NO,s.DIST_FROM,s.DIST_TO FROM SCRIPTS s,CERTIFICATE c WHERE (c.CERTIFICATE_NO = s.CERTIFICATE_NO  AND c.VALIDITY='T' and c.HOLDER_FOLIO<>'19000833' and s.dist_to BETWEEN "+DIST_FROM+" and "+DIST_TO+" )";
               cm1.queryExecute(selectsql1);
               if(cm1.toNext())
               {
                 slno++;
                 %>
                 <tr>
                 <td><%=slno%></td>
                   <td><%=TotalCount%></td>
                   <td><%=CERTIFICATE_NO%></td>
                   <td><%=DIST_FROM%></td>
                   <td><%=DIST_TO%></td>
                 </tr>
               <%
                selectsql1="Insert into MIGRATE_SCRIPTS (DIST_FROM,DIST_TO,HOLDER_FOLIO,CERTIFICATE_NO,TOTAL) values("+DIST_FROM+","+DIST_TO+",'"+Cur_Holder_folio+"','"+CERTIFICATE_NO+"','"+total_share+"')";
                cm1.queryExecute(selectsql1);
                selectsql1="Update CERTIFICATE set VALIDITY='F' where HOLDER_FOLIO='"+Cur_Holder_folio+"' and CERTIFICATE_NO='"+CERTIFICATE_NO+"'";
                //System.out.println(selectsql1);
                cm1.queryExecute(selectsql1);
             }
               if(TotalCount>=5000)
                 break;
            }

   if (isConnect)
   {
     cm.takeDown();
     cm1.takeDown();
   }
  %>
  </table>
</body>
</html>
