<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Creation Date : December 2005
'Page Purpose  : Generates Print Preview
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="util" class="batbsms.Utility"/>
<%@ page language="java" import="oracle.jdbc.driver.*,java.io.*,java.util.*,java.text.*" %>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>
<html>
<head>
<title>Print Preview</title>
<script type="text/javascript">
  function confirmprint()
  {
      if (confirm("Do you want to Print the Document?"))
      {
        document.all.dprint.style.display = 'none'
        window.print();
        document.all.dprint.style.display = '';
      }
  }
</script>
<style type="text/css">
<!--

.style7 {
	color: black;
	font-size: 12px;
}
.style8
{
	color: black;
	font-size:14px;
	font-weight:bold;
}
.style9 {
	color: black;
	font-size: 12px;
	font-weight:bold;
}
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>
</head>
<body >
<%
         boolean isConnect = cm.connect();
         if(isConnect==false)
         {
          %>
          <jsp:forward page="ErrorMsg.jsp" >
            <jsp:param name="ErrorTitle" value="Connection Failure" />
            <jsp:param name="ErrorHeading" value="Connection Problem" />
            <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
          </jsp:forward>
          <%
          }
          String certificateno ="";
          String foliono="";
          String start_dist = "";
          String end_dist = "";
          String old_start_dist="";
          String old_end_dist="";
          String total_shares = "";
          String issuedate="";
          String thisissdate="";
          String old_total_share="";
	  String Follow_name="";

          Date now = new Date();
          DateFormat df = DateFormat.getDateInstance();
          String current_date = df.format(now);
          String slipdate = util.changeDateFormat(current_date,"MM/dd/yyyy h:m:s","dd/MM/yyyy");

          certificateno = request.getParameter("certificateno");
          foliono =   request.getParameter("foliono");
          start_dist  = request.getParameter("start_dist");
          total_shares = request.getParameter("total");
          old_start_dist = request.getParameter("old_dist_from");
          old_end_dist = request.getParameter("old_dist_to");

          String findcertinfo = "SELECT * FROM LOCKEDFOLIOFROMONETOTHREE_VIEW WHERE GH3_DE_CERTIFICATE_NUMB = '"+certificateno+"'";
          cm.queryExecute(findcertinfo);

          while(cm.toNext())
          {
           thisissdate = cm.getColumnDT("ISSUEDATE");
          }


          if(certificateno==null) certificateno = "";

          String finddateQuery = "SELECT C.CERTIFICATE_ISSUE_DATE,S.Name FROM CERTIFICATE C, SHAREHOLDER S WHERE C.HOLDER_FOLIO=S.FOLIO_NO and  C.CERTIFICATE_NO='"+certificateno+"'";
          cm.queryExecute(finddateQuery);

          while(cm.toNext())
          {
              issuedate = cm.getColumnS("CERTIFICATE_ISSUE_DATE");
              issuedate = util.changeDateFormat(issuedate,"MM/dd/yyyy h:m:s","dd/MM/yyyy");
              Follow_name= cm.getColumnS("Name");

          }

          int iold_total_share = Integer.parseInt(old_end_dist)-Integer.parseInt(old_start_dist)+1;
          old_total_share = String.valueOf(iold_total_share);

          int itotal_share = Integer.parseInt(start_dist) + Integer.parseInt(total_shares)- 1;
          total_shares = String.valueOf(itotal_share);


%>

<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="13%"><img src="<%=request.getContextPath()%>/images/BATBLogo.gif"></td>
		<td width="19%" >&nbsp;</td>
        <td width="68%" class="style8">Distinctive Numbers for Demat Process </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr >
    <td colspan="2" ><table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="black" style="border-collapse:collapse">
      <tr>
        <td width="69%"><table width="100%" border="0" cellspacing="0" cellpadding="0" >
          <tr class="style7">
            <td width="6%">&nbsp;</td>
            <td width="9%" >Folio: </td>
            <td width="39%">&nbsp;<%=foliono%></td>
            <td width="12%">Certificate No: </td>
            <td width="21%">&nbsp;<%=certificateno%></td>
            <td width="13%">&nbsp;</td>
          </tr>
          <tr class="style7">
            <td>&nbsp;</td>
            <td colspan="5">&nbsp;<%=Follow_name%></td>
          </tr>
          <tr>
            <td colspan="5" align="right" class="style7">Date: &nbsp;&nbsp;</td>
            <td align="left" class="style7"><%=thisissdate%></td>
          </tr>
          <tr>
            <td colspan="6"><table width="77%" border="1" cellspacing="0" cellpadding="0" bordercolor="black" style="border-collapse:collapse">
              <tr>
                <td width="95%" class="style7" align="center" ><p>Distinctive No. (inclusive) for Demat </p>
                  </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td colspan="6"><table width="77%" border="1" cellspacing="0" cellpadding="0" bordercolor="black" style="border-collapse:collapse">
              <tr class="style7">
                <td width="35%" align="center">From</td>
                <td width="35%" align="center">To</td>
                <td width="30%" align="center">Number of Shares </td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td colspan="6"><table width="77%" border="1" cellspacing="0" cellpadding="0" bordercolor="black" style="border-collapse:collapse">
              <tr class="style7">
                <td width="35%" align="center"><%=start_dist%></td>
                <td width="35%" align="center"><%=total_shares%></td>
                <td width="30%" align="center"><%=old_total_share%></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
        <td width="31%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr class="style7">
            <td>&nbsp;</td>
            <td>Old Distinctive No. </td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3">&nbsp;</td>
          </tr>
          <tr>
            <td width="5%">&nbsp;</td>
            <td width="33%" class="style7">Start DN:</td>
            <td width="62%" class="style7"><%=old_start_dist%></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="style7">End DN:</td>
            <td class="style7"><%=old_end_dist%></td>
          </tr>
          <tr>
            <td colspan="3">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2"><p>&nbsp;</p>
    <p>&nbsp;</p></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr class="style7">
        <td width="20%" align="left">_____________________________</td>
        <td width="29%">&nbsp;</td>
        <td width="23%">&nbsp;</td>
        <td width="28%" align="right">______________________________</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr class="style7">
        <td width="6%">&nbsp;</td>
        <td width="38%">Checked By </td>
        <td width="6%">&nbsp;</td>
        <td width="32%" align="center">&nbsp;</td>
        <td width="18%" align="center">Approved By </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><hr></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr class="style9">
        <td width="6%">Printed:</td>
        <td width="94%"><%=current_date%></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center"><label>
    <!--  <input type="button" name="print" value=" Print ">-->
      <SPAN id="dprint">
        <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
      </SPAN>
    </label></td>
  </tr>
</table>


</body>
</html>
