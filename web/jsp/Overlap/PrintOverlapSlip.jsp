<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : June 2006
'Page Purpose  : Generates Overlap Slip Preview
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="util" class="batbsms.Utility"/>
<%@ page language="java" import="oracle.jdbc.driver.*,java.io.*,java.util.*,java.text.*" %>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

 String certificateno = request.getParameter("certificateno");
 String thisissdate = "";
%>
<html>
<head>
<title>Print Preview</title>
<script type="text/javascript">
  function confirmprint()
  {
      if (confirm("Do you want to Print the Document?"))
      {
        document.all.dprint.style.display = 'none'
        window.print();
        document.all.dprint.style.display = '';
      }
  }


</script>
<style type="text/css">
<!--

.style7 {
	color: black;
	font-size: 12px;
}
.style8
{
	color: black;
	font-size:14px;
	font-weight:bold;
	font-family: Arial, Helvetica, sans-serif;
}
.style9 {
	color: black;
	font-size: 12px;
	font-weight:bold;
}
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.style10 {font-family: Arial, Helvetica, sans-serif}
-->
</style>
</head>
<body >
<%
         boolean isConnect = cm.connect();
         if(isConnect==false)
         {
          %>
          <jsp:forward page="ErrorMsg.jsp" >
            <jsp:param name="ErrorTitle" value="Connection Failure" />
            <jsp:param name="ErrorHeading" value="Connection Problem" />
            <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
          </jsp:forward>
          <%
          }
          String foliono="";
          int start_dist = 0;
          int end_dist = 0;
          int total_shares = 0;
          String issuedate="";
          String Follow_name="";

          String[][] distarray = new String[4][2];
          int rowcounter = 0;

          distarray[0][0] = "";
          distarray[0][1] = "";
          distarray[1][0] = "";
          distarray[1][1] = "";
          distarray[2][0] = "";
          distarray[2][1] = "";
          distarray[3][0] = "";
          distarray[3][1] = "";

          Date now = new Date();
          DateFormat df = DateFormat.getDateInstance();
          String current_date = df.format(now);
          String slipdate = util.changeDateFormat(current_date,"MM/dd/yyyy h:m:s","dd/MM/yyyy");

          String findcertinfo = "SELECT * FROM LOCKEDFOLIOFROMONETOTHREE_VIEW WHERE GH3_DE_CERTIFICATE_NUMB = '"+certificateno+"'";

          cm.queryExecute(findcertinfo);

          while(cm.toNext())
          {
           foliono = cm.getColumnS("GH3_DE_REG_FOLIO_NUMB");
           start_dist = cm.getColumnI("DE_DE_START_DIST_NUMB");
           end_dist = cm.getColumnI("DE_DE_END_DIST_NUMB");
           thisissdate = cm.getColumnDT("ISSUEDATE");
          }

          total_shares = end_dist - start_dist + 1;

          String finddistinfo = "SELECT * FROM OLD_DISTINCTIONS_VIEW WHERE CERTIFICATE_NO = '"+certificateno+"'";
          cm.queryExecute(finddistinfo);

          while(cm.toNext())
          {
           if (rowcounter < 4)
           {
             distarray[rowcounter][0] = String.valueOf(cm.getColumnI("DIST_FROM"));
             distarray[rowcounter][1] = String.valueOf(cm.getColumnI("DIST_TO"));
           }
           rowcounter++;
          }

          if(certificateno==null) certificateno = "";

          String finddateQuery = "SELECT C.CERTIFICATE_ISSUE_DATE,S.Name FROM CERTIFICATE C, SHAREHOLDER S WHERE C.HOLDER_FOLIO=S.FOLIO_NO and  C.CERTIFICATE_NO='"+certificateno+"'";
          cm.queryExecute(finddateQuery);

          while(cm.toNext())
          {
           issuedate = cm.getColumnS("CERTIFICATE_ISSUE_DATE")  ;
           issuedate = util.changeDateFormat(issuedate,"MM/dd/yyyy h:m:s","dd/MM/yyyy");
		   Follow_name= cm.getColumnS("Name");
          }
%>

<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="13%" class="style10"><img src="<%=request.getContextPath()%>/images/BATBLogo.gif"></td>
		<td width="23%" class="style10" >&nbsp;</td>
        <td width="64%" class="style8">Distinction Numbers for Demat Process </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2"><span class="style10"><br>&nbsp;</span></td>
  </tr>
  <tr >
    <td colspan="2" ><table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="black" style="border-collapse:collapse">
      <tr>
        <td width="56%" class="style10"><table width="100%" border="0" cellspacing="0" cellpadding="0" >
          <tr class="style7">
            <td width="7%">&nbsp;</td>
            <td width="14%" align="left"><strong>Folio No. </strong>: </td>
            <td width="25%">&nbsp;<%=foliono%></td>
            <td width="23%" align="right"><strong>Certificate No. : </strong></td>
            <td colspan="2">&nbsp;<%=certificateno%></td>
            </tr>
          <tr class="style7">
            <td>&nbsp;</td>
            <td colspan="5">&nbsp;<%=Follow_name%></td>
          </tr>
          <tr>
            <td colspan="5" align="right" class="style7" valign="top"><strong>Date: </strong>&nbsp;&nbsp;</td>
            <td width="19%" align="left" valign="top" class="style7"><%=thisissdate%><br>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="6"><table width="81%" border="1" cellspacing="0" cellpadding="0" bordercolor="black" style="border-collapse:collapse">
              <tr>
                <td width="95%" class="style7" align="center" ><p><strong>Dist. No. (inclusive) for Demat </strong></p>
                  </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td colspan="6"><table width="81%" border="1" cellspacing="0" cellpadding="0" bordercolor="black" style="border-collapse:collapse">
              <tr class="style7">
                <td width="35%" align="center"><strong>From</strong></td>
                <td width="35%" align="center"><strong>To</strong></td>
                <td width="30%" align="center"><strong>No. of Shares </strong></td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td colspan="6"><table width="81%" border="1" cellspacing="0" cellpadding="0" bordercolor="black" style="border-collapse:collapse">
              <tr class="style7">
                <td width="35%" align="center"><%=start_dist%></td>
                <td width="35%" align="center"><%=end_dist%></td>
                <td width="30%" align="center"><%=total_shares%></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
        <td width="44%" class="style10"><table width="100%" height="137" border="0" cellpadding="0" cellspacing="0">
          <tr class="style7">
            <td><strong>&nbsp;Old Dist. No.</strong></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td width="19%" class="style9">&nbsp;Start DN</td>
            <td width="2%" class="style9" align="center">:</td>
            <td width="19%" class="style7" align="right"><%=distarray[0][0]%></td>
            <td width="19%" class="style7" align="right"><%=distarray[1][0]%></td>
            <td width="19%" class="style7" align="right"><%=distarray[2][0]%></td>
            <td width="22%" class="style7" align="right"><%=distarray[3][0]%></td>
          </tr>
          <tr>
            <td width="19%" class="style9">&nbsp;End DN</td>
            <td width="2%" class="style9" align="center">:</td>
            <td width="19%" class="style7" align="right"><%=distarray[0][1]%></td>
            <td width="19%" class="style7" align="right"><%=distarray[1][1]%></td>
            <td width="19%" class="style7" align="right"><%=distarray[2][1]%></td>
            <td width="22%" class="style7" align="right"><%=distarray[3][1]%></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2"><p class="style10">&nbsp;</p>
    <p class="style10">&nbsp;</p></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr class="style7">
        <td width="20%" align="left" class="style10">_____________________________</td>
        <td width="29%" class="style10">&nbsp;</td>
        <td width="23%" class="style10">&nbsp;</td>
        <td width="28%" align="right" class="style10">______________________________</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr class="style7">
        <td width="6%" class="style10">&nbsp;</td>
        <td width="38%" class="style10"><strong>Checked By </strong></td>
        <td width="6%" class="style10">&nbsp;</td>
        <td width="32%" align="center" class="style10">&nbsp;</td>
        <td width="18%" align="center" class="style10"><strong>Approved By </strong></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><hr></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr class="style9">
        <td width="8%" class="style10">&nbsp;Printed:</td>
        <td width="92%" class="style10"><%=current_date%></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2"><span class="style10"></span></td>
  </tr>
  <tr>
    <td colspan="2" align="center">
	<span class="style10">
      <label>
      <SPAN id="dprint">
        <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<img name="B3" src="<%=request.getContextPath()%>/images/btnPreviewStickerForPrint.gif"  onclick="openNewWindowSticker();" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnPreviewStickerForPrint.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnPreviewStickerForPrint.gif'">
      </span></label>
	  </td>
  </tr>
</table>


</body>

</html>
<script type="text/javascript">

function openNewWindowSticker()
{
  var windownamesticker = "PrintPreviewStickerFolio.jsp?foliono=<%=foliono%>&certificateno=<%=certificateno%>&start_dist=<%=start_dist%>&end_dist=<%=end_dist%>&total=<%=total_shares%>&name=<%=Follow_name%>&Idate=<%=thisissdate%>";
  var winsticker = window.open(windownamesticker,"","'status=no,menubar=no,scrollbars=yes,resizable=yes,toolbar=no'");
  winsticker.focus();
  winsticker.moveTo( 0, 0 );
  winsticker.resizeTo( screen.availWidth, screen.availHeight );
  return winsticker;
}
</script>
