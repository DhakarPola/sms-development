<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : June 2005
'Page Purpose  : List of Solved Overlaps
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
.style21 {
	color: #0044B0;
	font-size: 11px;
	font-weight:bold;
}
.styhand{
	text-decoration:underline;
	cursor:hahand;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Solved Overlaps</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

function goNextPrevious(pstart,pend,certificateno)
{
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Overlap/SolvedOverlaps.jsp?StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;
  thisurl = thisurl + "&searchCertificate="+certificateno;
  location = thisurl;
}

function openNewDistinctionWindow(certno,afolio,isdt)
{
 var windowname = "PrintOverlapSlip1.jsp?certificateno=" + certno + "&afoliono=" + afolio + "&aissdate=" + isdt;
 var win = window.open(windowname,"","'status=yes,menubar=no,scrollbars=yes,resizable=yes,toolbar=no'");
 win.focus();
 win.moveTo( 0, 0 );
 win.resizeTo(screen.availWidth,screen.availHeight);
 return win;
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}

//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();

   if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   int certificate_state=0;
   int COUNT=0;
   int MAX_RECORDS=25;
   String certificate_no=request.getParameter("searchCertificate");

   String issdate = "";
   String hFolio = "";
   String issby = "";
   int distfrom = 0;
   int distto = 0;
   String dest = "";
   String sname = "";
   int totallocked = 0;

 //Used for NEXT/PREVIOUS
    String SStartingValue = String.valueOf(request.getParameter("StartValue"));
    String SEndingValue = String.valueOf(request.getParameter("EndValue"));


    if(SStartingValue == null || SStartingValue.equals("") || SStartingValue.equals("null")) SStartingValue="1";
    if(SEndingValue == null || SEndingValue.equals("") || SEndingValue.equals("null")) SEndingValue="0";

    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    int Chunk = MAX_RECORDS;
    EndingValue = StartingValue + Chunk - 1;
    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;

    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }


   if(certificate_no==null)
   {
     certificate_no="000000000";
   }

   certificate_no = certificate_no.trim();

   String query1;


//     query1 =  "SELECT * FROM (SELECT div.*, rownum rnum FROM ";
//     query1 += "(SELECT * FROM LOCKEDFOLIOFROMONETOTHREE_VIEW WHERE GH3_DE_CERTIFICATE_NUMB >= '"+certificate_no+"' ORDER BY GH3_DE_CERTIFICATE_NUMB) div";
//     query1 += " WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;

//    query1 =  "SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM LOCKEDFOLIOFROMONETOTHREE_VIEW WHERE GH3_DE_CERTIFICATE_NUMB >= '"+certificate_no+"' ORDER BY ISSUEDATE DESC) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;

    if (certificate_no.equalsIgnoreCase("000000000"))
    {
      query1 =  "SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM LOCKEDFOLIOFROMONETOTHREE_VIEW ORDER BY ISSUEDATE DESC) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;
    }
    else
    {
      query1 =  "SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM LOCKEDFOLIOFROMONETOTHREE_VIEW WHERE GH3_DE_CERTIFICATE_NUMB >= '"+certificate_no+"' ORDER BY GH3_DE_CERTIFICATE_NUMB) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;
    }


    COUNT = cm.queryExecuteCount(query1);
    cm.queryExecute(query1);

    String grpquery = "SELECT SUM(to_Number(DE_DE_END_DIST_NUMB) - to_Number(DE_DE_START_DIST_NUMB) + 1) as TLCK FROM LOCKEDFOLIOFROMONETOTHREE_VIEW";
    cm1.queryExecute(grpquery);
    cm1.toNext();
    totallocked = cm1.getColumnI("TLCK");

%>
  <span class="style7">
  <form method="GET" action="SolvedOverlaps.jsp">
  <SPAN id="dprint">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
  </SPAN>
  <div align="right" class="style21"><b>Total Locked Shares = <%=totallocked%></b></div>
  <table width="100%" BORDER=1  cellpadding="0" style="border-collapse: collapse" bordercolor="#06689E">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7" height="30"><center>Solved Overlaps</center></td></tr>
  <tr>
  	<td>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0">
			<tr bgcolor="#E8F3FD">
				<td width="6%"></td>
				<td width="8%"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goNextPrevious(<%=pStartingValue%>,<%=pEndingValue%>,<%=certificate_no%>)"></td>
				<td width="9%"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNextPrevious(<%=nStartingValue%>,<%=nEndingValue%>,<%=certificate_no%>)"></td>
				<td width="15%" align="right" class="style21"><b>Enter Certificate No:</b></td>
				<td width="20%" align="center"><input name="searchCertificate" type="text" >
                                <input type="hidden" name="hcertificate" value="<%=certificate_no%>"/>              </td>
				<td width="15%"><img src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="SubmitThis()"></td>
			</tr>
                </table>
	</td>
  </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="0044B0">
  <tr bgcolor="#0044B0">
    <td width="13%" class="style9" align="center">Certificate No.</td>
    <td width="13%"><div align="center" class="style9">
      <div align="center">Folio No.</div>
    </div></td>
    <td width="35%"><div align="center" class="style9">
      <div align="center">Name</div>
    </div></td>
    <td width="13%"><div align="center" class="style9">
      <div align="center">Dist. From</div>
    </div></td>
    <td width="13%"><div align="center" class="style9">
      <div align="center">Dist. To</div>
    </div></td>
    <td width="13%" align="center" class="style9">
      Issue Date
    </td>
  </tr>
  <div align="left">
     <%
     certificate_state=0;
    while(cm.toNext() && COUNT!=0)
     {
       certificate_state++;
       certificate_no=cm.getColumnS("GH3_DE_CERTIFICATE_NUMB");
       hFolio = cm.getColumnS("GH3_DE_REG_FOLIO_NUMB");
       distfrom = cm.getColumnI("DE_DE_START_DIST_NUMB");
       distto = cm.getColumnI("DE_DE_END_DIST_NUMB");
       issdate = cm.getColumnDT("ISSUEDATE");
       issby = cm.getColumnS("ISSUEBY");

       dest = request.getContextPath() + "/jsp/Overlap/PrintOverlapSlip.jsp";

       if (!certificate_no.equals("null"))
       {
         if (String.valueOf(issby).equals("null"))
           issby = "";
         if (String.valueOf(hFolio).equals("null"))
           hFolio = "";
         if (String.valueOf(issby).equals("null"))
           issby = "";

         if (!hFolio.equalsIgnoreCase(""))
         {
           String query1a = "SELECT * FROM SHAREHOLDER_VIEW WHERE FOLIO_NO = '" + hFolio + "'";
           cm1.queryExecute(query1a);

           while(cm1.toNext())
           {
             sname = cm1.getColumnS("NAME");
           }

           if (String.valueOf(sname).equals("null"))
             sname = "";
         }
         %>
    </div>
    <tr bgcolor="#E8F3FD">
           <td valign="top"><div align="center" class="style10">
             <div align="center" class="style10">
               <span class="style12">
               <a class="styhand" onclick="openNewDistinctionWindow('<%=certificate_no%>','<%=hFolio%>','<%=issdate%>')"><%=certificate_no%></a>
               &nbsp;
               <%
               if(certificate_state==COUNT)
               {
               %>
                 <input type="hidden" name="svalue" value="<%=StartingValue%>">
                 <input type="hidden" name="evalue" value="<%=EndingValue%>">
              <%}%>
               </span></div>
             </div></td>
           <td class="style13" valign="top"><div align="center" class="style12">
             <div align="center"><%=hFolio%>&nbsp;</div>
           </div></td>
           <td class="style13" valign="top"><div align="left" class="style12">
             <div align="left">&nbsp;<%=sname%></div>
           </div></td>
           <td class="style13" valign="top"><div align="center" class="style12">
             <div align="center"><%=distfrom%>&nbsp;</div>
           </div></td>
           <td class="style13" valign="top"><div align="center" class="style12">
             <div align="center"><%=distto%>&nbsp;</div>
           </div></td>
           <td class="style13" valign="top"><div align="center" class="style12">
             <div align="center"><%=issdate%>&nbsp;</div>
           </div></td>
    </tr>
             <%
         }
     }

   if (isConnect)
   {
     cm.takeDown();
     cm1.takeDown();
   }
  %>
  </table>
  </form>

</body>
</html>
