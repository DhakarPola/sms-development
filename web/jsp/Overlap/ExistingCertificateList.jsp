<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Creation Date : December 2005
'Page Purpose  : Generates List of Existing Distinction Numbers
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<%@page language="java" import="java.util.Vector" %>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>
<html>
<head>
<style type="text/css">
<!--

.style7 {
	color: black;
	font-size: 11px;
}

body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>
<script type="text/javascript">
<!--
function goNextPrevious(pstart,pend,total)
{
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Overlap/ExistingCertificateList.jsp?StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;
  thisurl = thisurl + "&no_of_shares=" + total;
  location = thisurl;

}

//-->
</script>
</head>

<body bgcolor="#E8F3FD">
<%

       boolean isConnect = cm.connect();
         if(isConnect==false)
         {
          %>
          <jsp:forward page="ErrorMsg.jsp" >
            <jsp:param name="ErrorTitle" value="Connection Failure" />
            <jsp:param name="ErrorHeading" value="Connection Problem" />
            <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
          </jsp:forward>
          <%
          }
    String queryListCertificates="";

    String querystring_total=request.getParameter("no_of_shares");

    if(querystring_total ==null) querystring_total="";

    Vector start_dist = new Vector();
    Vector end_dist = new Vector();
    Vector total = new Vector();
    Integer istart_dist ;
    Integer iend_dist ;
    Integer itotal ;

    String SStartingValue = String.valueOf(request.getParameter("StartValue"));
    String SEndingValue = String.valueOf(request.getParameter("EndValue"));


    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    int Chunk = 10;

    EndingValue = StartingValue + Chunk - 1;

    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;
    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }


  queryListCertificates = " SELECT * FROM (SELECT div.*, rownum rnum FROM ";
  queryListCertificates += "(SELECT START_DIST_NUMB, END_DIST_NUMB, (END_DIST_NUMB-START_DIST_NUMB+1) AS TOTAL \n";
  queryListCertificates += "FROM ( \n";
  queryListCertificates += "SELECT START_DIST_NUMB, END_DIST_NUMB,FOLIO_NUMB,CERTIFICATE_NUMB \n";
  queryListCertificates += "FROM ";
  queryListCertificates += "(SELECT B.START_DIST_NUMB, B.END_DIST_NUMB, B.FOLIO_NUMB, B.CERTIFICATE_NUMB \n";
  queryListCertificates += "FROM BODEMATCONFIRMATION B \n";
  queryListCertificates += "WHERE B.CERTIFICATE_STAT='ACCEPTED') \n";
  queryListCertificates += "UNION \n";
  queryListCertificates += "SELECT DE_DE_START_DIST_NUMB, DE_DE_END_DIST_NUMB, GH3_DE_REG_FOLIO_NUMB,GH3_DE_CERTIFICATE_NUMB \n";
  queryListCertificates += "FROM \n";
  queryListCertificates += "(SELECT DE_DE_START_DIST_NUMB, DE_DE_END_DIST_NUMB,GH3_DE_REG_FOLIO_NUMB,GH3_DE_CERTIFICATE_NUMB \n";
  queryListCertificates += "FROM LockedDistinctiveNumbers \n";
  queryListCertificates += "UNION SELECT DE_DE_START_DIST_NUMB, DE_DE_END_DIST_NUMB,GH3_DE_REG_FOLIO_NUMB,GH3_DE_CERTIFICATE_NUMB \n";
  queryListCertificates += "FROM LockedFolioFromOneToThree)) \n";
  queryListCertificates += "WHERE (END_DIST_NUMB-START_DIST_NUMB+1) >="+querystring_total+" \n";
  queryListCertificates += "ORDER BY \n";
  queryListCertificates += "START_DIST_NUMB,END_DIST_NUMB) div";
  queryListCertificates += "  WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;

//  System.out.println(queryListCertificates);

  cm.queryExecute(queryListCertificates);
  %>
  <form action="ExistingCertificateList.jsp" method="GET">
  <table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="#0044B0">

  <%
  int count=0;
  while(cm.toNext())
  {

    istart_dist = new Integer(cm.getColumnI("START_DIST_NUMB")) ;
    iend_dist = new Integer(cm.getColumnI("END_DIST_NUMB")) ;
    itotal =  new Integer(cm.getColumnI("TOTAL")) ;
    count++;

 %>

  <tr class="style7" align="center">
    <%
    if(count==1)
    {
    %>
        <td width="16%" ><input type="radio"  name="start_dist" value="<%=istart_dist.toString()%>" checked="checked"/>&nbsp;&nbsp;</td>
        <td width="17%" class="style7"><%=istart_dist.toString()%></td>
      <%
      }
      else
      {
        %>
        <td width="16%" ><input type="radio"  name="start_dist" value="<%=istart_dist.toString()%>" />&nbsp;&nbsp;</td>
		<td width="17%" class="style7"><%=istart_dist.toString()%></td>
        <%
        }
        %>
    <td width="33%" class="style7"><%=iend_dist.toString()%></td>
    <td width="33%" class="style7"><%=itotal%></td>
  </tr>
 <%
  }
%>
      <!-- Hidden fields for next/previous -->
      <input type="hidden" name="svalue" value="<%=StartingValue%>">
      <input type="hidden" name="evalue" value="<%=EndingValue%>">
      <input type="hidden" name="no_of_shares" value="<%=querystring_total%>">
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
<tr>
	<td width="50%" align="center"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goNextPrevious(<%=pStartingValue%>,<%=pEndingValue%>,<%=querystring_total%>)"></td>
	<td width="50%" align="center"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNextPrevious(<%=nStartingValue%>,<%=nEndingValue%>,<%=querystring_total%>)"></td>
</tr>
</table>

  </form>
</body>
</html>
