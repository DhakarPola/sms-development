<%@ page errorPage="CommonError.jsp" %>
<% if((String.valueOf(session.getAttribute("UserName")).equals("null")) || (String.valueOf(session.getAttribute("UserRole")).equals("Operator")) || (String.valueOf(session.getAttribute("UserRole")).equals("Supervisor"))){
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<jsp:useBean id="cm"  class="batbsms.conBean"/>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js">
/*
	'******************************************************************************************************************************************
	'Script Author :Aminul Islam
        'Creation Date: 21 Nov 2005
	'Page Purpose: Save JSP, This will update the SQL table.
	'******************************************************************************************************************************************
*/
</SCRIPT>
<%
String SAdmin = String.valueOf(request.getParameter("HSAdmin"));
String[] SAdminArr = SAdmin.split("#");
String Expt = String.valueOf(request.getParameter("HSupervisor"));
String[] ExptArr = Expt.split("#");
String Users = String.valueOf(request.getParameter("HOperator"));
String[] UsersArr = Users.split("#");
int rowID=1;
boolean b =  cm.connect();
if(b) // if 1, means connection established
{
  //Deletes table content
    String DeleteSQL="";
    DeleteSQL = "call DELETE_AD_USERS()";
    b = cm.procedureExecute(DeleteSQL);

//For SuperAdmin
  String SAdminSQL="";
  boolean c;
  for (int i=0; i<SAdminArr.length;i++){
    SAdminSQL = "call ADD_AD_USER( " + rowID + ", "+

    "'" + SAdminArr[i].substring(0,SAdminArr[i].indexOf("|")) + "', " +
    "'" + SAdminArr[i].substring(SAdminArr[i].indexOf("|")+1,SAdminArr[i].length()) + "', " +
    "'Administrator')";
    c = cm.procedureExecute(SAdminSQL);
    rowID++;
  }
//For Admin
  String ExptSQL="";
  boolean d;
  for (int i=0; i<ExptArr.length;i++){
    ExptSQL = "call ADD_AD_USER(" + rowID + ", "+
    "'" + ExptArr[i].substring(0,ExptArr[i].indexOf("|")) + "', " +
    "'" + ExptArr[i].substring(ExptArr[i].indexOf("|")+1,ExptArr[i].length()) + "', " +
    "'Supervisor')";
    d = cm.procedureExecute(ExptSQL);
    rowID++;
  }

//For User
  String UserSQL="";
  boolean f;
  for (int i=0; i<UsersArr.length;i++){
    UserSQL = "call ADD_AD_USER(" + rowID + ", "+
    "'" + UsersArr[i].substring(0,UsersArr[i].indexOf("|")) + "', " +
    "'" + UsersArr[i].substring(UsersArr[i].indexOf("|")+1,UsersArr[i].length()) + "', " +
    "'Operator')";
    f = cm.procedureExecute(UserSQL);
    rowID++;
  }
}

/*
	'******************************************************************************************************************************************
        'Updated By :Renad Hakim
        'Date       :March 2006
	'******************************************************************************************************************************************
*/

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Edited AD User List')";
  boolean ub = cm.procedureExecute(ulog);

cm.takeDown();
%>
<script LANGUAGE="JavaScript"> location = "<%=request.getContextPath()%>/jsp/Users/AdUserSelection.jsp"</script>
