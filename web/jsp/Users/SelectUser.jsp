<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="ad" scope="session" class="batbsms.MSADConnection"/>
<% if((String.valueOf(session.getAttribute("UserName")).equals("null")) || (String.valueOf(session.getAttribute("UserRole")).equals("User")) || (String.valueOf(session.getAttribute("UserRole")).equals("Treasury")) || (String.valueOf(session.getAttribute("UserRole")).equals("Logistics")) || (String.valueOf(session.getAttribute("UserRole")).equals("Export"))){
%>
 <script>top.location = "<%=request.getContextPath()%>/index.jsp"</script>
<%
}%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.util.Vector"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Select Users</title>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<script language="javascript">
/*
	'******************************************************************************************************************************************
	'Script Author :Renad Hakim
        'Creation Date:February 2005
	'Page Purpose: Child Window to select peoples and groups
	'******************************************************************************************************************************************
*/
var FieldName
var iindex = 0;
var jindex = 0;

//Add users
  function AddUserList(){
    var f=document.forms[0];
    if(f.UsersList.selectedIndex !=-1)
      AddFromTo(f.UsersList,f.SelectedUser)
  }

  function RemoveFromList(){
    var f=document.forms[0];
    if(f.SelectedUser.selectedIndex !=-1)
	RemoveClick(f.SelectedUser)
  }

  function PassOptionFld(){
        var f=document.forms[0];
	var ValueArray = new Array();
	var TextArray = new Array();
	var Count=0;
	var SelectionUser=f.SelectedUser;
 	for(var i=0;i<SelectionUser.options.length;i++)   {
       		ValueArray[Count] = SelectionUser.options[i].value ;
      		TextArray[Count] = SelectionUser.options[i].text ;
      		Count++;
  	  	}

	// Calling Parent function
	if (window.opener) {
 		window.opener.AddSelUser(ValueArray,TextArray) ;
 		self.close();
	}
  }
//Called on load of the page, will bring the selected users from the parent window.
function GetFldVal(Fld){
         var f=document.forms[0];
 	 var LocT = window.location.href.toLowerCase();
 	 var Idx = LocT.indexOf("&seq");
  	 if(Idx==-1) {
     		var MyFld = f.SelectedUser
                 MyFld.options.length=0;
   			 var TargetFld=window.opener.document.forms[0].elements[window.opener.FieldName]
			for (var i=0; i < TargetFld.options.length; ++i) {
					if(TargetFld.options[i].text!=""){
							MyFld.options[MyFld.options.length]=new Option(TargetFld.options[i].text,TargetFld.options[i].value);
					}
			}
	}
}
//******************** START-For the Image Swaping ********************//
var stimages = new Array()   //declare an array to store the images

//************************************************************************************
//function to populate the array
function storeimages(){
	for (j=0;j<storeimages.arguments.length;j++){
     	stimages[j]=new Image();
		stimages[j].src="<%=request.getContextPath()%>/images/"+storeimages.arguments[j]+".gif";
	}
}
//************************************************************************************
//Main function to swap the Imagaes
function onLoad() {
	SwapImage ("peopleBtn", 1); //To make People Tab On
        SwapImage ("groupsBtn", 2); //To make Group Tab On
        iindex = 0;
        jindex = 0;
}
//Main function to swap the Imagaes
function SwapImage(img,picnum) {
	document.images[img].src = stimages[picnum].src ;
}
storeimages("PeopleOff","PeopleOn", "GroupsOff", "GroupsOn");
//************************************************************************************

//******************** END - the Image Swaping ********************//


//******************** START - CLICKS ON PEOPLE/GROUP TAB ********************//
//************************************************************************************
//IF CLICK ON PEOPLE TAB
function clickOnPeopleTab() {
	SwapImage ("peopleBtn", 1); //To make People Tab On
	SwapImage ("groupsBtn", 2); //To make Group Tab Off
	document.forms[0].TabSelected.value = "People";
//	if (document.forms[0].PeopleTabYesNo.value == "No") {
//		 	}
        document.forms[0].UsersList.options.length=0;
	document.forms[0].PeopleTabYesNo.value = "Yes";
	document.forms[0].GroupsTabYesNo.value = "No";
        iindex = 0;
        jindex = 0;
}
//************************************************************************************

//IF CLICK ON GROUP TAB
function clickOnGroupsTab() {
	SwapImage ("peopleBtn", 0); //To make People Tab Off
	SwapImage ("groupsBtn", 3); //To make Group Tab On
	document.forms[0].TabSelected.value = "Groups";
//	if (document.forms[0].GroupsTabYesNo.value == "No") {
//    	}
        document.forms[0].UsersList.options.length=0;

	document.forms[0].GroupsTabYesNo.value = "Yes";
	document.forms[0].PeopleTabYesNo.value = "No";
        iindex = 0;
        jindex = 0;
}
//******************** END - CLICKS ON PEOPLE/GROUP TAB ********************//
function setCat(Cat) {
//  alert("At the start of CAT: IINDEX is " + iindex + " JINDEX is " + jindex);
  document.forms[0].Cat.value=Cat;
  var field=document.forms[0].UsersList;
  var AllArray = new Array();
  var TotalCount=0;
  var TotalAdded=0;
  var StartIndex=0;
  if (document.forms[0].GroupsTabYesNo.value == "Yes") {
    AllArray=AllGroupArray;
  } else {
    AllArray=AllUserArray;
  }

  TotalCount=AllArray.length;
  var TotalChunkNo=Math.ceil(TotalCount/MaxCount);
  for (var i=0;i<TotalChunkNo;i++) {
    if (AllArray[i*MaxCount].charAt(0).toLowerCase()>=Cat.toLowerCase()) {
      if (i==0) StartIndex=0
      else StartIndex=(i*MaxCount)- MaxCount;

      break;
    }
  }
  field.options.length=0;
  var TotalAdded=0
//  iindex = StartIndex;
  for (var j=StartIndex;(j<TotalCount) && (TotalAdded<MaxCount);j++) {
      if (AllArray[j].charAt(0).toLowerCase()>=Cat.toLowerCase()) {
        Value=AllArray[j].substring(AllArray[j].indexOf("~|~")+3,AllArray[j].length);
        Text=AllArray[j].substring(0,AllArray[j].indexOf("~|~"));
        field.options[field.options.length]=new Option(Text,Value);
        TotalAdded++
      }
  }
  iindex = j - MaxCount;
  if (iindex < 0)
  {
    iindex = 0;
  }
  jindex = j - 1;
  //alert("At the end of CAT: IINDEX is " + iindex + " JINDEX is " + jindex);
}

//************************************************************************************

//LOADING ALL THE USERS & GROUPS IN ARRAYS
var AllGroupArray = new Array();
var AllUserArray = new Array();
var AllGroupCount=0;
var AllUserCount=0;
var MaxCount=50;

  function LoadInArray(){
        var f=document.forms[0];
 	for(var i=0;i<f.AllUserList.options.length;i++)   {
           if (f.AllUserList.options[i].value!='') {
             AllUserArray[AllUserCount] = f.AllUserList.options[i].value ;
             AllUserCount++;
           }
 	}
        for(var i=0;i<f.AllGroupList.options.length;i++)   {
           if (f.AllGroupList.options[i].value!='') {
             AllGroupArray[AllGroupCount] = f.AllGroupList.options[i].value ;
             AllGroupCount++;
           }
 	}
  }
//************************************************************************************

function loadPrev()
{
//  alert("At the start of PREV: IINDEX is " + iindex + " JINDEX is " + jindex);
  var field=document.forms[0].UsersList;
  var AllArray = new Array();
  var TotalCount=0;
  var TotalAdded=0;

  if (document.forms[0].GroupsTabYesNo.value == "Yes") {
    AllArray=AllGroupArray;
  } else {
    AllArray=AllUserArray;
  }

  TotalCount=AllArray.length;
  field.options.length=0;

  if (iindex < 0)
  {
    iindex = 0;
    jindex = MaxCount - 1;
  }

  if (jindex + 1 > TotalCount)
  {
    jindex = TotalCount - 1;
  }

  for (var j=iindex;((j > -1)) && (j<TotalCount) && (TotalAdded<MaxCount);j++)
  {
    Value=AllArray[j].substring(AllArray[j].indexOf("~|~")+3,AllArray[j].length);
    Text=AllArray[j].substring(0,AllArray[j].indexOf("~|~"));
    field.options[field.options.length]=new Option(Text,Value);
    TotalAdded++;
  }

  iindex = iindex - MaxCount;
  jindex = jindex - MaxCount;

  if (iindex < 0)
  {
    iindex = 0;
    jindex = MaxCount - 1;
  }

  if (jindex + 1 > TotalCount)
  {
    jindex = TotalCount - 1;
  }

// alert("At the end of PREV: IINDEX is " + iindex + " JINDEX is " + jindex);
}

function loadNext()
{
//  alert("At the start of NEXT: IINDEX is " + iindex + " JINDEX is " + jindex);
  var field=document.forms[0].UsersList;
  var AllArray = new Array();
  var TotalCount=0;
  var TotalAdded=0;

  if (document.forms[0].GroupsTabYesNo.value == "Yes") {
    AllArray=AllGroupArray;
  } else {
    AllArray=AllUserArray;
  }

  TotalCount=AllArray.length;
  field.options.length=0;

  var k = jindex;

  if (jindex + 1 >= TotalCount)
  {
    iindex = TotalCount - MaxCount;
    jindex = TotalCount;
    k = iindex;
  }
  for (var j=k;(j<TotalCount) && (TotalAdded<MaxCount);j++)
  {
    Value=AllArray[j].substring(AllArray[j].indexOf("~|~")+3,AllArray[j].length);
    Text=AllArray[j].substring(0,AllArray[j].indexOf("~|~"));
    field.options[field.options.length]=new Option(Text,Value);
    TotalAdded++;
  }
  jindex = jindex + TotalAdded;
  iindex = iindex + TotalAdded;

  if (jindex + 1 >= TotalCount)
  {
    iindex = TotalCount - MaxCount;
    jindex = TotalCount;
  }

//  alert("At the end of NEXT: IINDEX is " + iindex + " JINDEX is " + jindex);
}

//************************************************************************************

</script>
</head>

<body BGCOLOR="#E8F3FD" bordercolor="#06689E" onload="GetFldVal('SelectedUser'); onLoad();LoadInArray();">
<form name="form0" method="post" action="">
  <input name="TabSelected" type="hidden" value="People">
  <input name="GroupsTabYesNo" type="hidden" value="No">
  <input name="PeopleTabYesNo" type="hidden" value="Yes">
  <input type="hidden" name="Cat" value="A">

<table width="706" border="0" height="331">
  <tr BGCOLOR="#E8F3FD">
    <td height="15" valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
    <tr>

      <td><input type="button" value="A" onclick="setCat('A');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="B" onclick="setCat('B');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="C" onclick="setCat('C');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="D" onclick="setCat('D');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="E" onclick="setCat('E');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="F" onclick="setCat('F');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="G" onclick="setCat('G');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="H" onclick="setCat('H');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="I" onclick="setCat('I');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="J" onclick="setCat('J');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="K" onclick="setCat('K');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="L" onclick="setCat('L');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="M" onclick="setCat('M');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
    </tr>
    <tr>
      <td><input type="button" value="N" onclick="setCat('N');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="O" onclick="setCat('O');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="P" onclick="setCat('P');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="Q" onclick="setCat('Q');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="R" onclick="setCat('R');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="S" onclick="setCat('S');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="T" onclick="setCat('T');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="U" onclick="setCat('U');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="V" onclick="setCat('V');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="W" onclick="setCat('W');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="X" onclick="setCat('X');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="Y" onclick="setCat('Y');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
      <td><input type="button" value="Z" onclick="setCat('Z');" style="letter-spacing:1px;font-family:tahoma; font-size:7pt; width:18px; background-color:#EEEEEE"></td>
    </tr>
  </table></td>
  </tr>
  <tr BGCOLOR="#E8F3FD">
     <td width="312" height="15" valign="top">
    <img src="<%=request.getContextPath()%>/images/PeopleOff.gif"  border ="0" leftmargin ="0 "rightmargin="0" topmargin="0" onClick="clickOnPeopleTab()" NAME="peopleBtn"><img src="<%=request.getContextPath()%>/images/GroupsOff.gif" border ="0" leftmargin ="0 "rightmargin="0" topmargin="0"  onClick="clickOnGroupsTab()" NAME="groupsBtn"><img src="<%=request.getContextPath()%>/images/usBoxTable.gif">

	<table width="96%"  border="0" cellpadding="0">
      <tr>
        <td><img name="B1" src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="loadPrev()"></td>
        <td><div align="right"><img name="B1" src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="loadNext()"></div></td>
      </tr>
    </table>
</td>
    </tr>
  <tr>
<!--  <td height="247" valign="top" width="312"><SELECT NAME="UsersList" SIZE=15 MULTIPLE style="width:300px"><OPTION VALUE="Masud Ahmad/BD/DTL">Masud Ahmad<OPTION VALUE="Aminul Islam/BD/DTL">Aminul Islam<OPTION VALUE="Renad Hakim/BD/DTL">Renad Hakim</select>-->
<td height="247" valign="top" width="312">
  <SELECT NAME="UsersList" SIZE=15 MULTIPLE style="width:300px">
</select>
</td>
    <td width="80" height="247">
<!--     <form name="form4" method="post" action="">-->
      <p>
        <img name="BAdd" src="<%=request.getContextPath()%>/images/btnAdd.gif" onclick="AddUserList()" onMouseOver="document.forms[0].BAdd.src = '<%=request.getContextPath()%>/images/btnAddOn.gif'" onMouseOut="document.forms[0].BAdd.src = '<%=request.getContextPath()%>/images/btnAdd.gif'">
</p>
      <p>
        <img name="BRemove" src="<%=request.getContextPath()%>/images/btnRemove.gif" onclick="RemoveFromList()" onMouseOver="document.forms[0].BRemove.src = '<%=request.getContextPath()%>/images/btnRemoveOn.gif'" onMouseOut="document.forms[0].BRemove.src = '<%=request.getContextPath()%>/images/btnRemove.gif'">
      </p>
    </td>
    <td height="247" width="100" valign="top"><SELECT NAME="SelectedUser" SIZE=15 MULTIPLE style="width:300px"></select></td>
  </tr>
  <tr>
    <td width="312" height="44" valign="top">&nbsp;</td>
    <td width="80" height="44">&nbsp;</td>
    <td height="44" valign="middle" width="300"><table width="288" border="0">
      <tr>
        <td width="80">&nbsp;</td>
        <td width="93"><img name="BOK" src="<%=request.getContextPath()%>/images/btnOK.gif" onclick="PassOptionFld()" onMouseOver="document.forms[0].BOK.src = '<%=request.getContextPath()%>/images/btnOKOn.gif'" onMouseOut="document.forms[0].BOK.src = '<%=request.getContextPath()%>/images/btnOK.gif'"></td>
        <td width="148"><img name="BCancel" src="<%=request.getContextPath()%>/images/btnCancel.gif" onclick="window.close()" onMouseOver="document.forms[0].BCancel.src = '<%=request.getContextPath()%>/images/btnCancelOn.gif'" onMouseOut="document.forms[0].BCancel.src = '<%=request.getContextPath()%>/images/btnCancel.gif'"></td>
        </tr>
    </table></td>
  </tr>
</table>
<span id="DivAllUserList" style="visibility:hidden">
<SELECT NAME="AllUserList" size="1">
<%
Vector members= new Vector();
members=ad.getMembers("Members");
String SelectedFullMember;
for (int i=0;i<members.size();i++) {
  SelectedFullMember=(String)members.elementAt(i);
//  SelectedMemberLogin=SelectedFullMember.substring(SelectedFullMember.indexOf("~|~")+3,SelectedFullMember.length());
//  SelectedMemberName=SelectedFullMember.substring(0,SelectedFullMember.indexOf("~|~"));
%><OPTION VALUE="<%=SelectedFullMember%>">
<%}%></select>
</span>
<span id="DivAllGroupList" style="visibility:hidden">
<SELECT NAME="AllGroupList">
<%
members=ad.getMembers("Group");
for (int i=0;i<members.size();i++) {
  SelectedFullMember=(String)members.elementAt(i);
%><OPTION VALUE="<%=SelectedFullMember%>">
<%}%>
</select>
</span>
</form>
</body>
</html>
