<%@page import="batbsms.ConfigurationDAO"%>
<%@page import="batbsms.ConfigurationModel"%>
<script>
    /*
     '******************************************************************************************************************************************
     'Script Author : Renad Hakim
     'Creation Date : October 2005
     'Page Purpose  : View of all the users.
     '******************************************************************************************************************************************
     */
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if (String.valueOf(session.getAttribute("UserName")).equals("null")) {
%>
<script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
    }%>

<html>
    <style type="text/css">
        <!--
        .style9 {
            color: #FFFFFF;
            font-weight: bold;
        }
        .style12 {
            color: #000066;
            font-size: 11px;
        }
        -->
    </style>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>All Users</title>

        <SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
        <LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>


    </head>

    <body TEXT="000000" BGCOLOR="FFFFFF">
        <%
            boolean isConnect = cm.connect();
            if (isConnect == false) {
        %>
        <jsp:forward page="ErrorMsg.jsp" >
            <jsp:param name="ErrorTitle" value="Connection Failure" />
            <jsp:param name="ErrorHeading" value="Connection Problem" />
            <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
        </jsp:forward>
        <%
            }

            String UNSUCCESSFUL_TRY                 = String.valueOf(request.getParameter("UNSUCCESSFUL_TRY"));
            String SESSION_TIMEOUT                  = String.valueOf(request.getParameter("SESSION_TIMEOUT"));
            String INACTIVITY_TIME                  = String.valueOf(request.getParameter("INACTIVITY_TIME"));
            String PASSWORD_CHANGE                  = String.valueOf(request.getParameter("PASSWORD_CHANGE"));
            String PASSWORD_CHANGE_HISTORY          = String.valueOf(request.getParameter("PASSWORD_CHANGE_HISTORY"));
            String PASSWORD_COMPLEXITY_SPCHARACTER  = String.valueOf(request.getParameter("PASSWORD_COMPLEXITY_SPCHARACTER")); 
            String PASSWORD_COMPLEXITY_CAPITAL      = String.valueOf(request.getParameter("PASSWORD_COMPLEXITY_CAPITAL")); 
            String PASSWORD_COMPLEXITY_NUMBER       = String.valueOf(request.getParameter("PASSWORD_COMPLEXITY_NUMBER")); 
            String PASSWORD_LENGTH_ADMIN            = String.valueOf(request.getParameter("PASSWORD_LENGTH_ADMIN")); 
            String PASSWORD_LENGTH_USER             = String.valueOf(request.getParameter("PASSWORD_LENGTH_USER")); 
            
            ConfigurationModel configurationModel = new ConfigurationModel();

            configurationModel.setUnsuccessfulTry(Integer.parseInt(UNSUCCESSFUL_TRY));
            configurationModel.setSessionTimeOut(Integer.parseInt(SESSION_TIMEOUT));
            configurationModel.setInactivityTime(Integer.parseInt(INACTIVITY_TIME));
            configurationModel.setPasswordChange(Integer.parseInt(PASSWORD_CHANGE));
            configurationModel.setPasswordChangeHistory(Integer.parseInt(PASSWORD_CHANGE_HISTORY));                 
            configurationModel.setPasswordComplexitySpeical(Boolean.parseBoolean(PASSWORD_COMPLEXITY_SPCHARACTER));                     
            configurationModel.setPasswordComplexityCapital(Boolean.parseBoolean(PASSWORD_COMPLEXITY_CAPITAL));                     
            configurationModel.setPasswordComplexityNumber(Boolean.parseBoolean(PASSWORD_COMPLEXITY_NUMBER));                     
            configurationModel.setPasswordLengthAdmin(Integer.parseInt(PASSWORD_LENGTH_ADMIN));                   
            configurationModel.setPasswordLengthUser(Integer.parseInt(PASSWORD_LENGTH_USER)); 

            out.print(new ConfigurationDAO().updateConfiguration(configurationModel));


 

                if (isConnect) {
                    cm.takeDown();
                }

                if(new ConfigurationDAO().updateConfiguration(configurationModel)){
                %>
                <script language="javascript">
                    location = "<%=request.getContextPath()%>/jsp/Users/Configuration.jsp";
                </script>
                <%
                }else{
                %>
                <jsp:forward page="ErrorMsg.jsp" >
                    <jsp:param name="ErrorTitle" value="Connection Failure" />
                    <jsp:param name="ErrorHeading" value="Connection Problem" />
                    <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
                </jsp:forward> 
                <%
                }
            %>


                </body>
                </html>
