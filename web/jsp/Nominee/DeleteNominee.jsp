<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Creation Date : December 2005
'Page Purpose  : Deletes a Nominee.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Delete Nominee</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String nominee_no = String.valueOf(request.getParameter("nominee_no"));
  String deleteNomineeQuery = "call DELETE_NOMINEE('" + nominee_no + "')";
  System.out.println(deleteNomineeQuery);
  boolean b = cm.procedureExecute(deleteNomineeQuery);

/*
'******************************************************************************************************************************************
'Updated By    : Renad Hakim
'Date          : March 2005
'******************************************************************************************************************************************
*/

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Deleted Nominee Information')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Nominee/AllNominees.jsp";
</script>
</body>
</html>
