<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Creation Date : December 2005
'Page Purpose  : Edits Nominee Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}
%>

<html>
<head>
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

  String nominee_no = String.valueOf(request.getParameter("nominee_no"));
  String name = "";
  String contact ="";
  String telephone = "";
  String address = "";
  String address2 = "";
  String address3 = "";
  String address4 = "";
  String remarks = "";


   String editNomineeQuery = "SELECT * FROM NOMINEE_VIEW WHERE NOMINEE_NO = '" + nominee_no + "'";
   cm.queryExecute(editNomineeQuery);
   System.out.println(editNomineeQuery);

   while(cm.toNext())
     {
        name= cm.getColumnS("NAME");
        contact= cm.getColumnS("CONTACTPERSON");
        telephone= cm.getColumnS("TELEPHONE");
        address = cm.getColumnS("ADDRESS1");
        address2 = cm.getColumnS("ADDRESS2");
        address3 = cm.getColumnS("ADDRESS3");
        address4 = cm.getColumnS("ADDRESS4");
        remarks = cm.getColumnS("REMARKS");

       if (String.valueOf(name).equals("null"))
         name = "";
       if (String.valueOf(contact).equals("null"))
         contact = "";
       if (String.valueOf(telephone).equals("null"))
         telephone = "";
       if (String.valueOf(address).equals("null"))
         address = "";
       if (String.valueOf(address2).equals("null"))
         address2 = "";
       if (String.valueOf(address3).equals("null"))
         address3 = "";
       if (String.valueOf(address4).equals("null"))
         address4 = "";
       if (String.valueOf(remarks).equals("null"))
         remarks = "";

       address = address + address2 + address3 + address4;
     }
%>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit Dividend Group</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function gotonominee()
{
  location = "<%=request.getContextPath()%>/jsp/Nominee/AllNominees.jsp";
}


function formconfirm()
{
  if (confirm("Do you want to change the Dividend Group Information?"))
  {
    return true;
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  //BlankValidate('category','- Nominee Name (Option must be entered)');


  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">

<form action="UpdateNominee.jsp" method="get" enctype="multipart/form-data" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Dividend Group Information</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Dividend Group Name </div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input name="name" type="text" class="SL2TextField" id="name" maxlength="200" value="<%=name%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Contact Person </div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left">
          <input name="contact" type="text" class="SL2TextField" id="contact" maxlength="200" value="<%=contact%>">
        </div></td>
      </tr>

      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Telephone</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left">
          <input name="telephone" type="text" class="SL2TextField" id="telephone" maxlength="50" value="<%=telephone%>" onkeypress="keypressOnNumberFld()">
        </div></td>
      </tr>
	  <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Address</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left">
		  <textarea name="address"  cols="20" rows="3" wrap="VIRTUAL" id="baddress"  class="ML9TextField" ><%=address%></textarea>
        </div></td>
      </tr>
	  <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Remarks</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left">
           <textarea name="remarks"  cols="20" rows="3" wrap="VIRTUAL" id="nremarks"  class="ML9TextField"><%=remarks%></textarea>
        </div></td>
      </tr>

  </table>

  <!--HIDDEN FIELDS-->
  <input type="hidden" name="nominee_no" value="<%=nominee_no%>">

  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="43%" scope="row">
            <div align="right">
            <!--  onclick="SubmitThis()"    -->
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
            </div></th>
        <td width="57%">
	          <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="gotonominee()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
        </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
