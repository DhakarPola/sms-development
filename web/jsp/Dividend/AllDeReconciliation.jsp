<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : January 2007
'Page Purpose  : View of all Dividend De-Reconciliations.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@ page language="java" import="java.sql.*,java.util.Vector,java.util.Date,java.text.SimpleDateFormat,java.math.*"%>

<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
.style19 {
	color: #0A2769;
	font-weight: bold;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Dividend De-Reconciliation</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
var check1 = 0;

function goPrevious(pdate,ptype,pstart,pend,smstype)
{
  if (check1 > 0)
  {
    count = 0;

    if (count == 0)
    {
     var thisurl = "<%=request.getContextPath()%>";
     thisurl = thisurl + "/jsp/Dividend/AllDeReconciliation.jsp?dateselect=" + pdate;
     thisurl = thisurl + "&orderby=" + ptype;
     thisurl = thisurl + "&StartValue=" + pstart;
     thisurl = thisurl + "&EndValue=" + pend;

     document.forms[0].pstatus.value = thisurl;
     document.forms[0].action = "MarkDeSelected.jsp";
     document.forms[0].submit();
    }
    else
    {
     ShowAllAlertMsg();
     return false;
    }
  }
  else
  {
   var thisurl = "<%=request.getContextPath()%>";
   thisurl = thisurl + "/jsp/Dividend/AllDeReconciliation.jsp?dateselect=" + pdate;
   thisurl = thisurl + "&orderby=" + ptype;
   thisurl = thisurl + "&StartValue=" + pstart;
   thisurl = thisurl + "&EndValue=" + pend;

   document.forms[0].pstatus.value = thisurl;
   document.forms[0].action = "MarkDeSelected.jsp";
   document.forms[0].submit();
  }
}

function goNext(ndate,ntype,nstart,nend,smstype)
{
  if (check1 > 0)
  {
    count = 0;

    if (count == 0)
    {
     var thisurl = "<%=request.getContextPath()%>";
     thisurl = thisurl + "/jsp/Dividend/AllDeReconciliation.jsp?dateselect=" + ndate;
     thisurl = thisurl + "&orderby=" + ntype;
     thisurl = thisurl + "&StartValue=" + nstart;
     thisurl = thisurl + "&EndValue=" + nend;

     document.forms[0].pstatus.value = thisurl;
     document.forms[0].action = "MarkDeSelected.jsp";
     document.forms[0].submit();
    }
    else
    {
     ShowAllAlertMsg();
     return false;
    }
  }
  else
  {
   var thisurl = "<%=request.getContextPath()%>";
   thisurl = thisurl + "/jsp/Dividend/AllDeReconciliation.jsp?dateselect=" + ndate;
   thisurl = thisurl + "&orderby=" + ntype;
   thisurl = thisurl + "&StartValue=" + nstart;
   thisurl = thisurl + "&EndValue=" + nend;

   document.forms[0].pstatus.value = thisurl;
   document.forms[0].action = "MarkDeSelected.jsp";
   document.forms[0].submit();
  }
}

function closedocument()
{
  if (check1 > 0)
  {
    count = 0;

    if (count == 0)
    {
      document.forms[0].action = "MarkDeSelected.jsp";
      document.forms[0].submit();
    }
    else
    {
     ShowAllAlertMsg();
     return false;
    }
  }
  else
  {
   document.forms[0].action = "MarkDeSelected.jsp";
   document.forms[0].submit();
  }
}

function toreclaccept()
{
  count = 0;

  if (count == 0)
  {
    document.forms[0].action = "AcceptDeReconciliation.jsp";
    document.forms[0].submit();
  }
  else
  {
   ShowAllAlertMsg();
   return false;
  }
}

function gosearch(sdate,stype,sstart,send,smstype)
{
  if (document.all.searchWarrant.value.length > 0)
  {
    if (check1 > 0)
    {
      count = 0;

      if (count == 0)
      {
        var thisurl = "<%=request.getContextPath()%>";
        thisurl = thisurl + "/jsp/Dividend/AllDeReconciliation.jsp?dateselect=" + sdate;
        thisurl = thisurl + "&orderby=" + stype;
        thisurl = thisurl + "&StartValue=" + sstart;
        thisurl = thisurl + "&EndValue=" + send;

        document.forms[0].pstatus.value = thisurl;
        document.forms[0].action = "MarkDeSelected.jsp";
        document.forms[0].submit();
      }
      else
      {
       ShowAllAlertMsg();
       return false;
      }
    }
    else
    {
      var thisurl = "<%=request.getContextPath()%>";
      thisurl = thisurl + "/jsp/Dividend/AllDeReconciliation.jsp?dateselect=" + sdate;
      thisurl = thisurl + "&orderby=" + stype;
      thisurl = thisurl + "&StartValue=" + sstart;
      thisurl = thisurl + "&EndValue=" + send;

      document.forms[0].pstatus.value = thisurl;
      document.forms[0].action = "MarkDeSelected.jsp";
      document.forms[0].submit();
    }
  }
}

 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

 function putrecondate()
 {
   check1++;
 }

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}

function keypresssubmit(sdate,stype,sstart,send) {
 if (event.keyCode==13)  {
   gosearch(sdate,stype,sstart,send,'');
  }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style17 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String DivDate = String.valueOf(request.getParameter("dateselect"));
    String DivType = String.valueOf(request.getParameter("orderby"));
    String SStartingValue = String.valueOf(request.getParameter("StartValue"));
    String SEndingValue = String.valueOf(request.getParameter("EndValue"));

    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    int Chunk = 20;
//    StartingValue = StartingValue + 1;
    EndingValue = StartingValue + Chunk - 1;

    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;
    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }

    if (DivType.equalsIgnoreCase("SMS"))
    {

    String query1cc = "SELECT * FROM DIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'T' AND SELECTED = 'T' ORDER BY SUBSTR(FOLIO_NO,3,6)";
    int fcc = cm.queryExecuteCount(query1cc);

    if (nStartingValue > (fcc-Chunk+1))
    {
      nStartingValue = fcc - Chunk + 1;
    }
    if (nEndingValue > fcc)
    {
      nEndingValue = fcc;
    }
    if (nStartingValue < 1)
    {
      nStartingValue = 1;
    }
    if (nEndingValue < Chunk)
    {
      nEndingValue = Chunk;
    }

    String query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM DIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'T' AND SELECTED = 'T' ORDER BY SUBSTR(FOLIO_NO,3,6)) div where rownum <= " + EndingValue + ") where rnum >= " + StartingValue + "";
    cm.queryExecute(query1);

    String foliono = "";
    String warrantno = "";
//    double divamount = 0;
    String isCollected = "";
    String isSelected = "";
    int rowcounter1 = 0;
%>
  <span class="style7">
  <form method="GET" action="MarkDeSelected.jsp">
  <SPAN id="dprint">
    <img name="B4" src="<%=request.getContextPath()%>/images/btnAccept.gif" onclick="toreclaccept()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAcceptR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAccept.gif'">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
    <img name="B7" src="<%=request.getContextPath()%>/images/btnClose.gif" onclick="closedocument()" onMouseOver="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
  </SPAN>
  <table width="100%" BORDER=0  cellpadding="0" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td height="30" bgcolor="#0044B0" class="style17" valign="middle"><center>Dividend De-Reconciliation - SMS [<%=DivDate%>]</center></td></tr>
  </table>
  <table width="100%"  border="0" cellpadding="0" cellspacing="0" bordercolor="#0044B0">
      <tr bgcolor="#E8F3FD">
	<td width="8%" align="center" style="border-left: solid 1px #0044B0;"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goPrevious('<%=DivDate%>','<%=DivType%>',<%=pStartingValue%>,<%=pEndingValue%>)"></td>
	<td width="8%" align="left"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNext('<%=DivDate%>','<%=DivType%>',<%=nStartingValue%>,<%=nEndingValue%>)"></td>
	<td width="29%" class="style19" valign="middle">
         &nbsp;
        </td>
	<td width="24%" align="right" height="35" class="style19">
            <input name="forw" type="radio" value="folio">&nbsp;Folio No.
            &nbsp;<input name="forw" type="radio" value="warrant" checked="checked">&nbsp;Warrant No.&nbsp;
        </td>
	<td width="19%" align="center"><input name="searchWarrant" type="text" class="rhakim" onkeypress="keypressOnNumberFld()" onkeyup="keypresssubmit('<%=DivDate%>','<%=DivType%>',<%=StartingValue%>,<%=EndingValue%>);" ></td>
	<td width="12%" align="left" style="border-right: solid 1px #0044B0;"><img src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="gosearch('<%=DivDate%>','<%=DivType%>',<%=StartingValue%>,<%=EndingValue%>)"></td>
      </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="25%" class="style9"><div align="center">Folio No.</div></td>
    <td width="25%"><div align="center" class="style9">
      <div align="center">Warrant No.</div>
    </div></td>
    <td width="25%"><div align="center" class="style9">
      <div align="center">Dividend (Tk)</div>
    </div></td>
    <td width="25%"><div align="center" class="style9">
      <div align="center">De-Select</div>
    </div></td>
  </tr>
  <div align="left">
     <%
    while(cm.toNext())
     {
       rowcounter1++;
       foliono = cm.getColumnS("FOLIO_NO");
       warrantno = cm.getColumnS("WARRANT_NO");
       BigDecimal divamount = cm.getColumnD("DIVIDEND");
       isCollected = cm.getColumnS("COLLECTED");
       isSelected = cm.getColumnS("SELECTED");

       if (String.valueOf(foliono).equals("null"))
         foliono = "";
       if (String.valueOf(warrantno).equals("null"))
         warrantno = "";
       if (String.valueOf(isCollected).equals("null"))
         isCollected = "";
       if (String.valueOf(isSelected).equals("null"))
         isSelected = "";

       divamount = divamount.setScale(2,BigDecimal.ROUND_HALF_UP);
     %>
    </div>
  <tr bgcolor="#E8F3FD">
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=foliono%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=warrantno%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=divamount%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">
             <%
               if (isSelected.equalsIgnoreCase("F"))
               {
               %>
                 <input type="checkbox" name="selectingbox<%=rowcounter1%>" value="<%=warrantno%>" checked="checked">
               <%
               }
               else if (isSelected.equalsIgnoreCase("T"))
               {
               %>
                 <input type="checkbox" name="selectingbox<%=rowcounter1%>" value="<%=warrantno%>" onclick="putrecondate()">
               <%
               }
             %>
             </div>
           </div></td>
         </tr>
         <div align="left" class="style13">
             <%
         }
         %>
           <!--HIDDEN FIELDS-->
            <input type="hidden" name="rowcounter1" value="<%=rowcounter1%>">
            <input type="hidden" name="ddate" value="<%=DivDate%>">
            <input type="hidden" name="dtype" value="<%=DivType%>">
            <input type="hidden" name="svalue" value="<%=StartingValue%>">
            <input type="hidden" name="evalue" value="<%=EndingValue%>">
            <input type="hidden" name="pstatus" value="close">
            <input type="hidden" name="pchunk" value="<%=Chunk%>">
         <%
    }
    else if (DivType.equalsIgnoreCase("CDBL"))
    {
    String query1cc = "SELECT * FROM BODIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'T' AND SELECTED = 'T' AND warrant_no not in(Select OMNIBUS_BO_DIVIDEND.warrent_no from BODiVIDEND,OMNIBUS_BO_DIVIDEND, omnibus_config_dividend where BODiVIDEND.warrant_no=OMNIBUS_BO_DIVIDEND.warrent_no and omnibus_config_dividend.rec_id=OMNIBUS_BO_DIVIDEND.omnibus_div_id and OMNIBUS_BO_DIVIDEND.ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') UNION Select OMNIBUS_BO_DIVIDEND_T.warrant_no from BODiVIDEND,OMNIBUS_BO_DIVIDEND_T, omnibus_config_dividend  where BODiVIDEND.warrant_no=OMNIBUS_BO_DIVIDEND_T.warrant_no and omnibus_config_dividend.rec_id=OMNIBUS_BO_DIVIDEND_T.omnibus_div_id and OMNIBUS_BO_DIVIDEND_T.ISSUE_DATE =  TO_DATE('" + DivDate + "','DD/MM/YYYY')) ORDER BY BO_ID";
    int fcc = cm.queryExecuteCount(query1cc);

    if (nStartingValue > (fcc-Chunk+1))
    {
      nStartingValue = fcc - Chunk + 1;
    }
    if (nEndingValue > fcc)
    {
      nEndingValue = fcc;
    }
    if (nStartingValue < 1)
    {
      nStartingValue = 1;
    }
    if (nEndingValue < Chunk)
    {
      nEndingValue = Chunk;
    }

    String query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM BODIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'T' AND SELECTED = 'T' AND warrant_no not in(Select OMNIBUS_BO_DIVIDEND.warrent_no from BODiVIDEND,OMNIBUS_BO_DIVIDEND, omnibus_config_dividend where BODiVIDEND.warrant_no=OMNIBUS_BO_DIVIDEND.warrent_no and omnibus_config_dividend.rec_id=OMNIBUS_BO_DIVIDEND.omnibus_div_id and OMNIBUS_BO_DIVIDEND.ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') UNION Select OMNIBUS_BO_DIVIDEND_T.warrant_no from BODiVIDEND,OMNIBUS_BO_DIVIDEND_T, omnibus_config_dividend  where BODiVIDEND.warrant_no=OMNIBUS_BO_DIVIDEND_T.warrant_no and omnibus_config_dividend.rec_id=OMNIBUS_BO_DIVIDEND_T.omnibus_div_id and OMNIBUS_BO_DIVIDEND_T.ISSUE_DATE =  TO_DATE('" + DivDate + "','DD/MM/YYYY')) ORDER BY BO_ID) div where rownum <= " + EndingValue + ") where rnum >= " + StartingValue + "";
    cm.queryExecute(query1);

    String boid = "";
    String warrantno = "";
//    double divamount = 0;
    String isCollected = "";
    String isSelected = "";
    int rowcounter1 = 0;
%>
  <span class="style7">
  <form method="GET" action="MarkDeSelected.jsp">
  <SPAN id="dprint">
    <img name="B4" src="<%=request.getContextPath()%>/images/btnAccept.gif" onclick="toreclaccept()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAcceptR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAccept.gif'">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
    <img name="B7" src="<%=request.getContextPath()%>/images/btnClose.gif" onclick="closedocument()" onMouseOver="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
  </SPAN>
  <table width="100%" BORDER=0  cellpadding="0" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td height="30" bgcolor="#0044B0" class="style17" valign="middle"><center>Dividend De-Reconciliation - CDBL [<%=DivDate%>]</center></td></tr>
  </table>
  <table width="100%"  border="0" cellpadding="0" cellspacing="0" bordercolor="#0044B0">
      <tr bgcolor="#E8F3FD">
	<td width="8%" align="center" style="border-left: solid 1px #0044B0;"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goPrevious('<%=DivDate%>','<%=DivType%>',<%=pStartingValue%>,<%=pEndingValue%>)"></td>
	<td width="8%"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNext('<%=DivDate%>','<%=DivType%>',<%=nStartingValue%>,<%=nEndingValue%>)"></td>
	<td width="29%" class="style19" valign="middle">
         &nbsp;
        </td>
	<td width="24%" align="right" height="35" class="style19">
            <input name="forw" type="radio" value="folio">&nbsp;BO ID
            &nbsp;<input name="forw" type="radio" value="warrant" checked="checked">&nbsp;Warrant No.&nbsp;
        </td>
	<td width="19%" align="center"><input name="searchWarrant" type="text" class="rhakim" onkeypress="keypressOnNumberFld()" onkeyup="keypresssubmit('<%=DivDate%>','<%=DivType%>',<%=StartingValue%>,<%=EndingValue%>);"></td>
	<td width="12%" align="left" style="border-right: solid 1px #0044B0;"><img src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="gosearch('<%=DivDate%>','<%=DivType%>',<%=StartingValue%>,<%=EndingValue%>)"></td>
      </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="25%" class="style9"><div align="center">BO ID</div></td>
    <td width="25%"><div align="center" class="style9">
      <div align="center">Warrant No.</div>
    </div></td>
    <td width="25%"><div align="center" class="style9">
      <div align="center">Dividend</div>
    </div></td>
    <td width="25%"><div align="center" class="style9">
      <div align="center">De-Select</div>
    </div></td>
  </tr>
  <div align="left">
     <%
    while(cm.toNext())
     {
       rowcounter1++;
       boid = cm.getColumnS("BO_ID");
       warrantno = cm.getColumnS("WARRANT_NO");
       BigDecimal divamount = cm.getColumnD("DIVIDEND");
//       divamount = cm.getColumnF("DIVIDEND");
       isCollected = cm.getColumnS("COLLECTED");
       isSelected = cm.getColumnS("SELECTED");

       if (String.valueOf(boid).equals("null"))
         boid = "";
       if (String.valueOf(warrantno).equals("null"))
         warrantno = "";
       if (String.valueOf(isCollected).equals("null"))
         isCollected = "";
       if (String.valueOf(isSelected).equals("null"))
         isSelected = "";

       divamount = divamount.setScale(2,BigDecimal.ROUND_HALF_UP);
     %>
    </div>
  <tr bgcolor="#E8F3FD">
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=boid%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=warrantno%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=divamount%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">
             <%
               if (isSelected.equalsIgnoreCase("F"))
               {
               %>
                 <input type="checkbox" name="selectingbox<%=rowcounter1%>" value="<%=warrantno%>" checked="checked">
               <%
               }
               else if (isSelected.equalsIgnoreCase("T"))
               {
               %>
                 <input type="checkbox" name="selectingbox<%=rowcounter1%>" value="<%=warrantno%>" onclick="putrecondate()">
               <%
               }
             %>
             </div>
           </div></td>
         </tr>
         <div align="left" class="style13">
             <%
         }
         %>
           <!--HIDDEN FIELDS-->
            <input type="hidden" name="rowcounter1" value="<%=rowcounter1%>">
            <input type="hidden" name="ddate" value="<%=DivDate%>">
            <input type="hidden" name="dtype" value="<%=DivType%>">
            <input type="hidden" name="svalue" value="<%=StartingValue%>">
            <input type="hidden" name="evalue" value="<%=EndingValue%>">
            <input type="hidden" name="pstatus" value="close">
            <input type="hidden" name="pchunk" value="<%=Chunk%>">
         <%
    }

  if (isConnect)
   {
     cm.takeDown();
   }
  %>
         </div>
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>

</body>
</html>
