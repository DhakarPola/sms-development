<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : November 2005
'Page Purpose  : Adds Dividend Information.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.util.Date,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<jsp:useBean id="cal" class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>

<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.imagehand{
  cursor:hand;
  color:#FFFFFF;
 }

-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Declare Dividend</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function formconfirm()
{
  if (confirm("Do you want to enter the mentioned Dividend Information?\nThis may take few minutes!!!"))
  {
    return true;
  }
}

function showfields(id1,id2)
{
  if (document.all[id1].style.display=='none')
  {
   document.all[id1].style.display = '';
   document.all[id2].src = '<%=request.getContextPath()%>/images/twisteeDown.gif';
  }
  else
  {
   document.all[id1].style.display = 'none';
   document.all[id2].src = '<%=request.getContextPath()%>/images/twisteeUp.gif';
  }
}

function setbankvalues(value1)
{
  var val1 = value1;
  var val2 = value1.indexOf("%");
  var val3 = val1.substring(0,val2);
  var val4 = value1.indexOf("$");
  var val5 = val1.substring(val2 + 1,val4);
  var val6 = val1.substring(val4 + 1,val1.length);

  document.all["bchname"].value = val5;
  document.all["bnkaddress"].value = val6;
//  document.all["bnkname"].value = val3;
//  alert(document.all["bnkname"].value);
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  SelectValidate('DivType','- Dividend Type (Option must be entered)');
  BlankValidate('divpercent','- Declared Dividend % (Option must be entered)');
  BlankValidate('recdate','- Record Date (Option must be entered)');
  BlankValidate('periodfrom','- Period From (Option must be entered)');
  BlankValidate('periodto','- Period To (Option must be entered)');
  BlankValidate('qualdate','- Record Date (Option must be entered)');
  BlankValidate('paydate','- Payable Date (Option must be entered)');
  BlankValidate('indrestax','- Individual Res. Tax w/out TIN (Option must be entered)');
  BlankValidate('indrestaxtin','- Individual Res. Tax with TIN (Option must be entered)');
  BlankValidate('indnonrestax','- Individual Non-Res. Tax w/out TIN (Option must be entered)');
  BlankValidate('indnonrestaxtin','- Individual Non-Res. Tax with TIN (Option must be entered)');
  BlankValidate('comprestax','- Company Res. Tax w/out TIN (Option must be entered)');
  BlankValidate('comprestaxtin','- Company Res. Tax with TIN (Option must be entered)');
  BlankValidate('compnonrestax','- Company Non-Res. Tax w/out TIN (Option must be entered)');
  BlankValidate('compnonrestaxtin','- Company Non-Res. Tax with TIN (Option must be entered)');
  BlankValidate('indtfreediv','- Individual Tax Free Dividend (Option must be entered)');
  BlankValidate('comptfreediv','- Company Tax Free Dividend (Option must be entered)');
  BlankValidate('finyear','- Financial Year (Option must be entered)');
  BlankValidate('bnkacc','- Bank Account Info. has not been configured.');

  val1  = trim(window.document.all["divpercent"].value);
  val2  = trim(window.document.all["indrestax"].value);
  val3  = trim(window.document.all["indnonrestax"].value);
  val4  = trim(window.document.all["comprestax"].value);
  val5  = trim(window.document.all["compnonrestax"].value);
  val6  = trim(window.document.all["sptax"].value);
  val7  = trim(window.document.all["deduction1"].value);
  val8  = trim(window.document.all["deduction2"].value);
  val9  = 0;
  val10  = 0;
  val11  = 0;
  val21  = trim(window.document.all["indrestaxtin"].value);
  val31  = trim(window.document.all["indnonrestaxtin"].value);
  val41  = trim(window.document.all["comprestaxtin"].value);
  val51  = trim(window.document.all["compnonrestaxtin"].value);
  
  if (val1 != "")
   {
     if ((val1 - 100) > 0)
     {
       count = count + 1;
       nArray[count] = '- Declared Dividend Cannot be more than 100%';
     }
   }
  if (val6 != "")
   {
     if ((val6 - 100) > 0)
     {
       count = count + 1;
       nArray[count] = '- Special Tax Cannot be more than 100%';
     }
     val9 = val6;
   }
  if (val7 != "")
   {
     if ((val7 - 100) > 0)
     {
       count = count + 1;
       nArray[count] = '- Deduction 1 Cannot be more than 100%';
     }
     val10 = val7;
   }
  if (val8 != "")
   {
     if ((val8 - 100) > 0)
     {
       count = count + 1;
       nArray[count] = '- Deduction 2 Cannot be more than 100%';
     }
     val11 = val8;
   }

  if ((100 - val2 - val6 - val7 - val8) < 0)
   {
     count = count + 1;
     nArray[count] = '- Total Tax for Individual Res. Cannot be more than 100%';
   }
   if ((100 - val21 - val6 - val7 - val8) < 0)
   {
     count = count + 1;
     nArray[count] = '- Total Tax for Individual Res. (with TIN) Cannot be more than 100%';
   }
  if ((100 - val3 - val6 - val7 - val8) < 0)
   {
     count = count + 1;
     nArray[count] = '- Total Tax for Individual Non-Res. Cannot be more than 100%';
   }
  if ((100 - val31 - val6 - val7 - val8) < 0)
   {
     count = count + 1;
     nArray[count] = '- Total Tax for Individual Non-Res. (with TIN) Cannot be more than 100%';
   }

  if ((100 - val4 - val6 - val7 - val8) < 0)
   {
     count = count + 1;
     nArray[count] = '- Total Tax for Company Res. Cannot be more than 100%';
   }
   if ((100 - val41 - val6 - val7 - val8) < 0)
   {
     count = count + 1;
     nArray[count] = '- Total Tax for Company Res. (with TIN) Cannot be more than 100%';
   }  
  if ((100 - val5 - val6 - val7 - val8) < 0)
   {
     count = count + 1;
     nArray[count] = '- Total Tax for Company Non-Res. Cannot be more than 100%';
   }
  if ((100 - val51 - val6 - val7 - val8) < 0)
   {
     count = count + 1;
     nArray[count] = '- Total Tax for Company Non-Res. (with TIN) Cannot be more than 100%';
   }
   
  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   String curacc = "";
   int curbankid = 0;
   String curbankname = "";
   String curbankbranch = "";
   String curbankaddress = "";

   String query4m = "SELECT * FROM BANK_ACCOUNTS_VIEW";
   cm.queryExecute(query4m);

   while(cm.toNext())
   {
     curbankid = cm.getColumnI("CURRENT_BANK_ID");
     curacc = cm.getColumnS("CURRENT_ACCOUNT");
   }

   query4m = "SELECT * FROM BANK_VIEW WHERE BANK_ID =" + curbankid;
   cm.queryExecute(query4m);

   while(cm.toNext())
   {
     curbankname = cm.getColumnS("BANK_NAME");
     curbankbranch = cm.getColumnS("BANK_BRANCH");
     curbankaddress = cm.getColumnS("BANK_ADDRESS");
   }

%>

<form action="SaveTempDividend.jsp" method="post" name="FileForm">

  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Declare Dividend</td></tr>

  <tr bgcolor="#005FFB">
    <td width="5%" height="20">&nbsp;&nbsp;
    <img name="DivInfoimg" id="DivInfo" class="DivInfoimg" src="<%=request.getContextPath()%>/images/twisteeUp.gif" onclick="showfields('DivInfospan','DivInfoimg');">
    <strong>
    <a class="imagehand" onclick="showfields('DivInfospan','DivInfoimg');">&nbsp;&nbsp;Dividend Information</a>
    </strong>
    </td>
  </tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <div align="left">
 <SPAN id="DivInfospan" name="DivInfospan" style="display:none">
  <br>
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Dividend Type</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
        <select name="DivType" class="SLTextFieldListBox">
            <option>--- Please Select ---</option>
            <option value="Final">Final</option>
            <option value="Interim">Interim</option>
            <option value="Interim 1">Interim 1</option>
            <option value="Interim 2">Interim 2</option>
            <option value="Interim 3">Interim 3</option>
            <option value="Interim 4">Interim 4</option>
            <option value="Interim 5">Interim 5</option>
        </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Declared Dividend (%)</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="divpercent" class="SL2TextField" onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Declaration Date </div></th>
        <td>:</td>
        <td><div align="left">
         <input name="recdate" type=text id="recdate" value="" maxlength="10" class="SL2TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('recdate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Period From</div></th>
        <td>:</td>
        <td><div align="left">
         <input name="periodfrom" type=text id="periodfrom" value="" maxlength="10" class="SL2TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('periodfrom','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Period to</div></th>
        <td>:</td>
        <td><div align="left">
         <input name="periodto" type=text id="periodto" value="" maxlength="10" class="SL2TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('periodto','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Record Date</div></th>
        <td>:</td>
        <td><div align="left">
         <input name="qualdate" type=text id="qualdate" value="" maxlength="10" class="SL2TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('qualdate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Payable Date</div></th>
        <td>:</td>
        <td><div align="left">
         <input name="paydate" type=text id="paydate" value="" maxlength="10" class="SL2TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('paydate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
  </table>
  <br>
  </span>


  <tr bgcolor="#005FFB">
    <td width="5%" height="20">&nbsp;&nbsp;
    <img name="TaxInfoimg" id="TaxInfo" class="TaxInfoimg" src="<%=request.getContextPath()%>/images/twisteeUp.gif" onclick="showfields('TaxInfospan','TaxInfoimg');">
    <strong>
    <a class="imagehand" onclick="showfields('TaxInfospan','TaxInfoimg');">&nbsp;&nbsp;Tax Information</a>
    </strong>
    </td>
  </tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <div align="left">
 <SPAN id="TaxInfospan" name="TaxInfospan" style="display:none">
  <br>
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Individual Res. Tax w/out TIN (%)</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="indrestax" class="SL2TextField"  onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
	  <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Individual Res. Tax with TIN(%)</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="indrestaxtin" class="SL2TextField"  onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Individual Non-Res. Tax w/out TIN (%)</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="indnonrestax" class="SL2TextField"  onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
	  <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Individual Non-Res. Tax with TIN (%)</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="indnonrestaxtin" class="SL2TextField"  onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Company Res. Tax w/out TIN(%)</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="comprestax" class="SL2TextField"  onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
	  <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Company Res. Tax with TIN(%)</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="comprestaxtin" class="SL2TextField"  onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Company Non-Res. Tax w/out TIN (%)</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="compnonrestax" class="SL2TextField"  onkeypress="keypressOnDoubleFld()">
          <input type="hidden" name="sptax" class="SL2TextField"  onkeypress="keypressOnDoubleFld()" value="0">
        </div></td>
      </tr>
	  <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Company Non-Res. Tax with TIN (%)</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="compnonrestaxtin" class="SL2TextField"  onkeypress="keypressOnDoubleFld()">
          
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Individual Tax Free Max. Dividend (Tk)</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="indtfreediv" class="SL2TextField"  onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Company Tax Free Max. Dividend (Tk)</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="comptfreediv" class="SL2TextField" onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Deduction 1 (%)</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="deduction1" class="SL2TextField" onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp; Reason</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="ded1reason" class="SL2TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- &nbsp;&nbsp;Criteria</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="ded1criteria" class="SL2TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Deduction 2 (%)</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="deduction2" class="SL2TextField" onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp; Reason</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="ded2reason" class="SL2TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- &nbsp;&nbsp;Criteria</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="ded2criteria" class="SL2TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Financial Year</div></th>
        <td>:</td>
        <td><div align="left">
         <input name="finyear" type=text id="finyear" value="" maxlength="10" class="SL2TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('finyear','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
  </table>
  <br>
  </span>


  <tr bgcolor="#005FFB">
    <td width="5%" height="20">&nbsp;&nbsp;
    <img name="BankInfoimg" id="BankInfo" class="BankInfoimg" src="<%=request.getContextPath()%>/images/twisteeUp.gif" onclick="showfields('BankInfospan','BankInfoimg');">
    <strong>
    <a class="imagehand" onclick="showfields('BankInfospan','BankInfoimg');">&nbsp;&nbsp;Bank Information</a>
    </strong>
    </td>
  </tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
 <SPAN id="BankInfospan" name="BankInfospan" style="display:none">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Name</div></th>
        <td>:</td>
        <td><div align="left">
         <input type="text" name="bnkname" class="SL68TextField" onfocus="this.blur()" value="<%=curbankname%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Branch Name</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="bchname" class="SL68TextField" onfocus="this.blur()" value="<%=curbankbranch%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Address</div></th>
        <td valign="top">:</td>
        <td><div align="left">
          <textarea name="bnkaddress" cols="20" rows="3" wrap="VIRTUAL" id="bnkaddress" class="ML10TextField" onfocus="this.blur()"><%=curbankaddress%></textarea>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Account No.</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="bnkacc" class="SL68TextField" value="<%=curacc%>" onfocus="this.blur()">
        </div></td>
      </tr>
  </table>
  <br>
  </span>
      </div>
      <table width="100%" BORDER=0  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
      <tr bgcolor="#E8F3FD">
        <th width="50%" scope="row" >
            <div align="right">
              <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
            </div></th>
        <td width="50%" >
	      <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
      </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
