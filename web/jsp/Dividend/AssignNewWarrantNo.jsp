<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : December 2005
'Page Purpose  : Assign New Warrant No. to Locked Dividends.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<%@ page language="java" import="java.sql.*,java.util.Vector,java.util.Date,java.text.SimpleDateFormat,java.math.*" %>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Assign New Warrant No.</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
     Date d = new Date();
     int thisyear = d.getYear();
     thisyear = thisyear + 1900;
     int thismonth = d.getMonth();
     thismonth = thismonth + 1;
     int thisday = d.getDate();
     int thisday1 = d.getDay();
     String wday = "";
     String thismonth1 = "";
     String formdate = thisday + "/" + thismonth + "/" + thisyear;

   String Destination = String.valueOf(request.getParameter("pstatus"));
   String DivDate = String.valueOf(request.getParameter("ddate"));
   String DivType = String.valueOf(request.getParameter("dtype"));
   String SDate = String.valueOf(request.getParameter("mstopdate"));

   String foliono = "";
   int totalshare = 0;
   int liableshare = 0;
   double divamount = 0;
   String warrantno = "";
   double deduction1 = 0;
   double deduction2 = 0;
   double incometax = 0;
   String issuedate = "";
   String iscollected = "";
   String isprinted = "";
   String isselected = "";
   String islocked = "";
   String stopdate = "";
   String issuedletter = "";
   String issuedwarrant = "";
   String decdate = "";
   String remarks = "";
   String curWarrant = "";
   String isMerged = "";
   boolean b;

   String query1 = "";
   query1 = "SELECT * FROM DIVIDENDDECLARATION_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY')";
   cm.queryExecute(query1);

   if(cm.toNext())
   {
     isMerged = cm.getColumnS("ACC_MERGED");
   }

   if (String.valueOf(isMerged).equals("null"))
     isMerged = "";

   if (DivType.equalsIgnoreCase("SMS"))
   {
     query1 = "SELECT * FROM DIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'T' ORDER BY SUBSTR(FOLIO_NO,3,6)";
   }
   else if (DivType.equalsIgnoreCase("CDBL"))
   {
     query1 = "SELECT * FROM BODIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'T' ORDER BY BO_ID";
   }

   cm.queryExecute(query1);

   while(cm.toNext())
   {
     if (DivType.equalsIgnoreCase("SMS"))
     {
       foliono = cm.getColumnS("FOLIO_NO");
     }
     else if (DivType.equalsIgnoreCase("CDBL"))
     {
       foliono = cm.getColumnS("BO_ID");
     }

     totalshare = cm.getColumnI("TOTAL_SHARE");
     liableshare = cm.getColumnI("SHARE_LIABLE");
     divamount = cm.getColumnF("DIVIDEND");
     warrantno = cm.getColumnS("WARRANT_NO");
     deduction1 = cm.getColumnF("DED1");
     deduction2 = cm.getColumnF("DED2");
     incometax = cm.getColumnF("INCOME_TAX");
     issuedate = cm.getColumnDT("ISSUE_DATE");
     iscollected = cm.getColumnS("COLLECTED");
     isprinted = cm.getColumnS("PRINTED");
     isselected = cm.getColumnS("SELECTED");
     islocked = cm.getColumnS("LOCKED");
     stopdate = cm.getColumnDT("STOP_DATE");
     issuedletter = cm.getColumnS("ISSUED_LETTER");
     issuedwarrant = cm.getColumnS("ISSUED_WARRANT");
     decdate = cm.getColumnDT("DEC_DATE");
     remarks = cm.getColumnS("REMARKS");

     if (String.valueOf(remarks).equals("null"))
       remarks = "";

     curWarrant = remarks;

     if (remarks.equalsIgnoreCase(""))
     {
       curWarrant = cm1.LastWarrant();
     }

     String markwar = "";
     String adddiv = "";

     if(isMerged.equalsIgnoreCase("Y"))
     {
       decdate = formdate;

       cm1.queryExecute("UPDATE DIVIDENDDECLARATION SET PAYABLE_DATE = TO_DATE('" + decdate + "','DD/MM/YYYY') WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY')");
     }

     if (DivType.equalsIgnoreCase("SMS") && remarks.equalsIgnoreCase(""))
     {
       markwar = "call WARRANT_ISSUED_SMS('" + warrantno + "','" + curWarrant + "')";
       b = cm1.procedureExecute(markwar);
       adddiv = "call ADD_DIVIDEND_DUP('" + foliono + "', " + totalshare + ", " + liableshare + ", " + divamount + ", '" + curWarrant + "', " + deduction1 + "," + deduction2 + "," + incometax + ",'" + issuedate + "','F','F','F','F','','F','F','','F','','" + decdate + "')";
       b = cm1.procedureExecute(adddiv);
       adddiv = "call ADD_LAST_DIVIDEND_DUP('" + foliono + "', " + totalshare + ", " + liableshare + ", " + divamount + ", '" + curWarrant + "', " + deduction1 + "," + deduction2 + "," + incometax + ",'" + issuedate + "','F','F','F','F','','F','F','','F','','" + decdate + "')";
       b = cm1.procedureExecute(adddiv);
     }
     else if (DivType.equalsIgnoreCase("CDBL") && remarks.equalsIgnoreCase(""))
     {
       markwar = "call WARRANT_ISSUED_CDBL('" + warrantno + "','" + curWarrant + "')";
       b = cm1.procedureExecute(markwar);
       adddiv = "call ADD_BODIVIDEND_DUP('" + foliono + "', " + totalshare + ", " + liableshare + ", " + divamount + ", '" + curWarrant + "', " + deduction1 + "," + deduction2 + "," + incometax + ",'" + issuedate + "','F','F','F','F','','F','F','','F','','" + decdate + "')";
       b = cm1.procedureExecute(adddiv);
       adddiv = "call ADD_LAST_BODIVIDEND_DUP('" + foliono + "', " + totalshare + ", " + liableshare + ", " + divamount + ", '" + curWarrant + "', " + deduction1 + "," + deduction2 + "," + incometax + ",'" + issuedate + "','F','F','F','F','','F','F','','F','','" + decdate + "')";
       b = cm1.procedureExecute(adddiv);
     }
   }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Assigned new Warrant No. to a Locked Dividend Warrant(s)')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
     cm1.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Dividend/WarrantDetails.jsp?stoppingdate=<%=SDate%>&dateselect=<%=DivDate%>&orderby=<%=DivType%>&StartValue=1&EndValue=0";
</script>
</body>
</html>
