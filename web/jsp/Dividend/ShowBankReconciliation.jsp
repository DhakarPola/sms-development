<script>
/*
'******************************************************************************************************************************************
'Script Author : Md.Kamruzzaman
'Updated By    : Renad Hakim
'Updated By    : Rahat Uddin
'Update Date   : August 2007
'Creation Date : November 2006
'Page Purpose  : View of all Bank Reconciliation
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<%@ page language="java" import="java.sql.*,java.util.Vector,java.util.Date,java.text.SimpleDateFormat,java.math.*,java.math.BigDecimal.*" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style19 {color: #0A2769; font-weight: bold; }
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
.style8
{
	color: #0044B0;
	font-size: 11px;
	font-weight:bold;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Bank Reconcilition</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--

function goNextPrevious(pstart,pend,account,unclaimedaccount)
{
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Dividend/ShowBankReconciliation.jsp?StartValue="+pstart+"&unclaimed_account="+unclaimedaccount;
  thisurl = thisurl + "&EndValue="+pend;
  thisurl = thisurl + "&accountselect="+account;
  location = thisurl;
}
function gotoNew(account,unclaimed)
{
  location="<%=request.getContextPath()%>/jsp/Dividend/NewBankReconciliation.jsp?account="+account+"&unclaimed_account="+unclaimed;
}

 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1=cm1.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   int com_state=0;
   int COUNT=0;
   int MAX_RECORDS=20;

   //double
   BigDecimal total_amount=new BigDecimal(0);
   BigDecimal company_lending=new BigDecimal(0);
   BigDecimal min_deposit_amount=new BigDecimal(0);
   BigDecimal max_lending_amount=new BigDecimal(0);
   String Stotal_amount="";
   BigDecimal amount_dc=new BigDecimal(0);
   String SStartingValue = String.valueOf(request.getParameter("StartValue"));
   String SEndingValue = String.valueOf(request.getParameter("EndValue"));
   String account_no=request.getParameter("accountselect");
   String unclaimed_account=String.valueOf(request.getParameter("unclaimed_account"));

     String q1="";
     if(!unclaimed_account.equalsIgnoreCase("Y")){
       q1="select TOTAL_BALANCE from CURRENT_ACC_BALANCE";
     }
     else{
       q1="select TOTAL_BALANCE from UNCLAIMED_ACC_BALANCE";
     }
     cm1.queryExecute(q1);
     cm1.toNext();

     total_amount=cm1.getColumnD("TOTAL_BALANCE");
     BigDecimal btotal_amount=total_amount;

     if(!unclaimed_account.equalsIgnoreCase("Y")){
       q1="select BANK_REC_BAL from BANK_RECON_BAL_CURRENT";
     }
     else{
       q1="select BANK_REC_BAL from BANK_RECON_BAL_UNCLAIMED";
     }
     cm1.queryExecute(q1);
     cm1.toNext();

     amount_dc=cm1.getColumnD("BANK_REC_BAL");

     BigDecimal total_amount_bank=total_amount;
     BigDecimal btotal_amount_bank=total_amount_bank.add(amount_dc);
     max_lending_amount= btotal_amount_bank;
     btotal_amount_bank=btotal_amount_bank.setScale(2,BigDecimal.ROUND_HALF_UP);

     if(unclaimed_account.equalsIgnoreCase("Y")){//company_lending
       q1="select CBALANCE from BANK_RECON_BAL_COMPANY_ACC";
       cm1.queryExecute(q1);
       if(cm1.toNext()){
       company_lending=cm1.getColumnD("CBALANCE");
       }
       else{
         company_lending=new BigDecimal(0);
       }
       //btotal_amount=btotal_amount.subtract(company_lending);
       company_lending=company_lending.setScale(2,BigDecimal.ROUND_HALF_UP);

        q1="select LENDING_POSS_BANALCE from LENDING_POSS_BALANCE";
       cm1.queryExecute(q1);
       if(cm1.toNext()){
         min_deposit_amount=cm1.getColumnD("LENDING_POSS_BANALCE");
       }
       else{
         min_deposit_amount=new BigDecimal(0);
       }

       max_lending_amount=max_lending_amount.subtract(min_deposit_amount);
     }
     btotal_amount=btotal_amount.setScale(2,BigDecimal.ROUND_HALF_UP);

     account_no=account_no.trim();

   String dest="";
 //Used for NEXT/PREVIOUS


    if(SStartingValue == null || SStartingValue.equals("") || SStartingValue.equals("null")) SStartingValue="1";
    if(SEndingValue == null || SEndingValue.equals("") || SEndingValue.equals("null")) SEndingValue="0";

    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    int Chunk = MAX_RECORDS;
    EndingValue = StartingValue + Chunk - 1;
    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;

    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }

    if (String.valueOf(account_no).equals("null"))
      account_no = "";
      String query1="";

     query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (select * from BANK_RECON_VIEW where ACCOUNT_NO='"+ account_no +"' order by PAR_DATE DESC) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;


   cm.queryExecute(query1);
   COUNT=0;
   while(cm.toNext())
   {
     COUNT++;
   }

   cm.queryExecute(query1);
%>
  <span class="style7">
  <form>
  <SPAN id="dprint">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
  </SPAN>
  <SPAN id="dnew">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnNew.gif"  onclick="gotoNew('<%=account_no%>','<%=unclaimed_account%>')" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnNewOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnNew.gif'">
  </SPAN>
  <SPAN id="dback">
    <img name="B3" src="<%=request.getContextPath()%>/images/btnBack.gif"  onclick="history.go(-1)" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnBackOn.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnBack.gif'">
  </SPAN>
  <br />
  <br />
  <table width="100%" BORDER="1"  cellpadding="0" bordercolor="0044B0" style="border-collapse: collapse" bordercolor="#06689E">
  <!--DWLayoutTable-->
  <tr>
    <table width="100%"  border="0" cellpadding="0" cellspacing="0">
       <tr>
          <td  align="left"><div align="left" class="style8"><strong>Amount (Bank): Tk. <%=btotal_amount_bank%></strong></div></td>
          <%
          if(unclaimed_account.equalsIgnoreCase("Y")){
          %>
          <td  align="center"><div align="center" class="style8"><strong>Max Lending: Tk. <%=max_lending_amount%></strong></div></td>
          <td  align="center"><div align="center" class="style8"><strong>Amount (Company): Tk. <%=company_lending%></strong></div></td>
          <%
          }
          %>
          <td align="right"><div align="right" class="style8"><strong>Amount (SMS): Tk. <%=btotal_amount%></strong></div></td>
       </tr>
    </table>
  </tr>
  </table>
  <br />
  <table width="100%" BORDER=1  cellpadding="0" bordercolor="0044B0" style="border-collapse: collapse" bordercolor="#06689E">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7" height="30"><center>Bank Reconciliation&nbsp;
   <%
   if(account_no.length()>0){
     if(unclaimed_account.equalsIgnoreCase("Y")){
     %>
     ( Unclaimed Account )
     <%
     }
     else{
        %>
     ( Current Account )
     <%
     }
   } %></center></td></tr>
  <tr>
  	<td>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0">
			<tr bgcolor="#E8F3FD">
				<td width="10%"></td>
				<td width="10%"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goNextPrevious(<%=pStartingValue%>,<%=pEndingValue%>,'<%=account_no%>','<%=unclaimed_account%>')"></td>
				<td width="29%"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNextPrevious(<%=nStartingValue%>,<%=nEndingValue%>,'<%=account_no%>','<%=unclaimed_account%>')"></td>
				<td width="12%" class="style19">&nbsp;</td>
				<td width="19%">&nbsp;</td>
				<td width="20%">&nbsp;</td>
			</tr>
                </table>
	</td>
  </tr>
  </table>
 <!-- <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="0044B0">
  <tr bgcolor="#0044B0">-->
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="12%" class="style9" align="center">Date</td>
    <td width="39%" class="style9" align="center">Particulars</td>
    <td width="15%" class="style9" align="center">Amount (Tk)</td>
    <td width="12%"><div align="center" class="style9">
      <div align="center">Debit/Credit</div>
    </div></td>
    <td width="10%"><div align="center" class="style9">
      <div align="center">Refunded</div>
    </div></td>
    <td width="12%"><div align="center" class="style9">
      <div align="center">Refund Date</div>
    </div></td>
  </tr>
  <div align="left">
     <%
     com_state=0;
     // take form data base
     while(cm.toNext() && COUNT!=0)
     {
       com_state++;
       String par_date=String.valueOf(cm.getColumnDT("PAR_DATE"));
       String particulars=cm.getColumnS("PARTICULARS");
       String amount=String.valueOf(cm.getColumnF("AMOUNT"));
       String debit=cm.getColumnS("TYPE");
       String refund=cm.getColumnS("REFUNDED");
       String refund_date=String.valueOf(cm.getColumnDT("REFUND_DATE"));
       int id=cm.getColumnI("BR_ID");
       dest =request.getContextPath()+"/jsp/Dividend/EditBankReconcilition.jsp?id="+id+"&unclaimed_account="+unclaimed_account;
       %>
    </div>
    <tr bgcolor="#E8F3FD">
           <td ><div align="center" class="style10">
             <div align="center" class="style10">
               <span class="style13">
                  <a HREF="<%=dest%>"><%=par_date%></a>
               &nbsp;
               <%
               if(com_state==COUNT)
               {
               %>
                 <input type="hidden" name="svalue" value="<%=StartingValue%>">
                 <input type="hidden" name="evalue" value="<%=EndingValue%>">
              <%}%>
               </span></div>
             </div></td>
           <td class="style13"><div align="left" class="style12">
             <div>&nbsp;<%=particulars%></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center"><%=amount%></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center"><%=debit%></div>
           </div></td>
           <td class="style13"><div align="justify" class="style12">
             <div align="center"><%=refund%></div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=refund_date%>
           </div></td>
    </tr>

     <!--    <div align="left" class="style13">-->
             <%
     }
     %>
     <table width="100%" BORDER="0"  cellpadding="0" bordercolor="0044B0" style="border-collapse: collapse" bordercolor="#06689E">
     <tr>
     <td>&nbsp;</td>
     </tr>
     <tr>
        <th valign="top" width="22%" scope="row" class="style8"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date of Dividend Declaration</div></th>
        <td valign="top" width="2%">:</td>
        <td valign="top" width="76%"><div align="left" class="style8">

                      <table border="0">
                         <%
                          //String qdate="select DATE_DEC from DIVIDENDDECLARATION_VIEW where ACCOUNT_NO='"+ account_no +"' order by DATE_DEC";
                          //String qdate="select DATE_DEC from DIVIDENDDECLARATION_VIEW where upper(ACC_MERGED)='Y' order by DATE_DEC";


                          //cm.queryExecute(qdate);
                          String date="";
                          if(!unclaimed_account.equalsIgnoreCase("Y")){
                            String qdate="select DATE_DEC  from DIVIDENDDECLARATION_VIEW where upper(ACC_MERGED)!='Y' or ACC_MERGED is null order by DATE_DEC";
                            cm.queryExecute(qdate);
                              while(cm.toNext())
                              {
                                date=cm.getColumnDT("DATE_DEC");
                              %>
                              <tr>
                                <th scope="row"><div align="left" class="style8"><%=date%></div></th>
                              </tr>
                              <%}
                             }
                             else{
                               String qdate="select DATE_DEC from DIVIDENDDECLARATION_VIEW where upper(ACC_MERGED)='Y' order by DATE_DEC";
                               cm.queryExecute(qdate);
                               String All_Divi_date="";
                               while(cm.toNext())
                               {
                                 date=cm.getColumnDT("DATE_DEC");
                                 if(All_Divi_date.length()>0){
                                   All_Divi_date=All_Divi_date+", "+date;
                                 }
                                 else{
                                   All_Divi_date=date;
                                 }
                               }
                               %>
                               <tr>
                                 <th scope="row"><div align="left" class="style8"><%=All_Divi_date%></div></th>
                               </tr>
                               <%
                             }%>
                      </table>
        </div></td>
      </tr>
      </table>
  <%
  if (isConnect)
   {
     cm.takeDown();
   }
   if(isConnect1)
   {
     cm1.takeDown();
   }
  %>
    <!--     </div>-->

  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD" ><center>

</body>
</html>
