<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : December
'Updated By    : Rahat Uddin
'Update Date   : August 2007
'Page Purpose  : Save new Bank Reconciliation
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Bank Reconciliation</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
     String account_no=String.valueOf(request.getParameter("account"));
     String entrydate=String.valueOf(request.getParameter("entrydate"));
     String particulars=String.valueOf(request.getParameter("particulars"));
     String amount=String.valueOf(request.getParameter("amount"));
     String type=String.valueOf(request.getParameter("type"));
     String iscompany=String.valueOf(request.getParameter("iscompany"));
     String unclaimed_account=String.valueOf(request.getParameter("unclaimed_account"));
     String refund="No";



     String query="select max(BR_ID) id from BANK_RECON_VIEW";
     cm.queryExecute(query);
     cm.toNext();
     int tempid=cm.getColumnI("id");
     tempid=tempid+1;
     String id=String.valueOf(tempid);


     if(account_no.equals("null"))
     account_no="";
     if(entrydate.equals("null"))
     entrydate="";
     if(particulars.equals("null"))
     particulars="";
     if(amount.equals("null"))
     amount="";
     if(type.equals("null"))
     type="";
     if(iscompany.equals("null"))
     {
       iscompany="";
     }
     account_no=cm.replace(account_no,"'","''");
     entrydate=cm.replace(entrydate,"'","''");
     particulars=cm.replace(particulars,"'","''");
     amount=cm.replace(amount,"'","''");
     type=cm.replace(type,"'","''");


  String nbank = "call ADD_BANK_RECON('" + account_no + "', '" + entrydate + "', '" + particulars + "', '" + amount + "', '" + type + "','" + refund + "','" + id + "','"+iscompany+"')";
  boolean b = cm.procedureExecute(nbank);

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added Bank Information')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Dividend/ShowBankReconciliation.jsp?accountselect=<%=account_no%>&unclaimed_account=<%=unclaimed_account%>";
</script>
</body>
</html>
