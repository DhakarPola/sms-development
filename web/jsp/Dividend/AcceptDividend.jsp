<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : November 2005
'Page Purpose  : Form for the Administrator to Accept Declared Dividends.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="ut"  class="batbsms.Utility"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Accept Dividend</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function toaccepteddividends()
{
  if (confirm("Do you want to Accept the Dividend Declaration?\nThis may take few minutes!!!"))
  {
    location = "<%=request.getContextPath()%>/jsp/Dividend/SaveDividend.jsp"
  }
}

 function askreject()
 {
  if (confirm("Do you want to Reject the Dividend Declaration?"))
  {
    SubmitThis();
  }
 }

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String query1 = "SELECT * FROM DIVIDENDDECLARATION_T_VIEW";
    cm.queryExecute(query1);

    double divperc = 0;
    String decdate = "";
    String periodfrom = "";
    String periodto = "";
    String dest = request.getContextPath() + "/jsp/Dividend/EditTempDividend.jsp";
%>
  <span class="style7">
  <form name="FormToSubmit" method="GET" action="RejectDeclaredDividends.jsp">
  <img name="B4" src="<%=request.getContextPath()%>/images/btnAccept.gif" onclick="toaccepteddividends()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAcceptR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAccept.gif'">
  <img name="B6" src="<%=request.getContextPath()%>/images/btnReject.gif" onclick="askreject()" onMouseOver="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnRejectR.gif'" onMouseOut="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnReject.gif'">
  <table width="100%" BORDER=1  cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Dividend Under Process</center></td></tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="25%" class="style9"><div align="center">Date Declared</div></td>
    <td width="25%"><div align="center" class="style9">
      <div align="center">Period From</div>
    </div></td>
    <td width="25%"><div align="center" class="style9">
      <div align="center">Period To</div>
    </div></td>
    <td width="25%"><div align="center" class="style9">
      <div align="center">Dividend</div>
    </div></td>
  </tr>
  <div align="left">
     <%
    while(cm.toNext())
     {
       divperc = cm.getColumnF("DEC_DIV");
       decdate = cm.getColumnDT("DATE_DEC");
       periodfrom = cm.getColumnDT("PERIOD_FROM");
       periodto = cm.getColumnDT("PERIOD_TO");

       if (String.valueOf(decdate).equals("null"))
         decdate = "";
       if (String.valueOf(periodfrom).equals("null"))
         periodfrom = "";
       if (String.valueOf(periodto).equals("null"))
         periodto = "";

       if (!decdate.equalsIgnoreCase(""))
       {
//         String decdate1 = ut.changeDateFormat(decdate,"MM/dd/yyyy h:m:s","dd/MM/yyyy");
//         periodfrom = ut.changeDateFormat(periodfrom,"MM/dd/yyyy h:m:s","dd/MM/yyyy");
//         periodto = ut.changeDateFormat(periodto,"MM/dd/yyyy h:m:s","dd/MM/yyyy");
         %>
    </div>
  <tr bgcolor="#E8F3FD">
           <td class="style13"><div align="left" class="style12">
             <div align="center">
               &nbsp;
               <a HREF="<%=dest%>?declarationdate=<%=decdate%>"><%=decdate%></a>
               &nbsp;
             </div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=periodfrom%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">&nbsp;&nbsp;<%=periodto%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=divperc%> %&nbsp;</div>
           </div></td>
         </tr>
             <%
         }
     }
     %>
       </form>
     <%
   if (isConnect)
   {
     cm.takeDown();
   }
  %>
</body>
</html>
