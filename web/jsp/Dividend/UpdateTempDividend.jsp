<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : November 2005
'Page Purpose  : Save temporary Dividend Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.util.Date,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Temporary Dividend</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String DivType = String.valueOf(request.getParameter("DivType"));
  String DivPerc = String.valueOf(request.getParameter("divpercent"));
  String RecDate = String.valueOf(request.getParameter("recdate"));
  String PeriodFrom = String.valueOf(request.getParameter("periodfrom"));
  String PeriodTo = String.valueOf(request.getParameter("periodto"));
  String IndResTax = String.valueOf(request.getParameter("indrestax"));
  String IndNonResTax = String.valueOf(request.getParameter("indnonrestax"));
  String CompResTax = String.valueOf(request.getParameter("comprestax"));
  String CompNonResTax = String.valueOf(request.getParameter("compnonrestax"));
   // Added to include tax for tin holders  12 Oct 2014
  String IndResTaxtin = String.valueOf(request.getParameter("indrestaxtin"));
  String IndNonResTaxtin = String.valueOf(request.getParameter("indnonrestaxtin"));
  String CompResTaxtin = String.valueOf(request.getParameter("comprestaxtin"));
  String CompNonResTaxtin = String.valueOf(request.getParameter("compnonrestaxtin"));
  
  String SpecialTax = String.valueOf(request.getParameter("sptax"));
  String IndTaxFree = String.valueOf(request.getParameter("indtfreediv"));
  String CompTaxFree = String.valueOf(request.getParameter("comptfreediv"));
  String Ded1Amount = String.valueOf(request.getParameter("deduction1"));
  String Ded1Reason = String.valueOf(request.getParameter("ded1reason"));
  String Ded1Criteria = String.valueOf(request.getParameter("ded1criteria"));
  String Ded2Amount = String.valueOf(request.getParameter("deduction2"));
  String Ded2Reason = String.valueOf(request.getParameter("ded2reason"));
  String Ded2Criteria = String.valueOf(request.getParameter("ded2criteria"));
  String QualDate = String.valueOf(request.getParameter("qualdate"));
  String PayDate = String.valueOf(request.getParameter("paydate"));
  String FinYear = String.valueOf(request.getParameter("finyear"));
  String BankName = String.valueOf(request.getParameter("bnkname"));
  String BranchName = String.valueOf(request.getParameter("bchname"));
  String BankAddress = String.valueOf(request.getParameter("bnkaddress"));
  String BankAccNo = String.valueOf(request.getParameter("bnkacc"));

  DivType=cm.replace(DivType,"'","''");
  DivPerc=cm.replace(DivPerc,"'","''");
  RecDate=cm.replace(RecDate,"'","''");
  PeriodFrom=cm.replace(PeriodFrom,"'","''");
  PeriodTo=cm.replace(PeriodTo,"'","''");
  IndResTax=cm.replace(IndResTax,"'","''");
  IndNonResTax=cm.replace(IndNonResTax,"'","''");
  CompResTax=cm.replace(CompResTax,"'","''");
  CompNonResTax=cm.replace(CompNonResTax,"'","''");
  // Added new 12 Oct 2014 
  IndResTaxtin=cm.replace(IndResTaxtin,"'","''");
  IndNonResTaxtin=cm.replace(IndNonResTaxtin,"'","''");
  CompResTaxtin=cm.replace(CompResTaxtin,"'","''");
  CompNonResTaxtin=cm.replace(CompNonResTaxtin,"'","''");
  
  SpecialTax=cm.replace(SpecialTax,"'","''");
  IndTaxFree=cm.replace(IndTaxFree,"'","''");
  CompTaxFree=cm.replace(CompTaxFree,"'","''");
  Ded1Amount=cm.replace(Ded1Amount,"'","''");
  Ded1Reason=cm.replace(Ded1Reason,"'","''");
  Ded1Criteria=cm.replace(Ded1Criteria,"'","''");
  Ded2Amount=cm.replace(Ded2Amount,"'","''");
  Ded2Reason=cm.replace(Ded2Reason,"'","''");
  Ded2Criteria=cm.replace(Ded2Criteria,"'","''");
  QualDate=cm.replace(QualDate,"'","''");
  PayDate=cm.replace(PayDate,"'","''");
  FinYear=cm.replace(FinYear,"'","''");
  BankName=cm.replace(BankName,"'","''");
  BranchName=cm.replace(BranchName,"'","''");
  BankAddress=cm.replace(BankAddress,"'","''");
  BankAccNo=cm.replace(BankAccNo,"'","''");

  if (String.valueOf(SpecialTax).equals("null"))
   SpecialTax = "";
  if (String.valueOf(Ded1Amount).equals("null"))
   Ded1Amount = "";
  if (String.valueOf(Ded1Reason).equals("null"))
   Ded1Reason = "";
  if (String.valueOf(Ded1Criteria).equals("null"))
   Ded1Criteria = "";
  if (String.valueOf(Ded2Amount).equals("null"))
   Ded2Amount = "";
  if (String.valueOf(Ded2Reason).equals("null"))
   Ded2Reason = "";
  if (String.valueOf(Ded2Criteria).equals("null"))
     Ded2Criteria = "";

  StringTokenizer BankSTK = new StringTokenizer(BankName,"%");
  BankName = BankSTK.nextToken();

  double iDivPerc = Double.parseDouble(DivPerc);
  double iIndResTax = Double.parseDouble(IndResTax);
  double iIndNonResTax = Double.parseDouble(IndNonResTax);
  double iCompResTax = Double.parseDouble(CompResTax);
  double iCompNonResTax = Double.parseDouble(CompNonResTax);
   // Added new 12 Oct 2014 
   double iIndResTaxtin = Double.parseDouble(IndResTaxtin);
  double iIndNonResTaxtin = Double.parseDouble(IndNonResTaxtin);
  double iCompResTaxtin = Double.parseDouble(CompResTaxtin);
  double iCompNonResTaxtin = Double.parseDouble(CompNonResTaxtin);
  
  double iIndTaxFree = Double.parseDouble(IndTaxFree);
  double iCompTaxFree = Double.parseDouble(CompTaxFree);
  double iSpecialTax = 0;
  double iDed1Amount = 0;
  double iDed2Amount = 0;
  if (!SpecialTax.equalsIgnoreCase(""))
  {
    iSpecialTax = Double.parseDouble(SpecialTax);
  }
  if (!Ded1Amount.equalsIgnoreCase(""))
  {
    iDed1Amount = Double.parseDouble(Ded1Amount);
  }
  if (!Ded2Amount.equalsIgnoreCase(""))
  {
    iDed2Amount = Double.parseDouble(Ded2Amount);
  }

  iDivPerc = bcal.roundtovalue(iDivPerc,4);
  iIndResTax = bcal.roundtovalue(iIndResTax,4);
  iIndNonResTax = bcal.roundtovalue(iIndNonResTax,4);
  iCompResTax = bcal.roundtovalue(iCompResTax,4);
  iCompNonResTax = bcal.roundtovalue(iCompNonResTax,4);
  // Added new 12 Oct 2014 
  iIndResTaxtin = bcal.roundtovalue(iIndResTaxtin,4);
  iIndNonResTaxtin = bcal.roundtovalue(iIndNonResTaxtin,4);
  iCompResTaxtin = bcal.roundtovalue(iCompResTaxtin,4);
  iCompNonResTaxtin = bcal.roundtovalue(iCompNonResTaxtin,4);
  
  iSpecialTax = bcal.roundtovalue(iSpecialTax,4);
  iIndTaxFree = bcal.roundtovalue(iIndTaxFree,4);
  iCompTaxFree = bcal.roundtovalue(iCompTaxFree,4);
  iDed1Amount = bcal.roundtovalue(iDed1Amount,4);
  iDed2Amount = bcal.roundtovalue(iDed2Amount,4);

  int errorcounter = 0;

  String query1 = "SELECT * FROM DIVIDENDDECLARATION_VIEW WHERE DATE_DEC = TO_DATE('" + RecDate + "','DD/MM/YYYY')";
  int dupprocessed = cm.queryExecuteCount(query1);

  if (dupprocessed > 0)
  {
     errorcounter++;
     %>
       <script language="javascript">
        alert("A Dividend has already been Declared on this Date!");
        history.go(-1);
       </script>
     <%
  }

  String folio_no = "";
  int totalshare = 0;
  int taxfreeshare = 0;
  int liableshare = 0;
  double taxrate = 0;
  double maxtaxfreeamount = 0;
  double nPayable = 0;
  double nDeduction1 = 0;
  double nDeduction2 = 0;
  double nIncomeTax = 0;
  double nDividend = 0;
  String holderclass = "";
  String isLiableToTax = "";
  String isSpecialTax = "";
  String isResident = "";
  String isActive = "";
  String curWarrant = "";
  String tin = "";  // Added new Oct 2014
  if (errorcounter == 0)
  {
   String ndividend = "call UPDATE_DIVIDENDDECLARATION_T('" + DivType + "', " + iDivPerc + ", '" + RecDate + "', '" + PeriodFrom + "', '" + PeriodTo + "', " + iIndResTax + "," + iIndNonResTax + "," + iCompResTax + "," + iCompNonResTax + "," + iSpecialTax + "," + iIndTaxFree + "," + iCompTaxFree + "," + iDed1Amount + ",'" + Ded1Reason + "','" + Ded1Criteria + "'," + iDed2Amount + ",'" + Ded2Reason + "','" + Ded2Criteria + "','" + QualDate + "','" + PayDate + "','" + FinYear + "','" + BankName + "','" + BranchName + "','" + BankAddress + "','" + BankAccNo+ "', " + iIndResTaxtin + "," + iIndNonResTaxtin + "," + iCompResTaxtin + "," + iCompNonResTaxtin  + ")";
   boolean b = cm.procedureExecute(ndividend);

   String ddiv = "call DELETE_DIVIDEND_T()";
   b = cm.procedureExecute(ddiv);

   ddiv = "call DELETE_BODIVIDEND_T()";
   b = cm.procedureExecute(ddiv);

   String query3 = "SELECT * FROM SHAREHOLDER_VIEW ORDER BY FOLIO_NO";
   cm.queryExecute(query3);

   curWarrant = cm1.LastWarrant();
   int icurWarrant = Integer.parseInt(curWarrant);
   icurWarrant--;

   while(cm.toNext())
   {
    folio_no = cm.getColumnS("FOLIO_NO");
    totalshare = cm.getColumnI("TOTAL_SHARE");
    taxfreeshare = cm.getColumnI("TAX_FREE_SHARE");
    holderclass = cm.getColumnS("CLASS");
    isLiableToTax = cm.getColumnS("LIABLE_TO_TAX");
    isSpecialTax = cm.getColumnS("SPECIAL_TAX");
    isResident = cm.getColumnS("RESIDENT");
    isActive = cm.getColumnS("ACTIVE");
    tin = cm.getColumnS("TIN"); 
	
    icurWarrant++;
    curWarrant = String.valueOf(icurWarrant);

 if (String.valueOf(tin).equals("null"))
      tin = "";
    tin = tin.trim();
    if (String.valueOf(folio_no).equals("null"))
      folio_no = "";
    if (String.valueOf(totalshare).equals("null"))
      totalshare = 0;
    if (String.valueOf(taxfreeshare).equals("null"))
      taxfreeshare = 0;
    if (String.valueOf(holderclass).equals("null"))
      holderclass = "";
    if (String.valueOf(isLiableToTax).equals("null"))
      isLiableToTax = "";
    if (String.valueOf(isSpecialTax).equals("null"))
      isSpecialTax = "";
    if (String.valueOf(isResident).equals("null"))
      isResident = "";
    if (String.valueOf(isActive).equals("null"))
      isActive = "";
    if (isActive.equalsIgnoreCase("T"))
    {
     liableshare = totalshare - taxfreeshare;

     if (holderclass.equalsIgnoreCase("A"))
     {
      if (isResident.equalsIgnoreCase("T"))
      {
	   if (tin.equalsIgnoreCase(""))  // Added new to check if TIN exists then tax rate will be for individual resident with tin else without tin tax rate 12 Oct 2014
       {
        taxrate = iIndResTax;
	   }
	   else
	   {
	    taxrate = iIndResTaxtin;
		System.out.println("folio = "+folio_no+" with tin "+tin+" and tax rate "+iIndResTaxtin);
	   }
      }
      else
      {
	  if (tin.equalsIgnoreCase(""))  // Added new to check if TIN exists then tax rate will be for individual resident with tin else without tin tax rate 12 Oct 2014
       {
        taxrate = iIndNonResTax;
	   }
	   else
	   {
	    taxrate = iIndNonResTaxtin;
		System.out.println("folio = "+folio_no+" with tin "+tin+" and tax rate "+iIndNonResTaxtin);
	   }	  
      }
      maxtaxfreeamount = iIndTaxFree;
     }
    else
    {
     if (isResident.equalsIgnoreCase("T"))
     {
	 if (tin.equalsIgnoreCase(""))  // Added new to check if TIN exists then tax rate will be for individual resident with tin else without tin tax rate 12 Oct 2014
       {
        taxrate = iCompResTax;
	   }
	   else
	   {
	    taxrate = iCompResTaxtin;
		System.out.println("folio = "+folio_no+" with tin "+tin+" and tax rate "+iCompResTaxtin);
	   }	       
     }
     else
     {
	 if (tin.equalsIgnoreCase(""))  // Added new to check if TIN exists then tax rate will be for individual resident with tin else without tin tax rate 12 Oct 2014
       {
        taxrate = iCompNonResTax;
	   }
	   else
	   {
	    taxrate = iCompNonResTaxtin;
		System.out.println("folio = "+folio_no+" with tin "+tin+" and tax rate "+iCompResTaxtin);
	   }		 
     }
     maxtaxfreeamount = iCompTaxFree;
    }
  /*  if (isActive.equalsIgnoreCase("T"))
    {
     liableshare = totalshare - taxfreeshare;

     if (holderclass.equalsIgnoreCase("A"))
     {
      if (isResident.equalsIgnoreCase("T"))
      {
        taxrate = iIndResTax;
      }
      else
      {
       taxrate = iIndNonResTax;
      }
      maxtaxfreeamount = iIndTaxFree;
     }
    else
    {
     if (isResident.equalsIgnoreCase("T"))
     {
       taxrate = iCompResTax;
     }
     else
     {
       taxrate = iCompNonResTax;
     }
     maxtaxfreeamount = iCompTaxFree;
    }
*/
    if (isSpecialTax.equalsIgnoreCase("T"))
    {
      iSpecialTax = 0;

      String query3spb = "SELECT * FROM SPECIAL_TAX WHERE FOLIO_NO = '" + folio_no + "'";
      cm1.queryExecute(query3spb);

      while(cm1.toNext())
      {
        iSpecialTax = cm1.getColumnF("AMOUNT");
      }

      taxrate = iSpecialTax;
      maxtaxfreeamount = iCompTaxFree;
    }

    nPayable = (totalshare * iDivPerc)/10;
    nDeduction1 = (nPayable * iDed1Amount)/100;
    nDeduction2 = (nPayable * iDed2Amount)/100;

    if (nPayable > maxtaxfreeamount)
    {
      nIncomeTax = ((totalshare - taxfreeshare) * iDivPerc/10) * taxrate/100;
    }
    else
    {
      nIncomeTax = 0;
    }

    if (isLiableToTax.equalsIgnoreCase("F"))
    {
      nIncomeTax = 0;
    }

    nDividend = nPayable - nIncomeTax - nDeduction1 - nDeduction2;

    String sdividend = "call ADD_DIVIDEND_T('" + folio_no + "', " + totalshare + ", " + liableshare + ", " + nDividend + ", '" + curWarrant + "', " + nDeduction1 + "," + nDeduction2 + "," + nIncomeTax + ",'" + RecDate + "','F','F','F','F','','F','F','','F','')";
    boolean bB1 = cm1.procedureExecute(sdividend);
   }
   }

   String boid = "";

//   String query3BO = "SELECT * FROM BOHOLDINGONLY_VIEW ORDER BY BOID";
   String query3BO = "SELECT * FROM BOHOLDING_VIEW ORDER BY BOID";
   cm.queryExecute(query3BO);

   curWarrant = cm1.LastWarrant();
   icurWarrant = Integer.parseInt(curWarrant);
   icurWarrant--;

   while(cm.toNext())
   {
    boid = cm.getColumnS("BOID");
    totalshare = cm.getColumnI("CURRBAL");
    taxfreeshare = cm.getColumnI("TAX_FREE_SHARE");
    holderclass = cm.getColumnS("CLASS");
    isLiableToTax = cm.getColumnS("LIABLE_TO_TAX");
    isSpecialTax = cm.getColumnS("SPECIAL_TAX");
    isResident = cm.getColumnS("RESIDENT");
    tin = cm.getColumnS("TIN");
//    taxfreeshare = 0;
//    holderclass = "A";
//    isLiableToTax = "T";
//    isSpecialTax = "F";
//    isResident = "T";

    icurWarrant++;
    curWarrant = String.valueOf(icurWarrant);
    if (String.valueOf(tin).equals("null"))
      tin = "";
	  
	  tin = tin.trim();
    if (String.valueOf(boid).equals("null"))
      boid = "";
    if (String.valueOf(totalshare).equals("null"))
      totalshare = 0;
    if (String.valueOf(taxfreeshare).equals("null"))
      taxfreeshare = 0;
    if (String.valueOf(holderclass).equals("null"))
      holderclass = "";
    if (String.valueOf(isLiableToTax).equals("null"))
      isLiableToTax = "";
    if (String.valueOf(isSpecialTax).equals("null"))
      isSpecialTax = "";
    if (String.valueOf(isResident).equals("null"))
      isResident = "";

     liableshare = totalshare - taxfreeshare;
if (holderclass.equalsIgnoreCase("A"))
     {
      if (isResident.equalsIgnoreCase("T"))
      {if (tin.equalsIgnoreCase(""))  // Added new to check if TIN exists then tax rate will be for individual resident with tin else without tin tax rate 12 Oct 2014
       {
        taxrate = iIndResTax;
	   }
	   else
	   {
	    taxrate = iIndResTaxtin;
		System.out.println("boid = "+boid+" with tin "+tin+" and tax rate "+iIndResTaxtin);
	   }
        
      }
      else
      {
	   if (tin.equalsIgnoreCase(""))  // Added new to check if TIN exists then tax rate will be for individual resident with tin else without tin tax rate 12 Oct 2014
       {
        taxrate = iIndNonResTax;
	   }
	   else
	   {
	    taxrate = iIndNonResTaxtin;
		System.out.println("boid = "+boid+" with tin "+tin+" and tax rate "+iIndNonResTaxtin);
	   }	
        
      }
      maxtaxfreeamount = iIndTaxFree;
     }
    else
    {
     if (isResident.equalsIgnoreCase("T"))
     {
	  if (tin.equalsIgnoreCase(""))  // Added new to check if TIN exists then tax rate will be for individual resident with tin else without tin tax rate 12 Oct 2014
       {
        taxrate = iCompResTax;
	   }
	   else
	   {
	    taxrate = iCompResTaxtin;
		System.out.println("boid = "+boid+" with tin "+tin+" and tax rate "+iCompResTaxtin);
	   }	
       
     }
     else
     { 
	  if (tin.equalsIgnoreCase(""))  // Added new to check if TIN exists then tax rate will be for individual resident with tin else without tin tax rate 12 Oct 2014
       {
        taxrate = iCompNonResTax;
	   }
	   else
	   {
	    taxrate = iCompNonResTaxtin;
		System.out.println("boid = "+boid+" with tin "+tin+" and tax rate "+iCompNonResTaxtin);
	   }		
      
     }
     maxtaxfreeamount = iCompTaxFree;
    }
    /* if (holderclass.equalsIgnoreCase("A"))
     {
      if (isResident.equalsIgnoreCase("T"))
      {
        taxrate = iIndResTax;
      }
      else
      {
       taxrate = iIndNonResTax;
      }
      maxtaxfreeamount = iIndTaxFree;
     }
    else
    {
     if (isResident.equalsIgnoreCase("T"))
     {
       taxrate = iCompResTax;
     }
     else
     {
       taxrate = iCompNonResTax;
     }
     maxtaxfreeamount = iCompTaxFree;
    }
*/
    if (isSpecialTax.equalsIgnoreCase("T"))
    {
      iSpecialTax = 0;

      String query3spb = "SELECT * FROM SPECIAL_TAX WHERE FOLIO_NO = '" + boid + "'";
      cm1.queryExecute(query3spb);

      while(cm1.toNext())
      {
        iSpecialTax = cm1.getColumnF("AMOUNT");
      }

      taxrate = iSpecialTax;
      maxtaxfreeamount = iCompTaxFree;
    }

    nPayable = (totalshare * iDivPerc)/10;
    nDeduction1 = (nPayable * iDed1Amount)/100;
    nDeduction2 = (nPayable * iDed2Amount)/100;

    if (nPayable > maxtaxfreeamount)
    {
      nIncomeTax = ((totalshare - taxfreeshare) * iDivPerc/10) * taxrate/100;
    }
    else
    {
      nIncomeTax = 0;
    }

    if (isLiableToTax.equalsIgnoreCase("F"))
    {
      nIncomeTax = 0;
    }

    nDividend = nPayable - nIncomeTax - nDeduction1 - nDeduction2;

    String sdividend1 = "call ADD_BODIVIDEND_T('" + boid + "', " + totalshare + ", " + liableshare + ", " + nDividend + ", '" + curWarrant + "', " + nDeduction1 + "," + nDeduction2 + "," + nIncomeTax + ",'" + RecDate + "','F','F','F','F','','F','F','','F','')";
    boolean bB2 = cm1.procedureExecute(sdividend1);
   }

  }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Edited Declared Dividend Information')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
     cm1.takeDown();
   }
%>
<script language="javascript">
  alert("Dividend Information Updated Successfully!");
  location = "<%=request.getContextPath()%>/jsp/Dividend/AcceptDividend.jsp";
</script>
</body>
</html>
