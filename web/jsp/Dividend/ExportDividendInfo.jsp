<script>
/*
'******************************************************************************************************************************************
'Script Author : Rahat
'Creation Date : April 2010
'Page Purpose  : Export ShareHolder and BOAddress info in Excel for Merge purpose
'******************************************************************************************************************************************
*/
</script>
<%@ page language="java" import="org.apache.poi.hssf.usermodel.*,java.util.Vector,java.io.*,java.util.*,java.util.List,java.util.Vector" %>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cal"  class="batbsms.batCalculations"/>
<jsp:useBean id="numConvert"  class="batbsms.NumToWords"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>
<html>
<head>
<title>
Export Dividend Information
</title>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update User</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>
<body bgcolor="#ffffff">
<h1>
JBuilder Generated JSP
</h1>
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
	
    String DivDate = String.valueOf(request.getParameter("dateselect"));
    //String TypeVal = String.valueOf(request.getParameter("orderby"));
    /*TypeVal="CDBL";
    DivDate="03/04/2005";*/
	
   String[] Divdate1 = DivDate.split("/");
   //CDBL
   String boid="";
   String total_share="";
   String dividend= "";
   String warrant_no="";
   String income_tax="";
   String net_dividend="";
   String issue_date="";
   String name_firstholder="";
   String address= "";
   String city="";
   String state="";
   String postal_code="";
   String country="";
   String phone= "";
   String fax="";
   String email="";
   String bankName="";
   String branchName="";
   String accountNo="";
   // Added new 12 Oct 2014 
   String routingno="";
   String tin="";

   HSSFWorkbook wb = new HSSFWorkbook();
   HSSFSheet sheet = wb.createSheet("Export Dividend Data");

   HSSFCellStyle style = wb.createCellStyle();
   style.setBorderTop((short) 1); // double lines border
   style.setBorderBottom((short) 1); // single line border
   HSSFFont font = wb.createFont();
   font.setFontName(HSSFFont.FONT_ARIAL);
   font.setFontHeightInPoints((short) 12);
   font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
   style.setFont(font);

   HSSFRow row =null;
   HSSFCell cell =null;

   HSSFCellStyle alignRight = wb.createCellStyle();
   alignRight.setAlignment(HSSFCellStyle.ALIGN_RIGHT);

   HSSFCellStyle alignCenter = wb.createCellStyle();
   alignCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);

   // Create a cell and put a value in it.


   //CDBL
   String TableInfo = "Select BO_ID, TOTAL_SHARE, DIVIDEND, WARRANT_NO, INCOME_TAX, NET_DIVIDEND, ISSUE_DATE, NAME_FIRSTHOLDER, ADDRESS, CITY, STATE, POSTAL_CODE, COUNTRY, PHONE, FAX, EMAIL, BANK_NAME, BRANCH_NAME, ACCOUNT_NO, ROUTING_NO, TIN from BODIVIDEND_EXCEL_VIEW where ISSUE_DATE=TO_DATE('" + DivDate + "','DD/MM/YYYY') AND warrant_no not in(Select OMNIBUS_BO_DIVIDEND.warrent_no from BODiVIDEND,OMNIBUS_BO_DIVIDEND, omnibus_config_dividend where BODiVIDEND.warrant_no=OMNIBUS_BO_DIVIDEND.warrent_no and omnibus_config_dividend.rec_id=OMNIBUS_BO_DIVIDEND.omnibus_div_id and omnibus_config_dividend.status='ACCEPTED' and OMNIBUS_BO_DIVIDEND.ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY')) ORDER BY WARRANT_NO";
   // 12-10-2014  String TableInfo = "Select BO_ID, TOTAL_SHARE, DIVIDEND, WARRANT_NO, INCOME_TAX, NET_DIVIDEND, ISSUE_DATE, NAME_FIRSTHOLDER, ADDRESS, CITY, STATE, POSTAL_CODE, COUNTRY, PHONE, FAX, EMAIL, BANK_NAME, BRANCH_NAME, ACCOUNT_NO from BODIVIDEND_EXCEL_VIEW where ISSUE_DATE=TO_DATE('" + DivDate + "','DD/MM/YYYY') AND warrant_no not in(Select OMNIBUS_BO_DIVIDEND.warrent_no from BODiVIDEND,OMNIBUS_BO_DIVIDEND, omnibus_config_dividend where BODiVIDEND.warrant_no=OMNIBUS_BO_DIVIDEND.warrent_no and omnibus_config_dividend.rec_id=OMNIBUS_BO_DIVIDEND.omnibus_div_id and omnibus_config_dividend.status='ACCEPTED' and OMNIBUS_BO_DIVIDEND.ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY')) ORDER BY WARRANT_NO";
   //String TableInfo = "Select BO_ID, TOTAL_SHARE, DIVIDEND, WARRANT_NO, INCOME_TAX, NET_DIVIDEND, ISSUE_DATE, NAME_FIRSTHOLDER, ADDRESS, CITY, STATE, POSTAL_CODE, COUNTRY, PHONE, FAX, EMAIL from BODIVIDEND_EXCEL_VIEW where ISSUE_DATE=TO_DATE('" + DivDate + "','DD/MM/YYYY') ORDER BY WARRANT_NO";
     //"Select bd.bo_id, bd.ISSUE_DATE, bd.dividend, bd.warrant_no, bd.total_share,ba.NAME_FIRSTHOLDER, ba.address, ba.city, ba.country, ba.phone, ba.state,ba.POSTAL_CODE from bodividend BD, boaddress BA where bd.bo_id=ba.bo_id and bd.ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') ORDER BY bd.BO_ID";


     //System.out.println("[1] " +TableInfo);
     cm.queryExecute(TableInfo);

     // Create a row and put some cells in it. Rows are 0 based.
     row = sheet.createRow((short)0);
     row.createCell((short)3).setCellValue("BRITISH AMERICAN TOBACCO BANGLADESH");
     cell = row.getCell((short)3);
     cell.setCellStyle(style);

     row = sheet.createRow((short)1);
	      row.createCell((short)3).setCellValue("DIVIDEND INFORMATION FOR THE YEAR "+Divdate1[2]);

     cell = row.getCell((short)3);
     cell.setCellStyle(style);
     row = sheet.createRow((short)2);
	 row = sheet.createRow((short)3);

     row.createCell((short)0).setCellValue("BO ID/FOLIO NO");
     cell = row.getCell((short)0);
     cell.setCellStyle(style);

     row.createCell((short)1).setCellValue("SHARE HOLDER NAME");
     cell = row.getCell((short)1);
     cell.setCellStyle(style);
     row.createCell((short)2).setCellValue("ADDRESS");
     cell = row.getCell((short)2);
     cell.setCellStyle(style);

     cell.setCellStyle(style);
     row.createCell((short)3).setCellValue("TOTAL SHARE");
     cell = row.getCell((short)3);
     cell.setCellStyle(style);
//     row.createCell((short)4).setCellValue("NET DIVIDEND");
	 row.createCell((short)4).setCellValue("GROSS DIVIDEND");
     cell = row.getCell((short)4);
     cell.setCellStyle(style);
     row.createCell((short)5).setCellValue("TAX");
     cell = row.getCell((short)5);
     cell.setCellStyle(style);
	 row.createCell((short)6).setCellValue("NET DIVIDEND");
     //row.createCell((short)6).setCellValue("GROSS DIVIDEND");
//	 System.out.println("[1] @ line 155" );
     cell = row.getCell((short)6);
     cell.setCellStyle(style);
     row.createCell((short)7).setCellValue("WARRANT NO");
     cell = row.getCell((short)7);
     cell.setCellStyle(style);
     row.createCell((short)8).setCellValue("AMOUNT IN WORD");
     cell = row.getCell((short)8);
     cell.setCellStyle(style);
     row.createCell((short)9).setCellValue("BANK NAME");
     cell = row.getCell((short)9);
     cell.setCellStyle(style);
	
     row.createCell((short)10).setCellValue("BRANCH NAME");
     cell = row.getCell((short)10);
     cell.setCellStyle(style);
     row.createCell((short)11).setCellValue("ACCOUNT NO");
     cell = row.getCell((short)11);
     cell.setCellStyle(style);
	 row.createCell((short)12).setCellValue("ROUTING NO"); // Added on 12 10 2014
     cell = row.getCell((short)12);
     cell.setCellStyle(style);
	 row.createCell((short)13).setCellValue("TIN");
     cell = row.getCell((short)13);
     cell.setCellStyle(style);

     int i=4;
     while(cm.toNext())
     {
       boid= cm.getColumnS("BO_ID");
       total_share= cm.getColumnS("TOTAL_SHARE");
       dividend= cm.getColumnS("DIVIDEND");
       warrant_no= cm.getColumnS("WARRANT_NO");
       income_tax= cm.getColumnS("INCOME_TAX");
       net_dividend= cm.getColumnS("NET_DIVIDEND");
       issue_date= cm.getColumnDT("ISSUE_DATE");
       name_firstholder= cm.getColumnS("NAME_FIRSTHOLDER");
       address= cm.getColumnS("ADDRESS");
       city=cm.getColumnS("CITY");
       bankName=cm.getColumnS("BANK_NAME");
       branchName=cm.getColumnS("BRANCH_NAME");
       accountNo=cm.getColumnS("ACCOUNT_NO");
	   routingno=cm.getColumnS("ROUTING_NO");
	   tin=cm.getColumnS("TIN");
      // String totalShareS=c
	  if(routingno==null || routingno=="" || routingno=="null")
       routingno="";
	  if(tin==null || tin=="" || tin=="null")
       tin="";
       if(total_share==null || total_share=="" || total_share=="null")
       total_share="0";
       if(dividend==null || dividend=="" || dividend=="null")
       dividend="0";
       if(income_tax==null || income_tax=="" || income_tax=="null")
       income_tax="0";
       if(net_dividend==null || net_dividend=="" || net_dividend=="null")
       net_dividend="0";

       //double totalShareD=cal.roundtovalue(Double.parseDouble(total_share),2);
       double dividendD=cal.roundtovalue(Double.parseDouble(dividend),2);
       double income_taxD=cal.roundtovalue(Double.parseDouble(income_tax),2);
       double net_dividendD=cal.roundtovalue(Double.parseDouble(net_dividend),2);

       String dividendS=String.valueOf(dividendD);
       String income_taxS=String.valueOf(income_taxD);
       String net_dividendS=String.valueOf(net_dividendD);

       if(dividendS.indexOf(".")==dividendS.length()-2)
        dividendS=dividendS+"0";
       if(income_taxS.indexOf(".")==income_taxS.length()-2)
        income_taxS=income_taxS+"0";
       if(net_dividendS.indexOf(".")==net_dividendS.length()-2)
        net_dividendS=net_dividendS+"0";

       HSSFRow rowi = sheet.createRow((short)i);

       rowi.createCell((short)0).setCellValue(boid);
       rowi.createCell((short)1).setCellValue(name_firstholder);
       rowi.createCell((short)2).setCellValue(address+","+city);
       //rowi.createCell((short)3).setCellValue(total_share);
       cell=rowi.createCell((short)3);
       cell.setCellStyle(alignRight);
       cell.setCellValue(total_share);

       //rowi.createCell((short)4).setCellValue(dividend);
       cell=rowi.createCell((short)4);
       cell.setCellStyle(alignRight);
	   cell.setCellValue(net_dividendS);
       //cell.setCellValue(dividendS);
       //rowi.createCell((short)5).setCellValue(income_tax);
       cell=rowi.createCell((short)5);
       cell.setCellStyle(alignRight);
       cell.setCellValue(income_taxS);
       //rowi.createCell((short)6).setCellValue(net_dividend);
       cell=rowi.createCell((short)6);
       cell.setCellStyle(alignRight);
	   cell.setCellValue(dividendS);
       //cell.setCellValue(net_dividendS);
       //rowi.createCell((short)7).setCellValue(warrant_no);
       cell=rowi.createCell((short)7);
       alignCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);
       cell.setCellStyle(alignCenter);
       cell.setCellValue(warrant_no);
       //rowi.createCell((short)8).setCellValue(warrant_no); added by Jahir 05.12.2012
       cell=rowi.createCell((short)8);
       //alignCenter.setAlignment(HSSFCellStyle.ALIGN_LEFT);
       //cell.setCellStyle(alignCenter);
       //System.out.println("--------------------"+net_dividendS+"----------------");
       if(dividendS!=null && !dividendS.equalsIgnoreCase(""))
       cell.setCellValue(numConvert.convert(dividendS));
       //cell.setCellValue(numConvert.convert(net_dividend));
       cell=rowi.createCell((short)9);
       cell.setCellValue(bankName);
       cell=rowi.createCell((short)10);
       cell.setCellValue(branchName);
       cell=rowi.createCell((short)11);
       cell.setCellValue(accountNo);
	   cell=rowi.createCell((short)12);
       cell.setCellValue(routingno);
	   cell=rowi.createCell((short)13);
       cell.setCellValue(tin);

       i++;
     }

     //align.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
     // fos SMS
     TableInfo="Select FOLIO_NO, TOTAL_SHARE, DIVIDEND, WARRANT_NO, INCOME_TAX, NET_DIVIDEND, ISSUE_DATE, NAME, ADDRESS, ZIP, TELEPHONE, CELLPHONE, EMAIL, FAX, ROUTING_NO,TIN from DIVIDEND_EXCEL_VIEW where ISSUE_DATE=TO_DATE('" + DivDate + "','DD/MM/YYYY') order by WARRANT_NO";
	 //     TableInfo="Select FOLIO_NO, TOTAL_SHARE, DIVIDEND, WARRANT_NO, INCOME_TAX, NET_DIVIDEND, ISSUE_DATE, NAME, ADDRESS, ZIP, TELEPHONE, CELLPHONE, EMAIL, FAX from DIVIDEND_EXCEL_VIEW where ISSUE_DATE=TO_DATE('" + DivDate + "','DD/MM/YYYY') order by WARRANT_NO";
//System.out.println("[2] " +TableInfo);
     //System.out.println(TableInfo);
     cm.queryExecute(TableInfo);
     while(cm.toNext())
     {
       boid= cm.getColumnS("FOLIO_NO");
       total_share= cm.getColumnS("TOTAL_SHARE");
       dividend= cm.getColumnS("DIVIDEND");
       warrant_no= cm.getColumnS("WARRANT_NO");
       income_tax= cm.getColumnS("INCOME_TAX");
       net_dividend= cm.getColumnS("NET_DIVIDEND");
       issue_date= cm.getColumnDT("ISSUE_DATE");
       name_firstholder= cm.getColumnS("NAME");
       address= cm.getColumnS("ADDRESS");
       city=cm.getColumnS("CITY");
	   routingno=cm.getColumnS("ROUTING_NO");
	   tin=cm.getColumnS("TIN");
       // String totalShareS=c
       if(total_share==null || total_share=="" || total_share=="null")
       total_share="0";
       if(dividend==null || dividend=="" || dividend=="null")
       dividend="0";
       if(income_tax==null || income_tax=="" || income_tax=="null")
       income_tax="0";
       if(net_dividend==null || net_dividend=="" || net_dividend=="null")
       net_dividend="0";
	   if(routingno==null || routingno=="" || routingno=="null")
       routingno="";
	  if(tin==null || tin=="" || tin=="null")
       tin="";
       //double totalShareD=cal.roundtovalue(Double.parseDouble(total_share),2);
       double dividendD=cal.roundtovalue(Double.parseDouble(dividend),2);
       double income_taxD=cal.roundtovalue(Double.parseDouble(income_tax),2);
       double net_dividendD=cal.roundtovalue(Double.parseDouble(net_dividend),2);

       String dividendS=String.valueOf(dividendD);
       String income_taxS=String.valueOf(income_taxD);
       String net_dividendS=String.valueOf(net_dividendD);

       if(dividendS.indexOf(".")==dividendS.length()-2)
        dividendS=dividendS+"0";
       if(income_taxS.indexOf(".")==income_taxS.length()-2)
        income_taxS=income_taxS+"0";
       if(net_dividendS.indexOf(".")==net_dividendS.length()-2)
        net_dividendS=net_dividendS+"0";

       HSSFRow rowi = sheet.createRow((short)i);

       rowi.createCell((short)0).setCellValue(boid);
       rowi.createCell((short)1).setCellValue(name_firstholder);
       rowi.createCell((short)2).setCellValue(address+","+city);
       //rowi.createCell((short)3).setCellValue(total_share);
       cell=rowi.createCell((short)3);
       cell.setCellStyle(alignRight);
       cell.setCellValue(total_share);

       //rowi.createCell((short)4).setCellValue(dividend);
       cell=rowi.createCell((short)4);
       cell.setCellStyle(alignRight);
	   cell.setCellValue(net_dividendS);
	   //cell.setCellValue(dividendS);
       //rowi.createCell((short)5).setCellValue(income_tax);
       cell=rowi.createCell((short)5);
       cell.setCellStyle(alignRight);
       cell.setCellValue(income_taxS);
       //rowi.createCell((short)6).setCellValue(net_dividend);
       cell=rowi.createCell((short)6);
       cell.setCellStyle(alignRight);
	   cell.setCellValue(dividendS);
       //cell.setCellValue(net_dividendS);
       cell=rowi.createCell((short)7);
       alignCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);
       cell.setCellStyle(alignCenter);
       cell.setCellValue(warrant_no);
       //rowi.createCell((short)8).setCellValue(warrant_no); added by Jahir 05.12.2012
       cell=rowi.createCell((short)8);
       //alignCenter.setAlignment(HSSFCellStyle.ALIGN_LEFT);
       //cell.setCellStyle(alignCenter);
       //System.out.println("--------------------"+net_dividendS+"----------------");
       if(dividendS!=null && !dividendS.equalsIgnoreCase(""))
       cell.setCellValue(numConvert.convert(dividendS));
       //cell.setCellValue(numConvert.convert(net_dividend));
		cell=rowi.createCell((short)12);
       cell.setCellValue(routingno);
	   cell=rowi.createCell((short)13);
       cell.setCellValue(tin);
       i++;
     }

     // fos Group
     TableInfo="Select NOMINEE_NO, NAME, ADDRESS, WARRANT_NO, INCOME_TAX, DIVIDEND, NET_DIVIDEND, TOTAL_SHARE from GROUP_DIVIDEND_EXCEL_VIEW  where ISSUE_DATE=TO_DATE('" + DivDate + "','DD/MM/YYYY') order by WARRANT_NO";

     //System.out.println(TableInfo);
     cm.queryExecute(TableInfo);
     while(cm.toNext())
     {
       //boid= cm.getColumnS("NOMINEE_NO");
       total_share= cm.getColumnS("TOTAL_SHARE");
       dividend= cm.getColumnS("DIVIDEND");
       warrant_no= cm.getColumnS("WARRANT_NO");
       income_tax= cm.getColumnS("INCOME_TAX");
       net_dividend= cm.getColumnS("NET_DIVIDEND");
       name_firstholder= cm.getColumnS("NAME");
       address= cm.getColumnS("ADDRESS");

      // String totalShareS=c
       if(total_share==null || total_share=="" || total_share=="null")
       total_share="0";
       if(dividend==null || dividend=="" || dividend=="null")
       dividend="0";
       if(income_tax==null || income_tax=="" || income_tax=="null")
       income_tax="0";
       if(net_dividend==null || net_dividend=="" || net_dividend=="null")
       net_dividend="0";

       //double totalShareD=cal.roundtovalue(Double.parseDouble(total_share),2);
       double dividendD=cal.roundtovalue(Double.parseDouble(dividend),2);
       double income_taxD=cal.roundtovalue(Double.parseDouble(income_tax),2);
       double net_dividendD=cal.roundtovalue(Double.parseDouble(net_dividend),2);

       String dividendS=String.valueOf(dividendD);
       String income_taxS=String.valueOf(income_taxD);
       String net_dividendS=String.valueOf(net_dividendD);

       if(dividendS.indexOf(".")==dividendS.length()-2)
        dividendS=dividendS+"0";
       if(income_taxS.indexOf(".")==income_taxS.length()-2)
        income_taxS=income_taxS+"0";
       if(net_dividendS.indexOf(".")==net_dividendS.length()-2)
        net_dividendS=net_dividendS+"0";

       HSSFRow rowi = sheet.createRow((short)i);

       rowi.createCell((short)0).setCellValue(boid);
       rowi.createCell((short)1).setCellValue(name_firstholder);
       rowi.createCell((short)2).setCellValue(address+","+city);
       //rowi.createCell((short)3).setCellValue(total_share);
       cell=rowi.createCell((short)3);
       cell.setCellStyle(alignRight);
       cell.setCellValue(total_share);

       //rowi.createCell((short)4).setCellValue(dividend);
       cell=rowi.createCell((short)4);
       cell.setCellStyle(alignRight);
       //cell.setCellValue(dividendS);
	   cell.setCellValue(net_dividendS);
       //rowi.createCell((short)5).setCellValue(income_tax);
       cell=rowi.createCell((short)5);
       cell.setCellStyle(alignRight);
       cell.setCellValue(income_taxS);
       //rowi.createCell((short)6).setCellValue(net_dividend);
       cell=rowi.createCell((short)6);
       cell.setCellStyle(alignRight);
	   cell.setCellValue(dividendS);
       //cell.setCellValue(net_dividendS);
       //rowi.createCell((short)7).setCellValue(warrant_no);
       cell=rowi.createCell((short)7);
       alignCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);
       cell.setCellStyle(alignCenter);
       cell.setCellValue(warrant_no);
       //rowi.createCell((short)8).setCellValue(warrant_no); added by Jahir 05.12.2012
       cell=rowi.createCell((short)8);
       //alignCenter.setAlignment(HSSFCellStyle.ALIGN_LEFT);
       //cell.setCellStyle(alignCenter);
       //System.out.println("--------------------"+net_dividendS+"----------------");
       if(dividendS!=null && !dividendS.equalsIgnoreCase(""))
       cell.setCellValue(numConvert.convert(dividendS));
       //cell.setCellValue(numConvert.convert(net_dividend));

       i++;
     }




      //--New Add For Omnibus Account

      // fos Omnibus
     //TableInfo="Select NOMINEE_NO, NAME, ADDRESS, WARRANT_NO, INCOME_TAX, DIVIDEND, NET_DIVIDEND, TOTAL_SHARE from GROUP_DIVIDEND_EXCEL_VIEW  where ISSUE_DATE=TO_DATE('" + DivDate + "','DD/MM/YYYY') order by WARRANT_NO";
      TableInfo="select con.name,con.address,omnibus_config_dividend.group_warrant_no,sum(d.TOTAL_SHARE) as TotalShare,(sum(d.dividend)+sum(d.INCOME_TAX)) as GrossDIVIDEND,sum(d.INCOME_TAX) as INCOME_TAX,sum(d.dividend) as NetDividend from OMNIBUS_BO_DIVIDEND o,BODIVIDEND d,omnibus_config_dividend,omnibus_config con where d.ISSUE_DATE=TO_DATE('" + DivDate + "','DD/MM/YYYY') and o.warrent_no= d.warrant_no and omnibus_config_dividend.rec_id= o.omnibus_div_id and con.rec_id=o.omnibus_ac_id and omnibus_config_dividend.status='ACCEPTED' group by con.name,omnibus_config_dividend.group_warrant_no,con.address";
    // System.out.println(TableInfo);
     cm.queryExecute(TableInfo);
     while(cm.toNext())
     {
       //boid= cm.getColumnS("NOMINEE_NO");
       total_share= cm.getColumnS("TotalShare");
       dividend= cm.getColumnS("NetDividend");
       warrant_no= cm.getColumnS("group_warrant_no");
       income_tax= cm.getColumnS("INCOME_TAX");
       net_dividend= cm.getColumnS("GrossDIVIDEND");
       name_firstholder= cm.getColumnS("Name");
       address= cm.getColumnS("address");

      // String totalShareS=c
       if(total_share==null || total_share=="" || total_share=="null")
       total_share="0";
       if(dividend==null || dividend=="" || dividend=="null")
       dividend="0";
       if(income_tax==null || income_tax=="" || income_tax=="null")
       income_tax="0";
       if(net_dividend==null || net_dividend=="" || net_dividend=="null")
       net_dividend="0";

       //double totalShareD=cal.roundtovalue(Double.parseDouble(total_share),2);
       double dividendD=cal.roundtovalue(Double.parseDouble(dividend),2);
       double income_taxD=cal.roundtovalue(Double.parseDouble(income_tax),2);
       double net_dividendD=cal.roundtovalue(Double.parseDouble(net_dividend),2);

       String dividendS=String.valueOf(dividendD);
       String income_taxS=String.valueOf(income_taxD);
       String net_dividendS=String.valueOf(net_dividendD);

       if(dividendS.indexOf(".")==dividendS.length()-2)
        dividendS=dividendS+"0";
       if(income_taxS.indexOf(".")==income_taxS.length()-2)
        income_taxS=income_taxS+"0";
       if(net_dividendS.indexOf(".")==net_dividendS.length()-2)
        net_dividendS=net_dividendS+"0";

       HSSFRow rowi = sheet.createRow((short)i);

       rowi.createCell((short)0).setCellValue(boid);
       rowi.createCell((short)1).setCellValue(name_firstholder);
       rowi.createCell((short)2).setCellValue(address+","+city);
       //rowi.createCell((short)3).setCellValue(total_share);
       cell=rowi.createCell((short)3);
       cell.setCellStyle(alignRight);
       cell.setCellValue(total_share);

       //rowi.createCell((short)4).setCellValue(dividend);
       cell=rowi.createCell((short)4);
       cell.setCellStyle(alignRight);
	   cell.setCellValue(net_dividendS);
       //cell.setCellValue(dividendS);
       //rowi.createCell((short)5).setCellValue(income_tax);
       cell=rowi.createCell((short)5);
       cell.setCellStyle(alignRight);
       cell.setCellValue(income_taxS);
       //rowi.createCell((short)6).setCellValue(net_dividend);
       cell=rowi.createCell((short)6);
       cell.setCellStyle(alignRight);
       //cell.setCellValue(net_dividendS);
	   cell.setCellValue(dividendS);
       //rowi.createCell((short)7).setCellValue(warrant_no);
       cell=rowi.createCell((short)7);
       alignCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);
       cell.setCellStyle(alignCenter);
       cell.setCellValue(warrant_no);
       //rowi.createCell((short)8).setCellValue(warrant_no); added by Jahir 05.12.2012
       cell=rowi.createCell((short)8);
       //alignCenter.setAlignment(HSSFCellStyle.ALIGN_LEFT);
       //cell.setCellStyle(alignCenter);
       //System.out.println("--------------------"+net_dividendS+"----------------");
       if(dividend!=null && !dividend.equalsIgnoreCase("")){
         try{
           cell.setCellValue(numConvert.convert(dividend));
           //cell.setCellValue(numConvert.convert(net_dividend));
         }
         catch(Exception e){}
       }

       i++;
     }













 if (isConnect)
  {
    cm.takeDown();
  }
    // Write the output to a file
    String savepath="";
    ServletContext sc = getServletConfig().getServletContext();
    //Code for creating a folder
   String path=getServletContext().getRealPath("/");
   File f1 = new File(path + "/Excel Export");
   if(!f1.exists())
   {
     f1.mkdir();
   }
   savepath = sc.getRealPath("/") +"Excel Export/"+"Dividendinfo.xls";

    FileOutputStream fileOut = new FileOutputStream(savepath);
    wb.write(fileOut);
    fileOut.close();
    fileOut=null;
    sheet=null;
    wb=null;
%>
<%
   String tempURL =  request.getContextPath() +"/Excel Export/"+"Dividendinfo.xls";
   //System.out.println(tempURL);
%>
<script language="javascript">
showPrintPreview6('<%=tempURL%>');
    location = "<%=request.getContextPath()%>/jsp/Dividend/ExportDividendManagement.jsp";
</script>
</body>
</html>
