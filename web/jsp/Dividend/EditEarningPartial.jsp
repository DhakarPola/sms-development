<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : August 2007
'Page Purpose  : Edit Earning Partial Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}
%>

<html>
<head>
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   String dyear = request.getParameter("dyear");

   String amount = "";

   String query1 = "SELECT AMOUNT FROM VIEW_EARNING_PARTIAL WHERE DEC_YEAR ='"+dyear+ "'";
   try
   {
     cm.queryExecute(query1);
     while(cm.toNext())
     {
       amount = cm.getColumnS("AMOUNT");
       if (String.valueOf(amount).equals("null"))
       amount = "0";
     }
   }catch(Exception e){}
%>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit Earning Partial</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function gotobank()
{
   location = "<%=request.getContextPath()%>/jsp/Dividend/AllEarningPartial.jsp";
}



function formconfirm()
{
  if (confirm("Do you want to change the Earning Partial Information?"))
  {
    return true;
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('dyear','- Year (Option must be entered)');
  BlankValidate('amount','- Amount (Option must be entered)');

  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">

<form action="UpdateEaringPartial.jsp" method="get" enctype="multipart/form-data" name="FileForm">
  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Edit Earning Per Share Information</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Year</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
         <select name="dyear" class="SL2TextField">
         <option>--Please Select--</option>
         <%
         for(int i=2001;i<=2025;i++)
         {
           String temp_i=String.valueOf(i);
           %>
           <option value="<%=i%>" <% if(temp_i.equals(dyear)){%> selected="<%=dyear%>"<%}%>><%=i%></option>
           <%
         }
         %>
         </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Amount</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="amount" class="SL2TextField" value="<%=amount%>" onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
  </table>

  <!--HIDDEN FIELDS-->
  <input type="hidden" name="oldYear" value="<%=dyear%>">

  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="43%" scope="row">
            <div align="right">
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
            </div></th>
        <td width="57%">
	          <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="gotobank()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
        </td>
      </tr>
</table>
</form>
<%
   if(isConnect)
   {
     try
     {
       cm.takeDown();
     }
     catch(Exception e){}
   }
%>
</body>
</html>
