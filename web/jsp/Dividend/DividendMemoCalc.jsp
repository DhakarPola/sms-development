<script>
/*
'******************************************************************************************************************************************
'Script Author : Mohammad Ashraful Islam
'Creation Date : December 2006
'Page Purpose  : Calculate Dividend Memo.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Dividend Memo Calculation</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String decdate = String.valueOf(request.getParameter("dateselect"));
  String lid = String.valueOf(request.getParameter("letter"));

  decdate=cm.replace(decdate,"'","''");

  String cat[];
  cat=new String[3];
  String code[];
  code=new String[3];

  String cate="SELECT A.*,B.*,C.* FROM (SELECT CATEGORY CAT1,CODE COD1 FROM CATEGORY WHERE UPPER(TRIM(CATEGORY))='INDIVIDUAL') A, (SELECT CATEGORY CAT2,CODE COD2 FROM CATEGORY WHERE UPPER(TRIM(CATEGORY))='GOVERNMENT ORGANIZATION') B, (SELECT CATEGORY CAT3,CODE COD3 FROM CATEGORY WHERE UPPER(TRIM(CATEGORY))='FOREIGN COMPANIES') C";
  cm.queryExecute(cate);
  cm.toNext();

  cat[0]=cm.getColumnS("CAT1");
  code[0]=cm.getColumnS("COD1");
  cat[1]=cm.getColumnS("CAT2");
  code[1]=cm.getColumnS("COD2");
  cat[2]=cm.getColumnS("CAT3");
  code[2]=cm.getColumnS("COD3");

  String acc="SELECT ACCOUNT_NO FROM DIVIDENDDECLARATION WHERE DATE_DEC=TO_DATE('" + decdate + "','DD/MM/YYYY')";
  cm.queryExecute(acc);
  cm.toNext();
  String account=cm.getColumnS("ACCOUNT_NO");

  int j=0;

  String memoname[];
  memoname=new String[20];
  String memotype[];
  memotype=new String[20];
  int totalSH[];
  totalSH=new int[20];
  double totaldiv[];
  totaldiv=new double[20];
  double d1=0;
  double d2=0;
  double inctax=0;
  double totaltax[];
  totaltax=new double[20];

  int bosh=0;
  double bodiv=0;
  double botax=0;

  String res[];
  res=new String[2];

  res[0]="T";
  res[1]="F";

  //FOR GOVERMENT BOTH BO AND SH

int a=0;
int b=0;
int t=0;
for (a=0;a<2;a++)
{
  for(b=0;b<2;b++)
  {
String allG="SELECT A.*,B.* FROM (SELECT SUM(D.TOTAL_SHARE) TS1,SUM(D.DIVIDEND) TD1,SUM(D.DED1) D11,SUM(D.DED2) D21,SUM(D.INCOME_TAX) DI1 FROM DIVIDEND D JOIN SHAREHOLDER S USING (FOLIO_NO) WHERE S.RESIDENT='"+res[a]+"' AND CLASS='"+code[b]+"' AND D.ISSUE_DATE=TO_DATE('"+ decdate +"','DD/MM/YYYY')) A,(SELECT SUM(TOTAL_SHARE) TS2,SUM(DIVIDEND) TD2,SUM(DED1) D12,SUM(DED2) D22,SUM(INCOME_TAX) DI2 FROM BODIVIDEND WHERE BO_ID IN (SELECT BO_ID FROM BOADDRESS WHERE RESIDENT='"+res[a]+"' AND CLASS='"+code[b]+"') AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')) B";
cm.queryExecute(allG);
cm.toNext();

totalSH[t]=cm.getColumnI("TS1");
totaldiv[t]=cm.getColumnF("TD1");

  d1=cm.getColumnF("D11");
  d2=cm.getColumnF("D21");
  inctax=cm.getColumnF("DI1");

  if (Double.valueOf(d1).equals("null"))
         d1 = 0;
  if (Double.valueOf(d2).equals("null"))
         d2 = 0;
  if (Double.valueOf(inctax).equals("null"))
         inctax = 0;
  totaltax[t]=d1+d2+inctax;

  memoname[t]=cat[b];
  memotype[t]=cat[b];

  bosh=cm.getColumnI("TS2");
  bodiv=cm.getColumnF("TD2");
  d1=cm.getColumnF("D12");
  d2=cm.getColumnF("D22");
  inctax=cm.getColumnF("DI2");

  if (Double.valueOf(d1).equals("null"))
         d1 = 0;
  if (Double.valueOf(d2).equals("null"))
         d2 = 0;
  if (Double.valueOf(inctax).equals("null"))
         inctax = 0;
  botax=d1+d2+inctax;

  totalSH[t]=totalSH[t]+bosh;
  totaldiv[t]=totaldiv[t]+bodiv;
  totaltax[t]=totaltax[t]+botax;
t++;
}
}

memoname[0]=memoname[0]+" RESIDENT";
memotype[0]=memotype[0]+" RESIDENT";
memoname[2]=memoname[2]+" NON-RESIDENT";
memotype[2]=memotype[2]+" NON-RESIDENT";

//FOR REST OF THE COMPANY

String rest="SELECT A.*,B.* FROM (SELECT SUM(D.TOTAL_SHARE) TS1,SUM(D.DIVIDEND) TD1,SUM(D.DED1) D11,SUM(D.DED2) D21,SUM(D.INCOME_TAX) DI1 FROM DIVIDEND D JOIN SHAREHOLDER S USING (FOLIO_NO) WHERE CLASS NOT IN('"+code[0]+"','"+code[1]+"','"+code[2]+"') AND D.ISSUE_DATE=TO_DATE('"+ decdate +"','DD/MM/YYYY')) A,(SELECT SUM(TOTAL_SHARE) TS2,SUM(DIVIDEND) TD2,SUM(DED1) D12,SUM(DED2) D22,SUM(INCOME_TAX) DI2 FROM BODIVIDEND WHERE BO_ID IN (SELECT BO_ID FROM BOADDRESS WHERE CLASS NOT IN('"+code[0]+"','"+code[1]+"','"+code[2]+"')) AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')) B";
cm.queryExecute(rest);
cm.toNext();

  totalSH[3]=cm.getColumnI("TS1");
  totaldiv[3]=cm.getColumnF("TD1");
  d1=cm.getColumnF("D11");
  d2=cm.getColumnF("D21");
  inctax=cm.getColumnF("DI1");

  if (Double.valueOf(d1).equals("null"))
         d1 = 0;
  if (Double.valueOf(d2).equals("null"))
         d2 = 0;
  if (Double.valueOf(inctax).equals("null"))
         inctax = 0;
  totaltax[3]=d1+d2+inctax;

  memoname[3]="COMPANY";
  memotype[3]="COMPANY";

  bosh=cm.getColumnI("TS2");
  bodiv=cm.getColumnF("TD2");

  d1=cm.getColumnF("D12");
  d2=cm.getColumnF("D22");
  inctax=cm.getColumnF("DI2");

  if (Double.valueOf(d1).equals("null"))
         d1 = 0;
  if (Double.valueOf(d2).equals("null"))
         d2 = 0;
  if (Double.valueOf(inctax).equals("null"))
         inctax = 0;


 totalSH[3]=totalSH[3]+bosh;
 totaldiv[3]=totaldiv[3]+bodiv;
 botax=d1+d2+inctax;
 totaltax[3]=totaltax[3]+botax;

 //FOR FOREIGN BOTH BO AND TAX

  int h=4;
  String allf="SELECT S.NAME N,SUM(D.TOTAL_SHARE) TS,SUM(D.DIVIDEND) TD,SUM(D.DED1) D1,SUM(D.DED2) D2,SUM(D.INCOME_TAX) DI FROM DIVIDEND D JOIN SHAREHOLDER S USING (FOLIO_NO) WHERE S.RESIDENT='F' AND CLASS='"+code[2]+"' AND D.ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY') GROUP BY S.NAME ORDER BY TS DESC";
  int q=cm.queryExecuteCount(allf);
  cm.queryExecute(allf);

  q=h+q;

  while (cm.toNext() && h<q)
  {
  memoname[h]=cm.getColumnS("N");
  totalSH[h]=cm.getColumnI("TS");
  totaldiv[h]=cm.getColumnF("TD");
  d1=cm.getColumnF("D1");
  d2=cm.getColumnF("D2");
  inctax=cm.getColumnF("DI");

  if (String.valueOf(memoname[h]).equals("null"))
         memoname[h]="";
  if (Double.valueOf(d1).equals("null"))
         d1 = 0;
  if (Double.valueOf(d2).equals("null"))
         d2 = 0;
  if (Double.valueOf(inctax).equals("null"))
         inctax = 0;
  totaltax[h]=d1+d2+inctax;

  memoname[h]=cm.replace(memoname[h],"'","''");
  memoname[h].trim();
  memotype[h]=cat[2];

  h++;
  }

  String boallf="SELECT D.NAME_FIRSTHOLDER N,B.TOTAL_SHARE TS,B.DIVIDEND TD,B.DED1 D1,B.DED2 D2,B.INCOME_TAX DI FROM BODIVIDEND B JOIN BOADDRESS D USING (BO_ID) WHERE D.RESIDENT='T' AND D.CLASS='H' AND B.ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY') ORDER BY TS DESC";
  int r=cm.queryExecuteCount(boallf);
  cm.queryExecute(boallf);

  r=r+h;

  while (cm.toNext() && h<r)
  {
  memoname[h]=cm.getColumnS("N");
  totalSH[h]=cm.getColumnI("TS");
  totaldiv[h]=cm.getColumnF("TD");
  d1=cm.getColumnF("D1");
  d2=cm.getColumnF("D2");
  inctax=cm.getColumnF("DI");

  if (String.valueOf(memoname[h]).equals("null"))
         memoname[h]="";
  if (Double.valueOf(d1).equals("null"))
         d1 = 0;
  if (Double.valueOf(d2).equals("null"))
         d2 = 0;
  if (Double.valueOf(inctax).equals("null"))
         inctax = 0;
  totaltax[h]=d1+d2+inctax;

  memoname[h]=cm.replace(memoname[h],"'","''");
  memoname[h].trim();
  memotype[h]=cat[2];

  h++;
  }

int c=0;
int d=0;
int temp=0;
String temp1="";
double temp2=0;
double temp3=0;

for(c=4;c<h;c++)
{
for(d=4;d<h;d++)
{
if(totalSH[d]<totalSH[d+1])
{
temp=totalSH[d];
totalSH[d]=totalSH[d+1];
totalSH[d+1]=temp;

temp1=memoname[d];
memoname[d]=memoname[d+1];
memoname[d+1]=temp1;

temp2=totaldiv[d];
totaldiv[d]=totaldiv[d+1];
totaldiv[d+1]=temp2;

temp3=totaltax[d];
totaltax[d]=totaltax[d+1];
totaltax[d+1]=temp3;

}
}
}

  String delmemo = "call DELETE_DIVIDEND_MEMO()";
  boolean del = cm.procedureExecute(delmemo);

  int x=0;
  for (x=0;x<h;x++)
  {
  if (Integer.valueOf(totalSH[x]).equals("null"))
         totalSH[x] = 0;
  if (Double.valueOf(totaldiv[x]).equals("null"))
         totaldiv[x] = 0;

  totaltax[x]=bcal.roundtovalue(totaltax[x],2);

  int memoid=0;
  memoid=x+1;
  String addmemo = "call ADD_DIVIDEND_MEMO(UPPER('"+ memoname[x] +"'),'"+memotype[x]+"','" + totalSH[x] + "','" + totaldiv[x] + "','" + totaltax[x] + "','"+memoid+"')";
  boolean add = cm.procedureExecute(addmemo);
  }
  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Calculated Dividend Memo Information')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>

<script language="javascript">
  location = "<%=request.getContextPath()%>/DividendMemoReportTemp.jsp?dateselect=<%=decdate%>&letterid=<%=lid%>";
</script>
</body>
</html>
