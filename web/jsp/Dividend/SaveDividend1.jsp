<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : October 2005
'Page Purpose  : Save New Dividends.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="cm2"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Dividend</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   boolean isConnect2 = cm2.connect();
   if((isConnect==false) || (isConnect1==false) || (isConnect2==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   double DivPerc = 0;
   String DateDec = "";
   String PeriodFrom = "";
   String PeriodTo = "";
   double deduction1 = 0;
   double deduction2 = 0;
   double indtaxfree = 0;
   double comptaxfree = 0;
   double taxindres = 0;
   double taxindnonres = 0;
   double taxcompres = 0;
   double taxcompnonres = 0;
   double sptax = 0;
   String bankname = "";
   String branchname = "";
   String bankaddress = "";
   String accountno = "";
   String DivType = "";
   String QualDate = "";
   String PayableDate = "";
   String FinancialYear = "";
   String D1Reason = "";
   String D1Criteria = "";
   String D2Reason = "";
   String D2Criteria = "";
   boolean b;

   String ndividend2B = "call DELETE_LAST_BODIVIDEND()";
   b = cm.procedureExecute(ndividend2B);

   String ndividend2C = "call DELETE_LAST_DIVIDEND()";
   b = cm.procedureExecute(ndividend2C);

   String query1 = "SELECT * FROM DIVIDENDDECLARATION_T_VIEW";
   int nofdividends = cm1.queryExecuteCount(query1);
   if (nofdividends == 0)
   {
     %>
       <script language="javascript">
        alert("Declare a Dividend First!");
        history.go(-1);
       </script>
     <%
   }

   cm.queryExecute(query1);

   while(cm.toNext())
   {
     DivPerc = cm.getColumnF("DEC_DIV");
     DateDec = cm.getColumnDT("DATE_DEC");
     PeriodFrom = cm.getColumnDT("PERIOD_FROM");
     PeriodTo = cm.getColumnDT("PERIOD_TO");
     deduction1 = cm.getColumnF("DED1");
     deduction2 = cm.getColumnF("DED2");
     indtaxfree = cm.getColumnF("IND_TAX_FREE");
     comptaxfree = cm.getColumnF("COMP_TAX_FREE");
     taxindres = cm.getColumnF("TAX_IND_RES");
     taxindnonres = cm.getColumnF("TAX_IND_NONRES");
     taxcompres = cm.getColumnF("TAX_COMP_RES");
     taxcompnonres = cm.getColumnF("TAX_COMP_NONRES");
     sptax = cm.getColumnF("SPECIAL_TAX");
     bankname = cm.getColumnS("BANK_NAME");
     branchname = cm.getColumnS("BANK_BRANCH");
     bankaddress = cm.getColumnS("BANK_ADDRESS");
     accountno = cm.getColumnS("ACCOUNT_NO");
     DivType = cm.getColumnS("DIV_TYPE");
     QualDate = cm.getColumnDT("QUAL_DATE");
     PayableDate = cm.getColumnDT("PAYABLE_DATE");
     FinancialYear = cm.getColumnDT("FINANCIAL_YEAR");
     D1Reason = cm.getColumnS("DED1REASON");
     D1Criteria = cm.getColumnS("DED1CRITERIA");
     D2Reason = cm.getColumnS("DED2REASON");
     D2Criteria = cm.getColumnS("DED2CRITERIA");

     String ndividend1 = "call ADD_DIVIDENDDECLARATION('" + DivType + "', " + DivPerc + ", '" + DateDec + "', '" + PeriodFrom + "', '" + PeriodTo + "', " + taxindres + "," + taxindnonres + "," + taxcompres + "," + taxcompnonres + "," + sptax + "," + indtaxfree + "," + comptaxfree + "," + deduction1 + ",'" + D1Reason + "','" + D1Criteria + "'," + deduction2 + ",'" + D2Reason + "','" + D1Criteria + "','" + QualDate + "','" + PayableDate + "','" + FinancialYear + "','" + bankname + "','" + branchname + "','" + bankaddress + "','" + accountno + "')";
     b = cm1.procedureExecute(ndividend1);
   }

   String ndividend2 = "call DELETE_DIVIDENDDECLARATION_T()";
   b = cm.procedureExecute(ndividend2);

   String ndividend4g = "call DELETE_DIVIDEND_GROUP()";
   b = cm.procedureExecute(ndividend4g);

//   String query2 = "SELECT * FROM DIVIDEND_T_VIEW";
//   cm.queryExecute(query2);

   String foliono = "";
   String boid = "";
   int totalshare = 0;
   int liableshare = 0;
   double totaldividend = 0;
   String warrantno = "";
   double ndeduction1 = 0;
   double ndeduction2 = 0;
   double incometax = 0;
   String issuedate = "";
   String isPrinted = "";
   String isCollected = "";
   String isSelected = "";

   String sdividend3 = "call ADD_DIVIDEND_NEW('" + DateDec + "')";
   b = cm.procedureExecute(sdividend3);

   String sdividend3a = "call ADD_LAST_DIVIDEND_NEW('" + DateDec + "')";
   b = cm.procedureExecute(sdividend3a);

/*   while(cm.toNext())
   {
     foliono = cm.getColumnS("FOLIO_NO");
     totalshare = cm.getColumnI("TOTAL_SHARE");
     liableshare = cm.getColumnI("SHARE_LIABLE");
     totaldividend = cm.getColumnF("DIVIDEND");
     warrantno = cm.getColumnS("WARRANT_NO");
     ndeduction1 = cm.getColumnF("DED1");
     ndeduction2 = cm.getColumnF("DED2");
     incometax = cm.getColumnF("INCOME_TAX");
     issuedate = cm.getColumnDT("ISSUE_DATE");
     isCollected = cm.getColumnS("COLLECTED");
     isPrinted = cm.getColumnS("PRINTED");
     isSelected = cm.getColumnS("SELECTED");

     String sdividend3 = "call ADD_DIVIDEND('" + foliono + "', " + totalshare + ", " + liableshare + ", " + totaldividend + ", '" + warrantno + "', " + ndeduction1 + "," + ndeduction2 + "," + incometax + ",'" + issuedate + "','" + isCollected + "','" + isPrinted + "','" + isSelected + "','F','','F','F','','F','')";
     b = cm1.procedureExecute(sdividend3);

     String sdividend3a = "call ADD_LAST_DIVIDEND('" + foliono + "', " + totalshare + ", " + liableshare + ", " + totaldividend + ", '" + warrantno + "', " + ndeduction1 + "," + ndeduction2 + "," + incometax + ",'" + issuedate + "','" + isCollected + "','" + isPrinted + "','" + isSelected + "','F','','F','F','','F','')";
     b = cm1.procedureExecute(sdividend3a);
   }

*/

   /***** Inserting DIVIDEND INQUIRY REPORT DATA *****/
   String dividendInquiryQueryStr = "call ADD_DIV_INQUIRY('"+DateDec+"')";
   b = cm.procedureExecute(dividendInquiryQueryStr);
   /**end DIVIDEND INQUIRY REPORT DATA**/

   String ndividend4 = "call DELETE_DIVIDEND_T()";
   b = cm.procedureExecute(ndividend4);

   String query2n = "SELECT * FROM NOMINEE_VIEW";
   cm.queryExecute(query2n);

   String nomineeno = "";
   double totalgrpdividend = 0;
   double totalgrpded1 = 0;
   double totalgrpded2 = 0;
   double totalgrptax = 0;
   double totalgrpshares = 0;
   String grpwarrant = "";


   while(cm.toNext())
   {
     nomineeno = cm.getColumnS("NOMINEE_NO");
     String grpquery = "SELECT SUM(to_Number(DIVIDEND)) as TDIV FROM SHAREHOLDER_GROUP_VIEW WHERE NOMINEE_NO = '" + nomineeno + "' AND ISSUE_DATE = TO_DATE('" + DateDec + "','DD/MM/YYYY')";
     cm2.queryExecute(grpquery);
     cm2.toNext();
     totalgrpdividend = cm2.getColumnF("TDIV");

     if(totalgrpdividend > 0)
     {
      grpquery = "SELECT SUM(to_Number(DED1)) as TDED1 FROM SHAREHOLDER_GROUP_VIEW WHERE NOMINEE_NO = '" + nomineeno + "' AND ISSUE_DATE = TO_DATE('" + DateDec + "','DD/MM/YYYY')";
      cm2.queryExecute(grpquery);
      cm2.toNext();
      totalgrpded1 = cm2.getColumnF("TDED1");

      grpquery = "SELECT SUM(to_Number(DED2)) as TDED2 FROM SHAREHOLDER_GROUP_VIEW WHERE NOMINEE_NO = '" + nomineeno + "' AND ISSUE_DATE = TO_DATE('" + DateDec + "','DD/MM/YYYY')";
      cm2.queryExecute(grpquery);
      cm2.toNext();
      totalgrpded2 = cm2.getColumnF("TDED2");

      grpquery = "SELECT SUM(to_Number(INCOME_TAX)) as TTAX FROM SHAREHOLDER_GROUP_VIEW WHERE NOMINEE_NO = '" + nomineeno + "' AND ISSUE_DATE = TO_DATE('" + DateDec + "','DD/MM/YYYY')";
      cm2.queryExecute(grpquery);
      cm2.toNext();
      totalgrptax = cm2.getColumnF("TTAX");

      grpquery = "SELECT SUM(to_Number(TOTAL_SHARE)) as TSHARES FROM SHAREHOLDER_GROUP_VIEW WHERE NOMINEE_NO = '" + nomineeno + "' AND ISSUE_DATE = TO_DATE('" + DateDec + "','DD/MM/YYYY')";
      cm2.queryExecute(grpquery);
      cm2.toNext();
      totalgrpshares = cm2.getColumnF("TSHARES");

      grpwarrant = cm1.LastWarrant();

      totalgrpdividend = bcal.roundtovalue(totalgrpdividend,4);
      totalgrpded1 = bcal.roundtovalue(totalgrpded1,4);
      totalgrpded2 = bcal.roundtovalue(totalgrpded2,4);
      totalgrptax = bcal.roundtovalue(totalgrptax,4);

      String grpdiv = "call ADD_DIVIDEND_GROUP('" + nomineeno + "', " + totalgrpdividend + ", '" + grpwarrant + "','" + DateDec + "','F','F','F','F','','F','F','','F','', " + totalgrpded1 + ", " + totalgrpded2 + ", " + totalgrptax + ", " + totalgrpshares + ")";
      System.out.println(grpdiv);
      b = cm1.procedureExecute(grpdiv);
     }
   }

   String query3 = "SELECT * FROM BODIVIDEND_T_VIEW";
   cm.queryExecute(query3);

   String sdividend4 = "call ADD_BODIVIDEND_NEW('" + DateDec + "')";
   b = cm.procedureExecute(sdividend4);

   String sdividend4a = "call ADD_LAST_BODIVIDEND_NEW('" + DateDec + "')";
   b = cm.procedureExecute(sdividend4a);

/*
   while(cm.toNext())
   {
     boid = cm.getColumnS("BO_ID");
     totalshare = cm.getColumnI("TOTAL_SHARE");
     liableshare = cm.getColumnI("SHARE_LIABLE");
     totaldividend = cm.getColumnF("DIVIDEND");
     warrantno = cm.getColumnS("WARRANT_NO");
     ndeduction1 = cm.getColumnF("DED1");
     ndeduction2 = cm.getColumnF("DED2");
     incometax = cm.getColumnF("INCOME_TAX");
     issuedate = cm.getColumnDT("ISSUE_DATE");
     isCollected = cm.getColumnS("COLLECTED");
     isPrinted = cm.getColumnS("PRINTED");
     isSelected = cm.getColumnS("SELECTED");

     String sdividend4 = "call ADD_BODIVIDEND('" + boid + "', " + totalshare + ", " + liableshare + ", " + totaldividend + ", '" + warrantno + "', " + ndeduction1 + "," + ndeduction2 + "," + incometax + ",'" + issuedate + "','" + isCollected + "','" + isPrinted + "','" + isSelected + "','F','','F','F','','F','')";
     b = cm1.procedureExecute(sdividend4);

     String sdividend4a = "call ADD_LAST_BODIVIDEND('" + boid + "', " + totalshare + ", " + liableshare + ", " + totaldividend + ", '" + warrantno + "', " + ndeduction1 + "," + ndeduction2 + "," + incometax + ",'" + issuedate + "','" + isCollected + "','" + isPrinted + "','" + isSelected + "','F','','F','F','','F','')";
     b = cm1.procedureExecute(sdividend4a);
   }
*/

   String ndividend5 = "call DELETE_BODIVIDEND_T()";
   b = cm.procedureExecute(ndividend5);

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Accept Declared Dividend')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
     cm1.takeDown();
     cm2.takeDown();
   }
%>
<script language="javascript">
  alert("Dividend Accepted!");
  location = "<%=request.getContextPath()%>/jsp/Dividend/SetDividendPostlist.jsp";
</script>
</body>
</html>
