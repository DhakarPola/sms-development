<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : August 2007
'Page Purpose  : Save Marge Account Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>

<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Marge Account</title>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();

   if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

     String merged=String.valueOf(request.getParameter("marged"));
     String decDate=String.valueOf(request.getParameter("decdate"));
     String rtnDate=decDate;

     //System.out.println("merged= "+merged);
     //System.out.println("decDate= "+decDate);

     if(String.valueOf(merged).equalsIgnoreCase("Y"))
     {
       merged="Y";
     }
     else
     {
       merged="";
     }

      String sysdate_q="Select sysdate from dual";
      //System.out.print("sysdate_q= "+sysdate_q);
      String merge_date="";
      try
      {
        cm.queryExecute(sysdate_q);
        cm.toNext();
        merge_date=cm.getColumnDT("sysdate");
      }
      catch(Exception e){}



      double date_diff=0.0;
      String q="select MONTHS_BETWEEN(sysdate,TO_DATE('" + decDate + "','DD/MM/YYYY'))as diff from dual";
      //System.out.println("q= "+q);
      try
      {
        cm.queryExecute(q);
        cm.toNext();
        date_diff=cm.getColumnF("diff");
      }
      catch(Exception e){}
      //System.out.println("date_diff= "+date_diff);
    if(date_diff<6.0)
    {
      //System.out.println("Greater");
      %>
      <script language="javascript">
      alert("Can not marge account due to date difference is less than 6 month.");
      </script>
      <%
    }
    else
    {
     // System.out.println("Less");
    if(String.valueOf(merged).equalsIgnoreCase("Y"))
     {
       merged="Y";
     }
     else
     {
       merged="";
       merge_date="";
     }
      if(String.valueOf(merged).equalsIgnoreCase("Y")){
        try
       {
         String s_q="Select B.BANK_NAME,B.BANK_BRANCH,B.BANK_ADDRESS,UB.Unclaimed_Account FROM BANK_ACCOUNTS_VIEW UB, Bank_View B where UB.UNCLAIMED_BANK_ID=B.BANK_ID";
         cm.queryExecute(s_q);
         cm.toNext();
         String bank_name=cm.getColumnS("BANK_NAME");
         String bank_add=cm.getColumnS("BANK_ADDRESS");
         String bank_branch=cm.getColumnS("BANK_BRANCH");
         String update ="update dividenddeclaration set ACC_MERGED='"+ merged + "',BANK_NAME='"+ bank_name + "',BANK_BRANCH='"+ bank_branch + "',BANK_ADDRESS='"+ bank_add + "', MERGED_DATE=TO_DATE('" + merge_date + "','DD/MM/YYYY'),OLD_ACCOUNT_NO=ACCOUNT_NO,ACCOUNT_NO=(SELECT UNCLAIMED_ACCOUNT FROM BANK_ACCOUNTS_VIEW) where DATE_DEC=TO_DATE('" + decDate + "','DD/MM/YYYY') ";
         System.out.println(update);
         String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Edited Merge Account Information')";

         cm.queryExecute(update);
         boolean ub = cm.procedureExecute(ulog);
      }
      catch(Exception e){}
    }
      else {
   try
      {
        String s_q="Select B.BANK_NAME,B.BANK_BRANCH,B.BANK_ADDRESS,UB.Unclaimed_Account FROM BANK_ACCOUNTS_VIEW UB, Bank_View B where UB.CURRENT_BANK_ID=B.BANK_ID";
        cm.queryExecute(s_q);
        cm.toNext();
        String bank_name=cm.getColumnS("BANK_NAME");
        String bank_add=cm.getColumnS("BANK_ADDRESS");
        String bank_branch=cm.getColumnS("BANK_BRANCH");
        String update ="update dividenddeclaration set ACC_MERGED='"+ merged + "',BANK_NAME='"+ bank_name + "',BANK_BRANCH='"+ bank_branch + "',BANK_ADDRESS='"+ bank_add + "', MERGED_DATE=TO_DATE('" + merge_date + "','DD/MM/YYYY'),ACCOUNT_NO=OLD_ACCOUNT_NO,OLD_ACCOUNT_NO='' where DATE_DEC=TO_DATE('" + decDate + "','DD/MM/YYYY') ";
        System.out.println(update);
        String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Edited De-Merge Account Information')";
        cm.queryExecute(update);
        boolean ub = cm.procedureExecute(ulog);
      }
      catch(Exception e){}
    }

    }
    try{

      if (isConnect)
      {
        cm.takeDown();
        cm1.takeDown();
      }
    }
    catch(Exception ex){
    }

%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Dividend/MergeAccounts.jsp?searchDate=";
  </script>
</body>
</html>
