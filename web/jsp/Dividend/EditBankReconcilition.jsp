<script>
/*
'******************************************************************************************************************************************
'Script Author : Md.Kamruzzamam
'Updated By    : Renad Hakim
'Updated By    : Rahat Uddin
'Update Date   : August 2007
'Creation Date : December 2006
'Page Purpose  : Edit Bank Reconciliation
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}
%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit Bank Reconcil</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
var check=0;
function checknow(status)
{
  var c=status;
  if(c==1)
  check=0;
  else if(c==0)
  check=1;
  return check;
}

<!--
//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('entrydate','- Entry Date (Option must be entered)');
  BlankValidate('particulars','- Particulars (Option must be entered)');
  BlankValidate('amount','- Amount (Option must be entered)');
  BlankValidate('type','- Debit/Credit (Option must be entered)');
  if(check==1)
  {
   BlankValidate('refunddate','- Refund Date (Option must be entered)');
  }
  if (count == 0)
  {
      document.forms[0].submit();
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
     String id=request.getParameter("id");
     String unclaimed_account=String.valueOf(request.getParameter("unclaimed_account"));
     int status=0;
     int True=1;
     int False=0;
     String query="select * from BANK_RECON_VIEW where BR_ID='"+ id +"'";
     cm.queryExecute(query);
     cm.toNext();

       String account_no=cm.getColumnS("ACCOUNT_NO");
       String par_date=String.valueOf(cm.getColumnDT("PAR_DATE"));
       String particulars=cm.getColumnS("PARTICULARS");
       String amount=String.valueOf(cm.getColumnF("AMOUNT"));
       String debit=cm.getColumnS("TYPE");
       String refund=cm.getColumnS("REFUNDED");
       String refund_date=String.valueOf(cm.getColumnDT("REFUND_DATE"));
       String Is_Company_Account=cm.getColumnS("IS_COMPANY_ACCOUNT");
       if(String.valueOf(Is_Company_Account).equals("null")){
         Is_Company_Account="";
       }
%>

<form action="UpdateBankReconciliation.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7"> Edit Bank Reconciliation</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Account No.</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
        <strong><%=account_no%></strong>
        </div></td>
      </tr>
      <tr>
        <th valign="top" scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date of Dividend Declaration.</div></th>
        <td valign="top">:</td>
        <td valign="top"><div align="left">

                      <table>
                                 <%
                                  //String qdate="select DATE_DEC from DIVIDENDDECLARATION_VIEW where ACCOUNT_NO='"+ account_no +"' order by DATE_DEC";

                                  String date="";

                                  if(!unclaimed_account.equalsIgnoreCase("Y")){
                                    String qdate="select DATE_DEC  from DIVIDENDDECLARATION_VIEW where upper(ACC_MERGED)!='Y' or ACC_MERGED is null order by DATE_DEC";
                                    cm.queryExecute(qdate);
                                  while(cm.toNext())
                                  {
                                    date=cm.getColumnDT("DATE_DEC");
                                  %>
                                  <tr>
                                    <th scope="row"><div align="left" class="style8"><%=date%></div></th>
                                  </tr>
                               <%}
                             }
                             else{
                               String qdate="select DATE_DEC from DIVIDENDDECLARATION_VIEW where upper(ACC_MERGED)='Y' order by DATE_DEC";
                               cm.queryExecute(qdate);
                               String All_Divi_date="";
                               while(cm.toNext())
                               {
                                 date=cm.getColumnDT("DATE_DEC");
                                 if(All_Divi_date.length()>0){
                                   All_Divi_date=All_Divi_date+", "+date;
                                 }
                                 else{
                                   All_Divi_date=date;
                                 }
                               }
                               %>
                               <tr>
                                 <th scope="row"><div align="left" class="style8"><%=All_Divi_date%></div></th>
                               </tr>
                               <%
                             }%>
                      </table>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Entry Date</div></th>
        <td>:</td>
        <td><div align="left">
         <input name="entrydate" type="text" id="entrydate" value="<%=par_date%>" maxlength="10" class="SL2TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('entrydate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Particulars</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="particulars" value="<%=particulars%>" class="SL2TextField">
        </div></td>
      </tr>
      <%if(unclaimed_account.equalsIgnoreCase("Y")){
            %>
            <tr>
              <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Company Account Transaction</div></th>
              <td>:</td>
              <td><div align="left"><input type="hidden" name="unclaimed_account" id="unclaimed_account" value="Y">
              <%
              if(Is_Company_Account.equalsIgnoreCase("Y"))
              {
              %>
              <input name="iscompany" id="iscompany" type="checkbox" checked="checked" value="Y">
              <%
               }
                else
                {
                %>
                <input name="iscompany" id="iscompany" type="checkbox" value="Y">
                <%
              }
                %>

             </div></td>
            </tr>
            <%
          } %>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Amount</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="amount" value="<%=amount%>" class="SL2TextField" onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Debit/Credit</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="type" class="SL1TextFieldListBox">
            <option>--- Please Select ---</option>
            <option value="Debit" <%if(debit.equalsIgnoreCase("Debit")){%>Selected<%}%>>Debit</option>
            <option value="Credit" <%if(debit.equalsIgnoreCase("Credit")){%>Selected<%}%>>Credit</option>
         </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Refunded</div></th>
        <td width="1%">:</td>
        <td><div align="left">
           <%
              if (refund.equals("Yes"))
              {
              %>
              <input name="refunded" id="refunded" type="checkbox" checked="checked" value="Yes" onClick="if(this.checked) {checknow(<%=False%>)} else {checknow(<%=True%>)}">
              <%
               }
                else
                {
                %>
                <input name="refunded" id="refunded" type="checkbox" value="Yes" onClick="if(this.checked) {checknow(<%=False%>)} else {checknow(<%=True%>)}">
                <%
              }
                %>
        </div></td>
      </tr>
     <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Refund Date</div></th>
        <td>:</td>
        <td><div align="left">
         <input name="refunddate" type=text id="refunddate" value="<%=refund_date%>" maxlength="10" class="SL2TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('refunddate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="10" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <td width="100%" align="center">
          <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
          &nbsp;&nbsp;&nbsp;<img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
          &nbsp;&nbsp;&nbsp;<img name="B3" src="<%=request.getContextPath()%>/images/btnBack.gif"  onclick="history.go(-1)" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnBackOn.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnBack.gif'">
      </td>
        <!--td width="20%" align="center">

        </td>
      <td width="40%" align="left">

      </td-->

      </tr>
</table>
   <input type="hidden" name="account" value="<%=account_no%>">
   <input type="hidden" name="id" value="<%=id%>">
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
