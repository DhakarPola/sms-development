<script>
/*
'******************************************************************************************************************************************
'Script Author : Rahat Uddin
'Creation Date : Octoberber 2011
'Page Purpose  : View of all the Chaque Information
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cms"  class="batbsms.conBean"/>
<jsp:useBean id="cmss"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style19 {color: #0A2769; font-weight: bold; }
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Folios</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function calculateTotal() {
    var totalAmount=document.getElementById('totalAmount').value;
    var taxAmount=document.getElementById('taxAmount').value;
    if(totalAmount=='') totalAmount='0';
      totalAmount=Number(totalAmount);
    if(taxAmount=='') taxAmount='0';
      taxAmount=Number(taxAmount);
    document.getElementById('netAmount').value=Number(totalAmount-taxAmount).toFixed(2);
}


//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}

function Cancel() {
  document.forms.cancel.submit();
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<form method="post" action="UpdateDividend.jsp">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cms.connect();
   boolean isConnect2 = cmss.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }


   String warrentNo=request.getParameter("warrentNo");
   String folioNo=request.getParameter("foliono");
   String issueDate=request.getParameter("issuedate");

 //System.out.println(("chequeno="+chequeno+"pppp");
   String folioname = "";
   String folioaddress = "";
   String folioaddress2 = "";
   String folioaddress3 = "";
   String folioaddress4 = "";
   String dest = "";
   int totalshares = 0;



    if (String.valueOf(warrentNo).equals("null"))
      warrentNo = "";
    if (String.valueOf(folioNo).equals("null"))
      folioNo = "";
    if (String.valueOf(issueDate).equals("null"))
      issueDate = "";

   String query1="",query="";

if (folioNo.length() <= 8) {
      query = "select * from shareholder_view WHERE folio_no='"+folioNo+"'";
      query1 = "select * from dividend WHERE folio_no='"+folioNo+"' and WARRANT_NO='"+warrentNo+"' and ISSUE_DATE=to_date('"+issueDate+"','dd/MM/yyyy')";
   }
   else {
      query = "Select * from boaddress_all_view where  BO_ID='"+folioNo+"'";
      query1 = "Select * from bodividend WHERE BO_ID='"+folioNo+"' and WARRANT_NO='"+warrentNo+"' and ISSUE_DATE=to_date('"+issueDate+"','dd/MM/yyyy')";
   }

  System.out.println("query="+query);
  System.out.println("query1="+query1);

   cm.queryExecute(query);
   cms.queryExecute(query1);

   while(cm.toNext()) {

     if (folioNo.length() <= 8){
       folioNo=cm.getColumnS("FOLIO_NO");
       folioname = cm.getColumnS("NAME");
       folioaddress = cm.getColumnS("ADDRESS1");
       folioaddress2 = cm.getColumnS("ADDRESS2");
       folioaddress3 = cm.getColumnS("ADDRESS3");
       folioaddress4 = cm.getColumnS("ADDRESS4");
     }
     else {
       folioNo=cm.getColumnS("BO_ID");
       folioname = cm.getColumnS("NAME_FIRSTHOLDER");
       folioaddress = cm.getColumnS("ADDRESS");
     }

     if (String.valueOf(folioname).equals("null"))
           folioname = "";
     if (String.valueOf(folioaddress).equals("null"))
           folioaddress = "";
     if (String.valueOf(folioaddress2).equals("null"))
           folioaddress2 = "";
     if (String.valueOf(folioaddress3).equals("null"))
           folioaddress3 = "";
     if (String.valueOf(folioaddress4).equals("null"))
           folioaddress4 = "";

     folioaddress = folioaddress + folioaddress2 + folioaddress3 + folioaddress4;
   %>
       <table width="100%"  border="0" cellpadding="0" cellspacing="0">
       <tr><td bgcolor="#0044B0" colspan="5" class="style7" height="30" align="left"><center>Dividend Information</center></td></tr>
       <tr bgcolor="#E8F3FD">
                <td width="7%">&nbsp;</td>
                <td width="15%" class="style19">&nbsp;</td>
                <td width="3%" align=center>&nbsp;</td>
                <td width="75%">&nbsp;</td>
            </tr>
            <tr bgcolor="#E8F3FD">
                <td width="7%"></td>
                <td width="15%" class="style19">Folio/BO No:</td>
                <td width="3%" align=center>:</td>
                <td width="75%"><%=folioNo%></td>
            </tr>
            <tr bgcolor="#E8F3FD">
                <td ></td>
                <td  class="style19">Name</td>
                <td align=center>:</td>
                <td><%=folioname%></td>
            </tr>
            <tr bgcolor="#E8F3FD">
                <td ></td>
                <td class="style19">Address</td>
                <td align=center>:</td>
                <td ><%=folioaddress%></td>
            </tr>
     <%
     while (cms.toNext()) {
       warrentNo=cms.getColumnS("WARRANT_NO");
       issueDate=cms.getColumnDT("ISSUE_DATE");
       String collStatus=cms.getColumnS("COLLECTED");
       if (collStatus.equalsIgnoreCase("T")){
         collStatus="Collected";
       }
       else if (collStatus.equalsIgnoreCase("F")){
         collStatus="Uncollected";
       }
       else if (collStatus.equalsIgnoreCase("C")){
         collStatus="Canceled";
       }
       String totalAmount=cms.getColumnS("DIVIDEND");
       if(totalAmount.indexOf(".")==totalAmount.length()-2)
       totalAmount=totalAmount+"0";
       else  if(totalAmount.indexOf(".")==-1) {
         totalAmount=totalAmount+".00";
       }

       String taxAmount=cms.getColumnS("INCOME_TAX");
       if(taxAmount.indexOf(".")==taxAmount.length()-2)
       taxAmount=taxAmount+"0";
       else  if(taxAmount.indexOf(".")==-1) {
         taxAmount=taxAmount+".00";
       }

       String totalShare=cms.getColumnS("TOTAL_SHARE");
       if(totalShare.equals("null"))
       totalShare="0";
     %>
      <tr bgcolor="#E8F3FD">
                <td ></td>
                <td class="style19">Warrent No</td>
                <td align=center>:</td>
                <td ><%=warrentNo%></td>
            </tr>
      <tr bgcolor="#E8F3FD">
                <td ></td>
                <td class="style19">Issue Date</td>
                <td align=center>:</td>
                <td ><%=issueDate%></td>
            </tr>
      <tr bgcolor="#E8F3FD">
                <td ></td>
                <td class="style19">Collection Status</td>
                <td align=center>:</td>
                <td ><%=collStatus%></td>
            </tr>
       <tr bgcolor="#E8F3FD">
                <td ></td>
                <td class="style19">Total Share</td>
                <td align=center>:</td>
                <td ><%=totalShare%></td>
            </tr>
      <tr bgcolor="#E8F3FD">
                <td ></td>
                <td class="style19">Total Amount</td>
                <td align=center>:</td>
                <td ><input type="text" value="<%=totalAmount%>" name="totalAmount" id="totalAmount" readonly="readonly"  /></td>
            </tr>
       <tr bgcolor="#E8F3FD">
                <td ></td>
                <td class="style19">Tax Amount</td>
                <td align=center>:</td>
                <td ><input type="text" value="<%=taxAmount%>" onkeypress="keypressOnNumberFld()" name="taxAmount" id="taxAmount" onchange="calculateTotal()"/></td>
            </tr>
         <tr bgcolor="#E8F3FD">
                <td ></td>
                <td class="style19">Net Dividend</td>
                <td align=center>:</td>
                <td ><input type="text" name="netAmount" id="netAmount" readonly="readonly"/></td>
            </tr>
         <tr bgcolor="#E8F3FD">
                <td ></td>
                <td class="style19"></td>
                <td align=center></td>
                <td >
                  <input type="submit" value="" style="background: white url('<%=request.getContextPath()%>/images/btnUpdate.gif') no-repeat;width: 63px; height:23px;border:0px;"/>
                  <input type="button" value="" name="btn" style="background: white url('<%=request.getContextPath()%>/images/btnCancel.gif') no-repeat;width: 63px; height:23px;border:0px;" onclick="Cancel()"/>
                </td>
            </tr>
      </table>
     <%
     }
   }
%>
  <input  type="hidden" name="foliono" id="foliono"  value="<%=folioNo%>"/>
  <input  type="hidden" name="warrentno" id="warrentno" value="<%=warrentNo%>"/>
  <input  type="hidden" name="issuedate" id="issuedate" value="<%=issueDate%>"/>
  <script language="javascript">
      calculateTotal();
  </script>
  </form>
  <form id="cancel" name="cancel" action="SearchDividend.jsp">
  </form>
</body>
</html>
