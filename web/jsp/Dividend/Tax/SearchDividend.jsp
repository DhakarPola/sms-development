<script>
/*
'******************************************************************************************************************************************
'Script Author : Rahat Uddin
'Creation Date : Octoberber 2011
'Page Purpose  : View of all the Chaque Information
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style19 {color: #0A2769; font-weight: bold; }
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Folios</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--

function goNextPrevious(pstart,pend,foliono)
{
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Folio/AllPermanentFolios.jsp?StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;
  thisurl = thisurl + "&searchFolio="+foliono;
  location = thisurl;
}

 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  var searchFolio=document.getElementById("searchFolio").value;
  if (count == 0){
    if(searchFolio.length >7){
      document.forms[0].action="SearchDividend.jsp";
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   int folio_state=0;
   int COUNT;
   int MAX_RECORDS=20;
   String folio_no=request.getParameter("searchFolio");
   String dateselect=request.getParameter("dateselect");
   String folioname = "";
   String folioaddress = "";
   String folioaddress2 = "";
   String folioaddress3 = "";
   String folioaddress4 = "";
   String dest = "";
   int totalshares = 0;

   if (String.valueOf(folio_no).equals("null"))
      folio_no = "1";
  if (String.valueOf(dateselect).equals("null"))
      dateselect = "";

   String query1;

   if (folio_no.length() <= 8)
   {
      query1 = "select * from shareholder_view WHERE folio_no='"+folio_no+"'";
   }
   else
   {
      query1 = "Select * from boaddress_all_view where  BO_ID='"+folio_no+"'";
   }

   //System.out.println(query1);
   cm.queryExecute(query1);


%>
  <span class="style7">
  <form method="POST" action="SearchDividend.jsp" onsubmit="SubmitThis()">
  <table width="100%" BORDER=0  cellpadding="0" bordercolor="0044B0" style="border-collapse: collapse" bordercolor="#06689E">



  <tr><td bgcolor="#0044B0" colspan="5" class="style7" height="30" align="left"><center>Folio/BO Search Information</center></td></tr>

   <tr bgcolor="#E8F3FD">
                          <td width="15%">&nbsp;</td>
				<td width="15%" class="style19">&nbsp;</td>
                                <td width="2%" class="style19">&nbsp;</td>
				<td>&nbsp;</td>
			</tr>

                            <tr bgcolor="#E8F3FD">
                                <td width="15%">&nbsp;</td>
				<td width="15%" class="style19">&nbsp;Date of Declaration</td>
                                <td width="2%" class="style19">:</td>
				<td><div align="left">
                                  <select name="dateselect" class="SL1TextFieldListBox">
                                    <option>--- Please Select ---</option>
                                    <%
                                    String thedate = "";

                                    String query = "SELECT DISTINCT ISSUE_DATE FROM DIVIDEND_VIEW ORDER BY ISSUE_DATE DESC";
                                    cm1.queryExecute(query);

                                    while(cm1.toNext())
                                    {
                                      thedate = cm1.getColumnDT("ISSUE_DATE");
                                      %>
                                      <option value="<%=thedate%>"><%=thedate%></option>
                                      <%
                                    }
                                    %>
                                  </select>
                                 </div></td>
			</tr>
			<tr bgcolor="#E8F3FD">
                                <td width="15%">&nbsp;</td>
				<td width="15%" class="style19">&nbsp;Enter Folio/BO No</td>
                                <td width="2%" class="style19">:</td>
				<td><input class="SL1TextField" id="searchFolio" name="searchFolio" type="text" value="">
                                </td>
			</tr>

                        <tr bgcolor="#E8F3FD">
                          <td width="15%">&nbsp;</td>
                          <td width="15%" class="style19">&nbsp;</td>
                          <td width="2%" class="style19"></td>
                          <td>
                            <input type="submit" value="" style="background: white url('<%=request.getContextPath()%>/images/btnSearch.gif') no-repeat;width: 63px; height:23px;border:0px;"/>
                          </td>

			</tr>
        </table>

      <%
       folio_state=0;
       while(cm.toNext())
       {
         folio_state++;
         if (folio_no.length() <= 8){
           folio_no=cm.getColumnS("FOLIO_NO");
           folioname = cm.getColumnS("NAME");
           folioaddress = cm.getColumnS("ADDRESS1");
           folioaddress2 = cm.getColumnS("ADDRESS2");
           folioaddress3 = cm.getColumnS("ADDRESS3");
           folioaddress4 = cm.getColumnS("ADDRESS4");
         }
         else{

           folio_no=cm.getColumnS("BO_ID");
           folioname = cm.getColumnS("NAME_FIRSTHOLDER");
           folioaddress = cm.getColumnS("ADDRESS");
         }
         dest = request.getContextPath() + "/jsp/Dividend/Tax/EditDividend.jsp";

         if (!folio_no.equals("null"))
         {
           if (String.valueOf(folioname).equals("null"))
           folioname = "";
           if (String.valueOf(folioaddress).equals("null"))
           folioaddress = "";
           if (String.valueOf(folioaddress2).equals("null"))
           folioaddress2 = "";
           if (String.valueOf(folioaddress3).equals("null"))
           folioaddress3 = "";
           if (String.valueOf(folioaddress4).equals("null"))
           folioaddress4 = "";

           folioaddress = folioaddress + folioaddress2 + folioaddress3 + folioaddress4;

         %>
          <table width="100%"  border="0" cellpadding="0" cellspacing="0">
          <tr><td bgcolor="#0044B0" colspan="4" class="style7" height="30" align="left"><center>Folio/BO Information</center></td></tr>
            <tr bgcolor="#E8F3FD">
                <td width="7%"></td>
                <td width="15%" class="style19">Folio/BO No:</td>
                <td width="3%" align=center>:</td>
                <td width="75%"><%=folio_no%></td>
            </tr>
            <tr bgcolor="#E8F3FD">
                <td ></td>
                <td  class="style19">Name</td>
                <td align=center>:</td>
                <td><%=folioname%></td>
            </tr>
            <tr bgcolor="#E8F3FD">
                <td ></td>
                <td class="style19">Address</td>
                <td align=center>:</td>
                <td ><%=folioaddress%></td>
            </tr>

          </table>

     <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="0044B0">
       <tr bgcolor="#0044B0">
         <td width="10%"><div align="center" class="style9">
           <div align="center">Folio No</div>
</div></td>
<td width="20%"><div align="center" class="style9">
  <div align="center">Issue Date</div>
</div></td>
<td width="20%"><div align="center" class="style9">
  <div align="center">Warrent No.</div>
</div></td>

<td width="10%"><div align="center" class="style9">
  <div align="center">Dividend Amount</div>
</div></td>
<td width="10%"><div align="center" class="style9">
  <div align="center"> Tax Amount </div>
</div></td>
<td width="11%"><div align="center" class="style9">
  <div align="center">Collection Status</div>
</div></td>
       </tr>
  <%
 if (folio_no.length() <= 8) {
      query1 = "select * from dividend WHERE folio_no='"+folio_no+"'  and ISSUE_DATE=to_date('"+dateselect+"','dd/MM/yyyy')";
   }
   else {
      query1 = "Select * from bodividend where  BO_ID='"+folio_no+"'  and ISSUE_DATE=to_date('"+dateselect+"','dd/MM/yyyy')";
   }

  cm.queryExecute(query1);
   while(cm.toNext())
   {
     if (folio_no.length() <= 8)
      folio_no=cm.getColumnS("FOLIO_NO");
     else
      folio_no=cm.getColumnS("BO_ID");

     String warrentNo=cm.getColumnS("WARRANT_NO");
     dateselect=cm.getColumnDT("ISSUE_DATE");
     String collStatus=cm.getColumnS("COLLECTED");
     if (collStatus.equalsIgnoreCase("T")){
       collStatus="Collected";
     }
      else if (collStatus.equalsIgnoreCase("F")){
        collStatus="Uncollected";
    }
    else if (collStatus.equalsIgnoreCase("C")){
      collStatus="Canceled";
    }
    String totalAmount=cm.getColumnS("DIVIDEND");
    if(totalAmount.indexOf(".")==totalAmount.length()-2)
        totalAmount=totalAmount+"0";
    else  if(totalAmount.indexOf(".")==-1) {
      totalAmount=totalAmount+".00";
    }

    String taxAmount=cm.getColumnS("INCOME_TAX");
    if(taxAmount.indexOf(".")==taxAmount.length()-2)
        taxAmount=taxAmount+"0";
    else  if(taxAmount.indexOf(".")==-1) {
      taxAmount=taxAmount+".00";
    }
     %>


       <tr bgcolor="#E8F3FD">
         <td ><div align="center" class="style10">
           <div align="center" class="style10">
             <span class="style13">
               <a HREF="<%=dest%>?foliono=<%=folio_no%>&issuedate=<%=dateselect%>&warrentNo=<%=warrentNo%>"><%=folio_no%></a>
               &nbsp;
             </span></div>
           </div></td>

           <td ><div align="center" class="style10">
           <div align="center" class="style10">
             <span class="style13">
               <a HREF="<%=dest%>?foliono=<%=folio_no%>&issuedate=<%=dateselect%>&warrentNo=<%=warrentNo%>"><%=dateselect%></a>
               &nbsp;
             </span></div>
           </div></td>

           <td ><div align="center" class="style10">
           <div align="center" class="style10">
             <span class="style13">
               <a HREF="<%=dest%>?foliono=<%=folio_no%>&issuedate=<%=dateselect%>&warrentNo=<%=warrentNo%>"><%=warrentNo%></a>
               &nbsp;
             </span></div>
           </div></td>

<td class="style13"><div align="justify" class="style12">
  <div align="right"><%=totalAmount%></div>
</div></td>
<td class="style13"><div align="justify" class="style12">
  <div align="right"><%=taxAmount%></div>
</div></td>
<td class="style13"><div align="center" class="style12">
  <div align="center"><%=collStatus%></div>
</div></td>
       </tr>
       <div align="left" class="style13">
       <%
           }//End of Chaque while
         }

     }

   if (isConnect) {
     try {
       cm.takeDown();
       cm1.takeDown();
     }
     catch(Exception e) {
     }
   }
  %>
         </div>
         </table>
  </form>

</body>
</html>
