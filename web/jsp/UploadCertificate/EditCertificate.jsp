<script>
/*
'******************************************************************************************************************************************
'Script Author : Mohammad Rahat Uddin
'Creation Date : April 2010
'Page Purpose  : Validate Certificate info
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>

<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style19 {color: #0A2769; font-weight: bold; }
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit Certificate</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<%@ page language="java" import="java.lang.Math.*,org.apache.poi.hssf.usermodel.*,java.util.Vector,java.io.*,java.util.*,java.util.List,java.util.Vector,org.apache.poi.hssf.usermodel.HSSFSheet,org.apache.poi.hssf.usermodel.HSSFCell,org.apache.poi.hssf.usermodel.HSSFRow,org.apache.poi.poifs.filesystem.POIFSFileSystem,java.util.Date,java.text.SimpleDateFormat;" %>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
function openPhotoWindow()
{
  	window.open("UploadPhoto.jsp","","HEIGHT=350,WIDTH=500")
}
function Submitme()
{
  document.forms[0].submit();
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }%>
<form action="ValidateCertificateUpdate.jsp" method="post">
    <table width="100%" border="0" cellpadding="4" style="border-collapse: collapse">
      <tr> <td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Update New Certificate</center></td>
    </tr>
	</Table>
    <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">


<%

      int recno=0;
      String recordnostr=String.valueOf(request.getParameter("RECORD_NO"));
      int recordno=Integer.parseInt(recordnostr);
try {

      String folioName="";
      String companyname="";
      Date dmatdate = null;
      String folioNo="";
      String certNo = "";
      String shareno="";
      String distFrom = "";
      String distTo = "";
      Date dateValue = null;
      int a=0;
      int b=0;
      int c=0;
      int e=0;
      int f=0;

      String dmatdatestr="";
      String dateValuestr="";
      String nCertificate ="";
      int blankcount=0;
      cm.queryExecute("select distinct TMP_FOLIO_CERTI.* from TMP_FOLIO_CERTI where TMP_FOLIO_CERTI.RECORD_NO="+recordno);
      if(cm.toNext())
      {
        %>
        <tr bgcolor="#E8F3FD" class="style9">
          <td width="35%">Record No.</td>
		  <td width="2%">:</td>
		  <td><%=cm.getColumnI("RECORD_NO")%></td>
        </tr>
        <tr bgcolor="#E8F3FD" class="style9">
          <td>Follo Name</td>
          <td>:</td><td><%=cm.getColumnS("FOLIO_NAME")%></td>
        </tr>
        <tr bgcolor="#E8F3FD" class="style9">
          <td>Company Name</td>
          <td>:</td><td><%=cm.getColumnS("COMPANY_NAME")%></td>
        </tr>
        <tr bgcolor="#E8F3FD" class="style9">
          <td>Holder Follo</td>
          <td>:</td><td><%=cm.getColumnS("HOLDER_FOLIO")%></td>
        </tr>
        <tr bgcolor="#E8F3FD" class="style9">
          <td>Dmat Date</td>
          <td>:</td><td><input type="text" class="SL74TextField" name="DMAT_DT" id="DMAT_DT" value="<%=cm.getColumnDT("DMAT_DT")%>"/></td>
        </tr>
        <tr bgcolor="#E8F3FD" class="style9">
          <td>Certificate No.</td>
          <td>:</td><td><input type="text" class="SL74TextField" name="CERTIFICATE_NO" id="CERTIFICATE_NO" value="<%=cm.getColumnS("CERTIFICATE_NO")%>"/></td>
        </tr>
        <tr bgcolor="#E8F3FD" class="style9">
          <td>Number of Share</td>
          <td>:</td><td><input type="text" class="SL74TextField" name="NO_SHARES" id="NO_SHARES" value="<%=cm.getColumnS("NO_SHARES")%>"/></td>
        </tr>
        <tr bgcolor="#E8F3FD" class="style9">
          <td>Dist. From</td>
          <td>:</td><td><input type="text" class="SL74TextField" name="DIS_NO_FM" id="DIS_NO_FM" value="<%=cm.getColumnS("DIS_NO_FM")%>"/></td>
        </tr>
        <tr bgcolor="#E8F3FD" class="style9">
          <td>Dist. To</td>
          <td>:</td><td><input type="text" class="SL74TextField" name="DIS_NO_TO" id="DIS_NO_TO" value="<%=cm.getColumnS("DIS_NO_TO")%>"/></td>
        </tr>
        <tr bgcolor="#E8F3FD" class="style9">
          <td>Sp Date</td>
          <td>:</td><td><input type="text" class="SL74TextField" name="SP_DATE" id="SP_DATE" value="<%=cm.getColumnDT("SP_DATE")%>"/></td>
        </tr>
        <%
      }
      else{
          %>
          <tr><td colspan="3">No Data for Edit</td></tr>
         <% //System.out.print("select distinct TMP_FOLIO_CERTI.* from TMP_FOLIO_CERTI,DUPLICATE_CERTIFICATE where TMP_FOLIO_CERTI.CERTIFICATE_NO=DUPLICATE_CERTIFICATE.CERTIFICATE_NO and DUPLICATE_CERTIFICATE.recno>1");
        }

    } catch(Exception ioe) {
    ioe.printStackTrace();
}
try{
  if (isConnect)
      {
        cm.takeDown();
      }

    }
    catch(Exception ei){
    ei.printStackTrace();
  }
  %>

  <tr>
      <td colspan="3">
	   <table width="100%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <td width="100%" scope="row" align="center">
            <div align="center">
			
		   	<img name="B2" src="<%=request.getContextPath()%>/images/btnSubmit.gif" onclick="Submitme()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
		    
            </div></td>
      </tr>
   </table>
	  </td>
    </tr>
  </table>
   <input type="hidden" name="recordno" id="recordno" value="<%=recordno%>"/>
  </form>
</body>
</html>
