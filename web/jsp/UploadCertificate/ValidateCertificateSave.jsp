<script>
/*
'******************************************************************************************************************************************
'Script Author : Mohammad Rahat Uddin
'Creation Date : April 2010
'Page Purpose  : Save Validate Certificate info
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>

<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>


<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<%@ page language="java" import="java.lang.Math.*,org.apache.poi.hssf.usermodel.*,java.util.Vector,java.io.*,java.util.*,java.util.List,java.util.Vector,org.apache.poi.hssf.usermodel.HSSFSheet,org.apache.poi.hssf.usermodel.HSSFCell,org.apache.poi.hssf.usermodel.HSSFRow,org.apache.poi.poifs.filesystem.POIFSFileSystem,java.util.Date,java.text.SimpleDateFormat;" %>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>

</head>

<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }%>

<%
      String recno1 =String.valueOf(request.getParameter("recno"));
      String objname="selectrecbox";
      String havingvalue="0";
      String deletecertificate="";
      int recno=Integer.parseInt(recno1);
      System.out.print(recno);
      for(int reccount=1;reccount<=recno;reccount++){
        objname="selectrecbox"+reccount;
        havingvalue=request.getParameter(objname)+","+havingvalue;
      }


      try{
        deletecertificate="delete  from TMP_FOLIO_CERTI where RECORD_NO in ("+havingvalue+")";
        cm.queryExecute(deletecertificate);
        if (isConnect)
        {
          cm.takeDown();
        }
        response.sendRedirect("ValidateCertificate.jsp");
      }
      catch(Exception ei){
        ei.printStackTrace();
      }
  %>
