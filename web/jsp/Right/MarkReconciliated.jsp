<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : February 2006
'Page Purpose  : Marks Reconciliated Folio Numbers Numbers.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="cm2"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Mark Reconciliated</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   boolean isConnect2 = cm2.connect();
   if((isConnect==false) || (isConnect1==false) || (isConnect2==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String rowcounter = String.valueOf(request.getParameter("rowcounter1"));
  String Destination = String.valueOf(request.getParameter("pstatus"));
  String DivDate = String.valueOf(request.getParameter("ddate"));
  String DivType = String.valueOf(request.getParameter("dtype"));
  String FNo = String.valueOf(request.getParameter("searchFolio"));
  String Chunk = String.valueOf(request.getParameter("pchunk"));
  boolean b = true;
  boolean b1 = true;

  int iChunk = Integer.parseInt(Chunk);


//
  String query3t = "SELECT * FROM RIGHTDECLARATION_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY')";
  cm.queryExecute(query3t);

  String thisrightfolio = "";
  String thisrightcertificate = "";

  while(cm.toNext())
  {
   thisrightfolio = cm.getColumnS("RIGHT_FOLIO");
  }

  if (String.valueOf(thisrightfolio).equals("null"))
  {
    thisrightfolio = "";
  }

  query3t = "SELECT * FROM CERTIFICATE_VIEW WHERE HOLDER_FOLIO = '" + thisrightfolio + "'";
  cm.queryExecute(query3t);

  while(cm.toNext())
  {
   thisrightcertificate = cm.getColumnS("CERTIFICATE_NO");
  }

  if (String.valueOf(thisrightcertificate).equals("null"))
  {
    thisrightcertificate = "";
  }

//

  FNo=cm.replace(FNo,"'","''");
  if (String.valueOf(FNo).equals("null"))
  {
    FNo = "";
  }

  if (!FNo.equalsIgnoreCase(""))
  {
      String ifwarexists = "";
      String countfolios = "";
      int cWAR = 0;

      if (DivType.equalsIgnoreCase("SMS"))
      {
        ifwarexists = "SELECT * FROM RIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND RECONCILIATED = 'F' AND FOLIO_NO='"+ FNo+"'";
        countfolios = "SELECT * FROM RIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND RECONCILIATED = 'F'";
      }
      else if (DivType.equalsIgnoreCase("CDBL"))
      {
        ifwarexists = "SELECT * FROM BORIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND RECONCILIATED = 'F' AND BO_ID='"+ FNo+"'";
        countfolios = "SELECT * FROM BORIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND RECONCILIATED = 'F'";
      }

    cWAR = cm.queryExecuteCount(ifwarexists);

    if (cWAR > 0)
    {
      Destination = request.getContextPath() + "/jsp/Right/AllRightReconciliation.jsp?dateselect=";
      Destination = Destination + DivDate + "&orderby=" + DivType + "&StartValue=";

      int tcount = cm.queryExecuteCount(countfolios);

      String getWarrant = "";
      if (DivType.equalsIgnoreCase("SMS"))
      {
       getWarrant = "SELECT rnum FROM (SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM RIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND RECONCILIATED = 'F' ORDER BY SUBSTR(FOLIO_NO,3,6)) div where rownum <= "+tcount+") where rnum >= 1) WHERE FOLIO_NO = '"+FNo+"'";
      }
      else if (DivType.equalsIgnoreCase("CDBL"))
      {
        getWarrant = "SELECT rnum FROM (SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM BORIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND RECONCILIATED = 'F' ORDER BY BO_ID) div where rownum <= "+tcount+") where rnum >= 1) WHERE BO_ID = '"+FNo+"'";
      }

      cm.queryExecute(getWarrant);
      cm.toNext();
      String srr = cm.getColumnS("rnum");
      int rr = Integer.parseInt(srr);
      int err = rr + iChunk;
      Destination = Destination + rr + "&EndValue=" + err;
    }
  }

  String Fldname = "";
  int irowcounter = Integer.parseInt(rowcounter);

  for (int i=1;i<irowcounter+1;i++)
  {
    Fldname = "selectingbox" + String.valueOf(i);
    String BoxValue = String.valueOf(request.getParameter(Fldname));

    Fldname = "inputbox" + String.valueOf(i);
    String SInputValue = String.valueOf(request.getParameter(Fldname));
    int InputValue = 0;

    Fldname = "hiddenbox" + String.valueOf(i);
    String FolioValue = String.valueOf(request.getParameter(Fldname));

    Fldname = "rightbox" + String.valueOf(i);
    int totalrightshares = Integer.parseInt(String.valueOf(request.getParameter(Fldname)));

//    Fldname = "totalbox" + String.valueOf(i);
//    int totalshares = Integer.parseInt(String.valueOf(request.getParameter(Fldname)));

    if (String.valueOf(SInputValue).equals("null"))
    {
      SInputValue = "";
    }
    if (!SInputValue.equalsIgnoreCase(""))
    {
      if (Integer.parseInt(SInputValue) <= totalrightshares)
      {
       InputValue = Integer.parseInt(SInputValue);
      }
    }

    BoxValue=cm.replace(BoxValue,"'","''");

    if (String.valueOf(BoxValue).equals("null"))
    {
      BoxValue = "";
    }

    if (!BoxValue.equalsIgnoreCase(""))
    {
      String marksel = "";
      String rightcertificateno = "";
      int distfrom = 0;
      int distto = 0;
      int sharessold = 0;
      double shareprice = 0;
      double marketprice = 0;
      double premium = 0;
      double discount = 0;
      double tax = 0;
      String Smarketprice = "";
      String rightfolio = "";

      if (DivType.equalsIgnoreCase("SMS"))
      {
       marksel = "call MARK_RECONCILIATED_RIGHT(" + InputValue + ",'" + FolioValue + "','" + DivDate + "')";

       rightcertificateno = cm.LastCertificate();
       String addcert1 = "call ADD_CERTIFICATE('" + rightcertificateno + "', '" + FolioValue + "','T','F','F','F','F','F','')";
       b1 = cm.procedureExecute(addcert1);

       String addcertr1 = "call ADD_CERTIFICATE_RIGHT('" + rightcertificateno + "', '" + FolioValue + "', '" + DivDate + "')";
       b1 = cm.procedureExecute(addcertr1);

       distfrom = cm.LastDistinction();
       distto = distfrom + InputValue - 1;

       String adddist1 = "call ADD_DISTINCTION(" + distfrom + ", " + distto + ",'" + rightcertificateno + "')";
       b1 = cm.procedureExecute(adddist1);

       String sbalance = "call ADD_SHARE_BALANCE('" + FolioValue + "', " + InputValue + ")";
       b1 = cm.procedureExecute(sbalance);

//
     String addtransfer = "call ADD_TRANSFER('" + thisrightfolio + "', '" + FolioValue + "', '" + thisrightcertificate + "', 'R', '" + rightcertificateno + "','" + distfrom + "','" + distto + "'," + InputValue + ")";
     b = cm1.procedureExecute(addtransfer);
//

       String query3 = "SELECT * FROM RIGHTDECLARATION_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY')";
       cm.queryExecute(query3);

       while(cm.toNext())
        {
         sharessold = cm.getColumnI("SHARES_SOLD");
         shareprice = cm.getColumnF("PRICE");
         premium = cm.getColumnF("PREMIUM");
         discount = cm.getColumnF("DISCOUNT");
         tax = cm.getColumnF("TAX");
         Smarketprice = cm.getColumnS("MARKET_PRICE");
         rightfolio = cm.getColumnS("RIGHT_FOLIO");

         if (String.valueOf(Smarketprice).equals("null"))
          Smarketprice = "";

           int rightshare = 0;
           double fractionshare = 0;
           int boughtshare = 0;

           String ff3 = "SELECT * FROM RIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND FOLIO_NO = '" + FolioValue + "'";
           cm1.queryExecute(ff3);

           while(cm1.toNext())
           {
             rightshare = cm1.getColumnI("RIGHT_SHARE");
             fractionshare = cm1.getColumnF("FRACTION_SHARE");
             boughtshare = cm1.getColumnI("BOUGHT");
           }

           int nbshares = rightshare - boughtshare;
           if (nbshares > 0)
           {
             String ff4a = "call UPDATE_RIGHT_FRACTION('" + rightfolio + "','" + DivDate + "'," + nbshares + ")";
             boolean nb1 = cm2.procedureExecute(ff4a);

//             String ff4b = "call ADD_SHARE_BALANCE('" + rightfolio + "', " + nbshares + ")";
//             nb1 = cm2.procedureExecute(ff4b);
           }

         if (!Smarketprice.equalsIgnoreCase(""))
         {
           if (rightshare == boughtshare)
           {
             marketprice = Double.parseDouble(Smarketprice);
             double fractionamount = fractionshare * (marketprice - (shareprice + premium + tax - discount));
             fractionamount = bcal.roundtovalue(fractionamount,2);

             String ff4 = "";
             if (fractionamount > 0)
             {
              String warrantno = cm1.LastWarrant();
              ff4 = "call UPDATE_RIGHT_FRACTION_AMOUNT('" + DivDate + "','" + FolioValue + "'," + fractionamount + ",'" + warrantno + "')";
             }
             else
             {
              ff4 = "call ADD_FRACTION_RIGHT(" + fractionamount + ",'" + FolioValue + "','" + DivDate + "')";
             }

             b1 = cm1.procedureExecute(ff4);
           }
          }

         sharessold = sharessold + InputValue;
         String ssold1 = "call UPDATE_SHARES_SOLD('" + DivDate + "'," + sharessold + ")";
         b1 = cm1.procedureExecute(ssold1);
        }
      }
      else if (DivType.equalsIgnoreCase("CDBL"))
      {
        marksel = "call MARK_RECONCILIATED_BORIGHT(" + InputValue + ",'" + FolioValue + "','" + DivDate + "')";

        String sbalance1 = "call ADD_BOSHARE_BALANCE('" + FolioValue + "', " + InputValue + ")";
        b1 = cm.procedureExecute(sbalance1);

        String query3 = "SELECT * FROM RIGHTDECLARATION_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY')";
        cm.queryExecute(query3);

        while(cm.toNext())
        {
         sharessold = cm.getColumnI("SHARES_SOLD");
         shareprice = cm.getColumnF("PRICE");
         premium = cm.getColumnF("PREMIUM");
         discount = cm.getColumnF("DISCOUNT");
         tax = cm.getColumnF("TAX");
         Smarketprice = cm.getColumnS("MARKET_PRICE");
         rightfolio = cm.getColumnS("RIGHT_FOLIO");

         if (String.valueOf(Smarketprice).equals("null"))
          Smarketprice = "";

           int rightshare = 0;
           double fractionshare = 0;
           int boughtshare = 0;

           String ff3 = "SELECT * FROM BORIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND BO_ID = '" + FolioValue + "'";
           cm1.queryExecute(ff3);

           while(cm1.toNext())
           {
             rightshare = cm1.getColumnI("RIGHT_SHARE");
             fractionshare = cm1.getColumnF("FRACTION_SHARE");
             boughtshare = cm1.getColumnI("BOUGHT");
           }

           int nbshares = rightshare - boughtshare;
           if (nbshares > 0)
           {
             String ff4a = "call UPDATE_RIGHT_FRACTION('" + rightfolio + "','" + DivDate + "'," + nbshares + ")";
             boolean nb1 = cm2.procedureExecute(ff4a);

//             String ff4b = "call ADD_SHARE_BALANCE('" + rightfolio + "', " + nbshares + ")";
//             nb1 = cm2.procedureExecute(ff4b);
           }

         if (!Smarketprice.equalsIgnoreCase(""))
         {
           if (rightshare == boughtshare)
           {
             marketprice = Double.parseDouble(Smarketprice);
             double fractionamount = fractionshare * (marketprice - (shareprice + premium + tax - discount));
             fractionamount = bcal.roundtovalue(fractionamount,2);

             String ff4 = "";
             if (fractionamount > 0)
             {
              String warrantno = cm1.LastWarrant();
              ff4 = "call UPDATE_RIGHT_FRACTION_AMOUNT('" + DivDate + "','" + FolioValue + "'," + fractionamount + ",'" + warrantno + "')";
             }
             else
             {
              ff4 = "call ADD_FRACTION_RIGHT(" + fractionamount + ",'" + FolioValue + "','" + DivDate + "')";
             }

             boolean b1a = cm1.procedureExecute(ff4);
           }
           else
           {

           }
         }

         sharessold = sharessold + InputValue;
         String ssold1 = "call UPDATE_SHARES_SOLD('" + DivDate + "'," + sharessold + ")";
         boolean b1b = cm1.procedureExecute(ssold1);
        }
      }

      b = cm.procedureExecute(marksel);
    }
    else
    {
      String marksel = "";
      if (DivType.equalsIgnoreCase("SMS"))
      {
        marksel = "call MARK_RECONCILIATED_RIGHT1(" + InputValue + ",'" + FolioValue + "','" + DivDate + "')";
      }
      else if (DivType.equalsIgnoreCase("CDBL"))
      {
        marksel = "call MARK_RECONCILIATED_BORIGHT1(" + InputValue + ",'" + FolioValue + "','" + DivDate + "')";
      }
      b = cm.procedureExecute(marksel);
    }
  }

  String query4 = "SELECT * FROM RIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND RECONCILIATED = 'F'";
  int alldone = cm.queryExecuteCount(query4);

  if (alldone == 0)
  {
    query4 = "SELECT * FROM RIGHTDECLARATION_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY')";
    cm.queryExecute(query4);

    String rightfolio = "";
    int tshares = 0;
    String Sproposedunits = "";
    int proposedunits = 0;
    int eshares = 0;
    int distfrom1 = 0;
    int distto1 = 0;
    boolean b3;
    int sharessold1 = 0;
    String rightcertificateno1 = "";

     while(cm.toNext())
     {
       sharessold1 = cm.getColumnI("SHARES_SOLD");
       tshares = cm.getColumnI("TOTAL_RIGHT_SHARES");
       Sproposedunits = cm.getColumnS("PROPOSED_UNITS");
       rightfolio = cm.getColumnS("RIGHT_FOLIO");
     }
       eshares = tshares - sharessold1;

//       sharessold = sharessold + rightshares;

       if (String.valueOf(Sproposedunits).equals("null"))
        Sproposedunits = "";

       if (Sproposedunits.equalsIgnoreCase(""))
       {
         rightcertificateno1 = cm.LastCertificate();
         distfrom1 = cm.LastDistinction();
         distto1 = distfrom1 + eshares - 1;

         String addcert3 = "call ADD_SHAREHOLDER_BONUS('" + rightfolio + "','Mr.','Company Secretary','Sir','Bangladesh Tobacco Company',0,'D'," + eshares + ",0,'T','F','T','Multinational','0','T','" + DivDate + "')";
         b3 = cm.procedureExecute(addcert3);

         String addcert2 = "call ADD_CERTIFICATE('" + rightcertificateno1 + "', '" + rightfolio + "','T','F','F','F','F','F','Right Shares')";
         b3 = cm.procedureExecute(addcert2);

         String addcertr2 = "call ADD_CERTIFICATE_RIGHT('" + rightcertificateno1 + "', '" + rightfolio + "', '" + DivDate + "')";
         b3 = cm.procedureExecute(addcertr2);

         String adddist2 = "call ADD_DISTINCTION(" + distfrom1 + ", " + distto1 + ",'" + rightcertificateno1 + "')";
         b3 = cm.procedureExecute(adddist2);
       }
       else
       {
         String addcert3 = "call ADD_SHAREHOLDER_BONUS('" + rightfolio + "','Mr.','Company Secretary','Sir','Bangladesh Tobacco Company',0,'D'," + eshares + ",0,'T','F','T','Multinational','0','T','" + DivDate + "')";
         b3 = cm.procedureExecute(addcert3);

         proposedunits = Integer.parseInt(Sproposedunits);
         double chunks = eshares/proposedunits;
         distfrom1 = cm.LastDistinction();
         int thisdistfrom = distfrom1;
         int thisdistto = 0;

         for (double i1=chunks;i1>0;i1=i1-1)
         {
          thisdistto = thisdistfrom + proposedunits - 1;
          rightcertificateno1 = cm.LastCertificate();

          String addcert4 = "call ADD_CERTIFICATE('" + rightcertificateno1 + "', '" + rightfolio + "','T','F','F','F','F','F','Right Shares')";
          b3 = cm.procedureExecute(addcert4);

          String addcertr3 = "call ADD_CERTIFICATE_RIGHT('" + rightcertificateno1 + "', '" + rightfolio + "', '" + DivDate + "')";
          b3 = cm.procedureExecute(addcertr3);

          String adddist4 = "call ADD_DISTINCTION(" + thisdistfrom + ", " + thisdistto + ",'" + rightcertificateno1 + "')";
          b3 = cm.procedureExecute(adddist4);

          thisdistfrom = thisdistfrom + proposedunits;
         }
       }
  }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Reconciled Right Share Information')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
     cm1.takeDown();
     cm2.takeDown();
   }

  if (Destination.equalsIgnoreCase("close"))
  {
   %>
     <script language="javascript">
       location = "<%=request.getContextPath()%>/jsp/Home.jsp";
    </script>
   <%
  }
  else
  {
   %>
     <script language="javascript">
       location = "<%=Destination%>";
    </script>
   <%
  }
%>
</body>
</html>
