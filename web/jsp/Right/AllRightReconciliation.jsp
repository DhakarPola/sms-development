<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : February 2006
'Page Purpose  : View of all Right Share Reconciliations.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@ page language="java" import="java.sql.*,java.util.Vector,java.util.Date,java.text.SimpleDateFormat,java.math.*"%>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

    String DivDate = String.valueOf(request.getParameter("dateselect"));

    String query1cc1 = "SELECT * FROM RIGHTDECLARATION_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY')";
    cm.queryExecute(query1cc1);

    double tprice = 0;
    double tpremium = 0;
    double tdiscount = 0;
    double ttax = 0;
    double prsv = 0;
    double ssprice = 0;

    while(cm.toNext())
     {
       tprice = cm.getColumnF("PRICE");
       tpremium = cm.getColumnF("PREMIUM");
       tdiscount = cm.getColumnF("DISCOUNT");
       ttax = cm.getColumnF("TAX");

       prsv = tprice + tpremium - tdiscount + ttax;
       prsv = cal.roundtovalue(prsv,2);
     }
%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
.style19 {
	color: #0A2769;
	font-weight: bold;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Right Share Reconciliation</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

function goPrevious(pdate,ptype,pstart,pend)
{
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Right/AllRightReconciliation.jsp?dateselect=" + pdate;
  thisurl = thisurl + "&orderby=" + ptype;
  thisurl = thisurl + "&StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;

  document.forms[0].pstatus.value = thisurl;
  document.forms[0].action = "MarkReconciliated.jsp";
  document.forms[0].submit();
}

function goNext(ndate,ntype,nstart,nend)
{
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Right/AllRightReconciliation.jsp?dateselect=" + ndate;
  thisurl = thisurl + "&orderby=" + ntype;
  thisurl = thisurl + "&StartValue=" + nstart;
  thisurl = thisurl + "&EndValue=" + nend;

  document.forms[0].pstatus.value = thisurl;
  document.forms[0].action = "MarkReconciliated.jsp";
  document.forms[0].submit();
}

function toreconcile(xdate,xtype,xstart,xend)
{
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Right/AllRightReconciliation.jsp?dateselect=" + xdate;
  thisurl = thisurl + "&orderby=" + xtype;
  thisurl = thisurl + "&StartValue=" + xstart;
  thisurl = thisurl + "&EndValue=" + xend;

  document.forms[0].pstatus.value = thisurl;
  document.forms[0].action = "MarkReconciliated.jsp";
  document.forms[0].submit();
}

function closedocument()
{
  document.forms[0].action = "MarkReconciliated.jsp";
  document.forms[0].submit();
}

function toreclaccept(ddate,dtype)
{
  if (confirm("All Right Shares will be Reconciliated.\nThat is, it will be assumed that everybody have bought all their issued shares.\nDo you want to continue?"))
  {
   location = "<%=request.getContextPath()%>/jsp/Right/AcceptRightReconciliated.jsp?datedeclared=" + ddate + "&stype=" + dtype;
  }
}

function gosearch(sdate,stype,sstart,send)
{
  if (document.all.searchFolio.value.length > 0)
  {
    var thisurl = "<%=request.getContextPath()%>";
    thisurl = thisurl + "/jsp/Right/AllRightReconciliation.jsp?dateselect=" + sdate;
    thisurl = thisurl + "&orderby=" + stype;
    thisurl = thisurl + "&StartValue=" + sstart;
    thisurl = thisurl + "&EndValue=" + send;

    document.forms[0].pstatus.value = thisurl;
    document.forms[0].action = "MarkReconciliated.jsp";
    document.forms[0].submit();
  }
}

/* function toprintdocument()
 {
  if (confirm("Do you want to Print the Document?"))
  {
   document.forms[0].submit();
  }
 }
*/

function setboxvalues(FldName1,FldName2)
{
  var var1 = document.forms[0].elements[FldName2].value;
  var1 = Number(var1) * <%=prsv%>;
  var var2 = parseFloat(var1);
  document.forms[0].elements[FldName1].value = var2;
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style17 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
.SL76aTextField{
	width:100px;
	font-family:arial;
        text-align:center;
	font-size:11px;
	border:solid black 1px;
	background:#FFFFFF
}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
    String DivType = String.valueOf(request.getParameter("orderby"));
    String SStartingValue = String.valueOf(request.getParameter("StartValue"));
    String SEndingValue = String.valueOf(request.getParameter("EndValue"));

    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    int Chunk = 20;
    EndingValue = StartingValue + Chunk - 1;

    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;
    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }

    if (DivType.equalsIgnoreCase("SMS"))
    {
    String query1cc = "SELECT * FROM RIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND RECONCILIATED = 'F' ORDER BY SUBSTR(FOLIO_NO,3,6)";
    int fcc = cm.queryExecuteCount(query1cc);

    if (nStartingValue > (fcc-Chunk+1))
    {
      nStartingValue = fcc - Chunk + 1;
    }
    if (nEndingValue > fcc)
    {
      nEndingValue = fcc;
    }
    if (nStartingValue < 1)
    {
      nStartingValue = 1;
    }
    if (nEndingValue < Chunk)
    {
      nEndingValue = Chunk;
    }

    String query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM RIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND RECONCILIATED = 'F' ORDER BY SUBSTR(FOLIO_NO,3,6)) div where rownum <= " + EndingValue + ") where rnum >= " + StartingValue + "";
    cm.queryExecute(query1);

    String foliono = "";
    int totalshare = 0;
    int rightshare = 0;
    double fractionshare = 0;
    int bought = 0;
    String isreconciliated = "";
    int rowcounter1 = 0;
    BigDecimal bssprice = new BigDecimal(0);
%>
  <span class="style7">
  <form method="GET" action="MarkReconciliated.jsp">
  <SPAN id="dprint">
    <img name="B8" src="<%=request.getContextPath()%>/images/btnReconcile.gif" onclick="toreconcile('<%=DivDate%>','<%=DivType%>',<%=StartingValue%>,<%=EndingValue%>)" onMouseOver="document.forms[0].B8.src = '<%=request.getContextPath()%>/images/btnReconcileR.gif'" onMouseOut="document.forms[0].B8.src = '<%=request.getContextPath()%>/images/btnReconcile.gif'">
    <img name="B4" src="<%=request.getContextPath()%>/images/btnReconcileAll.gif" onclick="toreclaccept('<%=DivDate%>','<%=DivType%>')" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnReconcileAllR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnReconcileAll.gif'">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
    <img name="B7" src="<%=request.getContextPath()%>/images/btnClose.gif" onclick="closedocument()" onMouseOver="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
  </SPAN>
  <table width="100%" BORDER=0  cellpadding="0" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td height="30" bgcolor="#0044B0" class="style17" valign="middle"><center>Right Share Reconciliation - SMS [<%=DivDate%>]</center></td></tr>
  </table>
  <table width="100%"  border="0" cellpadding="0" cellspacing="0" bordercolor="#0044B0">
      <tr bgcolor="#E8F3FD">
	<td width="10%" style="border-left: solid 1px #0044B0;">&nbsp;</td>
	<td width="10%"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goPrevious('<%=DivDate%>','<%=DivType%>',<%=pStartingValue%>,<%=pEndingValue%>)"></td>
	<td width="29%"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNext('<%=DivDate%>','<%=DivType%>',<%=nStartingValue%>,<%=nEndingValue%>)"></td>
	<td width="12%" height="35" class="style19" align="right">Folio No:&nbsp;</td>
	<td width="19%"><input name="searchFolio" type="text" class="rhakim" onkeypress="keypressOnNumberFld()"></td>
	<td width="20%" style="border-right: solid 1px #0044B0;"><img src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="gosearch('<%=DivDate%>','<%=DivType%>',<%=StartingValue%>,<%=EndingValue%>)"></td>
      </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="15%" class="style9"><div align="center">Folio No.</div></td>
    <td width="13%"><div align="center" class="style9">
      <div align="center">Total Shares</div>
    </div></td>
    <td width="13%"><div align="center" class="style9">
      <div align="center">Right Shares</div>
    </div></td>
    <td width="14%"><div align="center" class="style9">
      <div align="center">Fraction Shares</div>
    </div></td>
    <td width="17%"><div align="center" class="style9">
      <div align="center">Bought</div>
    </div></td>
    <td width="17%"><div align="center" class="style9">
      <div align="center">Share Price</div>
    </div></td>
    <td width="11%"><div align="center" class="style9">
      <div align="center">Reconciled</div>
    </div></td>
  </tr>
  <div align="left">
     <%
    while(cm.toNext())
     {
       rowcounter1++;
       foliono = cm.getColumnS("FOLIO_NO");
       totalshare = cm.getColumnI("TOTAL_SHARE");
       rightshare = cm.getColumnI("RIGHT_SHARE");
       fractionshare = cm.getColumnF("FRACTION_SHARE");
       bought = cm.getColumnI("BOUGHT");
       isreconciliated = cm.getColumnS("RECONCILIATED");

       if (String.valueOf(foliono).equals("null"))
         foliono = "";
       if (String.valueOf(isreconciliated).equals("null"))
         isreconciliated = "";

       fractionshare = cal.roundtovalue(fractionshare,4);
     %>
    </div>
  <tr bgcolor="#E8F3FD">
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=foliono%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=totalshare%>&nbsp;</div>
             <input type="hidden" name="totalbox<%=rowcounter1%>" value="<%=totalshare%>">
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=rightshare%>&nbsp;</div>
             <input type="hidden" name="rightbox<%=rowcounter1%>" value="<%=rightshare%>">
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=fractionshare%>&nbsp;</div>
           </div></td>
           <%
             if (bought > 0)
             {
              ssprice = prsv * bought;
              bssprice = new BigDecimal(ssprice);
              bssprice = bssprice.setScale(2,BigDecimal.ROUND_HALF_UP);
              ssprice = cal.roundtovalue(ssprice,2);
             %>
             <td class="style13"><div align="center" class="style12">
              <div align="center">
               <input type="text" class="SL76aTextField" name="inputbox<%=rowcounter1%>" value="<%=bought%>" onkeypress="keypressOnNumberFld()" onblur="setboxvalues('pricebox<%=rowcounter1%>','inputbox<%=rowcounter1%>')">
               <input type="hidden" name="hiddenbox<%=rowcounter1%>" value="<%=foliono%>">
              </div>
             </div></td>
             <%
             }
             else
             {
              ssprice = prsv * rightshare;
              bssprice = new BigDecimal (ssprice);
              bssprice = bssprice.setScale(2,BigDecimal.ROUND_HALF_UP);
              ssprice = cal.roundtovalue(ssprice,2);
             %>
             <td><div align="center">
              <div align="center">
               <input type="text" class="SL76aTextField" name="inputbox<%=rowcounter1%>" value="<%=rightshare%>" onkeypress="keypressOnNumberFld()" onblur="setboxvalues('pricebox<%=rowcounter1%>','inputbox<%=rowcounter1%>')">
               <input type="hidden" name="hiddenbox<%=rowcounter1%>" value="<%=foliono%>">
              </div>
             </div></td>
             <%
             }
           %>
             <td class="style13"><div align="center" class="style12">
              <div align="center">
               <input type="text" class="SL76aTextField" name="pricebox<%=rowcounter1%>" value="<%=bssprice%>" onkeypress="keypressOnNumberFld()" onfocus="this.blur()">
              </div>
             </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">
               <input type="checkbox" name="selectingbox<%=rowcounter1%>" value="<%=foliono%>">
             </div>
           </div></td>
         </tr>
         <div align="left" class="style13">
             <%
         }
         %>
           <!--HIDDEN FIELDS-->
            <input type="hidden" name="rowcounter1" value="<%=rowcounter1%>">
            <input type="hidden" name="ddate" value="<%=DivDate%>">
            <input type="hidden" name="dtype" value="<%=DivType%>">
            <input type="hidden" name="svalue" value="<%=StartingValue%>">
            <input type="hidden" name="evalue" value="<%=EndingValue%>">
            <input type="hidden" name="pstatus" value="close">
            <input type="hidden" name="pchunk" value="<%=Chunk%>">
         <%
    }
    else if (DivType.equalsIgnoreCase("CDBL"))
    {
    String query1cc = "SELECT * FROM BORIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND RECONCILIATED = 'F' ORDER BY BO_ID";
    int fcc = cm.queryExecuteCount(query1cc);

    if (nStartingValue > (fcc-Chunk+1))
    {
      nStartingValue = fcc - Chunk + 1;
    }
    if (nEndingValue > fcc)
    {
      nEndingValue = fcc;
    }
    if (nStartingValue < 1)
    {
      nStartingValue = 1;
    }
    if (nEndingValue < Chunk)
    {
      nEndingValue = Chunk;
    }

    String query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM BORIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND RECONCILIATED = 'F' ORDER BY BO_ID) div where rownum <= " + EndingValue + ") where rnum >= " + StartingValue + "";
    cm.queryExecute(query1);

    String boid = "";
    int totalshare = 0;
    int rightshare = 0;
    double fractionshare = 0;
    int bought = 0;
    String isreconciliated = "";
    int rowcounter1 = 0;
    BigDecimal bssprice = new BigDecimal(0);
%>
  <span class="style7">
  <form method="GET" action="MarkReconciliated.jsp">
  <SPAN id="dprint">
    <img name="B8" src="<%=request.getContextPath()%>/images/btnReconcile.gif" onclick="toreconcile('<%=DivDate%>','<%=DivType%>',<%=StartingValue%>,<%=EndingValue%>)" onMouseOver="document.forms[0].B8.src = '<%=request.getContextPath()%>/images/btnReconcileR.gif'" onMouseOut="document.forms[0].B8.src = '<%=request.getContextPath()%>/images/btnReconcile.gif'">
    <img name="B4" src="<%=request.getContextPath()%>/images/btnReconcileAll.gif" onclick="toreclaccept('<%=DivDate%>','<%=DivType%>')" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnReconcileAllR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnReconcileAll.gif'">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
    <img name="B7" src="<%=request.getContextPath()%>/images/btnClose.gif" onclick="closedocument()" onMouseOver="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
  </SPAN>
  <table width="100%" BORDER=0  cellpadding="0" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td height="30" bgcolor="#0044B0" class="style17" valign="middle"><center>Right Share Reconciliation - CDBL [<%=DivDate%>]</center></td></tr>
  </table>
  <table width="100%"  border="0" cellpadding="0" cellspacing="0" bordercolor="#0044B0">
      <tr bgcolor="#E8F3FD">
	<td width="10%" style="border-left: solid 1px #0044B0;">&nbsp;</td>
	<td width="10%"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goPrevious('<%=DivDate%>','<%=DivType%>',<%=pStartingValue%>,<%=pEndingValue%>)"></td>
	<td width="29%"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNext('<%=DivDate%>','<%=DivType%>',<%=nStartingValue%>,<%=nEndingValue%>)"></td>
	<td width="12%" height="35" class="style19" align="right">BO ID:&nbsp;</td>
	<td width="19%"><input name="searchFolio" type="text" class="rhakim" onkeypress="keypressOnNumberFld()"></td>
	<td width="20%" style="border-right: solid 1px #0044B0;"><img src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="gosearch('<%=DivDate%>','<%=DivType%>',<%=StartingValue%>,<%=EndingValue%>)"></td>
      </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="15%" class="style9"><div align="center">BO ID</div></td>
    <td width="14%"><div align="center" class="style9">
      <div align="center">Total Shares</div>
    </div></td>
    <td width="14%"><div align="center" class="style9">
      <div align="center">Right Shares</div>
    </div></td>
    <td width="13%"><div align="center" class="style9">
      <div align="center">Fraction Shares</div>
    </div></td>
    <td width="17%"><div align="center" class="style9">
      <div align="center">Bought</div>
    </div></td>
    <td width="17%"><div align="center" class="style9">
      <div align="center">Share Price</div>
    </div></td>
    <td width="11%"><div align="center" class="style9">
      <div align="center">Reconciled</div>
    </div></td>
  </tr>
  <div align="left">
     <%
    while(cm.toNext())
     {
       rowcounter1++;
       boid = cm.getColumnS("BO_ID");
       totalshare = cm.getColumnI("TOTAL_SHARE");
       rightshare = cm.getColumnI("RIGHT_SHARE");
       fractionshare = cm.getColumnF("FRACTION_SHARE");
       bought = cm.getColumnI("BOUGHT");
       isreconciliated = cm.getColumnS("RECONCILIATED");

       if (String.valueOf(boid).equals("null"))
         boid = "";
       if (String.valueOf(isreconciliated).equals("null"))
         isreconciliated = "";

       fractionshare = cal.roundtovalue(fractionshare,4);
     %>
    </div>
  <tr bgcolor="#E8F3FD">
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=boid%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=totalshare%>&nbsp;</div>
             <input type="hidden" name="totalbox<%=rowcounter1%>" value="<%=totalshare%>">
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=rightshare%>&nbsp;</div>
             <input type="hidden" name="rightbox<%=rowcounter1%>" value="<%=rightshare%>">
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=fractionshare%>&nbsp;</div>
           </div></td>
           <%
             if (bought > 0)
             {
             %>
             <td class="style13"><div align="center" class="style12">
              <div align="center">
               <input type="text" class="SL76aTextField" name="inputbox<%=rowcounter1%>" value="<%=bought%>" onkeypress="keypressOnNumberFld()" onblur="setboxvalues('pricebox<%=rowcounter1%>','inputbox<%=rowcounter1%>')">
               <input type="hidden" name="hiddenbox<%=rowcounter1%>" value="<%=boid%>">
              </div>
             </div></td>
             <%
              ssprice = prsv * bought;
              bssprice = new BigDecimal(ssprice);
              bssprice = bssprice.setScale(2,BigDecimal.ROUND_HALF_UP);
              ssprice = cal.roundtovalue(ssprice,2);
             }
             else
             {
             %>
             <td class="style13"><div align="center" class="style12">
              <div align="center">
               <input type="text" class="SL76aTextField" name="inputbox<%=rowcounter1%>" value="<%=rightshare%>" onkeypress="keypressOnNumberFld()" onblur="setboxvalues('pricebox<%=rowcounter1%>','inputbox<%=rowcounter1%>')">
               <input type="hidden" name="hiddenbox<%=rowcounter1%>" value="<%=boid%>">
              </div>
             </div></td>
             <%
              ssprice = prsv * rightshare;
              bssprice = new BigDecimal(ssprice);
              bssprice = bssprice.setScale(2,BigDecimal.ROUND_HALF_UP);
              ssprice = cal.roundtovalue(ssprice,2);
             }
           %>
           <td class="style13"><div align="center" class="style12">
            <div align="center">
             <input type="text" class="SL76aTextField" name="pricebox<%=rowcounter1%>" value="<%=bssprice%>" onkeypress="keypressOnNumberFld()" onfocus="this.blur()">
            </div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">
               <input type="checkbox" name="selectingbox<%=rowcounter1%>" value="<%=boid%>">
             </div>
           </div></td>
         </tr>
         <div align="left" class="style13">
             <%
         }
         %>
           <!--HIDDEN FIELDS-->
            <input type="hidden" name="rowcounter1" value="<%=rowcounter1%>">
            <input type="hidden" name="ddate" value="<%=DivDate%>">
            <input type="hidden" name="dtype" value="<%=DivType%>">
            <input type="hidden" name="svalue" value="<%=StartingValue%>">
            <input type="hidden" name="evalue" value="<%=EndingValue%>">
            <input type="hidden" name="pstatus" value="close">
            <input type="hidden" name="pchunk" value="<%=Chunk%>">
         <%
    }

  if (isConnect)
   {
     cm.takeDown();
   }
  %>
         </div>
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>

</body>
</html>
