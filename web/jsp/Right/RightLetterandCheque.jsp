<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : March 2006
'Page Purpose  : Sets Criteria for Printing Letter and Cheque for Right Fraction Amount.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Right Letter and Cheque Printing</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function openrangespans()
{
 document.all.rangespan1.style.display = '';
 document.all.rangespan2.style.display = '';
 document.all.rangespan3.style.display = '';
 document.all.rangespan4.style.display = '';
 document.all.rangespan5.style.display = '';
 document.all.rangespan6.style.display = '';
 document.all.singlespan1.style.display = 'none';
 document.all.singlespan2.style.display = 'none';
 document.all.singlespan3.style.display = 'none';
}

function opensinglespans()
{
 document.all.rangespan1.style.display = 'none';
 document.all.rangespan2.style.display = 'none';
 document.all.rangespan3.style.display = 'none';
 document.all.rangespan4.style.display = 'none';
 document.all.rangespan5.style.display = 'none';
 document.all.rangespan6.style.display = 'none';
 document.all.singlespan1.style.display = '';
 document.all.singlespan2.style.display = '';
 document.all.singlespan3.style.display = '';
}

function closeallspans()
{
 document.all.rangespan1.style.display = 'none';
 document.all.rangespan2.style.display = 'none';
 document.all.rangespan3.style.display = 'none';
 document.all.rangespan4.style.display = 'none';
 document.all.rangespan5.style.display = 'none';
 document.all.rangespan6.style.display = 'none';
 document.all.singlespan1.style.display = 'none';
 document.all.singlespan2.style.display = 'none';
 document.all.singlespan3.style.display = 'none';
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  SelectValidate('dateselect','- Declaration Date (Option must be entered)');
  SelectValidate('sigselect','- Signature (Option must be entered)');

  var radio_choice;
  var counter;

  for (counter = 0; counter < document.forms[0].printscope.length; counter++)
   {
    if (document.forms[0].printscope[counter].checked)
    {
      radio_choice = document.forms[0].printscope[counter].value;
    }
   }

  if (radio_choice == "single")
  {
    BlankValidate('swarrantno','- Warrant No. (Option must be entered)');
  }
  else if (radio_choice == "range")
  {
    BlankValidate('rwarrantno1','- Warrant Range From (Option must be entered)');
    BlankValidate('rwarrantno2','- Warrant Range To (Option must be entered)');
  }

  if (count == 0)
  {
    document.forms[0].submit();
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
%>

<form action="<%=request.getContextPath()%>/RightFractionLetter.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Letter and Cheque for Fractional Amount</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Type</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left" class="style9">
            <input name="righttype" type="radio" value="SMStype" checked="checked">&nbsp;SMS<br>
            <input name="righttype" type="radio" value="CDBLtype">&nbsp;CDBL
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date of Declaration</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="dateselect" class="SL1TextFieldListBox">
            <option>--- Please Select ---</option>
            <%
              String thedate = "";

              String query1 = "SELECT DISTINCT DATE_DEC FROM RIGHTDECLARATION_VIEW ORDER BY DATE_DEC DESC";
              cm.queryExecute(query1);

              while(cm.toNext())
              {
                 thedate = cm.getColumnDT("DATE_DEC");
                 %>
                   <option value="<%=thedate%>"><%=thedate%></option>
                 <%
              }
            %>
         </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Print Scope</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left" class="style9">
            <input name="printscope" type="radio" value="single" onclick="opensinglespans()">&nbsp;Single<br>
            <input name="printscope" type="radio" value="range" onclick="openrangespans()">&nbsp;Range<br>
            <input name="printscope" type="radio" value="all"  checked="checked" onclick="closeallspans()">&nbsp;All Not Printed
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8"><SPAN id="singlespan1" style="display:none">&nbsp;&nbsp;&nbsp;&nbsp;Warrant No.</span></div></th>
        <td width="1%"><SPAN id="singlespan2" style="display:none">:</span></td>
        <td width="55%"><div align="left">
          <SPAN id="singlespan3" style="display:none">
          <input type="text" name="swarrantno" class="SL2TextField" onkeypress="keypressOnNumberFld()">
          </span>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8"><SPAN id="rangespan1" style="display:none">&nbsp;&nbsp;&nbsp;&nbsp;Warrant Range</SPAN></div></th>
        <td width="1%" valign="top"><SPAN id="rangespan2" style="display:none">:</span></td>
        <td width="55%"><div align="left">
         <table width="75%"  border="0" cellpadding="2">
           <tr>
            <td width="30%" align="left"><div align="left" class="style8">
              <b><SPAN id="rangespan3" style="display:none">From&nbsp;&nbsp;</span></b></div>
            </td>
            <td width="70%" align="left"><div align="left" class="style8">
              <SPAN id="rangespan4" style="display:none">
              <input type="text" name="rwarrantno1" class="SL67TextField" onkeypress="keypressOnNumberFld()"></span></div>
            </td>
           </tr>
           <tr>
            <td width="30%" align="left"><div align="left" class="style8">
              <b><SPAN id="rangespan5" style="display:none">To&nbsp;&nbsp;</span></b></div>
            </td>
            <td width="70%" align="left"><div align="left" class="style8">
              <SPAN id="rangespan6" style="display:none"><input type="text" name="rwarrantno2" class="SL67TextField" onkeypress="keypressOnNumberFld()"></span></div>
            </td>
           </tr>
        </table>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Signature</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="sigselect" class="SL3TextFieldListBox">
            <option>--- Please Select ---</option>
            <%
              String signame = "";
              String sigid = "";
              String sigrank = "";

              String query2 = "SELECT * FROM SIGNATURE_VIEW ORDER BY SIG_ID";
              cm.queryExecute(query2);

              while(cm.toNext())
              {
                 signame = cm.getColumnS("NAME");
                 sigid = cm.getColumnS("SIG_ID");
                 sigrank = cm.getColumnS("RANK");
                 %>
                   <option value="<%=sigid%>"><%=signame%> (<%=sigrank%>)</option>
                 <%
              }
            %>
         </select>
        </div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="50%" scope="row">
            <div align="right">
	      <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
            </div></th>
        <td width="50%" align="left">
 	      <img name="B1" src="<%=request.getContextPath()%>/images/btnOK.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOKOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOK.gif'">
      </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
