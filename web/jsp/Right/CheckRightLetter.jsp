<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : February 2006
'Page Purpose  : Checks Right Share Invitition Letter Information Before Printing.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.util.Date,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Check Right Share Invitition Letter</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

  String DateDec = String.valueOf(request.getParameter("dateselect"));
  String RightType = String.valueOf(request.getParameter("righttype"));
  String PrintScope = String.valueOf(request.getParameter("printscope"));
  String ForBNo = String.valueOf(request.getParameter("foliono"));

  if (String.valueOf(ForBNo).equals("null"))
   ForBNo = "";

  String query3 = "SELECT * FROM LETTER_VIEW WHERE lower(LETTER_NAME)=lower('Right Share Invitation Letter')";
  int letterexists = cm.queryExecuteCount(query3);

  if (letterexists < 1)
  {
   %>
    <script language="javascript">
     alert("Right Share Invitation Letter Template does not Exist!\nFirst make a Template named Right Share Invitition Letter.\nThen, try Printing the Letter Again.");
     location = "<%=request.getContextPath()%>/jsp/Others/OthersLetterEdit.jsp";
    </script>
   <%
  }

  if (RightType.equalsIgnoreCase("SMStype"))
  {
    if (PrintScope.equalsIgnoreCase("printsingle"))
    {
      String query1 = "SELECT * FROM SHAREHOLDER_VIEW WHERE lower(FOLIO_NO)=lower('"+ForBNo+"')";
      int folioexists = cm.queryExecuteCount(query1);

      if (folioexists < 1)
      {
       %>
        <script language="javascript">
         alert("Folio does not Exist!");
         location = "<%=request.getContextPath()%>/jsp/Right/RightLetterPrint.jsp";
//         history.go(-1);
        </script>
       <%
      }
    }
  }
  else if (RightType.equalsIgnoreCase("CDBLtype"))
  {
    if (PrintScope.equalsIgnoreCase("printsingle"))
    {
      String query2 = "SELECT * FROM BOHOLDINGONLY_VIEW WHERE lower(BOID)=lower('"+ForBNo+"')";
      int boexists = cm.queryExecuteCount(query2);

      if (boexists < 1)
      {
       %>
        <script language="javascript">
         alert("BO ID. does not Exist!");
         location = "<%=request.getContextPath()%>/jsp/Right/RightLetterPrint.jsp";
//         history.go(-1);
        </script>
       <%
      }
    }
  }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Printed Right Share Invitation Letter to Shareholder')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
     cm1.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/RightLetter.jsp?dateselect=<%=DateDec%>&righttype=<%=RightType%>&printscope=<%=PrintScope%>&foliono=<%=ForBNo%>";
</script>
</body>
</html>
