<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : January 2006
'Page Purpose  : Checks Right Share Certificate Information Before Printing.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.io.*,java.util.*,java.util.List,oracle.jdbc.driver.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*,org.apache.commons.fileupload.*,java.util.Vector,java.util.StringTokenizer,java.util.Date,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="util" class="batbsms.Utility"/>
<jsp:useBean id="cert" class="batbsms.Certificate"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Check Right Share Certificate</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

  String DateDec = String.valueOf(request.getParameter("dateselect"));
  String PrintScope = String.valueOf(request.getParameter("printscope"));
  String CertificateNo = String.valueOf(request.getParameter("certificateno"));

  System.out.println("Printscope is: " + PrintScope);

  if (String.valueOf(CertificateNo).equals("null"))
   CertificateNo = "";

  if (PrintScope.equalsIgnoreCase("printsingle"))
  {
   String query3 = "SELECT * FROM CERTIFICATE_RIGHT_VIEW WHERE lower(CERTIFICATE_NO)=lower('" + CertificateNo + "') AND DATE_DEC = TO_DATE('" + DateDec + "','DD/MM/YYYY')";
   int Cexists = cm.queryExecuteCount(query3);

   if (Cexists < 1)
   {
    %>
     <script language="javascript">
      alert("Certificate does not Exist!");
      location = "<%=request.getContextPath()%>/jsp/Right/RightCertificates.jsp";
     </script>
    <%
   }
  }

//

  String path=application.getRealPath(request.getContextPath());
  cert.deleteBarcodes();

  if (PrintScope.equalsIgnoreCase("printsingle"))
  {
    cert.insertBarCode(CertificateNo,path);
  }
  else if (PrintScope.equalsIgnoreCase("printall"))
  {
   String query4 = "SELECT * FROM CERTIFICATE_RIGHT_VIEW1 WHERE DATE_DEC = TO_DATE('" + DateDec + "','DD/MM/YYYY')";
   int Cexists1 = cm.queryExecuteCount(query4);

   String thiscertificate = "";

   while(cm.toNext())
   {
     thiscertificate = cm.getColumnS("CERTIFICATE_NO");
     cert.insertBarCode(thiscertificate,path);
   }
  }

//

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Printed Right Share Certificate')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
     cm1.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/PrintRightCertificate.jsp?dateselect=<%=DateDec%>&printscope=<%=PrintScope%>&certificateno=<%=CertificateNo%>";
</script>
</body>
</html>
