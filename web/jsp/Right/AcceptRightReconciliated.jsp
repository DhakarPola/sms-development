<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : February 2006
'Page Purpose  : Marks Reconciliated Folio Numbers Numbers.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="cm2"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Mark Reconciliated</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   boolean isConnect2 = cm2.connect();
   if((isConnect==false) || (isConnect1==false) || (isConnect2==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String DivDate = String.valueOf(request.getParameter("datedeclared"));
  String DivType = String.valueOf(request.getParameter("stype"));

  String markall = "";
  boolean b1;
  boolean b;
  String foliono = "";
  int rightshares = 0;
  int boughtshares = 0;
  String rightcertificateno = "";
  int distfrom = 0;
  int distto = 0;
  int sharessold = 0;
  double shareprice = 0;
  double marketprice = 0;
  double premium = 0;
  double discount = 0;
  double tax = 0;
  String Smarketprice = "";
  double fractionshares = 0;
  boolean b56;

//
  String query3t = "SELECT * FROM RIGHTDECLARATION_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY')";
  cm.queryExecute(query3t);

  String thisrightfolio = "";
  String thisrightcertificate = "";

  while(cm.toNext())
  {
   thisrightfolio = cm.getColumnS("RIGHT_FOLIO");
  }

  if (String.valueOf(thisrightfolio).equals("null"))
  {
    thisrightfolio = "";
  }

  query3t = "SELECT * FROM CERTIFICATE_VIEW WHERE HOLDER_FOLIO = '" + thisrightfolio + "'";
  cm.queryExecute(query3t);

  while(cm.toNext())
  {
   thisrightcertificate = cm.getColumnS("CERTIFICATE_NO");
  }

  if (String.valueOf(thisrightcertificate).equals("null"))
  {
    thisrightcertificate = "";
  }

//

  if (DivType.equalsIgnoreCase("SMS"))
  {
    String query1 = "SELECT * FROM RIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND RECONCILIATED = 'F'";
    cm.queryExecute(query1);

   while(cm.toNext())
   {
     foliono = cm.getColumnS("FOLIO_NO");
     rightshares = cm.getColumnI("RIGHT_SHARE");
     fractionshares = cm.getColumnF("FRACTION_SHARE");
     boughtshares = cm.getColumnI("BOUGHT");

     String query2 = "call MARK_RECONCILIATED_RIGHT(" + rightshares + ",'" + foliono + "','" + DivDate + "')";
     b1 = cm1.procedureExecute(query2);

     rightcertificateno = cm1.LastCertificate();
     String addcert1 = "call ADD_CERTIFICATE('" + rightcertificateno + "', '" + foliono + "','T','F','F','F','F','F','')";
     b1 = cm1.procedureExecute(addcert1);

     String addcertr1 = "call ADD_CERTIFICATE_RIGHT('" + rightcertificateno + "', '" + foliono + "', '" + DivDate + "')";
     b56 = cm1.procedureExecute(addcertr1);

     distfrom = cm1.LastDistinction();
     distto = distfrom + rightshares - 1;

     String adddist1 = "call ADD_DISTINCTION(" + distfrom + ", " + distto + ",'" + rightcertificateno + "')";
     b1 = cm1.procedureExecute(adddist1);

     String sbalance = "call ADD_SHARE_BALANCE('" + foliono + "', " + rightshares + ")";
     b1 = cm1.procedureExecute(sbalance);

//
     String addtransfer = "call ADD_TRANSFER('" + thisrightfolio + "', '" + foliono + "', '" + thisrightcertificate + "', 'R', '" + rightcertificateno + "','" + distfrom + "','" + distto + "'," + rightshares + ")";
     b = cm1.procedureExecute(addtransfer);
//

     String query3 = "SELECT * FROM RIGHTDECLARATION_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY')";
     cm1.queryExecute(query3);

     while(cm1.toNext())
     {
       sharessold = cm1.getColumnI("SHARES_SOLD");
       shareprice = cm1.getColumnF("PRICE");
       premium = cm1.getColumnF("PREMIUM");
       discount = cm1.getColumnF("DISCOUNT");
       tax = cm1.getColumnF("TAX");
       Smarketprice = cm1.getColumnS("MARKET_PRICE");

       if (String.valueOf(Smarketprice).equals("null"))
        Smarketprice = "";

       if (!Smarketprice.equalsIgnoreCase(""))
       {
        if (rightshares == boughtshares)
        {
         marketprice = Double.parseDouble(Smarketprice);
         double fractionamount = fractionshares * (marketprice - (shareprice + premium + tax - discount));
         fractionamount = bcal.roundtovalue(fractionamount,2);

         String ff4 = "";
         if (fractionamount > 0)
         {
          String warrantno = cm2.LastWarrant();
          ff4 = "call UPDATE_RIGHT_FRACTION_AMOUNT('" + DivDate + "','" + foliono + "'," + fractionamount + ",'" + warrantno + "')";
         }
         else
         {
          ff4 = "call ADD_FRACTION_RIGHT(" + fractionamount + ",'" + foliono + "','" + DivDate + "')";
         }

         b1 = cm2.procedureExecute(ff4);
        }
       }

       sharessold = sharessold + rightshares;
       String ssold1 = "call UPDATE_SHARES_SOLD('" + DivDate + "'," + sharessold + ")";
       b1 = cm2.procedureExecute(ssold1);
     }
   }

   String ndividend4 = "call DELETE_RIGHT_T()";
   boolean b5 = cm.procedureExecute(ndividend4);

  }
  else if (DivType.equalsIgnoreCase("CDBL"))
  {
    String query1 = "SELECT * FROM BORIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND RECONCILIATED = 'F'";
    cm.queryExecute(query1);

    while(cm.toNext())
    {
     foliono = cm.getColumnS("BO_ID");
     rightshares = cm.getColumnI("RIGHT_SHARE");
     fractionshares = cm.getColumnF("FRACTION_SHARE");
     boughtshares = cm.getColumnI("BOUGHT");

     String query2 = "call MARK_RECONCILIATED_BORIGHT(" + rightshares + ",'" + foliono + "','" + DivDate + "')";
     b1 = cm1.procedureExecute(query2);

     String sbalance1 = "call ADD_BOSHARE_BALANCE('" + foliono + "', " + rightshares + ")";
     b1 = cm1.procedureExecute(sbalance1);

     String query3 = "SELECT * FROM RIGHTDECLARATION_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY')";
     cm1.queryExecute(query3);

     while(cm1.toNext())
     {
       sharessold = cm1.getColumnI("SHARES_SOLD");
       shareprice = cm1.getColumnF("PRICE");
       premium = cm1.getColumnF("PREMIUM");
       discount = cm1.getColumnF("DISCOUNT");
       tax = cm1.getColumnF("TAX");
       Smarketprice = cm1.getColumnS("MARKET_PRICE");

       if (String.valueOf(Smarketprice).equals("null"))
        Smarketprice = "";

       if (!Smarketprice.equalsIgnoreCase(""))
       {
        if (rightshares == boughtshares)
        {
         marketprice = Double.parseDouble(Smarketprice);
         double fractionamount = fractionshares * (marketprice - (shareprice + premium + tax - discount));
         fractionamount = bcal.roundtovalue(fractionamount,2);

         String ff4 = "";
         if (fractionamount > 0)
         {
          String warrantno = cm2.LastWarrant();
          ff4 = "call UPDATE_BORIGHT_FRACTION_AMOUNT('" + DivDate + "','" + foliono + "'," + fractionamount + ",'" + warrantno + "')";
         }
         else
         {
          ff4 = "call ADD_FRACTION_BORIGHT(" + fractionamount + ",'" + foliono + "','" + DivDate + "')";
         }
         b1 = cm2.procedureExecute(ff4);
        }
       }

       sharessold = sharessold + rightshares;
       String ssold1 = "call UPDATE_SHARES_SOLD('" + DivDate + "'," + sharessold + ")";
       b1 = cm2.procedureExecute(ssold1);
     }
   }

   String ndividend4 = "call DELETE_BORIGHT_T()";
   boolean b5 = cm.procedureExecute(ndividend4);
  }

  String query4 = "SELECT * FROM RIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND RECONCILIATED = 'F'";
  int alldone = cm.queryExecuteCount(query4);

  if (alldone == 0)
  {
    query4 = "SELECT * FROM RIGHTDECLARATION_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY')";
    cm.queryExecute(query4);

    String rightfolio = "";
    int tshares = 0;
    String Sproposedunits = "";
    int proposedunits = 0;
    int eshares = 0;
    boolean b3;

     while(cm.toNext())
     {
       sharessold = cm.getColumnI("SHARES_SOLD");
       tshares = cm.getColumnI("TOTAL_RIGHT_SHARES");
       Sproposedunits = cm.getColumnS("PROPOSED_UNITS");
       rightfolio = cm.getColumnS("RIGHT_FOLIO");
     }
       eshares = tshares - sharessold;

//       sharessold = sharessold + rightshares;

       if (String.valueOf(Sproposedunits).equals("null"))
        Sproposedunits = "";

       if (Sproposedunits.equalsIgnoreCase(""))
       {
         rightcertificateno = cm.LastCertificate();
         distfrom = cm.LastDistinction();
         distto = distfrom + eshares - 1;

         String addcert3 = "call ADD_SHAREHOLDER_BONUS('" + rightfolio + "','Mr.','Company Secretary','Sir','Bangladesh Tobacco Company',0,'D'," + eshares + ",0,'T','F','T','Multinational','0','T','" + DivDate + "')";
         b3 = cm.procedureExecute(addcert3);

         String addcert2 = "call ADD_CERTIFICATE('" + rightcertificateno + "', '" + rightfolio + "','T','F','F','F','F','F','Right Shares')";
         b3 = cm.procedureExecute(addcert2);

         String addcertr2 = "call ADD_CERTIFICATE_RIGHT('" + rightcertificateno + "', '" + rightfolio + "', '" + DivDate + "')";
         b56 = cm.procedureExecute(addcertr2);

         String adddist2 = "call ADD_DISTINCTION(" + distfrom + ", " + distto + ",'" + rightcertificateno + "')";
         b3 = cm.procedureExecute(adddist2);

         String addtransfer11 = "call ADD_TRANSFER('" + rightfolio + "', '" + rightfolio + "', '" + rightcertificateno + "', 'R', '" + rightcertificateno + "','" + distfrom + "','" + distto + "'," + eshares + ")";
         b3 = cm.procedureExecute(addtransfer11);
       }
       else
       {
         String addcert3 = "call ADD_SHAREHOLDER_BONUS('" + rightfolio + "','Mr.','Company Secretary','Sir','Bangladesh Tobacco Company',0,'D'," + eshares + ",0,'T','F','T','Multinational','0','T','" + DivDate + "')";
         b3 = cm.procedureExecute(addcert3);

         proposedunits = Integer.parseInt(Sproposedunits);
         double chunks = eshares/proposedunits;
         distfrom = cm.LastDistinction();
         int thisdistfrom = distfrom;
         int thisdistto = 0;

         for (double i1=chunks;i1>0;i1=i1-1)
         {
          thisdistto = thisdistfrom + proposedunits - 1;
          rightcertificateno = cm.LastCertificate();

          String addcert4 = "call ADD_CERTIFICATE('" + rightcertificateno + "', '" + rightfolio + "','T','F','F','F','F','F','Right Shares')";
          b3 = cm.procedureExecute(addcert4);

          String addcertr3 = "call ADD_CERTIFICATE_RIGHT('" + rightcertificateno + "', '" + rightfolio + "', '" + DivDate + "')";
          b56 = cm.procedureExecute(addcertr3);

          String adddist4 = "call ADD_DISTINCTION(" + thisdistfrom + ", " + thisdistto + ",'" + rightcertificateno + "')";
          b3 = cm.procedureExecute(adddist4);

          int thischnshare = thisdistto - thisdistfrom + 1;

          String addtransfer11 = "call ADD_TRANSFER('" + rightfolio + "', '" + rightfolio + "', '" + rightcertificateno + "', 'R', '" + rightcertificateno + "','" + thisdistfrom + "','" + thisdistto + "'," + thischnshare + ")";
          b3 = cm.procedureExecute(addtransfer11);

          thisdistfrom = thisdistfrom + proposedunits;
         }
       }
  }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Reconciled Right Share Information')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
     cm1.takeDown();
     cm2.takeDown();
   }
  %>
     <script language="javascript">
       alert("All Right Shares for <%=DivDate%> Reconciled!");
       location = "<%=request.getContextPath()%>/jsp/Right/SetRightPostlist.jsp";
    </script>
</body>
</html>
