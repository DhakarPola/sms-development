<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : March 2006
'Page Purpose  : Marks the selected Warrant Numbers.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Mark Selected Warrants</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String rowcounter = String.valueOf(request.getParameter("rowcounter1"));
  String Destination = String.valueOf(request.getParameter("pstatus"));
  String DivDate = String.valueOf(request.getParameter("ddate"));
  String DivType = String.valueOf(request.getParameter("dtype"));
  String Warrantno = String.valueOf(request.getParameter("searchWarrant"));
  String Chunk = String.valueOf(request.getParameter("pchunk"));
  String ForW = String.valueOf(request.getParameter("forw"));

  int iChunk = Integer.parseInt(Chunk);

  Warrantno=cm.replace(Warrantno,"'","''");
  if (String.valueOf(Warrantno).equals("null"))
  {
    Warrantno = "";
  }

  if (!Warrantno.equalsIgnoreCase(""))
  {
      String ifwarexists = "";
      String countfolios = "";
      int cWAR = 0;

      if (DivType.equalsIgnoreCase("SMS"))
      {
       if (ForW.equalsIgnoreCase("folio"))
       {
        ifwarexists = "SELECT * FROM RIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND RECONCILIATED = 'T' AND FRACTION_AMOUNT > 0 AND FOLIO_NO='"+ Warrantno+"'";
       }
       else if (ForW.equalsIgnoreCase("warrant"))
       {
        ifwarexists = "SELECT * FROM RIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND RECONCILIATED = 'T' AND FRACTION_AMOUNT > 0 AND WARRANT_NO='"+ Warrantno+"'";
       }

        countfolios = "SELECT * FROM RIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND RECONCILIATED = 'T' AND FRACTION_AMOUNT > 0";
      }
      else if (DivType.equalsIgnoreCase("CDBL"))
      {
       if (ForW.equalsIgnoreCase("folio"))
       {
        ifwarexists = "SELECT * FROM BORIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND RECONCILIATED = 'T' AND FRACTION_AMOUNT > 0 AND BO_ID='"+ Warrantno+"'";
       }
       else if (ForW.equalsIgnoreCase("warrant"))
       {
        ifwarexists = "SELECT * FROM BORIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND RECONCILIATED = 'T' AND FRACTION_AMOUNT > 0 AND WARRANT_NO='"+ Warrantno+"'";
       }

        countfolios = "SELECT * FROM BORIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND RECONCILIATED = 'T' AND FRACTION_AMOUNT > 0";
      }

    cWAR = cm.queryExecuteCount(ifwarexists);

    if (cWAR > 0)
    {
      Destination = request.getContextPath() + "/jsp/Right/AllPaymentReconciliation.jsp?dateselect=";
      Destination = Destination + DivDate + "&orderby=" + DivType + "&StartValue=";

      int tcount = cm.queryExecuteCount(countfolios);

      String getWarrant = "";
      if (DivType.equalsIgnoreCase("SMS"))
      {
       if (ForW.equalsIgnoreCase("folio"))
       {
        getWarrant = "SELECT rnum FROM (SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM RIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND RECONCILIATED = 'T' AND FRACTION_AMOUNT > 0 ORDER BY SUBSTR(FOLIO_NO,3,6)) div where rownum <= "+tcount+") where rnum >= 1) WHERE FOLIO_NO = '"+Warrantno+"'";
       }
       else if (ForW.equalsIgnoreCase("warrant"))
       {
        getWarrant = "SELECT rnum FROM (SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM RIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND RECONCILIATED = 'T' AND FRACTION_AMOUNT > 0 ORDER BY SUBSTR(FOLIO_NO,3,6)) div where rownum <= "+tcount+") where rnum >= 1) WHERE WARRANT_NO = '"+Warrantno+"'";
       }
      }
      else if (DivType.equalsIgnoreCase("CDBL"))
      {
       if (ForW.equalsIgnoreCase("folio"))
       {
        getWarrant = "SELECT rnum FROM (SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM BORIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND RECONCILIATED = 'T' AND FRACTION_AMOUNT > 0 ORDER BY BO_ID) div where rownum <= "+tcount+") where rnum >= 1) WHERE BO_ID = '"+Warrantno+"'";
       }
       else if (ForW.equalsIgnoreCase("warrant"))
       {
        getWarrant = "SELECT rnum FROM (SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM BORIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND RECONCILIATED = 'T' AND FRACTION_AMOUNT > 0 ORDER BY BO_ID) div where rownum <= "+tcount+") where rnum >= 1) WHERE WARRANT_NO = '"+Warrantno+"'";
       }
      }

      cm.queryExecute(getWarrant);
      cm.toNext();
      String srr = cm.getColumnS("rnum");
      int rr = Integer.parseInt(srr);
      int err = rr + iChunk;
      Destination = Destination + rr + "&EndValue=" + err;
    }
  }

  String Fldname = "";
  int irowcounter = Integer.parseInt(rowcounter);

  for (int i=1;i<irowcounter+1;i++)
  {
    Fldname = "selectingbox" + String.valueOf(i);
    String BoxValue = String.valueOf(request.getParameter(Fldname));

    BoxValue=cm.replace(BoxValue,"'","''");

    if (String.valueOf(BoxValue).equals("null"))
    {
      BoxValue = "";
    }

    if (!BoxValue.equalsIgnoreCase(""))
    {
      String marksel = "";
      if (DivType.equalsIgnoreCase("SMS"))
      {
       marksel = "call MARK_SELECTED_RIGHT('" + BoxValue + "')";
      }
      else if (DivType.equalsIgnoreCase("CDBL"))
      {
        marksel = "call MARK_SELECTED_BORIGHT('" + BoxValue + "')";
      }

      boolean b = cm.procedureExecute(marksel);
    }
  }

   if (isConnect)
   {
     cm.takeDown();
   }

  if (Destination.equalsIgnoreCase("close"))
  {
   %>
     <script language="javascript">
       location = "<%=request.getContextPath()%>/jsp/Home.jsp";
    </script>
   <%
  }
  else
  {
   %>
     <script language="javascript">
       location = "<%=Destination%>";
    </script>
   <%
  }
%>
</body>
</html>
