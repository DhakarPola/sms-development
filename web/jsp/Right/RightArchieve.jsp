<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : February 2006
'Page Purpose  : Form showing all the Declared Right Shares.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Right Share Archieve</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>

<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String query1 = "SELECT * FROM RIGHTDECLARATION_VIEW ORDER BY DATE_DEC DESC";
    cm.queryExecute(query1);

    int hratio = 0;
    int rratio = 0;
    int totalshares = 0;
    int totalrightshares = 0;
    int totalfractionshares = 0;
    String decdate = "";
    String dest = request.getContextPath() + "/jsp/Right/SeeRight.jsp";
%>
  <span class="style7">
  <form name="FormToSubmit" method="GET" action="RejectDeclaredDividends.jsp">
  <table width="100%" BORDER=1  cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Right Share History</center></td></tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="20%" class="style9"><div align="center">Date Declared</div></td>
    <td width="20%"><div align="center" class="style9">
      <div align="center">Holder : Right Ratio</div>
    </div></td>
    <td width="20%"><div align="center" class="style9">
      <div align="center">Total Shares</div>
    </div></td>
    <td width="20%"><div align="center" class="style9">
      <div align="center">Total Right Shares</div>
    </div></td>
    <td width="20%"><div align="center" class="style9">
      <div align="center">Total Fraction Shares</div>
    </div></td>
  </tr>
  <div align="left">
     <%
    while(cm.toNext())
     {
       hratio = cm.getColumnI("HOLDER_RATIO");
       rratio = cm.getColumnI("RIGHT_RATIO");
       decdate = cm.getColumnDT("DATE_DEC");
       totalrightshares = cm.getColumnI("TOTAL_RIGHT_SHARES");
       totalfractionshares = cm.getColumnI("TOTAL_FRACTION_SHARES");
       totalshares = cm.getColumnI("TOTAL_SHARES");
      %>
    </div>
  <tr bgcolor="#E8F3FD">
           <td class="style13"><div align="left" class="style12">
             <div align="center">
               &nbsp;
               <a HREF="<%=dest%>?declarationdate=<%=decdate%>"><%=decdate%></a>
               &nbsp;
             </div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=hratio%>&nbsp;:&nbsp;<%=rratio%></div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">&nbsp;&nbsp;<%=totalshares%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">&nbsp;&nbsp;<%=totalrightshares%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">&nbsp;&nbsp;<%=totalfractionshares%>&nbsp;</div>
           </div></td>
         </tr>
             <%
     }
     %>
       </form>
     <%
   if (isConnect)
   {
     cm.takeDown();
   }
  %>
</body>
</html>
