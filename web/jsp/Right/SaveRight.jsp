<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : February 2006
'Page Purpose  : Saves Right Share Declaration.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@ page language="java" import="java.sql.*,java.util.Vector,java.util.Date,java.text.SimpleDateFormat,java.math.*" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="cm2"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Right</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   boolean isConnect2 = cm2.connect();
   if((isConnect==false) || (isConnect1==false) || (isConnect2==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

     Date d = new Date();
     int thisyear = d.getYear();
     thisyear = thisyear - 100;
     String Sthisyear = String.valueOf(thisyear);

     if (Sthisyear.length() < 3)
     {
      int lendiff = 3 - Sthisyear.length();
      for (int ii=0;ii<lendiff;ii++)
      {
       Sthisyear = "0" + Sthisyear;
      }
     }

     String formdate = "RIGHT" + Sthisyear;

   String DateDec = "";
   String recdate = "";
   int hratio = 0;
   int rratio = 0;
   int totalrightshares = 0;
   int totalfractionshares = 0;
   int totalshares = 0;
   double price = 0;
   double premium = 0;
   double tax = 0;
   double discount = 0;
   String decby = "";
   String Sproposedunit = "";
   boolean b;
   int bankid = 0;
   String accountnumber = "";

   String ndividend2B = "call DELETE_LAST_BORIGHT()";
   b = cm.procedureExecute(ndividend2B);

   String ndividend2C = "call DELETE_LAST_RIGHT()";
   b = cm.procedureExecute(ndividend2C);

   String query1 = "SELECT * FROM RIGHTDECLARATION_T_VIEW";
   int nofright = cm1.queryExecuteCount(query1);
   if (nofright == 0)
   {
     %>
       <script language="javascript">
        alert("Declare a Right Share First!");
        history.go(-1);
       </script>
     <%
   }

   cm.queryExecute(query1);

   while(cm.toNext())
   {
     DateDec = cm.getColumnDT("DATE_DEC");
     recdate = cm.getColumnDT("REC_DATE");
     hratio = cm.getColumnI("HOLDER_RATIO");
     rratio = cm.getColumnI("RIGHT_RATIO");
     totalrightshares = cm.getColumnI("TOTAL_RIGHT_SHARES");
     totalfractionshares = cm.getColumnI("TOTAL_FRACTION_SHARES");
     totalshares = cm.getColumnI("TOTAL_SHARES");
     price = cm.getColumnF("PRICE");
     premium = cm.getColumnF("PREMIUM");
     decby = cm.getColumnS("DEC_BY");
     discount = cm.getColumnF("DISCOUNT");
     tax = cm.getColumnF("TAX");
     Sproposedunit = cm.getColumnS("PROPOSED_UNITS");
     bankid = cm.getColumnI("BANK_ID");
     accountnumber = cm.getColumnS("ACCOUNT_NO");

     String ndividend1 = "call ADD_RIGHTDECLARATION('" + DateDec + "','" + recdate + "'," + hratio + "," + rratio + "," + totalshares + "," + totalrightshares + "," + totalfractionshares + "," + price + "," + premium + "," + discount + ",0,'" + formdate + "','" + decby + "','" + tax + "',null," + bankid +",'" + accountnumber + "'," + Sproposedunit + ")";
     b = cm1.procedureExecute(ndividend1);
   }

   String ndividend2 = "call DELETE_RIGHTDECLARATION_T()";
   b = cm.procedureExecute(ndividend2);

   String query2mx = "SELECT MAX(ALLOTMENT_NO) AS MAXALL FROM RIGHT_VIEW";
   cm.queryExecute(query2mx);
   cm.toNext();
   String getmax = cm.getColumnS("MAXALL");

   int maxaltno = 0;

   if (String.valueOf(getmax).equals("null"))
    getmax = "";
   if (!getmax.equalsIgnoreCase(""))
   {
     maxaltno = Integer.parseInt(getmax);
   }

   String query2 = "SELECT * FROM RIGHT_T_VIEW";
   cm.queryExecute(query2);

   String foliono = "";
   String boid = "";
   String decdate = "";
   int totalshare = 0;
   int rightshare = 0;
   double fractionshare = 0;
   double fractionamount = 0;
   double persharevalue = price + premium - discount + tax;

   while(cm.toNext())
   {
     foliono = cm.getColumnS("FOLIO_NO");
     decdate = cm.getColumnDT("DATE_DEC");
     totalshare = cm.getColumnI("TOTAL_SHARE");
     rightshare = cm.getColumnI("RIGHT_SHARE");
     fractionshare = cm.getColumnF("FRACTION_SHARE");

     fractionamount = fractionshare * persharevalue;
     fractionamount = bcal.roundtovalue(fractionamount,4);

     maxaltno++;

     String sdividend3 = "call ADD_RIGHT('" + foliono + "', '" + decdate + "', " + totalshare + ", " + rightshare + ", " + fractionshare + ",0,'F','',null,null,0,'','F','F','F','F'," + maxaltno + ")";
     b = cm1.procedureExecute(sdividend3);

     String sdividend3a = "call ADD_LAST_RIGHT('" + foliono + "', '" + decdate + "', " + totalshare + ", " + rightshare + ", " + fractionshare + ",0,'F','',null,null,0,'','F','F','F','F'," + maxaltno + ")";
     b = cm1.procedureExecute(sdividend3a);

   }

//   String addcert1 = "call ADD_SHAREHOLDER_BONUS('" + formdate + "','Mr.','Company Secretary','Sir','Bangladesh Tobacco Company',0,'D'," + totalfractionshares + ",0,'T','F','T','Multinational','0','T','" + DateDec + "')";
//   b = cm1.procedureExecute(addcert1);

   maxaltno++;

   String comsec1 = "call ADD_RIGHT_FRACTION('" + formdate + "', '" + DateDec + "', 0, " + totalfractionshares + ", 0,0,'F','',null,null,0,'','F','F','F','F'," + maxaltno + ")";
   b = cm1.procedureExecute(comsec1);

   String thisrightcertificate = cm1.LastCertificate();
   String addcert1 = "call ADD_CERTIFICATE('" + thisrightcertificate + "', '" + formdate + "','F','F','F','F','T','F','')";
   b = cm1.procedureExecute(addcert1);

   String ndividend4 = "call DELETE_RIGHT_T()";
   b = cm.procedureExecute(ndividend4);

   String query3 = "SELECT * FROM BORIGHT_T_VIEW";
   cm.queryExecute(query3);

   while(cm.toNext())
   {
     boid = cm.getColumnS("BO_ID");
     decdate = cm.getColumnDT("DATE_DEC");
     totalshare = cm.getColumnI("TOTAL_SHARE");
     rightshare = cm.getColumnI("RIGHT_SHARE");
     fractionshare = cm.getColumnF("FRACTION_SHARE");

     fractionamount = fractionshare * persharevalue;
     fractionamount = bcal.roundtovalue(fractionamount,4);

     maxaltno++;

     String sdividend4 = "call ADD_BORIGHT('" + boid + "', '" + decdate + "', " + totalshare + ", " + rightshare + ", " + fractionshare + ",0,'F',0,'','F','F','F','F'," + maxaltno + ")";
     b = cm1.procedureExecute(sdividend4);

     String sdividend4a = "call ADD_LAST_BORIGHT('" + boid + "', '" + decdate + "', " + totalshare + ", " + rightshare + ", " + fractionshare + ",0,'F',0,'','F','F','F','F'," + maxaltno + ")";
     b = cm1.procedureExecute(sdividend4a);
   }

   String ndividend5 = "call DELETE_BORIGHT_T()";
   b = cm.procedureExecute(ndividend5);

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Accepted Declared Right Share')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
     cm1.takeDown();
     cm2.takeDown();
   }
%>
<script language="javascript">
  alert("Right Share Accepted!");
  location = "<%=request.getContextPath()%>/jsp/Right/RightAllotmentList.jsp";
</script>
</body>
</html>
