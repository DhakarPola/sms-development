<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : January 2006
'Page Purpose  : Form for the Administrator to Accept Right Shares.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="ut"  class="batbsms.Utility"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Accept Right Shares</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function toacceptedright()
{
  if (confirm("Do you want to Accept the Right Share Declaration?\nThis may take few minutes!!!"))
  {
    location = "<%=request.getContextPath()%>/jsp/Right/SaveRight.jsp"
  }
}

 function askreject()
 {
  if (confirm("Do you want to Reject the Right Share Declaration?"))
  {
    SubmitThis();
  }
 }

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String query1 = "SELECT * FROM RIGHTDECLARATION_T_VIEW";
    cm.queryExecute(query1);

    int holderratio = 0;
    int rightratio = 0;
    String decdate = "";
    double rprice = 0;
    double rpremium = 0;
    int rightshares = 0;
    int fractionshares = 0;
    int totalshares = 0;
    String dest = request.getContextPath() + "/jsp/Right/EditTempRight.jsp";
%>
  <span class="style7">
  <form name="FormToSubmit" method="GET" action="RejectDeclaredRight.jsp">
  <img name="B4" src="<%=request.getContextPath()%>/images/btnAccept.gif" onclick="toacceptedright()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAcceptR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAccept.gif'">
  <img name="B6" src="<%=request.getContextPath()%>/images/btnReject.gif" onclick="askreject()" onMouseOver="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnRejectR.gif'" onMouseOut="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnReject.gif'">
  <table width="100%" BORDER=1  cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Right Share Declaration Under Process</center></td></tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="20%" class="style9"><div align="center">Date Declared</div></td>
    <td width="20%"><div align="center" class="style9">
      <div align="center">Holder : Right Ratio</div>
    </div></td>
    <td width="20%"><div align="center" class="style9">
      <div align="center">Total Shares</div>
    </div></td>
    <td width="20%"><div align="center" class="style9">
      <div align="center">Total Right Shares</div>
    </div></td>
    <td width="20%"><div align="center" class="style9">
      <div align="center">Total Fraction Shares</div>
    </div></td>
  </tr>
  <div align="left">
     <%
    while(cm.toNext())
     {
       holderratio = cm.getColumnI("HOLDER_RATIO");
       rightratio = cm.getColumnI("RIGHT_RATIO");
       decdate = cm.getColumnDT("DATE_DEC");
       rightshares = cm.getColumnI("TOTAL_RIGHT_SHARES");
       fractionshares = cm.getColumnI("TOTAL_FRACTION_SHARES");
       totalshares = cm.getColumnI("TOTAL_SHARES");

       rprice = bcal.roundtovalue(rprice,2);
       rpremium = bcal.roundtovalue(rpremium,2);
       %>
    </div>
  <tr bgcolor="#E8F3FD">
           <td class="style13"><div align="left" class="style12">
             <div align="center">
               &nbsp;
               <a HREF="<%=dest%>?declarationdate=<%=decdate%>"><%=decdate%></a>
               &nbsp;
             </div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=holderratio%>&nbsp;:&nbsp;<%=rightratio%></div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">&nbsp;&nbsp;<%=totalshares%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">&nbsp;&nbsp;<%=rightshares%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">&nbsp;&nbsp;<%=fractionshares%>&nbsp;</div>
           </div></td>
         </tr>
             <%
         }
     %>
       </form>
     <%
   if (isConnect)
   {
     cm.takeDown();
   }
  %>
</body>
</html>
