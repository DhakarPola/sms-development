<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : May 2007
'Page Purpose  : Form for the Administrator to Accept Remats.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Accept Remats</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

function toacceptedremats()
{
  if (confirm("Do you want to Accept the Remat Requests?"))
  {
    location = "<%=request.getContextPath()%>/jsp/Remat/SaveAcceptedRemats.jsp"
  }
}

 function askreject()
 {
  if (confirm("Do you want to Reject the Selected Remats?"))
  {
    SubmitThis();
  }
 }

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String query1 = "SELECT * FROM REMAT_TEMP_VIEW";
    cm.queryExecute(query1);

    String foliono = "";
    String boid = "";
    String sname = "";
    int nofshares = 0;
    String certificateno = "";
    String dnrstart = "";
    String dnrend = "";

    String dest = "";

    int rowcounter = 0;
%>
  <span class="style7">
  <form name="FormToSubmit" method="GET" action="RejectPendingRemats.jsp">
  <SPAN id="dprint">
    <img name="B4" src="<%=request.getContextPath()%>/images/btnAcceptAll.gif" onclick="toacceptedremats()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAcceptAllR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAcceptAll.gif'">
    <img name="B6" src="<%=request.getContextPath()%>/images/btnReject.gif" onclick="askreject()" onMouseOver="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnRejectR.gif'" onMouseOut="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnReject.gif'">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
  </SPAN>
  <table width="100%" BORDER=1  cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Remat Requests</center></td></tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="12%" class="style9"><div align="center">Folio No.</div></td>
    <td width="18%"><div align="center" class="style9">
      <div align="center">BO ID</div>
    </div></td>
    <td width="25%"><div align="center" class="style9">
      <div align="center">Name</div>
    </div></td>
    <td width="12%"><div align="center" class="style9">
      <div align="center">Certificate No.</div>
    </div></td>
    <td width="11%"><div align="center" class="style9">
      <div align="center">DNR Start</div>
    </div></td>
    <td width="11%"><div align="center" class="style9">
      <div align="center">DNR End</div>
    </div></td>
    <td width="11%"><div align="center" class="style9">
      <div align="center">Shares</div>
    </div></td>
  </tr>
  <div align="left">
     <%
    while(cm.toNext())
     {
       rowcounter++;
       foliono = cm.getColumnS("FOLIO_NO");
       boid = cm.getColumnS("BO_ID");
       sname = cm.getColumnS("NAME");
       nofshares = cm.getColumnI("REMAT_QTY");
       certificateno = cm.getColumnS("CERTIFICATE_NO");
       dnrstart = cm.getColumnS("DNR_START");
       dnrend = cm.getColumnS("DNR_END");

       if (String.valueOf(sname).equals("null"))
         sname = "";

       dest = request.getContextPath() + "/jsp/Remat/EditRemat.jsp";

      %>
    </div>
  <tr bgcolor="#E8F3FD">
           <td valign="middle"><div align="center" class="style10">
             <div align="left" class="style10">
               <span class="style13">
               <input type="checkbox" name="rejectbox<%=rowcounter%>" value="<%=certificateno%>">
               <a HREF="<%=dest%>?rid1=<%=certificateno%>"><%=foliono%></a>
               </span></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center" class="style10">
               <span class="style13">
               <a HREF="<%=dest%>?rid1=<%=certificateno%>"><%=boid%></a>
               </span></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="left" class="style10">
               <span class="style13">
               <a HREF="<%=dest%>?rid1=<%=certificateno%>"><%=sname%></a>
               </span></div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center" class="style10">
               <span class="style13">
               <a HREF="<%=dest%>?rid1=<%=certificateno%>"><%=certificateno%></a>
               </span></div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">&nbsp;&nbsp;<%=dnrstart%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=dnrend%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=nofshares%>&nbsp;</div>
           </div></td>
         </tr>
             <%
     }
     %>
       </form>
     <%
   if (isConnect)
   {
     cm.takeDown();
   }
  %>
</body>
</html>
