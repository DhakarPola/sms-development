<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Updated By    : Renad Hakim
'Creation Date : May 2007
'Page Purpose  : Prints Remat certificate
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<%@ page language="java" import="java.sql.*" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head >
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.imagehand{
  cursor:hand;
  color:#FFFFFF;
 }

-->
</style>
<SCRIPT LANGUAGE="JavaScript" SRC="/batbsms/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="/batbsms/js/common.css" TYPE="text/css"></LINK>

<script type="text/javascript">
<!--
function showSelected()
{
    document.all.spanSelected.style.display = '';
    document.all.spanRange.style.display = 'none';
}
function showRange()
{
    document.all.spanSelected.style.display = 'none';
    document.all.spanRange.style.display = '';


}
function showNonPrinted()
{
    document.all.spanSelected.style.display = 'none';
    document.all.spanRange.style.display = 'none';
}
function radio_button_checker()
{
    var radio_choice = false;

    for (counter = 0; counter < document.forms[0].rdoPrintStyle.length; counter++)
    {
      if (document.forms[0].rdoPrintStyle[counter].checked)
        return document.forms[0].rdoPrintStyle[counter].value
    }

}

function SubmitThis()
{
     var radio_value=radio_button_checker();

     if(radio_value != null)
     {
       count = 0;
       if(radio_value =="on")
       {
           BlankValidate('txtCertificate','- Certificate Number (Option must be entered)');
       }
       else
       if(radio_value == "range")
       {
         BlankValidate('txtCertificateFrom','- Certificate Number From (Option must be entered)');
         BlankValidate('txtCertificateTo','- Certificate Number To(Option must be entered)');
       }

       if (count == 0)
       {
         document.forms[0].submit();
       }
       else
       {
         ShowAllAlertMsg();
         return false;
       }
     }
 }
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
.style10{color: #0A2769; font-weight: bold; font-size:13px;}
-->
</style>
</head>
<body >
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
%>
<form action="BarCode.jsp"  method="get" >
<table width="100%" border="1" cellspacing="0" cellpadding="0" style="border-collapse:collapse" bordercolor="#0044B0" bgcolor="#E8F3FD">
  <tr>
    <td>
	<table width="100%" border="0" cellspacing="0" cellpadding="5" >
      <tr>
        <td bgcolor="#0044B0" class="style7" height="30">Remat Certificate Printing </td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="15">
              <tr>
                <td width="30%" class="style9"><input name="rdoPrintStyle" type="radio" checked="checked"  onClick="showSelected()">
                  Selected Certificate<br>
				  <input name="rdoPrintStyle" type="radio" onClick="showRange()" value="range">
                  Range<br>
				  </td>
                <td width="70%" class="style9">
					<p>
					<span id="spanSelected" >
					Certificate No&nbsp;:&nbsp;<input name="txtCertificate" type="text" size="9" maxlength="9" class="SL67TextField"><br>
					</span>
					</p>
					<p>
					<span id="spanRange" style="display:none">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td width="22%" class="style9">Certificate No. From</td>
								<td width="3%" class="style9">:</td>
								<td width="75%"><input name="txtCertificateFrom" type="text" size="9" maxlength="9" class="SL67TextField"></td>
							  </tr>
							  <tr>
								<td class="style9">Certificate No. To</td>
								<td class="style9">:</td>
								<td><input name="txtCertificateTo" type="text" size="9" maxlength="9" class="SL67TextField"></td>
							  </tr>
							</table>
					</span>
					</p>
				</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td class="style10">&nbsp;&nbsp;&nbsp;&nbsp;Signature on the Certificate:</td>
          </tr>
          <tr>
            <td>
			<table width="100%" border="0" cellspacing="0" cellpadding="15" style="border-collapse:collapse" bordercolor="#0044B0" >
			<tr><td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" >
              <tr>
                <td class="style9">DIRECTOR</td>
                <td class="style9">SECRETARY</td>
              </tr>
              <tr>
                  <td><select name="lstDirector" class="SL1TextFieldListBox">
                    <%

                     String directoryQuery="SELECT NAME FROM SIGNATURE WHERE RANK LIKE '%Director%'";
                     try
                     {
                       cm.queryExecute(directoryQuery);
                       while(cm.toNext())
                       {
                         %>
                         <option value="<%= cm.getColumnS("NAME") %>" ><%= cm.getColumnS("NAME") %></option>
                         <%
                         }
                       }
                       catch(Exception e){}
                    %>
                  </select></td>
                  <td><select name="lstSecretary" class="SL1TextFieldListBox">
                    <%

                     String secretaryQuery="SELECT NAME FROM SIGNATURE WHERE RANK LIKE '%Secretary%'";
                     try
                     {
                       cm.queryExecute(secretaryQuery);
                       while(cm.toNext())
                       {
                         %>
                         <option value="<%= cm.getColumnS("NAME") %>" ><%= cm.getColumnS("NAME") %></option>
                         <%
                         }
                       }catch(Exception e){}
                    %>
                  </select></td>
              </tr>
            </table>

            </td></tr>
            </table>
            </td>
          </tr>
          <tr>
            <td>
      <table width="100%" BORDER=0  cellpadding="10" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <td width="50%" align="right">
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
      </td>
        <td width="50%">
	          <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
      </td>
      </tr>
      </table>
            </td>
          </tr>
        </table></td>
      </tr>
    </table>
  </tr>
</table>

</form>
<%
  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Printed Remat Certificate')";
  try
  {
    boolean ub = cm.procedureExecute(ulog);
  }
  catch(Exception e){}

if (isConnect)
   {
     try
     {
       cm.takeDown();
     }catch(Exception e){}
   }
%>
</body>
</html>
