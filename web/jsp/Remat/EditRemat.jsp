<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : May 2007
'Page Purpose  : Edits Remat Info.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit Remat Info.</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('folio_no','- Folio No. (Option must be entered)');
  BlankValidate('bo_id','- BO ID (Option must be entered)');
  BlankValidate('dnr_start','- DNR Start (Option must be entered)');
  BlankValidate('dnr_end','- DNR End (Option must be entered)');
  BlankValidate('remat_qty','- Remat Quantity (Option must be entered)');

  if (count == 0)
  {
    document.forms[0].submit();
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

    String FolioNo = "";
    String BOID = "";
    String DNRStart = "";
    String DNREnd = "";
    int RQuantity = 0;
    String RRN1 = "";
    String TraceNo = "";
    String RTA_Date = "";
    String RRF1 = "";
    String RStatus = "";
    String BalanceType = "";

    String certificateno = String.valueOf(request.getParameter("rid1"));

    String query1 = "SELECT * FROM REMAT_TEMP_VIEW WHERE LOWER(CERTIFICATE_NO) = LOWER('" + certificateno + "')";
    cm.queryExecute(query1);

    while(cm.toNext())
     {
       FolioNo = cm.getColumnS("FOLIO_NO");
       BOID = cm.getColumnS("BO_ID");
       RQuantity = cm.getColumnI("REMAT_QTY");
       DNRStart = cm.getColumnS("DNR_START");
       DNREnd = cm.getColumnS("DNR_END");
       RRN1 = cm.getColumnS("RRN");
       TraceNo = cm.getColumnS("TRACE_NO");
       RTA_Date = cm.getColumnDT("RTA_CON_DATE");
       RRF1 = cm.getColumnS("RRF");
       RStatus = cm.getColumnS("REMAT_STATUS");
       BalanceType = cm.getColumnS("BALANCE_TYPE");

       if (String.valueOf(RRN1).equals("null"))
         RRN1 = "";
       if (String.valueOf(TraceNo).equals("null"))
         TraceNo = "";
       if (String.valueOf(RTA_Date).equals("null"))
         RTA_Date = "";
       if (String.valueOf(RRF1).equals("null"))
         RRF1 = "";
       if (String.valueOf(RStatus).equals("null"))
         RStatus = "";
       if (String.valueOf(BalanceType).equals("null"))
         BalanceType = "";
     }

%>

<form action="UpdateTempRemat.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Edit Remat Info.</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Folio No.</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8">
          <input type="text" name="folio_no" class="SL2TextField" value="<%=FolioNo%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;BO ID</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8">
          <input type="text" name="bo_id" class="SL2TextField" value="<%=BOID%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Certificate No.</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8">
          <input type="text" name="certno" class="SL2TextField" value="<%=certificateno%>" onfocus="this.blur()">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;DNR Start</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8">
          <input type="text" name="dnr_start" class="SL2TextField" value="<%=DNRStart%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;DNR End</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8">
          <input type="text" name="dnr_end" class="SL2TextField" value="<%=DNREnd%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Remat Quantity</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8">
          <input type="text" name="remat_qty" class="SL2TextField" value="<%=RQuantity%>" onkeypress="keypressOnNumberFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;RRN</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8">
          <input type="text" name="rr_n" class="SL2TextField" value="<%=RRN1%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Trace No.</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8">
          <input type="text" name="trace_no" class="SL2TextField" value="<%=TraceNo%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;RTA Confirmation Date</div></th>
        <td>:</td>
        <td><div align="left">
         <input name="rtadate" type=text id="rtadate" maxlength="10" class="SL2TextField" value="<%=RTA_Date%>" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('rtadate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;RRF</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8">
          <input type="text" name="rr_f" class="SL2TextField" value="<%=RRF1%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Status</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8">
          <input type="text" name="stat" class="SL2TextField" value="<%=RStatus%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Balance Type</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8">
          <input type="text" name="bal_type" class="SL2TextField" value="<%=BalanceType%>">
        </div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="10" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <td width="50%" align="right">
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
      </td>
        <td width="50%">
	          <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
      </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
