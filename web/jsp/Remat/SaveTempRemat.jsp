<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : May 2007
'Page Purpose  : Save Remat Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Temp Remat</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String FolioNo = String.valueOf(request.getParameter("folio_no"));
  String BOID = String.valueOf(request.getParameter("bo_id"));
  String DNRStart = String.valueOf(request.getParameter("dnr_start"));
  String DNREnd = String.valueOf(request.getParameter("dnr_end"));
  int RQuantity = Integer.parseInt(request.getParameter("remat_qty"));
  String RRN = String.valueOf(request.getParameter("rr_n"));
  String TraceNo = String.valueOf(request.getParameter("trace_no"));
  String RTA_Date = String.valueOf(request.getParameter("rtadate"));
  String RRF = String.valueOf(request.getParameter("rr_f"));
  String RStatus = String.valueOf(request.getParameter("stat"));
  String BalanceType = String.valueOf(request.getParameter("bal_type"));

  String CertificateNo = "";

  DNRStart = cm.replace(DNRStart,"'","''");
  DNREnd = cm.replace(DNREnd,"'","''");
  RRN = cm.replace(RRN,"'","''");
  TraceNo = cm.replace(TraceNo,"'","''");
  RRF = cm.replace(RRF,"'","''");
  RStatus = cm.replace(RStatus,"'","''");
  BalanceType = cm.replace(BalanceType,"'","''");

  String query10 = "SELECT * FROM SHAREHOLDER_VIEW WHERE LOWER(FOLIO_NO) = LOWER('" + FolioNo + "') AND ACTIVE = 'T'";
  int folioexists = cm.queryExecuteCount(query10);

  if (folioexists < 1)
   {
     %>
       <script language="javascript">
        alert("Folio does not exist.\nPlease Check!");
        history.go(-1);
       </script>
     <%
   }

  CertificateNo = cm.LastCertificate();

  String nremat = "call ADD_REMAT_TEMP('" + RRN + "', '" + TraceNo + "', '" + RTA_Date + "', '" + FolioNo + "', '" + BOID + "', '" + CertificateNo + "','" + RRF + "','" + RStatus + "','" + BalanceType + "','" + DNRStart + "','" + DNREnd + "'," + RQuantity + ")";
  boolean b = cm.procedureExecute(nremat);

  nremat = "call ADD_CERTIFICATE('" + CertificateNo + "', '" + FolioNo + "','T','F','F','F','F','F','Remat Shares')";
  b = cm.procedureExecute(nremat);

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Entered Remat Request')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  alert("The Remat Request has been Entered");
  location = "<%=request.getContextPath()%>/jsp/Remat/AcceptRemats.jsp";
</script>
</body>
</html>
