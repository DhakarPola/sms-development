<script>
/*
'******************************************************************************************************************************************
'Script Author : Mohammad Ashraful Islam
'Updated By    : Renad Hakim
'Creation Date : November 2006
'Page Purpose  : Save new AGM Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>

<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save AGM</title>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();

   if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
     String boid="";
     String Srname="";
     String Shname="";
     String Address="";
     String agm ="";
     int c=0;

     String agmdate = String.valueOf(request.getParameter("AgmDate"));

     String q1 = "SELECT MAX(AGMDATE) total FROM AGMATTENDANCE_VIEW HAVING MAX(AGMDATE) < TO_DATE('" +agmdate+ "','DD/MM/YYYY')";
     c=cm.queryExecuteCount(q1);

     if (c==0)
     {
       %>
       <script language="javascript">
       alert("AGM entry already exists on or after this date.\nTry a later date.");
       location = "<%=request.getContextPath()%>/jsp/AGM/NewAgm.jsp";
       </script>
       <%
     }

     else
    {
     String query1 = "SELECT * FROM NEWAGMLIST order by BOID";

     /*query1 = "(SELECT distinct BH.BOID,BA.NAME_FIRSTHOLDER, BA.NAME_FIRSTHOLDER SURNAME, BA.ADDRESS FROM BOHOLDING BH,BOADDRESS BA ";
     query1 =query1 + "WHERE BH.BOID=BA.BO_ID UNION SELECT distinct FOLIO_NO BOID, (NAME) NAME_FIRSTHOLDER, SURNAME, ";
     query1 =query1 + "(nvl(ADDRESS1,'')||' '||nvl(ADDRESS2,' ')||' '||nvl(ADDRESS3,' ')||' '||nvl(ADDRESS4,' ')) ADDRESS FROM ";
     query1 =query1 + "SHAREHOLDER WHERE active='T' and TOTAL_SHARE>0 and FOLIO_NO not in (select FOLIO_NUMB from (SELECT distinct sum(ACCEPT_QTY) TotalS,FOLIO_NUMB FROM BODEMATCONFIRMATION group by FOLIO_NUMB order by FOLIO_NUMB) tab3)) order by BOID";
     */

     try{
       cm.queryExecute(query1);

       while(cm.toNext())
       {
         boid = cm.getColumnS("BOID");
         Shname = cm.getColumnS("NAME_FIRSTHOLDER");
         Srname = cm.getColumnS("SURNAME");
         Address = cm.getColumnS("ADDRESS");

         if (String.valueOf(boid).equals("null"))
           boid = "";
         if (String.valueOf(Shname).equals("null"))
           Shname = "";
         if (String.valueOf(Srname).equals("null"))
           Srname = "";
         if (String.valueOf(Address).equals("null"))
           Address = "";

         Shname=cm.replace(Shname,"'","''");
         Srname=cm.replace(Srname,"'","''");
         Address=cm.replace(Address,"'","''");

         agm = "call ADD_AGM('" + boid + "', '" + Srname + "', '" + Shname + "', '" + Address + "', '" + agmdate + "', 'F' )";
         boolean b = cm1.procedureExecute(agm);
        }
     }
     catch(Exception ex){

     }


/*
     String foliono="";
     String SrName="";
     String ShName="";
     String ADDress="";
     String Address1="";
     String Address2="";
     String Address3="";
     String Address4="";

     String query2 = "SELECT * FROM SHAREHOLDER_VIEW WHERE ACTIVE='T'";
     cm.queryExecute(query2);

    while(cm.toNext())
              {

                foliono = cm.getColumnS("FOLIO_NO");
                SrName = cm.getColumnS("SURNAME");
                ShName = cm.getColumnS("NAME");
                Address1 = cm.getColumnS("ADDRESS1");
                Address2 = cm.getColumnS("ADDRESS2");
                Address3 = cm.getColumnS("ADDRESS3");
                Address4 = cm.getColumnS("ADDRESS4");

                 if (String.valueOf(foliono).equals("null"))
                     foliono = "";
                 if (String.valueOf(SrName).equals("null"))
                     SrName = "";
                 if (String.valueOf(ShName).equals("null"))
                     ShName = "";
                 if (String.valueOf(Address1).equals("null"))
                     Address1 = "";
                 if (String.valueOf(Address2).equals("null"))
                     Address2 = "";
                if (String.valueOf(Address3).equals("null"))
                     Address3 = "";
                if (String.valueOf(Address4).equals("null"))
                     Address4 = "";

                SrName=cm.replace(SrName,"'","''");
                ShName=cm.replace(ShName,"'","''");
                Address1=cm.replace(Address1,"'","''");
                Address2=cm.replace(Address2,"'","''");
                Address3=cm.replace(Address3,"'","''");
                Address4=cm.replace(Address4,"'","''");

                ADDress = Address1 + Address2 + Address3 + Address4;

                String agmfolio = "call ADD_AGM('" + foliono + "', '" + SrName + "', '" + ShName + "', '" + ADDress + "', '" + agmdate + "', 'F' )";
               boolean b = cm1.procedureExecute(agmfolio);

             }*/
           }
    String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added AGM Information')";
    try{
      boolean ub = cm.procedureExecute(ulog);

      if (isConnect)
      {
        cm.takeDown();
        cm1.takeDown();
      }
    }
    catch(Exception ex){
    }

%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/AGM/GenerateList.jsp";
  </script>
</body>
</html>
