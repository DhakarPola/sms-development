<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Rahat Uddin
'Creation Date : August 2007
'Page Purpose  : Show all BO With Folio List i.e. Bo having Folio.
'******************************************************************************************************************************************
*/
</script>
<html>
<head>
<title>
AGMAttendance
</title>
<script>

</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All AGM Attendance</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<%@page language="java" import="java.io.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*"%>
<script language="javascript">
<!--

function goNextPrevious(pstart,pend)
{
  var oldstrval=document.all.svalue.value;
  var oldendval=document.all.evalue.value;

  if(Number(pstart)>Number(oldstrval)){
    pstart=oldstrval;
    pend=oldendval;
  }
  //alert("oldstrval"+oldstrval+"pstart"+pstart);
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/AGM/MulFolioSingleBoList.jsp?StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;
  location = thisurl;

}

function closedocument()
{
	//history.back();
         location = "<%=request.getContextPath()%>/jsp/Home.jsp";
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}

//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
    .style19 {
	color: #0A2769;
	font-weight: bold;
    }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   int folio_state=0;
   int COUNT;
   int MAX_RECORDS=50;
   int typedate=0;
   String Boid="";
   Boid=Boid.trim();

   String folioname = "";
   String folioaddress = "";
   String BOAddress = "";
   String FolioNo="";
   String PreviousBoID="";
   String PreviousFolioname="";
   String PreviousFolioNo="";
   String TotalFolio="";
   int rowcounter1 = 0;

 //Used for NEXT/PREVIOUS
    String SStartingValue = String.valueOf(request.getParameter("StartValue"));
    String SEndingValue = String.valueOf(request.getParameter("EndValue"));


    if(SStartingValue == null || SStartingValue.equals("") || SStartingValue.equals("null")) SStartingValue="1";
    if(SEndingValue == null || SEndingValue.equals("") || SEndingValue.equals("null")) SEndingValue="0";



    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    int Chunk = MAX_RECORDS;
    EndingValue = StartingValue + Chunk - 1;
    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;

    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }

     Boid=cm.replace(Boid,"'","''");

     if (String.valueOf(Boid).equals("null"))
      Boid = "";

   String query1="";

  query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (select * from MULTIPLEFOLIOSINGLEBO) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;

      cm.queryExecute(query1);
     COUNT=0;
     while(cm.toNext())
     {
       COUNT++;
     }

       cm.queryExecute(query1);
     %>
     <span class="style7">
       <form method="post" action="BoWithFolioList.jsp">
       <SPAN id="dprint">
           <img name="B7" src="<%=request.getContextPath()%>/images/btnClose.gif" onclick="closedocument()" onMouseOver="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
       </SPAN>
       <table width="100%" BORDER=1  cellpadding="0"  style="border-collapse:collapse;" bordercolor="#06689E">
         <!--DWLayoutTable-->
         <tr><td bgcolor="#0044B0" class="style7" height="30"><center>
           Multiple Folio Having Single BO
         </center></td></tr>
         <tr>
           <td>
             <table width="100%"  border="0" cellpadding="0" cellspacing="0">
               <tr bgcolor="#E8F3FD" height="35">
                 <td width="10%"></td>
                 <td width="6%"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goNextPrevious(<%=pStartingValue%>,<%=pEndingValue%>)"></td>
                   <td width="6%" align="right"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNextPrevious(<%=nStartingValue%>,<%=nEndingValue%>)"></td>
                   <td width="78%">&nbsp;</td>
               </tr>
             </table>
            </td>
         </tr>
       </table>
       <table width="100%" border="1" cellpadding="5" cellspacing="0" style="border-collapse: collapse" bordercolor="0044B0">
         <tr bgcolor="#0044B0">
           <td width="15%"><div align="center" class="style9">
             <div align="center">BOID</div></div></td>
           <td width="50%" class="style9" align="center">Folio No</td>
           <td width="35%"><div align="center" class="style9">
             <div align="center">Name</div></div></td>
             <!--td width="25%"><div align="center" class="style9">
               <div align="center">Address</div></div></td-->

         </tr>
         <%
         folio_state=0;
         int CountRow=0;
         while(cm.toNext() && COUNT!=0)
         {
           rowcounter1++;
           folio_state++;
           CountRow++;
           Boid=cm.getColumnS("BOID");
           FolioNo=cm.getColumnS("FOLIO_NUMB");
           folioname = cm.getColumnS("BOSHORTNAME");
          // BOAddress = cm.getColumnS("ADDRESS1");
           Boid=Boid.trim();

           if(String.valueOf(Boid).equals("null"))
           	Boid="";
           if(String.valueOf(folioname).equals("null"))
           	folioname="";
           //if(String.valueOf(BOAddress).equals("null"))
           	//BOAddress="";
            if(String.valueOf(FolioNo).equals("null"))
                FolioNo="";
           Boid=cm.replace(Boid,"'","''");
           folioname=cm.replace(folioname,"'","''");
           //BOAddress=cm.replace(BOAddress,"'","''");
           if (!Boid.equals("null"))
           {
             if (String.valueOf(folioname).equals("null"))
             folioname = "";
             if(CountRow==1){
               TotalFolio=FolioNo;
               PreviousBoID=Boid;
               PreviousFolioname=folioname;
             }
              if(String.valueOf(PreviousBoID).equalsIgnoreCase(Boid)&&CountRow>1){
                TotalFolio=TotalFolio+", "+FolioNo;//<br>
                PreviousFolioname=folioname;
              }
              else if(!String.valueOf(PreviousBoID).equalsIgnoreCase(Boid) && CountRow>1){
                %>
          <tr bgcolor="#E8F3FD">
               <td valign="top"><div align="left" class="style12">
                   <div align="left"><%=PreviousBoID%></div></div></td>
              <td valign="top"><div align="left" class="style12">
                   <div align="left"><%=TotalFolio%>&nbsp;</div></div></td>
              <td valign="top" ><div align="left" class="style12">
                   <div align="left"><%=PreviousFolioname%></div></div></td>
              <!--td valign="top" class="style13"><div align="left" class="style12">
                   <div align="left"><BOAddress%>&nbsp;</div></div></td-->
             </tr>

                <%
                TotalFolio=FolioNo;
                PreviousFolioname=folioname;
                PreviousBoID=Boid;

              }

              if(folio_state==COUNT)
              {
                %>
               <input type="hidden" name="svalue" value="<%=StartingValue%>">
               <input type="hidden" name="evalue" value="<%=EndingValue%>">
               <%}
             }
           }
           %>
           <tr bgcolor="#E8F3FD">
               <td valign="top" ><div align="left" class="style12">
                   <div align="left"><%=PreviousBoID%></div></div></td>
              <td valign="top" ><div align="left" class="style12">
                   <div align="left"><%=TotalFolio%>&nbsp;</div></div></td>
              <td valign="top" ><div align="left" class="style12">
                   <div align="left"><%=folioname%></div></div></td>
              <!--PreviousFolioname td valign="top" class="style13"><div align="left" class="style12">
                   <div align="left"><BOAddress%>&nbsp;</div></div></td-->
             </tr>
             <%
                 if (isConnect)
                 {
                   cm.takeDown();
                 }
                 %>
		  <!--HIDDEN FIELDS-->
            <input type="hidden" name="rowcounter1" value="<%=rowcounter1%>">

  </table>
  </form>
</body>
</html>

