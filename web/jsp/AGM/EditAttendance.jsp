<script>
/*
'******************************************************************************************************************************************
'Script Author : Md.Kamruzzaman
'Creation Date : November 2006
'Page Purpose  : Edits Attendance Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.io.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*"%>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<jsp:useBean id="util" class="batbsms.Utility"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css">
</LINK><%if (String.valueOf(session.getAttribute("UserName")).equals("null")) {%>
<script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%}%>
<html>
<head>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datepicker.js"></SCRIPT>
<script language="javascript">
<!--
function CLClose()
{
  //location = "<//%=request.getContextPath()%>/jsp/AGM/AGMAttendance.jsp";
  history.back();
}
function UpdateTable()
{
  document.forms[0].submit();
}


//-->
</script>
<%
  boolean isConnect = cm.connect();
  if (isConnect == false) {
%>
<jsp:forward page="ErrorMsg.jsp">
  <jsp:param name="ErrorTitle" value="Connection Failure"/>
  <jsp:param name="ErrorHeading" value="Connection Problem"/>
  <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator"/>
</jsp:forward>
<%
  }
  String sboid = request.getParameter("fno");
  String agm_date=request.getParameter("agmdate");

  String boid = "";
  String shareholdername = "";
  String address = "";
  String surname="";

  String VStatus ="";
  String queryFolio;
  String selectQuery="";
   byte[] imgData1=null;
  Connection conn1=null;

 queryFolio = "SELECT * FROM AGMATTENDANCE_VIEW WHERE boid = '" + sboid+"' and AGMDATE=TO_DATE('" + agm_date + "','DD/MM/YYYY')";
 cm.queryExecute(queryFolio);

  while (cm.toNext()) {
    boid  = cm.getColumnS("boid");
    shareholdername = cm.getColumnS("name");
    address = cm.getColumnS("address");
    surname = cm.getColumnS("SURNAME");
	VStatus = cm.getColumnS("IS_PRESENT");

    if (String.valueOf(shareholdername).equals("null"))
          shareholdername = "";
    if (String.valueOf(address).equals("null"))
          address = "";
    if (String.valueOf(surname).equals("null"))
          surname = "";
    if (String.valueOf(address).equals("null"))
          address = "";
	if (String.valueOf(VStatus).equals("null"))
          VStatus = "F";

   //***********************************************
  }

%>
<style type="text/css">
  <!--
    body {
    background-color: #FFFFFF;
    }
    body,td,th {
    color: #000000;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
    }
  -->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit AGM Attendance</title>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css">
</LINK><script language="javascript">
<!--


function closedocument()
{
	location = "<%=request.getContextPath()%>/jsp/AGM/AGMAttendance.jsp";
}


function formconfirm()
{
  if (confirm("Do you want to change the Folio Information?"))
  {
    return true;
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
   if(document.forms[0].foliono.value.length == 8)
  {
    if (count == 0)
    {
      if (formconfirm())
      {
        document.forms[0].submit();
      }
    }
    else{
      ShowAllAlertMsg();
      return false;
    }
  }
  else
  {
    alert("Folio Number should be 8 characters long");
  }
}

//-->
</script>
<style type="text/css">
  <!--
    .style7 {
    color: #FFFFFF;
    font-weight: bold;
    font-size: 13px;
    }
    .style8 {color: #0A2769;
            font-weight: bold;}
    .style9 {color: #0A2769; font-weight: bold; }
  -->
</style>
</head>
<body TEXT="000000" BGCOLOR="FFFFFF">
<form action="UpdateAttendance.jsp?agmdate=<%=agm_date%>" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Signature Verification</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Folio</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=sboid%></div></td>

      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Name</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=shareholdername%></div></td>
      </tr>
	  <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Address</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=address%></div></td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Signature</div></th>
        <td valign="top">:</td>
        <td><div align="left" class="style9">
                 <%
                 if(sboid.length()>10){
                 %>
                 <font color="Blue">
                 <!--<img alt="Signature not available"  title="Share Holder Signature" src="<%//request.getContextPath()%>/Bosignature/<%//sboid%>.jpg"/>-->
                 <img alt="Not available"  title="Share Holder Signature" src="<%=request.getContextPath()%>/Bosignature/<%=sboid%>0101.jpg"/>
                 </font>
                 <%
                  }
                  else{
                  imgData1 =null;
                  selectQuery="SELECT SIGNATURE FROM SHAREHOLDER WHERE folio_no = '"+sboid +"'";
                  try
                  {
                    conn1 = cm.getConnection();
                    conn1.setAutoCommit (false);
                    imgData1 =  cm.getPhoto(  conn1, selectQuery );
                  }
                  catch(Exception ex)
                  {
                    conn1=null;
                  }
                  conn1=null;

                if(imgData1.length!=0)
                {
                  %>
                        <img alt="Share Holder Signature" title="Share Holder Signature" src="image.jsp?selectQuery=<%=selectQuery%>" />
                    <%
                 }
                     else
                     {
                     %>
                     <center>
                     <font color="Blue">
                       <b>Signature not available</b>
                     </font>
                     </center>
                    <%
                       }
                    }
                   %>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Present</div></th>
        <td>:</td>
        <td><div align="left" class="style9">
                <%
              if (VStatus.equals("T"))
              {
              %>
              <input name="AStatus" id="AStatus" type="checkbox" checked="checked" value="T">
              <%
               }
                else
                {
                %>
                <input name="AStatus" id="AStatus" type="checkbox" value="T">
                <%
              }
                %>
        </div></td>
      </tr>
      </table>
      <br>
      </div>

      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="50%" scope="row">
            <div align="right">
	      <img name="B2" src="<%=request.getContextPath()%>/images/btnSubmit.gif" onclick="UpdateTable()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
            </div></th>
        <td width="50%" align="left">
              <img name="B3" src="<%=request.getContextPath()%>/images/btnClose.gif" onclick="CLClose()" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
              <input type="hidden" name="Vboid" value="<%=sboid%>">
              <input type="hidden" name="agmdate" value="<%=agm_date%>">
      </td>
      </tr>
   </table>
</form>
<%
  if (isConnect) {
    cm.takeDown();
  }
%>
</body>
</html>

