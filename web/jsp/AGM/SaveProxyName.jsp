<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : July 2007
'Page Purpose  : Save New Proxy Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>

<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Proxy</title>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();

   if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

     String agmDate=String.valueOf(request.getParameter("dateselect"));
     String folio_no=String.valueOf(request.getParameter("folio")).trim();
     String pname=String.valueOf(request.getParameter("pname")).trim();


     int countFolio=0;

     String checkFolio="select BOID from AGMATTENDANCE_VIEW where BOID='" + folio_no + "'";

     try
     {
       countFolio=cm.queryExecuteCount(checkFolio);
     }catch(Exception e){}

     if (countFolio==0)
     {
       %>
       <script language="javascript">
       alert("Folio/BOID Invalid or Not Invited.");
       </script>
       <%
     }
     else
     {
     String AddProxy="call ADD_PROXY('" + agmDate + "','" + folio_no + "','" + pname + "')";
     try
     {
       boolean b= cm.procedureExecute(AddProxy);
     }catch(Exception e){}
     %>
     <script language="javascript">
     alert("Proxy Information Entry Successfull.");
     </script>
     <%
   }
    String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added New Proxy Information')";
    try{
       boolean ub = cm.procedureExecute(ulog);
       }catch(Exception e){}
      if (isConnect)
      {
        try
        {
        cm.takeDown();
        cm1.takeDown();
         } catch(Exception e){}
    }


%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/AGM/NewProxyName.jsp";
  </script>
</body>
</html>
