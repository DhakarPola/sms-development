<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Updated By    : Renad Hakim
'Creation Date : January 2006
'Page Purpose  : Updates certificate lock information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@ page language="java" import="oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*,org.apache.commons.fileupload.*,java.io.*,java.util.*,java.util.List,java.util.Vector,java.util.Date,java.text.SimpleDateFormat,java.util.Vector,java.math.*" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update Signature</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
     Date d = new Date();
     int thisyear = d.getYear();
     thisyear = thisyear + 1900;
     int thismonth = d.getMonth();
     thismonth = thismonth + 1;
     int thisday = d.getDate();
     int thisday1 = d.getDay();
     String wday = "";
     String thismonth1 = "";
     if (thisday1 == 1)
       wday = "Monday";
     else if (thisday1 == 2)
       wday = "Tuesday";
     else if (thisday1 == 3)
       wday = "Wednesday";
     else if (thisday1 == 4)
       wday = "Thursday";
     else if (thisday1 == 5)
       wday = "Friday";
     else if (thisday1 == 6)
       wday = "Saturday";
     else if (thisday1 == 0)
       wday = "Sunday";

     if (thismonth == 1)
       thismonth1 = "January";
     else if (thismonth == 2)
       thismonth1 = "February";
     else if (thismonth == 3)
       thismonth1 = "March";
     else if (thismonth == 4)
       thismonth1 = "April";
     else if (thismonth == 5)
       thismonth1 = "May";
     else if (thismonth == 6)
       thismonth1 = "June";
     else if (thismonth == 7)
       thismonth1 = "July";
     else if (thismonth == 8)
       thismonth1 = "August";
     else if (thismonth == 9)
       thismonth1 = "September";
     else if (thismonth == 10)
       thismonth1 = "October";
     else if (thismonth == 11)
       thismonth1 = "November";
     else if (thismonth == 12)
       thismonth1 = "December";

     String formdate = wday + ", " + thismonth1 + " " + thisday + ", " + thisyear;

     String lockedby = "Locked By: " + String.valueOf(session.getAttribute("FullName"));
     lockedby = lockedby + "\r\nDate: " + formdate;

  String requestedby = String.valueOf(request.getParameter("requestedby"));
  String certificateno = String.valueOf(request.getParameter("certificateno"));
  String signature = String.valueOf(request.getParameter("signature"));
  String foliono = String.valueOf(request.getParameter("foliono"));
  String signaturestatus = String.valueOf(request.getParameter("signaturestatus"));
  String comments = String.valueOf(request.getParameter("comments"));
  String pagestatus = String.valueOf(request.getParameter("pagestatus"));
  String updateCommentQuery="";
  String checkifCertExistQuery="";
  int COUNT_CERT=0;

  if(signaturestatus==null) signaturestatus="";
  requestedby=cm.replace(requestedby,"'","''");
  certificateno=cm.replace(certificateno,"'","''");
  signature=cm.replace(signature,"'","''");
  foliono=cm.replace(foliono,"'","''");
  signaturestatus=cm.replace(signaturestatus,"'","''");
  comments=cm.replace(comments,"'","''");
  pagestatus= (cm.replace(pagestatus,"'","''")).trim();

  if (String.valueOf(comments).equals("null"))
   comments = "";

  lockedby = lockedby + "\r\nComments: " + comments;

  checkifCertExistQuery = "SELECT * FROM CERTIFICATE WHERE \"LOCK\"='T' AND CERTIFICATE_NO='"+certificateno+"'";
  COUNT_CERT = cm.queryExecuteCount(checkifCertExistQuery);
  cm.queryExecute(checkifCertExistQuery);

  cm.toNext();
  String excomments = cm.getColumnS("COMMENT");

  if (String.valueOf(excomments).equals("null"))
   excomments = "";

  if(COUNT_CERT > 0)
  {
      %>
      <script type="text/javascript">
        alert("Certificate already locked");
        history.go(-1);
      </script>
      <%
  }
  else
  {
    comments = lockedby + "\r\n" + excomments;

    updateCommentQuery="CALL UPDATE_CERTIFICATE_COMMENT('"+certificateno +"','"+comments+"','T')";
    System.out.println(updateCommentQuery);
    boolean uCQR = cm.procedureExecute(updateCommentQuery);
    String uuser="";

    if(pagestatus.equals("new"))
    uuser = "call ADD_CERTIFICATE_LOCK_REQUESTBY('"+certificateno+"','"+requestedby +"',null)";
    else
    if(signaturestatus.equals("add") || signaturestatus.equals("delete"))
    uuser = "call UPDATE_CERTIFICATE_LOCK('" + certificateno + "','" + requestedby + "',null)";
    else
    uuser = "call UPDATE_CERTIFICATE_LOCK_EMPTY('" + certificateno + "','" + requestedby +"')";

    System.out.println(uuser);
    boolean b = cm.procedureExecute(uuser);



    if(signaturestatus.equals("delete") || signaturestatus.equals("edit"))
    {
      ;
    }
    else
    if (signature!=null && !signature.equals(""))//This section inserts signature into database
    {
      //Insert Blob into database
      BLOB blob;
      PreparedStatement pstmt;
      cm.connect();
      Connection connection = cm.getConnection() ;

      String selectQuery="";

      selectQuery="SELECT * FROM CERTIFICATE_LOCK_REQUESTBY where CERTIFICATE_NO = ? for update";

      pstmt = connection.prepareStatement(selectQuery);

      pstmt.setString(1, certificateno);
      connection.setAutoCommit(false);

      ResultSet rset=pstmt.executeQuery();
      rset.next();


      //Use the OracleDriver resultset, we take the blob locator
      blob = ((OracleResultSet) rset).getBLOB("ATTACHMENT");
      OutputStream os = blob.getBinaryOutputStream();

      //Read the file by chuncks and insert them in the Blob. The chunk size come from the blob
      byte[] chunk = new byte[blob.getChunkSize()];
      int i = -1;

      File fullFile = new File(signature);

      FileInputStream is = new FileInputStream(fullFile );
      while ((i = is.read(chunk)) != -1) {
        os.write(chunk, 0, i); //Write the chunk
      }

      is.close();
      os.close();
      //Close the statement and commit
      pstmt.close();
      connection.commit();
      connection.setAutoCommit(true);
//      cm.takeDown();
    }
    %>
    <script type="text/javascript">
    alert("Certificate successfully locked");
    </script>
    <%
    }

/*
'******************************************************************************************************************************************
'Updated By    : Renad Hakim
'Date          : March 2005
'******************************************************************************************************************************************
*/

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Updated Certificate Lock Information')";
  boolean ub = cm.procedureExecute(ulog);

 if (isConnect)
  {
    cm.takeDown();
  }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Certificate/CertificateLock.jsp";
</script>
</body>
</html>
