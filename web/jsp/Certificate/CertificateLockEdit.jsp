<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Updated By    : Renad Hakim
'Creation Date : January 2006
'Page Purpose  : Edits Certificate Lock Information.
'******************************************************************************************************************************************
*/
</script>

<%@page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.io.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*"%>

<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="util"  class="batbsms.Utility"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}
%>

<html>
<head>
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   String certificateno =util.changeIfNull(request.getParameter("certificateno")).trim();
   String hfolio="";
   String comment="";
   String PAGE_STATUS="";
   String requested_by="";
   String queryPgStatus ="SELECT REQUESTED_BY FROM CERTIFICATE_LOCK_REQUESTBY WHERE CERTIFICATE_NO ='"+certificateno+"'";
   String islocked = "";

  if(cm.queryExecuteCount(queryPgStatus) <=0)
    PAGE_STATUS = "new";
   else
   {
     PAGE_STATUS = "old";
     cm.queryExecute(queryPgStatus);
     while(cm.toNext())
     {
       requested_by = cm.getColumnS("REQUESTED_BY");
       if (String.valueOf(requested_by).equals("null"))
           requested_by = "";
     }
   }


   String query1 = "SELECT HOLDER_FOLIO,\"LOCK\" AS IFLOCKED,\"COMMENT\" AS COMM FROM CERTIFICATE_VIEW WHERE CERTIFICATE_NO= '"+certificateno+ "'";
   cm.queryExecute(query1);

   while(cm.toNext())
     {
       hfolio = cm.getColumnS("HOLDER_FOLIO");
       comment = cm.getColumnS("COMM");
       islocked = cm.getColumnS("IFLOCKED");

       if (String.valueOf(hfolio).equals("null"))
         hfolio = "";
       if (String.valueOf(comment).equals("null"))
         comment = "";
         if (String.valueOf(islocked).equals("null") || islocked.equalsIgnoreCase(""))
           islocked = "NO";
         if(islocked.equalsIgnoreCase("T"))
         {
           islocked="YES";
         }
         else if(islocked.equalsIgnoreCase("F"))
         {
           islocked="NO";
         }

         comment = comment.trim();
     }

%>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit Signature</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function gotosig()
{
  location = "<%=request.getContextPath()%>/jsp/Certificate/CertificateLock.jsp";
}
function unlock()
{
  location = "<%=request.getContextPath()%>/jsp/Certificate/Unlock.jsp?certificateno="+ document.forms[0].certificateno.value + "&comments=" + document.forms[0].comments.value;
}
function editAttatchmentWindow()
{
  var win = window.open("CertificateLockEditPopup.jsp","","'status=yes,menubar=no,scrollbars=yes,resizable=yes,toolbar=no'");
  win.focus();
  win.moveTo( 0, 0 );
  win.resizeTo( screen.availWidth, screen.availHeight );
  return win;
}

function formconfirm()
{
  if (confirm("Do you really want to LOCK  the certificate?"))
  {
    return true;
  }
}

function showPreview(certificateno)
{
  var url="PrintPreview.jsp?certificateno="+certificateno;
  var win = window.open(url,"","'status=yes,menubar=no,scrollbars=yes,resizable=yes,toolbar=no'");
  win.focus();
  win.moveTo( 0, 0 );
  win.resizeTo( screen.availWidth, screen.availHeight );
  return win;
}
//Execute while click on Submit
function SubmitThis() {
  count = 0;

  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">

<form action="CertificateLockUpdate.jsp" method="get" enctype="multipart/form-data" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Certificate Information</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD" ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
	  <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Certificate No  </div></th>
        <td>:</td>
        <td><div align="left">
          <input name="certificateno" type="text" class="SL2TextField" id="certificateno" value="<%=certificateno%>" readonly="readonly">
        </div></td>
      </tr>
	  <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Holder's Folio </div></th>
        <td>:</td>
        <td><div align="left">
          <input name="foliono" type="text" class="SL2TextField" id="foliono" value="<%=hfolio%>" readonly="readonly">
        </div></td>
        </tr>
	  <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Locked</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <b><%=islocked%></b>
        </div></td>
        </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Requested by </div></th>
        <td width="1%">:</td>
        <td width="55%">
                  <select name="requestedby" class="SL1TextFieldListBox">
                    <%
                      requested_by = requested_by.trim();
                      String values[] = {"--Please Select--","CDBL","DSE","CSE","SEC","Others"};
                      for(int i=0;i<values.length;i++)
                      {
                        if(requested_by.equalsIgnoreCase(values[i]))
                        {
                          %>
                          <option value="<%=values[i]%>" selected="selected"><%=values[i]%></option>
                          <%
                          }
                          else
                          {
                          %>
                          <option value="<%=values[i]%>" ><%=values[i]%></option>
                          <%
                          }
                        }
                    %>
                  </select>

        </td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Comments</div></th>
        <td valign="top">:</td>
        <td align="left">
          <textarea name="comments" id="comments" class="ML10TextField"></textarea>
        </td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Attatchment (*.gif)</div></th>
        <td valign="top">:</td>
        <%
                 String selectQuery="SELECT ATTACHMENT FROM CERTIFICATE_LOCK_REQUESTBY WHERE CERTIFICATE_NO = '"+certificateno +"'";
                 int result  = cm.queryExecuteCount(selectQuery);
                 byte[] imgData = null;
                 if(result >0)
                   imgData =  cm.getPhoto( cm.getConnection(), selectQuery  );


        %>
        <td>
                <table width="149">
                  <tr><td width="41">
                  <%
                     if(imgData !=null && imgData.length !=0)
                     {
                  %>
                    <img name="BP" src="<%=request.getContextPath()%>/images/btnPreview.gif"  onclick="showPreview('<%=certificateno%>')" onMouseOver="document.forms[0].BP.src = '<%=request.getContextPath()%>/images/btnPreviewR.gif'" onMouseOut="document.forms[0].BP.src = '<%=request.getContextPath()%>/images/btnPreview.gif'">
                    </td>
                    <%
                     }
                     else
                     {
                     %>
                     <div class="style8"><b>[Attatchment Unavailable]</b></div>
                       <%
                       }
                     %>
                       <td width="96">
                      <img name="B6" src="<%=request.getContextPath()%>/images/btnEdit.gif"  onclick="editAttatchmentWindow()" onMouseOver="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnEditR.gif'" onMouseOut="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnEdit.gif'">
                       <input type="hidden" name="signature">
                       <input type="hidden" name="signaturestatus" value="edit"/>
                       <input type="hidden" name="pagestatus" value="<%=PAGE_STATUS%>"/>
                    </td>
                  </tr></table>
        </td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <td width="21%">
          <img name="BL" src="<%=request.getContextPath()%>/images/btnLockCert.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].BL.src = '<%=request.getContextPath()%>/images/btnLockCertR.gif'" onMouseOut="document.forms[0].BL.src = '<%=request.getContextPath()%>/images/btnLockCert.gif'">
        </td>
        <td width="22%">
          <img name="BUL" src="<%=request.getContextPath()%>/images/btnUnlockCert.gif"  onclick="unlock()" onMouseOver="document.forms[0].BUL.src = '<%=request.getContextPath()%>/images/btnUnlockCertR.gif'" onMouseOut="document.forms[0].BUL.src = '<%=request.getContextPath()%>/images/btnUnlockCert.gif'">
        </td>
        <td width="57%">
           <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="gotosig()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
        </td>
      </tr>
</table>
</form>
<%
/*
'******************************************************************************************************************************************
'Updated By    : Renad Hakim
'Date          : March 2005
'******************************************************************************************************************************************
*/

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Edited Certificate Lock Information')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
