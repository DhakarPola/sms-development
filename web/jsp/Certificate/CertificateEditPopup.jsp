<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Creation Date : January 2005
'Page Purpose  : Changes certificate number
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="util"  class="batbsms.Utility" scope="session"/>
<jsp:useBean id="cert"  class="batbsms.Certificate" scope="session"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head >
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>

<script type="text/javascript">
<!--
//Execute while click on Submit
function SubmitThis() {
 count = 0;
 BlankValidate('newCertificate','- New Certificate Number(Option must be entered)');

 if(document.forms[0].newCertificate.value.length == 9)
 {
   if (count == 0)
   {
     document.forms[0].submit();
   }
   else
   {
     ShowAllAlertMsg();
     return false;
   }
 }
  else  alert("Certificate Number should be 9 characters long");
}

function getOldCertificate()
{
  return window.opener.document.forms[0].oldCertificate.value;
}

function closeWindow()
{
  window.opener.history.go(0);
  self.close();
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 15px;
}
.style8 {
color: #0A2769;

}
.style9 {color: #0A2769; font-weight: bold; font-size:12px;}
-->
</style>
</head>
<body onload="document.forms[0].oldCertificate.value =getOldCertificate()" >
<%
    String updateCertificateNoQuery="";
    String newCertificate =util.changeIfNull(String.valueOf(request.getParameter("newCertificate")));
    String oldCertificate =util.changeIfNull(String.valueOf(request.getParameter("oldCertificate")));
    if(!newCertificate.equals(""))
    {
      if(util.alreadyExistsInDB("CERTIFICATE","CERTIFICATE_NO",newCertificate))
      {
          %>
              <script type="text/javascript">
                alert("The certificate no you have entered already exists in database.");
                history.go(-1);
              </script>
          <%
      }
      else
      {
         boolean isSuccess= cert.replaceCertificateNo(newCertificate,oldCertificate);
         if(isSuccess)
         {
            %>
                <script type="text/javascript">
                  alert("Certificate number successfully changed.");
                </script>
            <%
         }

      }
    }

%>
<form action="CertificateEditPopup.jsp" method="get" name="frmCertificateEditView" >
<table width="100%" border="1" cellspacing="0" cellpadding="0" style="border-collapse:collapse" bordercolor="#0044B0" bgcolor="#E8F3FD">
  <tr>
    <td>
	<table width="100%" border="0" cellspacing="0" cellpadding="5" >
      <tr>
        <td bgcolor="#0044B0" class="style7" height="30">Change Certificate Number </td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="26%" class="style9">&nbsp;&nbsp;&nbsp;&nbsp;Old Certificate No </td>
            <td width="1%">:</td>
            <td width="73%"><input name="oldCertificate" type="text" id="oldCertificate" class="SL70TextField" ></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="26%" class="style9">&nbsp;&nbsp;&nbsp;&nbsp;New Certificate No </td>
            <td width="1%">:</td>
            <td width="73%"><input name="newCertificate" type="text" class="SL70TextField" id="newCertificate" maxlength="9"></td>
          </tr>
        </table></td>
      </tr>
    </table>
  </tr>
</table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="22%"></td>
            <td width="27%">
                <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
            </td>
            <td width="51%">
                <img name="B2" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="closeWindow()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
            </td>
          </tr>
        </table>
</form>
</body>
</html>
