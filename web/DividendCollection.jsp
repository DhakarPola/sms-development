<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : June 2006
'Page Purpose  : Passes Parameters for Dividend Collection Printing.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

 boolean isConnect = cm.connect();
 if(isConnect==false)
 {
   %>
    <jsp:forward page="ErrorMsg.jsp" >
    <jsp:param name="ErrorTitle" value="Connection Failure" />
    <jsp:param name="ErrorHeading" value="Connection Problem" />
    <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
    </jsp:forward>
  <%
 }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Printed Dividend Collection Info.')";
  boolean ub = cm.procedureExecute(ulog);

  if (isConnect)
  {
   cm.takeDown();
  }
%>

<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page
import="java.util.Date,java.text.*,com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
Dividend Collection
</title>
</head>
<body bgcolor="#ffffff">
<h1>
</h1>
<%
  String DateDec = String.valueOf(request.getParameter("dateselect"));
  String DivType = String.valueOf(request.getParameter("dividendtype"));
  String DivCL = String.valueOf(request.getParameter("dividendcl"));


  String reporturl = "";

  if (DivType.equalsIgnoreCase("sms"))
  {
   if (DivCL.equalsIgnoreCase("divclaimed"))
   {
     reporturl = "/CR_Reports/Div_Collection/Claimed_Div.rpt";
   }
   else if (DivCL.equalsIgnoreCase("divunclaimed"))
   {
     reporturl = "/CR_Reports/Div_Collection/Unclaimed_Div.rpt";
   }
  }
  else if (DivType.equalsIgnoreCase("cdbl"))
  {
   if (DivCL.equalsIgnoreCase("divclaimed"))
   {
     reporturl = "/CR_Reports/Div_Collection/Claimed_BO_Div_Order_By_BOID.rpt";
   }
   else if (DivCL.equalsIgnoreCase("divunclaimed"))
   {
     reporturl = "/CR_Reports/Div_Collection/Unclaimed_BO_Div_Order_By_BOID.rpt";
   }
  }
  else if (DivType.equalsIgnoreCase("combined"))
  {
   if (DivCL.equalsIgnoreCase("divclaimed"))
   {
     reporturl = "/CR_Reports/Div_Collection/Combined_Claimed_Div_Order_By_ID.rpt";
   }
   else if (DivCL.equalsIgnoreCase("divunclaimed"))
   {
     reporturl = "/CR_Reports/Div_Collection/Combined_Unclaimed_Div_Order_By_ID.rpt";
   }
  }

  if (!reporturl.equals("")) {
    session.setAttribute("reportSource", null);
    session.setAttribute("refreshed", null);
  }

Object reportSource = session.getAttribute("reportSource");
if (reportSource == null)
{
try{
  IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
  reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
  session.setAttribute("reportSource", reportSource);
}catch (Exception e) {}
}
  CrystalReportViewer viewer= new CrystalReportViewer();
  try{
    viewer.setReportSource(reportSource);
  }catch (Exception e) {}

  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setOwnForm(true);

// Passing parameters

ParameterField param1 = new ParameterField();
param1.setReportName(""); // must always be set, even if just to ""
param1.setName("DecDate_Param");

Values vals1 = new Values();
ParameterFieldDiscreteValue val1 = new ParameterFieldDiscreteValue();

try {
 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
 Date d = df.parse(DateDec);
 val1.setValue(d);
 vals1.add(val1);
} catch(Exception e) {
e.printStackTrace();
}

param1.setCurrentValues(vals1);

Fields fields = new Fields();
fields.add(param1);

viewer.setParameterFields(fields);

if (session.getAttribute("refreshed") == null)
{
viewer.refresh();
session.setAttribute("refreshed", "true");
}


try
{
viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);
}
catch(Exception e){System.out.println(e.getMessage());}
%>
</body>
</html>

