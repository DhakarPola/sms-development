/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batbsms;

import java.sql.Timestamp;

/**
 *
 * @author User
 */
public class UserModel {
    int userId;
    String name;
    String loginId;
    String userRole;
    String userPass;
    int loginAttempt;
    String sessionId;
    boolean lockOut;                         // boolean true false 
    boolean disabled;                        // boolean true false 
    boolean firstTimeLogin;                  // boolean true false 
    boolean loggedIn;                        // boolean true false 
    Timestamp lockOutEndTime;
    Timestamp lastFailTime;
    Timestamp sessionEndTime;
    Timestamp inactivityEndTime;
    Timestamp passwordExpireTime;
    Timestamp loginTime;
    Timestamp logoutTime; 
    Timestamp lastActivityTime; 
    

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public int getLoginAttempt() {
        return loginAttempt;
    }

    public void setLoginAttempt(int loginAttempt) {
        this.loginAttempt = loginAttempt;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public boolean getLockOut() {
        return lockOut;
    }

    public void setLockOut(String lockOut) {
        this.lockOut = Boolean.parseBoolean(lockOut);
    }

    public boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(String disabled) {
        this.disabled = Boolean.parseBoolean(disabled);
    }

    public Timestamp getLockOutEndTime() {
        return lockOutEndTime;
    }

    public void setLockOutEndTime(Timestamp lockOutEndTime) {
        this.lockOutEndTime = lockOutEndTime;
    }

    public Timestamp getLastFailTime() {
        return lastFailTime;
    }

    public void setLastFailTime(Timestamp lastFailTime) {
        this.lastFailTime = lastFailTime;
    }

    public Timestamp getSessionEndTime() {
        return sessionEndTime;
    }

    public void setSessionEndTime(Timestamp sessionEndTime) {
        this.sessionEndTime = sessionEndTime;
    }

    public Timestamp getInactivityEndTime() {
        return inactivityEndTime;
    }

    public void setInactivityEndTime(Timestamp inactivityEndTime) {
        this.inactivityEndTime = inactivityEndTime;
    }

    public Timestamp getPasswordExpireTime() {
        return passwordExpireTime;
    }

    public void setPasswordExpireTime(Timestamp passwordExpireTime) {
        this.passwordExpireTime = passwordExpireTime;
    }

    public Timestamp getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Timestamp loginTime) {
        this.loginTime = loginTime;
    }

    public Timestamp getLogoutTime() {
        return logoutTime;
    }

    public void setLogoutTime(Timestamp logoutTime) {
        this.logoutTime = logoutTime;
    }   

    public boolean getFirstTimeLogin() {
        return firstTimeLogin;
    }

    public void setFirstTimeLogin(String firstTimeLogin) {
        this.firstTimeLogin = Boolean.parseBoolean(firstTimeLogin);
    }

    public boolean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(String loggedIn) {
        this.loggedIn = Boolean.parseBoolean(loggedIn);
    }

    public Timestamp getLastActivityTime() {
        return lastActivityTime;
    }

    public void setLastActivityTime(Timestamp lastActivityTime) {
        this.lastActivityTime = lastActivityTime;
    }

    @Override
    public String toString() {
        return "UserModel{" + "userId=" + userId + ", name=" + name + ", loginId=" + loginId + ", userRole=" + userRole + ", userPass=" + userPass + ", loginAttempt=" + loginAttempt + ", sessionId=" + sessionId + ", lockOut=" + lockOut + ", disabled=" + disabled + ", firstTimeLogin=" + firstTimeLogin + ", loggedIn=" + loggedIn + ", lockOutEndTime=" + lockOutEndTime + ", lastFailTime=" + lastFailTime + ", sessionEndTime=" + sessionEndTime + ", inactivityEndTime=" + inactivityEndTime + ", passwordExpireTime=" + passwordExpireTime + ", loginTime=" + loginTime + ", logoutTime=" + logoutTime + ", lastActivityTime=" + lastActivityTime + '}';
    }
}
