/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batbsms;

import java.security.MessageDigest;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Authentication {

    private UserDAO userData;
    public static boolean authError = false;
    public static String authMessageTitle = "";
    public static String authMessageHeading = "";
    public static String authMessage = "";
    private String LOGINATTEMPT = "LOGINATTEMPT";
    private String LOCKOUT = "LOCKOUT";
    private String LOCKOUTENDTIME = "LOCKOUTENDTIME";
    private String DISABLED = "DISABLED";
    private String LASTACTIVITYTIME = "LASTACTIVITYTIME";
    private String INACTIVITYENDTIME = "INACTIVITYENDTIME";
    private String FIRSTTIMELOGIN = "FIRSTTIMELOGIN";
    private String LOGGEDIN = "LOGGEDIN";
    private String PASSWORDEXPIRETIME = "PASSWORDEXPIRETIME";

    public boolean checkUserId(String userId) {
        boolean userExist = false;
        try {
            userData = new UserDAO();
            UserModel user = userData.getUserByName(userId);

            if (user.getLoginId().equals(userId)) {
                System.out.println(user.getName() + " - " + user.getLoginId());
                userExist = true;
            } else if (!user.getLoginId().equals(userId)) {
                System.out.println(user.getName() + " - " + user.getLoginId());
                userExist = false;
            } else if (user.getLoginId().equals(null)) {
                System.out.println(user.getName() + " - " + user.getLoginId());
                userExist = false;
            }
        } catch (NullPointerException ex) {
            userExist = false;
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, "NullPointerException", "NullPointerException");
        } catch (Exception ex) {
            userExist = false;
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, "Exception", "Exception");
        }

        return userExist;
    }

    public boolean lockedOut(String userId) {
        boolean lockedOut = false;
        try {
            userData = new UserDAO();
            UserModel user = userData.getUserByName(userId);

            if (user.getLockOut()) {
                lockedOut = true;
            } else if (user.getLockOut() == false) {
                lockedOut = false;
            }
        } catch (NullPointerException ex) {
            lockedOut = false;
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, "NullPointerException", "NullPointerException");
        } catch (Exception ex) {
            lockedOut = false;
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, "Exception", "Exception");
        }

        return lockedOut;
    }

    public boolean passwordValidation(String userPassword, String dbPassword) {
        boolean passwordValid = false;
        try {

            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(userPassword.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            userPassword = sb.toString();

            if (userPassword.equals(dbPassword)) {
                passwordValid = true;
                System.out.println("Password Validated .... ");
            } else if (!userPassword.equals(dbPassword)) {
                passwordValid = false;
                System.out.println("Password Validation Failed .... ");
            }
        } catch (NullPointerException ex) {
            passwordValid = false;
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, "NullPointerException", "NullPointerException");
        } catch (Exception ex) {
            passwordValid = false;
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, "Exception", "Exception");
        }
        return passwordValid;
    }

    public void increaseLoginAttempt(String userId, int loginAttempt) {

        if (loginAttempt > 3) {
            loginAttempt = 0;
        } else {
            loginAttempt = loginAttempt + 1;
        }
        try {
            userData.updateColumnInteger(userId, LOGINATTEMPT, loginAttempt);
            System.out.println("Login Attempt Increased .... ");
        } catch (Exception ex) {
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void doSetLockout(String userId, boolean lockOutStatus) {
        String newStatus = "";
        try {
            if (lockOutStatus == false) {
                newStatus = "TRUE";
            }

            userData.updateColumnStatus(userId, LOCKOUT, newStatus);
            System.out.println("Account Locked Out .... ");
        } catch (Exception ex) {
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            Date currentTime = new Date();
            Date lockoutEndTime = new Date();

            Calendar cal = Calendar.getInstance();
            cal.setTime(currentTime);
            cal.add(Calendar.MINUTE, 2);                                        // this will add two minutes
            lockoutEndTime = cal.getTime();
            userData.updateDatabaseTime(userId, LOCKOUTENDTIME, lockoutEndTime.getTime());
            System.out.println("Locked Out until .... " + lockoutEndTime);
        } catch (Exception ex) {
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void doResetLockout(String userId, boolean lockOutStatus) {
        String newStatus = "";
        try {
            if (lockOutStatus == true) {
                newStatus = "FALSE";
            }
            userData.updateColumnStatus(userId, LOCKOUT, newStatus);

            System.out.println("Lockout released .... ");
        } catch (Exception ex) {
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            userData.updateColumnInteger(userId, LOGINATTEMPT, 0);
            System.out.println("Lockout Attempt Reset ... ");
        } catch (Exception ex) {
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean lockoutEndTimeOver(Timestamp time) {
        Date nowTime = new Date();
        Date endTime = new Date();
        boolean timeOver = false;
        try {

            endTime = new Date(time.getTime());

            if (nowTime.after(endTime)) {
                timeOver = true;
                System.out.println("Lockout End Time Over .... ");
            }
        } catch (NullPointerException ex) {
            timeOver = false;
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, "NullPointerException", ex);
        } catch (Exception ex) {
            timeOver = false;
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, "Exception", "Exception");
        }
        return timeOver;
    }

    public boolean checkUserDisabled(String userId) {
        boolean userDisabled = false;
        try {
            if (Boolean.parseBoolean(userData.getColumnString(userId, DISABLED))) {
                userDisabled = true;
                System.out.println("User access disable .... ");
            }

        } catch (Exception ex) {
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, null, ex);
        }
        return userDisabled;
    }

    public boolean checkUserInactivity(String userId) {
        Timestamp lastActivityTime = null;
        boolean userInactive = false;
        Date currentTime = new Date();
        Date inactivityDeadline = new Date();

        try {
            lastActivityTime = userData.getColumnTime(userId, LASTACTIVITYTIME);

            Calendar cal = Calendar.getInstance();
            cal.setTime(lastActivityTime);
            cal.add(Calendar.MINUTE, 20);                                        // this will add two minutes
            inactivityDeadline = cal.getTime();

            if (currentTime.after(inactivityDeadline)) {
                userInactive = true;
                System.out.println("currentTime is after inactivityDeadline .....");
                System.out.println("User has no activity for last 45 days .... ");
            } else {
                System.out.println("inactivityDeadline is after currentTime .....");
            }

        } catch (Exception ex) {
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, null, ex);
        }
        return userInactive;
    }

    public boolean firstTimeLogin(String userId) {
        boolean firstLogin = false;
        try {
            if (Boolean.parseBoolean(userData.getColumnString(userId, FIRSTTIMELOGIN))) {
                firstLogin = true;
                System.out.println("First Time loggedin .... ");
            }

        } catch (Exception ex) {
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, null, ex);
        }
        return firstLogin;
    }

    public boolean concurrentLogin(String userId) {
        boolean concurrentLogin = false;
        
        try {
            //System.err.println("logged in status "+ userData.getColumnString(userId, LOGGEDIN));
            if (Boolean.parseBoolean(userData.getColumnString(userId, LOGGEDIN))) {
                concurrentLogin = true;
                System.out.println("Already logged into system .... ");
            }

        } catch (Exception ex) {
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, null, ex);
        }
        return concurrentLogin;        
    }

    public boolean passwordExpiration(String userId, Timestamp expirationTime) {
        boolean passwordExpired = false;
        Date currentTime = new Date();

        try {
            if (currentTime.after(expirationTime)) {
                passwordExpired = true;
                System.out.println("currentTime is after expirationDeadline .....");
                System.out.println("Password  has not been change since last 90 days .... ");
            } else {
                System.out.println("expirationDeadline is after currentTime .....");
            }
        } catch (Exception ex) {
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, null, ex);
        }
        return passwordExpired; 
    }

    public String passwordChange(String userId, String password) {
        return "";
    }

    public void disableUser(String userId, boolean disableStatus) {
        String newStatus = "";
        try {
            if (disableStatus == false) {
                newStatus = "TRUE";
            }

            userData.updateColumnStatus(userId, DISABLED, newStatus);
            System.out.println("User disabled for Inactivity .... ");
        } catch (Exception ex) {
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String passwordHistoryCheck(String userId, String password) {
        return "";
    }

    public void updateLoginTime(String userId, String time) {
    }

    public void updateLogoutTime(String userId, String time) {
    }

    public void updateLockoutEndTime() {

    }

    public void updateLastFailTime(String userId, String time) {
    }

    public void updateSsessionEndTime(String userId, String time) {
    }

    public void updateInactivityEndTime(String userId, String time) {
    }

    public void updatePasswordExpireTime(String userId, String time) {
    }
}
