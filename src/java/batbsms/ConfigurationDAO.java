/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batbsms;

import java.sql.PreparedStatement;

/**
 *
 * @author User
 */
public class ConfigurationDAO {

    private final conBean connection;
    private final String TableName = "Configuration";
    private final String ConfigKey = "CONFIGKEY";
    private final String ConfigValue = "CONFIGVALUE";

    private PreparedStatement statement;

    public ConfigurationDAO() {
        connection = new conBean();
    }

    public String getConfigurationByKey(String keyName) throws Exception {

        connection.connect();
        String columnValue = "";

        String SQL = "SELECT " + ConfigValue + " FROM " + TableName + " WHERE CONFIGKEY ='" + keyName + "'";
        //System.err.println(SQL);

        if (connection.queryExecuteCount(SQL) > 0) {
            connection.rs = connection.stmt.executeQuery(SQL);

            while (connection.rs.next()) {
                columnValue = connection.rs.getString("ConfigValue");
            }
        }
        connection.takeDown();
        return columnValue;
    }

    public ConfigurationModel getConfiguration() throws Exception {
        ConfigurationModel config = new ConfigurationModel();

        config.setUnsuccessfulTry(Integer.parseInt(getConfigurationByKey("UNSUCCESSFUL_TRY")));
        config.setSessionTimeOut(Integer.parseInt(getConfigurationByKey("SESSION_TIMEOUT")));
        config.setInactivityTime(Integer.parseInt(getConfigurationByKey("INACTIVITY_TIME")));
        config.setPasswordChange(Integer.parseInt(getConfigurationByKey("PASSWORD_CHANGE")));
        config.setPasswordChangeHistory(Integer.parseInt(getConfigurationByKey("PASSWORD_CHANGE_HISTORY")));
        config.setPasswordComplexitySpeical(Boolean.parseBoolean(getConfigurationByKey("PASSWORD_COMPLEXITY_SPCHARACTER")));
        config.setPasswordComplexityCapital(Boolean.parseBoolean(getConfigurationByKey("PASSWORD_COMPLEXITY_CAPITAL")));
        config.setPasswordComplexityNumber(Boolean.parseBoolean(getConfigurationByKey("PASSWORD_COMPLEXITY_NUMBER")));
        config.setPasswordLengthAdmin(Integer.parseInt(getConfigurationByKey("PASSWORD_LENGTH_ADMIN")));
        config.setPasswordLengthUser(Integer.parseInt(getConfigurationByKey("PASSWORD_LENGTH_USER")));

        return config;
    }

    public void updateConfigurationByKey(String keyName, String columnValue) throws Exception {
        connection.connect();
        String SQL = "UPDATE " + TableName + " SET  ConfigValue = '"+columnValue + "' WHERE CONFIGKEY ='" + keyName + "'";
        connection.stmt.executeUpdate(SQL);
        connection.takeDown();
    }

    public boolean updateConfiguration(ConfigurationModel c) {
        boolean updateSuccessful = false;
        try {
            updateConfigurationByKey("UNSUCCESSFUL_TRY",String.valueOf(c.getUnsuccessfulTry()));
            updateConfigurationByKey("SESSION_TIMEOUT",String.valueOf(c.getSessionTimeOut()));
            updateConfigurationByKey("INACTIVITY_TIME",String.valueOf(c.getInactivityTime()));
            updateConfigurationByKey("PASSWORD_CHANGE",String.valueOf(c.getPasswordChange()));
            updateConfigurationByKey("PASSWORD_CHANGE_HISTORY",String.valueOf(c.getPasswordChangeHistory()));
            updateConfigurationByKey("PASSWORD_COMPLEXITY_SPCHARACTER",String.valueOf(c.isPasswordComplexityCapital()));
            updateConfigurationByKey("PASSWORD_COMPLEXITY_CAPITAL",String.valueOf(c.isPasswordComplexitySpeical()));
            updateConfigurationByKey("PASSWORD_COMPLEXITY_NUMBER",String.valueOf(c.isPasswordComplexityNumber()));
            updateConfigurationByKey("PASSWORD_LENGTH_ADMIN",String.valueOf(c.getPasswordLengthAdmin()));
            updateConfigurationByKey("PASSWORD_LENGTH_USER",String.valueOf(c.getPasswordLengthUser()));
            updateSuccessful = true;
        } catch (Exception e) {
            updateSuccessful = false;
            System.err.println("Co9nfiguration update problem. ");
        }
        return updateSuccessful;
    }
}
