/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batbsms;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class UserDAO {

    private final conBean connection;
    private final String TableName = "ROLEDEFINITION_DEV";
    private PreparedStatement statement;

    public UserDAO() {
        connection = new conBean();
    }

    public List<UserModel> getAllUser() throws Exception {
        connection.connect();
        List<UserModel> users = new ArrayList<>();
        String SQL = "SELECT * FROM ROLEDEFINITION_DEV";

        if (connection.queryExecuteCount(SQL) > 0) {
            connection.rs = connection.stmt.executeQuery(SQL);

            while (connection.rs.next()) {
                UserModel user = new UserModel();

                user.setUserId(connection.rs.getInt("USERID"));
                user.setName(connection.rs.getString("NAME"));
                user.setLoginId(connection.rs.getString("LOGIN_ID"));
                user.setUserRole(connection.rs.getString("USER_ROLE"));
                user.setUserPass(connection.rs.getString("USER_PASS"));
                user.setLoginAttempt(connection.rs.getInt("LOGINATTEMPT"));
                user.setLockOut(connection.rs.getString("LOCKOUT"));
                user.setDisabled(connection.rs.getString("DISABLED"));
                user.setLockOutEndTime(connection.rs.getTimestamp("LOCKOUTENDTIME"));
                user.setLastFailTime(connection.rs.getTimestamp("LASTFAILTIME"));
                user.setSessionEndTime(connection.rs.getTimestamp("SESSIONENDTIME"));
                user.setInactivityEndTime(connection.rs.getTimestamp("INACTIVITYENDTIME"));
                user.setPasswordExpireTime(connection.rs.getTimestamp("PASSWORDEXPIRETIME"));
                user.setLoginTime(connection.rs.getTimestamp("LOGINTIME"));
                user.setLogoutTime(connection.rs.getTimestamp("LOGOUTTIME"));
                user.setLastActivityTime(connection.rs.getTimestamp("LASTACTIVITYTIME"));
                user.setFirstTimeLogin(connection.rs.getString("FIRSTTIMELOGIN"));
                user.setLoggedIn(connection.rs.getString("LOGGEDIN"));

                users.add(user);
            }
        }
        connection.takeDown();
        return users;
    }

    public UserModel getUserByName(String userId) throws Exception {
        connection.connect();
        UserModel user = new UserModel();
        String SQL = "SELECT * FROM ROLEDEFINITION_DEV where lower(login_id)=lower('" + userId + "')";

        if (connection.queryExecuteCount(SQL) > 0) {
            connection.rs = connection.stmt.executeQuery(SQL);

            while (connection.rs.next()) {
                user.setUserId(connection.rs.getInt("USERID"));
                user.setName(connection.rs.getString("NAME"));
                user.setLoginId(connection.rs.getString("LOGIN_ID"));
                user.setUserRole(connection.rs.getString("USER_ROLE"));
                user.setUserPass(connection.rs.getString("USER_PASS"));
                user.setLoginAttempt(connection.rs.getInt("LOGINATTEMPT"));
                user.setLockOut(connection.rs.getString("LOCKOUT"));
                user.setDisabled(connection.rs.getString("DISABLED"));
                user.setLockOutEndTime(connection.rs.getTimestamp("LOCKOUTENDTIME"));
                user.setLastFailTime(connection.rs.getTimestamp("LASTFAILTIME"));
                user.setSessionEndTime(connection.rs.getTimestamp("SESSIONENDTIME"));
                user.setInactivityEndTime(connection.rs.getTimestamp("INACTIVITYENDTIME"));
                user.setPasswordExpireTime(connection.rs.getTimestamp("PASSWORDEXPIRETIME"));
                user.setLoginTime(connection.rs.getTimestamp("LOGINTIME"));
                user.setLogoutTime(connection.rs.getTimestamp("LOGOUTTIME"));
                user.setLastActivityTime(connection.rs.getTimestamp("LASTACTIVITYTIME"));
                user.setFirstTimeLogin(connection.rs.getString("FIRSTTIMELOGIN"));
                user.setLoggedIn(connection.rs.getString("LOGGEDIN"));
            }
        }
        connection.takeDown();
        return user;
    }

    public String getColumnString(String userId, String colunmName) throws Exception {
        String columnValue = "";
        String SQL = "SELECT " + colunmName + " FROM " + TableName + " WHERE lower(login_id)=lower('" + userId + "')";

        connection.connect();
        if (connection.queryExecuteCount(SQL) > 0) {
            connection.rs = connection.stmt.executeQuery(SQL);
            connection.rs.next();
            columnValue = connection.rs.getString(colunmName);
        }
        connection.takeDown();
        return columnValue;
    }

    public Timestamp getColumnTime(String userId, String colunmName) throws Exception {
        Timestamp columnTime = null;
        String SQL = "SELECT " + colunmName + " FROM " + TableName + " WHERE lower(login_id)=lower('" + userId + "')";

        connection.connect();
        if (connection.queryExecuteCount(SQL) > 0) {
            connection.rs = connection.stmt.executeQuery(SQL);
            connection.rs.next();
            columnTime = connection.rs.getTimestamp(colunmName);
        }
        connection.takeDown();
        return columnTime;
    }

    public void updateColumnInteger(String userId, String colunmName, int value) throws Exception {
        connection.connect();
        String SQL = "UPDATE " + TableName + " SET " + colunmName + " = " + value + " WHERE lower(login_id)=lower('" + userId + "')";
        connection.stmt.execute(SQL);
        connection.connCommit();
        connection.takeDown();
    }

    public void updateColumnStatus(String userId, String colunmName, String status) throws Exception {
        connection.connect();
        statement = connection.con.prepareStatement("UPDATE " + TableName + " SET " + colunmName + " = ? WHERE lower(login_id)=lower('" + userId + "')");
        statement.setString(1, status);
        statement.executeUpdate();
        connection.takeDown();
    }

    public void updateDatabaseTime(String userId, String colunmName, long timeInMillis) throws Exception {
        connection.connect();
        statement = connection.con.prepareStatement("UPDATE " + TableName + " SET " + colunmName + " = ? WHERE lower(login_id)=lower('" + userId + "')");
        statement.setTimestamp(1, new Timestamp(timeInMillis));
        statement.executeUpdate();
        connection.takeDown();
    }
}
