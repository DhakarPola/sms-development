package barcode.env;

import java.awt.*;

/**
 * Provides a way of accessing operating environment specific
 * information that is used in processing or displaying barcodes.
 *
 * @author <a href="mailto:ian@thoughtworks.net">Ian Bourke</a>
 */
public interface Environment {

        /**
         * Returns the environment determined resolution for
         * outputting barcodes.
         * @return The resolution for the environment
         */
        int getResolution();

    /**
     * Returns the default font for the environment. This is
     * included mostly to get around problems with font initialisation
     * on certain VMs in a headless environment.
     * @return The default font for the environment
     */
    Font getDefaultFont();
}
