package barcode.env;

import java.awt.*;

public final class DefaultEnvironment implements Environment {
    /** The default font for drawing the barcode data underneath the bars */
        public static final Font DEFAULT_FONT = new Font("Arial", Font.PLAIN, 20);

        /**
         * Returns the environment determined resolution for
         * outputting barcodes.
         * @return The resolution for the environment
         */
        public int getResolution() {
                return Toolkit.getDefaultToolkit().getScreenResolution();
        }

    /**
     * Returns the default font for the environment.
     * @return The default font for the environment
     */
    public Font getDefaultFont() {
        return DEFAULT_FONT;
    }
}
