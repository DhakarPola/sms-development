package barcode.linear.code128;

import java.util.List;
import java.util.ArrayList;

final class CharBuffer {
	private final int size;
	private List chars;

	CharBuffer(int size) {
		this.size = size;
		this.chars = new ArrayList();
	}

	int size() {
		return chars.size();
	}

	void addChar(char c) {
		chars.add(new Character(c));
	}

	boolean isFull() {
		return chars.size() == size;
	}

	public String toString() {
		char[] out = new char[size];
		for (int i = 0; i < size; i++) {
			Character character = (Character) chars.get(i);
			out[i] = character.charValue();
		}
		return new String(out);
	}

	void clear() {
		chars.clear();
	}
}
