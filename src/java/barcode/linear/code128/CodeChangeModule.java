package barcode.linear.code128;

import barcode.Module;

/**
 * Specific module implementation that defines the Code 128 Code change character.
 *
 * @author <a href="mailto:ian@thoughtworks.net">Ian Bourke</a>
 */
class CodeChangeModule extends Module {
	private final int code;

	/**
	 * Constructs a code change module with the specified bars and character set.
	 * @param bars The bars to draw to encode the shift
	 * @param code The character set that this module represents
	 */
	public CodeChangeModule(int[] bars, int code) {
		super(bars);
		this.code = code;
	}

	/**
	 * Returns the character code set that this module represents.
	 * @return The character set
	 */
	public int getCode() {
		return code;
	}
}
