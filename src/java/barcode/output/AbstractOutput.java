package barcode.output;
import java.awt.*;
public abstract class AbstractOutput {

        /** The bar height. */
        protected final double barHeight;
        /** Flag indicating whether the barcode will actually be outputted, or is just being sized. */
        protected final boolean painting;
        /** The scaling factor to correctly size the barcode in the output units. */
        protected final double scalar;
        /** The font to draw any text labels with. */
        protected final Font font;
        /** The background colour for drawing */
        protected final Color backgroundColour;
        /** The foreground colour for drawing */
        protected Color foregroundColour;

        private Color savedColour;
        private final double barWidth;

        /**
         * Populates this abstract outputter with common values.
         * @param font The font to draw text labels with
         * @param barWidth The width of the smallest bar (in bar units)
         * @param barHeight The height of the bars
         * @param painting Flag indicating whether painting will actually occur
         * @param scalar The scaling factor to size the barcode into the correct units
         * @param foregroundColour The colour to paint in
         * @param backgroundColour The background colour
         */
        protected AbstractOutput(Font font, double barWidth, double barHeight, boolean painting,
                                                         double scalar, Color foregroundColour, Color backgroundColour) {
                this.barWidth = barWidth;
                this.barHeight = barHeight;
                this.painting = painting;
                this.scalar = scalar;
                this.font = font;
                this.foregroundColour = foregroundColour;
                this.backgroundColour = backgroundColour;
        }

        /**
         * Used by BlankModule() to prevent drawing of the white header space. Must be
         * matched with a call to tearDownFromBlankDrawing()
         */
        public void setupForBlankDrawing() {
                savedColour = foregroundColour;
                foregroundColour = backgroundColour;
        }

        /** see setupForBlankDrawing() */
        public void teardownFromBlankDrawing() {
                foregroundColour = savedColour;
                savedColour = null;
        }

        /** returns the unit bar width
         * @return the width of the smallest bar
         */
        public double getBarWidth() { return barWidth; }

        /** Get the Height of the barcode
         * @return the height
         */
        public double getBarHeight() { return barHeight; }

        /**
         * "Draws" a bar at the given coordinates.
         * @param x the x coordinate
         * @param y the y coordinate
         * @param width the width
         * @param height the height
         * @param paintWithForegroundColor if true, use the foreground color, otherwise use the background color
         */
        public abstract void drawBar(int x, int y, int width, int height, boolean paintWithForegroundColor);

        /**
         * Sets up the drawing environment.  Called before drawBar().  Matched with call to
         * endDraw() at the end.  This allows for caching as needed.
         * @param width The output width (in pixels) of the barcode
         * @param height The output height (in pixels) of the barcode.
         */
        public abstract void beginDraw(double width, double height);

        /**
         * Balanced with startDraw() above, used for caching, output of epilogues (for
         * SVG), etc.
         */
        public abstract void endDraw();

        /**
         * Draw the specified text.
         * @param text text to draw
         * @param x x position
         * @param y y position
         * @param width width of barcode (total)
         * @return the height of this text
         */
        public abstract double drawText(String text, double x, double y, double width);

        /**
         * Returns the current foreground colour for any artifacts drawn to
         * the graphics.
         * @return The current foreground colour
         */
        public Color getForegroundColour() {
                return foregroundColour;
        }

        /**
         * Returns the current background colour for any artifacts drawn to
         * the graphics.
         * @return The current background colour
         */
        public Color getBackgroundColour() {
                return backgroundColour;
        }
}
