package barcode.output;

import java.awt.*;
import java.awt.font.TextLayout;
public class GraphicsOutput extends AbstractOutput {

        /** Pixel gap between the barcode bars and the top of the data text underneath */
        public static final int BARS_TEXT_VGAP = 5;

        private final Graphics2D g;

        /**
         * Creates a Graphics2D AbstractOutput
         * @param graphics The graphics to output to
         * @param font The font for text rendering
         * @param barWidth The width of the smallest bar
         * @param barHeight The height of the barcode
         * @param paint If true, we are drawing, if false we are calculating
         * @param fgColor Foreground Color
         * @param bgColor Background Color
         */
        public GraphicsOutput(Graphics2D graphics, Font font, double barWidth,
                                                  double barHeight, boolean paint, Color fgColor, Color bgColor) {
                super(font, barWidth, barHeight, paint, 1.0, fgColor, bgColor);
                this.g = graphics;
        }

        /**
         * From AbstractOutput - Does nothing.
         * @param width The output width (in pixels) of the barcode
         * @param height The output height (in pixels) of the barcode.
         */
        public void beginDraw(double width, double height) {
                // Do nothing
        }

        /**
         * From AbstractOutput - Does nothing.
         */
        public void endDraw() {
                // Do nothing
        }

        /**
         * From AbstractOutput - Draws a bar at the given coordinates onto the output Graphics.
         * @param x the x coordinate
         * @param y the y coordinate
         * @param width the width
         * @param height the height
         * @param paintWithForegroundColor if true, use the foreground color, otherwise use the background color
         */
        public void drawBar(int x, int y, int width, int height, boolean paintWithForegroundColor) {
                if (painting) {
                        if (paintWithForegroundColor) {
                                g.setColor(this.foregroundColour);
                        } else {
                                g.setColor(this.backgroundColour);
                        }

                        g.fillRect((int) (scalar * x), (int) (scalar * y),
                                           (int) (scalar * width), (int) (scalar * height));
                }
        }

        /**
         * From AbstractOutput - Draw the specified text to the output graphics.
         * @param text The text to draw
         * @param x The x position
         * @param y The y position
         * @param width The width of barcode (total)
         * @return The height of this text
         */
        public double drawText(String text, double x, double y, double width) {
        if (font == null) {
            return 0;
        } else {
            TextLayout layout = new TextLayout(text, font, g.getFontRenderContext());
            float startX = (float) ((((width - x) - layout.getBounds().getWidth()) / 2) + x);
            double startY = (float) (y + layout.getBounds().getHeight() + BARS_TEXT_VGAP);
            int height = (int) (startY - y + 1);

            if (painting) {
                g.setColor(backgroundColour);
                g.fillRect((int) x, (int) y, (int) (width - x), height);
                g.setColor(foregroundColour);
                layout.draw(g, startX, (float) startY);
            }

            return height;
        }
        }
}
