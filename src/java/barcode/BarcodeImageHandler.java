package barcode;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

import java.io.IOException;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;

//import barcode.linear.code128.Code128Barcode;
//import barcode.env.EnvironmentFactory;

public final class BarcodeImageHandler {

        ///CLOVER:OFF
        /**
         * You can't construct one of these.
         */
        private BarcodeImageHandler() {
        }
        ///CLOVER:ON

        /**
         * Creates an image for a barcode that can be used in other GUI
         * componenets (e.g. ImageIcon, JLabel etc).
         * @param barcode The barcode to convert into an image
         * @return The image
         */
        public static BufferedImage getImage(Barcode barcode) {
        BufferedImage bi = new BufferedImage(barcode.getWidth(), barcode.getHeight(), BufferedImage.TYPE_BYTE_INDEXED);
        Graphics2D g = bi.createGraphics();
        barcode.draw(g, 0, 0);
        bi.flush();
        return bi;
    }

        /**
         * Outputs a barcode directly to the given output stream, encoded as a JPEG
         * image.
         * @param barcode The barcode to output
         * @param os The output stream to write the image to
         * @throws IOException If the image cannot be written to the output stream correctly
         */
        public static void outputBarcodeAsJPEGImage(Barcode barcode, OutputStream os) throws IOException {
                outputImage(barcode, os);
        }

        /**
         * Outputs a barcode as a JPEG image to the specified stream.
         * @param barcode The barcode to output
         * @param os The output stream
         * @throws IOException If an error occurred writing to the stream
         */
        private static void outputImage(Barcode barcode, OutputStream os) throws IOException {
                BufferedImage image = getImage(barcode);
                JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(os);
        encoder.encode(image, encoder.getDefaultJPEGEncodeParam(image));
        }
}
