<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : January 2007
'Page Purpose  : Passes Parameters for Bank Letter Report.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

 boolean isConnect = cm.connect();
 if(isConnect==false)
 {
   %>
    <jsp:forward page="ErrorMsg.jsp" >
    <jsp:param name="ErrorTitle" value="Connection Failure" />
    <jsp:param name="ErrorHeading" value="Connection Problem" />
    <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
    </jsp:forward>
  <%
 }

%>

<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<%@page
import="java.util.Date,java.text.*,com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
Bank Letter
</title>
</head>
<body bgcolor="#ffffff">
<h1>
</h1>
<%


  String letter = String.valueOf(request.getParameter("letterselect"));
  String bank=String.valueOf(request.getParameter("bankselect"));
  String sig=String.valueOf(request.getParameter("sigselect"));
  String account=String.valueOf(request.getParameter("accountselect"));
  String sigid=String.valueOf(request.getParameter("sid"));


   if (String.valueOf(letter).equals("null"))
   letter = "";
   if (String.valueOf(bank).equals("null"))
   bank = "";
   if (String.valueOf(sig).equals("null"))
   sig = "";
   if (String.valueOf(account).equals("null"))
   account = "";
  String reporturl = "";

     if (letter.equals("null")||letter.equals(""))
     {
     %>
      <script language="javascript">
       alert("Letter name not exist!");
       location = "<%=request.getContextPath()%>/jsp/Dividend/SetBankLettr.jsp?account=0&sig=0&letter=0&bank=0&sigid=0";
      </script>
     <%
     }
    else if (bank.equals("null")||bank.equals(""))
     {
     %>
      <script language="javascript">
       alert("Bank name not exist!");
       location = "<%=request.getContextPath()%>/jsp/Dividend/SetBankLettr.jsp?account=0&sig=0&letter=0&bank=0&sigid=0";
      </script>
     <%
     }

     else if (sig.equals("null")||sig.equals(""))
     {
     %>
      <script language="javascript">
       alert("Signature not exist!");
       location = "<%=request.getContextPath()%>/jsp/Dividend/SetBankLettr.jsp?account=0&sig=0&letter=0&bank=0&sigid=0";
      </script>
     <%
     }
     else if (account.equals("null")||account.equals(""))
     {
     %>
      <script language="javascript">
       alert("Account number not exist!");
       location = "<%=request.getContextPath()%>/jsp/Dividend/SetBankLettr.jsp?account=0&sig=0&letter=0&bank=0&sigid=0";
      </script>
     <%
     }
     else
     {
         reporturl ="/CR_Reports/Bank_Recon/Bank_Letter1.rpt";
     }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Printed Bank Letter')";
  boolean ub = cm.procedureExecute(ulog);

  if (!reporturl.equals("")) {
    session.setAttribute("reportSource", null);
    session.setAttribute("refreshed", null);
  }

Object reportSource = session.getAttribute("reportSource");
if (reportSource == null)
{
try{
  IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
  reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
  session.setAttribute("reportSource", reportSource);
}catch (Exception e) {}
}
  CrystalReportViewer viewer= new CrystalReportViewer();
  try{
    viewer.setReportSource(reportSource);
  }catch (Exception e) {}
  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  //viewer.setPrintMode(CrPrintMode.PDF);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setReuseParameterValuesOnRefresh(true);
// Passing parameters
ParameterField param1 = new ParameterField();
param1.setReportName("");
param1.setName("bankname");

Values vals1 = new Values();
ParameterFieldDiscreteValue val1 = new ParameterFieldDiscreteValue();
val1.setValue(bank);
vals1.add(val1);

ParameterField param2=new ParameterField();
param2.setReportName("");
param2.setName("lettername");

Values vals2=new Values();
ParameterFieldDiscreteValue val2 = new ParameterFieldDiscreteValue();
val2.setValue(String.valueOf(letter));
vals2.add(val2);

ParameterField param3=new ParameterField();
param3.setReportName("");
param3.setName("sigid");

Values vals3=new Values();
ParameterFieldDiscreteValue val3 = new ParameterFieldDiscreteValue();
val3.setValue(sigid);
vals3.add(val3);

ParameterField param4=new ParameterField();
param4.setReportName("");
param4.setName("account");

Values vals4=new Values();
ParameterFieldDiscreteValue val4 = new ParameterFieldDiscreteValue();
val4.setValue(account);
vals4.add(val4);

param1.setCurrentValues(vals1);
param2.setCurrentValues(vals2);
param3.setCurrentValues(vals3);
param4.setCurrentValues(vals4);

Fields fields = new Fields();
fields.add(param1);
fields.add(param2);
fields.add(param3);
fields.add(param4);

viewer.setParameterFields(fields);
viewer.refresh();

try
{
viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);
}
catch(Exception e)
{
  System.out.println(e.getMessage());
}
  if (isConnect)
  {
   cm.takeDown();
  }

%>
</body>
</html>

