<script>
/*
'******************************************************************************************************************************************
'Script Author : Mohammad Ashraful Islam
'Creation Date : January 2007
'Page Purpose  : Passes Parameters for Right Share Fraction List report.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

 boolean isConnect = cm.connect();
 if(isConnect==false)
 {
   %>
    <jsp:forward page="ErrorMsg.jsp" >
    <jsp:param name="ErrorTitle" value="Connection Failure" />
    <jsp:param name="ErrorHeading" value="Connection Problem" />
    <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
    </jsp:forward>
  <%
 }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Printed Right Share Fraction List')";
  boolean ub = cm.procedureExecute(ulog);

  if (isConnect)
  {
   cm.takeDown();
  }
%>

<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page
import="java.util.Date,java.text.*,com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
Right Share Fraction List
</title>
</head>
<body bgcolor="#ffffff">
<h1>
</h1>
<%
  String Type = String.valueOf(request.getParameter("righttype"));
  String Order = String.valueOf(request.getParameter("orderby"));
  String DecDate = String.valueOf(request.getParameter("dateselect"));
  //String Div_LT = String.valueOf(request.getParameter("divls"));

  String reporturl = "";

if (Type.equalsIgnoreCase("SMStype"))
  {
   if (Order.equalsIgnoreCase("byname"))
   {
     reporturl = "/CR_Reports/RightFractionList/RightFractionName.rpt";
   }

   else if (Order.equalsIgnoreCase("byfolio"))
   {
     reporturl = "/CR_Reports/RightFractionList/RightFractionFolio.rpt";
   }
  }
  else if (Type.equalsIgnoreCase("CDBLtype"))
  {
   if (Order.equalsIgnoreCase("byname"))
   {
     reporturl = "/CR_Reports/RightFractionList/BORightFractionName.rpt";
   }
   else if (Order.equalsIgnoreCase("byfolio"))
   {
     reporturl = "/CR_Reports/RightFractionList/BORightFractionBo.rpt";
   }
  }

  if (!reporturl.equals("")) {
    session.setAttribute("reportSource", null);
    session.setAttribute("refreshed", null);
  }

Object reportSource = session.getAttribute("reportSource");
if (reportSource == null)
{
try{
  IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
  reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
  session.setAttribute("reportSource", reportSource);
}catch (Exception e) {}
}
  CrystalReportViewer viewer= new CrystalReportViewer();
  try{
    viewer.setReportSource(reportSource);
  }catch (Exception e) {}

  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  //viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setOwnForm(true);

// Passing parameters

ParameterField param1 = new ParameterField();
param1.setReportName("");
param1.setName("datedec");

Values vals1 = new Values();
ParameterFieldDiscreteValue val1 = new ParameterFieldDiscreteValue();
val1.setValue(DecDate);
vals1.add(val1);

try {
 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
 Date d = df.parse(DecDate);
 val1.setValue(d);
 System.out.println("ASHRAF:--"+d);
 vals1.add(val1);
}
catch(Exception e)
{
e.printStackTrace();
}

param1.setCurrentValues(vals1);


Fields fields = new Fields();
fields.add(param1);


viewer.setParameterFields(fields);

//viewer.refresh();

if (session.getAttribute("refreshed") == null)
{
viewer.refresh();
session.setAttribute("refreshed", "true");
}

try
{
viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);
}
catch(Exception e)
{
  e.getMessage();
}
if (isConnect)
  {
   cm.takeDown();
  }
%>
</body>
</html>
