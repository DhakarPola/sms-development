<script>
/*
'******************************************************************************************************************************************
'Script Author : Mohammad Ashraful Islam
'Creation Date : December 2006
'Page Purpose  : Passes Parameters for Dividend Memo Report.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}
 boolean isConnect = cm.connect();
 if(isConnect==false)
 {
   %>
    <jsp:forward page="ErrorMsg.jsp" >
    <jsp:param name="ErrorTitle" value="Connection Failure" />
    <jsp:param name="ErrorHeading" value="Connection Problem" />
    <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
    </jsp:forward>
  <%
 }
%>
<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<%@page
import="java.util.Date,java.text.*,com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
Dividend Memo Report
</title>
</head>
<body bgcolor="#ffffff">
<h1>
</h1>
<%
    //Value of parameter get from page DividendMemoCalc.jsp

  String LetterId = String.valueOf(request.getParameter("letterid"));

  if (String.valueOf(LetterId).equals("null"))
   LetterId = "";

  String reporturl = "";
  String markprinted = "";
  boolean mp;

     String query1 = "SELECT * FROM DIVIDEND_MEMO ORDER BY DM_ID";
     int count=cm.queryExecuteCount(query1);
     if (count == 0){
     %>
      <script language="javascript">
       alert("Information does not exist!");
       location = "<%=request.getContextPath()%>/jsp/Dividend/DividendMemo.jsp";
      </script>
     <%
     }
     else
     {
         //Add the report name
         reporturl ="";
         //reporturl = "/CR_Reports/DividendWarrants/SMS/ByName/Folio_Div_Warrant_&_LetterOrderByName_ParamFolio.rpt";

     }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Printed Dividend Memo Report')";
  boolean ub = cm.procedureExecute(ulog);

  if (!reporturl.equals("")) {
    session.setAttribute("reportSource", null);
  }

Object reportSource = session.getAttribute("reportSource");
if (reportSource == null)
{
try{
  IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
  reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
  session.setAttribute("reportSource", reportSource);
}catch (Exception e) {}
}
  CrystalReportViewer viewer= new CrystalReportViewer();
  try{
    viewer.setReportSource(reportSource);
  }catch (Exception e) {}
  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  //viewer.setPrintMode(CrPrintMode.PDF);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setReuseParameterValuesOnRefresh(true);
// Passing parameters

ParameterField param1 = new ParameterField();
param1.setReportName("");
param1.setName("ddate");

Values vals1 = new Values();
ParameterFieldDiscreteValue val1 = new ParameterFieldDiscreteValue();
val1.setValue(LetterId);
vals1.add(val1);

ParameterField param2=new ParameterField();
param2.setReportName("");
param2.setName("ispresent");

Values vals2=new Values();
ParameterFieldDiscreteValue val2 = new ParameterFieldDiscreteValue();
//val2.setValue(is_present);
vals2.add(val2);

/*try {
 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
 Date d = df.parse(DateDec);
 val1.setValue(d);
 vals1.add(val1);
} catch(Exception e) {
e.printStackTrace();
}*/

param1.setCurrentValues(vals1);
//param2.setCurrentValues(vals2);

Fields fields = new Fields();
fields.add(param1);
//fields.add(param2);

viewer.setParameterFields(fields);
viewer.refresh();

try
{
viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);
}
catch(Exception e)
{
  System.out.println(e.getMessage());
}

  if (isConnect)
  {
   cm.takeDown();
  }

%>
</body>
</html>

