<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : March 2006
'Page Purpose  : Passes Parameters for Right Share Fraction Letter Printing.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

 boolean isConnect = cm.connect();
 if(isConnect==false)
 {
   %>
    <jsp:forward page="ErrorMsg.jsp" >
    <jsp:param name="ErrorTitle" value="Connection Failure" />
    <jsp:param name="ErrorHeading" value="Connection Problem" />
    <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
    </jsp:forward>
  <%
 }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Printed Right Share Fraction Letter')";
  boolean ub = cm.procedureExecute(ulog);

%>

<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<%@page
import="java.util.Date,java.text.*,com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
Bonus Share Letter
</title>
</head>
<body bgcolor="#ffffff">
<h1>
</h1>
<%
  String DateDec = String.valueOf(request.getParameter("dateselect"));
  String RightType = String.valueOf(request.getParameter("righttype"));
  String PrintScope = String.valueOf(request.getParameter("printscope"));
  String WarrantNo = String.valueOf(request.getParameter("swarrantno"));
  String WarrantNoFrom = String.valueOf(request.getParameter("rwarrantno1"));
  String WarrantNoTo = String.valueOf(request.getParameter("rwarrantno2"));

  if (String.valueOf(WarrantNo).equals("null"))
   WarrantNo = "";
  if (String.valueOf(WarrantNoFrom).equals("null"))
   WarrantNoFrom = "";
  if (String.valueOf(WarrantNoTo).equals("null"))
   WarrantNoTo = "";

  String reporturl = "";
  String markprinted = "";
  boolean mp;

  if (RightType.equalsIgnoreCase("SMStype"))
  {
   if (PrintScope.equalsIgnoreCase("single"))
   {
      reporturl = "/CR_Reports/Folio_RightFractionWarrant/Folio_RightFrac.rpt";
   }
   else if (PrintScope.equalsIgnoreCase("range"))
   {
      reporturl = "/CR_Reports/Folio_RightFractionWarrant/Folio_RightFracByRange.rpt";
   }
   else if (PrintScope.equalsIgnoreCase("all"))
   {
      reporturl = "/CR_Reports/Folio_RightFractionWarrant/Folio_RightFracAllNotPrinted.rpt";
   }
  }
  else if (RightType.equalsIgnoreCase("CDBLtype"))
  {
   if (PrintScope.equalsIgnoreCase("single"))
   {
      reporturl = "/CR_Reports/BO_RightFractionWarrant/BO_RightFracByWarrant.rpt";
   }
   else if (PrintScope.equalsIgnoreCase("range"))
   {
      reporturl = "/CR_Reports/BO_RightFractionWarrant/BO_RightFracByRange.rpt";
   }
   else if (PrintScope.equalsIgnoreCase("all"))
   {
      reporturl = "/CR_Reports/BO_RightFractionWarrant/BO_RightFracAllNotPrinted.rpt";
   }
  }

  if (!reporturl.equals("")) {
    session.setAttribute("reportSource", null);
  }

Object reportSource = session.getAttribute("reportSource");
if (reportSource == null)
{
try{
  IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
  reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
  session.setAttribute("reportSource", reportSource);
}catch (Exception e) {}
}
  CrystalReportViewer viewer= new CrystalReportViewer();
  try{
    viewer.setReportSource(reportSource);
  }catch (Exception e) {}
  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  //viewer.setPrintMode(CrPrintMode.PDF);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setReuseParameterValuesOnRefresh(true);

// Passing parameters

ParameterField param1 = new ParameterField();
param1.setReportName("");
param1.setName("ParamDDate");

Values vals1 = new Values();
ParameterFieldDiscreteValue val1 = new ParameterFieldDiscreteValue();
val1.setValue(DateDec);
vals1.add(val1);

ParameterField param2 = new ParameterField();
param2.setReportName("");
param2.setName("ParamWarrantNo");

Values vals2 = new Values();
ParameterFieldDiscreteValue val2 = new ParameterFieldDiscreteValue();
val2.setValue(WarrantNo);
vals2.add(val2);

ParameterField param3 = new ParameterField();
param3.setReportName("");
param3.setName("ParamGreaterThan");

Values vals3 = new Values();
ParameterFieldDiscreteValue val3 = new ParameterFieldDiscreteValue();
val3.setValue(WarrantNoFrom);
vals3.add(val3);

ParameterField param4 = new ParameterField();
param4.setReportName("");
param4.setName("ParamLessThan");

Values vals4 = new Values();
ParameterFieldDiscreteValue val4 = new ParameterFieldDiscreteValue();
val4.setValue(WarrantNoTo);
vals4.add(val4);

try {
 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
 Date d = df.parse(DateDec);
 val1.setValue(d);
 vals1.add(val1);
} catch(Exception e) {
e.printStackTrace();
}

param1.setCurrentValues(vals1);
param2.setCurrentValues(vals2);
param3.setCurrentValues(vals3);
param4.setCurrentValues(vals4);

Fields fields = new Fields();
fields.add(param1);
fields.add(param2);
fields.add(param3);
fields.add(param4);

viewer.setParameterFields(fields);
viewer.refresh();

try
{
viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);

  if (RightType.equalsIgnoreCase("SMStype"))
  {
   if (PrintScope.equalsIgnoreCase("single"))
   {
     markprinted = "call FRACTION_LETTER_RIGHT(" + WarrantNo + ",'" + DateDec + "')";
     mp = cm.procedureExecute(markprinted);
   }
   else if (PrintScope.equalsIgnoreCase("range"))
   {
     markprinted = "call FRACTION_LETTER_RANGE_RIGHT(" + WarrantNoFrom + "," + WarrantNoTo + ",'" + DateDec + "')";
     mp = cm.procedureExecute(markprinted);
   }
   else if (PrintScope.equalsIgnoreCase("all"))
   {
     markprinted = "call FRACTION_LETTER_ALL_RIGHT('" + DateDec + "')";
     mp = cm.procedureExecute(markprinted);
   }
  }
  else if (RightType.equalsIgnoreCase("CDBLtype"))
  {
   if (PrintScope.equalsIgnoreCase("single"))
   {
     markprinted = "call FRACTION_LETTER_BORIGHT(" + WarrantNo + ",'" + DateDec + "')";
     mp = cm.procedureExecute(markprinted);
   }
   else if (PrintScope.equalsIgnoreCase("range"))
   {
     markprinted = "call FRACTION_LETTER_RANGE_BORIGHT(" + WarrantNoFrom + "," + WarrantNoTo + ",'" + DateDec + "')";
     mp = cm.procedureExecute(markprinted);
   }
   else if (PrintScope.equalsIgnoreCase("all"))
   {
     markprinted = "call FRACTION_LETTER_ALL_BORIGHT('" + DateDec + "')";
     mp = cm.procedureExecute(markprinted);
   }
  }
}
catch(Exception e)
{
  System.out.println(e.getMessage());
}

  if (isConnect)
  {
   cm.takeDown();
  }
%>
</body>
</html>

