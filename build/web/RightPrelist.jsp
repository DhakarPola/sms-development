<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : February 2006
'Page Purpose  : Passes Parameters for Right Share Prelist Printing.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

 boolean isConnect = cm.connect();
 if(isConnect==false)
 {
   %>
    <jsp:forward page="ErrorMsg.jsp" >
    <jsp:param name="ErrorTitle" value="Connection Failure" />
    <jsp:param name="ErrorHeading" value="Connection Problem" />
    <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
    </jsp:forward>
  <%
 }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Printed Right Share Prelist')";
  boolean ub = cm.procedureExecute(ulog);

  if (isConnect)
  {
   cm.takeDown();
  }
%>

<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page
import="com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
Right Prelist
</title>
</head>
<body bgcolor="#ffffff">
<h1>
</h1>
<%
  String DivType = String.valueOf(request.getParameter("righttype"));
  String DivOrder = String.valueOf(request.getParameter("orderby"));

  String reporturl = "";


if (DivType.equalsIgnoreCase("SMStype"))
  {
   if (DivOrder.equalsIgnoreCase("byname"))
   {
     reporturl = "/CR_Reports/Folio_RightShare_PreList/Folio_RightPreListOrderByName.rpt";
   }
   else if (DivOrder.equalsIgnoreCase("byfolio"))
   {
     reporturl = "/CR_Reports/Folio_RightShare_PreList/Folio_RightPreListOrderByFolio.rpt";
   }
  }
  else if (DivType.equalsIgnoreCase("CDBLtype"))
  {
   if (DivOrder.equalsIgnoreCase("byname"))
   {
     reporturl = "/CR_Reports/BO_RightShare_PreList/BO_RightPreListOrderByName.rpt";
   }
   else if (DivOrder.equalsIgnoreCase("byfolio"))
   {
     reporturl = "/CR_Reports/BO_RightShare_PreList/BO_RightPreListOrderByBO.rpt";
   }
  }

  if (!reporturl.equals("")) {
    session.setAttribute("reportSource", null);
    session.setAttribute("refreshed", null);
  }

Object reportSource = session.getAttribute("reportSource");
if (reportSource == null)
{
try{
  IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
  reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
  session.setAttribute("reportSource", reportSource);
}catch (Exception e) {}
}
  CrystalReportViewer viewer= new CrystalReportViewer();
  try{
    viewer.setReportSource(reportSource);
  }catch (Exception e) {}

  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setOwnForm(true);

try {
// DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
// Date d = df.parse(DateDec);
} catch(Exception e) {
e.printStackTrace();
}

Fields fields = new Fields();

viewer.setParameterFields(fields);

if (session.getAttribute("refreshed") == null)
{
viewer.refresh();
session.setAttribute("refreshed", "true");
}


try
{
viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);
}
catch(Exception e){System.out.println(e.getMessage());}
%>
</body>
</html>

