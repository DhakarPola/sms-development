<script>
/*
'******************************************************************************************************************************************
'Script Author : Ehsan Al Baqui
'Updated By    : Renad Hakim
'Creation Date : December 2005
'Page Purpose  : View of all the BO Address Import
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm" class="batbsms.conBean"  />
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script type="">location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>BO Address Import</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"><LINK>
<script language="javascript">
<!--
//Displays the previous list of BO_ID
function goPrevious()
{
  var f=document.forms[0];
  if (f.FirstCount.value>1) {
    var FC=parseInt(f.FirstCount.value) - parseInt(f.maxCount.value);
    var LC=parseInt(f.FirstCount.value)-1;
    if (LC<1) LC=1;
    if (FC<1) {
      FC=1;
      LC=f.maxCount.value;
    }
    location = "<%=request.getContextPath()%>/jsp/CDBL/ViewBOAddress.jsp?prev=true&FirstCount="+FC+"&LastCount="+LC;
  }
}
function goNext()
{
  var f=document.forms[0];
  if (f.LastCount.value<f.TotalRows.value) {
    var FC=parseInt(f.LastCount.value)+1;
    var LC=parseInt(f.LastCount.value)+parseInt(f.maxCount.value);
    if (LC>parseInt(f.TotalRows.value)) LC=parseInt(f.TotalRows.value);
    location = "<%=request.getContextPath()%>/jsp/CDBL/ViewBOAddress.jsp?next=true&FirstCount="+FC+"&LastCount="+LC;
  }
}

var FldName = null;
function loadAccept(){
   var TU = "<%=request.getContextPath()%>/jsp/CDBL/ConfirmAcceptBOAddress.jsp";
   var width = 380;
   var height = 130;
   ONW('AcceptDC',TU, width, height, 0, 0, 0);
}

function SubmitTo()
{
  document.forms[0].action = "SaveAcceptBOAddress.jsp";
  document.forms[0].submit();
}

 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
.style19 {
	color: #0A2769;
	font-weight: bold;
}
.style17 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false)
   {
     %>
       <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
       </jsp:forward>
     <%
   }

   String previous = "";
   String next = "";

   previous= request.getParameter("prev");
   next= request.getParameter("next");

   int folio_state=0;
   int COUNT;
   int MAX_RECORDS=20;

   String FirstCount = request.getParameter("FirstCount");
   String LastCount=request.getParameter("LastCount");

   String BO_ID=request.getParameter("searchBOID");
   String name_FirstHolder="";
   String name_Contact_Person="";
   String address="";
   String city = "";
   String postal_code="";
   String phone="";
   String pfBDate = "";

   int total =0;
   String boid = "";
   int intFC=0;
   int intLC=0;
   String TotalCountQuery="";
   String rnum="1";


   TotalCountQuery="SELECT * FROM BOADDRESSTEMP_VIEW";
   int TotalRows=cm.queryExecuteCount(TotalCountQuery);

   String query1;

   if(((BO_ID==null) && (previous==null) && (next==null)) || ((BO_ID!=null) && (previous!=null) && (next!=null)))
   {
     if (BO_ID==null)
     {
       BO_ID="0000000000000000";
     }
     FirstCount="1";
     LastCount=String.valueOf(MAX_RECORDS);
     query1 = "select * from (";
     query1 += "select BO_ID,NAME_FIRSTHOLDER,NAME_CONTACT_PERSON,ADDRESS,CITY,POSTAL_CODE,PHONE,PF_BDATE,rownum rnum";
     query1 += " FROM BOADDRESSTEMP_VIEW ORDER BY BO_ID)";
     query1 += " WHERE (BO_ID>='"+BO_ID+"') and rownum between 1 and "+String.valueOf(MAX_RECORDS);
   } else   {
     query1 = "select * from (";
     query1 += "SELECT BO_ID,NAME_FIRSTHOLDER,NAME_CONTACT_PERSON,ADDRESS,CITY,POSTAL_CODE,PHONE,PF_BDATE,rownum rnum";
     query1 += " FROM BOADDRESSTEMP_VIEW";
     query1 += " WHERE rownum<="+LastCount+" ORDER BY BO_ID)";
     query1 += " where rnum >="+FirstCount;
   }

 COUNT = cm.queryExecuteCount(query1);
 cm.queryExecute(query1);

%>
  <span class="style7">
 <form method="GET" action="ViewBOAddress.jsp">
  <SPAN id="dprint">
    <img name="B3" src="<%=request.getContextPath()%>/images/btnAccept.gif" onclick="loadAccept();" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnAcceptR.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnAccept.gif'">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
  </SPAN>
  <table width="100%" BORDER=1  cellpadding="0" bordercolor="#0044B0" style="border-collapse: collapse">
  <!--DWLayoutTable-->
  <tr><td height="30" bgcolor="#0044B0" class="style17" valign="middle"><center>BO Address Import</center></td></tr>
  <tr>
  	<td>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0">
			<tr bgcolor="#E8F3FD">
				<td width="10%">
                                </td>
                <td width="10%"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goPrevious()" alt=""></td>
                <td width="29%"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNext()" alt=""></td>
                <td width="12%" align="right" height="35" class="style19">BO ID : </td>
                <td width="19%" align="center"><input name="searchBOID" type="text"></td>
                <td width="20%"><img src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="submit()" alt=""></td>
                </table>
	</td>
  </tr>

  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#06689E">
    <td width="20%" class="style9" bgcolor="#0044B0"><div align="center">BO ID</div></td>
    <td width="32%" class="style9" bgcolor="#0044B0"><div align="center">First Holder</div></td>
    <td width="32%" class="style9" bgcolor="#0044B0"><div align="center">Contact Person</div></td>
    <td width="16%" class="style9" bgcolor="#0044B0"><div align="center">PF BDate</div></td>
  </tr>
  <div align="left">
     <%
    folio_state=0;
    String dest=request.getContextPath() + "/jsp/CDBL/EditBOAddressNotReconciled.jsp";;
    while(cm.toNext())
     {
       folio_state++;
       BO_ID=cm.getColumnS("BO_ID");
       name_FirstHolder = cm.getColumnS("NAME_FIRSTHOLDER");
       name_Contact_Person = cm.getColumnS("NAME_CONTACT_PERSON");
       address = cm.getColumnS("ADDRESS");
       city = cm.getColumnS("CITY");
       postal_code = cm.getColumnS("POSTAL_CODE");
       phone = cm.getColumnS("PHONE");
       pfBDate = cm.getColumnDT("PF_BDATE");
       rnum = cm.getColumnS("rnum");

       if(folio_state==1) {
         FirstCount=rnum;
       }
       if (!BO_ID.equals("null"))
       {
         if (String.valueOf(pfBDate).equals("null"))
            pfBDate = "";
         if (String.valueOf(name_FirstHolder).equals("null"))
            name_FirstHolder = "";
         if (String.valueOf(name_Contact_Person ).equals("null"))
           name_Contact_Person = "";

         if (String.valueOf(phone).equals("null"))
           phone = "";
         %>
  </div>
  <tr bgcolor="#E8F3FD">
           <td class="style13"><div align="left" class="style12">
             <div align="center"><a HREF="<%=dest%>?boid=<%=BO_ID%>"><%=BO_ID%></a></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="left">&nbsp;&nbsp;&nbsp;<%=name_FirstHolder%></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="left">&nbsp;&nbsp;&nbsp;<%=name_Contact_Person%></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center"><%=pfBDate%></div>
           </div></td>
      <div align="left" class="style13">
             <%
         }
     }
     LastCount=rnum;
   if (isConnect)
   {
     cm.takeDown();
   }
  %>
     <input  type="hidden" name="prev" value="false">
     <input  type="hidden" name="next" value="false">
     <input type="hidden" name="FirstCount" value="<%=FirstCount%>"/>
     <input type="hidden" name="LastCount" value="<%=LastCount%>"/>
     <input type="hidden" name="TotalRows" value="<%=TotalRows%>"/>
     <input type="hidden" name="maxCount" value="<%=MAX_RECORDS%>"/>
	 </div>
	 </tr>
	 </table>
         </table>
</table>
</form>
  <!--<tr bgcolor="#E8F3FD" >
  <td height="100%" bgcolor="#E8F3FD" ><center>-->

</body>
</html>

