<script>
/*
'******************************************************************************************************************************************
'Script Author : Aminul Islam
'Creation Date : December 2005
'Page Purpose  : Update not-reconciled demat confirmation.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String OriginalFOLIO_NUMB = String.valueOf(request.getParameter("OriginalFOLIO_NUMB"));
  String OriginalCERTIFICATE_NUMB = String.valueOf(request.getParameter("OriginalCERTIFICATE_NUMB"));
  String FOLIO_NUMB = String.valueOf(request.getParameter("txtFOLIO_NUMB"));
  String CERTIFICATE_NUMB = String.valueOf(request.getParameter("txtCERTIFICATE_NUMB"));
  String START_DIST_NUMB = String.valueOf(request.getParameter("START_DIST_NUMB"));

  String SQL = "SELECT * FROM BODEMATCONFIRMATIONTEMP_VIEW WHERE (FOLIO_NUMB='"+FOLIO_NUMB+"') AND (CERTIFICATE_NUMB='"+CERTIFICATE_NUMB+"') AND (START_DIST_NUMB="+START_DIST_NUMB+")";

  int Count = cm.queryExecuteCount(SQL);

  if (Count > 0)
  {
    %>
       <script language="javascript">
         alert("This certificate <%=CERTIFICATE_NUMB%> of folio <%=FOLIO_NUMB%> already exists in current import list!!!");
         history.go(-1);
       </script>
    <%
  } else {
    SQL = "SELECT * FROM ALL_VALID_CERTIFIC WHERE (HOLDER_FOLIO='"+FOLIO_NUMB+"') AND (CERTIFICATE_NO='"+CERTIFICATE_NUMB+"') AND (DIST_FROM="+START_DIST_NUMB+")";
    Count = cm.queryExecuteCount(SQL);
    if (Count>0) {
      SQL = "call UPDATE_BODEMATCONFIRMATIONTEMP('" + FOLIO_NUMB + "', '" + CERTIFICATE_NUMB + "','" + OriginalFOLIO_NUMB + "', '" + OriginalCERTIFICATE_NUMB +"'," + START_DIST_NUMB + ")";
      System.out.println(SQL);
      boolean b = cm.procedureExecute(SQL);
    %>
       <script language="javascript">
          location="<%=request.getContextPath()%>/jsp/CDBL/DCNotReconciled.jsp"
       </script>
    <%
    } else {
    %>
       <script language="javascript">
         alert("This certificate <%=CERTIFICATE_NUMB%> of folio <%=FOLIO_NUMB%> does not exists!!!");
         history.go(-1);
       </script>
    <%
    }
  }

/*
'******************************************************************************************************************************************
'Updated By    : Renad Hakim
'Date          : March 2005
'******************************************************************************************************************************************
*/

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Updated Non-Reconciled Demat Confirmations')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Users/AllUsers.jsp";
</script>
</body>
</html>
