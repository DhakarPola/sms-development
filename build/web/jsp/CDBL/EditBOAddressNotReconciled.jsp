<script>
/*
'******************************************************************************************************************************************
'Script Author : Ehsan Al-Baqui
'Updated By    : Renad Hakim
'Creation Date : December 2005
'Page Purpose  : Edit BO Address Not Reconciled
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean" scope="request"/>
<%@ page language="java" import="java.util.Vector,java.util.Date,java.text.SimpleDateFormat" %>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
{
%>
 <script> top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>
<%
   boolean isConnect = cm.connect();
   if(isConnect==false)
   {
%>
     <jsp:forward page="ErrorMsg.jsp" >
     <jsp:param name="ErrorTitle" value="Connection Failure" />
     <jsp:param name="ErrorHeading" value="Connection Problem" />
     <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
<%
    }
    else
    {

   String BO_ID=request.getParameter("boid");

   String NAME_FIRSTHOLDER="";
   String NAME_CONTACT_PERSON = "";
   String ADDRESS ="";
   String CITY="";
   String POSTAL_CODE="";
   String STATE="";
   String COUNTRY ="";
   String PHONE = "";
   String FAX ="";
   String EMAIL="";
   String TAX_FREE_SHARE = "";
   String LIABLE_TO_TAX="";
   String LIABLE_TAX = "";
   String SPECIAL_TAX = "";
   String RESIDENT = "";
   String NATIONALITY = "";
   String CLASS = "";
   String PF_BDATE="";

   String query = "SELECT BO_ID,NAME_FIRSTHOLDER,NAME_CONTACT_PERSON,ADDRESS,CITY,STATE,POSTAL_CODE,COUNTRY,PHONE,FAX,";
   query += "EMAIL,TAX_FREE_SHARE,LIABLE_TO_TAX,SPECIAL_TAX,RESIDENT,NATIONALITY,CLASS,TO_CHAR(PF_BDATE,'DD/MM/YYYY') AS strPF_BDATE";
   query += " FROM BOADDRESSTEMP WHERE BO_ID='" +BO_ID+ "'";

   cm.queryExecute(query);

   while(cm.toNext())
   {
     NAME_FIRSTHOLDER=cm.getColumnS("NAME_FIRSTHOLDER");
     NAME_CONTACT_PERSON=cm.getColumnS("NAME_CONTACT_PERSON");
     ADDRESS=cm.getColumnS("ADDRESS");
     CITY=cm.getColumnS("CITY");
     STATE=cm.getColumnS("STATE");
     POSTAL_CODE=cm.getColumnS("POSTAL_CODE");
     COUNTRY=cm.getColumnS("COUNTRY");
     PHONE=cm.getColumnS("PHONE");
     FAX=cm.getColumnS("FAX");
     EMAIL=cm.getColumnS("EMAIL");
     TAX_FREE_SHARE=cm.getColumnS("TAX_FREE_SHARE");
     LIABLE_TO_TAX=cm.getColumnS("LIABLE_TO_TAX");
     SPECIAL_TAX=cm.getColumnS("SPECIAL_TAX");
     RESIDENT=cm.getColumnS("RESIDENT");
     NATIONALITY=cm.getColumnS("NATIONALITY");
     CLASS=cm.getColumnS("CLASS");
     PF_BDATE=cm.getColumnS("strPF_BDATE");
   }

   if ((NAME_CONTACT_PERSON==null) || (NAME_CONTACT_PERSON.equals("null")))
   {
     NAME_CONTACT_PERSON="";
   }

   if ((STATE==null) || (STATE.equals("null")))
   {
     STATE="";
   }

   if ((NATIONALITY==null) || (NATIONALITY.equals("null")))
  {
     NATIONALITY="Bangladeshi";
  }

  if ((STATE==null) || (STATE.equals("null")))
   {
     STATE="";
   }

   if ((LIABLE_TO_TAX==null) || (LIABLE_TO_TAX.equals("null")))
   {
     LIABLE_TO_TAX="F";
   }


   if ((TAX_FREE_SHARE==null) || (TAX_FREE_SHARE.equals("null")))
   {
     TAX_FREE_SHARE="0";
   }

   if ((SPECIAL_TAX==null) || (SPECIAL_TAX.equals("null")))
   {
     SPECIAL_TAX="F";
   }

   if ((RESIDENT==null) || (RESIDENT.equals("null")))
   {
     RESIDENT="T";
   }

   if ((CLASS==null) || (CLASS.equals("null")))
   {
     CLASS="";
   }

%>
<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<title>Edit BO Address Form</title>
<script language="javascript">

//Execute while click on Submit
function SubmitThis()
{
  if (confirm("Do you want to submit?"))
  {
    document.forms[0].submit();
  }
}

function closeForm()
{
  location = "<%=request.getContextPath()%>/jsp/CDBL/ViewBOAddress.jsp"
}
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
.style18 {font-family: Arial, Helvetica, sans-serif; font-size: 11px; }
.style19 {font-size: 11px}
.style20 {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
	color: #FFFFFF;
}
-->
</style>
</head>

   <body TEXT="000000" BGCOLOR="#ffffff">
  <form action="SaveBOAddressNotReconciled.jsp" method="post">
  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Edit BO Address</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <div align="left">
  <table width="80%" border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;BO ID</div></th>
        <td width="1%">:</td>
        <td width="55%">
          <div align="left" class="style8">&nbsp;<b><%=BO_ID%></b></div>
        </td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;First Holder Name</div></th>
        <td width="1%">:</td>
        <td width="55%">
          <div align="left" class="style8">&nbsp;<b><%=NAME_FIRSTHOLDER%></b></div>
        </td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Contact Person</div></th>
        <td width="1%">:</td>
        <td width="55%">
          <div align="left" class="style8">&nbsp;<b><%=NAME_CONTACT_PERSON%></b></div>
        </td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Address</div></th>
        <td width="1%">:</td>
        <td width="55%">
          <div align="left" class="style8">&nbsp;<b><%=ADDRESS%></b></div>
        </td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;City</div></th>
        <td width="1%">:</td>
        <td width="55%">
          <div align="left" class="style8">&nbsp;<b><%=CITY%></b></div>
        </td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Postal Code</div></th>
        <td width="1%">:</td>
        <td width="55%">
          <div align="left" class="style8">&nbsp;<b><%=POSTAL_CODE%></b></div>
        </td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Phone</div></th>
        <td width="1%">:</td>
        <td width="55%">
          <div align="left" class="style8">&nbsp;<b><%=PHONE%></b></div>
        </td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Tax Free Share</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          &nbsp;<input type="text" name="TAX_FREE_SHARE" class="SL2TextField" onkeypress="keypressOnNumberFld()" value="<%=TAX_FREE_SHARE%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Liable to Tax</div></th>
        <td width="1%">:</td>
        <td width="55%">
        <%if (LIABLE_TO_TAX.equals("T")) {%>
        <input name="LIABLE_TO_TAX" type="checkbox" value="T"  checked></td>
        <% } else {%>
        <input name="LIABLE_TO_TAX" type="checkbox" value="T"></td>
        <%} %>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Special Tax</div></th>
        <td width="1%">:</td>
        <td width="55%">
         <% if (SPECIAL_TAX.equals("T"))
         {%>
        <input name="SPECIAL_TAX" type="checkbox" value="T"  checked></td>
        <% } else
        {%>
        <input name="SPECIAL_TAX" type="checkbox" value="T"></td>
        <%} %>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Resident</div></th>
        <td width="1%">:</td>
        <td width="55%">
         <%if (RESIDENT.equals("T")) {%>
        <input name="RESIDENT" type="checkbox" value="T" checked></td>
        <% } else {%>
        <input name="RESIDENT" type="checkbox" value="T"></td>
        <%} %>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Nationality</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          &nbsp;<input type="text" name="NATIONALITY" class="SL2TextField" value="<%=NATIONALITY%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Category</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          &nbsp;<select name="CLASS" class="SL1TextFieldListBox" >
          <%
                     String catVal="";
                     cm.queryExecute("SELECT CATEGORY,CODE FROM CATEGORY_VIEW");
                     String cat="";
                     while(cm.toNext()){
                        cat=cm.getColumnS("CATEGORY");
                        catVal=cm.getColumnS("CODE");
                        if(catVal.equals(CLASS)){
                           %>
                           <option value="<%= catVal %>" selected="selected"><%= cat%></option>
                         <%
                        }else {
                          %>
                          <option value="<%= catVal %>" ><%= cat%></option>
                          <%
                        }
                  }
                  %>
                  </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;PF Business Date</div></th>
        <td width="1%">:</td>
        <td width="55%">
          <div align="left" class="style8">&nbsp;<b><%=PF_BDATE%></b></div>
        </td>
      </tr>

  <br>
      <table width="100%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="50%" scope="row">
            <div align="right">
 	      <br>
              <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onClick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
  	      <br>&nbsp;
            </div></th>
        <td width="50%">
 	      <br>
	        <img name="B2" src="<%=request.getContextPath()%>/images/btnClose.gif"  onClick="closeForm()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
 	      <br>&nbsp;
        </td>
      </tr>
</table>
        </tr>
       </table>
      <input name="boid" type="hidden" value="<%=BO_ID%>">
      </form>
     </body>
</html>

<%
   if (isConnect)
   {
     cm.takeDown();
   }
}%>




