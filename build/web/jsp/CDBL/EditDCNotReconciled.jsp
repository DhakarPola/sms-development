<script>
/*
'******************************************************************************************************************************************
'Script Author : Aminul Islam
'Updated By    : Renad Hakim
'Creation Date : December 2005
'Page Purpose  : Edit not-reconciled demat confirmation
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean" scope="request"/>
<%@ page language="java" import="java.util.Vector,java.util.Date,java.text.SimpleDateFormat" %>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
%>
     <jsp:forward page="ErrorMsg.jsp" >
     <jsp:param name="ErrorTitle" value="Connection Failure" />
     <jsp:param name="ErrorHeading" value="Connection Problem" />
     <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
<%
    }
    else
    {

   String FOLIO_NUMB = request.getParameter("fno");
   String CERTIFICATE_NUMB = request.getParameter("certno");
   String dstart=request.getParameter("dstart");
   String ChangedFOLIO_NUMB = request.getParameter("cfno");
   String ChangedCERTIFICATE_NUMB = request.getParameter("ccertno");
   if (ChangedFOLIO_NUMB==null)
   {
     ChangedFOLIO_NUMB="";
     ChangedCERTIFICATE_NUMB="";
   }

   String SEQ_NUMB="";
   String DRF_NUMB="";
   String BOID = "";
   String SHR_NAME ="";
   String START_DIST_NUMB="";
   String END_DIST_NUMB="";
   String SHR_Total = "";
   String PF_BDATE="";

   String query1 = "SELECT BOID,FOLIO_NUMB,CERTIFICATE_NUMB,SEQ_NUMB,DRF_NUMB,SHR_NAME,";
   query1 += "START_DIST_NUMB,END_DIST_NUMB,((END_DIST_NUMB - START_DIST_NUMB)+1) AS SHR_Total,";
   query1 += "PF_BDATE FROM BODEMATCONFIRMATIONTEMP_VIEW WHERE FOLIO_NUMB = '" + FOLIO_NUMB + "'";
   query1 += " AND CERTIFICATE_NUMB = '"+CERTIFICATE_NUMB+ "' AND START_DIST_NUMB = "+dstart;

   cm.queryExecute(query1);

   while(cm.toNext())
   {
     SEQ_NUMB=cm.getColumnS("SEQ_NUMB");
     DRF_NUMB=cm.getColumnS("DRF_NUMB");
     BOID = cm.getColumnS("BOID");
     SHR_NAME =cm.getColumnS("SHR_NAME");
     START_DIST_NUMB=cm.getColumnS("START_DIST_NUMB");
     END_DIST_NUMB=cm.getColumnS("END_DIST_NUMB");
     SHR_Total = cm.getColumnS("SHR_Total");
     PF_BDATE=cm.getColumnS("PF_BDATE");
   }
%>
<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<title>Edit Demat Confirmation - NotReconciled</title>
<script language="javascript">
<!--

//Execute while click on Submit
function SubmitThis() {
  if (confirm("Do you want to Update Demat Imformation?")){
    document.forms[0].submit();
  }
}

function closeForm()
{
  location = "<%=request.getContextPath()%>/jsp/CDBL/DCNotReconciled.jsp"
}
-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
.style18 {font-family: Arial, Helvetica, sans-serif; font-size: 11px; }
.style19 {font-size: 11px}
.style20 {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
	color: #FFFFFF;
}
-->
</style>
</head>
<body TEXT="000000" BGCOLOR="#ffffff">
<form action="SaveDCNotReconciled.jsp" method="post">
  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Edit Demat Confirmation - Not Reconciled</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <div align="left">
  <table width="80%" border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Sequence Number</div></th>
        <td width="1%">:</td>
        <td width="55%">
          <div align="left" class="style8">&nbsp;<b><%=SEQ_NUMB%></b></div>
        </td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;DRF Number</div></th>
        <td width="1%">:</td>
        <td width="55%">
          <div align="left" class="style8">&nbsp;<b><%=DRF_NUMB%></b></div>
        </td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;BO ID</div></th>
        <td width="1%">:</td>
        <td width="55%">
          <div align="left" class="style8">&nbsp;<b><%=BOID%></b></div>
        </td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Folio No.</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          &nbsp;<input type="text" name="txtFOLIO_NUMB" class="SL2TextField" value="<%=FOLIO_NUMB%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Shareholder Name</div></th>
        <td width="1%">:</td>
        <td width="55%">
          <div align="left" class="style8">&nbsp;<b><%=SHR_NAME%></b></div>
        </td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Certificate No.</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          &nbsp;<input type="text" name="txtCERTIFICATE_NUMB" class="SL2TextField" value="<%=CERTIFICATE_NUMB%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Distinction From</div></th>
        <td width="1%">:</td>
        <td width="55%">
          <div align="left" class="style8">&nbsp;<b><%=START_DIST_NUMB%></b></div>
        </td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Distinction To</div></th>
        <td width="1%">:</td>
        <td width="55%">
          <div align="left" class="style8">&nbsp;<b><%=END_DIST_NUMB%></b></div>
        </td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Total Shares</div></th>
        <td width="1%">:</td>
        <td width="55%">
          <div align="left" class="style8">&nbsp;<b><%=SHR_Total%></b></div>
        </td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;PF Business Date</div></th>
        <td width="1%">:</td>
        <td width="55%">
          <div align="left" class="style8">&nbsp;<b><%=PF_BDATE%></b></div>
        </td>
      </tr>
  <br>
      <table width="100%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="50%" scope="row">
            <div align="right">
 	      <br>
              <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onClick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
  	      <br>&nbsp;
            </div></th>
        <td width="50%">
 	      <br>
	        <img name="B2" src="<%=request.getContextPath()%>/images/btnClose.gif"  onClick="closeForm()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
 	      <br>&nbsp;
        </td>
      </tr>
</table>
          </tr>
      </table></td>
    </tr>
  </table>
  <input type="hidden" name="OriginalFOLIO_NUMB" value="<%=FOLIO_NUMB%>"/>
  <input type="hidden" name="OriginalCERTIFICATE_NUMB" value="<%=CERTIFICATE_NUMB%>"/>
  <input type="hidden" name="START_DIST_NUMB" value="<%=START_DIST_NUMB%>"/>
  <input type="hidden" name="END_DIST_NUMB" value="<%=END_DIST_NUMB%>"/>
  <input type="hidden" name="ChangedFOLIO_NUMB" value="<%=ChangedFOLIO_NUMB%>"/>
  <input type="hidden" name="ChangedCERTIFICATE_NUMB" value="<%=ChangedCERTIFICATE_NUMB%>"/>
</form>
</body>
</html>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
}%>







