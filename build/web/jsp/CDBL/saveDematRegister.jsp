<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : August 2007
'Page Purpose  : update register info.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update Register</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String refNo = String.valueOf(request.getParameter("refNo"));
  String confirmshare = String.valueOf(request.getParameter("sharecon"));
  String confirmdate = String.valueOf(request.getParameter("condate"));
  String dcr = String.valueOf(request.getParameter("dcr"));
  String rejshare = String.valueOf(request.getParameter("sharerej"));
  String rejDate = String.valueOf(request.getParameter("rejdate"));
  String reason = String.valueOf(request.getParameter("reason"));
  String dtf = String.valueOf(request.getParameter("dtf"));
  String remarks = String.valueOf(request.getParameter("remarks"));

  if(String.valueOf(confirmshare).equals("null"))
      confirmshare="";
    if(String.valueOf(confirmdate).equals("null"))
    confirmdate="";
    if(String.valueOf(dcr).equals("null"))
    dcr="F";
    if(String.valueOf(rejshare).equals("null"))
    rejshare="";
    if(String.valueOf(rejDate).equals("null"))
    rejDate="";
    if(String.valueOf(reason).equals("null"))
    reason="";
    if(String.valueOf(dtf).equals("null"))
    dtf="";
    if(String.valueOf(remarks).equals("null"))
    remarks="";

     if(String.valueOf(dcr).equals("on"))
     dcr="T";
     else
     dcr="F";

    dcr=dcr.replace("'","''");
    reason=reason.replace("'","''");
    dtf=dtf.replace("'","''");
    remarks=remarks.replace("'","''");

  /* System.out.println("share_con= "+confirmshare);
    System.out.println("con_date= "+confirmdate);
    System.out.println("dcr= "+dcr);
    System.out.println("rej_qty= "+rejshare);
    System.out.println("rej_date= "+rejDate);
    System.out.println("reason= "+reason);
    System.out.println("dtf= "+dtf);
    System.out.println("remarks= "+remarks);*/

  String uregister = "call UPDATE_DEMAT_REGISTER('" + refNo + "', '" + confirmshare + "', '" + confirmdate + "', '" + dcr + "', '" + rejshare + "', '" + rejDate + "','" + reason + "','" + dtf + "','" + remarks + "')";
  //System.out.println("uregister= "+uregister);
  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Updated register Information')";
  try
  {
    boolean b=cm.procedureExecute(uregister);
    boolean ub = cm.procedureExecute(ulog);
  }
  catch(Exception e){}

   if (isConnect)
   {
     try
     {
       cm.takeDown();
     }catch(Exception e){}
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/CDBL/DematRegisters.jsp";
</script>
</body>
</html>
