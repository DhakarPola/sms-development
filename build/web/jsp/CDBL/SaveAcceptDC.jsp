<script>
/*
'******************************************************************************************************************************************
'Script Author : Aminul Islam
'Creation Date : December 2005
'Page Purpose  : add all the demat confirmation import [if all are reconciled] in to demat confirmation table.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   String TotalCountQuery="";
   String rnum="1";

/*   TotalCountQuery="SELECT DC. BOID, DC.SHR_NAME, DC.FOLIO_NUMB, DC.CERTIFICATE_NUMB, DC.START_DIST_NUMB,";
   TotalCountQuery += "DC.END_DIST_NUMB, DC.TOTAL, DC.PF_BDATE, rownum rnum";
   TotalCountQuery += " FROM BODEMATCONFIRMATIONTEMP_VIEW DC";
   TotalCountQuery += " WHERE NOT Exists (SELECT * FROM ALL_VALID_CERTIFIC AVC WHERE ((AVC.HOLDER_FOLIO= DC.FOLIO_NUMB)";
   TotalCountQuery += " AND (AVC.CERTIFICATE_NO= DC.CERTIFICATE_NUMB) AND (AVC.DIST_FROM= DC.START_DIST_NUMB)))";
*/

   TotalCountQuery = "SELECT DC. BOID, DC.SHR_NAME, DC.FOLIO_NUMB, DC.CERTIFICATE_NUMB, DC.START_DIST_NUMB, DC.END_DIST_NUMB, DC.TOTAL, DC.PF_BDATE FROM BODEMATCONFIRMATIONTEMP_VIEW DC WHERE (NOT Exists (SELECT * FROM ALL_VALID_CERTIFIC AVC WHERE ((AVC.HOLDER_FOLIO= DC.FOLIO_NUMB) AND (AVC.CERTIFICATE_NO= DC.CERTIFICATE_NUMB)))) ORDER BY DC.FOLIO_NUMB,DC.CERTIFICATE_NUMB";

   int Count=cm.queryExecuteCount(TotalCountQuery);

  if (Count > 0)
  {
        System.out.println("Count = " + Count);
    %>
       <script language="javascript">
         alert("Reconciliation is not complete.\nIn order to accept the Demat Confirmations Import,\nall the certificates should be reconciled first !!!");
         history.go(-1);
       </script>
    <%
  } else {
      String SQL = "call ADD_BODEMATCONFIRMATION()";
      System.out.println(SQL);
      cm.connAutoCommit(false);
      boolean b = cm.procedureExecute(SQL);
      //make all the certificates invalid
      //deduct cert total from the folio total
      //delete from the BODEMATCONFIRMATIONTEMP
      cm.connCommit();
      cm.connAutoCommit(true);
    %>
       <script language="javascript">
          location="<%=request.getContextPath()%>/jsp/CDBL/DematConfirmation.jsp"
       </script>
    <%
  }

/*
'******************************************************************************************************************************************
'Updated By    : Renad Hakim
'Date          : March 2005
'******************************************************************************************************************************************
*/

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Saved Demat Confirmations')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/CDBL/DematConfirmation.jsp";
</script>
</body>
</html>

