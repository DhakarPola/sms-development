<script>
/*
'******************************************************************************************************************************************
'Script Author : Md.Kamruzzaman
'Creation Date : June 2007
'Page Purpose  : View Register Details for a Demat Register
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.io.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*"%>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<jsp:useBean id="util" class="batbsms.Utility"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<%if (String.valueOf(session.getAttribute("UserName")).equals("null")) {%>
<script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%}%>
<html>
<head>
<script language="javascript">
<!--
var totalshare=0;
function CLClose()
{
   history.back();
}

function closeDCImport()
{
  history.go(-1);
}

function formconfirm()
{
  if (confirm("Do you want to Save the Demat Register Information?"))
  {
    return true;
  }
}

function SubmitThis() {
  count = 0;
  var conshare=document.forms[0].sharecon.value;
  if(conshare=='')
  conshare='0';
  var conshareI=parseInt(conshare);
  var tshareI=parseInt(totalshare);
  var rejshare=tshareI-conshareI;
  if(conshareI>tshareI)
  {
    count++;
    nArray[count]='Confirm share can not more than Total share.';
  }
  BlankValidate('sharecon','- Share Confirm (Option must be entered)');
  BlankValidate('condate','- Confirm Date (Option must be entered)');
  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}

function calculate()
{
  var conshare=document.forms[0].sharecon.value;
  if(conshare=='')
  conshare='0';
  var conshareI=parseInt(conshare);
  var tshareI=parseInt(totalshare);
  var rejshare=tshareI-conshareI;
  if(conshareI>tshareI)
  {
    alert("Confirm share can not more than Total share.");
  }
    document.forms[0].sharerej.value=rejshare;
}

//-->
</script>
<%
  boolean isConnect = cm.connect();
  if (isConnect == false) {
%>
<jsp:forward page="ErrorMsg.jsp">
  <jsp:param name="ErrorTitle" value="Connection Failure"/>
  <jsp:param name="ErrorHeading" value="Connection Problem"/>
  <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator"/>
</jsp:forward>
<%
  }
  String refNo = request.getParameter("refNo");

  String req_Date="";
  String shareholderName="";
  String folio_No="";
  String boid="";
  String de_Num="";
  String total_Share="";
  String Rec_Date="";
  String ShareholderName="";
  String share_con="";
  String con_date="";
  String dcr="";
  String rej_qty="";
  String rej_date="";
  String reason="";
  String dtf="";
  String remarks="";

  String query="select * from DEMAT_REGISTER_VIEW where SN='" + refNo + "'";
 //System.out.println("query= "+query);
  try
  {
    cm.queryExecute(query);
    cm.toNext();
    Rec_Date=cm.getColumnDT("REC_DATE");
    ShareholderName = cm.getColumnS("SH_NAME");
    folio_No=cm.getColumnS("FOLIO_NO");
    boid = cm.getColumnS("BOID");
    de_Num=cm.getColumnS("DREQ_NO");
    total_Share=cm.getColumnS("TOTAL_SHARE");
    share_con=cm.getColumnS("SHARE_CONFIRM");
    con_date=cm.getColumnDT("CON_DATE");
    dcr=cm.getColumnS("DCR");
    rej_qty=cm.getColumnS("REJ_QTY");
    rej_date=cm.getColumnDT("REJ_DATE");
    reason=cm.getColumnS("REJ_REASON");
    dtf=cm.getColumnS("DTF");
    remarks=cm.getColumnS("REMARKS");

    %>
    <script language="javascript">
    totalshare=<%=total_Share%>;
    </script>
    <%

    if(Rec_Date.equals("null"))
    Rec_Date="";
    if(ShareholderName.equals("null"))
    ShareholderName="";
    if(folio_No.equals("null"))
    folio_No="";
    if(boid.equals("null"))
    boid="";
    if(de_Num.equals("null"))
    de_Num="";
    if(total_Share.equals("null"))
    total_Share="0";
    if(String.valueOf(share_con).equals("null"))
      share_con="";
    if(String.valueOf(con_date).equals("null"))
    con_date="";
    if(String.valueOf(dcr).equals("null"))
    dcr="F";
    if(String.valueOf(rej_qty).equals("null"))
    rej_qty="";
    if(String.valueOf(rej_date).equals("null"))
    rej_date="";
    if(String.valueOf(reason).equals("null"))
    reason="";
    if(String.valueOf(dtf).equals("null"))
    dtf="";
    if(String.valueOf(remarks).equals("null"))
    remarks="";

    dcr=dcr.replace("''","'");
    reason=reason.replace("''","'");
    dtf=dtf.replace("''","'");
    remarks=remarks.replace("''","'");
  }
  catch(Exception e){}



    /*System.out.println("share_con= "+share_con);
    System.out.println("con_date= "+con_date);
    System.out.println("dcr= "+dcr);
    System.out.println("rej_qty= "+rej_qty);
    System.out.println("rej_date= "+rej_date);
    System.out.println("reason= "+reason);
    System.out.println("dtf= "+dtf);
    System.out.println("remarks= "+remarks);*/

%>
<style type="text/css">
  <!--
    body {
    background-color: #FFFFFF;
    }
    body,td,th {
    color: #000000;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
    }
  -->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Demat Register Details</title>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<style type="text/css">
  <!--
    .style7 {
    color: #FFFFFF;
    font-weight: bold;
    font-size: 13px;
    }
    .style8 {color: #0A2769;
            font-weight: bold;}
    .style9 {color: #0A2769; font-weight: bold; }
  -->
</style>
</head>
<body TEXT="000000" BGCOLOR="FFFFFF">
<form action="saveDematRegister.jsp" method="POST" name="FileForm">
  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Demat Register Details</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Reference Number</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=refNo%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Recivied Date</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=Rec_Date%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Shareholder Name</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=ShareholderName%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Folio No</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=folio_No%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;BOID</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=boid%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Demat Request No</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=de_Num%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Total Share</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=total_Share%></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Share Confirm</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="sharecon" class="SL2TextField" value="<%=share_con%>" onkeypress="keypressOnNumberFld()" onkeyup="calculate()">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Confirm Date</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <input name="condate" type=text id="condate" value="<%=con_date%>" maxlength="10" class="SL2TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('condate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;DCR</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="checkbox" name="dcr" <%if(dcr.equals("T")){%> checked="checked"<%}%>/>
        </div></td>
      </tr>
       <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Rejected Quantity</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="sharerej" class="SL2TextField" value="<%=rej_qty%>" onfocus="this.blur()">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Rejected Date</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <input name="rejdate" type=text id="rejdate" value="<%=con_date%>" maxlength="10" class="SL2TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('rejdate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
      <tr>
        <td class="style8" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;Reason</td>
        <td valign="top">:</td>
        <td><textarea name="reason" class="ML9TextField"><%=reason%></textarea></td>
        </tr>
        <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;DTF</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="dtf" class="SL2TextField" value="<%=dtf%>"/>
        </div></td>
      </tr>
      <tr>
        <td class="style8" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;Remarks</td>
        <td valign="top">:</td>
        <td><textarea name="remarks" class="ML9TextField"><%=remarks%></textarea></td>
        </tr>
      </table>
      <br>
      </div>
      <table width="100%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="50%" scope="row">
            <div align="right">
 	      <br>
              <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onClick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
  	      <br>&nbsp;
            </div></th>
        <td width="50%">
 	      <br>
              <img name="B2" src="<%=request.getContextPath()%>/images/btnClose.gif"  onClick="closeDCImport()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
 	      <br>&nbsp;
        </td>
      </tr>
</table>
<input type="hidden" name="refNo" value="<%=refNo%>"/>
</form>
<%
  if (isConnect) {
    try
    {
      cm.takeDown();
    }
    catch(Exception e){}
  }
%>
</body>
</html>

