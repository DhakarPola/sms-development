<script>
/*
'******************************************************************************************************************************************
'Script Author : Kamruzzaman
'Creation Date : July 2007
'Page Purpose  : View of all BOAddress Import
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>BO Address View</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

<!--
//Displays the previous list of BOID

function goPrevious()
{
  var f=document.forms[0];
  var FCT=parseInt(f.FirstCount.value);
  var LCT=parseInt(f.LastCount.value);
  var chunk=parseInt(f.maxCount.value);
  var totalRows=parseInt(f.TotalRows.value);
  var FC=FCT-chunk;
  var LC=FC+chunk-1;
  if(LC>totalRows)
  LC=totalRows;
  if(LC<chunk)
  LC=chunk;
  if(FC<=0)
  FC=1;
    location = "<%=request.getContextPath()%>/jsp/CDBL/viewAddress.jsp?prev=true&FirstCount="+FC+"&LastCount="+LC;
  }

  function submitNow()
  {
    document.forms[0].submit();
   }


function goNext()
{
  var f=document.forms[0];
  var FCT=parseInt(f.FirstCount.value);
  var LCT=parseInt(f.LastCount.value);
  var chunk=parseInt(f.maxCount.value);
  var totalRows=parseInt(f.TotalRows.value);
  var FC=FCT;
  var LC=LCT+chunk;
  if((FCT+chunk)<totalRows)
  FC=FCT+chunk;
  if(LC>totalRows)
  {
    LC=totalRows;
  }
  location = "<%=request.getContextPath()%>/jsp/CDBL/viewAddress.jsp?next=true&FirstCount="+FC+"&LastCount="+LC;
}

//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
.style19 {
	color: #0A2769;
	font-weight: bold;
}
.style17 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   String previous = "";
   String next = "";

   previous= request.getParameter("prev");
   next= request.getParameter("next");


   int folio_state=0;
   int COUNT=0;
   int MAX_RECORDS=20;
   String BO_ID=request.getParameter("searchB0ID");
   String FirstCount = request.getParameter("FirstCount");
   String LastCount=request.getParameter("LastCount");
   String pfdate1 = "";


   if(FirstCount==null)
   FirstCount="1";
   if(LastCount==null)
   LastCount="20";

   String BO_name = "";
   String pfBDate = "";
   int total = 0;
   String boid = "";
   int intFC=0;
   int intLC=0;
   String TotalCountQuery="";
   int TotalRows=0;

   TotalCountQuery="select * from BOHOLDINGONLY_VIEW";
   try
   {
      TotalRows=cm.queryExecuteCount(TotalCountQuery);
   }
   catch(Exception e){}

   String query1="";



   if(BO_ID==null)
   {
     query1 = "select * from(";
     query1 += "select BOID, BOSHORTNAME,CURRBAL,to_char(PF_BDATE,'dd/MM/YYYY') AS PFBDATE,rownum rnum";
     query1 += " FROM BOHOLDINGONLY_VIEW WHERE rownum<="+LastCount+" ORDER BY BOID)";
     query1 += " where rnum >="+FirstCount;
   }
   else
   {
   query1 = "select * from (";
   query1 += "select BOID, BOSHORTNAME,CURRBAL,to_char(PF_BDATE,'dd/MM/YYYY') AS PFBDATE,rownum rnum";
   query1 += " FROM BOHOLDINGONLY_VIEW ORDER BY BOID)";
   query1 += " WHERE (BOID>='"+BO_ID+"') and rownum between 1 and "+String.valueOf(MAX_RECORDS);
   }

try
{
  cm1.queryExecute(query1);
  while(cm1.toNext())
  {
    pfdate1 = cm1.getColumnS("PFBDATE");
  }
}
  catch(Exception e){}

 if (String.valueOf(pfdate1).equals("null"))
  pfdate1 = "";

try
{
  COUNT = cm.queryExecuteCount(query1);
  cm.queryExecute(query1);

  %>
  <span class="style7">
    <form method="GET" action="viewAddress.jsp">
    <SPAN id="dprint">
      <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
    </SPAN>
    <table width="100%" BORDER=1  cellpadding="0"  bordercolor="#0044B0" style="border-collapse: collapse">

      <!--DWLayoutTable-->
      <tr><td height="30" bgcolor="#0044B0" class="style17" valign="middle"><center>Imported BOAddress on Date (<%=pfdate1%>)</center></td></tr>
      <tr>
        <td>
          <table width="100%"  border="0" cellpadding="0" cellspacing="0" bordercolor="#0044B0">
            <tr bgcolor="#E8F3FD">
              <td width="10%"></td>
              <td width="10%"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goPrevious()"></td>
                <td width="29%"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNext()"></td>
                  <td width="12%" align="right" height="35" class="style19">BO ID:</td>
                  <td width="19%" align="center"><input name="searchB0ID" type="text"></td>
                    <td width="20%"><img src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="submitNow()"></td>
            </tr>
          </table>
                    </td>
      </tr>
    </table>
    <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse"  bordercolor="#0044B0">
      <tr >
        <td width="25%" class="style9" bgcolor="#0044B0"><div align="center">BO ID</div></td>
        <td width="50%" class="style9" bgcolor="#0044B0"><div align="center">BO Holder Name</div></td>
        <td width="25%" class="style9" bgcolor="#0044B0"><div align="center">Shares</div></td>
      </tr>

      <div align="left">
      <%
      folio_state=0;
      String dest=request.getContextPath()+"/jsp/CDBL/EditBOAddress.jsp";
      while(cm.toNext())
      {
        folio_state++;
        BO_ID=cm.getColumnS("BOID");
        BO_name = cm.getColumnS("BOSHORTNAME");
        total = cm.getColumnI("CURRBAL");
        pfBDate = cm.getColumnS("PFBDATE");
        //rnum = cm.getColumnS("rnum");
        if(folio_state==1) {
          //FirstCount=rnum;
        }
        if (!BO_ID.equals("null"))
        {
          if (String.valueOf(BO_name).equals("null"))
          BO_name = "";
          %>
          </div>
          <tr bgcolor="#E8F3FD">
            <td class="style13"><div align="left" class="style12">
              <div align="center"><a HREF="<%=dest%>?boid=<%=BO_ID%>"><%=BO_ID%></div>
              </div></td>
              <td class="style13"><div align="center" class="style12">
                <div align="left">&nbsp;&nbsp;&nbsp;<%=BO_name%></div>
</div></td>
<td class="style13"><div align="center" class="style12">
  <div align="center"><%=total%>&nbsp;</div>
</div></td>
          </tr>
          <div align="left" class="style13">
          <%
          }
        }
      }
      catch (Exception e){}
     //LastCount=rnum;
   if (isConnect)
   {
     try
     {
       cm.takeDown();
       cm1.takeDown();
     }
     catch(Exception e){}
   }
  %>
  </div>
     <input  type="hidden" name="prev" value="false">
     <input  type="hidden" name="next" value="false">
     <input type="hidden" name="FirstCount" value="<%=FirstCount%>"/>
     <input type="hidden" name="LastCount" value="<%=LastCount%>"/>
     <input type="hidden" name="TotalRows" value="<%=TotalRows%>"/>
     <input type="hidden" name="maxCount" value="<%=MAX_RECORDS%>"/>
  </form>
  <tr bgcolor="#E8F3FD">
    <td height="100%" bgcolor="#E8F3FD"><center>
</body>
</html>
