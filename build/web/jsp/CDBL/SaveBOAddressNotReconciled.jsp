<script>
/*
'******************************************************************************************************************************************
'Script Author : Ehsan Al-Baqui
'Creation Date : December 2005
'Page Purpose  : Update not-reconciled BO Address.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))

{
%>
 <script> top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%

}%>

<html>
<head>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false)
   {
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

  String BO_ID=request.getParameter("boid");

  String TAX_FREE_SHARE= String.valueOf(request.getParameter("TAX_FREE_SHARE"));
  String LIABLE_TO_TAX=  String.valueOf(request.getParameter("LIABLE_TO_TAX"));
  String SPECIAL_TAX=    String.valueOf(request.getParameter("SPECIAL_TAX"));
  String RESIDENT=       String.valueOf(request.getParameter("RESIDENT"));
  String NATIONALITY=    String.valueOf(request.getParameter("NATIONALITY"));
  String CLASS=          String.valueOf(request.getParameter("CLASS"));

  String SQL = "SELECT * FROM BOADDRESSTEMPNEW_VIEW WHERE (BO_ID= '"+BO_ID+"')";

  int Count = cm.queryExecuteCount(SQL);


   if (Count>0){

    if (TAX_FREE_SHARE.equals("null") || TAX_FREE_SHARE.equals(""))
            TAX_FREE_SHARE = "0";
    if (LIABLE_TO_TAX.equals("null"))
            LIABLE_TO_TAX = "F";
    if (SPECIAL_TAX.equals("null"))
            SPECIAL_TAX = "F";
    if (RESIDENT.equals("null"))
            RESIDENT = "F";
    if (NATIONALITY.equals("null"))
            NATIONALITY = "";
    if (CLASS.equals("null"))
            CLASS = "";

    SQL = "call UPDATE_BOADDRESSTEMP('" +BO_ID + "', "+TAX_FREE_SHARE + ", '" + LIABLE_TO_TAX + "','" + SPECIAL_TAX + "', '" + RESIDENT +"','" + NATIONALITY + "','" + CLASS + "')";
    boolean b = cm.procedureExecute(SQL);
  }

/*
'******************************************************************************************************************************************
'Updated By    : Renad Hakim
'Date          : March 2005
'******************************************************************************************************************************************
*/

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Updated Non-reconciled BO Address Information')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }

%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/CDBL/ViewBOAddress.jsp";
</script>
</body>
</html>
