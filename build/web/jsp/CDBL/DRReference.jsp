<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : June 2007
'Page Purpose  : View of all the Demat Request import of Last Date
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Demat Request Import on Last Date</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

function closedocument()
{
  location = "<%=request.getContextPath()%>/jsp/Home.jsp";
}

//Displays the previous list of folio
function goPrevious()
{
 var f=document.forms[0];
  var FCT=parseInt(f.FirstCount.value);
  var LCT=parseInt(f.LastCount.value);
  var chunk=parseInt(f.maxCount.value);
  var totalRows=parseInt(f.TotalRows.value);
  var FC=FCT-chunk;
  var LC=FC+chunk-1;
  if(LC>totalRows)
  LC=totalRows;
  if(LC<chunk)
  LC=chunk;
  if(FC<=0)
  FC=1;
    location = "<%=request.getContextPath()%>/jsp/CDBL/DRReference.jsp?prev=true&FirstCount="+FC+"&LastCount="+LC;
  }

function goNext()
{
  var f=document.forms[0];
  var FCT=parseInt(f.FirstCount.value);
  var LCT=parseInt(f.LastCount.value);
  var chunk=parseInt(f.maxCount.value);
  var totalRows=parseInt(f.TotalRows.value);
  var FC=FCT;
  var LC=LCT+chunk;
  if((FCT+chunk)<totalRows)
  FC=FCT+chunk;
  if(LC>totalRows)
  {
    LC=totalRows;
  }
    location = "<%=request.getContextPath()%>/jsp/CDBL/DRReference.jsp?next=true&FirstCount="+FC+"&LastCount="+LC;
  }

function submitNow()
{
  document.forms[0].submit();
}

//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
.style19 {
	color: #0A2769;
	font-weight: bold;
}
.style17 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   String previous = "";
   String next = "";

   previous= request.getParameter("prev");
   next= request.getParameter("next");


   int folio_state=0;
   int COUNT;
   int MAX_RECORDS=20;
   String refNo=request.getParameter("RefNo");
   String FirstCount = request.getParameter("FirstCount");
   String LastCount=request.getParameter("LastCount");

   if(refNo!=null)
   refNo=refNo.trim();

   String folioname = "";
   String certNo = "";
   String pfBDate = "";
   int total = 0;
   String boid = "";
   int dist_from = 0;
   int dist_to = 0;
   int intFC=0;
   int intLC=0;
   String dest = "";
   int TotalRows=0;
   String TotalCountQuery="";

   if(FirstCount==null)
   FirstCount="1";
   if(LastCount==null)
   LastCount="20";



   TotalCountQuery="select * from DEMAT_REQUEST_VIEW";
   try
   {
   TotalRows= cm.queryExecuteCount(TotalCountQuery);
   }
   catch(Exception e){}


   String query1="";

   if(refNo==null)
   {
     query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (select * from DEMAT_REQUEST_VIEW order by REF_NO) div WHERE rownum<='" + LastCount + "') WHERE  rnum>='" + FirstCount + "'";
   }
   else
   {
     query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (select * from DEMAT_REQUEST_VIEW where REF_NO>='" + refNo + "' order by REF_NO) div WHERE rownum<='" + LastCount + "') WHERE  rnum>='" + FirstCount + "'";
   }

 try
 {
 cm.queryExecute(query1);

%>
  <span class="style7">
  <form method="GET" action="DRReference.jsp">
  <SPAN id="dprint">

    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
    <img name="B7" src="<%=request.getContextPath()%>/images/btnClose.gif" onclick="closedocument()" onMouseOver="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
  </SPAN>
  <table width="100%" BORDER=1  cellpadding="0" bordercolor="#0044B0" style="border-collapse: collapse">
  <!--DWLayoutTable-->
  <tr><td height="30" bgcolor="#0044B0" class="style17" valign="middle"><center>Demat Request List</center></td></tr>
  <tr>
  	<td>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0">
			<tr bgcolor="#E8F3FD">
				<td width="10%">
                                </td>
				<td width="10%"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goPrevious();"></td>
				<td width="29%"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNext();"></td>
				<td width="12%" height="35" class="style19">Ref. No:</td>
				<td width="19%"><input name="RefNo" type="text"></td>
				<td width="20%"><img  name="B4" src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="submitNow();"  onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnSearchR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnSearch.gif'"></td>
			</tr>
                </table>
	</td>
  </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#06689E">
    <td width="10%" class="style9" bgcolor="#0044B0"><div align="center">Ref.No.</div></td>
    <td width="19%" class="style9" bgcolor="#0044B0"><div align="center">BO No.</div></td>
    <td width="15%" class="style9" bgcolor="#0044B0"><div align="center">Folio No.</div></td>
    <td width="25%" class="style9" bgcolor="#0044B0"><div align="center">Shareholder Name</div></td>
    <td width="15%" class="style9" bgcolor="#0044B0"><div align="center">Certificate No.</div></td>
    <td width="16%" class="style9" bgcolor="#0044B0"><div align="center">Demat Req.Qty</div></td>
  </tr>
  <div align="left">
     <%
    folio_state=0;
    dest = request.getContextPath() + "/jsp/CDBL/viewDematDetails.jsp";
    while(cm.toNext())
     {
       folio_state++;
       int RefNo=cm.getColumnI("REF_NO");
       boid = cm.getColumnS("BO_ID");
       String Folio_no=cm.getColumnS("FOLIO_NO");
       certNo = cm.getColumnS("CERTIFICATE_NO");
       String ShareholderName = cm.getColumnS("SH_NAME");
       int DmatQuantity = cm.getColumnI("DR_QTY");

         %>
    </div>
  <tr bgcolor="#E8F3FD">
           <td class="style13"><div align="left" class="style12">
           <div align="center"><a HREF="<%=dest%>?refNo=<%=RefNo%>"><%=RefNo%></div>
            </div></td>
           <td class="style13"><div align="left" class="style12">
           <div align="center"><a HREF="<%=dest%>?refNo=<%=RefNo%>"><%=boid%></div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
           <div align="center"><a HREF="<%=dest%>?refNo=<%=RefNo%>"><%=Folio_no%></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="left"><%=ShareholderName%></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center"><%=certNo%></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center"><%=DmatQuantity%></div>
           </div></td>
         </tr>
         <%
         }
 }
 catch(Exception e){}

   if (isConnect)
   {
     try
     {
     cm.takeDown();
     }
     catch(Exception e){}
   }
  %>
     <input  type="hidden" name="prev" value="false">
     <input  type="hidden" name="next" value="false">
     <input type="hidden" name="FirstCount" value="<%=FirstCount%>"/>
     <input type="hidden" name="LastCount" value="<%=LastCount%>"/>
     <input type="hidden" name="TotalRows" value="<%=TotalRows%>"/>
     <input type="hidden" name="maxCount" value="<%=MAX_RECORDS%>"/>
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD" ><center>

</body>
</html>
