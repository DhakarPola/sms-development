<script>
/*
'******************************************************************************************************************************************
'Script Author : Md.Kamruzzaman
'Creation Date : June 2007
'Page Purpose  : View Demat Details for a Demat Request
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.io.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*"%>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<jsp:useBean id="util" class="batbsms.Utility"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css">
</LINK><%if (String.valueOf(session.getAttribute("UserName")).equals("null")) {%>
<script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%}%>
<html>
<head>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datepicker.js"></SCRIPT>
<script language="javascript">
<!--
function CLClose()
{
   history.back();
}

//-->
</script>
<%
  boolean isConnect = cm.connect();
  if (isConnect == false) {
%>
<jsp:forward page="ErrorMsg.jsp">
  <jsp:param name="ErrorTitle" value="Connection Failure"/>
  <jsp:param name="ErrorHeading" value="Connection Problem"/>
  <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator"/>
</jsp:forward>
<%
  }
  String refNo = request.getParameter("refNo");

  String boId="";
  String shareholderName="";
  String drQty="";
  String folio_No="";
  String certificate_No="";
  String dist_From="";
  String dist_To="";
  String req_Date="";
  String def_Qty="";
  String de_Num="";
  String def_Num="";
  String address="";
  String phone_Num="";
  String email_Add="";
  String total_Share="";

  String query="select * from DEMAT_REQUEST_VIEW where REF_NO='" + refNo + "'";
  try
  {
    cm.queryExecute(query);
    cm.toNext();
    boId=cm.getColumnS("BO_ID");
    shareholderName=cm.getColumnS("SH_NAME");
    drQty=cm.getColumnS("DR_QTY");
    folio_No=cm.getColumnS("FOLIO_NO");
    certificate_No=cm.getColumnS("CERTIFICATE_NO");
    dist_From=cm.getColumnS("DIST_FROM");
    dist_To=cm.getColumnS("DIST_TO");
    req_Date=cm.getColumnDT("PF_BDATE");
    def_Qty=cm.getColumnS("DRF_QTY");
    de_Num=cm.getColumnS("DE_NO");
    def_Num=cm.getColumnS("DRF_NO");

    String q="select * from BOADDRESS_VIEW  where BO_ID='" + boId + "'";
    cm.queryExecute(q);
    cm.toNext();
    address=cm.getColumnS("ADDRESS");
    phone_Num=cm.getColumnS("PHONE");
    email_Add=cm.getColumnS("EMAIL");
    total_Share=cm.getColumnS("TOTAL_SHARE");
  }
  catch(Exception e){}

%>
<style type="text/css">
  <!--
    body {
    background-color: #FFFFFF;
    }
    body,td,th {
    color: #000000;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
    }
  -->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Demat Request Details</title>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<style type="text/css">
  <!--
    .style7 {
    color: #FFFFFF;
    font-weight: bold;
    font-size: 13px;
    }
    .style8 {color: #0A2769;
            font-weight: bold;}
    .style9 {color: #0A2769; font-weight: bold; }
  -->
</style>
</head>
<body TEXT="000000" BGCOLOR="FFFFFF">
<form name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Demat Request Details</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Reference Number</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=refNo%></div></td>

      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;BOID</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=boId%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Folio</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=folio_No%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Name</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=shareholderName%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Address</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=address%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Phone Number</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=phone_Num%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Email</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=email_Add%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Certificate Number</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=certificate_No%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Distinction From</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=dist_From%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Distinction To</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=dist_To%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Total Share</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=total_Share%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Request Date</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=req_Date%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Request Quantity</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=drQty%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Def. Quantity</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=def_Qty%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;De. Number</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=de_Num%></div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Def. Number</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=def_Num%></div></td>
      </tr>
      </table>
      <br>
      </div>

      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <td width="50%" align="center">
        <img name="B3" src="<%=request.getContextPath()%>/images/btnClose.gif" onclick="CLClose()" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
      </td>
      </tr>
   </table>
</form>
<%
  if (isConnect) {
    try
    {
      cm.takeDown();
    }
    catch(Exception e){}
  }
%>
</body>
</html>

