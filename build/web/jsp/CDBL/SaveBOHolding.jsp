
<script>
/*
'******************************************************************************************************************************************
'Script Author : Ehsan Al Baqui
'Creation Date : December 2005

'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<%@ page language="java" import="oracle.jdbc.driver.*,java.sql.*,org.apache.commons.fileupload.*,java.io.*,java.util.*,java.util.List,java.util.Vector,java.util.Date,java.text.SimpleDateFormat" %>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
%>
     <jsp:forward page="ErrorMsg.jsp" >
     <jsp:param name="ErrorTitle" value="Connection Failure" />
     <jsp:param name="ErrorHeading" value="Connection Problem" />
     <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
<%
    }
    else {
        String CurrentFile = "";
        String fname ="";
        String fieldName;
        String filepath=null;
        String txtTableName="";
        String txtRemarks="";
        String realFilePath="";
        String urlpath ="";
        String folderpath ="";


        DiskFileUpload u1 = new DiskFileUpload();
        List items = u1.parseRequest(request);
        Iterator itr1 = items.iterator();
        FileItem item;
        boolean isFileExists=false;


         while(itr1.hasNext()) {
            item = (FileItem) itr1.next();
            fieldName = item.getFieldName();
            if(fieldName.equals("filepath")) {
                CurrentFile = item.getName();
                File fullFile  = new File(item.getName());
                realFilePath=  fullFile.getAbsolutePath();
                fname =  fullFile.getName();
                if (!CurrentFile.equals("")){
                  //Code for creating a folder
                  String path=getServletContext().getRealPath("/");
                  File f1 = new File(path + "/import");
                  if(!f1.exists()){
                    f1.mkdir();
                  }
                  File f2 = new File(path + "/import/BOHOLDING");
                  if(!f2.exists()){
                    f2.mkdir();
                  }

                 urlpath = path + "import\\BOHOLDING\\" + fname;
                 folderpath = path + "/import/BOHOLDING/";
                  File savedFile = new File(folderpath,fullFile.getName());
                  item.write(savedFile);
                  isFileExists=savedFile.isFile();
                }
            } else if (fieldName.equals("txtTableName")) {
                txtTableName=item.getString();
            } else if (fieldName.equals("txtRemarks")) {
                txtRemarks=item.getString();
            }
         }


        if(isFileExists)
        {

             String Success=cm.importBOHolding(realFilePath,"BO Holding Position",txtTableName,txtRemarks,"Import", new Date(), urlpath);
             File f = new File(urlpath);
             f.delete();
             System.out.println(Success);
             if (Success.equals("")) {
             %>
             <script language="javascript">
             location = "<%=request.getContextPath()%>/jsp/CDBL/ViewBOHolding.jsp";
             </script>
           <%} else {%>
             <script language="javascript">
             location = "<%=request.getContextPath()%>/jsp/CDBL/BOHolding.jsp?msg=<%=Success%>";
             </script>
           <%
            }
        }
        else
        { %>
             <script language="javascript">
             location = "<%=request.getContextPath()%>/jsp/CDBL/BOHolding.jsp?msg=FileNotFound";
             </script>
      <%}

      //cm.takeDown();
        }
/*
'******************************************************************************************************************************************
'Updated By    : Renad Hakim
'Date          : March 2005
'******************************************************************************************************************************************
*/

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Imported BO Holding Information')";
  boolean ub = cm.procedureExecute(ulog);

        %>


