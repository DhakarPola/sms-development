<script>
/*
'******************************************************************************************************************************************
'Script Author : Ehsan Al Baqui
'Updated By    : Renad Hakim
'Creation Date : December 2005
'Page Purpose  : Import the BO Address from the attached .mdb file.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean" scope="request"/>
<%@ page language="java" import="java.util.Vector,java.util.Date,java.text.SimpleDateFormat" %>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
%>
     <jsp:forward page="ErrorMsg.jsp" >
     <jsp:param name="ErrorTitle" value="Connection Failure" />
     <jsp:param name="ErrorHeading" value="Connection Problem" />
     <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
<%
    }
    else
{

  String QueryStr = String.valueOf(request.getParameter("msg"));
  String lastdate = "";

  String query1cc1 = "SELECT MAX(PF_BDATE) AS MDATE FROM BOADDRESS_VIEW";
  cm.queryExecute(query1cc1);
  cm.toNext();
  lastdate = cm.getColumnDT("MDATE");
%>
<html><title>BOAddress</title>
<head>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"> </SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"> </LINK>
<script language="javascript">
<!--
//Execute while click on Submit


function SubmitThis1() {
   //alert("The file name is"+document.forms[0].filepath.value);
  // alert(document.forms[0].filepath.value);
   if (document.forms[0].filepath.value!="" && document.forms[0].filepath.value.toLowerCase().indexOf(".txt")==-1)
   {
     alert("The file is not .txt file");
     //alert("The file name is"+document.forms[0].filepath.value);
   }
   else
   {
     document.forms[0].submit();
      //alert("ok");
   }
}

function SubmitThis() {

  count == 0
  BlankValidate('filepath','- Upload File (Option must be entered)');
  if (document.forms[0].filepath.value!="" && document.forms[0].filepath.value.toLowerCase().indexOf(".mdb")==-1)
  {
   count = count + 1;
   nArray[count] = '- Upload File. (MS Access file only)';
  }
  BlankValidate('txtTableName','- Table Name (Option must be entered)');

  if (count == 0)
  {
    if (confirm("Do you want to Import now?"))
    {
      document.forms[0].submit();
    }
  }
  else
  {
   ShowAllAlertMsg();
   return false;
  }
}

function closeBOAddress()
{
     location = "<%=request.getContextPath()%>/jsp/CDBL/ViewBOAddress.jsp";
}

function LoadValidation()
{
  count=0
  var QS=document.forms[0].QueryStr.value;
  if ((!(QS=="null") && !(QS==""))) {
    if (QS=="FileNotFound") {
      count = count + 1;
      nArray[count] = '- Import Failed. Contact System Admin.';
    } else if (QS=="ImportFailed") {
      count = count + 1;
      nArray[count] = "- Import Failed. MS Access Table Structure do not match";
    } else if (QS=="ConnectionError") {
      count = count + 1;
      nArray[count] = "- Import Failed. [ODBC] Connection Error";
    } else if (QS.indexOf("TableNotFound")!=-1) {
      count = count + 1;
      var tblName=QS.substring(QS.indexOf("TableNotFound.")+14,QS.length);
      nArray[count] = "- Import Failed. " + "Table ["+tblName+"] not found.";
    } else if (QS.indexOf("Imported-")!=-1) {
      count = count + 1;
      var dtstr= QS.substring(QS.indexOf("Imported-")+9,QS.length);
      nArray[count] = "- Import Failed. " + "Demat for " + dtstr + " already imported. please select another date table";
    }
    if (count != 0) {
      ShowAllAlertMsg();
    }
  }
}

//-->
</script>

<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

  <style type="text/css">
  <!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }

  .None {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 14px;
  }
  .style14 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; }
  .style16 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; }
  .style20 {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 10px;
  color: #000000;
  }
  -->
  </style>
</head>

<body bgcolor="#ffffff" onload="LoadValidation();">
<form action="SaveBOAddressForXLS.jsp" method="post" enctype="multipart/form-data" >

<span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Import - BO Address From CDBL(.txt format)</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Upload File(.txt format only) </div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input name="filepath" type="file" id="filepath"  class="SL73TextField">
        </div></td>
      </tr>

  <br>
      <table width="100%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="50%" scope="row">
            <div align="right">
 	      <br>
              <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onClick="SubmitThis1()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
            </div></th>
        <td width="50%">
 	      <br>
              <img name="B2" src="<%=request.getContextPath()%>/images/btnClose.gif"  onClick="closeBOAddress()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
        </td>
      </tr>
      <tr>
        <th width="50%" scope="row">
            <div align="right">
            </div></th>
        <td width="50%" align="right">
        <div align="right" class="style8">
          <b>Date of Last Import : <%=lastdate%></b>
          </div>
        </td>
      </tr>
</table>

  </tr>
</table>
</table>
<p>&nbsp;</p>
<input type="hidden" name="QueryStr" value="<%=QueryStr%>">
</form>



</body>
</html>
<%}

   if (isConnect)
   {
     cm.takeDown();
   }
%>
