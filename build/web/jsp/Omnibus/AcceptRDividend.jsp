<script>
/*
'******************************************************************************************************************************************
'Script Author : Jahirul Islam
'Creation Date : 1st March' 2013
'Page Purpose  : Deletes a omnibus.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Delete Dividend</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   if(isConnect1==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
   }
  //System.out.println("--------------Accept R dividend--------------");
  String OmnibusID = String.valueOf(request.getParameter("domnibus"));
  //System.out.println("--------------OmnibusID--------------"+OmnibusID);
  int iOmnibusID=Integer.parseInt(OmnibusID);
  int oid = 0;
  String decdate = "";
  String warrantno = "";
  String boid = "";
  String stvMaxWarrant="";



  //System.out.println("--------------iOmnibusID--------------"+iOmnibusID);

  String domnibus1 = "SELECT * FROM OMNIBUS_CONFIG_DIVIDEND WHERE REC_ID="+iOmnibusID;
  int drid=cm.queryExecuteCount(domnibus1);
  //System.out.println("---------1-----drid--------------"+drid+"------domnibus1---"+domnibus1);
  if(drid>0){
    oid = cm.getColumnI("OMNIBUS_ID");
    decdate = cm.getColumnDT("DIVIDEND_DATE");
    //System.out.println("-------2-------oid-----"+oid+"---decdate-"+decdate+"-----"+drid);
  }

  String domnibus2 = "SELECT * FROM OMNIBUS_BO_DIVIDEND_T WHERE OMNIBUS_DIV_ID="+iOmnibusID+" AND OMNIBUS_AC_ID="+oid+" AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
  //System.out.println("------domnibus2--------------------"+domnibus2);
  int drid2=cm.queryExecuteCount(domnibus2);
  if(drid2>0){
    boid = "";
    warrantno = "";
    cm.queryExecute(domnibus2);

    while(cm.toNext())
    {
      boid = cm.getColumnS("BO_ID");
      warrantno = cm.getColumnS("WARRANT_NO");
      //System.out.println("------decdate--"+decdate+"------boid-----"+boid+"----warrantno----"+warrantno);

      if(boid.equals("null"))
      boid="";
      if(warrantno.equals("null"))
      warrantno="";

      String dbodiv1="SELECT * FROM OMNIBUS_BO_DIVIDEND WHERE OMNIBUS_DIV_ID="+iOmnibusID+" AND OMNIBUS_AC_ID="+oid+" AND BO_ID='"+boid+"' AND WARRENT_NO='"+warrantno+"' AND OMNIBUS_AC_ID="+oid+" AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
      //System.out.println("---------dbodiv1-------"+dbodiv1);
      int qc=cm1.queryExecuteCount(dbodiv1);

      if(qc<1){
        String dbodiv2="INSERT INTO OMNIBUS_BO_DIVIDEND (REC_ID,OMNIBUS_DIV_ID,OMNIBUS_AC_ID,BO_ID,WARRENT_NO,DIV_DATE,ISSUE_DATE,STATUS) VALUES ((SELECT NVL(MAX(rec_id),0)+1 FROM OMNIBUS_BO_DIVIDEND),"+iOmnibusID+","+oid+",'"+boid+"','"+warrantno+"',SYSDATE,TO_DATE('"+decdate+"','DD/MM/YYYY'),'T')";
        //System.out.println("---------dbodiv2-------"+dbodiv2);
        cm1.queryExecute(dbodiv2);
      }

      String dbodiv3="DELETE FROM OMNIBUS_BO_DIVIDEND_T WHERE OMNIBUS_DIV_ID="+iOmnibusID+" AND OMNIBUS_AC_ID="+oid+" AND BO_ID='"+boid+"' AND WARRANT_NO='"+warrantno+"' AND OMNIBUS_AC_ID="+oid+" AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
      //System.out.println("---------dbodiv3-------"+dbodiv3);
      cm1.queryExecute(dbodiv3);
    }
    //System.out.println("--------------OMNIBUS_BO_DIVIDEND_T deleted------");
  }

  stvMaxWarrant=cm.LastWarrant();
  //System.out.println("---------stvMaxWarrant-------"+stvMaxWarrant);

  String dbodiv4="UPDATE OMNIBUS_CONFIG_DIVIDEND SET STATUS=UPPER('ACCEPTED'), GROUP_WARRANT_NO='"+stvMaxWarrant+"' WHERE REC_ID="+iOmnibusID;
  cm.queryExecute(dbodiv4);
  //System.out.println("---------OMNIBUS_CONFIG_DIVIDEND ACCEPTED-------"+dbodiv4);

  //String docdiv="DELETE FROM OMNIBUS_CONFIG_DIVIDEND WHERE REC_ID="+iOmnibusID+" AND OMNIBUS_ID="+oid+" AND DIVIDEND_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
  //cm.queryExecute(docdiv);

  //System.out.println("-------------- accepting complete ---------");



  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Inactive Dividend for Omnibus')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
   if (isConnect1)
   {
     cm1.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Omnibus/RDividendList.jsp";
</script>
</body>
</html>
