<script>
/*
'******************************************************************************************************************************************
'Script Author : Jahirul Islam
'Creation Date : 1st March' 2013
'Page Purpose  : Deletes a omnibus.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Delete Dividend</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   if(isConnect1==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
   }
  //System.out.println("--------------delete dividend--------------");
  String OmnibusID = String.valueOf(request.getParameter("domnibus"));
  int iOmnibusID=Integer.parseInt(OmnibusID);
  int oid = 0;
  String decdate = "";
  String boid = "";
  String warrantno = "";
  //System.out.println("--------------iOmnibusID--------------"+iOmnibusID);
  //String domnibus1 = "call DELETE_OMNIBUS('" + OmnibusID + "')";
  //System.out.println("--------------domnibus1--------------"+domnibus1);

  //boolean b = cm.procedureExecute(domnibus1);

  //String domnibus1 = "UPDATE OMNIBUS_CONFIG_DIVIDEND SET STATUS = UPPER('INACTIVE') WHERE REC_ID="+OmnibusID;
  String domnibus1 = "SELECT * FROM OMNIBUS_CONFIG_DIVIDEND WHERE REC_ID="+iOmnibusID;
  int drid=cm.queryExecuteCount(domnibus1);
  //System.out.println("---------1-----drid--------------"+drid+"------domnibus1---"+domnibus1);
  if(drid>0){
    //System.out.println("-------2-------drid--------------"+drid);
    //warrantno = cm.getColumnS("BO_ID");
    oid = cm.getColumnI("OMNIBUS_ID");
    decdate = cm.getColumnDT("DIVIDEND_DATE");
    //System.out.println("-------2-------oid-----"+oid+"---decdate-"+decdate+"-----"+drid);

  }

  String domnibus2 = "SELECT * FROM OMNIBUS_BO_DIVIDEND_T WHERE OMNIBUS_DIV_ID="+iOmnibusID+" AND OMNIBUS_AC_ID="+oid+" AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
  //System.out.println("------domnibus2--------------------"+domnibus2);
  int drid2=cm.queryExecuteCount(domnibus2);
  if(drid2>0){
    boid = "";
    warrantno = "";
    cm.queryExecute(domnibus2);

    while(cm.toNext())
    {
      boid = cm.getColumnS("BO_ID");
      warrantno = cm.getColumnS("WARRANT_NO");
      //System.out.println("------decdate--"+decdate+"------boid-----"+boid+"----warrantno----"+warrantno);

      if(boid.equals("null"))
      boid="";
      if(warrantno.equals("null"))
      warrantno="";

      String dbodiv1="UPDATE BODIVIDEND SET OMNIBUS_SELECT=NULL WHERE WARRANT_NO='"+warrantno+"' AND BO_ID='"+boid+"' AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
      cm1.queryExecute(dbodiv1);

      String dbodiv2="DELETE FROM OMNIBUS_BO_DIVIDEND_T WHERE WARRANT_NO='"+warrantno+"' AND OMNIBUS_AC_ID="+oid+" AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
      cm1.queryExecute(dbodiv2);


    }
    //System.out.println("--------------OMNIBUS_BO_DIVIDEND_T deleted------");
  }

  String docdiv="DELETE FROM OMNIBUS_CONFIG_DIVIDEND WHERE REC_ID="+iOmnibusID+" AND OMNIBUS_ID="+oid+" AND DIVIDEND_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
  cm.queryExecute(docdiv);

  //System.out.println("-------------- deletion complete ---------");



  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Inactive Dividend for Omnibus')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
   if (isConnect1)
   {
     cm1.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Omnibus/RDividendList.jsp";
</script>
</body>
</html>
