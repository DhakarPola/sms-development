<script>
/*
'******************************************************************************************************************************************
'Script Author : Jahirul Islam
'Creation Date : 27th February' 2013
'Page Purpose  : Delete a BO dividend from omnibus.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Delete Omnibus</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String warrantno = String.valueOf(request.getParameter("divtOWno"));
  String oid = String.valueOf(request.getParameter("oid"));
  String decdate = String.valueOf(request.getParameter("ddate"));
  //System.out.println("--------------warrantno--------------"+warrantno+"-----oid------"+oid+"------decdate------"+decdate);

  String boid="";

  String dbodiv="SELECT * FROM OMNIBUS_BO_DIVIDEND_T WHERE WARRANT_NO='"+warrantno+"' AND OMNIBUS_AC_ID="+oid+" AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
  int fcc = cm.queryExecuteCount(dbodiv);

  if(fcc>0){
     boid=cm.getColumnS("BO_ID");
     //System.out.println("--------------boid--"+boid);
     String dbodiv1="UPDATE BODIVIDEND SET OMNIBUS_SELECT=NULL WHERE WARRANT_NO='"+warrantno+"' AND BO_ID='"+boid+"' AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
     cm.queryExecute(dbodiv1);
     String dbodiv2="DELETE FROM OMNIBUS_BO_DIVIDEND_T WHERE WARRANT_NO='"+warrantno+"' AND OMNIBUS_AC_ID="+oid+" AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
     cm.queryExecute(dbodiv2);
  }

  //to delete empty omnibus_config_dividend
  String dbodiv2="SELECT * FROM OMNIBUS_BO_DIVIDEND_T WHERE OMNIBUS_AC_ID="+oid+" AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
  int fcc1 = cm.queryExecuteCount(dbodiv2);
  if(fcc1<1){
    String docdiv="DELETE FROM OMNIBUS_CONFIG_DIVIDEND WHERE OMNIBUS_ID="+oid+" AND DIVIDEND_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
    cm.queryExecute(docdiv);
  }


  //System.out.println("--------------deletion complete--------------");

  //boolean b = cm.procedureExecute(domnibus1);

  //String domnibus1 = "UPDATE OMNIBUS_CONFIG_DIVIDEND SET STATUS = UPPER('INACTIVE') WHERE REC_ID="+OmnibusID;
  //cm.queryExecute(domnibus1);

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','BO Dividend deleted from Omnibus')";
  //boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>

<script language="javascript">
 <%
 if(fcc1<1){
   //System.out.println("----fcc1-------------"+fcc1);
 %>
      location = "<%=request.getContextPath()%>/jsp/Omnibus/SDividendList.jsp";
 <%
 }
 else{
   //System.out.println("----fcc1-------------"+fcc1);
 %>
      location = "<%=request.getContextPath()%>/jsp/Omnibus/EditDividend.jsp?dateselect=<%=decdate%>&omnibusName=<%=oid%>&oStartValue=1&oEndValue=0";
 <%
 }
 %>
</script>
</body>
</html>
