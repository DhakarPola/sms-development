<script>
/*
'******************************************************************************************************************************************
'Script Author : Hasanul Azaz Aman
'Creation Date : Feb 2013
'Page Purpose  : Shows  List By Group.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.Locale" %>

<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cmP"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="cm2"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}
 String sigselect = String.valueOf(request.getParameter("sigselect"));
 String DateDec = String.valueOf(request.getParameter("dateselect"));
 String DateDecP = String.valueOf(request.getParameter("dateselect"));
 String thenomineenoP = String.valueOf(request.getParameter("nomineeno"));
 // System.out.print("the Id-----"+thenomineenoP+"-----omnibusDate-----"+DateDec+"------sigselect----"+sigselect+"----------");
%>

<html>

<style type="text/css">
.styleB{
	font-weight:bold !important;
}
img.sigImg1{
	display: block;
	max-width:120px;
	max-height:45px;
	width: auto;
	height: auto;

}
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
.style19 {
	color: #0A2769;
	font-weight: bold;
}
.style20
{
	color: #0044B0;
	font-size: 11px;
	font-weight:bold;
}

-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Omnibus Group List</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
 <%  

DecimalFormat df = new DecimalFormat("#,##0.00");       
DecimalFormat dfnonDec = new DecimalFormat("#,##0");

   boolean isConnectP = cmP.connect();
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   boolean isConnect2 = cm2.connect();
   if((isConnectP==false) || (isConnect==false) || (isConnect1==false) || (isConnect2==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String SNomineeNo = String.valueOf(request.getParameter("nomineeno"));
    /*
	String SStartingValue = String.valueOf(request.getParameter("StartValue"));
    String SEndingValue = String.valueOf(request.getParameter("EndValue"));

    int NomineeNo = 0;
    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    int Chunk = 20;
    EndingValue = StartingValue + Chunk - 1;

    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;

    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }

    NomineeNo = Integer.parseInt(SNomineeNo);
    //String query1cc = "SELECT * FROM SHAREHOLDER_VIEW WHERE NOMINEE_NO = '" + NomineeNo + "' AND ACTIVE = 'T' ORDER BY SUBSTR(FOLIO_NO,3,6)"; //FOLIO_NO";
    String query1cc="SELECT BODIVIDEND.BO_ID,BODIVIDEND.NAME_FIRSTHOLDER,BODIVIDEND.WARRANT_NO,BODIVIDEND.DIVIDEND,BODIVIDEND.total_Share,OMNIBUS_CONFIG_DIVIDEND.group_warrant_no FROM BODIVIDEND_EXCEL_VIEW BODIVIDEND,OMNIBUS_BO_DIVIDEND,OMNIBUS_CONFIG_DIVIDEND where OMNIBUS_BO_DIVIDEND.OMNIBUS_AC_ID="+NomineeNo+" and bodividend.warrant_no=OMNIBUS_BO_DIVIDEND.warrent_no and OMNIBUS_CONFIG_DIVIDEND.rec_id=OMNIBUS_BO_DIVIDEND.OMNIBUS_DIV_ID";

    int fcc = cm.queryExecuteCount(query1cc);

    if (nStartingValue > (fcc-Chunk+1))
    {
      nStartingValue = fcc - Chunk + 1;
    }
    if (nEndingValue > fcc)
    {
      nEndingValue = fcc;
    }
    if (nStartingValue < 1)
    {
      nStartingValue = 1;
    }
    if (nEndingValue < Chunk)
    {
      nEndingValue = Chunk;
    }

    //String query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM SHAREHOLDER_VIEW WHERE NOMINEE_NO = '" + NomineeNo + "' AND ACTIVE = 'T' ORDER BY SUBSTR(FOLIO_NO,3,6)) div where rownum <= " + EndingValue + ") where rnum >= " + StartingValue + "";
    String query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT BODIVIDEND.BO_ID,BODIVIDEND.NAME_FIRSTHOLDER,BODIVIDEND.WARRANT_NO,BODIVIDEND.DIVIDEND,BODIVIDEND.total_Share,OMNIBUS_CONFIG_DIVIDEND.group_warrant_no FROM BODIVIDEND_EXCEL_VIEW BODIVIDEND,OMNIBUS_BO_DIVIDEND,OMNIBUS_CONFIG_DIVIDEND where OMNIBUS_BO_DIVIDEND.OMNIBUS_AC_ID="+NomineeNo+" and bodividend.warrant_no=OMNIBUS_BO_DIVIDEND.warrent_no and OMNIBUS_CONFIG_DIVIDEND.rec_id=OMNIBUS_BO_DIVIDEND.OMNIBUS_DIV_ID) div where rownum <= " + EndingValue + ") where rnum >= " + StartingValue + "";
    // String query1cc="SELECT BODIVIDEND.BO_ID,BODIVIDEND.NAME_FIRSTHOLDER,BODIVIDEND.WARRANT_NO,BODIVIDEND.DIVIDEND,BODIVIDEND.total_Share,OMNIBUS_CONFIG_DIVIDEND.group_warrant_no FROM BODIVIDEND_EXCEL_VIEW BODIVIDEND,OMNIBUS_BO_DIVIDEND,OMNIBUS_CONFIG_DIVIDEND where OMNIBUS_BO_DIVIDEND.OMNIBUS_AC_ID="+NomineeNo+" and bodividend.bo_id=OMNIBUS_BO_DIVIDEND.bo_id and OMNIBUS_CONFIG_DIVIDEND.omnibus_id=OMNIBUS_BO_DIVIDEND.OMNIBUS_DIV_ID"
    cm.queryExecute(query1);
*/
    String foliono = "";
    String folioname = "";
    int totalshares = 0;
    int gtotalshares = 0;
    double inddividend = 0;
	double indTax = 0;
	double grandindTax = 0;
	double indnetdiv = 0;
	double grandindnetdiv = 0;
    double ginddividend = 0;
    String iwarrantno = "";
    String gwarrantno = "";
    int grandTotalShare = 0;
    double grandTotalDividend = 0;
    int rowcounter1 = 0;
%>
  <span class="style7">
  <form method="GET" action="ViewOmnibusbyNominee.jsp">
  
  <SPAN id="dprint">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="gotoPrinter();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
  </SPAN>
  
  <table width="100%" border="0" cellpadding="2" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr>
	<td align="center" Colspan=6><div align="center"><img src="<%=request.getContextPath()%>/images/BATBLogo.bmp"/></div></td>
  </tr>
  <tr>
	<td align="center" Colspan=6>
		<div align="center" style="font-size:18; font-weight:bold; margin-bottom:4px;">British American Tobacco Bangladesh Ltd.</div>
		<div align="center" style="font-size:12;">New DOHS Road, Mohakhali, Dhaka-1206.</div>
		<div align="center" style="font-size:12;">Telephone: +88029842791-5 Fax: +88029842786</div>
		<div align="center" style="font-size:16;font-weight:bold; margin-top:4px;margin-bottom:8px;">Omnibus Dividend Report</div>
	
	</td>
  </tr>
	<%
             
              String thenomineenameP = "";
			  String prGpWarrantNo ="";
			  String prName = "";
			  String prAddress ="";
			  String prAC =  "";
			  String prBank = "";
			  String prBranch = "";
			  String prRouting = "";
			if (!SNomineeNo.equalsIgnoreCase("0"))
		{ 
			  String query1naP = "SELECT group_warrant_no FROM OMNIBUS_CONFIG_DIVIDEND where OMNIBUS_CONFIG_DIVIDEND.OMNIBUS_ID="+thenomineenoP+" and OMNIBUS_CONFIG_DIVIDEND.DIVIDEND_DATE= TO_DATE('" + DateDecP + "','DD/MM/YYYY') ";
			  cmP.queryExecute(query1naP);
			  while(cmP.toNext())
				{
				prGpWarrantNo = cmP.getColumnS("group_warrant_no");
				System.out.println("XXXX-"+prGpWarrantNo);
				break;
				}
				
              String query1nP = "select NAME,ADDRESS,AC_NO,BANK_NAME,BANK_BRANCH,ROUTING_NO from OMNIBUS_CONFIG where REC_ID="+thenomineenoP;
			  System.out.println("Muksud Query-------------------------------"+query1nP);
			  cmP.queryExecute(query1nP);
			  while(cmP.toNext())
				{
			  prName = cmP.getColumnS("NAME");
			  System.out.println("XXXX-Name");
			  prAddress = cmP.getColumnS("ADDRESS");
			  System.out.println("XXXX-Ad");
			  prAC = cmP.getColumnS("AC_NO");
			  //System.out.println("XXXX-AC");
			  prBank = cmP.getColumnS("BANK_NAME");
			  //System.out.println("XXXX-BN");
			  prBranch = cmP.getColumnS("BANK_BRANCH");
			  //System.out.println("XXXX-BR");
			  prRouting = cmP.getColumnS("ROUTING_NO");
			  System.out.println(prRouting);
			  break;
				}
			  
		}		
	%>
  <tr>
    <td width="17%" ><div align="right">Company Name</div></td>
    <td ><div>: &nbsp; &nbsp; <%=prName%></div></td>
  </tr>
  <tr>
    <td width="17%" ><div align="right">Address</div></td>
    <td ><div>: &nbsp; &nbsp; <%=prAddress%></div></td>
  </tr>
  <tr>
    <td width="17%" ><div align="right">Group Warant</div></td>
    <td ><div>: &nbsp; &nbsp; <%=prGpWarrantNo%></div></td>
  </tr>
  <tr>
    <td width="17%" ><div align="right">Bank Account</div></td>
    <td ><div>: &nbsp; &nbsp; <%=prAC%></div></td>
  </tr>
  <tr>
    <td width="17%" ><div align="right">Bank Name</div></td>
    <td ><div>: &nbsp; &nbsp; <%=prBank%></div></td>
  </tr>
  <tr>
    <td width="17%" ><div align="right">Bank Branch</div></td>
    <td ><div>: &nbsp; &nbsp; <%=prBranch%></div></td>
  </tr>
  <tr>
    <td width="17%" ><div align="right">Routing No.</div></td>
    <td ><div>: &nbsp; &nbsp; <%=prRouting%></div></td>
  </tr>
  </table>
  <br/>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#A2CEF7">
    <td width="10%" class="style9"><div align="center" style="font-weight:bold;">BO No.</div></td>
    <td width="32%" class="style9"><div align="center" style="font-weight:bold;">Name</div></td>
    <td width="12%" class="style9"><div align="center" style="font-weight:bold;">Total Shares</div></td>
    <td width="16%" class="style9"><div align="center" style="font-weight:bold;">Gross Dividend</div></td>
    <td width="15%" class="style9"><div align="center" style="font-weight:bold;">Tax</div></td>
    <td width="15%" class="style9"><div align="center" style="font-weight:bold;">Net Dividend</div></td>
  </tr>

  <div align="left">
     <%
    if (!SNomineeNo.equalsIgnoreCase("0"))
    {
		String query1na = "SELECT BODIVIDEND.BO_ID,BODIVIDEND.NAME_FIRSTHOLDER,BODIVIDEND.WARRANT_NO,BODIVIDEND.DIVIDEND,BODIVIDEND.INCOME_TAX,BODIVIDEND.NET_DIVIDEND,BODIVIDEND.total_Share,OMNIBUS_CONFIG_DIVIDEND.group_warrant_no FROM BODIVIDEND_EXCEL_VIEW BODIVIDEND,OMNIBUS_BO_DIVIDEND,OMNIBUS_CONFIG_DIVIDEND where OMNIBUS_BO_DIVIDEND.OMNIBUS_AC_ID="+SNomineeNo+" and bodividend.warrant_no=OMNIBUS_BO_DIVIDEND.warrent_no and OMNIBUS_CONFIG_DIVIDEND.rec_id=OMNIBUS_BO_DIVIDEND.OMNIBUS_DIV_ID AND OMNIBUS_BO_DIVIDEND.ISSUE_DATE = TO_DATE('" + DateDec + "','DD/MM/YYYY') Union SELECT BODIVIDEND.BO_ID,BODIVIDEND.NAME_FIRSTHOLDER,BODIVIDEND.WARRANT_NO,BODIVIDEND.DIVIDEND,BODIVIDEND.INCOME_TAX,BODIVIDEND.NET_DIVIDEND,BODIVIDEND.total_Share,OMNIBUS_CONFIG_DIVIDEND.group_warrant_no FROM BODIVIDEND_EXCEL_VIEW BODIVIDEND,OMNIBUS_BO_DIVIDEND_T,OMNIBUS_CONFIG_DIVIDEND where OMNIBUS_BO_DIVIDEND_T.OMNIBUS_AC_ID="+SNomineeNo+" and bodividend.warrant_no=OMNIBUS_BO_DIVIDEND_T.warrant_no and OMNIBUS_CONFIG_DIVIDEND.rec_id=OMNIBUS_BO_DIVIDEND_T.OMNIBUS_DIV_ID AND OMNIBUS_BO_DIVIDEND_T.ISSUE_DATE = TO_DATE('" + DateDec + "','DD/MM/YYYY')";
		System.out.println("Second Query-------------------------------"+query1na);
		cm2.queryExecute(query1na);

		while(cm2.toNext())
		{
			rowcounter1++;
			foliono = cm2.getColumnS("BO_ID");
			folioname = cm2.getColumnS("NAME_FIRSTHOLDER");

			if (String.valueOf(foliono).equals("null"))
				foliono = "";
			if (String.valueOf(folioname).equals("null"))
				folioname = "";



			totalshares = cm2.getColumnI("TOTAL_SHARE");
			inddividend = cm2.getColumnF("DIVIDEND");
			indTax = cm2.getColumnF("INCOME_TAX");
			indnetdiv = cm2.getColumnF("NET_DIVIDEND");
			grandindTax+=indTax;
			grandindnetdiv+=indnetdiv;
			
			inddividend = bcal.roundtovalue(inddividend,2);

			gwarrantno = cm2.getColumnS("group_warrant_no");
			grandTotalShare+=totalshares;
			grandTotalDividend+=inddividend;
			if (String.valueOf(gwarrantno).equals("null"))
				gwarrantno= "";
     %>
    </div>
  <tr bgcolor="#E8F3FD">
            <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=foliono%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="left">&nbsp;<%=folioname%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12" style="margin-right:5px;">
             <div align="right">&nbsp;<%=dfnonDec.format(totalshares)%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12" style="margin-right:5px;">
             <div align="right">&nbsp;<%=df.format(inddividend)%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12" style="margin-right:5px;">
             <div align="right">&nbsp;<%=df.format(indTax)%>&nbsp;</div>
           </div></td>
           <td class="style13" ><div align="left" class="style12" style="margin-right:5px;">
             <div align="right">&nbsp;<%= df.format(indnetdiv)%>&nbsp;</div>
           </div></td>
  </tr>
	 <%
        }
		if (grandTotalShare > 0)
		{
			
			
     %>
       <tr>
			<td class="style20"></td>
			<td class="style20">
				<div align="left" class="style12">
					<div align="right" style="font-weight:bold;">
						&nbsp;Grand Total&nbsp;
					</div>
				</div>
			</td>
			<td class="style20">
				<div align="left" class="style12" style="margin-right:5px;">
					<div align="right" style="font-weight:bold;">
						&nbsp;<%=dfnonDec.format(grandTotalShare)%>&nbsp;
					</div>
				</div>
			</td>
			<td class="style20">
				<div align="left" class="style12" style="margin-right:5px;">
					<div align="right" style="font-weight:bold;">
						&nbsp;<%=df.format(grandTotalDividend)%>&nbsp;
					</div>
				</div>
			</td>
			<td class="style20">
				<div align="left" class="style12" style="margin-right:5px;">
					<div align="right" style="font-weight:bold;">
						&nbsp;<%=df.format(grandindTax)%>&nbsp;
					</div>
				</div>
			</td>
			<td class="style20">
				<div align="left" class="style12" style="margin-right:5px;">
					<div align="right"  style="font-weight:bold;">
						&nbsp;<%=df.format(grandindnetdiv)%>&nbsp;
					</div>
				</div>
			</td>
		   
         <!--td width="50%" align="left" class="style20"><br>Total Shares: <%=grandTotalShare%></td>
         <td width="50%" align="right" class="style20"><br>Total Dividend: <%=grandTotalDividend%></td -->
       </tr>
     </table>
     <%
     }

    }
  
  %>
  <br>
  <table width="100%" border="0" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
	<tr>
		<td>
			<div align="left" style="font-size:12;">Kind Regards</div>
			<div align="left" style="font-size:6;">&nbsp;</div>
			<div align="left" style="font-weight:bold; font-size:12;">British American Tobacco Bangladesh Company Limited</div>
		</td>
	</tr>
	
	<tr>
	<td>
	<div>
	<%
        String selectQuery="SELECT NAME,RANK,SIGN FROM SIGNATURE_CUSTOMNAME_VIEW WHERE SIG_ID = "+sigselect;
		String sigsign ="";
		String sigrank ="";
		cm.queryExecute(selectQuery);

		while(cm.toNext())
		{
			sigrank = cm.getColumnS("RANK");
			sigsign = cm.getColumnS("NAME");

			if (String.valueOf(sigrank).equals("null"))
			 sigrank = "";
			if (String.valueOf(sigsign).equals("null"))
			 sigsign = "";
		 break;
		}
		selectQuery="SELECT SIGN FROM SIGNATURE_CUSTOMNAME_VIEW WHERE SIG_ID = "+sigselect; 
        byte[] imgData =  cm.getPhoto( cm.getConnection(), selectQuery  );
		if (isConnect)
		{
		 cm.takeDown();
		 cm1.takeDown();
		 cm2.takeDown();
		}
        if(imgData.length !=0)
		{
    %>
		<img class="sigImg" alt="Signature" src="image.jsp?selectQuery=<%=selectQuery%>" width="140" height="49"/>
	<%
		}
	%>
	
     </div></td></tr>
	 <tr>
		<td>
			<div align="left" style="font-weight:bold; font-size:12;"><%=sigsign%></div>
			<div align="left" style="font-size:12;"><%=sigrank%></div>
		</td>
	</tr>
	 </table>
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>

	<script>
		function gotoPrinter()
		{
			//NumberFormat.getNumberInstance(Locale.US).format(grandindTax)
			if (confirm("Do you want to Print the Document?"))
			{
				window.open("<%=request.getContextPath()%>/jsp/Omnibus/group/wordGenerator.jsp?nomineeno=<%=thenomineenoP%>&dateselect=<%=DateDecP%>&sigselect=<%=sigselect%>", "_blank", "toolbar=no,scrollbars=no,resizable=no,top=500,left=500,width=400,height=400");

			}
		}
	</script>

</body>
</html>
