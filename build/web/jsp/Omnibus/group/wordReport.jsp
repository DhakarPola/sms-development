<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Hasanul Azaz Aman
'Creation Date : Feb 2013
'Page Purpose  : Selects Date for Omnbious Group List.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">

</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Omnibus Group List</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">


<!--
//Execute while click on Submit


function SubmitThis() {

  var count = 0;

    //SelectValidate('dateselect','- Declaration Date (Option must be entered)');
    //SelectValidate('omnibusName','- Omnibus Name  (Option must be entered)');
    var e = document.getElementById("dateselect");
    var f = document.getElementById("omnibusName");
    var s = document.getElementById("sigselect");

   var strUser = e.options[e.selectedIndex].value;
   var strUser1 = f.options[f.selectedIndex].value;
   var strUser2 = s.options[s.selectedIndex].value;

  // alert(strUser)
  // alert(strUser1)

   //alert(omnibusName)
   if(strUser==null)
   {
     alert('Declaration Date must be entered');
     return false;
   }
   if(strUser=='')
   {
     alert('Declaration Date must be entered');
     return false;
   }

    if(strUser1==null)
    {
      alert('Omnibus Name  must be entered');
      return false;
    }
     if(strUser1=='')
     {
       alert('Omnibus Name must be entered');
       return false;
     }

     if(strUser2==null)
    {
      alert('Signature must be entered');
      return false;
    }
     if(strUser2=='')
     {
       alert('Signature must be entered');
       return false;
     }



  if (count == 0)
  {

    // document.forms[0].action="<%=request.getContextPath()%>/jsp/Omnibus/group/ViewWordReport.jsp?nomineeno="+strUser1+"&dateselect="+strUser;
	location.href="<%=request.getContextPath()%>/jsp/Omnibus/group/ViewWordReport.jsp?nomineeno="+strUser1+"&dateselect="+strUser+"&sigselect="+strUser2;
    //document.forms[0].submit();
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}

function refresh()
{
  location="<%=request.getContextPath()%>/jsp/Omnibus/group/wordReport.jsp";
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
%>

<form action="<%=request.getContextPath()%>/jsp/Omnibus/group/ViewWordReport.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Omnibus Report</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date of Declaration</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="dateselect" class="SL1TextFieldListBox">
            <option>--- Please Select ---</option>
            <%
              String thedate = "";

              String query1 = "SELECT DISTINCT ISSUE_DATE FROM DIVIDEND_VIEW ORDER BY ISSUE_DATE DESC";
              cm.queryExecute(query1);

              while(cm.toNext())
              {
                 thedate = cm.getColumnDT("ISSUE_DATE");
                 %>
                   <option value="<%=thedate%>"><%=thedate%></option>
                 <%
              }
            %>
         </select>
        </div></td>
      </tr>




      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Omnibus</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="omnibusName" class="SL1TextFieldListBox">
            <option>--- Please Select ---</option>
            <%
              String oid = "";
              String oname = "";

             String query2 = "SELECT REC_ID, NAME FROM OMNIBUS_CONFIG ORDER BY NAME ASC";
              cm.queryExecute(query2);

              while(cm.toNext())
              {
                oid=cm.getColumnS("REC_ID");
                oname=cm.getColumnS("NAME");
                                 %>
                   <option value="<%=oid%>"><%=oname%></option>
                 <%
              }

            %>
         </select>
        </div></td>
      </tr>

      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Signature</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="sigselect" class="SL1TextFieldListBox">
            <option>--- Please Select ---</option>
            <%
              String signame = "";
              String sigid = "";
              String sigrank = "";

              String query3 = "SELECT * FROM SIGNATURE_VIEW ORDER BY SIG_ID";
              cm.queryExecute(query3);

              while(cm.toNext())
              {
                 signame = cm.getColumnS("NAME");
                 sigid = cm.getColumnS("SIG_ID");
                 sigrank = cm.getColumnS("RANK");
                 %>
                   <option value="<%=sigid%>"><%=signame%> (<%=sigrank%>)</option>
                 <%
              }
            %>
         </select>
        </div></td>
      </tr>


  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="50%" scope="row">
            <div align="right">
	      <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
            </div></th>
        <td width="50%" align="left">
 	      <img name="B1" src="<%=request.getContextPath()%>/images/btnOK.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOKOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOK.gif'">
      </td>
      </tr>
</table>





</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
