<script>
/*
'******************************************************************************************************************************************
'Script Author : Jahirul Islam
'Creation Date : 14th February' 2013
'Page Purpose  : Edits Omnibus Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}
%>

<html>
<head>
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   String omnibusname  	= request.getParameter("uname1");
   String Somnibusid  	= request.getParameter("uid1");
   int omnibusid 		= Integer.parseInt(Somnibusid);
   String OmnibusAddress = "";
   String OmnibusContact = "";
   String OmnibusAc = "";
   String OmnibusStatus = "";
   	String OmnibusAcName        = ""; 
    String OmnibusAcBank        = "";
    String OmnibusAcBankBranch  = "";

   String query1 = "SELECT * FROM OMNIBUS_CONFIG WHERE REC_ID = " + omnibusid + " AND NAME = '"+omnibusname+ "'";
   cm.queryExecute(query1);

   while(cm.toNext())
     {
       OmnibusAddress 	= cm.getColumnS("ADDRESS");
       OmnibusContact 	= cm.getColumnS("CONTACT");
       OmnibusAc  		= cm.getColumnS("AC_NO");
       OmnibusStatus 	= cm.getColumnS("STATUS");
	   OmnibusAcName 	= cm.getColumnS("AC_NAME");
		OmnibusAcBank    =  cm.getColumnS("BANK_NAME");
		OmnibusAcBankBranch = cm.getColumnS("BANK_BRANCH");

       if (String.valueOf(OmnibusAddress).equals("null"))
         OmnibusAddress = "";
       if (String.valueOf(OmnibusContact).equals("null"))
         OmnibusContact = "";
      if (String.valueOf(OmnibusAc ).equals("null"))
         OmnibusAc = "";
      if (String.valueOf(OmnibusStatus).equals("null"))
         OmnibusStatus = "INACTIVE";
	 		if (String.valueOf(OmnibusAcName).equals("null"))
			OmnibusAcName       = "";
		if (String.valueOf(OmnibusAcBank).equals("null"))
			OmnibusAcBank       = "";
		if (String.valueOf(OmnibusAcBankBranch).equals("null"))
			OmnibusAcBankBranch       = "";	
     }
%>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit Omnibus</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function gotoomnibus()
{
  location = "<%=request.getContextPath()%>/jsp/Omnibus/AllOmnibus.jsp";
}

function gotodelete()
{
  if (confirm("Do you want to delete the Omnibus?"))
  {
    location = "<%=request.getContextPath()%>/jsp/Omnibus/DeleteOmnibus.jsp?cusid2=<%=Somnibusid%>";
  }
}

function formconfirm()
{
  if (confirm("Do you want to change the Omnibus Information?"))
  {
    return true;
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('uname','- Name (Option must be entered)');
  /*
  BlankValidate('uloginid','- Login ID (Option must be entered)');
  BlankValidate('upass','- Password (Option must be entered)');
  BlankValidate('ucpass','- Confirm Password (Option must be entered)');

  if (document.all.upass.value != document.all.ucpass.value)
  {
    count++;
    nArray[count] = '- Passwords do no match!';
  }
  */

  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">

<form action="UpdateOmnibus.jsp" method="get" enctype="multipart/form-data" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Omnibus Information</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Name</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="uname" value="<%=omnibusname%>" class="SL2TextField">
</div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Address</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="uaddress" value="<%=OmnibusAddress%>" class="SL2TextField" >
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Contact</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="ucontact" value="<%=OmnibusContact%>" class="SL2TextField" >
        </div></td>
      </tr>
   
     <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;A/C No</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="uAc" value="<%=OmnibusAc%>" class="SL2TextField" >
        </div></td>
      </tr>
	   <tr>
		  <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;A/C Name </div></th>
		  <td>:</td>
		  <td><div align="left">
			<input type="text" name="uAc_Name" value="<%=OmnibusAcName%>" class="SL2TextField">
		  </div></td>
		</tr>
	   <tr>
		  <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank </div></th>
		  <td>:</td>
		  <td><div align="left">
			<input type="text" name="uAc_Bank" value="<%=OmnibusAcBank%>" class="SL2TextField">
		  </div></td>
		</tr>
	   <tr>
		  <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Branch </div></th>
		  <td>:</td>
		  <td><div align="left">
			<input type="text" name="uAc_Bank_Branch" value="<%=OmnibusAcBankBranch%>" class="SL2TextField">
		  </div></td>
		</tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Status </div></th>
        <td>:</td>
        <td><div align="left">
          <%
          if (OmnibusStatus.equalsIgnoreCase("ACTIVE"))
          {
          %>
          <input type="radio" id="uactive" name="ustatus" value="ACTIVE" checked="checked" ><label for="uactive">&nbsp;&nbsp;ACTIVE&nbsp;&nbsp;</label>
          <input type="radio" id="uinactive" name="ustatus" value="INACTIVE" ><label for="uinactive">&nbsp;&nbsp;INACTIVE&nbsp;&nbsp;</label>
          <%
          }
          else if (OmnibusStatus.equalsIgnoreCase("INACTIVE"))
          {
          %>
          <input type="radio" id="uactive" name="ustatus" value="ACTIVE" ><label for="uactive">&nbsp;&nbsp;ACTIVE&nbsp;&nbsp;</label>
          <input type="radio" id="uinactive" name="ustatus" value="INACTIVE" checked="checked" ><label for="uinactive">&nbsp;&nbsp;INACTIVE&nbsp;&nbsp;</label>
          <%
          }
          %>
        </div></td>
      </tr>
  </table>

  <!--HIDDEN FIELDS-->
  <input type="hidden" name="fomnibusid" value="<%=Somnibusid%>">

  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="43%" scope="row">
            <div align="right">
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
            </div></th>
        <td width="57%">
	          <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="gotoomnibus()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
        </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
