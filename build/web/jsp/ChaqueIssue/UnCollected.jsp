<script>
/*
'******************************************************************************************************************************************
'Script Author : Rahat Uddin
'Creation Date : Octoberber 2011
'Page Purpose  : View of all the Chaque Information
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cms"  class="batbsms.conBean"/>
<jsp:useBean id="cmss"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style19 {color: #0A2769; font-weight: bold; }
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Folios</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>

<script language="javascript">
<!--

function calculateTotal() {
 var value=document.getElementById('totalRow').value;
 var totalAmount=0;
 for(var i=1;i<=value;i++) {
   var chk='selectingbox'+i;
   var hiddenVal='hidden'+i;
   if(document.getElementById(chk).checked) {
    var val=document.getElementById(hiddenVal).value;
    totalAmount=Number(totalAmount)+Number(val);
   }
 }
 document.getElementById('totalamount').value=totalAmount;
}

function goNextPrevious(pstart,pend,foliono)
{
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Folio/AllPermanentFolios.jsp?StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;
  thisurl = thisurl + "&searchFolio="+foliono;
  location = thisurl;
}

 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}

function Cancel() {
  document.forms.cancel.submit();
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cms.connect();
   boolean isConnect2 = cmss.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   int rowcounter1 = 0;
   int folio_state=0;
   int COUNT;
   int MAX_RECORDS=20;
   String chaqueid=request.getParameter("chaid");
   String folio_no=request.getParameter("foliono");
   String chequeno=request.getParameter("chequeNo");

 //System.out.println(("chequeno="+chequeno+"pppp");
   String folioname = "";
   String folioaddress = "";
   String folioaddress2 = "";
   String folioaddress3 = "";
   String folioaddress4 = "";
   String dest = "";
   int totalshares = 0;



    if (String.valueOf(chaqueid).equals("null"))
      chaqueid = "0";
    if (String.valueOf(chequeno).equals("null"))
      chequeno = "0";
   String query1;

if(chequeno.length()>2){
  query1 = "Select chaque_id, chaque_no, coll_status, to_char(issue_date,'dd/mm/yyyy') as issue_date, folio_bo,Total_amount,ACCOUNT_NO from issued_chaque where chaque_no='"+chequeno+"'";
}
else{
  query1 = "Select chaque_id, chaque_no, coll_status, to_char(issue_date,'dd/mm/yyyy') as issue_date, folio_bo,Total_amount,ACCOUNT_NO from issued_chaque where chaque_id="+chaqueid;
}
   //System.out.println(query1);
  try {
    cm.queryExecute(query1);
  }catch(Exception e) {
   e.printStackTrace();
  }

%>
  <span class="style7">
  <form method="post" action="UpdateUncollected.jsp">
  <table width="100%" BORDER=1  cellpadding="0" bordercolor="0044B0" style="border-collapse: collapse" bordercolor="#06689E">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7" height="30"><center>Chaque Information</center></td></tr>

  <tr>
  	<td>
      <%
       folio_state=0;
       String accountNoS="";
       String chequeNo="";
       String issueDate="";
       String collStatus="";
       String totalAmount="";
       while(cm.toNext())
       {
         folio_no=cm.getColumnS("folio_bo");
         chaqueid=cm.getColumnS("chaque_id");
         accountNoS=cm.getColumnS("ACCOUNT_NO");
         chequeNo=cm.getColumnS("CHAQUE_NO");
         totalAmount=cm.getColumnS("TOTAL_AMOUNT");
         collStatus=cm.getColumnS("COLL_STATUS");
         issueDate=cm.getColumnS("ISSUE_DATE");
         if(String.valueOf(accountNoS).equals("null")) accountNoS="";

         if (!String.valueOf(chaqueid).equals("null")){
           %>
           <table width="100%"  border="0" cellpadding="0" cellspacing="0">
             <tr bgcolor="#E8F3FD">
               <td width="7%"></td>
               <td width="15%" class="style19">Folio No:</td>
               <td width="3%" align=center>:</td>
               <td width="75%"><%=folio_no%><input name="Folio_Bo" type="Hidden" value="<%=folio_no%>"></td>
             </tr>
             <tr bgcolor="#E8F3FD">
               <td ></td>
               <td  class="style19">Chaque ID</td>
               <td align=center>:</td>
               <td><%=chaqueid%><input name="chaqueid" type="hidden" value="<%=chaqueid%>"></td>
             </tr>
             <tr bgcolor="#E8F3FD">
               <td ></td>
               <td class="style19">Chaque No.</td>
               <td align=center>:</td>
               <td ><input name="chaqueno" type="text" value="<%=chequeNo%>"></td>
             </tr>

             <tr bgcolor="#E8F3FD">
               <td ></td>
               <td class="style19">Account No.</td>
               <td align=center>:</td>
               <td ><div align="left">
                 <select id="account" name="account" class="SL1TextFieldListBox">
                   <option value="">--- Please Select ---</option>
                   <%
                   //String query1 = "SELECT DISTINCT ACCOUNT_NO FROM DIVIDENDDECLARATION_VIEW ORDER BY ACCOUNT_NO DESC";
                   String qry = "SELECT DISTINCT * FROM BANK_ACCOUNTS_VIEW";
                   cms.queryExecute(qry);
                   //System.out.println(("qry="+qry);
                   String unclaimed_account="";
                   String current_account ="";
                   while(cms.toNext())
                   {
                     //String account_no = cm.getColumnS("ACCOUNT_NO");
                     current_account = cms.getColumnS("CURRENT_ACCOUNT");
                     unclaimed_account = cms.getColumnS("UNCLAIMED_ACCOUNT");
                     //System.out.println("current_account="+current_account);
                     //System.out.println("unclaimed_account="+unclaimed_account);
                     %>
                     <option value="<%=current_account%>" <%if(current_account.equalsIgnoreCase(accountNoS)){%>selected="selected"<%}%>><%=current_account%>&nbsp;(Current Account)</option>
                     <option value="<%=unclaimed_account%>" <%if(unclaimed_account.equalsIgnoreCase(accountNoS)){%>selected="selected"<%}%>><%=unclaimed_account%>&nbsp;(Marged Account)</option>
                     <%
                     }
                     %>
                     </select>
</div></td>
             </tr>

             <tr bgcolor="#E8F3FD">
               <td ></td>
               <td class="style19">Total Amount</td>
               <td align=center>:</td>
               <td ><input name="totalamount" type="text" value="<%=totalAmount%>"></td>
             </tr>
             <tr bgcolor="#E8F3FD">
               <td ></td>
               <td class="style19">Issue Date</td>
               <td align=center>:</td>
               <td ><input name="issuedate" type="text" value="<%=issueDate%>"></td>
             </tr>
             <tr bgcolor="#E8F3FD">
               <td ></td>
               <td class="style19">Status</td>
               <td align=center>:</td>
               <td >
                 <Select name="ChaqueStatus">
                   <option value="">--Please Select--</option>
                   <option value="T" <%if (collStatus.equalsIgnoreCase("T")){%> selected="selected" <%} %>>Collected</option>
                   <option value="F" <%if (collStatus.equalsIgnoreCase("F")){%> selected="selected" <%} %>>Uncollected</option>
                   <option value="C" <%if (collStatus.equalsIgnoreCase("C")){%> selected="selected" <%} %>>Canceled</option>
</select>
               </td>
             </tr>

           </table>
</td>
  </tr>
  </table>
  <%
  }
}
%>
  <BR>
     <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="0044B0">
     <tr><td bgcolor="#0044B0" class="style7" height="30" colspan="6"><center>Dividend Information</center></td></tr>
       <tr bgcolor="#0044B0">
       <td width="10%"><div align="center" class="style9">
           <div align="center"></div></div></td>
         <td width="18%"><div align="center" class="style9">
           <div align="center">Issue Date</div></div></td>
        <td width="18%"><div align="center" class="style9">
          <div align="center">Warrant No.</div></div></td>
        <td width="18%"><div align="center" class="style9">
          <div align="center">Total Share</div></div></td>
        <td width="18%"><div align="center" class="style9">
          <div align="center">Amount</div></div></td>
        <td width="18%"><div align="center" class="style9">
          <div align="center">Remarks</div></div></td>
       </tr>
       <%
       query1 = "Select FOLIO_BO,to_char(issue_date,'dd/mm/yyyy') as issue_date,warrent_no from warr_chaque where chaque_id="+chaqueid;

   cm.queryExecute(query1);

   while(cm.toNext())
   {
     rowcounter1++;
     //folio_no=cms.getColumnS("FOLIO_NO");
     String warrentIssueDate=cm.getColumnS("issue_date");
     String warrentNo=cm.getColumnS("warrent_no");

   //System.out.println(("folio_no="+folio_no);
   //System.out.println(("warrentIssueDate="+warrentIssueDate);
   //System.out.println(("warrentNo="+warrentNo);

     String q="(select dividend,total_share,remarks from dividend where WARRANT_NO='"+warrentNo+"' and folio_no='"+folio_no+"' and issue_date=to_date('"+warrentIssueDate+"','dd/MM/yyyy')) union (select dividend,total_share,remarks from bodividend where WARRANT_NO='"+warrentNo+"' and bo_id='"+folio_no+"' and issue_date=to_date('"+warrentIssueDate+"','dd/MM/yyyy'))";
     //System.out.println(q);
     String dividendAmount="";
     String remarks="";
     String totalShare="";
     cmss.queryExecute(q);
     while(cmss.toNext()) {
        dividendAmount=cmss.getColumnS("dividend");
        totalShare=cmss.getColumnS("total_share");
        remarks=cmss.getColumnS("remarks");
        if(remarks==null||remarks.equalsIgnoreCase("null"))
       remarks="";
      //System.out.println(("dividendAmount="+dividendAmount);
        if(dividendAmount.indexOf(".")==dividendAmount.length()-2)
           dividendAmount=dividendAmount+"0";
        else  if(dividendAmount.indexOf(".")==-1) {
          dividendAmount=dividendAmount+".00";
       }
     }
     %>


       <tr bgcolor="#E8F3FD">
         <td ><div align="left" class="style12">
           <div align="center" class="style12">
               <input onclick="calculateTotal()" type="checkbox" name="selectingbox<%=rowcounter1%>" id="selectingbox<%=rowcounter1%>" value="<%=warrentIssueDate%>^<%=warrentNo%>" checked="checked">
               <input type="hidden" id="warrantval<%=rowcounter1%>" name="warrantval<%=rowcounter1%>" value="<%=cm.getColumnS("issue_date")%>^<%=warrentNo%>"/>
           </div></div></td>
         <td ><div align="left" class="style12">
           <div align="center" class="style12">
               <%=warrentIssueDate%>&nbsp;
             </div></div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;&nbsp;<%=warrentNo%></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="right">&nbsp;&nbsp;<%=totalShare%></div>
           </div></td>
            <td class="style13"><div align="left" class="style12">
             <div align="right">&nbsp;&nbsp;<%=dividendAmount%></div>
             <input type="hidden" id="hidden<%=rowcounter1%>" name="hidden<%=rowcounter1%>" value="<%=dividendAmount%>"/>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="left">&nbsp;&nbsp;<%=remarks%></div>
           </div></td>
       </tr>
       <div align="left" class="style13">
       <%
       }//End of Chaque while

 //query1 = "SELECT FOLIO_BO, issue_date,warrant_no FROM (select di.folio_no FOLIO_BO, to_char(di.issue_date,'dd/mm/yyyy') as issue_date, di.warrant_no  from dividend DI where di.folio_no='"+folio_no+"' and di.remarks is null and collected='F' ";
 //query1 = query1 + " UNION ";
 //query1 = query1 + " SELECT bo.bo_id FOLIO_BO, to_char(bo.issue_date,'dd/mm/yyyy') as issue_date, bo.warrant_no  FROM bodividend BO WHERE  BO.bo_id='"+folio_no+"' and bo.remarks is null and collected='F') ";
 //query1 = query1 + " MINUS ";
 //query1 = query1+ " (Select warr_chaque.FOLIO_BO, to_char(warr_chaque.ISSUE_DATE,'dd/mm/yyyy') as ISSUE_DATE, warr_chaque.warrent_no from warr_chaque, issued_chaque where warr_chaque.folio_bo='"+folio_no+"' and issued_chaque.chaque_id=warr_chaque.chaque_id and issued_chaque.coll_status='F') ORDER BY ISSUE_DATE ";

//query1 = "SELECT FOLIO_BO, issue_date,warrant_no, dividend, total_share, remarks FROM (select di.folio_no FOLIO_BO, to_char(di.issue_date,'dd/mm/yyyy') as issue_date, di.warrant_no, di.dividend,di.total_share,di.remarks  from dividend DI where di.folio_no='"+folio_no+"' and di.remarks is null and collected='F'";
//query1 = query1 + " UNION";
//query1 = query1 + " SELECT bo.bo_id FOLIO_BO, to_char(bo.issue_date,'dd/mm/yyyy') as issue_date, bo.warrant_no,bo.dividend,bo.total_share,bo.remarks  FROM bodividend BO WHERE  BO.bo_id='"+folio_no+"' and bo.remarks is null and collected='F')";
//query1 = query1 + "  MINUS";
//query1 = query1 + "(Select warr_chaque.FOLIO_BO, to_char(warr_chaque.ISSUE_DATE,'dd/mm/yyyy') as ISSUE_DATE, warr_chaque.warrent_no, 0 as dividend,0 as total_share, '' as remarks from warr_chaque, issued_chaque where  warr_chaque.folio_bo='"+folio_no+"' and issued_chaque.chaque_id=warr_chaque.chaque_id) ORDER BY ISSUE_DATE";

query1="select * from (select di.folio_no FOLIO_BO, to_char(di.issue_date,'dd/mm/yyyy') as issue_date, ";
query1 = query1 +"di.warrant_no, di.dividend,di.total_share,di.remarks  from dividend DI  where di.folio_no='"+folio_no+"'" ;
query1 = query1 +" and di.remarks is null and collected='F' ";
query1 = query1 +" UNION ";
query1 = query1 +" SELECT bo.bo_id FOLIO_BO, to_char(bo.issue_date,'dd/mm/yyyy') as issue_date, bo.warrant_no,bo.dividend ,";
query1 = query1 +" bo.total_share,bo.remarks ";
query1 = query1 +" FROM bodividend BO WHERE  BO.bo_id='"+folio_no+"' and bo.remarks is null and collected='F') ";
query1 = query1 +" where (folio_bo,issue_date,warrant_no) not in (    ";
query1 = query1 +" Select warr_chaque.FOLIO_BO, to_char(warr_chaque.ISSUE_DATE,'dd/mm/yyyy') as ISSUE_DATE, warr_chaque.warrent_no as warrant_no ";
query1 = query1 +" from warr_chaque, issued_chaque where  warr_chaque.folio_bo='"+folio_no+"' and issued_chaque.chaque_id=warr_chaque.chaque_id)";
//System.out.println(query1);
cm.queryExecute(query1);

   //int rowcounter1 = 0;
   while(cm.toNext()) {
     rowcounter1++;
     folio_no=cm.getColumnS("FOLIO_NO");

    totalAmount=cm.getColumnS("dividend");
    String remarks="";
    String totalShare="";
    if(totalAmount.indexOf(".")==totalAmount.length()-2)
        totalAmount=totalAmount+"0";
    else  if(totalAmount.indexOf(".")==-1) {
      totalAmount=totalAmount+".00";
    }

    totalShare=cm.getColumnS("total_share");
    remarks=cm.getColumnS("remarks");
    if(remarks==null||remarks.equalsIgnoreCase("null"))
      remarks="";
     %>
       <tr bgcolor="#E8F3FD">
          <td ><div align="left" class="style12">
           <div align="center" class="style12">
               <input onclick="calculateTotal()" type="checkbox" name="selectingbox<%=rowcounter1%>" id="selectingbox<%=rowcounter1%>" value="<%=cm.getColumnS("issue_date")%>^<%=cm.getColumnS("warrant_no")%>">
               <input type="hidden" id="warrantval<%=rowcounter1%>" name="warrantval<%=rowcounter1%>" value="<%=cm.getColumnS("issue_date")%>^<%=cm.getColumnS("warrant_no")%>"/>
           </div></div></td>

         <td ><div align="left" class="style12">
           <div align="center" class="style12">
               <%=cm.getColumnS("issue_date")%>&nbsp;
             </div></div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;&nbsp;<%=cm.getColumnS("warrant_no")%></div>
           </div></td>
            <td class="style13"><div align="left" class="style12">
             <div align="right">&nbsp;&nbsp;<%=totalShare%></div>
           </div></td>
            <td class="style13"><div align="left" class="style12">
             <div align="right">&nbsp;&nbsp;<%=totalAmount%></div>
             <input type="hidden" id="hidden<%=rowcounter1%>" name="hidden<%=rowcounter1%>" value="<%=totalAmount%>"/>
           </div></td>
            <td class="style13"><div align="left" class="style12">
             <div align="left">&nbsp;&nbsp;<%=remarks%></div>
           </div></td>
       </tr>
       <div align="left" class="style13">
       <%
       }//End of Chaque while

   if (isConnect) {
     cm.takeDown();
     cms.takeDown();
     cmss.takeDown();
   }
  %>
  <input type="hidden" id="totalRow" name="totalRow" value="<%=rowcounter1%>"/>
         </div>
         <tr>
           <td colspan="6" align="center">
             <table>
               <tr>
                 <td align="center"><img src="<%=request.getContextPath()%>/images/btnSubmit.gif" onclick="SubmitThis()"></td>
                 <td align="center"><img src="<%=request.getContextPath()%>/images/btnCancel.gif" onclick="Cancel()"></td>
               </tr>
             </table>
             </td>
         </tr>
         </table>
         <input type="hidden" name="rowcounter1" value="<%=rowcounter1%>">
  </form>
  <form id="cancel" name="cancel" action="SearchChaque.jsp">
  </form>
</body>
</html>
