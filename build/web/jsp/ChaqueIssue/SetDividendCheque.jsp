<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Creation Date : October 2005
'Updated By    : Renad Hakim
'Updated By    : Kamruzzaman
'Page Purpose  : Adds a new Folio.
'
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.io.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*"%>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<jsp:useBean id="util" class="batbsms.Utility"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
</LINK><%if (String.valueOf(session.getAttribute("UserName")).equals("null")) {%>
<script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%}%>

<html>
<head>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datepicker.js"></SCRIPT>
<style type="text/css">
  <!--
    body {
    background-color: #FFFFFF;
    }
    body,td,th {
    color: #000000;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
    }
  -->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Add New Folio</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function formconfirm()
{
  if (confirm("Do you want to enter the mentioned Folio Information?"))
  {
    return true;
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;

 //Blank Field Validation

 var typeObj=document.forms[0].elements['type'];
// alert(typeObj);
 var type='';
 if(typeObj[0].checked) {
   type='individual';
 }
 if(typeObj[2].checked) {
   type='allDate';
 }

 //alert(type);
 if(type=='individual') {
    BlankValidate('bofolio','- BO/Folio No (Option must be entered)');
 }
  else if(type=='allDate') {
     BlankValidate('entrydatefrom','- Date From (Option must be entered)');
     BlankValidate('entrydateto','- Date To (Option must be entered)');
  }
  else {
     BlankValidate('bofolio','- BO/Folio No (Option must be entered)');
     BlankValidate('entrydatefrom','- Date From (Option must be entered)');
     BlankValidate('entrydateto','- Date To (Option must be entered)');
  }

    if (count == 0)
    {
        document.forms[0].submit();
    }
    else
    {
      ShowAllAlertMsg();
      return false;
    }
}



function showHide(type)
{
    document.getElementById('lblBoFolio').style.display='none';
    document.getElementById('lblBoFolioC').style.display='none';
    document.getElementById('boFolio').style.display='none';
    document.getElementById('lblDateTo').style.display='none';
    document.getElementById('lblDateToC').style.display='none';
    document.getElementById('dateTo').style.display='none';
    document.getElementById('lblDateFrom').style.display='none';
    document.getElementById('lblDateFromC').style.display='none';
    document.getElementById('dateFrom').style.display='none';

    if(type=='individual') {
      document.getElementById('lblBoFolio').style.display='';
      document.getElementById('lblBoFolioC').style.display='';
      document.getElementById('boFolio').style.display='';
    }
    else if(type=='allDate') {
      document.getElementById('lblDateTo').style.display='';
      document.getElementById('lblDateToC').style.display='';
      document.getElementById('dateTo').style.display='';
      document.getElementById('lblDateFrom').style.display='';
      document.getElementById('lblDateFromC').style.display='';
      document.getElementById('dateFrom').style.display='';
    }
    else {
      document.getElementById('lblBoFolio').style.display='';
      document.getElementById('lblBoFolioC').style.display='';
      document.getElementById('boFolio').style.display='';
      document.getElementById('lblDateTo').style.display='';
      document.getElementById('lblDateToC').style.display='';
      document.getElementById('dateTo').style.display='';
      document.getElementById('lblDateFrom').style.display='';
      document.getElementById('lblDateFromC').style.display='';
      document.getElementById('dateFrom').style.display='';
     }

}

//-->
</script>
<style type="text/css">
<!--
    .style7 {
    color: #FFFFFF;
    font-weight: bold;
    font-size: 13px;
    }
    .style8 {color: #0A2769;
            font-weight: bold;}
    .style9 {color: #0A2769; font-weight: bold; }
.style12 {color: #FFFFFF}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
%>
<form action="<%=request.getContextPath()%>/viewchequereport.jsp" method="get"   name="newFolioForm">
<span class="style7">
<table width="100%" border="1" cellspacing="0" cellpadding="0" style="border-collapse:collapse" bordercolor="#0044B0" bgcolor="#E8F3FD">
  <tr>
    <td>

          <tr>
              <td bgcolor="#0044B0" class="style7" height="30">Issued Chaque Report</td>
          </tr>
                    <tr>
                      <td colspan="2">

                        <table width="100%" border="0" cellpadding="5">
                         <tr>
                <td width="23%" class="style8">&nbsp;</td>
                 <td width="1%">&nbsp;</td>
                <td width="67%">&nbsp;</td>
              </tr>
              <tr>
                <td width="23%" class="style8" valign="top"><div>Type</div></td>
                 <td width="1%" valign="top"><div>:</div></td>
                <td width="67%" class="style8" >
                  <input type="radio" name="type" id="type" value="individual" onclick="showHide('individual')">&nbsp;Individual<br />
                  <input type="radio" name="type" id="type" value="individualDate" onclick="showHide('individualDate')" checked="checked">&nbsp;Individual (Date Range)<br />
                  <input type="radio" name="type" id="type" value="allDate" onclick="showHide('allDate')">&nbsp;All (Date Range)<br />
                </td>
              </tr>
              <tr>
                <td width="23%" class="style8"><div id="lblBoFolio">BO/FOLIO NO.</div></td>
                 <td width="1%" class="style8"><div id="lblBoFolioC">:</div></td>
                <td width="67%"><div id="boFolio"><input name="bofolio" type="text" class="SL2TextField" onkeypress="keypressOnNumberFld()"></div></td>
              </tr>
              <tr>
                <td class="style8"><div id="lblDateFrom">Date From</div></td>
                 <td class="style8"><div id="lblDateFromC">:</div></td>
                <td><div id="dateFrom"><input name="entrydatefrom" type="text"  class="SL2TextField">
                <a href="javascript:show_calendar('newFolioForm.entrydatefrom');"  onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"><img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date"></a>                </td>
               </div>
              </tr>
              <tr>
                <td class="style8"><div id="lblDateTo">Date To</div></td>
                <td class="style8"><div id="lblDateToC">:</div></td>
                <td>
                  <div id="dateTo"><input name="entrydateto" type="text"  class="SL2TextField">
                  <a href="javascript:show_calendar('newFolioForm.entrydateto');"  onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"><img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date"></a>                </td>
               </div>
              </tr>
              <tr>
                <td width="23%" class="style8">&nbsp;</td>
                <td width="1%">&nbsp;</td>
                <td width="67%">
                 <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
                 <img name="B1" src="<%=request.getContextPath()%>/images/btnOK.gif" onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOKOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOK.gif'">
                </td>
              </tr>
              <tr>
                <td class="style8">&nbsp;</td>
                 <td>&nbsp;</td>
                <td align="center">
                 </td>
              </tr>
            </table></td>
          </tr>
    </table>
</table>
</form>
<%
if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
