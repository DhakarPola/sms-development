<script>
/*
'******************************************************************************************************************************************
'Script Author : Masud Ahmad
'Updated By    : Renad Hakim
'Creation Date : March 2006
'Update Info   : Provides certificate Information
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.io.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*"%>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<jsp:useBean id="cm1" class="batbsms.conBean"/>
<jsp:useBean id="util" class="batbsms.Utility"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css">
</LINK><%if (String.valueOf(session.getAttribute("UserName")).equals("null")) {%>
<script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%}%>
<html>
<link href="file:///C|/inetpub/wwwroot/SMS/CSS/colors1.css" rel="stylesheet" type="text/css">
<link href="file:///C|/inetpub/wwwroot/SMS/CSS/Level1_Arial.css" rel="stylesheet" type="text/css">
<head>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datepicker.js"></SCRIPT>
<script language="javascript">
<!--

//-->
</script>

<%
     boolean isConnect = cm.connect();
     boolean isConnect1 = cm1.connect();
  if ((isConnect == false)||(isConnect1 == false))  {
%>
<jsp:forward page="ErrorMsg.jsp">
  <jsp:param name="ErrorTitle" value="Connection Failure"/>
  <jsp:param name="ErrorHeading" value="Connection Problem"/>
  <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator"/>
</jsp:forward>
<%
  }
  String sfolioID = request.getParameter("hfoliono");
%>
<style type="text/css">
  <!--
    body {
    background-color: #FFFFFF;
    }
    body,td,th {
    color: #000000;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
    }
  -->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Certificate Information</title>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css">
</LINK><script language="javascript">
<!--
function setDFDTvalues(value1)
{
   var val1 = value1;
   var val2 = value1.indexOf("&&1");
   var val3 = val1.substring(3,val2);
   var val4 = value1.indexOf("&&2");
   var val5 = val1.substring(val2 + 3,val4);
   var val6 = value1.indexOf("&&3");
   var val7 = val1.substring(val4 + 3,val6);
   var val8 = value1.indexOf("//0");
   var val9 = val1.substring(val6 + 3,val8);
   var val10 = value1.indexOf("//1");
   var val11 = val1.substring(val8 + 3,val10);
   var val12 = value1.indexOf("//2");
   var val13 = val1.substring(val10 + 3,val12);
   var val14 = value1.indexOf("//3");
   var val15 = val1.substring(val12 + 3,val14);
   var val16 = val1.substring(val14 + 3,val1.length);

 if (val3 !="^^0"){
   document.all["DF1"].value = val3;
 }       else{
         document.all["DF1"].value = "";
       }
 if (val5 !="^^1"){
   document.all["DF2"].value = val5;
 }  else{
         document.all["DF2"].value = "";
       }

  if (val7 !="^^2"){
   document.all["DF3"].value = val7;
 } else{
         document.all["DF3"].value = "";
       }
 if (val9 !="^^3"){
   document.all["DF4"].value = val9;
 }else{
         document.all["DF4"].value = "";
       }
  if (val11 !="**0"){
   document.all["DT1"].value = val11;
 }else{
         document.all["DT1"].value = "";
       }
   if (val13 !="**1"){
   document.all["DT2"].value = val13;
 }else{
         document.all["DT2"].value = "";
       }
  if (val15 !="**2"){
   document.all["DT3"].value = val15;
 }else{
         document.all["DT3"].value = "";
       }
   if (val16 !="**3"){
   document.all["DT4"].value = val16;
 }else{
         document.all["DT4"].value = "";
       }
  }

//-->
</script>
<style type="text/css">
  <!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
    .style8 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
    .style9 {color: #0A2769; font-weight: bold; }
  -->
</style>
</head>
<body TEXT="000000" BGCOLOR="FFFFFF" onload="window.refresh()">
<table width="100%" border="1" cellspacing="0" cellpadding="0" style="border-collapse:collapse" bordercolor="#0044B0" bgcolor="#E8F3FD">
  <tr>
    <td>
	<table width="100%" border="0" cellspacing="0" cellpadding="5" >
      <tr>
        <td bgcolor="#0044B0" class="style7" height="30">Certificate Information</td>
      </tr>
      <tr bgcolor="#E8F3FD">
        <td height="100%" bgcolor="#E8F3FD"><center>

          <div align="left">
      <table width="100%" height="100%" border="0" cellpadding="5">
      <tr bgcolor="#E8F3FD">
        <td height="20"><span class="style9">Certificate No.</span></td>
        <td><div align="left"><span class="style9">Distinction From </span></div></td>
        <td><div align="left"><span class="style9">Distinction To </span></div></td>
      </tr>
      <tr bgcolor="#E8F3FD">
        <td width="236" height="181" valign="top"><select name="CertNo" size="9" class="SL1TextField" onChange="setDFDTvalues(document.all.CertNo.value)" >
            <%

                          String CNo = "";
			  String DT= "";
			  String DF = "";
                          String ComboDF = "";
                          String ComboDT = "";
                         String FCombo="";
                          int i =0;
                          int k =0;

                          String[] DFCombo = new String[100];
                          String[] DTCombo = new String[100];

              String CertNoQuery = "SELECT * FROM ALL_VALID_CERTIFIC WHERE HOLDER_FOLIO='"+sfolioID +"' ORDER BY TO_NUMBER(CERTIFICATE_NO)";
              cm.queryExecute(CertNoQuery);

              while(cm.toNext())
              {
                ComboDF="";
                ComboDT="";
                FCombo ="";
                i =0;
                CNo = cm.getColumnS("CERTIFICATE_NO");
                //******************************************************
                //New cm1
              String DFDTQuery = "SELECT * FROM ALL_VALID_CERTIFIC WHERE CERTIFICATE_NO='"+ CNo +"'";
              cm1.queryExecute(DFDTQuery);

              while(cm1.toNext())
              {
              DF = cm1.getColumnS("DIST_FROM");
              DT = cm1.getColumnS("DIST_TO");

               DFCombo[i] = DF;
               DTCombo[i] = DT;
               i++;
               }
            //******************************************************
           for (k=0; k<4; k++){
           if (DFCombo[k]==null){
                 DFCombo[k]="^^"+k;
               }
                 if (DTCombo[k]==null){
                DTCombo[k]="**"+k;
              }
               ComboDF= ComboDF+"&&"+ k +DFCombo[k] ;
               ComboDT= ComboDT+"//"+ k +DTCombo[k] ;
               FCombo =ComboDF + ComboDT;
                         }
                      %>
            <option value="<%=FCombo%>"><%=CNo%></option>
            <%
             }
            %>
        </select></td>
        <td width="145" valign="top"><div align="left">
            <p>
              <input name="DF1" type="text" class="SL1TextField" onfocus="this.blur()">
            </p>
            <p>
              <input name="DF2" type="text" class="SL1TextField" onfocus="this.blur()">
            </p>
            <p>
              <input name="DF3" type="text" class="SL1TextField" onfocus="this.blur()">
            </p>
            <p>
              <input name="DF4" type="text" class="SL1TextField" onfocus="this.blur()">
            </p>
        </div></td>
        <td width="144" valign="top" bordercolor="#0044B0" bgcolor="#E8F3FD" class="style9"><p>
            <input name="DT1" type="text" class="SL1TextField" onfocus="this.blur()">
          </p>
            <p>
              <input name="DT2" type="text" class="SL1TextField" onfocus="this.blur()">
            </p>
            <p>
              <input name="DT3" type="text" class="SL1TextField" onfocus="this.blur()" >
            </p>
            <p>
              <input name="DT4" type="text" class="SL1TextField" onfocus="this.blur()">
            </p>
            </td>
      </tr>
    </table></td>
  </tr>
</table>
<%
  if (isConnect) {
    cm.takeDown();
      }
       if (isConnect1) {
    cm1.takeDown();
      }
%>
</body>
</html>
