<script>
/*
'******************************************************************************************************************************************
'Script Author : Hasanul Azaz Aman
'Creation Date : December 2009
'Page Purpose  : Freeze save the Folio
'******************************************************************************************************************************************
*/
</script>


<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>
<%@ page language="java" import="oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*,org.apache.commons.fileupload.*,java.io.*,java.util.*,java.text.*,java.util.List,java.util.Vector,java.util.Date,java.text.SimpleDateFormat" %>
<jsp:useBean id="util"  class="batbsms.Utility" scope="session"/>
<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Folio</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<script language="javascript" >
function redirect() {
  location = "<%=request.getContextPath()%>/jsp/Folio/freezefolio.jsp";
}
</script>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
     String foliono="";
     String active="";
     try {

       foliono = String.valueOf(request.getParameter("Cfoliono"));
       active=String.valueOf(request.getParameter("active"));
      // System.out.println("foliono="+foliono);
      // System.out.println("active="+active);

       if(active==null || active.equals("null"))
       active="T";

       String query="update shareholder set active='"+active+"' where FOLIO_NO='"+foliono+"'";
      // System.out.println("query="+query);
       cm.queryExecute(query);
    }
  catch(Exception e){
  }
   if (isConnect)
   {
     try{
        cm.takeDown();
      }
     catch(Exception e){System.out.println("Error =="+e);}
   }
%>
<div align="center">
   <br /><br />
   <% if(active.equals("F")) { %>
   <h3>The Folio <%=foliono%> has freezed successfully </h3>
   <%
   }
   else if(active.equals("T")) {
     %>
       <h3>The Folio <%=foliono%> has unfreezed successfully </h3>
     <%
     }
   else  {
     %>
       <h3>The Folio <%=foliono%> Freez/Unfreezed Failed. Try again </h3>
     <%
     }
   %>
   <br />
   <a href="#" onclick="redirect()">Go to Freez Folio</a>
</div>

</body>
</html>
