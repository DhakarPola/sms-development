<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Updated By    : Renad Hakim
'Updated By    : Kamruzzaman
'Creation Date : October 2005
'Page Purpose  : Save new Folio.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>
<%@ page language="java" import="oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*,org.apache.commons.fileupload.*,java.io.*,java.util.*,java.util.List,java.util.Vector,java.util.Date,java.text.SimpleDateFormat" %>
<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Folio</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

     String joint_no = "";



     String foliono = String.valueOf(request.getParameter("foliono"));

     String checkIfFolioExistQuery = "SELECT COUNT(*) AS TOTAL FROM SHAREHOLDER WHERE  FOLIO_NO = '"+ foliono +"'";
     String checkIfFolioExistQueryTemp = "SELECT COUNT(*) AS TOTAL FROM SHAREHOLDER_TEMP WHERE  FOLIO_NO = '"+ foliono +"'";
     cm.queryExecute(checkIfFolioExistQuery);
     int intCheckFolio =0;
     int intCheckFolioTemp=0;

     while(cm.toNext())
     {
       intCheckFolio = cm.getColumnI("TOTAL");
     }

     cm.queryExecute(checkIfFolioExistQueryTemp);
     while(cm.toNext())
     {
       intCheckFolioTemp = cm.getColumnI("TOTAL");
     }

     if(intCheckFolio >0 || intCheckFolioTemp>0)
     {
     %>
        <script language="javascript">
        alert("Folio No Already Exist in Database");
        history.go(-1);
       </script>
     <%
     }


     String category = String.valueOf(request.getParameter("category")).trim();
     String prefix = String.valueOf(request.getParameter("prefix"));
     String shareholdername = String.valueOf(request.getParameter("shareholdername"));
     String surname = String.valueOf(request.getParameter("surname"));
     String relation = String.valueOf(request.getParameter("relation"));
     String father = String.valueOf(request.getParameter("father"));
     String resident = String.valueOf(request.getParameter("resident"));
     String nationality = String.valueOf(request.getParameter("nationality"));
     String occupation = String.valueOf(request.getParameter("occupation"));
     String address1 = String.valueOf(request.getParameter("address1"));

     String zipcode = String.valueOf(request.getParameter("zipcode"));
     String telephone = String.valueOf(request.getParameter("telephone"));
     String cellphone = String.valueOf(request.getParameter("cellphone"));
     String email = String.valueOf(request.getParameter("email"));
     String fax = String.valueOf(request.getParameter("fax"));
     String total_shares = String.valueOf(request.getParameter("totalshares"));
     String ndetails = String.valueOf(request.getParameter("ndetails"));

     String address2 = "";
     String address3 = "";
     String address4 = "";

     String accountno = String.valueOf(request.getParameter("accountno"));
     String bankname = String.valueOf(request.getParameter("bankname"));
     String bankbranch = String.valueOf(request.getParameter("bankbranch"));
     String tin = String.valueOf(request.getParameter("tin"));
     String taxfreeshare = String.valueOf(request.getParameter("taxfreeshare"));
     String entrydate = String.valueOf(request.getParameter("entrydate"));
     String nomineeno = String.valueOf(request.getParameter("nomineeno"));
     String signature = String.valueOf(request.getParameter("signature"));
     String photo = String.valueOf(request.getParameter("photo"));
	 String nid = String.valueOf(request.getParameter("nid"));  // Addded on 01 10 2014
     String routingno = String.valueOf(request.getParameter("routingno")); // Addded on 01 10 2014
	 
     String jointprefix = String.valueOf(request.getParameter("jointprefix"));
     String jointname = String.valueOf(request.getParameter("jointname"));
     String jointrelation = String.valueOf(request.getParameter("jointrelation"));
     String jointfather = String.valueOf(request.getParameter("jointfather"));
     String jointoccupation = String.valueOf(request.getParameter("jointoccupation"));
     String jointtelephone = String.valueOf(request.getParameter("jointtelephone"));
     String jointfax = String.valueOf(request.getParameter("jointfax"));
     String jointemail = String.valueOf(request.getParameter("jointemail"));
     String jointaddress = String.valueOf(request.getParameter("jointaddress"));
     String jointaddress2 = "";
     String jointaddress3 = "";
     String jointaddress4 = "";
     String jointcontactperson = String.valueOf(request.getParameter("jointcontactperson"));
     String jointremarks = String.valueOf(request.getParameter("jointremarks"));
     String jointsignature = String.valueOf(request.getParameter("joint1signature"));
     String jointphoto = String.valueOf(request.getParameter("joint1photo"));



     String joint2prefix = String.valueOf(request.getParameter("joint2prefix"));
     String joint2name = String.valueOf(request.getParameter("joint2name"));
     String joint2relation = String.valueOf(request.getParameter("joint2relation"));
     String joint2father = String.valueOf(request.getParameter("joint2father"));
     String joint2occupation = String.valueOf(request.getParameter("joint2occupation"));
     String joint2telephone = String.valueOf(request.getParameter("joint2telephone"));
     String joint2fax = String.valueOf(request.getParameter("joint2fax"));
     String joint2email = String.valueOf(request.getParameter("joint2email"));
     String joint2address = String.valueOf(request.getParameter("joint2address"));
     String joint2signature = String.valueOf(request.getParameter("joint2signature"));
     String joint2photo = String.valueOf(request.getParameter("joint2photo"));

     String joint2address2 = "";
     String joint2address3 = "";
     String joint2address4 = "";

     String joint2contactperson = String.valueOf(request.getParameter("joint2contactperson"));
     String joint2remarks = String.valueOf(request.getParameter("joint2remarks"));

     String sisactive = String.valueOf(request.getParameter("boxactive"));
     String sisliable = String.valueOf(request.getParameter("boxliable"));
     String sissptax = String.valueOf(request.getParameter("boxsptax"));
    // System.out.println("sissptax= "+sissptax);
     String sptaxValue=String.valueOf(request.getParameter("spTax"));


      signature = cm.replace(signature,"'","''");
      photo = cm.replace(photo,"'","''");
      taxfreeshare = cm.replace(taxfreeshare,"'","''");
      entrydate = cm.replace(entrydate,"'","''");
      foliono = cm.replace(foliono,"'","''");
      category = cm.replace(category,"'","''");
      prefix =cm.replace(prefix,"'","''");
      shareholdername = cm.replace(shareholdername,"'","''");
      surname = cm.replace(surname,"'","''");
      relation = cm.replace(relation,"'","''");
      father = cm.replace(father,"'","''");
      resident = cm.replace(resident,"'","''");
      nationality =cm.replace(nationality,"'","''");
      occupation = cm.replace(occupation,"'","''");
      address1 = cm.replace(address1,"'","''");
      address2 = cm.replace(address2,"'","''");
      zipcode = cm.replace(zipcode,"'","''");
      telephone = cm.replace(telephone,"'","''");
      cellphone = cm.replace(cellphone,"'","''");
      email = cm.replace(email,"'","''");
      fax = cm.replace(fax,"'","''");
      address3 = cm.replace(address3,"'","''");
      address4 = cm.replace(address4,"'","''");
      accountno = cm.replace(accountno,"'","''");
      bankname = cm.replace(bankname,"'","''");
      bankbranch = cm.replace(bankbranch,"'","''");
      tin = cm.replace(tin,"'","''");
	  nid = cm.replace(nid,"'","''");
	  routingno = cm.replace(routingno,"'","''");
	  
      taxfreeshare = cm.replace(taxfreeshare,"'","''");
      entrydate = cm.replace(entrydate,"'","''");
      nomineeno = cm.replace(nomineeno,"'","''");


      jointprefix = cm.replace(jointprefix,"'","''");
      jointname = cm.replace(jointname,"'","''");
      jointrelation = cm.replace(jointrelation,"'","''");
      jointfather = cm.replace(jointfather,"'","''");
      jointoccupation = cm.replace(jointoccupation,"'","''");
      jointtelephone = cm.replace(jointtelephone,"'","''");
      jointfax = cm.replace(jointfax,"'","''");
      jointemail = cm.replace(jointemail,"'","''");
      jointaddress = cm.replace(jointaddress,"'","''");
      jointaddress2 = cm.replace(jointaddress2,"'","''");
      jointaddress3 = cm.replace(jointaddress3,"'","''");
      jointaddress4 = cm.replace(jointaddress4,"'","''");
      jointcontactperson = cm.replace(jointcontactperson,"'","''");
      jointremarks = cm.replace(jointremarks,"'","''");


      joint2prefix = cm.replace(joint2prefix,"'","''");
      joint2name = cm.replace(joint2name,"'","''");
      joint2relation = cm.replace(joint2relation,"'","''");
      joint2father = cm.replace(joint2father,"'","''");
      joint2occupation = cm.replace(joint2occupation,"'","''");
      joint2telephone = cm.replace(joint2telephone,"'","''");
      joint2fax = cm.replace(joint2fax,"'","''");
      joint2email = cm.replace(joint2email,"'","''");
      joint2address = cm.replace(joint2address,"'","''");
      joint2address2 = cm.replace(joint2address2,"'","''");
      joint2address3 = cm.replace(joint2address3,"'","''");
      joint2address4 = cm.replace(joint2address4,"'","''");
      joint2contactperson = cm.replace(joint2contactperson,"'","''");
      joint2remarks = cm.replace(joint2remarks,"'","''");
      ndetails = cm.replace(ndetails,"'","''");

    if (String.valueOf(ndetails).equals("null"))
     ndetails = "";

    if(String.valueOf(resident).equals("null"))
       resident="F";
    else
       resident="T";

    if(String.valueOf(sisactive).equals("null"))
       sisactive="F";
    else
       sisactive="T";

    if(String.valueOf(sisliable).equals("null"))
       sisliable="F";
    else
       sisliable="T";

    if(String.valueOf(sissptax).equals("on"))
       sissptax="T";
    else
    {
      sissptax="F";
      sptaxValue="";
    }


    if(category=="null")
       category="";

    if(total_shares==null)
      total_shares="0";

    //System.out.println("sptaxValue= "+sptaxValue);
   // System.out.println("foliono= "+foliono);

    if(!String.valueOf(sptaxValue).equals("null")||!String.valueOf(sptaxValue).equals(""))
    {
      String sp_tax="call ADD_SPECIAL_TAX('" + foliono + "','" + sptaxValue + "')";
     try
     {
       boolean b=cm.procedureExecute(sp_tax);
     }
     catch(Exception e){}
    }

   String nfolio = "call ADD_SHAREHOLDER_TEMP('" + foliono  + "', '" + prefix  + "', '" + shareholdername   + "', '" + surname + "', '" + relation + "', '" + father + "', '" + address1 + "', '" +address2 + "', '" + address3 +  "', '" + address4  + "', '" +zipcode    + "', '" + occupation  + "', '" + joint_no + "', '" + category  + "', '" +  accountno + "', '" +  bankname  + "', '" +  bankbranch  + "', "+total_shares+" , '" +  taxfreeshare  + "', '" + sisliable + "' ,'" + sissptax + "', '" + resident   + "', '" + nationality  + "', '"  +nomineeno + "', '" + sisactive + "', '" + entrydate +  "', null, null,'" + telephone + "', '" + cellphone + "', '" + email + "', '" + fax + "', '" + tin   +"', null,'" + ndetails + "', '" + nid + "', '" + routingno + "')";

   String procJoint=" call ADD_JOINT_N_TEMP('" + foliono  + "', 1, '" + jointprefix+"' , '" + jointname + "', '" + jointrelation  + "', '" + jointfather + "', '" + jointaddress  + "', '" +jointaddress2  + "', '" + jointaddress3 + " ', '" + jointaddress4 + " ', '" + zipcode    + "', '" + occupation  + "', null,'"+  jointtelephone+ "','"+jointemail +"','"+jointfax+"','"+jointcontactperson+"','"+jointremarks+"', null)";
   String procJoint2=" call ADD_JOINT_N_TEMP('" + foliono  + "', 2 , '"+joint2prefix+ "' , '" + joint2name + "', '" + joint2relation  + "', '" + joint2father + "', '" + joint2address  + "', '" + joint2address2 + "', '" + joint2address3 + " ', '" + joint2address4 + " ', '" + zipcode    + "', '" + occupation   + "', null,'"+  joint2telephone+ "','"+joint2email +"','"+joint2fax+"','"+joint2contactperson+"')";

  if(intCheckFolio <=0 || intCheckFolioTemp <= 0)
   {
    //System.out.println(nfolio);
     boolean b = cm.procedureExecute(nfolio);
     if(!jointname.equals(""))
     {
       //System.out.println(procJoint);
      boolean c = cm.procedureExecute(procJoint);

     }
     if(!joint2name.equals(""))
     {
       //System.out.println(procJoint2);
      boolean d = cm.procedureExecute(procJoint2);

     }

     try
     {

       //*********** SIGNATURE SECTION ************************
       //This section inserts signature into database
       if (signature!=null && !signature.equals(""))
       {
         //Insert Blob into database
         BLOB blob;
         PreparedStatement pstmt;
         cm.connect();
         Connection connection = cm.getConnection() ;

         String selectQuery="SELECT * FROM shareholder_temp where folio_no = ? for update";
         pstmt = connection.prepareStatement(selectQuery);

         pstmt.setString(1, foliono);
         connection.setAutoCommit(false);

         ResultSet rset=pstmt.executeQuery();
         rset.next();


         //Use the OracleDriver resultset, we take the blob locator
         blob = ((OracleResultSet) rset).getBLOB("SIGNATURE");
         OutputStream os = blob.getBinaryOutputStream();

         //Read the file by chuncks and insert them in the Blob. The chunk size come from the blob
         byte[] chunk = new byte[blob.getChunkSize()];
         int i = -1;

         System.out.println("SIGNATURE : "+signature);
         File fullFile = new File(signature);

         FileInputStream is = new FileInputStream(fullFile );
         while ((i = is.read(chunk)) != -1) {
           os.write(chunk, 0, i); //Write the chunk
           System.out.print('.'); // print progression
         }

         is.close();
         os.close();
         //Close the statement and commit
         pstmt.close();
         connection.commit();
         connection.setAutoCommit(true);
         //cm.takeDown();
       }

       //INSERTS JOINT1 SIGNATURE
       if (jointsignature!=null && !jointsignature.equals(""))
       {
         //Insert Blob into database
         BLOB blob;
         PreparedStatement pstmt;
         cm.connect();
         Connection connection = cm.getConnection() ;

         String selectQuery="SELECT * FROM JOINT_N_TEMP where folio_no = ? and joint=1 for update";
         pstmt = connection.prepareStatement(selectQuery);

         pstmt.setString(1, foliono);

         connection.setAutoCommit(false);

         ResultSet rset=pstmt.executeQuery();
         rset.next();


         //Use the OracleDriver resultset, we take the blob locator
         blob = ((OracleResultSet) rset).getBLOB("SIGNATURE");
         OutputStream os = blob.getBinaryOutputStream();

         //Read the file by chuncks and insert them in the Blob. The chunk size come from the blob
         byte[] chunk = new byte[blob.getChunkSize()];
         int i = -1;

        // System.out.println("SIGNATURE Joint1: "+jointsignature);
         File fullFile = new File(jointsignature);

         FileInputStream is = new FileInputStream(fullFile );
         while ((i = is.read(chunk)) != -1) {
           os.write(chunk, 0, i); //Write the chunk
           System.out.print('.'); // print progression
         }

         is.close();
         os.close();
         //Close the statement and commit
         pstmt.close();
         connection.commit();
         connection.setAutoCommit(true);
         //cm.takeDown();
       }


       //INSERTS JOINT2 SIGNATURE
       if (joint2signature!=null && !joint2signature.equals(""))
       {
         //Insert Blob into database
         BLOB blob;
         PreparedStatement pstmt;
         cm.connect();
         Connection connection = cm.getConnection() ;

         String selectQuery="SELECT * FROM JOINT_N_TEMP where folio_no = ? and joint=2 for update";
         pstmt = connection.prepareStatement(selectQuery);

         pstmt.setString(1, foliono);

         connection.setAutoCommit(false);

         ResultSet rset=pstmt.executeQuery();
         rset.next();


         //Use the OracleDriver resultset, we take the blob locator
         blob = ((OracleResultSet) rset).getBLOB("SIGNATURE");
         OutputStream os = blob.getBinaryOutputStream();

         //Read the file by chuncks and insert them in the Blob. The chunk size come from the blob
         byte[] chunk = new byte[blob.getChunkSize()];
         int i = -1;

         System.out.println("SIGNATURE Joint2: "+joint2signature);
         File fullFile = new File(joint2signature);

         FileInputStream is = new FileInputStream(fullFile );
         while ((i = is.read(chunk)) != -1) {
           os.write(chunk, 0, i); //Write the chunk
           System.out.print('.'); // print progression
         }

         is.close();
         os.close();
         //Close the statement and commit
         pstmt.close();
         connection.commit();
         connection.setAutoCommit(true);
         //cm.takeDown();
       }

       //*********** PHOTO SECTION ************************
       //This section inserts photo into database
       if (photo!=null && !photo.equals(""))
       {
         //System.out.println("PHOTO : "+photo);
         //Insert Blob into database
         BLOB blob;
         PreparedStatement pstmt;
         cm.connect();
         Connection connection = cm.getConnection() ;

         String selectQuery="SELECT PHOTO FROM shareholder_temp where folio_no = '"+ foliono+"' for update";
         pstmt = connection.prepareStatement(selectQuery);


         //pstmt.setString(1, foliono);

         connection.setAutoCommit(false);


         ResultSet rset=pstmt.executeQuery();


         rset.next();

         //Use the OracleDriver resultset, we take the blob locator
         blob = ((OracleResultSet) rset).getBLOB("PHOTO");
         System.out.println(blob);
         OutputStream os = blob.getBinaryOutputStream();

         //Read the file by chuncks and insert them in the Blob. The chunk size come from the blob
         byte[] chunk = new byte[blob.getChunkSize()];
         int i = -1;


         File fullFile = new File(photo);

         FileInputStream is = new FileInputStream(fullFile );
         while ((i = is.read(chunk)) != -1) {
           os.write(chunk, 0, i); //Write the chunk
           System.out.print('.'); // print progression
         }

         is.close();
         os.close();
         //Close the statement and commit
         pstmt.close();
         connection.commit();
         connection.setAutoCommit(true);
         //cm.takeDown();
       }

 //INSERTS JOINT 2 PHOTO
       if (jointphoto!=null && !jointphoto.equals(""))
       {
         System.out.println("PHOTO joint 2 : "+jointphoto);

         BLOB blob;
         PreparedStatement pstmt;
         cm.connect();
         Connection connection = cm.getConnection() ;

         String selectQuery="SELECT PHOTO FROM JOINT_N_TEMP where folio_no = '"+ foliono+"' and joint = 1 for update";
         pstmt = connection.prepareStatement(selectQuery);



         connection.setAutoCommit(false);


         ResultSet rset=pstmt.executeQuery();


         rset.next();

         //Use the OracleDriver resultset, we take the blob locator
         blob = ((OracleResultSet) rset).getBLOB("PHOTO");
         System.out.println(blob);
         OutputStream os = blob.getBinaryOutputStream();

         //Read the file by chuncks and insert them in the Blob. The chunk size come from the blob
         byte[] chunk = new byte[blob.getChunkSize()];
         int i = -1;


         File fullFile = new File(jointphoto);

         FileInputStream is = new FileInputStream(fullFile );
         while ((i = is.read(chunk)) != -1) {
           os.write(chunk, 0, i); //Write the chunk
           System.out.print('.'); // print progression
         }

         is.close();
         os.close();
         //Close the statement and commit
         pstmt.close();
         connection.commit();
         connection.setAutoCommit(true);
         //cm.takeDown();
       }



       //INSERTS JOINT 2 PHOTO
       if (joint2photo!=null && !joint2photo.equals(""))
       {
         System.out.println("PHOTO joint 1 : "+joint2photo);

         BLOB blob;
         PreparedStatement pstmt;
         cm.connect();
         Connection connection = cm.getConnection() ;

         String selectQuery="SELECT PHOTO FROM JOINT_N_TEMP where folio_no = '"+ foliono+"' and joint = 2 for update";
         pstmt = connection.prepareStatement(selectQuery);



         connection.setAutoCommit(false);


         ResultSet rset=pstmt.executeQuery();


         rset.next();

         //Use the OracleDriver resultset, we take the blob locator
         blob = ((OracleResultSet) rset).getBLOB("PHOTO");
         System.out.println(blob);
         OutputStream os = blob.getBinaryOutputStream();

         //Read the file by chuncks and insert them in the Blob. The chunk size come from the blob
         byte[] chunk = new byte[blob.getChunkSize()];
         int i = -1;


         File fullFile = new File(joint2photo);

         FileInputStream is = new FileInputStream(fullFile );
         while ((i = is.read(chunk)) != -1) {
           os.write(chunk, 0, i); //Write the chunk
           System.out.print('.'); // print progression
         }

         is.close();
         os.close();
         //Close the statement and commit
         pstmt.close();
         connection.commit();
         connection.setAutoCommit(true);
//         cm.takeDown();
       }


     }//end try
     catch(Exception ex)
     {
       System.out.println("Error Message : "+ex.getMessage());
       StackTraceElement[] st  = ex.getStackTrace();
       for(int i=0;i<st.length;i++)
       System.out.println(st[i]);
     }
   }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added Folio Information')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript" >
  location = "<%=request.getContextPath()%>/jsp/Folio/AllFolios.jsp";
</script>
</body>
</html>
