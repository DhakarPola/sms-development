

<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Updated By    : Renad Hakim
'Creation Date : December 2005
'Page Purpose  : Adds first Joint2 Share Holder .
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.io.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*"%>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {
        color: #0A2769;
        font-weight: bold;
	font-size: 13px;
        }
.style12 {color: #FFFFFF}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Add Second Joint Shareholder Details</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function editSignatureWindow()
{
  	window.open("EditJoint2Signature.jsp","","HEIGHT=350,WIDTH=500")
}
function editPhotoWindow()
{
        window.open("EditJoint2Photo.jsp","","HEIGHT=350,WIDTH=500")
}
function getFolio()
{
  return window.opener.document.forms[0].foliono.value
}
//Execute while click on Submit
function SubmitThis() {

 // BlankValidate('joint2name','- Joint Name (Option must be entered)');

  //Enter the values in NewFolio.jsp hidden fields
  if(document.forms[0].joint2name.value == null || document.forms[0].joint2name.value == "" )
  {
    alert("Please enter Joint Holder's name");
  }
  else
  {
    window.opener.document.forms[0].joint2prefix.value = document.forms[0].joint2prefix.value
    window.opener.document.forms[0].joint2name.value = document.forms[0].joint2name.value
    window.opener.document.forms[0].joint2relation.value = document.forms[0].joint2relation.value
    window.opener.document.forms[0].joint2father.value = document.forms[0].joint2father.value
    window.opener.document.forms[0].joint2occupation.value = document.forms[0].joint2occupation.value
    window.opener.document.forms[0].joint2telephone.value = document.forms[0].joint2telephone.value
    window.opener.document.forms[0].joint2fax.value = document.forms[0].joint2fax.value
    window.opener.document.forms[0].joint2email.value = document.forms[0].joint2email.value
    window.opener.document.forms[0].joint2address.value = document.forms[0].joint2address.value
    window.opener.document.forms[0].joint2contactperson.value = document.forms[0].joint2contactperson.value
    window.opener.document.forms[0].joint2remarks.value = document.forms[0].joint2remarks.value
    window.opener.document.forms[0].joint2signature.value = document.forms[0].joint2signature.value
    window.opener.document.forms[0].joint2photo.value = document.forms[0].joint2photo.value
    window.opener.document.forms[0].joint2photostatus.value = document.forms[0].joint2photostatus.value
    window.opener.document.forms[0].joint2signaturestatus.value = document.forms[0].joint2signaturestatus.value
    self.close()
  }
}
//-->
</script>
<style type="text/css">
<!--
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String foliono = request.getParameter("hfoliono");
    String dbstatus = request.getParameter("dbstatus");

    foliono=  foliono.trim();
    dbstatus = dbstatus.trim();

    String tablename="";

    String joint2prefix ="";
    String joint2name = "";
    String joint2relation = "";
    String joint2father = "";
    String joint2occupation = "";
    String joint2telephone = "";
    String joint2fax = "";
    String joint2email = "";
    String joint2address = "";
    String joint2address2 = "";
    String joint2address3 = "";
    String joint2address4 = "";
    String joint2contactperson = "";
    String joint2remarks = "";
    String joint2signature ="";
    String joint2photo = "";

    if(dbstatus!=null && dbstatus.equals("actual"))
    tablename = "JOINT_N_VIEW";
  else
    tablename = "JOINT_N_TEMP_VIEW";

      String queryJoint2 = "SELECT * FROM " +tablename +" WHERE FOLIO_NO = '" + foliono + "' AND JOINT = 2" ;


     cm.queryExecute(queryJoint2);
      while(cm.toNext())
        {
          joint2prefix=cm.getColumnS("PREFIX");
          joint2name= cm.getColumnS("NAME");
          joint2relation = cm.getColumnS("RELATION");
          joint2father = cm.getColumnS("FATHER");
          joint2occupation = cm.getColumnS("OCCUPATION");
          joint2telephone = cm.getColumnS("TELEPHONE");
          joint2fax = cm.getColumnS("FAX");
          joint2email = cm.getColumnS("EMAIL");
          joint2address= cm.getColumnS("ADDRESS1");

          joint2contactperson = cm.getColumnS("CONTACTPERSON");
          joint2remarks = cm.getColumnS("REMARKS");


          if (String.valueOf(joint2prefix).equals("null"))
          joint2prefix = "";
            if (String.valueOf(joint2name).equals("null"))
           joint2name= "";
            if (String.valueOf(joint2relation).equals("null"))
           joint2relation= "";
            if (String.valueOf(joint2father).equals("null"))
           joint2father= "";
            if (String.valueOf(joint2occupation).equals("null"))
           joint2occupation= "";

            if (String.valueOf(joint2telephone).equals("null"))
           joint2telephone= "";
            if (String.valueOf(joint2fax).equals("null"))
           joint2fax= "";
            if (String.valueOf(joint2email).equals("null"))
           joint2email= "";
            if (String.valueOf(joint2address).equals("null"))
           joint2address= "";
            if (String.valueOf(joint2remarks).equals("null"))
           joint2remarks= "";
            if (String.valueOf(joint2contactperson).equals("null"))
           joint2contactperson= "";

        }
%>

<form action="EditJoint2.jsp" method="get" name="FileForm">
  <span class="style7">
  <table width="100%"  border="1" bordercolor="#06689E" style="border-collapse: collapse" cellpadding="0">
    <tr>
      <td><table width="100%"  border="0" cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
        <tr bgcolor="#E8F3FD">
          <td bgcolor="#0044B0" class="style7">Joint One Details</td>
          <td bgcolor="#0044B0" class="style7">&nbsp;</td>
          <td bgcolor="#0044B0" class="style7">&nbsp;</td>
          </tr>
       <tr bgcolor="#E8F3FD">
          <td width="44%" class="style8"><br>Prefix</td>
          <td width="1%"><br>:</td>
          <td width="55%"><br><input name="joint2prefix" type="text" class="SL2TextField" id="joint2prefix" value="<%=joint2prefix%>"></td>
       </tr>
                          <tr bgcolor="#E8F3FD">
                            <td class="style8">Joint Name</td>
                            <td>:</td>
                            <td><input name="joint2name" type="text" class="SL2TextField" id="joint2name" value="<%=joint2name%>"></td>
                          </tr>
                          <tr bgcolor="#E8F3FD">
                            <td class="style8">Relation</td>
                            <td>:</td>
                            <td>
                                <select name="joint2relation" class="SL1TextFieldListBox">
                                    <%
                                    joint2relation = joint2relation.trim();
                                    String values1[] = {"--Please Select--","S/O","W/O","D/O"};
                                    for(int i=0;i<values1.length;i++)
                                    {
                                      if(joint2relation.equals(values1[i]))
                                      {
                                        %>
                                        <option value="<%=values1[i]%>" selected="selected"><%=values1[i]%></option>
                                        <%
                                        }
                                        else
                                        {
                                        %>
                                        <option value="<%=values1[i]%>" ><%=values1[i]%></option>
                                        <%
                                        }
                                      }
                                  %>
                                </select>
                            </td>
                          </tr>
                          <tr bgcolor="#E8F3FD">
                            <td class="style8">Father/Husband</td>
                            <td>:</td>
                            <td><input name="joint2father" type="text" class="SL2TextField" id="joint2father" value="<%=joint2father%>"></td>
                          </tr>
                          <tr bgcolor="#E8F3FD">
                            <td class="style8">Occupation</td>
                            <td>:</td>
                            <td><input name="joint2occupation" type="text" class="SL2TextField" id="joint2occupation" value="<%=joint2occupation%>"></td>
                          </tr>
                          <tr bgcolor="#E8F3FD">
                            <td class="style8">Telephone</td>
                            <td>:</td>
                            <td><input name="joint2telephone" class="SL2TextField" type="text" id="joint2telephone" value="<%=joint2telephone%>"></td>
                          </tr>
                         <tr bgcolor="#E8F3FD">
                           <td class="style8">Fax</td>
                            <td>:</td>
                           <td><input name="joint2fax" type="text" class="SL2TextField" id="joint2fax" value="<%=joint2fax%>"></td>
                         </tr>
                         <tr bgcolor="#E8F3FD">
                           <td class="style8">E-mail</td>
                            <td>:</td>
                           <td><input name="joint2email" type="text" class="SL2TextField" id="joint2email" value="<%=joint2email%>"></td>
                         </tr>
                         <tr bgcolor="#E8F3FD">
                           <td class="style8" valign="top">Address</td>
                            <td valign="top">:</td>
                           <td><textarea name="joint2address" class="ML9TextField" id="joint2address"><%=joint2address%></textarea></td>
                         </tr>
                         <tr bgcolor="#E8F3FD">
                           <td class="style8">Contact Person</td>
                            <td>:</td>
                           <td><input name="joint2contactperson" type="text" class="SL2TextField" id="joint2contactperson" value="<%=joint2contactperson%>"></td>
                         </tr>
                         <tr bgcolor="#E8F3FD">
                           <td class="style8" valign="top">Remarks</td>
                            <td valign="top">:</td>
                           <td><textarea name="joint2remarks" class="ML9TextField" id="joint2remarks"><%=joint2remarks%>
</textarea></td>
                         </tr>
                         <tr bgcolor="#E8F3FD">
                           <td class="style8" valign="top">Signature (*.gif)</td>
                            <td valign="top">:</td>
                                    <%
                                       if(dbstatus!=null && dbstatus.equals("actual"))
                                            tablename = "JOINT_N_VIEW";
                                          else
                                            tablename = "JOINT_N_TEMP_VIEW";

                                        String selectQuery="SELECT SIGNATURE FROM " + tablename + " WHERE folio_no = '" + foliono + "'  AND JOINT=2 ";
                                        byte[] imgData =  null;


                                        if(!joint2name.equals(""))
                                         {
                                           imgData =  cm.getPhoto( cm.getConnection(), selectQuery  );
                                           if(imgData.length == 0)
                                            imgData = null;
                                         }


                                    %>
                                    <td><table width="149">
                                      <tr><td width="41">
                                      <%
                                         if(imgData != null)
                                         {
                                      %>
                                        <img alt="Joint 2 Signature" src="image.jsp?selectQuery=<%=selectQuery%>" /></td>
                                        <%
                                         }
                                         else
                                         {
                                         %>
                                         <b class="style8">(Signature Unavailable)</b>
                                           <%
                                           }
                                         %>
                                           <td width="96">
<!--                                             <input type="button" name="ButtonSignature" value="Edit" onClick="editSignatureWindow()">-->
                                           &nbsp;<img name="B4" src="<%=request.getContextPath()%>/images/btnEdit.gif"  onclick="editSignatureWindow()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnEditR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnEdit.gif'">
                                           <input type="hidden" name="joint2signature">
                                           <input type="hidden" name="joint2signaturestatus" value="edit"/>
                                                    </td>
                                      </tr></table></td>
                         </tr>
                         <tr bgcolor="#E8F3FD">
                            <td width="22%" class="style8" valign="top">Photo (*.gif)</td>
                            <td valign="top">:</td>
                                  <%
                                      if(dbstatus!=null && dbstatus.equals("actual"))
                                            tablename = "JOINT_N_VIEW";
                                          else
                                            tablename = "JOINT_N_TEMP_VIEW";

                                         selectQuery="SELECT PHOTO FROM " + tablename +" WHERE folio_no = '" + foliono + "'  AND JOINT=2 ";

                                        imgData = null;
                                         if(!joint2name.equals(""))
                                         {
                                           imgData =  cm.getPhoto( cm.getConnection(), selectQuery  );
                                           if(imgData.length == 0)
                                            imgData = null;
                                         }


                                  %>
                                  <td width="78%"><table width="149">
                                    <tr><td width="51">
                                    <%
                                       if(imgData != null)
                                       {
                                    %>
                                      <img alt="Share Holder Photo" src="image.jsp?selectQuery=<%=selectQuery%>" /></td>
                                      <%
                                       }
                                       else
                                       {
                                       %>
                                       <b class="style8">(Photo Unavailable)</b>
                                         <%
                                         }
                                       %>
                                         <td width="86">
<!--                                           <input type="button" name="ButtonPhoto" value="Edit" onClick="editPhotoWindow()">-->
                                           &nbsp;<img name="B5" src="<%=request.getContextPath()%>/images/btnEdit.gif"  onclick="editPhotoWindow()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnEditR.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnEdit.gif'">
                                         <input type="hidden" name="joint2photo">
                                         <input type="hidden" name="joint2photostatus" value="edit"/>
                                        </td>
                  </tr></table></td>
                         </tr>
    <table width="100%" BORDER=0  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
      <tr bgcolor="#E8F3FD">
        <td align="center" valign="top"><br>
 		<img name="B3" src="<%=request.getContextPath()%>/images/btnCancel.gif"  onclick="self.close()" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnCancelOn.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnCancel.gif'">
        </td>
        <td align="center" valign="top"><br>
                <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
        </td>
        <td align="center" valign="top"><br>
	       <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
               <br>&nbsp;
      </td>
      </tr>
</table>

			</table>

                        </td>
                    </tr>
  </table>
  </span>
  <br>
      </div>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>


