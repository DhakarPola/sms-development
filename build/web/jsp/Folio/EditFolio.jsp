<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Updated by    : Md. Kamruzzaman
'Creation Date : October 2005
'Page Purpose  : Edits Folio Information.
'Updated By Masud Ahmad for Certification functionality
'Updated By Renad Hakim for GUI Enhancement and Bug Fixing.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.io.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*"%>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<jsp:useBean id="util" class="batbsms.Utility"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
</LINK><%if (String.valueOf(session.getAttribute("UserName")).equals("null")) {%>
<script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%}%>
<html>
<head>

<script language="javascript">
<!--
function editSignatureWindow()
{
  	window.open("EditSignature.jsp","","HEIGHT=350,WIDTH=500")
}
function CertificateInfo()
{
  	var windowname = "CertificateInfo.jsp?hfoliono=" + document.forms[0].hfoliono.value;

        window.open(windowname,"","HEIGHT=290,WIDTH=570")
       //var myref= window.open('',windowname,'left=100,top=100,width=570,height=290,toolbar=1,resizable=0');
      // return myref;
}
function editPhotoWindow()
{
        window.open("EditPhoto.jsp","","HEIGHT=350,WIDTH=500")
}
function openNomineeWindow()
{
	window.open("NomineeList.jsp","","HEIGHT=500,WIDTH=500,SCROLLBAR=YES")
}
function openJoint1Edit()
{

      var windowname = "EditJoint1.jsp?hfoliono=" + document.forms[0].hfoliono.value+" &dbstatus="+document.forms[0].dbstatus.value;

      var win = window.open(windowname,"","HEIGHT=680,WIDTH=500");

      return win;
}
function openJoint2Edit()
{

      var windowname = "EditJoint2.jsp?hfoliono=" + document.forms[0].hfoliono.value+" &dbstatus="+document.forms[0].dbstatus.value;

      var win = window.open(windowname,"","HEIGHT=680,WIDTH=500");

      return win;
}

function showText()
{
    if(document.forms[0].boxsptax.checked==true)
    {
      document.forms[0].spTax.style.display='';
    }
    else
    {
      document.forms[0].spTax.style.display="none";
    }
}

//-->
</script>
<%
  boolean isConnect = cm.connect();
  if (isConnect == false) {
%>
<jsp:forward page="ErrorMsg.jsp">
  <jsp:param name="ErrorTitle" value="Connection Failure"/>
  <jsp:param name="ErrorHeading" value="Connection Problem"/>
  <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator"/>
</jsp:forward>
<%
  }
  String sfolioID = request.getParameter("fno");

  int joint=0;

  String category = "";
  String prefix = "";
  String shareholdername = "";
  String surname = "";
  String relation = "";
  String father = "";
  String resident = "";
  String activity = "";
  String nationality = "";
  String occupation = "";
  String address1 = "";
  String address2 = "";
  String zipcode = "";
  String telephone = "";
  String cellphone = "";
  String email = "";
  String fax = "";
  String address3 = "";
  String address4 = "";
  String accountno = "";
  String bankname = "";
  String bankbranch = "";
  String tin = "";
  String taxfreeshare = "";
  String entrydate = "";
  String lastModifiedDate="";
  String nomineeno = "";
  String dbstatus = request.getParameter("dbstatus");
  String tablename="";
  String ndetails="";
  int total_shares=0;
  String sisliable = "";
  String sissptax = "";
  String nid = "";
  String routingno = "";

  String jointprefix ="";
  String jointname = "";
  String jointrelation = "";
  String jointfather = "";
  String jointoccupation = "";
  String jointtelephone = "";
  String jointfax = "";
  String jointemail = "";
  String jointaddress = "";
  String jointaddress2 = "";
  String jointaddress3 = "";
  String jointaddress4 = "";
  String jointcontactperson = "";
  String jointremarks = "";



  String joint2prefix ="";
  String joint2name = "";
  String joint2relation = "";
  String joint2father = "";
  String joint2occupation = "";
  String joint2telephone = "";
  String joint2fax = "";
  String joint2email = "";
  String joint2address = "";
  String joint2address2 = "";
  String joint2address3 = "";
  String joint2address4 = "";
  String joint2contactperson = "";
  String joint2remarks = "";


  String queryFolio;//SHAREHOLDER_TEMP

  if(dbstatus!=null && dbstatus.equals("actual"))
    tablename = "SHAREHOLDER";
  else
    tablename = "SHAREHOLDER_TEMP";

 String specailTax_q="select amount from special_tax where folio_no='" + sfolioID + "'";
 cm.queryExecute(specailTax_q);
 cm.toNext();
 String specialTaxVal=cm.getColumnS("amount");
 queryFolio = "SELECT * FROM " + tablename + " WHERE FOLIO_NO = '" + sfolioID+"'";

  cm.queryExecute(queryFolio);
  while (cm.toNext()) {
    joint  = cm.getColumnI("JOINT");

    ndetails = cm.getColumnS("NOMINEE_DETAILS");
    category = cm.getColumnS("CLASS");
    prefix = cm.getColumnS("PREFIX");
    shareholdername = cm.getColumnS("NAME");
    surname = cm.getColumnS("SURNAME");
    relation = cm.getColumnS("RELATION");
    father = cm.getColumnS("FATHER");
    resident = cm.getColumnS("RESIDENT");
    activity = cm.getColumnS("ACTIVE");
    nationality = cm.getColumnS("NATIONALITY");
    occupation = cm.getColumnS("OCCUPATION");
    address1 = cm.getColumnS("ADDRESS1");
    address2 = cm.getColumnS("ADDRESS2");
    zipcode = cm.getColumnS("ZIP");

    telephone = cm.getColumnS("TELEPHONE");
    cellphone = cm.getColumnS("CELLPHONE");
    email = cm.getColumnS("EMAIL");
    fax = cm.getColumnS("FAX");

    address3 = cm.getColumnS("ADDRESS3");
    address4 = cm.getColumnS("ADDRESS4");
    accountno = cm.getColumnS("ACCOUNT_NO");
    bankname = cm.getColumnS("BANK_NAME");
    bankbranch = cm.getColumnS("BANK_BRANCH");
    tin = cm.getColumnS("TIN");
	nid = cm.getColumnS("NID"); // Added new 01 10 2014
	routingno = cm.getColumnS("ROUTING_NO"); // Added new 01 10 2014
    taxfreeshare = cm.getColumnS("TAX_FREE_SHARE");
    entrydate = cm.getColumnS("ENTRY_DATE");

    total_shares = cm.getColumnI("TOTAL_SHARE");
    sisliable = cm.getColumnS("LIABLE_TO_TAX");
    sissptax = cm.getColumnS("SPECIAL_TAX");

    if(entrydate!=null)
    {
      entrydate = util.changeDateFormat(entrydate,"MM/dd/yyyy h:m:s","dd/MM/yyyy");
    }
    if(dbstatus!=null && dbstatus.equals("actual")) {
      lastModifiedDate=cm.getColumnS("LAST_UPDATED_DATE");
      if(lastModifiedDate!=null && !lastModifiedDate.equals("null") && !lastModifiedDate.equals("")) {
        lastModifiedDate = util.changeDateFormat(lastModifiedDate,"MM/dd/yyyy h:m:s","dd/MM/yyyy");
      }
      else
        lastModifiedDate="";
    }
    nomineeno = cm.getColumnS("NOMINEE_NO");
    tablename="";

    if (String.valueOf(ndetails).equals("null"))
          ndetails = "";
    if (String.valueOf(category).equals("null"))
          category = "";
    if (String.valueOf(prefix).equals("null"))
          prefix = "";
    if (String.valueOf(shareholdername).equals("null"))
         shareholdername  = "";
    if (String.valueOf(surname).equals("null"))
          surname = "";
    if (String.valueOf(relation).equals("null"))
          relation = "";
    if (String.valueOf(father).equals("null"))
          father = "";
    if (String.valueOf(resident).equals("null"))
           resident= "";
    if (String.valueOf(sisliable).equals("null"))
           sisliable= "";
    if (String.valueOf(sissptax).equals("null"))
           sissptax= "";
    if (String.valueOf(nationality).equals("null"))
         nationality  = "";
    if (String.valueOf(occupation).equals("null"))
        occupation   = "";
    if (String.valueOf(address1).equals("null"))
         address1  = "";
    if (String.valueOf(zipcode).equals("null"))
          zipcode = "";
    if (String.valueOf(telephone).equals("null"))
          telephone = "";
    if (String.valueOf(accountno).equals("null"))
          accountno = "";
    if (String.valueOf(fax).equals("null"))
          fax = "";
    if (String.valueOf(taxfreeshare).equals("null"))
          taxfreeshare = "";
    if (String.valueOf(bankname).equals("null"))
          bankname = "";
    if (String.valueOf(entrydate).equals("null"))
          entrydate = "";
    if (String.valueOf(nomineeno).equals("null"))
          nomineeno = "";
    if (String.valueOf(bankbranch).equals("null"))
          bankbranch = "";
    if (String.valueOf(cellphone).equals("null"))
          cellphone = "";
   if (String.valueOf(email).equals("null"))
          email = "";
   if (String.valueOf(tin).equals("null"))
          tin = "";
  if (String.valueOf(address2).equals("null"))
          address2 = "";
  if (String.valueOf(address3).equals("null"))
          address3 = "";
  if (String.valueOf(address4).equals("null"))
          address4 = "";
  
  if (String.valueOf(nid).equals("null"))  // Added new 01 10 2014
          nid = "";
  if (String.valueOf(routingno).equals("null"))  // Added new 01 10 2014
          routingno = "";

   address1 = address1.trim() + "\n" +address2.trim() + "\n" + address3.trim() + "\n" + address4.trim();

   //************Trims whitespaces************

        category = category.trim();
        prefix = prefix.trim();
        shareholdername  = shareholdername.trim();
        surname = surname.trim();
        relation = relation.trim();
        father = father.trim();
        resident= resident.trim();
        nationality  = nationality.trim();
        occupation   = occupation.trim();
        address1  = address1.trim();
        zipcode = zipcode.trim();
        telephone = telephone.trim();
        accountno = accountno.trim();
        fax = fax.trim();
        taxfreeshare = taxfreeshare.trim();
        bankname = bankname.trim();
        entrydate = entrydate.trim();
        nomineeno = nomineeno.trim();
        bankbranch = bankbranch.trim();
        cellphone = cellphone.trim();
        email = email.trim();
        tin = tin.trim();
		
		nid = nid.trim();
		routingno = routingno.trim();

   //***********************************************
  }

%>
<style type="text/css">
  <!--
    body {
    background-color: #FFFFFF;
    }
    body,td,th {
    color: #000000;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
    }
  -->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit Folio</title>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css">
</LINK><script language="javascript">
<!--
function gotofolio()
{
  location = "<%=request.getContextPath()%>/jsp/Folio/AllFolios.jsp";
}
function gotoPermanentFolios()
{
    location = "<%=request.getContextPath()%>/jsp/Folio/AllPermanentFolios.jsp";
}

function gotodelete()
{
  if (confirm("Do you want to delete the Folio Information?"))
  {
    location = "<%=request.getContextPath()%>/jsp/Folio/DeleteFolio.jsp?bid2=<%=sfolioID%>";
  }
}

function formconfirm()
{
  if (confirm("Do you want to change the Folio Information?"))
  {
    return true;
  }
}

//Execute while click on Submit
function SubmitThis() {
  //alert(document.forms[0].signature.value);
  //return;
  count = 0;
  BlankValidate('shareholdername','-  Share Holder Name(Option must be entered)');
  if(document.forms[0].boxsptax.checked==true)
    {
      BlankValidate('spTax','- Special Tax Value (Option must be entered)');
    }
    //alert(document.forms[0].foliono.value);
     //alert(document.forms[0].foliono.value.length);
   if(document.forms[0].foliono.value.length == 8)
  {
    if (count == 0)
    {
      if (formconfirm())
      {
        document.forms[0].submit();
      }
    }
    else{
      ShowAllAlertMsg();
      return false;
    }
  }
  else
  {
    alert("Folio Number should be 8 characters long");
  }
}

//-->
</script>
<style type="text/css">
  <!--
    .style7 {
    color: #FFFFFF;
    font-weight: bold;
    font-size: 13px;
    }
    .style8 {color: #0A2769;
            font-weight: bold;}
    .style9 {color: #0A2769; font-weight: bold; }
  -->
</style>
</head>
<body TEXT="000000" BGCOLOR="FFFFFF">
<form action="UpdateFolio.jsp" method="get" name="FileForm">

  <span class="style7">
<table width="100%" border="1" cellspacing="0" cellpadding="0" style="border-collapse:collapse" bordercolor="#0044B0" bgcolor="#E8F3FD">
  <tr>
    <td>
	<table width="100%" border="0" cellspacing="0" cellpadding="5" >
      <tr>
        <td bgcolor="#0044B0" class="style7" height="30">Share Holder's Personal Information</td>
      </tr>
      <tr bgcolor="#E8F3FD">
        <td height="100%" bgcolor="#E8F3FD"><center>
          <br>
          <div align="left">
            <table width="100%" height="100%" border="0" cellpadding="5">
              <tr>
                <td width="32%" class="style8">Folio</td>
                <td width="1%">:</td>
                <td width="67%"><input name="foliono" type="text" maxlength="8" class="SL2TextField" value="<%=sfolioID%>" readonly="readonly">
                    <input type="hidden" name="dbstatus" value="<%=dbstatus%>"/>
                    <input type="hidden" name = "hfoliono" value="<%=sfolioID%>"/>

                      <!-- Joint 1 hidden fields-->
                  <input type="hidden" name="jointprefix" />
                  <input type="hidden" name="jointname"  />
                  <input type="hidden" name="jointrelation" />
                  <input type="hidden" name="jointfather" />
                  <input type="hidden" name="jointoccupation" />
                  <input type="hidden" name="jointtelephone" />
                  <input type="hidden" name="jointfax" />
                  <input type="hidden" name="jointemail" />
                  <input type="hidden" name="jointaddress" />
                  <input type="hidden" name="jointcontactperson" />
                  <input type="hidden" name="jointremarks" />
                  <input type="hidden" name="joint1signature" />
                  <input type="hidden" name="joint1photo" />
                  <input  type="hidden" name="joint1photostatus"/>
                  <input  type="hidden" name="joint1signaturestatus"/>

                <!-- End of Joint 1 fields-->
                <!-- Joint 2 hidden fields-->
                  <input type="hidden" name="joint2prefix" />
                  <input type="hidden" name="joint2name"  />
                  <input type="hidden" name="joint2relation" />
                  <input type="hidden" name="joint2father" />
                  <input type="hidden" name="joint2occupation" />
                  <input type="hidden" name="joint2telephone" />
                  <input type="hidden" name="joint2fax" />
                  <input type="hidden" name="joint2email" />
                  <input type="hidden" name="joint2address" />
                  <input type="hidden" name="joint2contactperson" />
                  <input type="hidden" name="joint2remarks" />
                  <input type="hidden" name="joint2signature" />
                  <input type="hidden" name="joint2photo" />
                  <input  type="hidden" name="joint2photostatus"/>
                  <input  type="hidden" name="joint2signaturestatus"/>
                <!-- End of Joint 2 fields-->
                </td>
              </tr>
              <tr>
                <td  class="style8">Category</td>
                <td >:</td>
                <td>
                  <select name="category" class="SL1TextFieldListBox" >
                    <%
                      String catQuery="SELECT * FROM SHAREHOLDER_TEMP_VIEW WHERE FOLIO_NO = '"+sfolioID+"'";

                      String catVal=null;
                      String catCode=null;
                      try
                      {
                        ResultSet rs1 = cm.queryExecuteGetResultSet(catQuery);
                        String cat=null;
                        while(rs1.next())
                        {
                          cat =  cm.getColumnS("CLASS").trim();
                        }

                        String categoryQuery="SELECT \"CATEGORY\",CODE FROM CATEGORY_VIEW";
                        ResultSet rs= cm.queryExecuteGetResultSet(categoryQuery);
                        while(rs.next())
                        {
                          catVal=cm.getColumnS("CATEGORY").trim();
                          catCode=cm.getColumnS("CODE").trim();

//                          if(catVal.equalsIgnoreCase(cat))
                          if(catCode.equalsIgnoreCase(category))
                          {
                            %>
                            <option value="<%=catCode%>" selected="selected"><%=catVal%></option>
                            <%
                            }
                            else
                            {
                              %>
                              <option value="<%=catCode%>" ><%=catVal%></option>
                              <%
                              }
                            //System.out.println("---------- " + catVal);
                            //System.out.println("---------- " + cat);

                            }

                          }
                          catch(Exception ex)
                          {
                            System.out.println("Error in Category.....");
                            System.out.println(ex.getMessage());
                          }

                  %>
                  </select>
                </td>
              </tr>
              <tr>
                <td  class="style8">Prefix</td>
                <td >:</td>
                <td>
                  <select name="prefix" class="SL1TextFieldListBox">
                    <%
                      prefix = prefix.trim();
                      String values[] = {"--Please Select--","Mr.","Mrs.","MISS","MS.","DR."};
                      for(int i=0;i<values.length;i++)
                      {
                        if(prefix.equalsIgnoreCase(values[i]))
                        {
                          %>
                          <option value="<%=values[i]%>" selected="selected"><%=values[i]%></option>
                          <%
                          }
                          else
                          {
                          %>
                          <option value="<%=values[i]%>" ><%=values[i]%></option>
                          <%
                          }
                        }
                    %>
                  </select>
                </td>
              </tr>
              <tr>
                <td height="37"  class="style8">Name</td>
                <td >:</td>
                <td><input name="shareholdername" type="text" class="SL2TextField" value="<%=shareholdername%>"></td>
              </tr>
              <tr>
                <td  class="style8">Surname</td>
                <td >:</td>
                <td><input name="surname" type="text" class="SL2TextField" value="<%=surname%>"></td>
              </tr>
              <tr>
                <td  class="style8">Relation</td>
                <td >:</td>
                <td>
                  <select name="relation" class="SL1TextFieldListBox">
                      <%
                      relation = relation.trim();
                      String values1[] = {"--Please Select--","S/O","W/O","D/O"};
                      for(int i=0;i<values1.length;i++)
                      {
                        if(relation.equalsIgnoreCase(values1[i]))
                        {
                          %>
                          <option value="<%=values1[i]%>" selected="selected"><%=values1[i]%></option>
                          <%
                          }
                          else
                          {
                          %>
                          <option value="<%=values1[i]%>" ><%=values1[i]%></option>
                          <%
                          }
                        }
                    %>
                  </select>
                </td>
              </tr>
              <tr>
                <td  class="style8">Father/Husband</td>
                <td >:</td>
                <td><input name="father" type="text" class="SL2TextField" value="<%=father%>"></td>
              </tr>
              <tr >
                <td  class="style8">Resident</td>
                <td >:</td>
                  <!--
                    Set Default value
                    -->
                <td>
                    <%
                      resident = resident.trim();
                      if(resident.equalsIgnoreCase("T"))
                      {
                      %>
                      <input name="resident" type="checkbox" checked="checked" value="T">
                      <%
                      }
                      else
                      {
                      %>
                        <input name="resident" type="checkbox" value="T">
                      <%
                      }
                      %>
                </td>
              </tr>
              <tr >
                <td  class="style8">Active</td>
                <td >:</td>
                  <!--
                    Set Default value
                    -->
                <td>
                    <%
                      activity = activity.trim();
                      if(activity.equalsIgnoreCase("T"))
                      {
                      %>
                      <input name="activity" type="checkbox" checked="checked" value="T">
                      <%
                      }
                      else
                      {
                      %>
                        <input name="activity" type="checkbox" value="T">
                      <%
                      }
                      %>
                </td>
              </tr>
              <tr>
                <td  class="style8">Nationality</td>
                <td >:</td>
                <td><input name="nationality" type="text" class="SL2TextField" value="<%=nationality%>">
              </tr>
              <tr>
                <td  class="style8">Occupation</td>
                <td >:</td>
                <td><input name="occupation" type="text" class="SL2TextField" value="<%=occupation%>"></td>
              </tr>
              <tr>
                <td valign="top" class="style8">Address</td>
                <td valign="top" >:</td>
                <td><textarea name="address1" class="ML9TextField"><%=address1%></textarea></td>
              </tr>
              <tr>
                <td  class="style8">Zip Code</td>
                <td >:</td>
                <td><input name="zipcode" type="text" class="SL2TextField" value="<%=zipcode%>"></td>
              </tr>
              <tr>
                <td  class="style8">Telephone</td>
                <td >:</td>
                <td><input name="telephone" type="text" class="SL2TextField" value="<%=telephone%>"></td>
              </tr>
              <tr>
                <td  class="style8">Mobile</td>
                <td >:</td>
                <td><input name="cellphone" type="text" class="SL2TextField" value="<%=cellphone%>"></td>
              </tr>
              <tr>
                <td  class="style8">E-mail</td>
                <td >:</td>
                <td><input name="email" type="text" class="SL2TextField" value="<%=email%>"></td>
              </tr>
              <tr>
                <td  class="style8">Fax</td>
                <td >:</td>
                <td><input name="fax" type="text" class="SL2TextField" value="<%=fax%>"></td>
              </tr>
	      <tr>
                <td  class="style8">Total Shares</td>
                <td >:</td>
                <td><input name="totalshares" type="text" class="SL2TextField" value="<%=total_shares%>"></td>
              </tr>
              <tr>
                <td valign="top" class="style8">Signature (*.gif)</td>
                <td valign="top" >:</td>
                <%
                   if(dbstatus!=null && dbstatus.equals("actual"))
                      tablename = "SHAREHOLDER";
                   else
                      tablename = "SHAREHOLDER_TEMP";

                  byte[] imgData =null;

                  String selectQuery="SELECT SIGNATURE FROM " + tablename + " WHERE folio_no = '"+sfolioID +"'";
                  try
                  {
                    imgData =  cm.getPhoto( cm.getConnection(), selectQuery  );
                  }
                  catch(Exception ex)
                  {
                        ;
                  }

                %>
                <td valign="top"><table width="149">
                  <tr><td width="50" valign="top">
                  <%
                     if(imgData.length!=0)
                     {
                  %>
                    <img alt="Share Holder Signature" src="image.jsp?selectQuery=<%=selectQuery%>" /></td>
                    <%
                     }
                     else
                     {
                     %>
                     <b class="style8">[Signature Unavailable]</b>
                       <%
                       }
                     %>
                       <td width="87" valign="top"><!--<input type="button" name="ButtonSignature" value="Edit" onClick="editSignatureWindow()">-->
			&nbsp;<img name="B6" src="<%=request.getContextPath()%>/images/btnEdit.gif"  onclick="editSignatureWindow()" onMouseOver="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnEditR.gif'" onMouseOut="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnEdit.gif'">
                       <input type="hidden" name="signature" id="signature">
                       <input type="hidden" name="signaturestatus" value="edit"/>
                        </td>
                  </tr></table></td>
              </tr>
              <tr>
                <td valign="top" class="style8">Photo (*.gif)</td>
                <td valign="top" >:</td>
                <%

                   if(dbstatus!=null && dbstatus.equals("actual"))
                      tablename = "SHAREHOLDER";
                   else
                      tablename = "SHAREHOLDER_TEMP";


                  selectQuery="SELECT PHOTO FROM " + tablename +" WHERE folio_no = '"+sfolioID +"'";

                  try
                  {
                    imgData =  cm.getPhoto( cm.getConnection(), selectQuery  );
                  }
                  catch(Exception ex)
                  {
                    ;
                  }

                %>
                <td width="78%" valign="top"><table width="149">
                  <tr><td>&nbsp;</td><td width="51" valign="top">
                  <%
                     if(imgData.length!=0)
                     {
                  %>
                    <img alt="Share Holder Photo" src="image.jsp?selectQuery=<%=selectQuery%>" /></td>
                    <%
                     }
                     else
                     {
                     %>
                     <b class="style8">[Photo Unavailable]</b>
                       <%
                       }
                     %>
                       <td width="86" valign="top"><!--<input type="button" name="ButtonPhoto" value="Edit" onClick="editPhotoWindow()">-->
			&nbsp;<img name="B7" src="<%=request.getContextPath()%>/images/btnEdit.gif"  onclick="editPhotoWindow()" onMouseOver="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnEditR.gif'" onMouseOut="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnEdit.gif'">
                       <input type="hidden" name="photo">
                       <input type="hidden" name="photostatus" value="edit"/>
                                </td>
                  </tr></table><br>&nbsp;</table>
                       <tr>
                         <td bgcolor="#0044B0" class="style7" height="30">Personal Details</td>
                       </tr>

                    <tr>
                      <td colspan="2">

                        <table width="100%" border="0" cellpadding="5">
                          <tr>
                            <td width="32%" class="style8">Account No.</td>
                            <td width="1%">:</td>
                            <td width="67%"><input name="accountno" type="text" class="SL2TextField" value="<%=accountno%>"></td>
                          </tr>
                          <tr>
                            <td class="style8">Bank</td>
                            <td >:</td>
                            <td><input name="bankname" type="text" class="SL2TextField" value="<%=bankname%>"></td>
                          </tr>
                          <tr>
                            <td class="style8">Branch</td>
                            <td >:</td>
                            <td><input name="bankbranch" type="text" class="SL2TextField" value="<%=bankbranch%>"></td>
                          </tr>
						  <tr>
                            <td class="style8">Routing Number</td>
                            <td >:</td>
                            <td><input name="routingno" type="text" class="SL2TextField" value="<%=routingno%>"></td>
                          </tr>
                          <tr>
                            <td class="style8">TIN</td>
                            <td >:</td>
                            <td><input name="tin" type="text" class="SL2TextField" value="<%=tin%>"></td>
                          </tr>
						  <tr>
                            <td class="style8">NID</td>
                            <td >:</td>
                            <td><input name="nid" type="text" class="SL2TextField" value="<%=nid%>"></td>
                          </tr>
                          <tr>
                            <td class="style8">Tax Free Share</td>
                            <td >:</td>
                            <td><input name="taxfreeshare" type="text" class="SL2TextField" value="<%=taxfreeshare%>"></td>
                          </tr>
                          <tr >
                            <td  class="style8">Liable to Tax</td>
                            <td >:</td>
                            <td>
                            <%
                            if(sisliable.equalsIgnoreCase("T"))
                            {
                              %>
                              <input name="boxliable" type="checkbox" checked="checked" value="T">
                              <%
                            }
                            else
                            {
                              %>
                              <input name="boxliable" type="checkbox" value="T">
                              <%
                            }
                            %>
                            </td>
                          </tr>
                          <tr >
                            <td  class="style8">Special Tax</td>
                            <td >:</td>
                            <td>
                            <%
                            if(sissptax.equalsIgnoreCase("T"))
                            {
                              %>
                              <input name="boxsptax" type="checkbox" onclick="showText()" checked="checked" value="T">&nbsp;<input id="spTax" type="text" name="spTax" class="SLSPTAXTextField" onkeypress="keypressOnDoubleFld()" value="<%=specialTaxVal%>"/>
                              <%
                            }
                            else
                            {
                              %>
                              <input name="boxsptax" type="checkbox" onclick="showText()">&nbsp;<input id="spTax" type="text" name="spTax" class="SLSPTAXTextField" style="display:none" onkeypress="keypressOnDoubleFld()" value="<%=specialTaxVal%>"/>
                              <%
                            }
                            %>
                            </td>
                          </tr>
                          <tr>
                            <td class="style8">Entry Date</td>
                            <td >:</td>
                            <td><div align="left">
                              <input name="entrydate" type=text id="entrydate" value="" maxlength="10" class="SL2TextField" onFocus="this.blur();">
                              &nbsp
                               <a href="javascript:NewCal('entrydate','ddmmyyyy',false,12)">
                               <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
                               </a>
                            </div></td>
                          </tr>
                          <tr>
                            <td class="style8" valign="top">Dividend Group No.</td>
                            <td valign="top">:</td>
                            <td valign="top"><input name="nomineeno" type="text" class="SL2TextField" readonly="readonly" value="<%=nomineeno%>"><br><img  src="<%=request.getContextPath()%>/images/btnSelect.gif" name="B5" onclick="openNomineeWindow()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnSelectOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnSelect.gif'">
 			    </td>
                          </tr>
                          <tr>
                            <td valign="top" class="style8">Nominee Details</td>
                            <td valign="top" >:</td>
                            <td><textarea name="ndetails" class="ML9TextField"><%=ndetails%></textarea></td>
                          </tr>
                          <% if(dbstatus!=null && dbstatus.equals("actual") && lastModifiedDate!="") { %>
                          <tr>
                            <td valign="top" class="style8">Last Modified Date</td>
                            <td valign="top" >:</td>
                            <td valign="top" class="style8"><%=lastModifiedDate%></td>
                          </tr>
                          <% } %>
                        </td>
                    </tr>
                </td>
              </tr>
            </table>
            <tr>
              <td>
                <!--HIDDEN FIELDS-->
                <input type="hidden" name="hfno" value="<%=sfolioID%>">
                <br>
                <!--bordercolor="#EAC06B"-->
                <table width="100%" BORDER=0  cellpadding="15" style="border-collapse: collapse"  bgcolor="#E8F3FD" >
                  <tr>
				  <td align="center">
						<!--<input type="button" name="editjoint1" value="editjoint1" onclick="openJoint1Edit()">-->
                                                <img name="B2" src="<%=request.getContextPath()%>/images/btnCertInfo.gif"  onclick="CertificateInfo()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnCertInfoR.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnCertInfo.gif'">
					</td>
				  	<td align="center">
						<!--<input type="button" name="editjoint1" value="editjoint1" onclick="openJoint1Edit()">-->
                                                <img name="B2" src="<%=request.getContextPath()%>/images/btnEditJointOne.gif"  onclick="openJoint1Edit()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnEditJointOneR.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnEditJointOne.gif'">
					</td>
				  	<td align="center">
						<!--<input type="button" name="editjoint2" value="editjoint2" onclick="openJoint2Edit()">-->
                                                <img name="B3" src="<%=request.getContextPath()%>/images/btnEditJointTwo.gif"  onclick="openJoint2Edit()" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnEditJointTwoR.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnEditJointTwo.gif'">
					</td>
                    <td align="center">
                      <div align="right">
                        <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
                      </div></td>
                      <td align="center">
                      <%
                          String funcName="";
                          if(dbstatus!=null && dbstatus.equals("actual"))
                            funcName = "gotoPermanentFolios";
                          else
                            funcName = "gotofolio";
                      %>
                        <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="<%=funcName%>()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
                      </td>
                  </tr>
                </table></table>
          </td>
        </tr>
    </table>
    </td></tr>
    </table>
</form>
<%
  if (isConnect) {
    cm.takeDown();
  }
%>
</body>
</html>
