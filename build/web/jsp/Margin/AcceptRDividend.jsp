<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Zahidul Islam
'Creation Date : 12th January' 2016
'Page Purpose  : Deletes a margin.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null")){ %>
    <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<% } %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Delete Dividend</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
    boolean isConnect       = cm.connect();
    boolean isConnect1      = cm1.connect();
    
    if(isConnect==false){ %>
        <jsp:forward page="ErrorMsg.jsp" >
           <jsp:param name="ErrorTitle" value="Connection Failure" />
           <jsp:param name="ErrorHeading" value="Connection Problem" />
           <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
        </jsp:forward> <% 
    }

    if(isConnect1==false){ %>
        <jsp:forward page="ErrorMsg.jsp" >
            <jsp:param name="ErrorTitle" value="Connection Failure" />
            <jsp:param name="ErrorHeading" value="Connection Problem" />
            <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
        </jsp:forward> <%
    }
    
    //System.out.println("--------------Accept R dividend--------------");
    String MarginID        = String.valueOf(request.getParameter("dmargin"));
    //System.out.println("--------------MarginID--------------"+MarginID);
    int iMarginID          =Integer.parseInt(MarginID);
    int oid                 = 0;
    String decdate          = "";
    String warrantno        = "";
    String boid             = "";
    String stvMaxWarrant    = "";

    //System.out.println("--------------iMarginID--------------"+iMarginID);

    String dmargin1        = "SELECT * FROM MARGIN_CONFIG_DIVIDEND WHERE REC_ID="+iMarginID;
    int drid                =cm.queryExecuteCount(dmargin1);
    //System.out.println("---------1-----drid--------------"+drid+"------dmargin1---"+dmargin1);
    if(drid>0){
        oid                   = cm.getColumnI("MARGIN_ID");
        decdate               = cm.getColumnDT("DIVIDEND_DATE");
      //System.out.println("-------2-------oid-----"+oid+"---decdate-"+decdate+"-----"+drid);
    }

    String dmargin2          = "SELECT * FROM MARGIN_BO_DIVIDEND_T WHERE MARGIN_DIV_ID="+iMarginID+" AND MARGIN_AC_ID="+oid+" AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
    
    //System.out.println("------dmargin2--------------------"+dmargin2);
    int drid2                 =cm.queryExecuteCount(dmargin2);
    if(drid2>0){
        boid            = "";
        warrantno       = "";
        cm.queryExecute(dmargin2);

        while(cm.toNext()){
            boid        = cm.getColumnS("BO_ID");
            warrantno   = cm.getColumnS("WARRANT_NO");
            //System.out.println("------decdate--"+decdate+"------boid-----"+boid+"----warrantno----"+warrantno);

            if(boid.equals("null"))
                boid            ="";
            if(warrantno.equals("null"))
                warrantno       ="";

            String dbodiv1      ="SELECT * FROM MARGIN_BO_DIVIDEND WHERE MARGIN_DIV_ID="+iMarginID+" AND MARGIN_AC_ID="+oid+" AND BO_ID='"+boid+"' AND WARRENT_NO='"+warrantno+"' AND MARGIN_AC_ID="+oid+" AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
            //System.out.println("---------dbodiv1-------"+dbodiv1);
            int qc              =cm1.queryExecuteCount(dbodiv1);

            if(qc<1){
                String dbodiv2      ="INSERT INTO MARGIN_BO_DIVIDEND (REC_ID,MARGIN_DIV_ID,MARGIN_AC_ID,BO_ID,WARRENT_NO,DIV_DATE,ISSUE_DATE,STATUS) VALUES ((SELECT NVL(MAX(rec_id),0)+1 FROM MARGIN_BO_DIVIDEND),"+iMarginID+","+oid+",'"+boid+"','"+warrantno+"',SYSDATE,TO_DATE('"+decdate+"','DD/MM/YYYY'),'T')";
                //System.out.println("---------dbodiv2-------"+dbodiv2);
                cm1.queryExecute(dbodiv2);
            }

            String dbodiv3          ="DELETE FROM MARGIN_BO_DIVIDEND_T WHERE MARGIN_DIV_ID="+iMarginID+" AND MARGIN_AC_ID="+oid+" AND BO_ID='"+boid+"' AND WARRANT_NO='"+warrantno+"' AND MARGIN_AC_ID="+oid+" AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
            //System.out.println("---------dbodiv3-------"+dbodiv3);
            cm1.queryExecute(dbodiv3);
        }
        //System.out.println("--------------MARGIN_BO_DIVIDEND_T deleted------");
    }

    stvMaxWarrant                   =cm.LastWarrant();
    //System.out.println("---------stvMaxWarrant-------"+stvMaxWarrant);

    String dbodiv4                  ="UPDATE MARGIN_CONFIG_DIVIDEND SET STATUS=UPPER('ACCEPTED'), GROUP_WARRANT_NO='"+stvMaxWarrant+"' WHERE REC_ID="+iMarginID;
    cm.queryExecute(dbodiv4);
    //System.out.println("---------MARGIN_CONFIG_DIVIDEND ACCEPTED-------"+dbodiv4);

    //String docdiv="DELETE FROM MARGIN_CONFIG_DIVIDEND WHERE REC_ID="+iMarginID+" AND MARGIN_ID="+oid+" AND DIVIDEND_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
    //cm.queryExecute(docdiv);

    //System.out.println("-------------- accepting complete ---------");



    String ulog                     = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Inactive Dividend for Margin')";
    boolean ub                      = cm.procedureExecute(ulog);

    if (isConnect){
        cm.takeDown();
    }
    if (isConnect1){
        cm1.takeDown();
    }
%>
<script language="javascript">
    location = "<%=request.getContextPath()%>/jsp/Margin/RDividendList.jsp";
</script>
</body>
</html>
