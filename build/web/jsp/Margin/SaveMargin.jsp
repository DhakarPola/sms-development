<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Zahidul Islam
'Creation Date : 12th January' 2016
'Page Purpose  : Save new Margin.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null")){ %>
    <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<% } %>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save margin</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
    boolean isConnect = cm.connect();
    if(isConnect==false){  %>
    <jsp:forward page="ErrorMsg.jsp" >
        <jsp:param name="ErrorTitle" value="Connection Failure" />
        <jsp:param name="ErrorHeading" value="Connection Problem" />
        <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
    </jsp:forward>
    <% 
    }
    String marginName      = String.valueOf(request.getParameter("uname"));
    String marginAddress   = String.valueOf(request.getParameter("uaddress"));
    String marginContact   = String.valueOf(request.getParameter("ucontact"));
    String marginAc        = String.valueOf(request.getParameter("uAc"));
    String marginStatus    = String.valueOf(request.getParameter("ustatus"));
    String marginRoutingNo        = String.valueOf(request.getParameter("uRouting_No"));
    String marginAcBank        = String.valueOf(request.getParameter("uAc_Bank"));	
    String marginAcBankBranch        = String.valueOf(request.getParameter("uAc_Bank_Branch"));		

     
  // System.out.println("---------------marginAddress-------------"+marginAddress);
  // System.out.println("----------------marginContact------------"+marginContact);
  // System.out.println("--------------marginStatus--------------"+marginStatus);

    marginName               =cm.replace(marginName,"'","''");
    marginAddress            =cm.replace(marginAddress,"'","''");
    marginContact            =cm.replace(marginContact,"'","''");
    marginStatus             =cm.replace(marginStatus,"'","''");
    marginAc                 =cm.replace(marginAc ,"'","''");
	marginRoutingNo             =cm.replace(marginRoutingNo ,"'","''");
	marginAcBank             =cm.replace(marginAcBank ,"'","''");
	marginAcBankBranch       =cm.replace(marginAcBankBranch ,"'","''");
	

    //System.out.println("-------------marginAc ---------------"+marginAc );
    String SQL              = "SELECT * FROM MARGIN_CONFIG WHERE UPPER(NAME)=UPPER('"+marginName+"')";

    int Count               = cm.queryExecuteCount(SQL);

    if (Count > 0){ %>
        <script language="javascript">
            alert("Margin name already exists!!!");
            history.go(-1);
       </script><%  
    }else{
		String nuser            = "call ADD_MARGIN('" + marginName + "', '" + marginAddress + "','" + marginContact + "','" + marginStatus+ "','" + marginAc+ "','"+marginRoutingNo+"','"+marginAcBank+"','"+marginAcBankBranch+ "')";
		
		// Line commented by Zahid 25 February 2016 
        // String nuser            = "call ADD_MARGIN('" + marginName + "', '" + marginAddress + "','" + marginContact + "','" + marginStatus+ "','" + marginAc+ "')";
        //String nuser = "INSERT INTO OMNIBUS_CONFIG(REC_ID, NAME, ADDRESS, CONTACT, STATUS) VALUES ((SELECT MAX(REC_ID)+1 FROM OMNIBUS_CONFIG), '" + marginName + "', '" + marginAddress + "', '" + marginContact + "', '" + marginAc+ "')";
        //System.out.println("--------------nuser--------------"+nuser);
        boolean b               = cm.procedureExecute(nuser);
        //System.out.println("------save--------b--------------"+b);
        String ulog             = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added a margin')";
        boolean ub              = cm.procedureExecute(ulog);
    }
    
    if (isConnect){
        cm.takeDown();
    } %>
<script language="javascript">
    location = "<%=request.getContextPath()%>/jsp/Margin/AllMargin.jsp";
</script>
</body>
</html>
