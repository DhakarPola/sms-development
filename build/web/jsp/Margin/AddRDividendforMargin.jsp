<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Zahidul Islam
'Creation Date : 12th January' 2016
'Page Purpose  : Add BO for Margin Dividend.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null")){ %>
    <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<% } %>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Dividend Margin Calculation</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<script language="javascript">
<!--


var check1              = 0;

function gotoPrevious(pdate,pstart,pend,poid){
    /*alert("goPrevious"+pdate+pstart+pend+poid);*/
    if (check1 > 0){
        count           = 0;
      /*BlankValidate('mrecondate','- Collection Date (Option must be entered)');*/

        if (count == 0){
            var thisurl = "<%=request.getContextPath()%>";
            thisurl = thisurl + "/jsp/Margin/AddRDividendforMargin.jsp?dateselect=" + pdate;
            thisurl = thisurl + "&oStartValue=" + pstart;
            thisurl = thisurl + "&oEndValue=" + pend;
            thisurl = thisurl + "&omnibusName=" + poid;
            thisurl = thisurl + "&ddate=" + pd;
            thisurl = thisurl + "&oid=" + id;

            document.forms[0].pstatus.value = thisurl;
            document.forms[0].action = "AddRMarkSelectedMargin.jsp";
            document.forms[0].submit();
        }else{
            ShowAllAlertMsg();
            return false;
        }
    }else{
        var thisurl = "<%=request.getContextPath()%>";
        thisurl = thisurl + "/jsp/Margin/AddRDividendforMargin.jsp?dateselect=" + pdate;
        thisurl = thisurl + "&oStartValue=" + pstart;
        thisurl = thisurl + "&oEndValue=" + pend;
        thisurl = thisurl + "&omnibusName=" + poid;
        thisurl = thisurl + "&ddate=" + pd;
        thisurl = thisurl + "&oid=" + id;

        document.forms[0].pstatus.value = thisurl;
        document.forms[0].action = "AddRMarkSelectedMargin.jsp";
        document.forms[0].submit();
    }
}

function gotoNext(ndate,nstart,nend,noid,nd,id){
    /*alert("goNext"+ndate+nstart+nend+noid+nd+id);*/
    if (check1 > 0){
        count       = 0;
        /*BlankValidate('mrecondate','- Collection Date (Option must be entered)');*/

        if (count == 0){
            var thisurl = "<%=request.getContextPath()%>";
            thisurl = thisurl + "/jsp/Margin/AddRDividendforMargin.jsp?dateselect=" + ndate;
            thisurl = thisurl + "&oStartValue=" + nstart;
            thisurl = thisurl + "&oEndValue=" + nend;
            thisurl = thisurl + "&omnibusName=" + noid;
            thisurl = thisurl + "&ddate=" + nd;
            thisurl = thisurl + "&oid=" + id;

            document.forms[0].pstatus.value = thisurl;
            document.forms[0].action = "AddRMarkSelectedMargin.jsp";
            document.forms[0].submit();
        }else{
            ShowAllAlertMsg();
            return false;
        }
    }else{
        var thisurl = "<%=request.getContextPath()%>";
        thisurl = thisurl + "/jsp/Margin/AddRDividendforMargin.jsp?dateselect=" + ndate;
        thisurl = thisurl + "&oStartValue=" + nstart;
        thisurl = thisurl + "&oEndValue=" + nend;
        thisurl = thisurl + "&omnibusName=" + noid;
        thisurl = thisurl + "&ddate=" + nd;
        thisurl = thisurl + "&oid=" + id;

        document.forms[0].pstatus.value = thisurl;
        document.forms[0].action = "AddRMarkSelectedMargin.jsp";
        document.forms[0].submit();
    }
}

function goforsearch(sdate,sstart,send,soid,sd,id){
    /*alert("gosearch"+sdate+sstart+send+soid+sd+id);*/
    if (document.all.searchWarrant.value.length > 0){
        if (check1 > 0){
            count = 0;
            /*BlankValidate('mrecondate','- Collection Date (Option must be entered)');*/

            if (count == 0){
                var thisurl = "<%=request.getContextPath()%>";
                thisurl = thisurl + "/jsp/Margin/AddRDividendforMargin.jsp?dateselect=" + sdate;
                thisurl = thisurl + "&oStartValue=" + sstart;
                thisurl = thisurl + "&oEndValue=" + send;
                thisurl = thisurl + "&omnibusName=" + soid;
                thisurl = thisurl + "&ddate=" + sd;
                thisurl = thisurl + "&oid=" + id;

                document.forms[0].pstatus.value = thisurl;
                document.forms[0].action = "AddRMarkSelectedMargin.jsp";
                document.forms[0].submit();
            }else{
                ShowAllAlertMsg();
                return false;
            }
        }else{
            var thisurl = "<%=request.getContextPath()%>";
            thisurl = thisurl + "/jsp/Margin/AddRDividendforMargin.jsp?dateselect=" + sdate;
            thisurl = thisurl + "&oStartValue=" + sstart;
            thisurl = thisurl + "&oEndValue=" + send;
            thisurl = thisurl + "&omnibusName=" + soid;
            thisurl = thisurl + "&ddate=" + sd;
            thisurl = thisurl + "&oid=" + id;

            document.forms[0].pstatus.value = thisurl;
            document.forms[0].action = "AddRMarkSelectedMargin.jsp";
            document.forms[0].submit();
        }
    }
}

function gotoback(){
    location = "<%=request.getContextPath()%>/jsp/Margin/RDividendList.jsp";
}

function putrecondate(){
    check1++;
 }


function SubmitThis(){
    count = 0;
    /*BlankValidate('mrecondate','- Collection Date (Option must be entered)');*/

    if (count == 0){
        document.forms[0].action = "SubmitRDividendforMargin.jsp";
        document.forms[0].submit();
    }else{
        ShowAllAlertMsg();
        return false;
    }
}


function keypresssubmit(sdate,sstart,send,soid,sd,id) {
    if (event.keyCode==13)  {
        goforsearch(sdate,sstart,send,soid,sd,id);
    }
}


//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style17 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF"><%
    boolean isConnect = cm.connect();
    if(isConnect==false){ %>
        <jsp:forward page="ErrorMsg.jsp" >
          <jsp:param name="ErrorTitle" value="Connection Failure" />
          <jsp:param name="ErrorHeading" value="Connection Problem" />
          <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
        </jsp:forward><%
    }

    //System.out.println("---------------AddDividendforMargin.jsp--------");
    String decdate              = String.valueOf(request.getParameter("ddate"));
    //System.out.println("---------------AddDividendforMargin.jsp----decdate----"+decdate);
    String oid                  = String.valueOf(request.getParameter("oid"));
    //System.out.println("---------------AddDividendforMargin.jsp----oid----"+oid);
    String SStartingValue       = String.valueOf(request.getParameter("oStartValue"));
    //System.out.println("---------------AddDividendforMargin.jsp----SStartingValue----"+SStartingValue);
    String SEndingValue         = String.valueOf(request.getParameter("oEndValue"));
    //System.out.println("---------------AddDividendforMargin.jsp----SEndingValue----"+SEndingValue);


    int ioid                    = Integer.parseInt(oid);
    int StartingValue           = Integer.parseInt(SStartingValue);
    int EndingValue             = Integer.parseInt(SEndingValue);
    int Chunk                   = 20;
//    StartingValue = StartingValue + 1;
    EndingValue                 = StartingValue + Chunk - 1;

    int pStartingValue          = StartingValue - Chunk;
    int pEndingValue            = EndingValue - Chunk;
    int nStartingValue          = StartingValue + Chunk;
    int nEndingValue            = EndingValue + Chunk;
    if (pStartingValue < 1){
        pStartingValue = 1;
    }
    if (pEndingValue < Chunk){
        pEndingValue = Chunk;
    }

    //String cate="SELECT * FROM BODIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + decdate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' ORDER BY BO_ID";
    //cm.queryExecute(cate);
    //cm.toNext(); %>

  <form method="GET" action="AddMarkSelectedMargin.jsp">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Dividend for Margin</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date of Declaration</div></th>
        <td width="1%">:</td>
        <td>
          <div align="left">
          <input type="text" name="ddatep" value="<%=decdate%>" class="SL2TextField" readonly="readonly">
          </div>
        </td>
      </tr>

      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Margin</div></th>
        <td width="1%">:</td>
        <td>
            <div align="left"><%
                String oname        = "";

                String query2       = "SELECT NAME FROM MARGIN_CONFIG WHERE REC_ID="+oid;
                cm.queryExecute(query2);

                while(cm.toNext()){
                    oname=cm.getColumnS("NAME");  %>
                    <input type="text" name="oname" value="<%=oname%>" class="SL2TextField" readonly="readonly"> <%
                } %>
            </div>
        </td>
      </tr>

  </table>
  </div>
  </center>
  </td>
  </tr>
  </table>


  <div align="left" ><br /></div>

  <SPAN id="dprint">
    <img name="B4" src="<%=request.getContextPath()%>/images/btnSubmit.gif" onclick="SubmitThis()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
    <img name="B7" src="<%=request.getContextPath()%>/images/btnClose.gif" onclick="gotoback()" onMouseOver="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
  </SPAN>


  <span class="style7">
  <table width="100%" BORDER=0  cellpadding="0" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td height="30" bgcolor="#0044B0" class="style17" valign="middle"><center>Add Dividend For Margin</center></td></tr>
  </table><%
    int rowcounter1             = 0;
    String oboid                ="";
    String owarrentNo           ="";
    String odividend            ="";
    int oTotalShare             = 0;
    String oselected            ="";



    //String cate="SELECT * FROM BODIVIDEND_VIEW WHERE WARRANT_NO NOT IN (((SELECT WARRANT_NO FROM MARGIN_BO_DIVIDEND_T WHERE ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY') AND STATUS=UPPER('S')) UNION (SELECT WARRENT_NO FROM MARGIN_BO_DIVIDEND WHERE ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY')))) AND ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' ORDER BY BO_ID";
    String cate                 ="SELECT * FROM BODIVIDEND WHERE WARRANT_NO NOT IN (((SELECT WARRANT_NO FROM MARGIN_BO_DIVIDEND_T WHERE WARRANT_NO NOT IN (SELECT WARRANT_NO FROM MARGIN_BO_DIVIDEND_T WHERE MARGIN_AC_ID="+ioid+" AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')) AND ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY') AND STATUS=UPPER('S')) UNION (SELECT WARRENT_NO FROM MARGIN_BO_DIVIDEND WHERE ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY')))) AND ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' ORDER BY BO_ID";
    //System.out.println("------------cate------->"+cate);
    int fcc                     = cm.queryExecuteCount(cate);


    if (nStartingValue > (fcc-Chunk+1)){
        nStartingValue = fcc - Chunk + 1;
    }
    if (nEndingValue > fcc){
        nEndingValue = fcc;
    }
    if (nStartingValue < 1){
        nStartingValue = 1;
    }
    if (nEndingValue < Chunk){
        nEndingValue = Chunk;
    }

    //String query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM BODIVIDEND_VIEW WHERE WARRANT_NO NOT IN (((SELECT WARRANT_NO FROM MARGIN_BO_DIVIDEND_T WHERE ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY') AND STATUS=UPPER('S')) UNION (SELECT WARRENT_NO FROM MARGIN_BO_DIVIDEND WHERE ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY')))) AND ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' ORDER BY BO_ID) div where rownum <= " + EndingValue + ") where rnum >= " + StartingValue + "";
    String query1               = "SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM BODIVIDEND WHERE WARRANT_NO NOT IN (((SELECT WARRANT_NO FROM MARGIN_BO_DIVIDEND_T WHERE WARRANT_NO NOT IN (SELECT WARRANT_NO FROM MARGIN_BO_DIVIDEND_T WHERE MARGIN_AC_ID="+ioid+" AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')) AND ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY') AND STATUS=UPPER('S')) UNION (SELECT WARRENT_NO FROM MARGIN_BO_DIVIDEND WHERE ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY')))) AND ISSUE_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' ORDER BY BO_ID) div where rownum <= " + EndingValue + ") where rnum >= " + StartingValue + "";
    //System.out.println("----calc--------query1------->"+query1);
    cm.queryExecute(query1); %>

  <table width="100%"  border="0" cellpadding="0" cellspacing="0" bordercolor="#0044B0">
      <tr bgcolor="#E8F3FD">
	<td width="8%" align="center" style="border-left: solid 1px #0044B0;"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="gotoPrevious('<%=decdate%>',<%=pStartingValue%>,<%=pEndingValue%>,<%=ioid%>,'<%=decdate%>',<%=ioid%>)"></td>
	<td width="8%" align="left"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="gotoNext('<%=decdate%>',<%=nStartingValue%>,<%=nEndingValue%>,<%=ioid%>,'<%=decdate%>',<%=ioid%>)"></td>
	<td width="29%" class="style19" valign="middle">
        </td>
	<td width="24%" align="right" height="35" class="style19">
            <input name="forw" type="radio" value="obo" checked="checked">&nbsp;BO ID&nbsp;
            <input name="forw" type="radio" value="warrant">&nbsp;Warrant No.&nbsp;
        </td>
	<td width="19%" align="center"><input name="searchWarrant" type="text" class="rhakim" onkeypress="keypressOnNumberFld()" onkeyup="keypresssubmit('<%=decdate%>',<%=StartingValue%>,<%=EndingValue%>,<%=ioid%>,'<%=decdate%>',<%=ioid%>);"></td>
	<td width="12%" align="left" style="border-right: solid 1px #0044B0;">
          <img src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="goforsearch('<%=decdate%>',<%=StartingValue%>,<%=EndingValue%>,<%=ioid%>,'<%=decdate%>',<%=ioid%>)">
        </td>
      </tr>
  </table>


  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="20%" class="style17">
      <div align="center">BO ID</div>
    </td>
    <td width="20%"><div align="center" class="style17">
      <div align="center">Warrant No.</div>
    </div></td>
    <td width="20%"><div align="center" class="style17">
      <div align="center">Dividend</div>
    </div></td>
    <td width="20%"><div align="center" class="style17">
      <div align="center">Total Share</div>
    </div></td>
    <td width="20%"><div align="center" class="style17">
      <div align="center">Select</div>
    </div></td>
  </tr>

  <div> <%
    while(cm.toNext()){
        rowcounter1++;
        oboid                       =cm.getColumnS("BO_ID");
        owarrentNo                  =cm.getColumnS("WARRANT_NO");
        odividend                   =cm.getColumnS("DIVIDEND");
        oTotalShare                 =cm.getColumnI("TOTAL_SHARE");
        oselected                   =cm.getColumnS("MARGIN_SELECT");

        if (String.valueOf(oboid).equals("null"))
            oboid                   = "";
        if (String.valueOf(owarrentNo).equals("null"))
            owarrentNo              = "";
        if (String.valueOf(odividend).equals("null"))
            odividend               = "";
        if (String.valueOf(oselected).equals("null"))
            oselected               = "";
        //System.out.println("------0-calc--oselected--------"+oselected);  %>

  <tr bgcolor="#E8F3FD">
    <td width="20%" class="style9">
      <div align="center"><%=oboid%>&nbsp;</div>
    </td>
    <td width="20%">
      <div align="center" class="style9">
      <div align="center"><%=owarrentNo%>&nbsp;</div>
      </div>
    </td>
    <td width="20%">
      <div align="center" class="style9">
      <div align="center"><%=odividend%>&nbsp;</div>
      </div>
    </td>
    <td width="20%">
      <div align="center" class="style9">
      <div align="center"><%=oTotalShare%>&nbsp;</div>
      </div>
    </td>
    <td width="20%">
        <div align="center" class="style9">
            <div align="center"><%
                //System.out.println("------1-calc--oselected--------"+oselected);
                if (oselected.equalsIgnoreCase("T")){
                    //System.out.println("----2---calc--oselected--------"+oselected); %>
                    <input type="checkbox" name="selectingbox<%=rowcounter1%>" value="<%=owarrentNo%>" checked="checked"> <%
                }else{
                    //System.out.println("----3---calc--oselected--------"+oselected); %>
                    <input type="checkbox" name="selectingbox<%=rowcounter1%>" value="<%=owarrentNo%>" onclick="putrecondate()"> <%
                } %>
            </div>
        </div>
    </td>
  </tr>
  </div> <% } %>
  </table>


    <!--HIDDEN FIELDS-->
    <input type="hidden" name="rowcounter1" value="<%=rowcounter1%>">
    <input type="hidden" name="ddate" value="<%=decdate%>">
    <input type="hidden" name="svalue" value="<%=StartingValue%>">
    <input type="hidden" name="evalue" value="<%=EndingValue%>">
    <input type="hidden" name="pchunk" value="<%=Chunk%>">
    <input type="hidden" name="pstatus" value="close">
    <input type="hidden" name="oid" value="<%=oid%>"><%
    /*String addmemo = "call ADD_DIVIDEND_MEMO(UPPER('"+ memoname[x] +"'),'"+memotype[x]+"','" + totalSH[x] + "','" + totaldiv[x] + "','" + totaltax[x] + "','"+memoid+"')";
    boolean add = cm.procedureExecute(addmemo);
    }*/
    String ulog                 = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Calculated Dividend for Margin')";
    boolean ub                  = cm.procedureExecute(ulog);

    if (isConnect){
        cm.takeDown();
    } %>


</body>
</html>
