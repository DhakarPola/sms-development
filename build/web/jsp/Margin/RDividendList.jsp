<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Zahidul Islam
'Creation Date : 12th January' 2016
'Page Purpose  : View of all the Dividend for Margin.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null")){ %>
    <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<% } %>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Awaiting Dividend for Reconciliation</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function tonewmargin(){
    if (confirm("Do you want to accept this Margin?")){
        document.forms[0].action    = "AcceptRDividend.jsp";
        document.forms[0].submit();
      /*location = "<%=request.getContextPath()%>/jsp/Margin/AcceptRDividend.jsp"*/
    }
}

function todeletemargin(){
    location = "<%=request.getContextPath()%>/jsp/Margin/DeleteRDividend.jsp?"
}

function askdelete(){
    if (confirm("Do you want to reject this Margin?")){
        document.forms[0].submit();
    }
}

//Execute while click on Submit
function SubmitThis(){
    count   = 0;
    if (count == 0){
        document.forms[0].submit();
    }else{
        ShowAllAlertMsg();
        return false;
    }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
    boolean isConnect = cm.connect();
    if(isConnect==false){ %>
        <jsp:forward page="ErrorMsg.jsp" >
           <jsp:param name="ErrorTitle" value="Connection Failure" />
           <jsp:param name="ErrorHeading" value="Connection Problem" />
           <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
        </jsp:forward><%
    }
    
    String query1       = "SELECT MCD.REC_ID, MC.REC_ID AS REC, MC.NAME, MCD.DIVIDEND_DATE, MCD.STATUS FROM MARGIN_CONFIG MC,MARGIN_CONFIG_DIVIDEND MCD WHERE MC.REC_ID=MCD.MARGIN_ID AND MCD.STATUS IN UPPER('SUBMITTED') ORDER BY MCD.DIVIDEND_DATE DESC, UPPER(MC.NAME)";
    cm.queryExecute(query1);

    String marginName          = "";
    String dividendDate         = "";
    String dividendStatus       = "";
    String dest                 = "";
    //String SdividendRecId = "";
    int dividendRecId           = 0;
    int oid                     = 0;
%>
  <span class="style7">
  <form method="GET" action="DeleteRDividend.jsp">
  <img name="B4" src="<%=request.getContextPath()%>/images/btnAccept.gif" onclick="tonewmargin()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAcceptR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAccept.gif'">
  <img name="B6" src="<%=request.getContextPath()%>/images/btnReject.gif" onclick="askdelete()" onMouseOver="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnRejectR.gif'" onMouseOut="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnReject.gif'">
  <table width="100%" BORDER=1  cellpadding="5" bordercolor="#0044B0" style="border-collapse: collapse">
  <!--DWLayoutTable-->
  <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Dividend for Margin</center></td></tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="34%" class="style9"><div align="center">Dividend Date</div></td>
    <td width="33%"><div align="center" class="style9">
      <div align="center">Margin Name</div>
    </div></td>
    <td width="33%" class="style9"><div align="center">Status</div></td>
  </tr>
  <div align="left"><%

    dest = request.getContextPath() + "/jsp/Margin/EditRDividend.jsp";

    while(cm.toNext()){
        marginName             = cm.getColumnS("NAME");
        dividendDate            = cm.getColumnDT("DIVIDEND_DATE");
        dividendStatus          = cm.getColumnS("STATUS");
        //SdividendRecId = cm.getColumnS("REC_ID");
        dividendRecId           = cm.getColumnI("REC_ID");
        oid                     = cm.getColumnI("REC");

       //System.out.println("---------dlist-----oid-----"+oid+"--dividendRecId--"+dividendRecId+"---dividendDate--"+dividendDate);
        if (!marginName.equals("null")){
            if (dividendStatus.equalsIgnoreCase("TEMP"))
                dividendStatus      = "Draft";
            else if (dividendStatus.equalsIgnoreCase("SUBMITTED"))
                dividendStatus      = "Awaiting";
            else if (dividendStatus.equalsIgnoreCase("null"))
                dividendStatus      = "";
  %></div>
        <tr bgcolor="#E8F3FD">
           <td>
                <div align="center" class="style10">
                    <div align="left" class="style10"><span class="style13">
                         <label><input type="radio" checked="checked" name="dmargin" value="<%=dividendRecId%>"></label>
                         &nbsp;&nbsp;&nbsp<a HREF="<%=dest%>?dateselect=<%=dividendDate%>&marginName=<%=oid%>&oStartValue=1&oEndValue=0"><%=dividendDate%></a>&nbsp;
                         </span>
                    </div>
                </div>
           </td>
           <td class="style13">
                <div align="center" class="style12">
                    <div align="left"><%=marginName%>&nbsp;</div>
                </div>
           </td>
           <td class="style12"><div align="center">&nbsp;&nbsp;&nbsp;<%=dividendStatus%>&nbsp;</div></td>
        </tr>
   <div align="left" class="style13"></div><%
        }
    }%>
    </table><%

    if (isConnect){
        cm.takeDown();
    }
    %>
</form>
</span>
</body>
</html>
