<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Zahidul Islam
'Creation Date : 12th January' 2016
'Page Purpose  : Submit the Collected Warrant Numbers for Margin.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null")){ %>
    <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<% } %>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Mark Selected Warrants</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF"><%
    boolean isConnect               = cm.connect();
    if(isConnect==false){ %>
        <jsp:forward page="ErrorMsg.jsp" >
           <jsp:param name="ErrorTitle" value="Connection Failure" />
           <jsp:param name="ErrorHeading" value="Connection Problem" />
           <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
        </jsp:forward> <%
    }

    String rowcounter               = String.valueOf(request.getParameter("rowcounter1"));
    String DivDate                  = String.valueOf(request.getParameter("ddate"));
    String StartingValue            = String.valueOf(request.getParameter("svalue"));
    String EndingValue              = String.valueOf(request.getParameter("evalue"));
    String oid                      = String.valueOf(request.getParameter("oid"));
    int ioid                        = Integer.parseInt(oid);

    String validating               ="SELECT REC_ID FROM MARGIN_CONFIG_DIVIDEND WHERE MARGIN_ID="+ioid+" AND DIVIDEND_DATE=TO_DATE('"+DivDate+"','DD/MM/YYYY')";
    //System.out.println("-----------S--------------------validating="+validating);
    int validity                    =cm.queryExecuteCount(validating);
    if(validity<1){
      String addMarginConfigDividend1  ="INSERT INTO MARGIN_CONFIG_DIVIDEND (REC_ID,MARGIN_ID,DIVIDEND_DATE,STATUS) VALUES ((SELECT NVL(MAX(rec_id),0)+1 FROM MARGIN_CONFIG_DIVIDEND),"+ioid+",TO_DATE('"+DivDate+"', 'DD/MM/YYYY'),'TEMP')";
      //System.out.println("-----------S--------------------aocd=");
      cm.queryExecute(addMarginConfigDividend1);
      //System.out.println("-----------S--------------------addMarginConfigDividend="+addMarginConfigDividend1);
    }
    String Fldname                  = "";
    int irowcounter                 = Integer.parseInt(rowcounter);

    //System.out.println("rowcounter="+rowcounter);
    //System.out.println("DivDate="+DivDate);
    //System.out.println("StartingValue="+StartingValue);
    //System.out.println("EndingValue="+EndingValue);
    //System.out.println("ioid="+ioid);

    for (int i=1;i<irowcounter+1;i++){
        Fldname                     = "selectingbox" + String.valueOf(i);
        String BoxValue             = String.valueOf(request.getParameter(Fldname));
        BoxValue                    =cm.replace(BoxValue,"'","''");

        if (String.valueOf(BoxValue).equals("null")){
            BoxValue                = "";
        }
        if (!BoxValue.equalsIgnoreCase("")){
            String marksel              = "";
            String marksel2             = "";
            String markcoll             = "";
            //System.out.println("-----------S--------------------BoxValue="+BoxValue);
            //System.out.println("-----------S--------------------DivDate="+DivDate);
            //System.out.println("-----------S--------------------ioid="+ioid);
            marksel                     ="UPDATE BODIVIDEND SET MARGIN_SELECT = 'T' WHERE WARRANT_NO = '"+BoxValue+"' AND ISSUE_DATE = TO_DATE('"+DivDate+"','DD/MM/YYYY')";
            //System.out.println("----------------SubmitDividendforMargin.jsp------- PROCEDURE TIME");
            /*
            if (DivType.equalsIgnoreCase("CDBL"))
            {
              marksel = "call MARK_SELECTED_BODIVIDEND('" + BoxValue + "','" + ColDate + "','" + DivDate + "')";
              markcoll = "call MARK_COLLECTED_BODIVIDEND()";
            }
            */
            //System.out.println("marksel="+marksel);
            //System.out.println("markcoll="+markcoll);
            //boolean b = cm.procedureExecute(marksel);
            cm.queryExecute(marksel);
            //System.out.println("----------S------------b=");

            String validating2          ="SELECT REC_ID FROM MARGIN_BO_DIVIDEND_T WHERE MARGIN_DIV_ID=(SELECT REC_ID FROM MARGIN_CONFIG_DIVIDEND WHERE MARGIN_ID="+ioid+" AND DIVIDEND_DATE=TO_DATE('"+DivDate+"','DD/MM/YYYY')) AND MARGIN_AC_ID="+ioid+" AND WARRANT_NO='"+BoxValue+"' AND ISSUE_DATE=TO_DATE('"+DivDate+"','DD/MM/YYYY')";
            //System.out.println("-----------S--------------------validating2="+validating2);
            int validity2               =cm.queryExecuteCount(validating2);
            if(validity2<1){
                marksel2                ="INSERT INTO MARGIN_BO_DIVIDEND_T (REC_ID,MARGIN_DIV_ID,MARGIN_AC_ID,BO_ID,WARRANT_NO,DIV_DATE,ISSUE_DATE,STATUS) VALUES ((SELECT NVL(MAX(rec_id),0)+1 FROM MARGIN_BO_DIVIDEND_T),(SELECT REC_ID FROM MARGIN_CONFIG_DIVIDEND WHERE MARGIN_ID="+ioid+" AND DIVIDEND_DATE=TO_DATE('"+DivDate+"','DD/MM/YYYY')),"+ioid+",(SELECT BO_ID FROM BODIVIDEND WHERE WARRANT_NO='"+BoxValue+"' AND ISSUE_DATE=TO_DATE('"+DivDate+"','DD/MM/YYYY')),'"+BoxValue+"',SYSDATE,TO_DATE('"+DivDate+"','DD/MM/YYYY'),'T')";
                //System.out.println("-----------S--------------------marksel2="+marksel2);
                cm.queryExecute(marksel2);
                //System.out.println("-----------S--------------------b2=");
            }
        }
    }

    String validating1                  ="SELECT REC_ID FROM MARGIN_CONFIG_DIVIDEND WHERE MARGIN_ID="+ioid+" AND DIVIDEND_DATE=TO_DATE('"+DivDate+"','DD/MM/YYYY') AND STATUS=UPPER('TEMP')";
    //System.out.println("-----------S--------------------validating1="+validating1);
    int validity1                       =cm.queryExecuteCount(validating1);
    if(validity1>0){
        String addMarginConfigDividend   ="UPDATE MARGIN_CONFIG_DIVIDEND SET STATUS = UPPER('SUBMITTED') WHERE MARGIN_ID="+ioid+" AND DIVIDEND_DATE=TO_DATE('"+DivDate+"','DD/MM/YYYY') AND STATUS=UPPER('TEMP')";
        //System.out.println("-----------S--------------------addMarginConfigDividend="+addMarginConfigDividend);
        cm.queryExecute(addMarginConfigDividend);
        //System.out.println("-----------S--------------------aocd=");
    }


    String changeMarginBoDivStatus     ="UPDATE MARGIN_BO_DIVIDEND_T SET STATUS = UPPER('S') WHERE MARGIN_DIV_ID=(SELECT REC_ID FROM MARGIN_CONFIG_DIVIDEND WHERE MARGIN_ID="+ioid+" AND DIVIDEND_DATE=TO_DATE('"+DivDate+"','DD/MM/YYYY')) AND MARGIN_AC_ID="+ioid+" AND ISSUE_DATE=TO_DATE('"+DivDate+"','DD/MM/YYYY')";
    cm.queryExecute(changeMarginBoDivStatus);
    //System.out.println("---------- change status in margin_bo_div table ----------");

    String ulog                         = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Dividends for Margin Submitted')";
    //boolean ub = cm.procedureExecute(ulog);

    if (isConnect){
        cm.takeDown();
    } %>
<script language="javascript">
    alert("Submission Complete!");
    location = "<%=request.getContextPath()%>/jsp/Margin/RDividendList.jsp?dateselect=<%=DivDate%>&oStartValue=<%=StartingValue%>&oEndValue=<%=EndingValue%>&marginName=<%=ioid%>";
</script>
</body>
</html>
