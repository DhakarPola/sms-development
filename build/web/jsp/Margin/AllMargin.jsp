<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Zahidul Islam
'Creation Date : 12th January' 2016
'Page Purpose  : View of all the Margin.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null")){ %>
    <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<% } %>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Margin</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function tonewmargin(){
    location = "<%=request.getContextPath()%>/jsp/Margin/NewMargin.jsp"
}

function todeletemargin(){
    location = "<%=request.getContextPath()%>/jsp/Margin/DeleteMargin.jsp?"
}

function askdelete(){
    if (confirm("Do you want to Delete the Margin?")){
        document.forms[0].submit();
    }
}

//Execute while click on Submit
function SubmitThis() {
    count = 0;
    if (count == 0){
        document.forms[0].submit();
    }else{
        ShowAllAlertMsg();
        return false;
    }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
    boolean isConnect = cm.connect();
    if(isConnect==false){  %>
    <jsp:forward page="ErrorMsg.jsp" >
        <jsp:param name="ErrorTitle" value="Connection Failure" />
        <jsp:param name="ErrorHeading" value="Connection Problem" />
        <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
    </jsp:forward>
    <% } 
    String query1               = "SELECT * FROM MARGIN_CONFIG  ORDER BY UPPER(NAME)";
    cm.queryExecute(query1);

    String marginName           = "";
    String marginAddress        = "";
    String marginContact        = "";
    String marginStatus         = "";
    String dest                 = "";

    int marginId = 0;
%>
  <span class="style7">
  <form method="GET" action="DeleteMargin.jsp">
  <img name="B4" src="<%=request.getContextPath()%>/images/btnNew.gif" onclick="tonewmargin()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNewOn.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNew.gif'">
  <img name="B6" src="<%=request.getContextPath()%>/images/btnDelete.gif" onclick="askdelete()" onMouseOver="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnDeleteOn.gif'" onMouseOut="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnDelete.gif'">
  <table width="100%" BORDER=1  cellpadding="5" bordercolor="#0044B0" style="border-collapse: collapse">
  <!--DWLayoutTable-->
  <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Margins</center></td></tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
    <tr bgcolor="#0044B0">
      <td width="25%" class="style9"><div align="center">Name</div></td>
      <td width="25%">
          <div align="center" class="style9">
          <div align="center">Address</div>
          </div></td>
      <td width="25%" class="style9"><div align="center">Contact</div></td>
      <td width="25%" class="style9"><div align="center">Status</div></td>
    </tr>
    <div align="left">
    <%
        while(cm.toNext()){
            marginName       = cm.getColumnS("NAME");
            marginAddress    = cm.getColumnS("ADDRESS");
            marginContact    = cm.getColumnS("CONTACT");
            marginStatus     = cm.getColumnS("STATUS");
            marginId         = cm.getColumnI("REC_ID");

            dest = request.getContextPath() + "/jsp/Margin/EditMargin.jsp";

            if (!marginName.equals("null")){
                if (String.valueOf(marginAddress).equals("null"))
                    marginAddress = "";
                if (String.valueOf(marginContact).equals("null"))
                    marginContact = "";
                if (String.valueOf(marginContact).equals("null"))
                    marginStatus = "";
    %>
    </div>
    <tr bgcolor="#E8F3FD">
        <td>
            <div align="center" class="style10">
                <div align="left" class="style10">
                  <span class="style13">
                 <label><input type="radio" checked="checked" name="dmargin" value="<%=marginId%>"></label>
                  &nbsp;&nbsp;&nbsp<a HREF="<%=dest%>?uname1=<%=marginName%>&uid1=<%=marginId%>"><%=marginName%></a>
                  &nbsp;
                  </span>
                </div>
            </div>
        </td>
        <td class="style13">
            <div align="center" class="style12">
                <div align="left"><%=marginAddress%>&nbsp;</div>
            </div>
        </td>
        <td class="style12"><div align="left">&nbsp;&nbsp;&nbsp;<%=marginContact%>&nbsp;</div></td>
        <td class="style12"><div align="center">&nbsp;&nbsp;&nbsp;<%=marginStatus%>&nbsp;</div></td>
    </tr>
    <div align="left" class="style13">
    <%
            }
        }
        if (isConnect){
            cm.takeDown();
        }
    %>
    </div>
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>

</body>
</html>
