<script>
/*
'******************************************************************************************************************************************
'Script Author : Jahirul Islam
'Creation Date : 1st March' 2013
'Page Purpose  : Deletes a margin.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null")) { %>
  <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script> <% 
} %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Delete Dividend</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   if(isConnect1==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
   }
  //System.out.println("--------------delete dividend--------------");
  String MarginID = String.valueOf(request.getParameter("dmargin"));
  int iMarginID=Integer.parseInt(MarginID);
  int oid = 0;
  String decdate = "";
  String boid = "";
  String warrantno = "";
  System.out.println("--------------iMarginID--------------"+iMarginID);
  //String dmargin1 = "call DELETE_MARGIN('" + MarginID + "')";
  //System.out.println("--------------dmargin1--------------"+dmargin1);

  //boolean b = cm.procedureExecute(dmargin1);

  //String dmargin1 = "UPDATE MARGIN_CONFIG_DIVIDEND SET STATUS = UPPER('INACTIVE') WHERE REC_ID="+MarginID;
  String dmargin1 = "SELECT * FROM MARGIN_CONFIG_DIVIDEND WHERE REC_ID="+iMarginID;
  int drid=cm.queryExecuteCount(dmargin1);
  //System.out.println("---------1-----drid--------------"+drid+"------dmargin1---"+dmargin1);
  if(drid>0){
    //System.out.println("-------2-------drid--------------"+drid);
    //warrantno = cm.getColumnS("BO_ID");
    oid = cm.getColumnI("MARGIN_ID");
    decdate = cm.getColumnDT("DIVIDEND_DATE");
    //System.out.println("-------2-------oid-----"+oid+"---decdate-"+decdate+"-----"+drid);

  }

  String dmargin2 = "SELECT * FROM MARGIN_BO_DIVIDEND_T WHERE MARGIN_DIV_ID="+iMarginID+" AND MARGIN_AC_ID="+oid+" AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
  //System.out.println("------dmargin2--------------------"+dmargin2);
  int drid2=cm.queryExecuteCount(dmargin2);
  if(drid2>0){
    boid = "";
    warrantno = "";
    cm.queryExecute(dmargin2);

    while(cm.toNext())
    {
      boid = cm.getColumnS("BO_ID");
      warrantno = cm.getColumnS("WARRANT_NO");
      //System.out.println("------decdate--"+decdate+"------boid-----"+boid+"----warrantno----"+warrantno);

      if(boid.equals("null"))
      boid="";
      if(warrantno.equals("null"))
      warrantno="";

      String dbodiv1="UPDATE BODIVIDEND SET MARGIN_SELECT=NULL WHERE WARRANT_NO='"+warrantno+"' AND BO_ID='"+boid+"' AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
      cm1.queryExecute(dbodiv1);

      String dbodiv2="DELETE FROM MARGIN_BO_DIVIDEND_T WHERE WARRANT_NO='"+warrantno+"' AND MARGIN_AC_ID="+oid+" AND ISSUE_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
      cm1.queryExecute(dbodiv2);


    }
    //System.out.println("--------------MARGIN_BO_DIVIDEND_T deleted------");
  }

  String docdiv="DELETE FROM MARGIN_CONFIG_DIVIDEND WHERE REC_ID="+iMarginID+" AND MARGIN_ID="+oid+" AND DIVIDEND_DATE=TO_DATE('"+decdate+"','DD/MM/YYYY')";
  cm.queryExecute(docdiv);

  //System.out.println("-------------- deletion complete ---------");



  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Inactive Dividend for Margin')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
   if (isConnect1)
   {
     cm1.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Margin/RDividendList.jsp";
</script>
</body>
</html>
