<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : January 2006
'Page Purpose  : Shows Nominee Listings.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="cal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
.style19 {
	color: #0A2769;
	font-weight: bold;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>View by Dividend Group</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

function goPrevious(pnomno,pstart,pend)
{
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Nominee/ViewbyNominee.jsp?nomineeno=" + pnomno;
  thisurl = thisurl + "&StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;

  location = thisurl;
}

function gosearch(sdate,stype,sstart,send)
{
  var list =  document.forms[0].nomineeinfo;
  location = list.options[list.selectedIndex].value;
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style17 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String SNomineeNo = String.valueOf(request.getParameter("nomineeno"));
    String SStartingValue = String.valueOf(request.getParameter("StartValue"));
    String SEndingValue = String.valueOf(request.getParameter("EndValue"));

    int NomineeNo = 0;
    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    int Chunk = 20;
    EndingValue = StartingValue + Chunk - 1;

    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;

    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }

    NomineeNo = Integer.parseInt(SNomineeNo);
    String query1cc = "SELECT * FROM SHAREHOLDER_VIEW WHERE NOMINEE_NO = '" + NomineeNo + "' AND ACTIVE = 'T' ORDER BY SUBSTR(FOLIO_NO,3,6)"; //FOLIO_NO";
    int fcc = cm.queryExecuteCount(query1cc);

    if (nStartingValue > (fcc-Chunk+1))
    {
      nStartingValue = fcc - Chunk + 1;
    }
    if (nEndingValue > fcc)
    {
      nEndingValue = fcc;
    }
    if (nStartingValue < 1)
    {
      nStartingValue = 1;
    }
    if (nEndingValue < Chunk)
    {
      nEndingValue = Chunk;
    }

    String query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM SHAREHOLDER_VIEW WHERE NOMINEE_NO = '" + NomineeNo + "' AND ACTIVE = 'T' ORDER BY SUBSTR(FOLIO_NO,3,6)) div where rownum <= " + EndingValue + ") where rnum >= " + StartingValue + "";
    cm.queryExecute(query1);

    String foliono = "";
    String folioname = "";
    String address = "";
    String address1 = "";
    String address2 = "";
    String address3 = "";
    String address4 = "";

    int rowcounter1 = 0;
%>
  <span class="style7">
  <form method="GET" action="ViewbyNominee.jsp">
  <SPAN id="dprint">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
  </SPAN>
  <table width="100%" BORDER=0  cellpadding="0" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td height="30" bgcolor="#0044B0" class="style17" valign="middle"><center>View by Dividend Group</center></td></tr>
  </table>
  <table width="100%"  border="0" cellpadding="0" cellspacing="0" bordercolor="#0044B0">
      <tr bgcolor="#E8F3FD">
	<td width="10%" style="border-left: solid 1px #0044B0;">&nbsp;</td>
	<td width="10%"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goPrevious('<%=SNomineeNo%>',<%=pStartingValue%>,<%=pEndingValue%>)"></td>
	<td width="22%"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goPrevious('<%=SNomineeNo%>',<%=nStartingValue%>,<%=nEndingValue%>)"></td>
	<td width="19%" height="35" class="style19" align="right">Dividend Group :&nbsp;&nbsp;&nbsp;</td>
	<td width="19%">
          <select name="nomineeinfo" class="SL2TextFieldListBox" onChange="gosearch()">
          <option value="<%=request.getContextPath()%>/jsp/Nominee/ViewbyNominee.jsp?nomineeno=0&StartValue=0&EndValue=0">--- Please Select ---</option>
            <%
              String thenomineeno = "";
              String thenomineename = "";

              String query1n = "SELECT * FROM NOMINEE_VIEW ORDER BY TO_NUMBER(NOMINEE_NO)";
              cm1.queryExecute(query1n);

              while(cm1.toNext())
              {
                 thenomineeno = cm1.getColumnS("NOMINEE_NO");
                 thenomineename = cm1.getColumnS("NAME");

                 if (String.valueOf(thenomineeno).equals("null"))
                  thenomineeno = "";

                 if (SNomineeNo.equalsIgnoreCase(thenomineeno))
                 {
                 %>
                   <option value="<%=request.getContextPath()%>/jsp/Nominee/ViewbyNominee.jsp?nomineeno=<%=thenomineeno%>&StartValue=0&EndValue=0" selected="selected"><%=thenomineename%></option>
                 <%
                 }
                 else
                 {
                 %>
                   <option value="<%=request.getContextPath()%>/jsp/Nominee/ViewbyNominee.jsp?nomineeno=<%=thenomineeno%>&StartValue=0&EndValue=0"><%=thenomineename%></option>
                 <%
                 }
              }
            %>
         </select>
        </td>
      </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="15%" class="style9"><div align="center">Folio No.</div></td>
    <td width="35%" class="style9"><div align="center">Name</div></td>
    <td width="50%" class="style9"><div align="center">Address</div></td>
  </tr>

  <div align="left">
     <%
    if (!SNomineeNo.equalsIgnoreCase("0"))
    {
    while(cm.toNext())
     {
       rowcounter1++;
       foliono = cm.getColumnS("FOLIO_NO");
       folioname = cm.getColumnS("NAME");
       address1 = cm.getColumnS("ADDRESS1");
       address2 = cm.getColumnS("ADDRESS2");
       address3 = cm.getColumnS("ADDRESS3");
       address4 = cm.getColumnS("ADDRESS4");

       if (String.valueOf(foliono).equals("null"))
         foliono = "";
       if (String.valueOf(folioname).equals("null"))
         folioname = "";
       if (String.valueOf(address1).equals("null"))
         address1 = "";
       if (String.valueOf(address2).equals("null"))
         address2 = "";
       if (String.valueOf(address3).equals("null"))
         address3 = "";
       if (String.valueOf(address4).equals("null"))
         address4 = "";

       address = address1 + address2 + address3 + address4;
     %>
    </div>
  <tr bgcolor="#E8F3FD">
            <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=foliono%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="left">&nbsp;<%=folioname%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="justify" class="style12">
             <div align="left"><%=address%></div>
           </div></td>
         </tr>
         <div align="left" class="style13">
             <%
         }
    }
  if (isConnect)
   {
     cm.takeDown();
     cm1.takeDown();
   }
  %>
         </div>
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>

</body>
</html>
