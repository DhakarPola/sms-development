<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Creation Date : December 2005
'Page Purpose  : View of all the Nominees.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Dividend Groups</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function tonewnominee()
{
  location = "<%=request.getContextPath()%>/jsp/Nominee/NewNominee.jsp"
}

function todeletecategories()
{
  location = "<%=request.getContextPath()%>/jsp/Nominee/DeleteNominee.jsp?"
}

 function askdelete()
 {
  if (confirm("Do you want to Delete the Dividend Group Information?"))
  {
   document.forms[0].submit();
  }
 }

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #000066;

	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%

   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%

     }

    String query1 = "SELECT * FROM NOMINEE_VIEW ORDER BY TO_NUMBER(NOMINEE_NO)";
    cm.queryExecute(query1);

    String nominee_no = "";
    String name = "";
    String address = "";
    String telephone = "";
    String remarks ="";
    String dest = "";
    String contact ="";
%>
  <span class="style7">
  <form method="GET" action="DeleteNominee.jsp">

  <img name="B4" src="<%=request.getContextPath()%>/images/btnNew.gif" onclick="tonewnominee()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNewOn.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNew.gif'" alt="Create New Nominee">
  <img name="B6" src="<%=request.getContextPath()%>/images/btnDelete.gif" onclick="askdelete()" onMouseOver="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnDeleteOn.gif'" onMouseOut="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnDelete.gif'">
  <table width="100%" BORDER=1  cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Dividend Group Information</center></td></tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="20%" class="style9"><div align="center">Dividend Group No.</div></td>
    <td width="50%"><div align="center" class="style9">
      <div align="center">Name</div>
    </div></td>
    <td width="30%"><div align="center" class="style9">
      <div align="center">Contact Person</div>
    </div></td>
  </tr>
  <div align="left">
     <%
    while(cm.toNext())
     {

       nominee_no = cm.getColumnS("NOMINEE_NO");
       name = cm.getColumnS("NAME");
       address = cm.getColumnS("ADDRESS1");
       contact = cm.getColumnS("CONTACTPERSON");
       //telephone = cm.getColumnS("TELEPHONE");
      // remarks = cm.getColumnS("REMARKS");


       dest = request.getContextPath() + "/jsp/Nominee/EditNominee.jsp";

       if (!nominee_no.equals("null"))
       {
         if (String.valueOf(name).equals("null"))
           name = "";
         if (String.valueOf(contact).equals("null"))
           contact = "";

         %>
    </div>
  <tr bgcolor="#E8F3FD">
           <td><div align="center" class="style11">
             <div align="left" class="style11">
               <span class="style11">
              <label><input type="radio" checked="checked" name="nominee_no" value="<%=nominee_no%>"></label>
               &nbsp;&nbsp;&nbsp<%=nominee_no%>
               &nbsp;
               </span></div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="LEFT">&nbsp;<a HREF="<%=dest%>?nominee_no=<%=nominee_no%>"><%=name%>&nbsp;</a></div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="left">&nbsp;&nbsp;<%=contact%></div>
           </div></td>

         </tr>
         <div align="left" class="style13">
             <%
         }
     }
   if (isConnect)
   {
     cm.takeDown();
   }
  %>
         </div>
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>

</body>
</html>
