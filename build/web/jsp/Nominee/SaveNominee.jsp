<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Creation Date : December 2005
'Page Purpose  : Save new Nominee.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Dividend Group</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
      String name = String.valueOf(request.getParameter("name"));
      String contact = String.valueOf(request.getParameter("contact"));
      String telephone = String.valueOf(request.getParameter("telephone"));
      String address = String.valueOf(request.getParameter("address"));
      String remarks = String.valueOf(request.getParameter("remarks"));

      name=cm.replace(name,"'","''");
      contact=cm.replace(contact,"'","''");
      telephone=cm.replace(telephone,"'","''");
      address=cm.replace(address,"'","''");
      remarks=cm.replace(remarks,"'","''");

      String addNomineeQuery = "call ADD_NOMINEE('" + name + "', '" + address + "', '" + telephone + "', '"+ remarks + "', '"+contact+"')";
      System.out.println(addNomineeQuery);
      boolean b = cm.procedureExecute(addNomineeQuery);

/*
'******************************************************************************************************************************************
'Updated By    : Renad Hakim
'Date          : March 2005
'******************************************************************************************************************************************
*/

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added Dividend Group Information')";
  boolean ub = cm.procedureExecute(ulog);

     if (isConnect)
     {
       cm.takeDown();
     }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Nominee/AllNominees.jsp";
</script>
</body>
</html>
