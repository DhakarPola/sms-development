<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : October 2005
'Page Purpose  : View of all the banks.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Banks</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function tonewbank()
{
  location = "<%=request.getContextPath()%>/jsp/Bank/NewBank.jsp"
}

function todeletebank()
{
  location = "<%=request.getContextPath()%>/jsp/Bank/DeleteBank.jsp?"
}

 function askdelete()
 {
  if (confirm("Do you want to Delete the Bank Information?"))
  {
   document.forms[0].submit();
  }
 }

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String query1 = "SELECT * FROM BANK_VIEW ORDER BY UPPER(BANK_NAME)";
    cm.queryExecute(query1);

    String bankname = "";
    String bankbranch = "";
    String banktelephone = "";
    String bankcontact = "";
    String dest = "";

    int bankid = 0;
%>
  <span class="style7">
  <form method="GET" action="DeleteBank.jsp">
  <img name="B4" src="<%=request.getContextPath()%>/images/btnNew.gif" onclick="tonewbank()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNewOn.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNew.gif'">
  <img name="B6" src="<%=request.getContextPath()%>/images/btnDelete.gif" onclick="askdelete()" onMouseOver="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnDeleteOn.gif'" onMouseOut="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnDelete.gif'">
  <table width="100%" BORDER=1  cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Bank Information</center></td></tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="37%" class="style9"><div align="center">Bank Name</div></td>
    <td width="25%"><div align="center" class="style9">
      <div align="center">Branch</div>
    </div></td>
    <td width="15%"><div align="center" class="style9">
      <div align="center">Telephone</div>
    </div></td>
    <td width="23%"><div align="center" class="style9">
      <div align="center">Contact Person</div>
    </div></td>
  </tr>
  <div align="left">
     <%
    while(cm.toNext())
     {
       bankname = cm.getColumnS("BANK_NAME");
       bankbranch = cm.getColumnS("BANK_BRANCH");
       bankid = cm.getColumnI("BANK_ID");
       banktelephone = cm.getColumnS("TELEPHONE");
       bankcontact = cm.getColumnS("CONTACT_PERSON");

       dest = request.getContextPath() + "/jsp/Bank/EditBank.jsp";

       if (!bankname.equals("null"))
       {
         if (String.valueOf(bankbranch).equals("null"))
           bankbranch = "";
         if (String.valueOf(banktelephone).equals("null"))
           banktelephone = "";
         if (String.valueOf(bankcontact).equals("null"))
           bankcontact = "";
         %>
    </div>
  <tr bgcolor="#E8F3FD">
           <td><div align="center" class="style10">
             <div align="left" class="style10">
               <span class="style13">
              <label><input type="radio" checked="checked" name="dbank" value="<%=bankid%>"></label>
               &nbsp;&nbsp;&nbsp<a HREF="<%=dest%>?bname1=<%=bankname%>&bid1=<%=bankid%>"><%=bankname%></a>
               &nbsp;
               </span></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="left">&nbsp;<%=bankbranch%></div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=banktelephone%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="left">&nbsp;<%=bankcontact%></div>
           </div></td>
         </tr>
         <div align="left" class="style13">
             <%
         }
     }
   if (isConnect)
   {
     cm.takeDown();
   }
  %>
         </div>
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>

</body>
</html>
