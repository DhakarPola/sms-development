<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : October 2005
'Page Purpose  : Adds a new Simple Transfer.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Simple Transfer</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function formconfirm()
{
  if (confirm("Do you want to do the mentioned Simple Transfer?"))
  {
    return true;
  }
}

function canwesplit()
{
  if (document.forms[0].cansplit.checked)
  {
    document.all.punits.style.display = ''
  }
  else
  {
    document.all.punits.style.display = 'none'
  }
}

function addprimaryshares(checkvalue1,checkvalue2,checkvalue3)
{
  var checkme1;
  var checkme2;
  var ptotal;
  checkme1 = document.all[checkvalue1].value;
  checkme2 = document.all[checkvalue2].value;

  if((checkme1.length > 0)&&(checkme2.length > 0))
  {
    if ((checkme2 - checkme1) > -1)
    {
      ptotal = checkme2 - checkme1 + 1;
      document.all[checkvalue3].value = ptotal;
      addshares(checkvalue3);
    }
  }
}

function addshares(checkvalue)
{
  var checkme;
  checkme = document.all[checkvalue].value;

  if(checkme.length <= 0)
  {
    document.all[checkvalue].value='';
  }

  var quantity1;
  var quantity2;
  var quantity3;
  var quantity4;
  var Totalquantitycal;

  quantity1 = replaceChars(document.all["nofshares1"].value,",","");
  quantity2 = replaceChars(document.all["nofshares2"].value,",","");
  quantity3 = replaceChars(document.all["nofshares3"].value,",","");
  quantity4 = replaceChars(document.all["nofshares4"].value,",","");

  Totalquantitycal = 0;

  if(quantity1 > 0)
   Totalquantitycal = Number(Totalquantitycal) + Number(quantity1);
  if(quantity2 > 0)
   Totalquantitycal = Number(Totalquantitycal) + Number(quantity2);
  if(quantity3 > 0)
   Totalquantitycal = Number(Totalquantitycal) + Number(quantity3);
  if(quantity4 > 0)
   Totalquantitycal = Number(Totalquantitycal) + Number(quantity4);

  document.all["tnofshares"].value = Totalquantitycal;
}

function setbarcode()
{
  var val1 = document.all["certificateno"].value;

  if (val1.length > 0)
  {
   location = "<%=request.getContextPath()%>/jsp/Transfer/PutBarcode.jsp?pgname=simpletransfer&certno=" + val1;
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('sfolio','- Seller Folio (Option must be entered)');
  BlankValidate('certificateno','- Certificate no. (Option must be entered)');
  BlankValidate('bfolio','- Buyer Folio (Option must be entered)');

  var val1;
  var val2;
  var val3;
  var val4;
  var val5;
  var val6;
  var val7;
  var val8;
  var val9;
  var val10;
  var val11;
  var val12;
  var val13;
  var val14;

  val13  = trim(window.document.all["sfolio"].value);
  val14  = trim(window.document.all["bfolio"].value);

  if ((val13 != "") && (val14 != ""))
  {
    if (val13 == val14)
     {
       count = count + 1;
       nArray[count] = '- Buyer and Seller Folios are same';
     }
  }

  val1  = trim(window.document.all["distfrom1"].value);
  val2  = trim(window.document.all["distto1"].value);
  val3  = trim(window.document.all["nofshares1"].value);
  val4  = trim(window.document.all["distfrom2"].value);
  val5  = trim(window.document.all["distto2"].value);
  val6  = trim(window.document.all["nofshares2"].value);
  val7  = trim(window.document.all["distfrom3"].value);
  val8  = trim(window.document.all["distto3"].value);
  val9  = trim(window.document.all["nofshares3"].value);
  val10 = trim(window.document.all["distfrom4"].value);
  val11 = trim(window.document.all["distto4"].value);
  val12 = trim(window.document.all["nofshares4"].value);

  if (((val1 == "") && (val2 == "") && (val3 == "")) && ((val4 == "") && (val5 == "") && (val6 == "")) && ((val7 == "") && (val8 == "") && (val9 == "") &&((val10 == "") && (val11 == "") && (val12 == ""))))
   {
     count = count + 1;
     nArray[count] = '- At least one Distinction must be entered';
   }
  else
  {
    if ((val1 != "") || (val2 != "") || (val3 != ""))
    {
      if ((val1 == "") || (val2 == "") || (val3 == ""))
      {
       count = count + 1;
       nArray[count] = '- Distinction 1 is Incomplete or Invalid';
      }
    }
    if ((val4 != "") || (val5 != "") || (val6 != ""))
    {
      if ((val4 == "") || (val5 == "") || (val6 == ""))
      {
       count = count + 1;
       nArray[count] = '- Distinction 2 is Incomplete or Invalid';
      }
    }
    if ((val7 != "") || (val8 != "") || (val9 != ""))
    {
      if ((val7 == "") || (val8 == "") || (val9 == ""))
      {
       count = count + 1;
       nArray[count] = '- Distinction 3 is Incomplete or Invalid';
      }
    }
    if ((val10 != "") || (val11 != "") || (val12 != ""))
    {
      if ((val10 == "") || (val11 == "") || (val12 == ""))
      {
       count = count + 1;
       nArray[count] = '- Distinction 4 is Incomplete or Invalid';
      }
    }
  }

  if (document.forms[0].cansplit.checked)
  {
    BlankValidate('proposedunits','- Proposed Unit (Option must be entered)');
  }

  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF" onload="canwesplit()">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

  String dist1from = String.valueOf(request.getParameter("dist1from"));
  String dist1to = String.valueOf(request.getParameter("dist1to"));
  String dist2from = String.valueOf(request.getParameter("dist2from"));
  String dist2to = String.valueOf(request.getParameter("dist2to"));
  String dist3from = String.valueOf(request.getParameter("dist3from"));
  String dist3to = String.valueOf(request.getParameter("dist3to"));
  String dist4from = String.valueOf(request.getParameter("dist4from"));
  String dist4to = String.valueOf(request.getParameter("dist4to"));
  String HolderFolio = String.valueOf(request.getParameter("hfolio"));
  String Certificate1No = String.valueOf(request.getParameter("cert1no"));
  int nofshares = 0;
  int tnofshares = 0;
  String nofshares1 = "";
  String nofshares2 = "";
  String nofshares3 = "";
  String nofshares4 = "";
  String Stnofshares = "";

  if (String.valueOf(dist1from).equals("null"))
    dist1from = "";
  if (String.valueOf(dist1to).equals("null"))
    dist1to = "";
  if (String.valueOf(dist2from).equals("null"))
    dist2from = "";
  if (String.valueOf(dist2to).equals("null"))
    dist2to = "";
  if (String.valueOf(dist3from).equals("null"))
    dist3from = "";
  if (String.valueOf(dist3to).equals("null"))
    dist3to = "";
  if (String.valueOf(dist4from).equals("null"))
    dist4from = "";
  if (String.valueOf(dist4to).equals("null"))
    dist4to = "";
  if (String.valueOf(HolderFolio).equals("null"))
    HolderFolio = "";
  if (String.valueOf(Certificate1No).equals("null"))
    Certificate1No = "";

  if (dist1from.length() > 0)
  {
    nofshares = Integer.parseInt(dist1to) - Integer.parseInt(dist1from) + 1;
    tnofshares = tnofshares + nofshares;
    nofshares1 = String.valueOf(nofshares);
  }
  if (dist2from.length() > 0)
  {
    nofshares = Integer.parseInt(dist2to) - Integer.parseInt(dist2from) + 1;
    tnofshares = tnofshares + nofshares;
    nofshares2 = String.valueOf(nofshares);
  }
  if (dist3from.length() > 0)
  {
    nofshares = Integer.parseInt(dist3to) - Integer.parseInt(dist3from) + 1;
    tnofshares = tnofshares + nofshares;
    nofshares3 = String.valueOf(nofshares);
  }
  if (dist4from.length() > 0)
  {
    nofshares = Integer.parseInt(dist4to) - Integer.parseInt(dist4from) + 1;
    tnofshares = tnofshares + nofshares;
    nofshares4 = String.valueOf(nofshares);
  }

  if (tnofshares == 0)
  {
   Stnofshares = "";
  }
  else
  {
   Stnofshares = String.valueOf(tnofshares);
  }
%>

<form action="SaveSimpleTransfer.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Simple Transfer</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Seller Folio</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="sfolio" class="SL2TextField" value="<%=HolderFolio%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Old Certificate No.</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%" valign="middle"><div align="left"><input type="text" name="certificateno" class="SL2TextField" onkeypress="keypressOnNumberFld()" value="<%=Certificate1No%>">
          <br>
          <img name="B3" src="<%=request.getContextPath()%>/images/btnSearch.gif"  onclick="setbarcode()" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnSearchR.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnSearch.gif'">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Distinction(s)</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left">

         <table width="75%"  border="0" cellpadding="2">
           <tr>
            <td width="33%" align="center"><div align="center" class="style8"><b>From</b></div></th>
            <td width="33%" align="center"><div align="center" class="style8"><b>To</b></div></td>
            <td width="33%" align="center"><div align="center" class="style8"><b>No. of Shares</b></div></td>
           </tr>
           <tr>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="distfrom1" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom1','distto1','nofshares1')" value="<%=dist1from%>"></div></td>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="distto1" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom1','distto1','nofshares1')" value="<%=dist1to%>"></div></td>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="nofshares1" class="SL67TextField"  onkeypress="keypressOnNumberFld()" onfocus="this.blur()" value="<%=nofshares1%>"></div></td>
           </tr>
           <tr>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="distfrom2" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom2','distto2','nofshares2')" value="<%=dist2from%>"></div></td>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="distto2" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom2','distto2','nofshares2')" value="<%=dist2to%>"></div></td>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="nofshares2" class="SL67TextField"  onkeypress="keypressOnNumberFld()" onfocus="this.blur()" value="<%=nofshares2%>"></div></td>
           </tr>
           <tr>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="distfrom3" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom3','distto3','nofshares3')" value="<%=dist3from%>"></div></td>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="distto3" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom3','distto3','nofshares3')" value="<%=dist3to%>"></div></td>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="nofshares3" class="SL67TextField"  onkeypress="keypressOnNumberFld()" onfocus="this.blur()" value="<%=nofshares3%>"></div></td>
           </tr>
           <tr>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="distfrom4" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom4','distto4','nofshares4')" value="<%=dist4from%>"></div></td>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="distto4" class="SL67TextField" onkeypress="keypressOnNumberFld()" onBlur="addprimaryshares('distfrom4','distto4','nofshares4')" value="<%=dist4to%>"></div></td>
            <td width="33%" align="center"><div align="center" class="style8"><input type="text" name="nofshares4" class="SL67TextField"  onkeypress="keypressOnNumberFld()" onfocus="this.blur()" value="<%=nofshares4%>"></div></td>
           </tr>
        </table>

        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Total Shares to be Transferred </div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="tnofshares" class="SL2TextField" onfocus="this.blur()" value="<%=Stnofshares%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Buyer Folio </div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="bfolio" class="SL2TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;
          Split&nbsp;
          <input type="checkbox" name="cansplit" value="checkbox" onclick="canwesplit()"></div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left" class="style8">
          <SPAN id="punits" style="display:none">
            <input type="text" name="proposedunits" class="SL2TextField" onkeypress="keypressOnNumberFld()">
            &nbsp;<b>(Proposed Unit)</b>
          </span>
            </div></td>
      </tr>

  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="10" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <td width="50%" align="right">
 	  <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
      </td>
        <td width="50%" align="left">
          <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
      </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
