<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : November 2005
'Page Purpose  : Saves Accepted Transfers.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@ page language="java" import="java.sql.*,java.util.Vector,java.util.Date,java.text.SimpleDateFormat,java.math.*" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Accepted Transfers</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

    int transferid = 0;
    String sfolio = "";
    String sname = "";
    String bfolio = "";
    String bname = "";
    String typetransfer = "";
    String distfrom1 = "";
    String distto1 = "";
    String distfrom2 = "";
    String distto2 = "";
    String distfrom3 = "";
    String distto3 = "";
    String distfrom4 = "";
    String distto4 = "";
    int nofshares = 0;
    int proposedunits = 0;
    String snofshares = "0";
    String sproposedunits = "0";
    String certificatefrom = "";
    String certificateto = "";
    String xcer1 = "";
    String xcer2 = "";
    String xcer3 = "";
    String xcer4 = "";
    String newcertificate = "";
    String issplit = "";
    int totalrequests = 0;

    String query1 = "SELECT * FROM TRANSFER_T_VIEW ORDER BY REC_NO";
    cm.queryExecute(query1);

    String dlasttransfer = "call DELETE_LAST_TRANSFER()";
    boolean b1 = cm1.procedureExecute(dlasttransfer);

    while(cm.toNext())
     {
       totalrequests++;
       transferid = cm.getColumnI("REC_NO");
       sfolio = cm.getColumnS("SELLER_FOLIO");
       sname = cm.getColumnS("SELLER_NAME");
       bfolio = cm.getColumnS("BUYER_FOLIO");
       bname = cm.getColumnS("BUYER_NAME");
       typetransfer = cm.getColumnS("TYPE_OF_TRANSFER");
       distfrom1 = cm.getColumnS("DIST_FROM1");
       distto1 = cm.getColumnS("DIST_TO1");
       distfrom2 = cm.getColumnS("DIST_FROM2");
       distto2 = cm.getColumnS("DIST_TO2");
       distfrom3 = cm.getColumnS("DIST_FROM3");
       distto3 = cm.getColumnS("DIST_TO3");
       distfrom4 = cm.getColumnS("DIST_FROM4");
       distto4 = cm.getColumnS("DIST_TO4");
       nofshares = cm.getColumnI("NO_OF_SHARES");
       issplit = cm.getColumnS("SPLIT");
       if (issplit.equalsIgnoreCase("T"))
       {
         proposedunits = cm.getColumnI("PROPOSED_UNITS");
         sproposedunits = String.valueOf(proposedunits);
       }
       certificatefrom = cm.getColumnS("OLD_CERTIFICATE_FROM");
       certificateto = cm.getColumnS("OLD_CERTIFICATE_TO");
       xcer1 = cm.getColumnS("XCERTIFICATE1");
       xcer2 = cm.getColumnS("XCERTIFICATE2");
       xcer3 = cm.getColumnS("XCERTIFICATE3");
       xcer4 = cm.getColumnS("XCERTIFICATE4");

       snofshares = String.valueOf(nofshares);

       if (String.valueOf(sfolio).equals("null"))
         sfolio = "";
       if (String.valueOf(sname).equals("null"))
         sname = "";
       if (String.valueOf(bfolio).equals("null"))
         bfolio = "";
       if (String.valueOf(bname).equals("null"))
         bname = "";
       if (String.valueOf(typetransfer).equals("null"))
         typetransfer = "";
       if (String.valueOf(distfrom1).equals("null"))
         distfrom1 = "";
       if (String.valueOf(distto1).equals("null"))
         distto1 = "";
       if (String.valueOf(distfrom2).equals("null"))
         distfrom2 = "";
       if (String.valueOf(distto2).equals("null"))
         distto2 = "";
       if (String.valueOf(distfrom3).equals("null"))
         distfrom3 = "";
       if (String.valueOf(distto3).equals("null"))
         distto3 = "";
       if (String.valueOf(distfrom4).equals("null"))
         distfrom4 = "";
       if (String.valueOf(distto4).equals("null"))
         distto4 = "";
       if (String.valueOf(certificatefrom).equals("null"))
         certificatefrom = "";
       if (String.valueOf(certificateto).equals("null"))
         certificateto = "";
       if (String.valueOf(xcer1).equals("null"))
         xcer1 = "";
       if (String.valueOf(xcer2).equals("null"))
         xcer2 = "";
       if (String.valueOf(xcer3).equals("null"))
         xcer3 = "";
       if (String.valueOf(xcer4).equals("null"))
         xcer4 = "";

       String sbal = "call ADD_SHARE_BALANCE('" + bfolio + "', " + nofshares + ")";
       boolean sb = cm1.procedureExecute(sbal);

       sbal = "call SUB_SHARE_BALANCE('" + sfolio + "', " + nofshares + ")";
       sb = cm1.procedureExecute(sbal);

       if (typetransfer.equalsIgnoreCase("T"))
       {
         String sbinvalid = "call SET_CERTIFICATE_INVALID('" + certificatefrom + "')";
         boolean b2 = cm1.procedureExecute(sbinvalid);

         if (issplit.equalsIgnoreCase("T"))
         {
           double chunks = Double.parseDouble(snofshares)/Double.parseDouble(sproposedunits);
           chunks = Math.ceil(chunks);

           int thisdistfrom = Integer.parseInt(distfrom1);
           int thisdistto = Integer.parseInt(distto1);
           int thisdisttof = thisdistto;

           for (double i1=chunks;i1>0;i1=i1-1)
           {
              thisdistto = thisdistfrom + proposedunits - 1;
              if (thisdistto > thisdisttof)
              {
                thisdistto = thisdisttof;
              }

              int chnofshares = thisdistto - thisdistfrom + 1;
              newcertificate = cm1.LastCertificate();
              String addtransfer = "call ADD_TRANSFER('" + sfolio + "', '" + bfolio + "', '" + certificatefrom + "', '" + typetransfer + "', '" + newcertificate + "','" + String.valueOf(thisdistfrom) + "','" + String.valueOf(thisdistto) + "'," + chnofshares + ")";

              boolean b3 = cm1.procedureExecute(addtransfer);

              String addcertificate = "call ADD_CERTIFICATE('" + newcertificate + "', '" + bfolio + "', 'T', 'F', 'F','F','F','F','')";
              boolean b4 = cm1.procedureExecute(addcertificate);

              String adddist = "call ADD_DISTINCTION(" + thisdistfrom + ", " + thisdistto + ", '" + newcertificate + "')";
              boolean b5 = cm1.procedureExecute(adddist);

              String addlasttransfer = "call ADD_LAST_TRANSFER('" + bfolio + "', '" + bname + "', '" + typetransfer + "', '" + String.valueOf(thisdistfrom) + "', '" + String.valueOf(thisdistto) + "','','','','','','','" + newcertificate + "')";
              boolean b6 = cm1.procedureExecute(addlasttransfer);

              thisdistfrom = thisdistfrom + proposedunits;
           }
         }
         else
         {
           int thisdistfrom = Integer.parseInt(distfrom1);
           int thisdistto = Integer.parseInt(distto1);

           newcertificate = cm1.LastCertificate();
           String addtransfer = "call ADD_TRANSFER('" + sfolio + "', '" + bfolio + "', '" + certificatefrom + "', '" + typetransfer + "', '" + newcertificate + "','" + distfrom1 + "','" + distto1 + "'," + nofshares + ")";
           boolean b7 = cm1.procedureExecute(addtransfer);

           String addcertificate = "call ADD_CERTIFICATE('" + newcertificate + "', '" + bfolio + "', 'T', 'F', 'F','F','F','F','')";
           boolean b8 = cm1.procedureExecute(addcertificate);

           String adddist = "call ADD_DISTINCTION(" + thisdistfrom + ", " + thisdistto + ", '" + newcertificate + "')";
           boolean b9 = cm1.procedureExecute(adddist);

           String addlasttransfer = "call ADD_LAST_TRANSFER('" + bfolio + "', '" + bname + "', '" + typetransfer + "', '" + String.valueOf(thisdistfrom) + "', '" + String.valueOf(thisdistto) + "','','','','','','','" + newcertificate + "')";
           boolean b10 = cm1.procedureExecute(addlasttransfer);
         }
       }
       else if (typetransfer.equalsIgnoreCase("M"))
       {
         if (bfolio.equalsIgnoreCase(sfolio))
         {
          //Multiple Split

          int certlength = certificatefrom.length();
          int icertificatefrom = Integer.parseInt(certificatefrom);
          int icertificateto = Integer.parseInt(certificateto);
          int nofcertificates = icertificateto - icertificatefrom + 1;
          String combcert = "";

          for (int certcount=0;certcount<nofcertificates;certcount++)
          {
            String thiscertificate = "";
            int ithiscertificate = icertificatefrom + certcount;
            thiscertificate = String.valueOf(ithiscertificate);

            if (thiscertificate.length() < certlength)
            {
              int sizedifferance = certlength - thiscertificate.length();
              for (int ib=0;ib<sizedifferance;ib++)
              {
                thiscertificate = "0" + thiscertificate;
              }
            }
            String sbinvalid = "call SET_CERTIFICATE_INVALID('" + thiscertificate + "')";
            boolean b41 = cm1.procedureExecute(sbinvalid);

            if (combcert.equalsIgnoreCase(""))
            {
             combcert = thiscertificate;
            }
            else
            {
             combcert = combcert + ", " + thiscertificate;
            }
          }

//          double chunks = nofshares/proposedunits;
          double chunks = Double.parseDouble(snofshares)/Double.parseDouble(sproposedunits);
          chunks = Math.ceil(chunks);

          int thisdistfrom = Integer.parseInt(distfrom1);
          int thisdistto = Integer.parseInt(distto1);
          int thisdisttof = thisdistto;

          for (double i1=chunks;i1>0;i1=i1-1)
          {
           thisdistto = thisdistfrom + proposedunits - 1;
           if (thisdistto > thisdisttof)
           {
             thisdistto = thisdisttof;
           }

           int chnofshares = thisdistto - thisdistfrom + 1;

           newcertificate = cm1.LastCertificate();

           String addtransfer = "call ADD_TRANSFER('" + sfolio + "', '" + bfolio + "', '" + combcert + "', '" + typetransfer + "', '" + newcertificate + "','" + String.valueOf(thisdistfrom) + "','" + String.valueOf(thisdistto) + "'," + chnofshares + ")";
           boolean b42 = cm1.procedureExecute(addtransfer);

           String addcertificate = "call ADD_CERTIFICATE('" + newcertificate + "', '" + bfolio + "', 'T', 'F', 'F','F','F','F','')";
           boolean b43 = cm1.procedureExecute(addcertificate);

           String adddist = "call ADD_DISTINCTION(" + thisdistfrom + ", " + thisdistto + ", '" + newcertificate + "')";
           boolean b44 = cm1.procedureExecute(adddist);

           String addlasttransfer = "call ADD_LAST_TRANSFER('" + bfolio + "', '" + bname + "', '" + typetransfer + "', '" + String.valueOf(thisdistfrom) + "', '" + String.valueOf(thisdistto) + "','','','','','','','" + newcertificate + "')";
           boolean b45 = cm1.procedureExecute(addlasttransfer);

           thisdistfrom = thisdistfrom + proposedunits;
          }
         }
         else
         {
           //Multiple Transfer
           int certlength = certificatefrom.length();
           int icertificatefrom = Integer.parseInt(certificatefrom);
           int icertificateto = Integer.parseInt(certificateto);
           int nofcertificates = 1;
           nofcertificates = icertificateto - icertificatefrom + 1;
           String combcert = "";

//           String arrcert[][] = new String[nofcertificates][nofcertificates];

           for (int certcount=0;certcount<nofcertificates;certcount++)
           {
             String thiscertificate = "";
             int ithiscertificate = icertificatefrom + certcount;
             thiscertificate = String.valueOf(ithiscertificate);

             if (thiscertificate.length() < certlength)
             {
               int sizedifferance = certlength - thiscertificate.length();
               for (int ib=0;ib<sizedifferance;ib++)
               {
                 thiscertificate = "0" + thiscertificate;
               }
             }

//             arrcert[certcount][certcount] = thiscertificate;

             String sbinvalid = "call SET_CERTIFICATE_INVALID('" + thiscertificate + "')";
             boolean b32 = cm1.procedureExecute(sbinvalid);

             if (combcert.equalsIgnoreCase(""))
             {
              combcert = thiscertificate;
             }
             else
             {
              combcert = combcert + ", " + thiscertificate;
             }
           }

           if (issplit.equalsIgnoreCase("T"))
           {
//             double chunks = nofshares/proposedunits;
             double chunks = Double.parseDouble(snofshares)/Double.parseDouble(sproposedunits);
             chunks = Math.ceil(chunks);

             int thisdistfrom = Integer.parseInt(distfrom1);
             int thisdistto = Integer.parseInt(distto1);
             int thisdisttof = thisdistto;

             for (double i1=chunks;i1>0;i1=i1-1)
             {
              thisdistto = thisdistfrom + proposedunits - 1;
              if (thisdistto > thisdisttof)
              {
                thisdistto = thisdisttof;
              }

              int chnofshares = thisdistto - thisdistfrom + 1;

              newcertificate = cm1.LastCertificate();

              String addtransfer = "call ADD_TRANSFER('" + sfolio + "', '" + bfolio + "', '" + combcert + "', '" + typetransfer + "', '" + newcertificate + "','" + String.valueOf(thisdistfrom) + "','" + String.valueOf(thisdistto) + "'," + chnofshares + ")";
              boolean b33 = cm1.procedureExecute(addtransfer);

              String addcertificate = "call ADD_CERTIFICATE('" + newcertificate + "', '" + bfolio + "', 'T', 'F', 'F','F','F','F','')";
              boolean b34 = cm1.procedureExecute(addcertificate);

              String adddist = "call ADD_DISTINCTION(" + thisdistfrom + ", " + thisdistto + ", '" + newcertificate + "')";
              boolean b35 = cm1.procedureExecute(adddist);

              String addlasttransfer = "call ADD_LAST_TRANSFER('" + bfolio + "', '" + bname + "', '" + typetransfer + "', '" + String.valueOf(thisdistfrom) + "', '" + String.valueOf(thisdistto) + "','','','','','','','" + newcertificate + "')";
              boolean b36 = cm1.procedureExecute(addlasttransfer);

              thisdistfrom = thisdistfrom + proposedunits;
             }
           }
           else
           {
             //int thisdistfrom = Integer.parseInt(distfrom1);
             //int thisdistto = Integer.parseInt(distto1);
             double thisdistfrom = Integer.parseInt(distfrom1);
             double thisdistto = Integer.parseInt(distto1);

             newcertificate = cm1.LastCertificate();
             String addtransfer = "call ADD_TRANSFER('" + sfolio + "', '" + bfolio + "', '" + combcert + "', '" + typetransfer + "', '" + newcertificate + "','" + distfrom1 + "','" + distto1 + "'," + nofshares + ")";
             boolean b37 = cm1.procedureExecute(addtransfer);

             String addcertificate = "call ADD_CERTIFICATE('" + newcertificate + "', '" + bfolio + "', 'T', 'F', 'F','F','F','F','')";
             boolean b38 = cm1.procedureExecute(addcertificate);

             String adddist = "call ADD_DISTINCTION(" + thisdistfrom + ", " + thisdistto + ", '" + newcertificate + "')";
             boolean b39 = cm1.procedureExecute(adddist);

             String addlasttransfer = "call ADD_LAST_TRANSFER('" + bfolio + "', '" + bname + "', '" + typetransfer + "', '" + String.valueOf(thisdistfrom) + "', '" + String.valueOf(thisdistto) + "','','','','','','','" + newcertificate + "')";
             boolean b40 = cm1.procedureExecute(addlasttransfer);
           }
         }
       }
       else if (typetransfer.equalsIgnoreCase("S"))
       {
        String sbinvalid = "call SET_CERTIFICATE_INVALID('" + certificatefrom + "')";
        boolean b22 = cm1.procedureExecute(sbinvalid);

//        double chunks = nofshares/proposedunits;
        double chunks = Double.parseDouble(snofshares)/Double.parseDouble(sproposedunits);
        chunks = Math.ceil(chunks);

        int thisdistfrom = Integer.parseInt(distfrom1);
        int thisdistto = Integer.parseInt(distto1);
        int thisdisttof = thisdistto;

        for (double i1=chunks;i1>0;i1=i1-1)
        {
         thisdistto = thisdistfrom + proposedunits - 1;

         if (thisdistto > thisdisttof)
         {
           thisdistto = thisdisttof;
         }

         int chnofshares = thisdistto - thisdistfrom + 1;

         newcertificate = cm1.LastCertificate();
         String addtransfer = "call ADD_TRANSFER('" + sfolio + "', '" + bfolio + "', '" + certificatefrom + "', '" + typetransfer + "', '" + newcertificate + "','" + String.valueOf(thisdistfrom) + "','" + String.valueOf(thisdistto) + "'," + chnofshares + ")";
         boolean b23 = cm1.procedureExecute(addtransfer);

         String addcertificate = "call ADD_CERTIFICATE('" + newcertificate + "', '" + bfolio + "', 'T', 'F', 'F','F','F','F','')";
         boolean b24 = cm1.procedureExecute(addcertificate);

         String adddist = "call ADD_DISTINCTION(" + thisdistfrom + ", " + thisdistto + ", '" + newcertificate + "')";
         boolean b25 = cm1.procedureExecute(adddist);

         String addlasttransfer = "call ADD_LAST_TRANSFER('" + bfolio + "', '" + bname + "', '" + typetransfer + "', '" + String.valueOf(thisdistfrom) + "', '" + String.valueOf(thisdistto) + "','','','','','','','" + newcertificate + "')";
         boolean b26 = cm1.procedureExecute(addlasttransfer);

         thisdistfrom = thisdistfrom + proposedunits;
        }

       }
       else if (typetransfer.equalsIgnoreCase("P"))
       {
         String[][] distarr = new String[4][2];
         distarr[0][0] = distfrom1;
         distarr[0][1] = distto1;
         distarr[1][0] = distfrom2;
         distarr[1][1] = distto2;
         distarr[2][0] = distfrom3;
         distarr[2][1] = distto3;
         distarr[3][0] = distfrom4;
         distarr[3][1] = distto4;

         String sbinvalid = "call SET_CERTIFICATE_INVALID('" + certificatefrom + "')";
         boolean b28 = cm1.procedureExecute(sbinvalid);

         for (int distcont=0;distcont<4;distcont++)
         {
           if (distarr[distcont][0].length() > 0)
           {
             newcertificate = cm1.LastCertificate();

             String addcertificate = "call ADD_CERTIFICATE('" + newcertificate + "', '" + bfolio + "', 'T', 'F', 'F','F','F','F','')";
             boolean b27 = cm1.procedureExecute(addcertificate);

             int chnofshares = Integer.parseInt(distarr[distcont][1]) - Integer.parseInt(distarr[distcont][0]) + 1;

             String addtransfer = "call ADD_TRANSFER('" + sfolio + "', '" + bfolio + "', '" + certificatefrom + "', '" + typetransfer + "', '" + newcertificate + "'," + distarr[distcont][0] + "," + distarr[distcont][1] + "," + chnofshares + ")";
             boolean b29 = cm1.procedureExecute(addtransfer);

             String adddist = "call ADD_DISTINCTION(" + distarr[distcont][0] + ", " + distarr[distcont][1] + ", '" + newcertificate + "')";
             boolean b30 = cm1.procedureExecute(adddist);

             String addlasttransfer = "call ADD_LAST_TRANSFER('" + bfolio + "', '" + bname + "', '" + typetransfer + "','" + distarr[distcont][0] + "', '" + distarr[distcont][1] + "','','','','','','','" + newcertificate + "')";
             boolean b31 = cm1.procedureExecute(addlasttransfer);
           }
         }

       }
       else if (typetransfer.equalsIgnoreCase("G"))
       {
         String[] xcerarr = new String[4];
         xcerarr[0] = xcer1;
         xcerarr[1] = xcer2;
         xcerarr[2] = xcer3;
         xcerarr[3] = xcer4;

         int[][] distarr = new int[4][2];
         if (!xcerarr[0].equalsIgnoreCase("0"))
         {
           distarr[0][0] = Integer.parseInt(distfrom1);
           distarr[0][1] = Integer.parseInt(distto1);
         }
         if (!xcerarr[1].equalsIgnoreCase("0"))
         {
           distarr[1][0] = Integer.parseInt(distfrom2);
           distarr[1][1] = Integer.parseInt(distto2);
         }
         if (!xcerarr[2].equalsIgnoreCase("0"))
         {
           distarr[2][0] = Integer.parseInt(distfrom3);
           distarr[2][1] = Integer.parseInt(distto3);
         }
         if (!xcerarr[3].equalsIgnoreCase("0"))
         {
           distarr[3][0] = Integer.parseInt(distfrom4);
           distarr[3][1] = Integer.parseInt(distto4);
         }

         newcertificate = cm1.LastCertificate();

         String addcertificate = "call ADD_CERTIFICATE('" + newcertificate + "', '" + bfolio + "', 'T', 'F', 'F','T','F','F','')";
         boolean b17 = cm1.procedureExecute(addcertificate);

         for (int xcercont=0;xcercont<4;xcercont++)
         {
           if(!xcerarr[xcercont].equalsIgnoreCase("0"))
           {
             String sbinvalid = "call SET_CERTIFICATE_INVALID('" + xcerarr[xcercont] + "')";
             boolean b18 = cm1.procedureExecute(sbinvalid);

             int chnofshares = distarr[xcercont][1] - distarr[xcercont][0] + 1;

             String addtransfer = "call ADD_TRANSFER('" + sfolio + "', '" + bfolio + "', '" + xcerarr[xcercont] + "', '" + typetransfer + "', '" + newcertificate + "'," + distarr[xcercont][0] + "," + distarr[xcercont][1] + "," + chnofshares + ")";
             boolean b19 = cm1.procedureExecute(addtransfer);

             String adddist = "call ADD_DISTINCTION(" + distarr[xcercont][0] + ", " + distarr[xcercont][1] + ", '" + newcertificate + "')";
             boolean b20 = cm1.procedureExecute(adddist);
           }
         }

        String addlasttransfer = "call ADD_LAST_TRANSFER('" + bfolio + "', '" + bname + "', '" + typetransfer + "','" + distarr[0][0] + "', '" + distarr[0][1] + "','" + distarr[1][0] + "','" + distarr[1][1] + "','" + distarr[2][0] + "','" + distarr[2][1] + "','" + distarr[3][0] + "','" + distarr[3][1] + "','" + newcertificate + "')";
        boolean b21 = cm1.procedureExecute(addlasttransfer);

       }
       else if (typetransfer.equalsIgnoreCase("C"))
       {
         String[] xcerarr = new String[4];
         xcerarr[0] = xcer1;
         xcerarr[1] = xcer2;
         xcerarr[2] = xcer3;
         xcerarr[3] = xcer4;

         int[][] distarr = new int[4][2];
         if (!xcerarr[0].equalsIgnoreCase("0"))
         {
           distarr[0][0] = Integer.parseInt(distfrom1);
           distarr[0][1] = Integer.parseInt(distto1);
         }
         if (!xcerarr[1].equalsIgnoreCase("0"))
         {
           distarr[1][0] = Integer.parseInt(distfrom2);
           distarr[1][1] = Integer.parseInt(distto2);
         }
         if (!xcerarr[2].equalsIgnoreCase("0"))
         {
           distarr[2][0] = Integer.parseInt(distfrom3);
           distarr[2][1] = Integer.parseInt(distto3);
         }
         if (!xcerarr[3].equalsIgnoreCase("0"))
         {
           distarr[3][0] = Integer.parseInt(distfrom4);
           distarr[3][1] = Integer.parseInt(distto4);
         }

         newcertificate = cm1.LastCertificate();

         String addcertificate = "call ADD_CERTIFICATE('" + newcertificate + "', '" + bfolio + "', 'T', 'F', 'F','F','F','F','')";
         boolean b11 = cm1.procedureExecute(addcertificate);

         int firstdist = 999999999;
         int lastdist = 0;

         for (int xcercont=0;xcercont<4;xcercont++)
         {
           if(!xcerarr[xcercont].equalsIgnoreCase("0"))
           {
             if (firstdist > distarr[xcercont][0])
             {
               firstdist = distarr[xcercont][0];
             }
             if (lastdist < distarr[xcercont][1])
             {
               lastdist = distarr[xcercont][1];
             }

             String sbinvalid = "call SET_CERTIFICATE_INVALID('" + xcerarr[xcercont] + "')";
             boolean b12 = cm1.procedureExecute(sbinvalid);

             int chnofshares = distarr[xcercont][1] - distarr[xcercont][0] + 1;

             String addtransfer = "call ADD_TRANSFER('" + sfolio + "', '" + bfolio + "', '" + xcerarr[xcercont] + "', '" + typetransfer + "', '" + newcertificate + "'," + distarr[xcercont][0] + "," + distarr[xcercont][1] + "," + chnofshares + ")";
             boolean b13 = cm1.procedureExecute(addtransfer);
           }
         }

        String adddist = "call ADD_DISTINCTION(" + firstdist + ", " + lastdist + ", '" + newcertificate + "')";
        boolean b14 = cm1.procedureExecute(adddist);

        String addlasttransfer = "call ADD_LAST_TRANSFER('" + bfolio + "', '" + bname + "', '" + typetransfer + "','" + String.valueOf(firstdist) + "', '" + String.valueOf(lastdist) + "','','','','','','','" + newcertificate + "')";
        boolean b15 = cm1.procedureExecute(addlasttransfer);
       }
   }

  String dalltransfert1 = "call DELETE_ALL_TRANSFER_T()";
  boolean b16 = cm1.procedureExecute(dalltransfert1);

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Accepted Share Transfer/Splitting Request(s)')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm1.takeDown();
     cm.takeDown();
   }
%>
<script language="javascript">
  alert(<%=totalrequests%> + " Transfer Request(s) Accepted!");
  location = "<%=request.getContextPath()%>/jsp/Transfer/AcceptTransfer.jsp";
</script>
</body>
</html>
