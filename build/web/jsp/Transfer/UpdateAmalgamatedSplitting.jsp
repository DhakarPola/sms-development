<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : November 2005
'Page Purpose  : Updates Amalgamated Splitting Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update Amalgamated Splitting</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String SellerFolio = String.valueOf(request.getParameter("sfolio"));
  String CertificateNo = String.valueOf(request.getParameter("certificateno"));
  String TotalShares = String.valueOf(request.getParameter("tnofshares"));
  String DistFrom1 = String.valueOf(request.getParameter("distfrom1"));
  String DistTo1 = String.valueOf(request.getParameter("distto1"));
  String DistFrom2 = String.valueOf(request.getParameter("distfrom2"));
  String DistTo2 = String.valueOf(request.getParameter("distto2"));
  String DistFrom3 = String.valueOf(request.getParameter("distfrom3"));
  String DistTo3 = String.valueOf(request.getParameter("distto3"));
  String DistFrom4 = String.valueOf(request.getParameter("distfrom4"));
  String DistTo4 = String.valueOf(request.getParameter("distto4"));
  String TransferID = String.valueOf(request.getParameter("transferid"));
  String SplitCheck = "F";
  String AmalCheck = "F";
  int distcounter = 0;
  int errorcounter = 0;

  int dist1 = 0;
  int dist2 = 0;
  int dist3 = 0;
  int dist4 = 0;

  if ((String.valueOf(DistFrom1).equals("null")) || (DistFrom1.equalsIgnoreCase("")))
  {
    distcounter++;
    dist1++;
  }
  if ((String.valueOf(DistFrom2).equals("null")) || (DistFrom2.equalsIgnoreCase("")))
  {
    distcounter++;
    dist2++;
  }
  if ((String.valueOf(DistFrom3).equals("null")) || (DistFrom3.equalsIgnoreCase("")))
  {
    dist3++;
    distcounter++;
  }
  if ((String.valueOf(DistFrom4).equals("null")) || (DistFrom4.equalsIgnoreCase("")))
  {
    dist4++;
    distcounter++;
  }

  distcounter = 4 - distcounter;

  if (distcounter < 2)
  {
    errorcounter++;
    %>
     <script language="javascript">
      alert("This might not be an Amalgamated Certificate. Please Check!");
      history.go(-1);
     </script>
    <%
  }

  SellerFolio=cm.replace(SellerFolio,"'","''");
  CertificateNo=cm.replace(CertificateNo,"'","''");
  DistFrom1=cm.replace(DistFrom1,"'","''");
  DistTo1=cm.replace(DistTo1,"'","''");
  DistFrom2=cm.replace(DistFrom2,"'","''");
  DistTo2=cm.replace(DistTo2,"'","''");
  DistFrom3=cm.replace(DistFrom3,"'","''");
  DistTo3=cm.replace(DistTo3,"'","''");
  DistFrom4=cm.replace(DistFrom4,"'","''");
  DistTo4=cm.replace(DistTo4,"'","''");
  TotalShares=cm.replace(TotalShares,"'","''");

  int iTotalShares = Integer.parseInt(TotalShares);

  String query10 = "SELECT * FROM SCRIPTS_VIEW WHERE LOWER(CERTIFICATE_NO) = LOWER('" + CertificateNo + "')";
  int nofscripts = cm.queryExecuteCount(query10);

  if (nofscripts != distcounter)
   {
     errorcounter++;
     %>
       <script language="javascript">
        alert("Wrong number of Distinctions. Please Check!");
        history.go(-1);
       </script>
     <%
   }

  String FolioHolder = "";
  String isValid = "";
  String isLocked = "";
  String isActive = "";
  String isConsolidated = "";
  String TypeofTransfer = "P";

  String query2 = "SELECT * FROM SHAREHOLDER_VIEW WHERE lower(FOLIO_NO)=lower('"+SellerFolio+"')";
  cm.queryExecute(query2);
  int sellerexists = cm.queryExecuteCount(query2);

  String SellerName = "";

  if (sellerexists > 0)
  {
   SellerName = cm.getColumnS("NAME");
   isActive = cm.getColumnS("ACTIVE");

   if (String.valueOf(isActive).equals("null"))
     isActive = "";
   if (String.valueOf(SellerName).equals("null"))
     SellerName = "";

   if (isActive.equalsIgnoreCase("F"))
   {
        errorcounter++;
       %>
        <script language="javascript">
         alert("The Seller Folio is not Active!");
         history.go(-1);
        </script>
       <%
   }

   String query3 = "SELECT * FROM CERTIFICATE_VIEW WHERE lower(CERTIFICATE_NO)=lower('"+CertificateNo+"')";
   cm.queryExecute(query3);
   int certificateexists = cm.queryExecuteCount(query3);

   if (certificateexists > 0)
    {
         FolioHolder = cm.getColumnS("HOLDER_FOLIO");
         isValid = cm.getColumnS("VALIDITY");
         isLocked = cm.getColumnS("LOCK");
         isConsolidated = cm.getColumnS("CONSOLIDATED");

         if (String.valueOf(FolioHolder).equals("null"))
         {
           FolioHolder = "";
         }
         if (String.valueOf(isValid).equals("null"))
         {
           isValid = "";
         }
         if (String.valueOf(isLocked).equals("null"))
         {
           isLocked = "";
         }
         if (String.valueOf(isConsolidated).equals("null"))
         {
           isConsolidated = "";
         }

      if (!FolioHolder.equalsIgnoreCase(SellerFolio))
      {
        errorcounter++;
       %>
        <script language="javascript">
         alert("The Certificate does not belong to the Folio!");
         history.go(-1);
        </script>
       <%
      }

      if (isValid.equalsIgnoreCase("F"))
      {
        errorcounter++;
       %>
        <script language="javascript">
         alert("The Certificate is no longer Valid!");
         history.go(-1);
        </script>
       <%
      }

      if (isLocked.equalsIgnoreCase("T"))
      {
        errorcounter++;
       %>
        <script language="javascript">
         alert("The Certificate is locked!");
         history.go(-1);
        </script>
       <%
      }

      if (isConsolidated.equalsIgnoreCase("F"))
      {
        errorcounter++;
       %>
        <script language="javascript">
         alert("The Certificate is not Consolidated!");
         history.go(-1);
        </script>
       <%
      }

      if (dist1 == 0)
      {
        String query6 = "SELECT * FROM SCRIPTS_VIEW WHERE LOWER(CERTIFICATE_NO) = LOWER('" + CertificateNo + "') AND LOWER(DIST_FROM) = LOWER('"+DistFrom1+ "') AND LOWER(DIST_TO) = LOWER('"+DistTo1+ "')";
        int dist1exists = cm.queryExecuteCount(query6);

        if (dist1exists < 1)
        {
        errorcounter++;
         %>
          <script language="javascript">
           alert("Distinctions are not valid. Please Check!");
           history.go(-1);
          </script>
         <%
        }
      }
      if (dist2 == 0)
      {
        String query7 = "SELECT * FROM SCRIPTS_VIEW WHERE LOWER(CERTIFICATE_NO) = LOWER('" + CertificateNo + "') AND LOWER(DIST_FROM) = LOWER('"+DistFrom2+ "') AND LOWER(DIST_TO) = LOWER('"+DistTo2+ "')";
        int dist2exists = cm.queryExecuteCount(query7);

        if (dist2exists < 1)
        {
        errorcounter++;
         %>
          <script language="javascript">
           alert("Distinctions are not valid. Please Check!");
           history.go(-1);
          </script>
         <%
        }
      }
      if (dist3 == 0)
      {
        String query8 = "SELECT * FROM SCRIPTS_VIEW WHERE LOWER(CERTIFICATE_NO) = LOWER('" + CertificateNo + "') AND LOWER(DIST_FROM) = LOWER('"+DistFrom3+ "') AND LOWER(DIST_TO) = LOWER('"+DistTo3+ "')";
        int dist3exists = cm.queryExecuteCount(query8);

        if (dist3exists < 1)
        {
        errorcounter++;
         %>
          <script language="javascript">
           alert("Distinctions are not valid. Please Check!");
           history.go(-1);
          </script>
         <%
        }
      }
      if (dist4 == 0)
      {
        String query9 = "SELECT * FROM SCRIPTS_VIEW WHERE LOWER(CERTIFICATE_NO) = LOWER('" + CertificateNo + "') AND LOWER(DIST_FROM) = LOWER('"+DistFrom4+ "') AND LOWER(DIST_TO) = LOWER('"+DistTo4+ "')";
        int dist4exists = cm.queryExecuteCount(query9);

        if (dist4exists < 1)
        {
        errorcounter++;
         %>
          <script language="javascript">
           alert("Distinctions are not valid. Please Check!");
           history.go(-1);
          </script>
         <%
        }
      }

    }
   else
   {
        errorcounter++;
    %>
     <script language="javascript">
       alert("Certificate Does Not Exist!");
       history.go(-1);
     </script>
    <%
   }
  }
  else
  {
        errorcounter++;
  %>
  <script language="javascript">
    alert("Folio Does Not Exist!");
    history.go(-1);
  </script>
  <%
  }

  int duplicateexists = 0;
  int temp1 = 0;

  String query4m = "SELECT * FROM TRANSFER_T_VIEW WHERE (REC_NO <> "+TransferID+") AND (LOWER(TYPE_OF_TRANSFER)=LOWER('M') AND ((OLD_CERTIFICATE_FROM <= " + CertificateNo + ") AND (OLD_CERTIFICATE_TO >= " + CertificateNo + ")))";
  temp1 = cm.queryExecuteCount(query4m);
  duplicateexists = duplicateexists + temp1;
  String query4a = "SELECT * FROM TRANSFER_T_VIEW WHERE (REC_NO <> "+TransferID+") AND (((lower(OLD_CERTIFICATE_FROM)=lower('"+CertificateNo+"')) OR (lower(XCERTIFICATE1)=lower('"+CertificateNo+"')) OR (lower(XCERTIFICATE2)=lower('"+CertificateNo+"')) OR (lower(XCERTIFICATE3)=lower('"+CertificateNo+"')) OR (lower(XCERTIFICATE4)=lower('"+CertificateNo+"'))))";
  temp1 = cm.queryExecuteCount(query4a);
  duplicateexists = duplicateexists + temp1;

  if (duplicateexists > 0)
  {
        errorcounter++;
    %>
     <script language="javascript">
      alert("The Certificate is already in the Transfer/Split Process!");
      history.go(-1);
     </script>
  <%
  }

  String zerobuffer = "";

  String nsimpletransfer = "call UPDATE_TRANSFER_T(" + TransferID + ", '" + SellerFolio + "', '" + SellerName + "', '" + SellerFolio + "', '" + SellerName + "', '" + TypeofTransfer + "', '" + TotalShares + "','" + CertificateNo + "','" + zerobuffer + "','" + SplitCheck + "','" + AmalCheck + "','" + DistFrom1 + "','" + DistTo1 + "','" + DistFrom2 + "','" + DistTo2 + "','" + DistFrom3 + "','" + DistTo3 + "','" + DistFrom4 + "','" + DistTo4 + "')";

  if (errorcounter == 0)
  {
    boolean b = cm.procedureExecute(nsimpletransfer);
  }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Edited Amalgamated Share Splitting Request')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  alert("The Splitting Request has been Updated");
  location = "<%=request.getContextPath()%>/jsp/Transfer/AcceptTransfer.jsp";
</script>
</body>
</html>
