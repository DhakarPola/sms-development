<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : November 2005
'Page Purpose  : Save Amalgamation Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Amalgamation</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String SellerFolio = String.valueOf(request.getParameter("sfolio"));
  String CertificateNo1 = String.valueOf(request.getParameter("certificateno1"));
  String CertificateNo2 = String.valueOf(request.getParameter("certificateno2"));
  String CertificateNo3 = String.valueOf(request.getParameter("certificateno3"));
  String CertificateNo4 = String.valueOf(request.getParameter("certificateno4"));
  String DistFrom1 = String.valueOf(request.getParameter("distfrom1"));
  String DistTo1 = String.valueOf(request.getParameter("distto1"));
  String DistFrom2 = String.valueOf(request.getParameter("distfrom2"));
  String DistTo2 = String.valueOf(request.getParameter("distto2"));
  String DistFrom3 = String.valueOf(request.getParameter("distfrom3"));
  String DistTo3 = String.valueOf(request.getParameter("distto3"));
  String DistFrom4 = String.valueOf(request.getParameter("distfrom4"));
  String DistTo4 = String.valueOf(request.getParameter("distto4"));
  String TotalShares = String.valueOf(request.getParameter("tnofshares"));

  String SplitCheck = "F";
  String AmalCheck = "T";

  int distcounter = 0;
  int errorcounter = 0;

  int dist1 = 0;
  int dist2 = 0;
  int dist3 = 0;
  int dist4 = 0;

  if ((String.valueOf(DistFrom1).equals("null")) || (DistFrom1.equalsIgnoreCase("")))
  {
    distcounter++;
    dist1++;
  }
  if ((String.valueOf(DistFrom2).equals("null")) || (DistFrom2.equalsIgnoreCase("")))
  {
    distcounter++;
    dist2++;
  }
  if ((String.valueOf(DistFrom3).equals("null")) || (DistFrom3.equalsIgnoreCase("")))
  {
    dist3++;
    distcounter++;
  }
  if ((String.valueOf(DistFrom4).equals("null")) || (DistFrom4.equalsIgnoreCase("")))
  {
    dist4++;
    distcounter++;
  }

  distcounter = 4 - distcounter;

  SellerFolio=cm.replace(SellerFolio,"'","''");
  CertificateNo1=cm.replace(CertificateNo1,"'","''");
  CertificateNo2=cm.replace(CertificateNo2,"'","''");
  CertificateNo3=cm.replace(CertificateNo3,"'","''");
  CertificateNo4=cm.replace(CertificateNo4,"'","''");
  DistFrom1=cm.replace(DistFrom1,"'","''");
  DistTo1=cm.replace(DistTo1,"'","''");
  DistFrom2=cm.replace(DistFrom2,"'","''");
  DistTo2=cm.replace(DistTo2,"'","''");
  DistFrom3=cm.replace(DistFrom3,"'","''");
  DistTo3=cm.replace(DistTo3,"'","''");
  DistFrom4=cm.replace(DistFrom4,"'","''");
  DistTo4=cm.replace(DistTo4,"'","''");
  TotalShares=cm.replace(TotalShares,"'","''");

  if (String.valueOf(CertificateNo1).equals("null"))
   {
     CertificateNo1 = "";
   }
  if (String.valueOf(CertificateNo2).equals("null"))
   {
     CertificateNo2 = "";
   }
  if (String.valueOf(CertificateNo3).equals("null"))
   {
     CertificateNo3 = "";
   }
  if (String.valueOf(CertificateNo4).equals("null"))
   {
     CertificateNo4 = "";
   }

  if (CertificateNo1.equalsIgnoreCase(""))
  {
    CertificateNo1 = "0";
  }
  if (CertificateNo2.equalsIgnoreCase(""))
  {
    CertificateNo2 = "0";
  }
  if (CertificateNo3.equalsIgnoreCase(""))
  {
    CertificateNo3 = "0";
  }
  if (CertificateNo4.equalsIgnoreCase(""))
  {
    CertificateNo4 = "0";
  }

  String[] cerarr = new String[4];
  cerarr[0] = CertificateNo1;
  cerarr[1] = CertificateNo2;
  cerarr[2] = CertificateNo3;
  cerarr[3] = CertificateNo4;

  String[] diststartarr = new String[4];
  diststartarr[0] = DistFrom1;
  diststartarr[1] = DistFrom2;
  diststartarr[2] = DistFrom3;
  diststartarr[3] = DistFrom4;

  String[] distendarr = new String[4];
  distendarr[0] = DistTo1;
  distendarr[1] = DistTo2;
  distendarr[2] = DistTo3;
  distendarr[3] = DistTo4;

  for(int j=0;j<distcounter;j++)
  {
  String query10 = "SELECT * FROM SCRIPTS_VIEW WHERE LOWER(CERTIFICATE_NO) = LOWER('" + cerarr[j] + "')";
  int nofscripts = cm.queryExecuteCount(query10);

  if (nofscripts > 1)
   {
     errorcounter++;
     %>
       <script language="javascript">
        alert("One or more Certificates are already Amalgamated");
        history.go(-1);
       </script>
     <%
   }
  else if (nofscripts == 1)
   {
      String query6 = "SELECT * FROM SCRIPTS_VIEW WHERE LOWER(CERTIFICATE_NO) = LOWER('" + cerarr[j] + "') AND LOWER(DIST_FROM) = LOWER('"+diststartarr[j]+ "') AND LOWER(DIST_TO) = LOWER('"+distendarr[j]+ "')";
      int distexists = cm.queryExecuteCount(query6);

        if (distexists < 1)
        {
        errorcounter++;
         %>
          <script language="javascript">
           alert("One or more Distinctions are not valid. Please Check!");
           history.go(-1);
          </script>
         <%
        }
    }
  }

  String FolioHolder = "";
  String isValid = "";
  String isLocked = "";
  String isActive = "";
  String TypeofTransfer = "G";

  String query2 = "SELECT * FROM SHAREHOLDER_VIEW WHERE lower(FOLIO_NO)=lower('"+SellerFolio+"')";
  cm.queryExecute(query2);
  int sellerexists = cm.queryExecuteCount(query2);

  String SellerName = "";

  if (sellerexists > 0)
  {
   SellerName = cm.getColumnS("NAME");
   isActive = cm.getColumnS("ACTIVE");

   if (String.valueOf(isActive).equals("null"))
     isActive = "";
   if (String.valueOf(SellerName).equals("null"))
     SellerName = "";

   if (isActive.equalsIgnoreCase("F"))
   {
        errorcounter++;
       %>
        <script language="javascript">
         alert("The Folio is not Active!");
         history.go(-1);
        </script>
       <%
   }

   for (int i=0;i<distcounter;i++)
   {
   String query3 = "SELECT * FROM CERTIFICATE_VIEW WHERE lower(CERTIFICATE_NO)=lower('"+cerarr[i]+"')";
   cm.queryExecute(query3);
   int certificateexists = cm.queryExecuteCount(query3);

   if (certificateexists > 0)
    {
         FolioHolder = cm.getColumnS("HOLDER_FOLIO");
         isValid = cm.getColumnS("VALIDITY");
         isLocked = cm.getColumnS("LOCK");

         if (String.valueOf(FolioHolder).equals("null"))
         {
           FolioHolder = "";
         }
         if (String.valueOf(isValid).equals("null"))
         {
           isValid = "";
         }
         if (String.valueOf(isLocked).equals("null"))
         {
           isLocked = "";
         }

      if (!FolioHolder.equalsIgnoreCase(SellerFolio))
      {
        errorcounter++;
       %>
        <script language="javascript">
         alert("One or more Certificates do not belong to the Folio!");
         history.go(-1);
        </script>
       <%
      }

      if (isValid.equalsIgnoreCase("F"))
      {
        errorcounter++;
       %>
        <script language="javascript">
         alert("One or more Certificates are no longer Valid!");
         history.go(-1);
        </script>
       <%
      }

      if (isLocked.equalsIgnoreCase("T"))
      {
        errorcounter++;
       %>
        <script language="javascript">
         alert("One or more Certificates are locked!");
         history.go(-1);
        </script>
       <%
      }
    }
   else
   {
        errorcounter++;
    %>
     <script language="javascript">
       alert("One or more Certificates Do Not Exist!");
       history.go(-1);
     </script>
    <%
   }
   }
  }
  else
  {
        errorcounter++;
  %>
  <script language="javascript">
    alert("Folio Does Not Exist!");
    history.go(-1);
  </script>
  <%
  }

  int duplicateexists = 0;
  int temp1 = 0;

  for (int k=0;k<distcounter;k++)
  {
    String query4m = "SELECT * FROM TRANSFER_T_VIEW WHERE LOWER(TYPE_OF_TRANSFER)=LOWER('M') AND ((OLD_CERTIFICATE_FROM <= " + cerarr[k] + ") AND (OLD_CERTIFICATE_TO >= " + cerarr[k] + "))";
    temp1 = cm.queryExecuteCount(query4m);
    duplicateexists = duplicateexists + temp1;
    String query4a = "SELECT * FROM TRANSFER_T_VIEW WHERE ((lower(OLD_CERTIFICATE_FROM)=lower('"+cerarr[k]+"')) OR (lower(XCERTIFICATE1)=lower('"+cerarr[k]+"')) OR (lower(XCERTIFICATE2)=lower('"+cerarr[k]+"')) OR (lower(XCERTIFICATE3)=lower('"+cerarr[k]+"')) OR (lower(XCERTIFICATE4)=lower('"+cerarr[k]+"')))";
    temp1 = cm.queryExecuteCount(query4a);
    duplicateexists = duplicateexists + temp1;
  }

  if (duplicateexists > 0)
  {
        errorcounter++;
    %>
     <script language="javascript">
      alert("One or more Certificates are already in the Transfer Process!");
      history.go(-1);
     </script>
  <%
  }

  String namalgamation = "call ADD_XTRANSFER_T('" + SellerFolio + "', '" + SellerName + "', '" + SellerFolio + "', '" + SellerName + "', '" + TypeofTransfer + "', '" + TotalShares + "','','" + SplitCheck + "','" + AmalCheck + "','" + DistFrom1 + "','" + DistTo1 + "','" + DistFrom2 + "','" + DistTo2 + "','" + DistFrom3 + "','" + DistTo3 + "','" + DistFrom4 + "','" + DistTo4 + "','" + CertificateNo1 + "','" + CertificateNo2 + "','" + CertificateNo3 + "','" + CertificateNo4 + "')";

  if (errorcounter == 0)
  {
    boolean b = cm.procedureExecute(namalgamation);
  }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added Share Amalgamation Request')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  alert("The Amalgamation Request has been Entered");
  location = "<%=request.getContextPath()%>/jsp/Transfer/Amalgamation.jsp";
</script>
</body>
</html>
