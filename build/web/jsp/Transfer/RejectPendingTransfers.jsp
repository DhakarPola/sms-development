<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : November 2005
'Page Purpose  : Deletes a Temporary Transfer Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Delete Temporary Transfer</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

    int countrows = 0;
    String CurrentCheckVal = "";
    String DelStr = "";

    String query1 = "SELECT * FROM TRANSFER_T_VIEW";
    countrows = cm.queryExecuteCount(query1);

    if (countrows == 0)
    {
     %>
       <script language="javascript">
         alert("No Records Found!");
         location = "<%=request.getContextPath()%>/jsp/Transfer/AcceptTransfer.jsp";
       </script>
     <%
    }
    else if (countrows > 0)
    {
      int[] idarr = new int[countrows];
      int j = 0;

      for(int i=1;i<countrows+1;i++)
      {
        String CurrentBox = "rejectbox" + String.valueOf(i);
        CurrentCheckVal = String.valueOf(request.getParameter(CurrentBox));

        if (!String.valueOf(CurrentCheckVal).equals("null"))
        {
          idarr[j] = Integer.parseInt(CurrentCheckVal);
          j++;
        }
      }

      if (j == 0)
      {
      %>
       <script language="javascript">
         alert("No Records Selected!");
         location = "<%=request.getContextPath()%>/jsp/Transfer/AcceptTransfer.jsp";
       </script>
      <%
      }
      else
      {
       for (int k=0;k<j;k++)
       {
         DelStr = "call DELETE_TRANSFER_T('" + idarr[k] + "')";
         boolean b = cm.procedureExecute(DelStr);
       }
      }
    }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Rejected Share Transfer Request(s)')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Transfer/AcceptTransfer.jsp";
</script>
</body>
</html>
