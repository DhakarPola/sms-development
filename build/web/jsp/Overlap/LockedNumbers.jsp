<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Creation Date : December 2005
'Page Purpose  : Generates New Distinction Number
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="util" class="batbsms.Utility"/>
<%@ page language="java" import="oracle.jdbc.driver.*,java.io.*,java.util.*,java.text.*" %>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>
<html>
<script type="text/javascript">
<!--
function goNextPrevious(pstart,pend,certificateno,UserName,no_of_shares,foliono,start_dist,status)
{

  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Overlap/LockedNumbers.jsp?StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;
  thisurl = thisurl + "&certificateno=" + certificateno;
  thisurl = thisurl + "&UserName=" + UserName;
  thisurl = thisurl + "&no_of_shares=" + no_of_shares;
  thisurl = thisurl + "&foliono=" + foliono;
  thisurl = thisurl + "&start_dist=" + start_dist;
  thisurl = thisurl + "&status=" + status;
  location = thisurl;
}


//-->
</script>
<style type="text/css">
<!--

.style7 {
	color: black;
	font-size: 11px;
}

body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>
<body bgcolor="#E8F3FD">
<%
         boolean isConnect = cm.connect();
         boolean isConnect1 = cm1.connect();
         if((isConnect==false) || (isConnect1==false))
         {
          %>
          <jsp:forward page="ErrorMsg.jsp" >
            <jsp:param name="ErrorTitle" value="Connection Failure" />
            <jsp:param name="ErrorHeading" value="Connection Problem" />
            <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
          </jsp:forward>
          <%
          }
          Date now = new Date();
          DateFormat df = DateFormat.getDateInstance();
          String current_date = df.format(now);
          current_date = util.changeDateFormat(current_date,"MM/dd/yyyy h:m:s","dd/MM/yyyy");

          int iend_dist=0;
          int istart =0;
          int iend=0;
          int total=0;
          int count_cert=0;

          String certificateno ="";
          String queryViewLockedDN ="";
          String foliono="";
          String start_dist = "";
          String end_dist = "";
          String total_shares = "";
          String issuedate="";
          String issueby="";
          String status="";
          String queryAdd="";
          String checkExistingQuery="";
          String queryDeleteScripts="";
          String queryAddScripts="";
          int olddistinctionfrom = 0;
          int olddistinctionto = 0;

          certificateno = request.getParameter("certificateno");
          status = request.getParameter("status");
          issueby= String.valueOf(session.getAttribute("UserName"));
          total_shares = request.getParameter("no_of_shares");

          foliono = request.getParameter("foliono");
          start_dist = request.getParameter("start_dist");


//         NEXT/PREV section
          String SStartingValue = String.valueOf(request.getParameter("StartValue"));
          String SEndingValue = String.valueOf(request.getParameter("EndValue"));

          int StartingValue = Integer.parseInt(SStartingValue);
          int EndingValue = Integer.parseInt(SEndingValue);
          int Chunk = 15;

          EndingValue = StartingValue + Chunk - 1;

          int pStartingValue = StartingValue - Chunk;
          int pEndingValue = EndingValue - Chunk;
          int nStartingValue = StartingValue + Chunk;
          int nEndingValue = EndingValue + Chunk;
          if (pStartingValue < 1)
          {
            pStartingValue = 1;
          }
          if (pEndingValue < Chunk)
          {
            pEndingValue = Chunk;
          }


          if(status.equals("lock"))
          {
              int end_dn = Integer.parseInt(start_dist) + Integer.parseInt(total_shares)-1;

              checkExistingQuery = "SELECT * FROM LOCKEDFOLIOFROMONETOTHREE WHERE DE_DE_END_DIST_NUMB= '"+String.valueOf(end_dn) +"'";
              count_cert = cm.queryExecuteCount(checkExistingQuery);

              if(count_cert > 0)
              {
                %>
                <script type="text/javascript">
                  alert('Sorry, values already assigned!');
                </script>
               <%
              }
              else
              {
                istart = Integer.parseInt(start_dist); // new start distinction number
                iend = end_dn;      // new end distinction number

                start_dist = String.valueOf(istart);
                end_dist = String.valueOf(iend);

                System.out.println(current_date);

                queryAdd = "CALL ADD_LOCKEDFOLIOFROMONETOTHREE('"+ foliono+"','"+certificateno+"','" + start_dist+"','"+end_dist + "','"+issueby+"','"+current_date+"')";
                cm.procedureExecute(queryAdd);

                String query1d = "SELECT * FROM SCRIPTS_VIEW WHERE CERTIFICATE_NO = '" + certificateno + "'";
                cm1.queryExecute(query1d);

                while(cm1.toNext())
                {
                  olddistinctionfrom = cm1.getColumnI("DIST_FROM");
                  olddistinctionto = cm1.getColumnI("DIST_TO");

                  queryAddScripts = "CALL ADD_OLD_DISTINCTIONS('"+ olddistinctionfrom + "','" +olddistinctionto +"','"+ certificateno +"')";
                  cm.procedureExecute(queryAddScripts) ;
                }

                queryDeleteScripts ="CALL DELETE_SCRIPTS('"+certificateno+"')";
                cm.procedureExecute(queryDeleteScripts);

                queryAddScripts = "CALL ADD_SCRIPTS('"+ start_dist + "','" +end_dist +"','"+ certificateno +"',null)";
                cm.procedureExecute(queryAddScripts) ;
              }
          }
          status="none";
          queryViewLockedDN = " SELECT * FROM (SELECT div.*, rownum rnum FROM ";
          queryViewLockedDN += " (SELECT GH3_DE_REG_FOLIO_NUMB, GH3_DE_CERTIFICATE_NUMB,DE_DE_START_DIST_NUMB,DE_DE_END_DIST_NUMB,(DE_DE_END_DIST_NUMB - DE_DE_START_DIST_NUMB +1) AS  NO_OF_SHARES, ";
          queryViewLockedDN +=" ISSUEDATE,ISSUEBY ";
          queryViewLockedDN += "FROM LOCKEDFOLIOFROMONETOTHREE ORDER BY DE_DE_START_DIST_NUMB DESC) div";
          queryViewLockedDN += "  WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;

          cm.queryExecute(queryViewLockedDN);

%>
      <table width="100%"  border="1" cellspacing="0" cellpadding="0" bordercolor="#0044B0" >

       <%
            while(cm.toNext())
            {

               foliono= cm.getColumnS("GH3_DE_REG_FOLIO_NUMB");
               certificateno= cm.getColumnS("GH3_DE_CERTIFICATE_NUMB");
               start_dist= cm.getColumnS("DE_DE_START_DIST_NUMB");
               end_dist= cm.getColumnS("DE_DE_END_DIST_NUMB");
               total_shares= cm.getColumnS("NO_OF_SHARES");
               issuedate= cm.getColumnDT("ISSUEDATE");
               issueby= cm.getColumnS("ISSUEBY");

               issuedate = util.changeDateFormat(issuedate,"MM/dd/yyyy h:m:s","dd/MM/yyyy");
        %>
        <tr align="center">
          <td width="15%" class="style7"><%=start_dist%></td>
          <td width="15%" class="style7"><%=end_dist%></td>
          <td width="10%" class="style7"><%=total_shares%></td>
          <td width="15%" class="style7"><%=certificateno%></td>
          <td width="15%" class="style7"><%=foliono%></td>
          <td width="15%" class="style7"><%=issueby%></td>
          <td width="15%" class="style7"><%=issuedate%></td>
        </tr>
      <%
           }

         if (isConnect)
         {
           cm.takeDown();
           cm1.takeDown();
         }

      %>
       <!-- Hidden fields for next/previous -->
       <input type="hidden" name="svalue" value="<%=StartingValue%>">
       <input type="hidden" name="evalue" value="<%=EndingValue%>">

    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" >
      <tr>
	<td width="50%" align="center"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goNextPrevious('<%=pStartingValue%>','<%=pEndingValue%>','<%=certificateno%>','<%=issueby%>','<%=total_shares%>','<%=foliono%>','<%=start_dist%>','<%=status%>');"></td>
	<td width="50%" align="center"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNextPrevious('<%=nStartingValue%>','<%=nEndingValue%>','<%=certificateno%>','<%=issueby%>','<%=total_shares%>','<%=foliono%>','<%=start_dist%>','<%=status%>');"></td>
      </tr>
    </table>
</body>
</html>
