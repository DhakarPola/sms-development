<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Enhanced By   : Renad Hakim
'Creation Date : December 2005
'Page Purpose  : Register by Folio/Certificate
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="util" class="batbsms.Utility"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--

span.visible {visibility:visible}
span.invisible {visibility:hidden}
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.colred {
	font-family: Arial, Helvetica, sans-serif;
	color: red;
  	font-size:10;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
      .Style14 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: 14px;
      }
      .style11 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; }
      .style16 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; }
.style1
{
  color: #0A2769;
  font-size:10;
}
.style2
{
  color:#FFFFFF;
  font-weight:bold;
  font-size:10;
}

.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Demat Overlap Solution</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--

function goNextPrevious(pstart,pend,tfoliono)
{

  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Overlap/RegbyFolio.jsp?StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;
  thisurl = thisurl + "&radiobutton=folio";
  thisurl = thisurl + "&foliono="+tfoliono;
  location = thisurl;
}

function radio_button_checker()
{
    var radio_choice = false;

    for (counter = 0; counter < document.forms[0].rdocertificate.length; counter++)
    {
      if (document.forms[0].rdocertificate[counter].checked)
        return document.forms[0].rdocertificate[counter].value
    }

}


function openNewDistinctionWindow()
{
        var windowname="";
        var certificateno="";

       if( document.forms[0].rdocertificate != null)
       {
         if(document.forms[0].rdocertificate.value==null)
         certificateno = radio_button_checker();
         else
         certificateno=document.forms[0].rdocertificate.value;

         if(certificateno == "0")
         {
           alert("No Overlap Found!");
           return;
         }

         windowname = "NewDistinctionNo.jsp?certificateno=" +certificateno ;

         var win = window.open(windowname,"","'status=yes,menubar=no,scrollbars=yes,resizable=yes,toolbar=yes'");
         win.focus();
         win.moveTo( 0, 0 );
         win.resizeTo( screen.availWidth, screen.availHeight );

         document.all.dbutton1.style.display = 'none';
         document.all.dbutton2.style.display = '';

         return win;
       }
       else
       {
         alert("Sorry, no certificate selected");
       }

}
function makefolioreadonly()
{
    document.all.foliospan1.style.display = 'none'
    document.all.foliospan2.style.display = 'none'
    document.all.certifatespan1.style.display = ''
    document.all.certifatespan2.style.display = ''
}
function makecertificatereadonly()
{
    document.all.certifatespan1.style.display = 'none'
    document.all.certifatespan2.style.display = 'none'
    document.all.foliospan1.style.display = ''
    document.all.foliospan2.style.display = ''
}
//-->
</script>
<style type="text/css">
<!--
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body  TEXT="000000" BGCOLOR="FFFFFF">
<%
         boolean isConnect = cm.connect();
         if(isConnect==false)
         {
          %>
          <jsp:forward page="ErrorMsg.jsp" >
            <jsp:param name="ErrorTitle" value="Connection Failure" />
            <jsp:param name="ErrorHeading" value="Connection Problem" />
            <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
          </jsp:forward>
          <%
          }
         boolean isConnect1 = cm1.connect();
         if(isConnect1==false)
         {
          %>
          <jsp:forward page="ErrorMsg.jsp" >
            <jsp:param name="ErrorTitle" value="Connection Failure" />
            <jsp:param name="ErrorHeading" value="Connection Problem" />
            <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
          </jsp:forward>
          <%
          }
          int TOTAL_SHARES=0;
          int MAX_RECORDS=3;
          int COUNT=0;
          int certificate_state=0;
          String sms_dist_from = "";
          String sms_dist_to = "";
          String no_of_shares = "";
          String de_start_dist_numb = "";
          String de_end_dist_numb = "";
          String no_of_demat_share = "";
          String shareholder_name = "";
          String total_share = "";
          String dest ="";
          String query1="";
          String querySearchFolio="";
          String folio_no="";
          String style="style2";
          String temp_cert_no="";
          String tempFolioNo="";


        folio_no = request.getParameter("foliono");


         String certificateno =request.getParameter("certificateno");
         String last_certificate_no = request.getParameter("last_certificate_no");
         String first_certificate_no = request.getParameter("first_certificate_no");
         String radiobutton_status = request.getParameter("radiobutton");


          //Used for NEXT/PREVIOUS

          String SStartingValue = String.valueOf(request.getParameter("StartValue"));
          String SEndingValue = String.valueOf(request.getParameter("EndValue"));


          if(SStartingValue == null || SStartingValue.equals("") || SStartingValue.equals("null")) SStartingValue="1";
          if(SEndingValue == null || SEndingValue.equals("") || SEndingValue.equals("null")) SEndingValue="0";



          int StartingValue = Integer.parseInt(SStartingValue);
          int EndingValue = Integer.parseInt(SEndingValue);
          int Chunk = 5;
          EndingValue = StartingValue + Chunk - 1;
          int pStartingValue = StartingValue - Chunk;
          int pEndingValue = EndingValue - Chunk;
          int nStartingValue = StartingValue + Chunk;
          int nEndingValue = EndingValue + Chunk;

          if (pStartingValue < 1)
          {
            pStartingValue = 1;
          }
          if (pEndingValue < Chunk)
          {
            pEndingValue = Chunk;
          }

         tempFolioNo = folio_no;
         if(folio_no == null)
         {
           tempFolioNo="";
           folio_no = "0";
         }
         if(certificateno == null)
           certificateno="";
         if(radiobutton_status == null)
           radiobutton_status ="";


          if(radiobutton_status.equals("folio"))
          {

              query1 += "SELECT * FROM (SELECT div.*, rownum rnum FROM ";
              query1 += "(SELECT  c.CERTIFICATE_NO,s.DIST_FROM,s.DIST_TO,(s.DIST_TO - s.DIST_FROM +1) AS  NO_OF_SHARES ";
              query1 += "FROM SCRIPTS s,CERTIFICATE c ";
              query1 += "WHERE (c.CERTIFICATE_NO = s.CERTIFICATE_NO AND c.HOLDER_FOLIO='"+ folio_no+"' AND c.VALIDITY='T' ";
              query1 += ")) div";
              query1 +=  " WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;

              querySearchFolio = "SELECT NAME,TOTAL_SHARE FROM SHAREHOLDER_VIEW WHERE FOLIO_NO = '"+folio_no+"'";
          }
          else
          if(radiobutton_status.equals("certificate"))
          {
              query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM ";
              query1 += "(SELECT s.CERTIFICATE_NO,s.DIST_FROM,s.DIST_TO,(s.DIST_TO - s.DIST_FROM +1) AS  NO_OF_SHARES ";
              query1 += "FROM SCRIPTS s,CERTIFICATE c ";
              query1 += "WHERE (c.CERTIFICATE_NO = s.CERTIFICATE_NO AND c.CERTIFICATE_NO='"+certificateno+"' AND c.VALIDITY='T') ";
              query1 += ") div";
              query1 += " WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;

              querySearchFolio = "SELECT NAME,TOTAL_SHARE FROM SHAREHOLDER_VIEW ";
              querySearchFolio +="WHERE FOLIO_NO IN ";
              querySearchFolio +="(SELECT HOLDER_FOLIO FROM CERTIFICATE ";
              querySearchFolio +="WHERE CERTIFICATE_NO = '"+certificateno +"')";
          }


          if(!radiobutton_status.equals(""))
          {
            cm.queryExecute(querySearchFolio);

            while(cm.toNext())
            {
              shareholder_name = cm.getColumnS("NAME");
              total_share = cm.getColumnS("TOTAL_SHARE");
            }


          cm.queryExecute(query1);
          //System.out.println(query1);
          COUNT=0;
          while(cm.toNext())
          {
            COUNT++;
          }

          cm.queryExecute(query1);

        }
%>
<span class="style7">
<form action="RegbyFolio.jsp" method="get">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Demat Overlap Solution</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td weight="100%" bgcolor="#E8F3FD"  ><center>
	  <br>
	  <table width="100%"  border="0" cellpadding="5">
	  <tr align="center" bgcolor="#E8F3FD">

		<%
		  String certRdoStat="";
		  String folioRdoStat="";
		  if(radiobutton_status.equals("") || radiobutton_status.equals("folio"))
			folioRdoStat = "checked";
		  else
			certRdoStat ="checked";
		%>
		  <td width="4%" align="center" ><input name="radiobutton" type="radio" onclick="makecertificatereadonly()"  value="folio" <%=folioRdoStat%> ></td>
		  <td width="12%" align="left" class="style8"><b>Enter Folio No.</b></td>
		  <td width="1%"><span  id="foliospan1">:</span></td>
		  <td width="33%" align="left"><span  id="foliospan2"><input type="text" name="foliono" class="SL67TextField" maxlength="9"  size="9" ></span></td>

		  <td width="4%" align="center"><input name="radiobutton" type="radio" value="certificate"  onClick="makefolioreadonly()" <%=certRdoStat%> ></td>
		  <td width="17%" align="left" class="style8"><b>Enter Certificate No.</b></td>
		  <td width="1%" align="left"><span id="certifatespan1" style="display:none">:</span></td>
		  <td width="28%" align="left"><span id="certifatespan2" style="display:none">
			<input type="text" name="certificateno" class="SL67TextField" maxlength="9"  size="9" value="<%=certificateno%>" ></span></td>
	<%
	   if(radiobutton_status.equals("certificate"))
			{
			  %>
			  <script type="text/javascript">
				makefolioreadonly()
			  </script>
			  <%
			}
	%>
	  </tr>
	  <tr><td bgcolor="#E8F3FD" colspan="10" align="center">&nbsp;<br>
		<img name="B5" src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="document.forms[0].submit()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnSearchR.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnSearch.gif'">
		<br></td></tr>
	  </table>
  	</td>
 </tr>
 <tr><td bgcolor="#0044B0" class="style7">Shareholder Information</td></tr>
 <tr bgcolor="#E8F3FD">
 	<td>
	  <table width="100%"  border="0" cellpadding="5">
		  <tr bgcolor="#E8F3FD">
			<th scope="row" width="35%"><div align="left" class="style8">&nbsp;&nbsp;Folio No.</div></th>
			<td width="1%">:</td>
			<td width="64%"><div align="left" class="style8">
			  <input type="text" name="textfield1aa" class="SL2TextField" onfocus="this.blur()" value="<%=tempFolioNo%>">
			</div></td>
		  </tr>
		  <tr bgcolor="#E8F3FD">
			<th scope="row" width="35%"><div align="left" class="style8">&nbsp;&nbsp;Shareholder Name</div></th>
			<td width="1%">:</td>
			<td width="64%"><div align="left" class="style8">
			  <input type="text" name="textfield1" class="SL2TextField" readonly="true" value="<%=shareholder_name%>"  align="middle">
			</div></td>
		  </tr>
		  <tr bgcolor="#E8F3FD">
			<th scope="row" width="35%"><div align="left" class="style8">&nbsp;&nbsp;Total Shares</div></th>
			<td width="1%">:</td>
			<td width="64%"><div align="left" class="style8">
			  <input type="text" name="textfield2" class="SL2TextField" readonly="true" value="<%=total_share%>" align="middle">
			</div></td>
		  </tr>
		  <tr bgcolor="#E8F3FD">
			<th scope="row" width="35%"><div align="left" class="style8">&nbsp;&nbsp;Total Shares by Certificate Found</div></th>
			<td width="1%">:</td>
			<%
			  if(!radiobutton_status.equals(""))
			  {
				 while(cm.toNext())
				{
				   no_of_shares = cm.getColumnS("NO_OF_SHARES");
				  if(no_of_shares == null) no_of_shares="0";
				  TOTAL_SHARES += Integer.parseInt(no_of_shares);
				}
			  }
			%>
			<td width="64%"><div align="left" class="style8">
			 <input type="text" name="textfield" class="SL2TextField" readonly="true" value="<%=String.valueOf(TOTAL_SHARES)%>" align="middle">
			</div></td>
		  </tr>
		</table>
 	</td>
 </tr>
  <tr><td bgcolor="#0044B0" class="style7">Assign New Distinction No.</td></tr>
  <tr bgcolor="#E8F3FD">
    <td>
      <table width="100%"  border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td bgcolor="#E8F3FD" colspan="4" align="center">
              <br>

              <SPAN id="dbutton1">
                <img name="B4" src="<%=request.getContextPath()%>/images/ReqNewDist.gif"  onclick="openNewDistinctionWindow()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/ReqNewDistR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/ReqNewDist.gif'">
              </span>
              <SPAN id="dbutton2" name="DivInfospan" style="display:none">
                <img name="B5" src="<%=request.getContextPath()%>/images/ReqNewDist.gif"  onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/ReqNewDistR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/ReqNewDist.gif'">
              </span>
            </td>
	    </tr>
	  <tr>
	    <td bgcolor="#E8F3FD" colspan="4">&nbsp;</td>
	    </tr>
	  <tr><td bgcolor="#E8F3FD" colspan="4"><table width="316" border="0">
        <tr>
          <td width="16%" align="left"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goNextPrevious('<%=pStartingValue%>','<%=pEndingValue%>','<%=folio_no%>')"></td>
          <td width="3%">&nbsp;</td>
          <td width="81%" align="left"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNextPrevious('<%=nStartingValue%>','<%=nEndingValue%>','<%=folio_no%>')"></td>
        </tr>
      </table>
	  </td>
	 </tr>
    <!--/table>
	</td>
  </tr-->
  <tr bgcolor="#E8F3FD">
   <td colspan="1">
   <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse"  cellspacing="2">
    <tr bgcolor="#C8E9FD">
      <td align="center" width="25%" bgcolor="#0044B0" class="style7">Certificate No.</td>
      <td align="center" width="25%" bgcolor="#0044B0" class="style7">Dist. From</td>
      <td align="center" width="25%" bgcolor="#0044B0" class="style7">Dist. To</td>
      <td align="center" width="25%" bgcolor="#0044B0" class="style7">Shares</td>
    </tr>
   </table>
   <table width="100%" border="1" style="border-collapse: collapse" bordercolor="#0044B0" cellpadding="5">
	  <tr bgcolor="#E8F3FD">
	  <%

			  if(!radiobutton_status.equals(""))
			  {

				cm.queryExecute(query1);
				int COUNT_SELECTED=0;
				certificate_state =0;

				while(cm.toNext())
				{
				  certificate_state++;
				  certificateno = cm.getColumnS("CERTIFICATE_NO");
				  if(certificate_state==1) first_certificate_no =certificateno;

				  if(certificate_state==COUNT)
				  {
					last_certificate_no=certificateno;
				  }

				  sms_dist_from = cm.getColumnS("DIST_FROM");
				  sms_dist_to = cm.getColumnS("DIST_TO");
				  no_of_shares = cm.getColumnS("NO_OF_SHARES");

				  COUNT_SELECTED++;

				  String findOverlapQuery = "SELECT SHR_NAME,BOID,PF_BDATE,START_DIST_NUMB,END_DIST_NUMB,DRF_NUMB";
				  findOverlapQuery  += " FROM BODEMATCONFIRMATION ";
				  findOverlapQuery  += " WHERE CERTIFICATE_STAT='ACCEPTED' AND ((START_DIST_NUMB <= '" + sms_dist_from + "' AND END_DIST_NUMB >= '"+sms_dist_from+"') OR (START_DIST_NUMB >= '"+ sms_dist_from + "' AND START_DIST_NUMB <= '"+ sms_dist_to + "' ))";
				  int countNoOfOverlaps = cm1.queryExecuteCount(findOverlapQuery);
				  temp_cert_no = certificateno;
				  if(countNoOfOverlaps <= 0) temp_cert_no="0";

				  %>
				  <!--tr-->
				  <%
				  if(COUNT_SELECTED==1)
				  {

					%>
				 	<td class="style13" width="25%"><div align="center" class="style12">
					  <div align="center"><input type="radio" name="rdocertificate" value="<%=temp_cert_no%>" checked="checked"/>&nbsp;<%=certificateno%></div>
					  </div></td>
				  <%
				  }
				  else
				  if(COUNT_SELECTED==COUNT)
				  {
					if(COUNT==1)
					{
					  %>
						 <td class="style13" width="25%"><div align="center" class="style12">
						  <div align="center"><input type="radio" name="rdocertificate" value="<%=temp_cert_no%>" checked="checked"/>&nbsp;<%=certificateno%></div>
						  </div></td>
					  <%
					}
					else
				   %>
				   <td class="style13" width="25%"><div align="center" class="style12">
					<div align="center"><input type="radio" name="rdocertificate" value="<%=temp_cert_no%>"/>&nbsp;<%=certificateno%></div>
					</div></td>
				   <input type="hidden" name="hfoliono" value="<%=folio_no%>"/>
				   <input type="hidden" name="svalue" value="<%=StartingValue%>">
				   <input type="hidden" name="evalue" value="<%=EndingValue%>">

				  <%
				  }
				  else
				{
			  %>
			  <td class="style13" width="25%"><div align="center" class="style12">
			   <div align="center"><input type="radio" name="rdocertificate" value="<%=temp_cert_no%>"/>&nbsp;<%=certificateno%></div>
			   </div>
			  </td>
			  <%
			  }
			  %>
		  <td class="style13" width="25%"><div align="center" class="style12">
		   <div align="center"><%=sms_dist_from%></div>
		   </div>
		  </td>
		  <td class="style13" width="25%"><div align="center" class="style12">
		   <div align="center"><%=sms_dist_to%></div>
		   </div>
		  </td>
		  <td class="style13" width="25%"><div align="center" class="style12">
		   <div align="center"><%=no_of_shares%></div>
		   </div>
		  </td>
	 </tr>
	 <tr>
	  <td>&nbsp;</td>
	  <td colspan="3">
	  		<table  width="100%"  border="1" cellspacing="0" cellpadding="0" bordercolor="#0044B0" style="border-collapse: collapse">
				  <%

				  //***************************************** Updated January 17, 2006***************



				  String tempCertificateno="";
				  String boid="";
				  String pf_date="";
				  String overlapName="";
				  String overlapStartDN="";
				  String overlapEndDN="";
				  String overlapDate="";
				  String DRF="";
				  style="style2";
				  cm1.queryExecute(findOverlapQuery);
				  int count_overlap=0;
				  while(cm1.toNext())
				  {
					boid = cm1.getColumnS("BOID");
					overlapDate = cm1.getColumnS("PF_BDATE");
					overlapName = cm1.getColumnS("SHR_NAME");
					overlapStartDN = cm1.getColumnS("START_DIST_NUMB");
					overlapEndDN = cm1.getColumnS("END_DIST_NUMB");
					overlapDate = cm1.getColumnS("PF_BDATE");
					DRF = cm1.getColumnS("DRF_NUMB");
					overlapDate = util.changeDateFormat(overlapDate,"MM/dd/yyyy h:m:s","dd/MM/yyyy");
					count_overlap++;
					if(count_overlap==1)
					{
				 %>

                                             <tr align="center" bgcolor="#0044B0">
                                                      <td class="style2" width="11%">Start DN</td>
                                                      <td class="style2" width="11%">End DN</td>
                                                      <td class="style2" width="11%">Date</td>
                                                      <td class="style2" width="17%">DRN</td>
                                                      <td class="style2" width="17%">BOID</td>
                                                      <td class="style2" width="33%">Name</td>
                                              </tr>
					<%
					  }
					%>
                                            <tr>
                                                    <td class="colred" width="11%" align="center"><%=overlapStartDN%></td>
                                                    <td class="colred" width="11%" align="center"><%=overlapEndDN%></td>
                                                    <td class="colred" width="11%" align="center"><%=overlapDate%></td>
                                                    <td class="colred" width="17%" align="left">&nbsp;&nbsp;<%=DRF%></td>
                                                    <td class="colred" width="17%" align="center"><%=boid%></td>
                                                    <td class="colred" width="33%" align="left">&nbsp;&nbsp;<%=overlapName%></td>
                                            </tr>
					<%

				  }//End of Enner While
			%>
					</table>
                                    </td>
                                  </tr>
				<%
				  //*****************************************

			}//End of Top While
		  }//End of Top If

		 if (isConnect)
		 {
		   cm.takeDown();
		 }
		 if(isConnect1)
		 {
		   cm1.takeDown();
		 }
	  %>
		</table>
	</td>
  </tr>

</table>
</form>
</span>
</body>
</html>
