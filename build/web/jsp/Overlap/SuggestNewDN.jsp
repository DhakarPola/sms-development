<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Creation Date : December 2005
'Page Purpose  : Generates New Distinction Number
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>
<html>
<head>
<style type="text/css">
<!--

.style7 {
	color: #0A2769;
	font-size: 13px;
	font-weight:bold;
}
.style8 {
	color: black;
	font-size: 11px;
}
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>
</head>
<body TEXT="000000" BGCOLOR="#E8F3FD"><%
         boolean isConnect = cm.connect();
         if(isConnect==false)
         {
          %>
          <jsp:forward page="ErrorMsg.jsp" >
            <jsp:param name="ErrorTitle" value="Connection Failure" />
            <jsp:param name="ErrorHeading" value="Connection Problem" />
            <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
          </jsp:forward>
          <%
          }
            String certificateno              ="";
            String queryViewLockedDN          ="";
            String foliono                    ="";
            String start_dist                 ="";
            String end_dist                   ="";
            String total_shares               ="";
            String status                     ="";
            int iend_dist                     =0;
            int istart                        =0;
            int iend                          =0;
            int total                         =0;

            certificateno                     = request.getParameter("certificateno");
            total_shares                      = request.getParameter("total");
            status                            = request.getParameter("status");
            start_dist                        = request.getParameter("start_dist");

            if(status == null) status="";
                total           = Integer.parseInt(total_shares);

            if(certificateno==null)
                certificateno   ="";

            queryViewLockedDN   = "SELECT MAX(DE_DE_END_DIST_NUMB) AS MAX_DIST  FROM LOCKEDFOLIOFROMONETOTHREE";

            cm.queryExecute(queryViewLockedDN);

            while(cm.toNext()){
                iend_dist       = cm.getColumnI("MAX_DIST");
            }

            if(status.equals("exist"))
              iend_dist         = Integer.parseInt(start_dist) -1;

            istart              = iend_dist +1; // new start distinction number
            iend                = istart+total-1;      // new end distinction number

            start_dist          = String.valueOf(istart);
            end_dist            = String.valueOf(iend);
            total_shares        = String.valueOf(total);

            if(certificateno.equals("test")){
                start_dist      = "";
                end_dist        = "";
                total_shares    = "";
            }
        %>

<form action="" method="GET">
<table cellspacing="0" cellpadding="0"  bgcolor="#E8F3FD" border="0" width="100%">
  <tr>
    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="42%" align="right" class="style7">Start Distinctive No</td>
            <td width="1%">&nbsp;</td>
            <td width="1%">:</td>
            <td width="56%"><input type="text" name="start_dist" readonly="readonly" value="<%=start_dist%>" class="style8" align="middle"></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="42%" align="right" class="style7">End Distinctive No&nbsp;&nbsp;&nbsp;</td>
            <td width="1%">&nbsp;</td>
            <td width="1%">:</td>
            <td width="56%"><input type="text" name="end_dist" readonly="readonly" value="<%=end_dist%>" class="style8" align="middle"></td>
          </tr>
        </table>
		</td>
      </tr>
      <tr>
        <td>
        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="42%" align="right" class="style7">Total Shares &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td width="1%">&nbsp;</td>
            <td width="1%">:</td>
            <td width="56%"><input type="text" name="total_shares" readonly="readonly" value="<%=total_shares%>" class="style8" align="middle"></td>
          </tr>
        </table>
        </td>
      </tr>
</table>
</form>
</body>
</html>
