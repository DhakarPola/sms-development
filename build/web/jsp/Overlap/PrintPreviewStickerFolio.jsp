<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : September 2006
'Page Purpose  : Generates Print Sticker
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="util" class="batbsms.Utility"/>
<%@ page language="java" import="oracle.jdbc.driver.*,java.io.*,java.util.*,java.text.*" %>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>
<html>
<head>
<title>Print Preview Sticker</title>
<script type="text/javascript">
  function confirmprint()
  {
      if (confirm("Do you want to Print the Document?"))
      {
        document.all.dprint.style.display = 'none'
        window.print();
        document.all.dprint.style.display = '';
      }
  }
</script>
<style type="text/css">
<!--

.style7 {
	color: black;
	font-size: 10px;
}
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.style10 {font-family: Arial, Helvetica, sans-serif;
          font-size: 10px;}
-->
</style>
</head>
<body >
<span class="style10">
<%
         boolean isConnect = cm.connect();
         if(isConnect==false)
         {
          %>
          <jsp:forward page="ErrorMsg.jsp" >
            <jsp:param name="ErrorTitle" value="Connection Failure" />
            <jsp:param name="ErrorHeading" value="Connection Problem" />
            <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
</jsp:forward>
          <%
          }
          String certificateno ="";
          String foliono="";
          String start_dist = "";
          String end_dist = "";
          String old_start_dist="";
          String old_end_dist="";
          String total_shares = "";
          String issuedate="";
          String thisissdate="";
          String old_total_share="";
          String Follow_name="";

          Date now = new Date();
          DateFormat df = DateFormat.getDateInstance();
          String current_date = df.format(now);
          String slipdate = util.changeDateFormat(current_date,"MM/dd/yyyy h:m:s","dd/MM/yyyy");

          certificateno = request.getParameter("certificateno");
          foliono =   request.getParameter("foliono");
          start_dist  = request.getParameter("start_dist");
          total_shares = request.getParameter("total");
//          end_dist = request.getParameter("end_dist");
          int iend_dist = Integer.parseInt(start_dist) + Integer.parseInt(total_shares) - 1;
          end_dist = String.valueOf(iend_dist);

          Follow_name= request.getParameter("name");
          issuedate= request.getParameter("Idate");

          String findcertinfo = "SELECT * FROM LOCKEDFOLIOFROMONETOTHREE_VIEW WHERE GH3_DE_CERTIFICATE_NUMB = '"+certificateno+"'";
          cm.queryExecute(findcertinfo);

          while(cm.toNext())
          {
           thisissdate = cm.getColumnDT("ISSUEDATE");
          }

/*          if(end_dist.length()<=0){
            int iend_dist= Integer.parseInt(start_dist) + Integer.parseInt(total_shares)- 1;
            end_dist = String.valueOf(iend_dist);
          }
*/
          if(certificateno==null) certificateno = "";

          String finddateQuery = "SELECT C.CERTIFICATE_ISSUE_DATE,S.Name FROM CERTIFICATE C, SHAREHOLDER S WHERE C.HOLDER_FOLIO=S.FOLIO_NO and  C.CERTIFICATE_NO='"+certificateno+"'";
          cm.queryExecute(finddateQuery);

          while(cm.toNext())
          {
              issuedate = cm.getColumnS("CERTIFICATE_ISSUE_DATE");
              issuedate = util.changeDateFormat(issuedate,"MM/dd/yyyy h:m:s","dd/MM/yyyy");
              Follow_name= cm.getColumnS("Name");

          }
%>
<br>
&nbsp;<br>
&nbsp;
</span>
                 <SPAN id="dprint">
                   &nbsp;&nbsp;&nbsp;<img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'"><br><br>
                 </SPAN>

<table width="50%" border="0" cellspacing="0" cellpadding="0" bordercolor="black" style="border-collapse:collapse">
	<tr>
		<td width="3%" class="style10" >&nbsp;</td>
		<td colspan="6" class="style10">
			<table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="black" style="border-collapse:collapse">
			 <tr>
				<td colspan="3" class="style7" align="center" ><p>New Distinctive Numbers (inclusive) for Demat </p>
			   </td>
			  </tr>
			  <tr class="style7">
				<td width="24%" align="center">From</td>
				<td width="20%" align="center">To</td>
				<td width="56%" align="center">Number of Shares </td>
			  </tr>
			  <tr class="style7">
				<td width="24%" align="center"><%=start_dist%></td>
				<td width="20%" align="center"><%=end_dist%></td>
				<td width="56%" align="center"><%=total_shares%></td>
			  </tr>
		  </table>
		</td>
	</tr>
	<tr class="style7">
		<td class="style10" >&nbsp;</td>
		<td colspan="6" class="style10">&nbsp;<%=Follow_name%></td>
  </tr>
	<tr>
		<td class="style10" >&nbsp;</td>
		<td width="17%" align="left" class="style10"><strong>&nbsp;Folio No. </strong>: </td>
		<td width="15%" class="style10"  >&nbsp;<%=foliono%></td>
		<td width="24%"  align="right" class="style10"><strong>&nbsp;Certificate No. : </strong></td>
		<td width="15%" class="style10" >&nbsp;<%=certificateno%></td>
		<td width="11%" align="right" class="style10" ><strong>&nbsp;Date : </strong></td>
		<td width="15%" class="style10" >&nbsp;<%=thisissdate%></td>
  </tr>
</table>

<%
//-------------------------------------------------------------------------

  int distfrom1 = 0;
  int distto1 = 0;
  int totalshares1 = 0;
  String thisissdate1 = "";
  String thiscertificate1 = "";

  String findcertinfo1 = "SELECT * FROM LOCKEDFOLIOFROMONETOTHREE_VIEW WHERE GH3_DE_REG_FOLIO_NUMB = '"+foliono+"' AND GH3_DE_CERTIFICATE_NUMB <> '"+certificateno+"' AND ISSUEDATE = TO_DATE('" + thisissdate + "','DD/MM/YYYY')";
  cm.queryExecute(findcertinfo1);

  int brcounter = 0;

  while(cm.toNext())
  {
   thisissdate1 = cm.getColumnDT("ISSUEDATE");
   distfrom1 = cm.getColumnI("DE_DE_START_DIST_NUMB");
   distto1 = cm.getColumnI("DE_DE_END_DIST_NUMB");
   thiscertificate1 = cm.getColumnS("GH3_DE_CERTIFICATE_NUMB");

   if(thiscertificate1==null) thiscertificate1 = "";
   totalshares1 = distto1 - distfrom1 + 1;

   brcounter++;

   if (brcounter == 9)
   {
    brcounter = 0;
    %>
    <br><br><br>
    <%
   }
   %>
<br><br>
<table width="50%" border="0" cellspacing="0" cellpadding="0" bordercolor="black" style="border-collapse:collapse">
	<tr>
		<td width="3%" class="style10" >&nbsp;</td>
		<td colspan="6" class="style10">
			<table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="black" style="border-collapse:collapse">
			 <tr>
				<td colspan="3" class="style7" align="center" ><p>New Distinctive Numbers (inclusive) for Demat </p>
			   </td>
			  </tr>
			  <tr class="style7">
				<td width="24%" align="center">From</td>
				<td width="20%" align="center">To</td>
				<td width="56%" align="center">Number of Shares </td>
			  </tr>
			  <tr class="style7">
				<td width="24%" align="center"><%=distfrom1%></td>
				<td width="20%" align="center"><%=distto1%></td>
				<td width="56%" align="center"><%=totalshares1%></td>
			  </tr>
		  </table>
		</td>
	</tr>
	<tr class="style7">
		<td class="style10" >&nbsp;</td>
		<td colspan="6" class="style10">&nbsp;<%=Follow_name%></td>
  </tr>
	<tr>
		<td class="style10" >&nbsp;</td>
		<td width="17%" align="left" class="style10"><strong>&nbsp;Folio No. </strong>: </td>
		<td width="15%" class="style10"  >&nbsp;<%=foliono%></td>
		<td width="24%"  align="right" class="style10"><strong>&nbsp;Certificate No. : </strong></td>
		<td width="15%" class="style10" >&nbsp;<%=thiscertificate1%></td>
		<td width="11%" align="right" class="style10" ><strong>&nbsp;Date : </strong></td>
		<td width="15%" class="style10" >&nbsp;<%=thisissdate1%></td>
  </tr>
</table>
   <%
  }

   if (isConnect)
   {
     cm.takeDown();
   }

%>


</body>
</html>
