<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Updated By    : Renad Hakim
'Creation Date : December 2005
'Page Purpose  : Generates New Distinction Number
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Request New Distinction Number</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function btnSuggestNDNClick()
{
  frames['frmSuggestNewDN'].location = "SuggestNewDN.jsp?certificateno="+document.forms[0].certificateno.value+"&total="+document.forms[0].no_of_shares.value+"&status=none"
}

function btnRequestSuggest()
{
    alert("This action is going to require some time.");
    frames['frameExistingLockedNumbers'].location = "ExistingCertificateList.jsp?no_of_shares="+document.forms[0].no_of_shares.value+"&StartValue=0&EndValue=0"
}

function btnLock()
{
   if(frames['frmSuggestNewDN'].document.forms[0].start_dist.value == "")
   {
          alert("Please select a Distinction Number first");
   }
   else
    {
      frames['frameLockedNumbers'].location = "LockedNumbers.jsp?start_dist="+frames['frmSuggestNewDN'].document.forms[0].start_dist.value+"&status=lock&no_of_shares="+document.forms[0].no_of_shares.value+"&foliono="+document.forms[0].foliono.value+"&certificateno="+document.forms[0].certificateno.value+"&StartValue=0&EndValue=0"
      alert("Distinction Numbers Locked.");
      document.all.dbutton1.style.display = 'none';
      document.all.dbutton2.style.display = '';
    }
}

function getframeExistingLockedNumbersValue()
{
    var radio_choice = false;

    for (counter = 0; counter < frames['frameExistingLockedNumbers'].document.forms[0].start_dist.length; counter++)
    {
      if (frames['frameExistingLockedNumbers'].document.forms[0].start_dist[counter].checked)
        return frames['frameExistingLockedNumbers'].document.forms[0].start_dist[counter].value
    }

}
function selectCertificate()
{
  if(getframeExistingLockedNumbersValue()==null)
  {
    alert("Please press Request/Suggest first");
  }
  else
  {
    frames['frmSuggestNewDN'].location = "SuggestNewDN.jsp?start_dist="+getframeExistingLockedNumbersValue()+"&total="+document.forms[0].no_of_shares.value+"&status=exist"+"&certificateno="+document.forms[0].certificateno.value
  }
}

function openInNewFrame()
{
  location = "";
}
function openNewDistinctionWindow()
{

        if(frames['frmSuggestNewDN'].document.forms[0].start_dist.value == "")
        {
          alert("Please select a Distinction Number first");
        }
        else
        {
          var windowname = "PrintPreviewSlip.jsp?foliono="+document.forms[0].foliono.value+"&certificateno="+document.forms[0].certificateno.value+"&start_dist="+frames['frmSuggestNewDN'].document.forms[0].start_dist.value+"&total="+document.forms[0].no_of_shares.value+"&old_dist_from="+document.forms[0].old_dist_from.value+"&old_dist_to="+document.forms[0].old_dist_to.value;
          var win = window.open(windowname,"","'status=no,menubar=no,scrollbars=yes,resizable=yes,toolbar=no'");
          win.focus();
          win.moveTo( 0, 0 );
          win.resizeTo( screen.availWidth, screen.availHeight );
          return win;
        }
}
function openNewWindowSticker()
{
        if(frames['frmSuggestNewDN'].document.forms[0].start_dist.value == "")
        {
          alert("Please select a Distinction Number first");
        }
        else
        {
          var windownamesticker = "PrintPreviewSticker.jsp?foliono="+document.forms[0].foliono.value+"&certificateno="+document.forms[0].certificateno.value+"&start_dist="+frames['frmSuggestNewDN'].document.forms[0].start_dist.value+"&total="+document.forms[0].no_of_shares.value+"&name=tt&Idate=01/01/2006";
          //alert(windownamesticker);
          var winsticker = window.open(windownamesticker,"","'status=no,menubar=no,scrollbars=yes,resizable=yes,toolbar=no'");
          winsticker.focus();
          winsticker.moveTo( 0, 0 );
          winsticker.resizeTo( screen.availWidth, screen.availHeight );
          return winsticker;
        }
}

 function askdelete()
 {
  if (confirm("Do you want to Delete the Signature?"))
  {
   document.forms[0].submit();
  }
 }

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}

function closeWindow()
{
  window.opener.history.go(0);
  self.close();
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #0A2769;
	font-weight: bold;
	font-size: 12px;
}
.style8 {
	color: white;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
        color: black;
	font-size: 11px;
}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
         boolean isConnect = cm.connect();
         if(isConnect==false)
         {
          %>
          <jsp:forward page="ErrorMsg.jsp" >
            <jsp:param name="ErrorTitle" value="Connection Failure" />
            <jsp:param name="ErrorHeading" value="Connection Problem" />
            <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
          </jsp:forward>
          <%
          }
          String certificateno =request.getParameter("certificateno");
          String queryFindFolio ="";
          String query1 = "";
          String foliono="";
          String sms_dist_from = "";
          String sms_dist_to = "";
          String no_of_shares = "";



          queryFindFolio = "SELECT HOLDER_FOLIO FROM CERTIFICATE WHERE CERTIFICATE_NO = '"+certificateno +"'";
          cm.queryExecute(queryFindFolio);
          while(cm.toNext())
          {
              foliono = cm.getColumnS("HOLDER_FOLIO");
          }

          query1 = "SELECT s.CERTIFICATE_NO,s.DIST_FROM,s.DIST_TO,(s.DIST_TO - s.DIST_FROM +1) AS  NO_OF_SHARES ";
          query1 += "FROM SCRIPTS s,CERTIFICATE c ";
          query1 += "WHERE (c.CERTIFICATE_NO = s.CERTIFICATE_NO AND c.CERTIFICATE_NO='"+certificateno+"' AND c.VALIDITY='T') ";

          cm.queryExecute(query1);
%>
<span class="style7">
<form action="test.jsp" method="get">
<table width="100%"  border="1" cellspacing="0" cellpadding="0" bordercolor="#0044B0" style="border-collapse: collapse" bgcolor="#E8F3FD" >
<tr><td >
<table width="100%"  border="0" cellspacing="0" cellpadding="0" >
  <tr  bgcolor="#0044B0">
    <td colspan="3" align="center" class="style8">New Distinction Number </td>
  </tr>
  <tr>
  <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0" >
      <tr bgcolor="#0044B0">
        <td align="center" class="style8">Selected Certificate and Distinction Number</td>
      </tr>
      <tr>
        <td><table width="100%"  border="1" cellspacing="0" cellpadding="0" bordercolor="#0044B0">
          <tr  >
            <th width="10%" scope="col" class="style7">Folio</th>
            <th width="18%" scope="col" class="style7">Cert_Num</th>
            <th width="21%" scope="col" class="style7">Dist_From</th>
            <th width="18%" scope="col" class="style7">Dist_To</th>
            <th width="33%" scope="col" class="style7">Total Shares </th>
          </tr>
          <%
            while(cm.toNext())
            {
              sms_dist_from = cm.getColumnS("DIST_FROM");
              sms_dist_to = cm.getColumnS("DIST_TO");
              no_of_shares = cm.getColumnS("NO_OF_SHARES");
          %>
          <tr align="center">
            <td class="style11"><%=foliono%></td>
            <td class="style11"><%=certificateno%></td>
            <td class="style11"><%=sms_dist_from%></td>
            <td class="style11"><%=sms_dist_to%></td>
            <td class="style11"><%=no_of_shares%></td>
          </tr>
          <%
            }
          %>
        </table></td>
      </tr>
	  <tr><td>&nbsp;</td></tr>
      <tr>
        <td align="center">
                  <input type="hidden"  name="certificateno" value="<%=certificateno%>"/>
                  <input type="hidden"  name="no_of_shares" value="<%=no_of_shares%>"/>
                  <input type="hidden"  name="foliono" value="<%=foliono%>"/>
                  <input type="hidden"  name="old_dist_from" value="<%=sms_dist_from%>"/>
                  <input type="hidden"  name="old_dist_to" value="<%=sms_dist_to%>"/>
  <!--                <input type="button" name="SuggestNDN" value="Suggest New Distinction" onclick="btnSuggestNDNClick()">-->
                  <img name="B1" src="<%=request.getContextPath()%>/images/btnSuggestNewDN.gif"  onclick="btnSuggestNDNClick()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSuggestNewDNR.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSuggestNewDN.gif'">
		   <!-- <a href="SuggestNewDN.jsp?certificateno=&total=&status=none" target="frmSuggestNewDN">
			Suggest New Distinction</a>		-->        </td>
      </tr>
	  <tr><td>&nbsp;</td></tr>
	  <tr><td align="center"><iframe  name="frmSuggestNewDN" id="frmSuggestNewDN" src="SuggestNewDN.jsp?certificateno=test&total=0&status=none" width="300" height="200" scrolling="no" frameborder="0">
  	  			</iframe>
	  </td></tr>
	  <tr><td>&nbsp;</td></tr>
    </table></td>

  <td>
  	<pre>

	</pre>
  	<table width="100%"  border="0" cellspacing="0" cellpadding="0" >
  		<tr style="border-bottom:none">
  		  <td>
		  	<table width="97%"  border="1" cellspacing="0" cellpadding="0" bordercolor="#0044B0" >
				 <tr  align="center">
					<td width="15%" class="style7">Start_Dist</td>
					<td width="15%" class="style7">End_Dist</td>
					<td width="10%" class="style7">Total Share </td>
					<td width="15%" class="style7">Certificate</td>
					<td width="15%" class="style7"> Folio</td>
					<td width="15%" class="style7">Issue By </td>
					<td width="15%" class="style7">Issue Date </td>
      			</tr>
			</table>		  </td>
  		</tr>
          <tr>
		  	<td height="300" style="border-top:none">
		  		<iframe  name="frameLockedNumbers" src="LockedNumbers.jsp?certificateno=0&status=0&no_of_shares=0&foliono=0&StartValue=0&EndValue=0" width="600" height="300" frameborder="0">  </iframe>
                        </td>
		  </tr>
      </table></td>
  </tr>
  <tr><td>&nbsp;</td></tr>
  <tr>
    <td colspan="3"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><!--<input type="button" name="requestSuggest" value="Request Suggest" onclick="btnRequestSuggest()">--><!--<a href="ExistingCertificateList.jsp?no_of_shares=" target="frameExistingLockedNumbers">
				Request Suggest</a>-->        </td>
        <td>&nbsp;</td>
        <td><!--<input type="button" name="select" value="Select...." onclick="selectCertificate()">-->
        <img name="B3" src="<%=request.getContextPath()%>/images/btnSelect.gif"  onclick="selectCertificate()" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnSelectOn.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnSelect.gif'">        </td>
        <td>&nbsp;</td>
        <td><!--<input type="button" name="lock" value="Lock Number" onclick="btnLock()">--><!-- <a href="LockedNumbers.jsp?certificateno=&status=lock&no_of_shares=&foliono=" target="frameLockedNumbers">
				LOCK Distinction Number</a>      -->  </td>
        <td>&nbsp;</td>
        <td><!--<input type="button" name="Button" value="Preview Slip for Print" onClick="openNewDistinctionWindow()">--></td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td width="46%"><table width="100%"  border="0" cellspacing="0" cellpadding="0" bordercolor="#0044B0">
      <tr>
	  	<td>
			<table width="92%"  border="1" cellspacing="0" cellpadding="0" bordercolor="#0044B0">
				<tr>
                                    <td width="33%" align="center" class="style7">From </td>
                                    <td width="33%"  align="center" class="style7">To</td>
                                    <td width="33%" align="center" class="style7">Total</td>
                                </tr>
			</table>
		<td>      </tr>
      <tr>
        <td colspan="3">
			<iframe  name="frameExistingLockedNumbers"  id="frameExistingLockedNumbers" src="ExistingCertificateList.jsp?no_of_shares=<%=no_of_shares%>&StartValue=0&EndValue=0" width="355" height="300" frameborder="0"></iframe>				        </td>
      </tr>
    </table></td>
    <td width="54%" colspan="2">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
        <SPAN id="dbutton1">
          <img name="B4" src="<%=request.getContextPath()%>/images/btnLOCKDistinctionNo.gif"  onclick="btnLock()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnLOCKDistinctionNoR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnLOCKDistinctionNo.gif'">
        </span>
        <SPAN id="dbutton2" name="DivInfospan" style="display:none">
          <img name="B4" src="<%=request.getContextPath()%>/images/btnLOCKDistinctionNo.gif"  onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnLOCKDistinctionNoR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnLOCKDistinctionNo.gif'">
        </span>
        </td>
        <td><img name="B5" src="<%=request.getContextPath()%>/images/btnPreviewSlipForPrint.gif"  onclick="openNewDistinctionWindow();" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnPreviewSlipForPrintR.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnPreviewSlipForPrint.gif'"></td>
        <td><img name="B7" src="<%=request.getContextPath()%>/images/btnPreviewStickerForPrint.gif"  onclick="openNewWindowSticker();" onMouseOver="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnPreviewStickerForPrint.gif'" onMouseOut="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnPreviewStickerForPrint.gif'"></td>
        <td><img name="B6" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="closeWindow()" onMouseOver="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnClose.gif'"></td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td colspan="3"><!--<input type="submit" name="Submit" value="Close" onClick="self.close()">--></td>
  </tr>

  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
</td></tr>
<%
     if (isConnect)
     {
       cm.takeDown();
     }
%>
</table>
</form>
</span>

</body>
</html>
