<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : December 2005
'Page Purpose  : View of all Letters.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Letters</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function tonewletter()
{
  location = "<%=request.getContextPath()%>/jsp/Others/NewLetter.jsp"
}

function todeleteletter()
{
  location = "<%=request.getContextPath()%>/jsp/Others/DeleteLetter.jsp?"
}

 function askdelete()
 {
  if (confirm("Do you want to Delete the Letter?"))
  {
   document.forms[0].submit();
  }
 }

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String query1 = "SELECT * FROM LETTER_VIEW ORDER BY LETTER_ID";
    cm.queryExecute(query1);

    String lettername = "";
    String letterid = "";
    int editdelete = 0;
    String signame = "";
    String sigrank = "";
    String dest = "";

    int bankid = 0;
%>
  <span class="style7">
  <form method="GET" action="DeleteLetter.jsp">
  <img name="B4" src="<%=request.getContextPath()%>/images/btnNew.gif" onclick="tonewletter()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNewOn.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNew.gif'">
  <img name="B6" src="<%=request.getContextPath()%>/images/btnDelete.gif" onclick="askdelete()" onMouseOver="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnDeleteOn.gif'" onMouseOut="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnDelete.gif'">
  <table width="100%" BORDER=1  cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>List of Letters</center></td></tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="40%" class="style9"><div align="center">Letter Name</div></td>
    <td width="26%"><div align="center" class="style9">
      <div align="center">Signed By</div>
    </div></td>
    <td width="26%"><div align="center" class="style9">
      <div align="center">Rank</div>
    </div></td>
    <td width="8%"><div align="center" class="style9">
      <div align="center">Edited</div>
    </div></td>
  </tr>
  <div align="left">
     <%
    while(cm.toNext())
     {
       lettername = cm.getColumnS("LETTER_NAME");
       letterid = cm.getColumnS("LETTER_ID");
       editdelete = cm.getColumnI("EDIT_DELETE");
       signame = cm.getColumnS("NAME");
       sigrank = cm.getColumnS("RANK");

       dest = request.getContextPath() + "/jsp/Others/EditLetter.jsp";

       if (String.valueOf(lettername).equals("null"))
         lettername = "";
       if (String.valueOf(letterid).equals("null"))
         letterid = "";
       if (String.valueOf(signame).equals("null"))
         signame = "";
       if (String.valueOf(sigrank).equals("null"))
         sigrank = "";
         %>
    </div>
  <tr bgcolor="#E8F3FD">
           <td><div align="center" class="style10">
             <div align="left" class="style10">
               <span class="style13">
              <label><input type="radio" checked="checked" name="dletter" value="<%=letterid%>"></label>
               &nbsp;&nbsp;&nbsp<a HREF="<%=dest%>?lname1=<%=lettername%>&lid1=<%=letterid%>"><%=lettername%></a>
               &nbsp;
               </span></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="left">&nbsp;<%=signame%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="left"><%=sigrank%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=editdelete%>&nbsp;</div>
           </div></td>
         </tr>
         <div align="left" class="style13">
             <%
     }
   if (isConnect)
   {
     cm.takeDown();
   }
  %>
         </div>
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>

</body>
</html>
