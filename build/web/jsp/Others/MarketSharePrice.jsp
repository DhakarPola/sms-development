<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Updated by    : Md. Kamruzzaman
'Creation Date : February 2006
'Page Purpose  : View of all Market Share Price Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
.style19 {
	color: #0A2769;
	font-weight: bold;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Market Share Price</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function goPrevious(pstart,pend)
{
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Others/MarketSharePrice.jsp?StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;
  location = thisurl;
}

function goNext(nstart,nend)
{
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Others/MarketSharePrice.jsp?StartValue=" + nstart;
  thisurl = thisurl + "&EndValue=" + nend;
  location = thisurl;
}

function gosearch(edate,tchunk,sstart,send)
{
  if (document.all.entrydate.value.length)
  {
   var thisurl = "<%=request.getContextPath()%>";
   thisurl = thisurl + "/jsp/Others/FindSharePricebyDate.jsp?EDate=" + edate;
   thisurl = thisurl + "&ChunkSize=" + tchunk;
   thisurl = thisurl + "&StartingValue=" + sstart;
   thisurl = thisurl + "&EndingValue=" + send;
   location = thisurl;
  }
}

 function tonewsp(tstart,tend)
 {
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Others/NewSharePrice.jsp?StartValue=" + tstart;
  thisurl = thisurl + "&EndValue=" + tend;
  location = thisurl;
 }

 function toprintdocument()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style17 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String SStartingValue = String.valueOf(request.getParameter("StartValue"));
    String SEndingValue = String.valueOf(request.getParameter("EndValue"));

    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    int Chunk = 15;
    EndingValue = StartingValue + Chunk - 1;

    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;
    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }

    String query1cc = "SELECT * FROM SHARE_PRICE_VIEW ORDER BY ENTRY_DATE DESC";
    int fcc = cm.queryExecuteCount(query1cc);

    if (nStartingValue > (fcc-Chunk+1))
    {
      nStartingValue = fcc - Chunk + 1;
    }
    if (nEndingValue > fcc)
    {
      nEndingValue = fcc;
    }
    if (nStartingValue < 1)
    {
      nStartingValue = 1;
    }
    if (nEndingValue < Chunk)
    {
      nEndingValue = Chunk;
    }

    String query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM SHARE_PRICE_VIEW ORDER BY ENTRY_DATE DESC) div where rownum <= " + EndingValue + ") where rnum >= " + StartingValue + "";
    cm.queryExecute(query1);

    String market = "";
    String entrydate = "";
    double unitprice = 0;
    double avgprice = 0;
    double iindex = 0;
    int rowcounter1 = 0;
    String shareTraded="0";
%>
  <span class="style7">
  <form method="GET" action="NewSharePrice.jsp">
  <SPAN id="dprint">
    <img name="B4" src="<%=request.getContextPath()%>/images/btnNew.gif" onclick="tonewsp(<%=pStartingValue%>,<%=pEndingValue%>)" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNewOn.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNew.gif'">
    <img name="B4" src="<%=request.getContextPath()%>/images/btnPrint.gif" onclick="toprintdocument()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
  </SPAN>
  <table width="100%" BORDER=0  cellpadding="0" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td height="30" bgcolor="#0044B0" class="style17" valign="middle"><center>Market Share Price</center></td></tr>
  </table>
  <table width="100%"  border="0" cellpadding="0" cellspacing="0" bordercolor="#0044B0">
      <tr bgcolor="#E8F3FD">
	<td width="10%" style="border-left: solid 1px #0044B0;">&nbsp;</td>
	<td width="10%"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goPrevious(<%=pStartingValue%>,<%=pEndingValue%>)"></td>
	<td width="10%"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNext(<%=nStartingValue%>,<%=nEndingValue%>)"></td>
	<td width="12%" height="35" class="style19" align="right">Date:</td>
	<td width="35%" align="center">
         <a href="javascript:NewCal('entrydate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
         &nbsp
         <input name="entrydate" type=text id="entrydate" value="" maxlength="10" class="SL2TextField" onFocus="this.blur();">
          </td>
	<td width="23%" style="border-right: solid 1px #0044B0;"><img src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="gosearch(document.all.entrydate.value,<%=Chunk%>,<%=StartingValue%>,<%=EndingValue%>)"></td>
      </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="12%" class="style9"><div align="center">Market</div></td>
    <td width="12%"><div align="center" class="style9">
      <div align="center">Date</div>
    </div></td>
    <td width="20%"><div align="center" class="style9">
      <div align="center">Unit Price (Tk)</div>
    </div></td>
    <td width="20%"><div align="center" class="style9">
      <div align="center">Avg. Price (Tk)</div>
    </div></td>
    <td width="16%"><div align="center" class="style9">
      <div align="center">Index</div>
    </div></td>
    <td width="20%"><div align="center" class="style9">
      <div align="center">No. Of Share Traded</div>
    </div></td>
  </tr>
  <div align="left">
     <%
    while(cm.toNext())
     {
       rowcounter1++;
       market = cm.getColumnS("MARKET");
       entrydate = cm.getColumnDT("ENTRY_DATE");
       unitprice = cm.getColumnF("UNIT_PRICE");
       avgprice = cm.getColumnF("AVG_PRICE");
       iindex = cm.getColumnF("SHARE_INDEX");
       shareTraded=cm.getColumnS("SHARE_TRADED");
       unitprice = cal.roundtovalue(unitprice,2);
       avgprice = cal.roundtovalue(avgprice,2);
       iindex = cal.roundtovalue(iindex,2);

       if(String.valueOf(shareTraded).equals("null"))
       shareTraded="0";
     %>
    </div>
  <tr bgcolor="#E8F3FD">
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;
               <a HREF="<%=request.getContextPath()%>/jsp/Others/EditSharePrice.jsp?marketname=<%=market%>&entrydate=<%=entrydate%>"><%=market%></a>
               &nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=entrydate%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=unitprice%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=avgprice%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=iindex%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=shareTraded%>&nbsp;</div>
           </div></td>
         </tr>
         <div align="left" class="style13">
             <%
         }
         %>
           <!--HIDDEN FIELDS-->
            <input type="hidden" name="rowcounter1" value="<%=rowcounter1%>">
            <input type="hidden" name="svalue" value="<%=StartingValue%>">
            <input type="hidden" name="evalue" value="<%=EndingValue%>">
            <input type="hidden" name="pchunk" value="<%=Chunk%>">
         <%

  if (isConnect)
   {
     cm.takeDown();
   }
  %>
         </div>
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>

</body>
</html>
