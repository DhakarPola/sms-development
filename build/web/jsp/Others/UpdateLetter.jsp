<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : December 2005
'Page Purpose  : Updates Letter information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update Letter</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

  String LetterName = String.valueOf(request.getParameter("lettername"));
  String LetterSubject = String.valueOf(request.getParameter("lettersubject"));
  String LetterBody = String.valueOf(request.getParameter("letterbody"));
  String SigID = String.valueOf(request.getParameter("sigselect"));
  String NofEdits = String.valueOf(request.getParameter("editdelete"));
  String LetterID = String.valueOf(request.getParameter("idletter"));

  LetterName=cm.replace(LetterName,"'","''");
  LetterSubject=cm.replace(LetterSubject,"'","''");
  LetterBody=cm.replace(LetterBody,"'","''");

  String uletter = "call UPDATE_LETTER('" + LetterID + "', '" + LetterName + "', '" + LetterSubject + "', '" + LetterBody + "','" + SigID + "'," + NofEdits + ")";
  boolean b = cm.procedureExecute(uletter);

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Edited Letter Template')";
  boolean ub = cm.procedureExecute(ulog);

 if (isConnect)
  {
    cm.takeDown();
  }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Others/OthersLetterEdit.jsp";
</script>
</body>
</html>
