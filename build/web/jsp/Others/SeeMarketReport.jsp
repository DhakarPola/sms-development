<script>
/*
'******************************************************************************************************************************************
'Script Author : Rahat
'Creation Date : July 2006
'Page Purpose  : Exports Share Market Report to Excel Format.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Market Comparison</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<%@ page language="java" import="org.apache.poi.hssf.usermodel.*,java.util.Vector,java.io.*,java.util.*,java.util.List,java.util.Vector" %>
<script language="javascript">
<!--
//Execute while click on Submit
function SubmitThis() {
  count = 0;

  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String MarketName = String.valueOf(request.getParameter("marketselect"));

    if (MarketName.equalsIgnoreCase("Others"))
    {
      MarketName = String.valueOf(request.getParameter("omname"));
    }

    String StartDate = String.valueOf(request.getParameter("datefrom"));
    String EndDate = String.valueOf(request.getParameter("dateto"));

    int countrecords = 0;
    String query1 = "SELECT * FROM SHARE_PRICE_VIEW WHERE MARKET = '" + MarketName + "'";
    countrecords = cm.queryExecuteCount(query1);

    if (countrecords == 0)
    {
     %>
       <script language="javascript">
        alert("No Market with this name exists in the Database!\nPlease check again!");
        history.go(-1);
       </script>
     <%
    }

    countrecords = 0;
    query1 = "SELECT * FROM SHARE_PRICE_VIEW WHERE MARKET = '" + MarketName + "' AND ( ENTRY_DATE BETWEEN TO_DATE('" + StartDate + "','DD/MM/YYYY') AND  TO_DATE('" + EndDate + "','DD/MM/YYYY'))";
    countrecords = cm.queryExecuteCount(query1);

    if (countrecords == 0)
    {
     %>
       <script language="javascript">
        alert("No Records with this combination exist in the Database!\nPlease check again!");
        history.go(-1);
       </script>
     <%
    }

      // Add Excel Export Code Here.
	  String Marketselect = String.valueOf(request.getParameter("marketselect"));
	  String Datefrom = String.valueOf(request.getParameter("datefrom"));
	  String Dateto = String.valueOf(request.getParameter("dateto"));

	  String SelectQuery="SELECT MARKET,ENTRY_DATE,UNIT_PRICE,\"MOVEMENT\",MAX_PRICE,MIN_PRICE,AVG_PRICE,SHARE_INDEX,INDEX_MOVEMENT FROM SHARE_PRICE_View where MARKET='"+Marketselect+"' and (ENTRY_DATE BETWEEN TO_DATE('"+Datefrom+"', 'DD/MM/YYYY') AND TO_DATE('"+Dateto+"', 'DD/MM/YYYY')) Order By ENTRY_DATE";

          String MARKET = "";
          String ENTRY_DATE = "";
          double UNIT_PRICE = 0;
          double MOVEMENT = 0;
          double MAX_PRICE = 0;
          double MIN_PRICE = 0;
          double AVG_PRICE = 0;
          double SHARE_INDEX =0;
          double INDEX_MOVEMENT = 0;

          cm.queryExecute(SelectQuery);

          HSSFWorkbook wb = new HSSFWorkbook();
          HSSFSheet sheet = wb.createSheet("Market Report");
          // Create a row and put some cells in it. Rows are 0 based.
           HSSFRow row = sheet.createRow((short)0);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue("Market Report");

           row = sheet.createRow((short)1);
           row.createCell((short)0).setCellValue("");
          row.createCell((short)1).setCellValue("Market : "+Marketselect);

           row = sheet.createRow((short)2);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue("Period From : "+Datefrom);

          row = sheet.createRow((short)3);
          row.createCell((short)0).setCellValue("");
          row.createCell((short)1).setCellValue("Period To : "+Dateto);

          row = sheet.createRow((short)4);
          row.createCell((short)0).setCellValue("");

          row = sheet.createRow((short)5);

          // Create a cell and put a value in it.
          row.createCell((short)0).setCellValue("");
          row.createCell((short)1).setCellValue("Entry Date");
          row.createCell((short)2).setCellValue("Unit Price");
          row.createCell((short)3).setCellValue("Movement");
          row.createCell((short)4).setCellValue("Max Price");
          row.createCell((short)5).setCellValue("Min Price");
          row.createCell((short)6).setCellValue("Avg Price");
          row.createCell((short)7).setCellValue("Share Index");
          row.createCell((short)8).setCellValue("Index Movement");

          int i=6;
           while(cm.toNext())
          {

            MARKET = cm.getColumnS("MARKET");
            ENTRY_DATE = cm.getColumnDT("ENTRY_DATE");
            UNIT_PRICE= cm.getColumnF("UNIT_PRICE");
            MOVEMENT = cm.getColumnF("MOVEMENT");
            MAX_PRICE= cm.getColumnF("MAX_PRICE");
            MIN_PRICE= cm.getColumnF("MIN_PRICE");
            AVG_PRICE = cm.getColumnF("AVG_PRICE");
            SHARE_INDEX = cm.getColumnF("SHARE_INDEX");
            INDEX_MOVEMENT = cm.getColumnF("INDEX_MOVEMENT");

            HSSFRow rowi = sheet.createRow((short)i);

            rowi.createCell((short)0).setCellValue("");
            rowi.createCell((short)1).setCellValue(ENTRY_DATE);
            rowi.createCell((short)2).setCellValue(UNIT_PRICE);
            rowi.createCell((short)3).setCellValue(MOVEMENT);
            rowi.createCell((short)4).setCellValue(MAX_PRICE);
            rowi.createCell((short)5).setCellValue(MIN_PRICE);
            rowi.createCell((short)6).setCellValue(AVG_PRICE);
            rowi.createCell((short)7).setCellValue(SHARE_INDEX);
            rowi.createCell((short)8).setCellValue(INDEX_MOVEMENT);
            i++;
             }


             // Write the output to a file
             String savepath="";
             ServletContext sc = getServletConfig().getServletContext();
          //Code for creating a folder
           String path=getServletContext().getRealPath("/");
           File f1 = new File(path + "/Excel Export");
           if(!f1.exists())
           {
                 f1.mkdir();
           }
           savepath = sc.getRealPath("/") +"Excel Export/"+"MarketReport.xls";

                FileOutputStream fileOut = new FileOutputStream(savepath);
                wb.write(fileOut);
                fileOut.close();
                fileOut=null;
                sheet=null;
                wb=null;
         //End Excel Export
        String tempURL =  request.getContextPath() +"/Excel Export/"+"MarketReport.xls";
        %>
<script language="javascript">
    showPrintPreview6('<%=tempURL%>');
    location = "<%=request.getContextPath()%>/jsp/Others/MarketReport.jsp?MarketName=<%=MarketName%>&FromDate=<%=Datefrom%>&ToDate=<%=Dateto%>";
</script>



<form action="SeeMarketReport.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Generate Market Report</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">


      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Market</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8"><b><%=MarketName%></b></div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="100%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="100%" scope="row">
            <div align="center">
 		<img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="history.go(-1)" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
            </div>
        </th>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
