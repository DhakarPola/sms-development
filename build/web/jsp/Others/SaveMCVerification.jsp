<script>
/*
'******************************************************************************************************************************************
'Script Author :Rahat
'Creation Date : June 2006
'Page Purpose  : Save Attendance.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Selected Certificates Varification</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String rowcounter = String.valueOf(request.getParameter("rowcounter1"));
  String Fldname = "";
  String markcoll = "";
  String uuser="";
   String ulog="";
  int irowcounter = Integer.parseInt(rowcounter);

  for (int i=1;i<irowcounter+1;i++)
  {
    Fldname = "selectingbox" + String.valueOf(i);
    String BoxValue = String.valueOf(request.getParameter(Fldname));

    BoxValue=cm.replace(BoxValue,"'","''");

    if (String.valueOf(BoxValue).equals("null"))
    {
      BoxValue = "";
    }
    if (!BoxValue.equalsIgnoreCase(""))
    {
		//markcoll="update agmattendance set is_present='T' where boid='" + BoxValue + "'";
		uuser = "call UPDATE_VERIFY_CERTIFICATE('" + BoxValue + "','T')";
		System.out.println(uuser);
        cm.queryExecute(uuser);
		ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Verified Certificate')";
  		boolean ub = cm.procedureExecute(ulog);
    }
  }

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  //alert("Attendance Done!");
  location = "<%=request.getContextPath()%>/jsp/Others/VerifySignature.jsp";
</script>
</body>
</html>
