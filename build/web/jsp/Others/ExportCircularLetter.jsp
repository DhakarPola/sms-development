<script>
/*
'******************************************************************************************************************************************
'Script Author : Masud Ahmad
'Creation Date : March 2006
'Updated By : Aminul Islam
'Creation Date : June 2006
'Page Purpose  : Export ShareHolder and BOAddress info in Excel for Merge purpose
'******************************************************************************************************************************************
*/
</script>
<%@ page language="java" import="org.apache.poi.hssf.usermodel.*,java.util.Vector,java.io.*,java.util.*,java.util.List,java.util.Vector" %>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>
<html>
<head>
<title>
ExportCircularLetter
</title>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update User</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>
<body bgcolor="#ffffff">
<h1>
JBuilder Generated JSP
</h1>
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String TypeVal = String.valueOf(request.getParameter("Type"));
  String qNationality=String.valueOf(request.getParameter("Nationality"));
  String qResidence=String.valueOf(request.getParameter("Residence"));
  String qGThan=String.valueOf(request.getParameter("CLGreaterThan"));
  String qLThan=String.valueOf(request.getParameter("CLLessThan"));
  String qCategory=String.valueOf(request.getParameter("CLOfCategory"));
  String qFolioGThan=String.valueOf(request.getParameter("CLFrom"));
  String qFolioLThan=String.valueOf(request.getParameter("CLTo"));

   String FNO = "";
   String PREFIX = "";
   String SHNAME = "";
   String SURNAME = "";
   String RELATION = "";
   String FATHER = "";
   String ADDRESS1 = "";
   String ADDRESS2 = "";
   String ADDRESS3 = "";
   String ADDRESS4 = "";
   String ZIP = "";
   String OCCUPATION = "";
   String JOINT = "";
   String CLASS="";
   String ACCOUNT_NO="";
   String BANK_NAME="";
   String BANK_BRANCH="";
   String TOTAL_SHARE="";
   String TAXFREE_SHARE="";
   String LIABLE_TO_TAX="";
   String SPECIAL_TAX="";
   String SMSRESIDENT="";
   String SMSNATIONALITY="";
   String NOMINEE_NO="";
   String ACTIVE="";
   String TO_CHARDATE="";
   String LEAVE_DATE="";

   //CDBL
   String BOID="";
   String NAMEFHODER="";
   String NAMECPERSON="";
   String ADDRESS="";
   String CITY="";
   String STATE="";
   String POSTCODE="";
   String COUNTRY="";
   String Ph ="";
   String FAX="";
   String EMAIL="";
   String TFSHARE="";
   String LTTAX="";
   String STAX="";
   String RESIDENT="";
   String NATIONALITY="";
   String C_LASS="";
   String PF_DATE="";

   String queryWhereClause="";

   if (TypeVal.equalsIgnoreCase("SMS")){
     String TableInfo="Select * from SHAREHOLDER_ACTIVE_VIEW ";
     queryWhereClause+=" WHERE TOTAL_SHARE >= "+qGThan;
     queryWhereClause+=" AND TOTAL_SHARE <= "+qLThan;
     queryWhereClause+=" AND folio_no >= "+qFolioGThan;
     queryWhereClause+=" AND folio_no <= "+qFolioLThan;
     if ((!qCategory.trim().equals("_")) && (qCategory.trim().equals("ALL")))
     {
       //queryWhereClause+=" AND trim(CLASS)='"+qCategory+"'";
     }
     else
     queryWhereClause+=" AND trim(CLASS)='"+qCategory+"'";

     if (qNationality.equals("Bangladeshi"))
       queryWhereClause+=" AND trim(NATIONALITY)='Bangladeshi'";
     else if (qNationality.equals("Not Bangladeshi"))
       queryWhereClause+=" AND trim(NATIONALITY) !='Bangladeshi'";
     if (qResidence.equals("Resident"))
       queryWhereClause+=" AND trim(RESIDENT)='T'";
     else if (qResidence.equals("Non Resident"))
       queryWhereClause+=" AND trim(RESIDENT)='F'";

     queryWhereClause+=" order by SUBSTR(folio_no,3,6)";
     System.out.println(TableInfo+queryWhereClause);
     cm.queryExecute(TableInfo+queryWhereClause);
   }

   if (TypeVal.equalsIgnoreCase("CDBL")){
     String TableInfo = "Select * from BOADDRESS_VIEW";
     //queryWhereClause+=" WHERE TOTAL_SHARE >= "+qGThan;
     //queryWhereClause+=" AND TOTAL_SHARE <= "+qLThan;
     queryWhereClause+="WHERE BO_ID >= "+qFolioGThan;
     queryWhereClause+=" AND BO_ID <= "+qFolioLThan;
     if ((!qCategory.trim().equals("_")) && (qCategory.trim().equals("ALL"))) 
    {
       //queryWhereClause+=" AND trim(CLASS)='"+qCategory+"'";
    }
    else
    queryWhereClause+=" AND trim(CLASS)='"+qCategory+"'";

     if (qNationality.equals("Bangladeshi"))
       queryWhereClause+=" AND trim(NATIONALITY)='Bangladeshi'";
     else if (qNationality.equals("Not Bangladeshi"))
       queryWhereClause+=" AND trim(NATIONALITY) !='Bangladeshi'";
     if (qResidence.equals("Resident"))
       queryWhereClause+=" AND trim(RESIDENT)='T'";
     else if (qResidence.equals("Non Resident"))
       queryWhereClause+=" AND trim(RESIDENT)='F'";

     queryWhereClause+=" ORDER BY BO_ID";
     System.out.println(TableInfo+queryWhereClause);
     cm.queryExecute(TableInfo+queryWhereClause);
   }
                HSSFWorkbook wb = new HSSFWorkbook();
                HSSFSheet sheet = wb.createSheet("Report");
                // Create a row and put some cells in it. Rows are 0 based.
                HSSFRow row = sheet.createRow((short)0);

                // Create a cell and put a value in it.

                  if (TypeVal.equalsIgnoreCase("CDBL")){
                   row.createCell((short)0).setCellValue("BO_ID");
                   row.createCell((short)1).setCellValue("NAME_FIRSTHOLDER");
                   row.createCell((short)2).setCellValue("NAME_CONTACT_PERSON");
                   row.createCell((short)3).setCellValue("ADDRESS");
                   row.createCell((short)4).setCellValue("CITY");
                   row.createCell((short)5).setCellValue("STATE");
                   row.createCell((short)6).setCellValue("POSTAL_CODE");
                   row.createCell((short)7).setCellValue("COUNTRY");
                   row.createCell((short)8).setCellValue("PHONE");
                   row.createCell((short)9).setCellValue("FAX");
                   row.createCell((short)10).setCellValue("EMAIL");
                   row.createCell((short)11).setCellValue("TAX_FREE_SHARE");
                   row.createCell((short)12).setCellValue("LIABLE_TO_TAX");
                   row.createCell((short)13).setCellValue("SPECIAL_TAX");
                   row.createCell((short)14).setCellValue("RESIDENT");
                   row.createCell((short)15).setCellValue("NATIONALITY");
                   row.createCell((short)16).setCellValue("CLASS");
                   row.createCell((short)17).setCellValue("TOTAL_SHARE");
                   row.createCell((short)18).setCellValue("PF_BDATE");
                 }
                 if (TypeVal.equalsIgnoreCase("SMS")){
                   row.createCell((short)0).setCellValue("FOLIO_NO");
                   row.createCell((short)1).setCellValue("PREFIX");
                   row.createCell((short)2).setCellValue("NAME");
                   row.createCell((short)3).setCellValue("SURNAME");
                   row.createCell((short)4).setCellValue("RELATION");
                   row.createCell((short)5).setCellValue("FATHER");
                   row.createCell((short)6).setCellValue("ADDRESS1");
                   row.createCell((short)7).setCellValue("ADDRESS2");
                   row.createCell((short)8).setCellValue("ADDRESS3");
                   row.createCell((short)9).setCellValue("ADDRESS4");
                   row.createCell((short)10).setCellValue("ZIP");
                   row.createCell((short)11).setCellValue("OCCUPATION");
                   row.createCell((short)12).setCellValue("JOINT");
                   row.createCell((short)13).setCellValue("CLASS");
                   row.createCell((short)14).setCellValue("ACCOUNT_NO");
                   row.createCell((short)15).setCellValue("BANK_NAME");
                   row.createCell((short)16).setCellValue("BANK_BRANCH");
                   row.createCell((short)17).setCellValue("TOTAL_SHARE");
                   row.createCell((short)18).setCellValue("TAXFREE_SHARE");
                   row.createCell((short)19).setCellValue("LIABLE_TO_TAX");
                   row.createCell((short)20).setCellValue("SPECIAL_TAX");
                   row.createCell((short)21).setCellValue("RESIDENT");
                   row.createCell((short)22).setCellValue("NATIONALITY");
                   row.createCell((short)23).setCellValue("NOMINEE_NO");
                   row.createCell((short)24).setCellValue("ACTIVE");
                   row.createCell((short)25).setCellValue("TO_CHAR(ENTRY_DATE,'YYYY/MM/DD')");
                   row.createCell((short)26).setCellValue("LEAVE_DATE");
                   }
                int i=1;
           while(cm.toNext())
                        {
                            FNO = cm.getColumnS("FOLIO_NO");
                            PREFIX = cm.getColumnS("PREFIX");
                            SHNAME = cm.getColumnS("NAME");
                            SURNAME = cm.getColumnS("SURNAME");
                            RELATION = cm.getColumnS("RELATION");
                            FATHER = cm.getColumnS("FATHER");
                            ADDRESS1 = cm.getColumnS("ADDRESS1");
                            ADDRESS2 = cm.getColumnS("ADDRESS2");
                            ADDRESS3 = cm.getColumnS("ADDRESS3");
                            ADDRESS4 = cm.getColumnS("ADDRESS4");
                            ZIP = cm.getColumnS("ZIP");
                            OCCUPATION = cm.getColumnS("OCCUPATION");
                            JOINT = cm.getColumnS("JOINT");
                            CLASS=cm.getColumnS("CLASS");
                            ACCOUNT_NO=cm.getColumnS("ACCOUNT_NO");
                            BANK_NAME=cm.getColumnS("BANK_NAME");
                            BANK_BRANCH=cm.getColumnS("BANK_BRANCH");
                            TOTAL_SHARE=cm.getColumnS("TOTAL_SHARE");
                            TAXFREE_SHARE=cm.getColumnS("TAXFREE_SHARE");
                            LIABLE_TO_TAX= cm.getColumnS("LIABLE_TO_TAX");
                            SPECIAL_TAX=cm.getColumnS("SPECIAL_TAX");
                            SMSRESIDENT=cm.getColumnS("RESIDENT");
                            SMSNATIONALITY=cm.getColumnS("NATIONALITY");
                            NOMINEE_NO=cm.getColumnS("NOMINEE_NO");
                            ACTIVE=cm.getColumnS("ACTIVE");
                            TO_CHARDATE=cm.getColumnS("TO_CHAR(ENTRY_DATE,'YYYY/MM/DD')");
                            LEAVE_DATE=cm.getColumnS("LEAVE_DATE");
                            //For CDBL
                            BOID= cm.getColumnS("BO_ID");
                            NAMEFHODER= cm.getColumnS("NAME_FIRSTHOLDER");
                            NAMECPERSON=cm.getColumnS("NAME_CONTACT_PERSON");
                            ADDRESS= cm.getColumnS("ADDRESS");
                            CITY=cm.getColumnS("CITY");
                            STATE= cm.getColumnS("STATE");
                            POSTCODE=cm.getColumnS("POSTAL_CODE");
                            COUNTRY=POSTCODE=cm.getColumnS("COUNTRY");
                            Ph= cm.getColumnS("PHONE");
                            FAX=cm.getColumnS("FAX");
                            EMAIL=cm.getColumnS("EMAIL");
                            TFSHARE=cm.getColumnS("TAX_FREE_SHARE");
                            LTTAX=cm.getColumnS("LIABLE_TO_TAX");
                            STAX=cm.getColumnS("SPECIAL_TAX");
                            RESIDENT=cm.getColumnS("RESIDENT");
                            NATIONALITY=cm.getColumnS("NATIONALITY");
                            C_LASS=cm.getColumnS("CLASS");
                            PF_DATE=cm.getColumnS("PF_BDATE");

                          HSSFRow rowi = sheet.createRow((short)i);
                          if (TypeVal.equalsIgnoreCase("SMS")){
                            rowi.createCell((short)0).setCellValue(FNO);
                            rowi.createCell((short)1).setCellValue(PREFIX);
                            rowi.createCell((short)2).setCellValue(SHNAME);
                            rowi.createCell((short)3).setCellValue(SURNAME);
                            rowi.createCell((short)4).setCellValue(RELATION);
                            rowi.createCell((short)5).setCellValue(FATHER);
                            rowi.createCell((short)6).setCellValue(ADDRESS1);
                            rowi.createCell((short)7).setCellValue(ADDRESS2);
                            rowi.createCell((short)8).setCellValue(ADDRESS3);
                            rowi.createCell((short)9).setCellValue(ADDRESS4);
                            rowi.createCell((short)10).setCellValue(ZIP);
                            rowi.createCell((short)11).setCellValue(OCCUPATION);
                            rowi.createCell((short)12).setCellValue(JOINT);
                            rowi.createCell((short)13).setCellValue(CLASS);
                            rowi.createCell((short)14).setCellValue(ACCOUNT_NO);
                            rowi.createCell((short)15).setCellValue(BANK_NAME);
                            rowi.createCell((short)16).setCellValue(BANK_BRANCH);
                            rowi.createCell((short)17).setCellValue(TOTAL_SHARE);
                            rowi.createCell((short)18).setCellValue(TAXFREE_SHARE);
                            rowi.createCell((short)19).setCellValue(LIABLE_TO_TAX);
                            rowi.createCell((short)20).setCellValue(SPECIAL_TAX);
                            rowi.createCell((short)21).setCellValue(SMSRESIDENT);
                            rowi.createCell((short)22).setCellValue(SMSNATIONALITY);
                            rowi.createCell((short)23).setCellValue(NOMINEE_NO);
                            rowi.createCell((short)24).setCellValue(ACTIVE);
                            rowi.createCell((short)25).setCellValue(TO_CHARDATE);
                            rowi.createCell((short)26).setCellValue(LEAVE_DATE);
                          }
                           if (TypeVal.equalsIgnoreCase("CDBL")){
                             rowi.createCell((short)0).setCellValue(BOID);
                             rowi.createCell((short)1).setCellValue(NAMEFHODER);
                             rowi.createCell((short)2).setCellValue(NAMECPERSON);
                             rowi.createCell((short)3).setCellValue(ADDRESS);
                             rowi.createCell((short)4).setCellValue(CITY);
                             rowi.createCell((short)5).setCellValue(STATE);
                             rowi.createCell((short)6).setCellValue(POSTCODE);
                             rowi.createCell((short)7).setCellValue(COUNTRY);
                             rowi.createCell((short)8).setCellValue(Ph);
                             rowi.createCell((short)9).setCellValue(FAX);
                             rowi.createCell((short)10).setCellValue(EMAIL);
                             rowi.createCell((short)11).setCellValue(TFSHARE);
                             rowi.createCell((short)12).setCellValue(LTTAX);
                             rowi.createCell((short)13).setCellValue(STAX);
                             rowi.createCell((short)14).setCellValue(RESIDENT);
                             rowi.createCell((short)15).setCellValue(NATIONALITY);
                             rowi.createCell((short)16).setCellValue(C_LASS);
                             rowi.createCell((short)17).setCellValue(TOTAL_SHARE);
                             rowi.createCell((short)18).setCellValue(PF_DATE);
                           }
                            i++;
                           }


 if (isConnect)
  {
    cm.takeDown();
  }
    // Write the output to a file
    String savepath="";
    ServletContext sc = getServletConfig().getServletContext();
    //Code for creating a folder
   String path=getServletContext().getRealPath("/");
   File f1 = new File(path + "/Excel Export");
   if(!f1.exists())
   {
     f1.mkdir();
   }
   savepath = sc.getRealPath("/") +"Excel Export/"+"DataSource.xls";

    FileOutputStream fileOut = new FileOutputStream(savepath);
    wb.write(fileOut);
    fileOut.close();
    fileOut=null;
    sheet=null;
    wb=null;
%>
<%
   String tempURL =  request.getContextPath() +"/Excel Export/"+"DataSource.xls";
   System.out.println(tempURL);
%>
<script language="javascript">
showPrintPreview6('<%=tempURL%>');
    location = "<%=request.getContextPath()%>/jsp/Others/OthersCircularLetter.jsp";
</script>
</body>
</html>
