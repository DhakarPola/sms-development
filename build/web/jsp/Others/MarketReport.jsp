<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : June 2006
'Page Purpose  : Generates Share Market Reports for a time period
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Market Report</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function setmarketvalues(mvalue)
{
  if (mvalue == "Others")
  {
   document.all.marketspan1.style.display = '';
   document.all.marketspan2.style.display = '';
   document.all.marketspan3.style.display = '';
  }
  else
  {
   document.all.marketspan1.style.display = 'none';
   document.all.marketspan2.style.display = 'none';
   document.all.marketspan3.style.display = 'none';
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  SelectValidate('marketselect','- Market Name (Option must be entered)');
  if (document.all.marketselect.value == "Others")
  {
    BlankValidate('omname','- Other Market Name (Option must be entered)');
  }
  BlankValidate('datefrom','- Period From (Option must be entered)');
  BlankValidate('dateto','- Period To (Option must be entered)');

  if (count == 0)
  {
    document.forms[0].submit();
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
      String MarketName = String.valueOf(request.getParameter("MarketName"));
      String FromDate = String.valueOf(request.getParameter("FromDate"));
      String ToDate = String.valueOf(request.getParameter("ToDate"));
      if (String.valueOf(MarketName).equals("null"))
            MarketName = "";
      if (String.valueOf(FromDate).equals("null"))
            FromDate = "";
      if (String.valueOf(ToDate).equals("null"))
            ToDate = "";

%>

<form action="<%=request.getContextPath()%>/jsp/Others/SeeMarketReport.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Market Report</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Market</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
         <select name="marketselect" class="SL1TextFieldListBox" onchange="setmarketvalues(document.all.marketselect.value)">
            <option>--- Please Select ---</option>
            <option value="CDBL" <%if(MarketName.equalsIgnoreCase("CDBL")){%>Selected<%}%>>CDBL</option>
            <option value="CSE" <%if(MarketName.equalsIgnoreCase("CSE")){%>Selected<%}%>>CSE</option>
            <option value="DSE" <%if(MarketName.equalsIgnoreCase("DSE")){%>Selected<%}%>>DSE</option>
            <option value="SEC" <%if(MarketName.equalsIgnoreCase("SEC")){%>Selected<%}%>>SEC</option>
            <option value="Others" <%if(MarketName.equalsIgnoreCase("Others")){%>Selected<%}%>>Others</option>
         </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8"><SPAN id="marketspan1" style="display:none">&nbsp;&nbsp;&nbsp;&nbsp;Enter Market Name</span>
        </div></th>
        <td><SPAN id="marketspan2" style="display:none">:</span></td>
        <td><div align="left">
          <SPAN id="marketspan3" style="display:none"><input type="text" name="omname" class="SL2TextField"></span>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Period From</div></th>
        <td>:</td>
        <td><div align="left">
         <input name="datefrom" type=text id="datefrom" value="<%=FromDate%>" maxlength="10" class="SL2TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('datefrom','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Period To</div></th>
        <td>:</td>
        <td><div align="left">
         <input name="dateto" type=text id="dateto" value="<%=ToDate%>" maxlength="10" class="SL2TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('dateto','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="100%" scope="row">
            <div align="center">
 	      <img name="B1" src="<%=request.getContextPath()%>/images/btnCreateReport.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnCreateReportOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnCreateReport.gif'">
            </div></th>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
