<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : October 2005
'Page Purpose  : Save new Bank.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Bank</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String BankName = String.valueOf(request.getParameter("bname"));
  String BankAddress = String.valueOf(request.getParameter("baddress"));
  String BranchName = String.valueOf(request.getParameter("brname"));
  String BContact = String.valueOf(request.getParameter("bcontact"));
  String BTelephone = String.valueOf(request.getParameter("btel"));
  String BFax = String.valueOf(request.getParameter("bfax"));
  String BMobile = String.valueOf(request.getParameter("bmob"));
  String BEmail = String.valueOf(request.getParameter("bemail"));

  BankName=cm.replace(BankName,"'","''");
  BankAddress=cm.replace(BankAddress,"'","''");
  BranchName=cm.replace(BranchName,"'","''");
  BContact=cm.replace(BContact,"'","''");
  BTelephone=cm.replace(BTelephone,"'","''");
  BFax=cm.replace(BFax,"'","''");
  BMobile=cm.replace(BMobile,"'","''");
  BEmail=cm.replace(BEmail,"'","''");

  String nbank = "call ADD_Bank('" + BankName + "', '" + BranchName + "', '" + BankAddress + "', '" + BContact + "', '" + BTelephone + "', '" + BMobile + "','" + BFax + "','" + BEmail + "')";
  boolean b = cm.procedureExecute(nbank);

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added Bank Information')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Bank/AllBanks.jsp";
</script>
</body>
</html>
