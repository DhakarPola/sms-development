<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : October 2005
'Page Purpose  : Edits Bank Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}
%>

<html>
<head>
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   String bankname = request.getParameter("bname1");
   String Sbankid = request.getParameter("bid1");
   int bankid = Integer.parseInt(Sbankid);

   String branchname = "";
   String bankaddress = "";
   String bankcontact = "";
   String banktelephone = "";
   String bankmobile = "";
   String bankfax = "";
   String bankemail = "";

   String query1 = "SELECT * FROM BANK_VIEW WHERE BANK_ID = " + bankid + " AND BANK_NAME = '"+bankname+ "'";
   cm.queryExecute(query1);

   while(cm.toNext())
     {
       branchname = cm.getColumnS("BANK_BRANCH");
       bankaddress = cm.getColumnS("BANK_ADDRESS");
       bankcontact = cm.getColumnS("CONTACT_PERSON");
       banktelephone = cm.getColumnS("TELEPHONE");
       bankmobile = cm.getColumnS("MOBILE");
       bankfax = cm.getColumnS("FAX");
       bankemail = cm.getColumnS("EMAIL");

       if (String.valueOf(branchname).equals("null"))
         branchname = "";
       if (String.valueOf(bankaddress).equals("null"))
         bankaddress = "";
       if (String.valueOf(bankcontact).equals("null"))
         bankcontact = "";
       if (String.valueOf(banktelephone).equals("null"))
         banktelephone = "";
       if (String.valueOf(bankmobile).equals("null"))
         bankmobile = "";
       if (String.valueOf(bankfax).equals("null"))
         bankfax = "";
       if (String.valueOf(bankemail).equals("null"))
         bankemail = "";
     }
%>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit Bank</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function gotobank()
{
  location = "<%=request.getContextPath()%>/jsp/Bank/AllBanks.jsp";
}

function gotodelete()
{
  if (confirm("Do you want to delete the Bank Information?"))
  {
    location = "<%=request.getContextPath()%>/jsp/Bank/DeleteBank.jsp?bid2=<%=Sbankid%>";
  }
}

function formconfirm()
{
  if (confirm("Do you want to change the Bank Information?"))
  {
    return true;
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('bname','- Bank Name (Option must be entered)');
  BlankValidate('brname','- Branch Name (Option must be entered)');
  BlankValidate('baddress','- Address (Option must be entered)');

  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">

<form action="UpdateBank.jsp" method="get" enctype="multipart/form-data" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Bank Information</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Name</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="bname" class="SL2TextField" value="<%=bankname%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Branch Name</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="brname" class="SL2TextField" value="<%=branchname%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Address:</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left">
           <textarea name="baddress" cols="20" rows="3" wrap="VIRTUAL" id="baddress" class="ML9TextField"><%=bankaddress%></textarea>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Contact Person </div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="bcontact" class="SL2TextField" value="<%=bankcontact%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Telephone</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="btel" class="SL2TextField" value="<%=banktelephone%>" onkeypress="keypressOnNumberFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Mobile</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="bmob" class="SL2TextField" value="<%=bankmobile%>" onkeypress="keypressOnNumberFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Fax</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="bfax" class="SL2TextField" value="<%=bankfax%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Email</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="bemail" class="SL2TextField" value="<%=bankemail%>">
        </div></td>
      </tr>
  </table>

  <!--HIDDEN FIELDS-->
  <input type="hidden" name="fbankid" value="<%=Sbankid%>">

  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="43%" scope="row">
            <div align="right">
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
            </div></th>
        <td width="57%">
	          <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="gotobank()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
        </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
