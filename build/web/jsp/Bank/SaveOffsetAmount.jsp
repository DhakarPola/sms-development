<script>
/*
'******************************************************************************************************************************************
'Script Author : Rahat
'Creation Date : September 2010
'Page Purpose  : Save Offset Amount.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>

<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Marge Account</title>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();

   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

     String description=String.valueOf(request.getParameter("description"));
     String offsetamount=String.valueOf(request.getParameter("offsetamount"));
     double offsetamountDuble=Double.parseDouble(offsetamount);
      String SqlUpdate="Update OFFSETAMOUNT set ISACTIVE='F'";
      String SqlInsert="INSERT INTO OFFSETAMOUNT VALUES((SELECT NVL(MAX(offsetid),0)+1 from OFFSETAMOUNT),'"+description+"',"+offsetamountDuble+",(Select sysdate from dual),'T')";

      try
      {
       cm.queryExecute(SqlUpdate);
        //System.out.println(SqlUpdate);
        cm.queryExecute(SqlInsert);
        //System.out.println(SqlInsert);
      }
      catch(Exception e){

        //System.out.println("catch="+SqlInsert);
      }
      //System.out.println("date_diff= "+date_diff);

    try{

      if (isConnect)
      {
        cm.takeDown();
      }
    }
    catch(Exception ex){
    }

%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Bank/OffsetAmount.jsp";
  </script>
</body>
</html>

