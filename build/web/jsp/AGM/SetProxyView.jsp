<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. kamruzzaman
'Creation Date : July 2007
'Page Purpose  : Generating Proxy List.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Proxy History</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function tonewdata()
{
  location = "<%=request.getContextPath()%>/jsp/AGM/NewProxyName.jsp"
}

//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
      if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

     String decdate = "";
     String dest=request.getContextPath()+"/ProxyReports.jsp";


   %>

     <span class="style7">
     <form name="" method="" action="">
     <img name="B4" src="<%=request.getContextPath()%>/images/btnNew.gif" onclick="tonewdata()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNewOn.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNew.gif'">
     <table width="100%" BORDER=1  cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
     <!--DWLayoutTable-->
     <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Proxy History</center></td></tr>
     </table>
     <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
     <tr bgcolor="#0044B0">
     <td width="50%" class="style9"><div align="center">Date</div></td>
     <td width="50%"><div align="center" class="style9">
     <div align="center">Total Proxy</div>
     </div></td>
     </tr>
     <div align="left">
     <%
      String query = "SELECT DISTINCT AGM_DATE FROM PROXY_VIEW ORDER BY AGM_DATE DESC";
     try
     {
     cm.queryExecute(query);
     while (cm.toNext())
     {
     decdate = cm.getColumnDT("AGM_DATE");
     String query1 = "SELECT * FROM PROXY_VIEW WHERE AGM_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY')";
     int invited = cm1.queryExecuteCount(query1);
     %>
     </div>
     <tr bgcolor="#E8F3FD">
     <td class="style13">
     <div align="left" class="style12">
              <div align="center"><a HREF="<%=dest%>?ddate=<%=decdate%>"><%=decdate%></div>
              </div>
      </td>
     <td class="style13"><div align="center" class="style12">
       <div align="center"><%=invited%>&nbsp;</div>
     </div></td>
     </tr>
     <%
     }
     }catch(Exception e){}
     %>
    </form>
    <%
     if (isConnect)
   {
     try
     {
       cm.takeDown();
       cm1.takeDown();
     }catch(Exception e){}
   }
   %>
</body>
</html>
