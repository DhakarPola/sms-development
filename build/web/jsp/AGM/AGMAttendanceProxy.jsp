<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Rahat Uddin
'Creation Date : August 2007
'Page Purpose  : Show all AGM Attendance with Proxy.
'******************************************************************************************************************************************
*/
</script>
<html>
<head>
<title>
AGMAttendance
</title>
<script>

</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All AGM Attendance</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<%@page language="java" import="java.io.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*"%>
<script language="javascript">
<!--

function goNextPrevious(pstart,pend,foliono,sdate)
{

  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/AGM/AGMAttendanceProxy.jsp?StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;
  thisurl = thisurl + "&searchFolio="+foliono;
  thisurl = thisurl + "&dateselect="+sdate;
  location = thisurl;

}

function closedocument()
{
	//history.back();
         location = "<%=request.getContextPath()%>/jsp/AGM/SetAGMAttendance.jsp";
}

function toreclaccept()
{
  count = 0;

  if (count == 0)
  {
    document.forms[0].action = "SaveAttendanceProxy.jsp";
    document.forms[0].submit();
  }
  else
  {
   ShowAllAlertMsg();
   return false;
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}


//-->

function putrecondate()
{
  //check1++
}
//-->


//#################################################################################
function keypresssubmit() {
 if (event.keyCode==13)  {
   SubmitThis();
  }
}
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
    .style19 {
	color: #0A2769;
	font-weight: bold;
    }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   int folio_state=0;
   int COUNT;
   int MAX_RECORDS=20;
   int typedate=0;
   String folio_no=String.valueOf(request.getParameter("searchFolio"));
   String agm_date=String.valueOf(request.getParameter("dateselect"));

   //System.out.println("agm_date= "+agm_date);
   //System.out.println("folio_no= "+folio_no);

   folio_no=folio_no.trim();

   String folioname = "";
   String folioaddress = "";
   String dest = "";
   String destproxy = "";
   String PreStatus = "";
   String SurName = "";
   String BOAddress = "";
   String BOProxyName="";
   String BOorFolio="";
   int rowcounter1 = 0;
   String selectQuery="";
   byte[] imgData1=null;
   Connection conn1=null;

   //Adding father's and mother's name // jahir 16.04.2013
   String father_name="";
   String mother_name="";
   String q="";

 //Used for NEXT/PREVIOUS
    String SStartingValue = String.valueOf(request.getParameter("StartValue"));
    String SEndingValue = String.valueOf(request.getParameter("EndValue"));


    if(SStartingValue == null || SStartingValue.equals("") || SStartingValue.equals("null")) SStartingValue="1";
    if(SEndingValue == null || SEndingValue.equals("") || SEndingValue.equals("null")) SEndingValue="0";



    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    int Chunk = MAX_RECORDS;
    EndingValue = StartingValue + Chunk - 1;
    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;

    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }

     agm_date=cm.replace(agm_date,"'","''");
     folio_no=cm.replace(folio_no,"'","''");

     if (String.valueOf(folio_no).equals("null"))
      folio_no = "";
   if(String.valueOf(agm_date).equals("null"))
     agm_date="";

   String query1="";
   //System.out.println("call setcontextsms('"+agm_date+"')");
  cm.procedureExecute("call setcontextsms('"+agm_date+"')");

  //System.out.println("Alter  : call setcontextsms('"+agm_date+"')");

     if (folio_no.length() <8)
     {
       //query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (select * from AGMATTENDANCE_Proxy_VIEW where AGMDATE=TO_DATE('" + agm_date + "','DD/MM/YYYY') order by BOID) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;
       query1 ="SELECT * FROM (SELECT div.*, rownum rnum FROM (Select  boid, name, address, surname, agmdate, nvl(px.proxy_name,' ') proxy_name,IS_PRESENT from agmattendance_view left outer join proxy px on agmattendance_view.boid=px.folio_or_bo and agmattendance_view.agmdate= px.agm_date where agmattendance_view.agmdate=TO_DATE('" + agm_date + "','DD/MM/YYYY')) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;
       //System.out.println("1===="+query1);
     }
     else if(folio_no.length()==8)
     {
       //query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (select * from AGMATTENDANCE_Proxy_VIEW where ( (CAST(BOID as NUMBER(20,0))>=CAST('"+folio_no+"' as NUMBER(20,0)) or CAST(FOLIO_BO as NUMBER(20,0))>=CAST('"+folio_no+"' as NUMBER(20,0))) and CAST(BOID as NUMBER(20,0))<99999999) and AGMDATE=TO_DATE('" + agm_date + "','DD/MM/YYYY') order by BOID) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;
        //query1 ="SELECT * FROM (SELECT div.*, rownum rnum FROM ((select * from AGMATTENDANCE_Proxy_VIEW where ( CAST(BOID as NUMBER(20,0))>=CAST('"+folio_no+"' as NUMBER(20,0)) and CAST(BOID as NUMBER(20,0))<99999999) and AGMDATE=TO_DATE('" + agm_date + "','DD/MM/YYYY')) UNION(select * from AGMATTENDANCE_Proxy_VIEW where ( CAST(FOLIO_BO as NUMBER(20,0))>=CAST('"+folio_no+"' as NUMBER(20,0))) and AGMDATE=TO_DATE('" + agm_date + "','DD/MM/YYYY'))) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;
        query1 ="SELECT * FROM (SELECT div.*, rownum rnum FROM (Select  boid, name, address, surname, agmdate, nvl(px.proxy_name,' ') proxy_name,IS_PRESENT from agmattendance_view left outer join proxy px on agmattendance_view.boid=px.folio_or_bo and agmattendance_view.agmdate= px.agm_date where agmattendance_view.agmdate=TO_DATE('" + agm_date + "','DD/MM/YYYY') and agmattendance_view.boid like '%"+folio_no+"') div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;
        //System.out.println("2===="+query1);
     }
     else if(folio_no.length()>8)
     {
       //query1 ="SELECT * FROM (SELECT div.*, rownum rnum FROM ((select * from AGMATTENDANCE_Proxy_VIEW where ( CAST(BOID as NUMBER(20,0))>=CAST('"+folio_no+"' as NUMBER(20,0))) and AGMDATE=TO_DATE('" + agm_date + "','DD/MM/YYYY')) UNION(select * from AGMATTENDANCE_Proxy_VIEW where ( CAST(FOLIO_BO as NUMBER(20,0))>=CAST('"+folio_no+"' as NUMBER(20,0))) and AGMDATE=TO_DATE('" + agm_date + "','DD/MM/YYYY'))) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;
       query1 ="SELECT * FROM (SELECT div.*, rownum rnum FROM (Select  boid, name, address, surname, agmdate, nvl(px.proxy_name,' ') proxy_name,IS_PRESENT from agmattendance_view left outer join proxy px on agmattendance_view.boid=px.folio_or_bo and agmattendance_view.agmdate= px.agm_date where agmattendance_view.agmdate=TO_DATE('" + agm_date + "','DD/MM/YYYY') and agmattendance_view.boid like '%"+folio_no+"') div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;
       //System.out.println("3===="+query1);
     }





   //Select  boid, name, address, surname, agmdate, px.proxy_name from agmattendance_view left outer join proxy px on agmattendance_view.boid=px.folio_or_bo where agmattendance_view.agmdate=TO_DATE('02/03/2011','DD/MM/YYYY') order by px.proxy_name DESC

      System.out.println("-----------------query1---->"+query1);
      cm.queryExecute(query1);

     COUNT=0;
     while(cm.toNext())
     {
       COUNT++;
     }

       cm.queryExecute(query1);
        //System.out.println("count="+COUNT);
     %>
     <span class="style7">
       <form method="post" action="AGMAttendanceProxy.jsp?dateselect=<%=agm_date%>">

       <SPAN id="dprint">
         <img name="B4" src="<%=request.getContextPath()%>/images/btnAccept.gif" onclick="toreclaccept()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAcceptR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnAccept.gif'">
           <img name="B7" src="<%=request.getContextPath()%>/images/btnClose.gif" onclick="closedocument()" onMouseOver="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
       </SPAN>

       <table width="100%" BORDER=1  cellpadding="0"  style="border-collapse:collapse;" bordercolor="#06689E">
         <!--DWLayoutTable-->
         <tr><td bgcolor="#0044B0" class="style7" height="30"><center>AGM Attendance Verification (Proxy)</center></td></tr>
         <tr>
           <td>
             <table width="100%"  border="0" cellpadding="0" cellspacing="0">
               <tr bgcolor="#E8F3FD" height="35">
                 <td width="10%"></td>
                 <td width="6%"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goNextPrevious(<%=pStartingValue%>,<%=pEndingValue%>,'<%=folio_no%>','<%=agm_date %>')"></td>
                 <td width="6%" align="right"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNextPrevious(<%=nStartingValue%>,<%=nEndingValue%>,'<%=folio_no%>','<%=agm_date%>')"></td>
                 <td width="43%" class="style19" align="right">Enter BO ID/Folio No:&nbsp;&nbsp;</td>
                 <td width="20%"><input name="searchFolio" type="text" onkeypress="keypressOnNumberFld()" onkeyup="keypresssubmit();">
                       <input type="hidden" name="hfolio" value="<%=folio_no%>"/>
                 </td>
                 <td width="15%"><img src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="SubmitThis()"></td>
               </tr>
             </table>
           </td>
         </tr>
       </table>
       <table width="100%" border="1" cellpadding="5" cellspacing="0" style="border-collapse: collapse" bordercolor="0044B0">
         <tr bgcolor="#0044B0">
           <td width="10%" class="style9" align="center">Folio No</td>
           <td width="14%"><div align="center" class="style9">
             <div align="center">Name</div></div>
           </td>
           <td width="12%"><div align="center" class="style9">
             <div align="center">Father's Name</div></div>
           </td>
           <td width="12%"><div align="center" class="style9">
             <div align="center">Mother's Name</div></div>
           </td>
           <td width="15%"><div align="center" class="style9">
             <div align="center">Address</div></div>
           </td>
           <td width="12%"><div align="center" class="style9">
             <div align="center">Proxy Name</div></div>
           </td>
           <td width="15%"><div align="center" class="style9">
             <div align="center">Signature</div></div>
           </td>
           <td width="5%"><div align="center" class="style9">
             <div align="center">Present</div></div>
           </td>
           <td width="5%"><div align="center" class="style9">
             <div align="center">Attendance</div></div>
           </td>
         </tr>
         <div align="left">
         <%
         folio_state=0;
         while(cm.toNext() && COUNT!=0)
         {
           rowcounter1++;
           folio_state++;
           folio_no=cm.getColumnS("BOID");
           //System.out.println("-----------------folio_no---->"+folio_no);
           //getting father's and mother's name from BOADDRESS and SHAREHOLDER table // jahir 16.04.2013
           if(folio_no.length()>8){
             q="SELECT NVL(FAHER_NAME,'') AS FATHER_NAME, NVL(MOTHER_NAME,'') AS MOTHER_NAME FROM BOADDRESS WHERE BO_ID='"+folio_no+"'";
             //System.out.println("----------if-------folio_no--q-->"+q);
           }else{
             q="SELECT NVL(FATHER,'') AS FATHER_NAME, '' AS MOTHER_NAME FROM SHAREHOLDER WHERE FOLIO_NO='"+folio_no+"'";
             //System.out.println("----------else-------folio_no--q-->"+q);
           }
           System.out.println("----------if---else----folio_no--q-->"+q);
           cm1.queryExecute(q);
           while(cm1.toNext()){
             father_name="";
             mother_name="";
             father_name=cm1.getColumnS("FATHER_NAME");
             mother_name=cm1.getColumnS("MOTHER_NAME");
             System.out.println("----------father_name-->"+father_name+"<------mother_name------>"+mother_name+"<-------");
           }

           BOorFolio=cm.getColumnS("FOLIO_BO");
           //System.out.println("-----------------BOorFolio---->"+BOorFolio);
           folioname = cm.getColumnS("NAME");
           BOAddress = cm.getColumnS("ADDRESS");
           BOProxyName = cm.getColumnS("PROXY_NAME");
           PreStatus = cm.getColumnS("IS_PRESENT");

           //System.out.println("folio_no= "+folio_no);
           //System.out.println("BOorFolio= "+BOorFolio);
           //System.out.println("folioname= "+folioname);
           //System.out.println("BOAddress= "+BOAddress);
           //System.out.println("BOProxyName= "+BOProxyName);
           //System.out.println("PreStatus= "+PreStatus);


           folio_no=folio_no.trim();

           if (String.valueOf(PreStatus).equals("null"))
           	PreStatus = "F";
           if(String.valueOf(folio_no).equals("null"))
           	folio_no="";
           if(String.valueOf(folioname).equals("null"))
           	folioname="";
           if(String.valueOf(father_name).equals("null"))
           	father_name="";
           if(String.valueOf(mother_name).equals("null"))
           	mother_name="";
           if(String.valueOf(BOAddress).equals("null"))
           	BOAddress="";
            if(String.valueOf(BOProxyName).equals("null"))
           	BOProxyName="";
            if(String.valueOf(BOorFolio).equals("null"))
                BOorFolio="";
           folio_no=cm.replace(folio_no,"'","''");
           folioname=cm.replace(folioname,"'","''");
           BOAddress=cm.replace(BOAddress,"'","''");
           PreStatus=cm.replace(PreStatus,"'","''");

           dest = request.getContextPath() + "/jsp/AGM/EditAttendance.jsp";
           destproxy = request.getContextPath() + "/jsp/AGM/EditAttendanceProxy.jsp";
           //dest=destproxy;
           if (!folio_no.equals("null"))
           {
             if (String.valueOf(folioname).equals("null"))
             folioname = "";
             if (String.valueOf(PreStatus ).equals("null"))
             PreStatus = "F";
             %>
             </div>
             <tr bgcolor="#E8F3FD">
               <td valign="top" ><div align="center" class="style10">
                 <div align="center" class="style10">
                   <span class="style13">
                     <a href="<%=dest%>?fno=<%=folio_no%>&agmdate=<%=agm_date%>"><%=folio_no%><br><br><%=BOorFolio%></a>               &nbsp;
                     <%
                     if(folio_state==COUNT)
                     {
                       %>
                       <input type="hidden" name="svalue" value="<%=StartingValue%>">
                       <input type="hidden" name="evalue" value="<%=EndingValue%>">
                       <%}%>
                   </span></div>
                 </div></td>
                 <td valign="top" class="style13"><div align="left" class="style12">
                   <div align="left"><a HREF="<%=dest%>?fno=<%=folio_no%>&agmdate=<%=agm_date%>"><%=folioname%>&nbsp;</div>
                   </div></td>

                   <td valign="top" class="style13"><div align="left" class="style12">
                     <div align="left"><a HREF="<%=dest%>?fno=<%=folio_no%>&agmdate=<%=agm_date%>"><%=father_name%>&nbsp;</div>
                     </div></td>
                   <td valign="top" class="style13"><div align="left" class="style12">
                     <div align="left"><a HREF="<%=dest%>?fno=<%=folio_no%>&agmdate=<%=agm_date%>"><%=mother_name%>&nbsp;</div>
                     </div></td>

                   <td valign="top" class="style13"><div align="left" class="style12">
                     <div align="left"><a HREF="<%=dest%>?fno=<%=folio_no%>&agmdate=<%=agm_date%>"><%=BOAddress%>&nbsp;</div>
                     </div></td>
                     <td valign="top" class="style13"><div align="left" class="style12">
                     <div align="left"><a HREF="<%=destproxy%>?fno=<%=folio_no%>&agmdate=<%=agm_date%>"><font color="red"><%=BOProxyName%></font></a>&nbsp;</div>
                     </div></td>
                     <td valign="top"><div align="left" class="style9">
                     <%
                     if(folio_no.length()>10)
                     {

                       %>
                       <font color="Blue">
                       <img alt="Not available" width="150" height="40" title="Share Holder Signature" src="<%=request.getContextPath()%>/Bosignature/<%=folio_no%>0101.jpg"/>
                       </font>
                       <%
                       }
                       else{
                         imgData1 =null;
                         selectQuery="SELECT SIGNATURE FROM SHAREHOLDER WHERE folio_no = '"+folio_no +"'";
                         try
                         {
                           conn1 = cm.getConnection();
                           conn1.setAutoCommit (false);
                           imgData1 =  cm.getPhoto(  conn1, selectQuery );
                         }
                         catch(Exception ex)
                         {
                           conn1=null;
                         }
                         conn1=null;

                        if(imgData1.length!=0)
                        {
                           %>

                             <img alt="Share Holder Signature" title="Share Holder Signature" width="150" height="40" src="image.jsp?selectQuery=<%=selectQuery%>" />

                            <%
                           }
                           else
                           {
                             %>
                                <center>
                                <font color="Blue">
                                   <b>Not available</b>
                                </font>
                                </center>
                             <%
                             }
                           }
                           %>
                           </div></td>
                           <td class="style13"><div align="center" class="style12">
                             <div align="center"><%
                             if (PreStatus.equals("T"))
                             {
                               %>
                               <img name="B2" src="<%=request.getContextPath()%>/images/icoCheck.gif">
                               <%
                               }
                               else
                               {
                                 %>
                                 <img name="B3" src="<%=request.getContextPath()%>/images/icoCross.gif">
                                 <%
                                 }
                                 %>
                                 </div>
</div></td>
<td class="style13"><div align="center" class="style12">
<%
if (!PreStatus.equalsIgnoreCase("T"))
{
  %>
  <input type="checkbox" name="selectingbox<%=rowcounter1%>" value="<%=folio_no%>" onclick="putrecondate()">
  <input type="hidden" name="agmdate" value="<%=agm_date%>">
  <%
  }
  %>
  </td>
             </tr>
             <div align="left" class="style13">
             <%
             }
           }


   if (isConnect)
   {
     cm.takeDown();
   }

   if (isConnect1)
   {
     cm1.takeDown();
   }
  %>
         </div>
		  <!--HIDDEN FIELDS-->
            <input type="hidden" name="rowcounter1" value="<%=rowcounter1%>">

  </table>
  </form>
</body>
</html>

