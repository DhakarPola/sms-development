<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.io.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*"%>
<jsp:useBean id="cm"  class="batbsms.conBean" />
<%
             boolean isConnect = cm.connect();
             if(isConnect==false){
               %>
               <jsp:forward page="ErrorMsg.jsp" >
                 <jsp:param name="ErrorTitle" value="Connection Failure" />
                 <jsp:param name="ErrorHeading" value="Connection Problem" />
                 <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
               </jsp:forward>
               <%
               }
              String selectQuery = request.getParameter("selectQuery");
            //  String selectQuery="SELECT SIGN FROM SIGNATURE WHERE SIG_ID = "+request.getParameter("Ssigid");
              //System.out.println(selectQuery);
              Connection conn=null;
              OutputStream o=null;
              byte[] imgData2=null;
              try
                {
                   conn = cm.getConnection();
                   conn.setAutoCommit (false);
                   // get the image from the database
                   imgData2 =  cm.getPhoto(conn, selectQuery) ;
                   // display the image
                   response.setContentType("image/gif");
                   o = response.getOutputStream();
                   o.write(imgData2);
                   o.flush();
                   o.close();
                   o=null;

                }
                catch (Exception e)
                {

                  conn=null;
                }
                conn=null;

                if (isConnect)
                {
                  cm.takeDown();
                }
%>
