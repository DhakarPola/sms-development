<script>
/*
'******************************************************************************************************************************************
'Script Author :Md Rahat Uddin
'Creation Date : August 2007
'Page Purpose  : Save Attendance Information Proxy.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save AGM Attendance</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String rowcounter = String.valueOf(request.getParameter("rowcounter1"));
  String agm_date=String.valueOf(request.getParameter("agmdate"));
  String Fldname = "";
  String updatequery="";
  String folio="";
  String folio_return="";
  int irowcounter = Integer.parseInt(rowcounter);

  for (int i=1;i<irowcounter+1;i++)
  {
    if(i==1)
    {
      folio="selectingbox1";
      folio_return=String.valueOf(request.getParameter(folio));
    }
    Fldname = "selectingbox" + String.valueOf(i);
    String BoxValue = String.valueOf(request.getParameter(Fldname));

    BoxValue=cm.replace(BoxValue,"'","''");

    if (String.valueOf(BoxValue).equals("null"))
    {
      BoxValue = "";
    }
    if (!BoxValue.equalsIgnoreCase(""))
    {
                String ispresent="T";
		//updatequery="UPDATE AGMATTENDANCE SET IS_PRESENT='T' where boid='" + BoxValue + "'and AGMDATE=TO_DATE('" + agm_date + "','DD/MM/YYYY')";
                updatequery="call UPDATE_AGMATTENDANCE ('" + ispresent +"','" + agm_date + "','" + BoxValue + "')";
                //cm.queryExecute(updatequery);
                boolean b = cm.procedureExecute(updatequery);
    }
  }
   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
    location = "<%=request.getContextPath()%>/jsp/AGM/AGMAttendanceProxy.jsp?dateselect=<%=agm_date%>&searchFolio=<%=folio_return%>";
</script>
</body>
</html>
