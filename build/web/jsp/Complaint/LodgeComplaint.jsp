<script>
/*
'******************************************************************************************************************************************
'Script Author : Mohammad Ashraful Islam
'Updated By    : Renad Hakim
'Creation Date : November 2006
'Page Purpose  : Form to add new Complaints.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Lodge Complaint</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--

function formconfirm()
{
  if (confirm("Do you want to lodge the complaint?"))
  {
    return true;
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('csubject','- Subject of Complaint (Option must be entered)');
  BlankValidate('decdate','- Date of Complaint (Option must be entered)');
  BlankValidate('foliono','- Folio No. (Option must be entered)');
  BlankValidate('compbody','- Complaints Summary (Option must be entered)');
  SelectValidate('declaredby','- Complaints Status (Option must be entered)');

  if (count == 0)
  {
    if (formconfirm())
    {
      var regexp = /\\/g;
      document.forms[0].filepath.value = document.forms[0].filepath.value.replace(regexp,"\\\\");
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
%>

<form action="SaveComplaint.jsp" method="post" name="FileForm" enctype="multipart/form-data">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Lodge Complaint</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="85%"  border="0" cellpadding="5">
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;Subject</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="csubject" id="csubject" value="" class="SL68TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;Date</div></th>
        <td>:</td>
        <td><div align="left">
         <input name="decdate" type=text id="decdate" value="" maxlength="10" class="SL77TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('decdate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;Folio No./BO ID</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="foliono" id="foliono"  class="SL68TextField" onkeypress="keypressOnNumberFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;Shareholder Name</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="shname" id="shname" class="SL68TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;Complaint</div></th>
        <td valign="top">:</td>
        <td><div align="left">
          <textarea name="compbody" id="compbody" cols="2" wrap="VIRTUAL" class="ML10TextField" ></textarea>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;Assigned Person</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="apname" id="apname" class="SL68TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;Timeline</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="tline" id="tline" class="SL68TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row"width="54%"><div align="left" class="style8">&nbsp;&nbsp;Status</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="declaredby" id="declaredby" class="SL2TextFieldListBox">
            <option>--- Please Select ---</option>
            <option value="Recieved">Recieved</option>
            <option value="In Progress">In Progress</option>
            <option value="On Hold">On Hold</option>
            <option value="Completed">Completed</option>
            <option value="Rejected">Rejected</option>
         </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;Comments</div></th>
        <td valign="top">:</td>
        <td><div align="left">
          <textarea name="commentsbody" id="commentsbody" cols="2" wrap="VIRTUAL" class="ML10TextField" ></textarea>
        </div></td>
      </tr>
      <tr>
        <th scope="row"width="54%"><div align="left" class="style8">&nbsp;&nbsp;Attachments </div></th>
            <td width="1%">:</td>

            <td width="45%"><input name="filepath" type="file" id="filepath" class="SL68TextField"></td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;Comments About Attachments</div></th>
        <td valign="top">:</td>
        <td><div align="left">
          <textarea name="AttComment" id="AttComment" cols="2" wrap="VIRTUAL" class="ML10TextField" ></textarea>
        </div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="45%" BORDER=0  cellpadding="10" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <td width="45%" align="right">
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
      </td>
        <td width="45%">
	          <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
      </td>
      </tr>
</table>

</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
