<script>
/*
'******************************************************************************************************************************************
'Script Author : Md.Kamruzzaman
'Creation Date : November 2006
'Page Purpose  : Saves update of Complaint.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<%@ page language="java" import="oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*,org.apache.commons.fileupload.*,java.io.*,java.util.*,java.util.List,java.util.Vector,java.util.Date,java.text.SimpleDateFormat,java.lang.String" %>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update Complaint</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

      String ccomid="";//String.valueOf(request.getParameter("bcomid"));
      String csubject ="";//String.valueOf(request.getParameter("bsubject"));
      String cdate ="";// String.valueOf(request.getParameter("bdate"));
      String cfolio ="";// String.valueOf(request.getParameter("bfolio"));
      String cshareholder ="";// String.valueOf(request.getParameter("bshare"));
      String ccomplaint ="";// String.valueOf(request.getParameter("bcomplaint"));
      String cassigned ="";// String.valueOf(request.getParameter("bassign"));
      String ctime ="";// String.valueOf(request.getParameter("btime"));
      String cstatus ="";// String.valueOf(request.getParameter("bstatus"));
      String ccomment ="";// String.valueOf(request.getParameter("bcomment"));
      String rowcounter ="";// String.valueOf(request.getParameter("rowcounter1"));
      int rowcounterInt =0;
  String AComments ="";
  String FilePath="";
  String CurrentFile = "";
  String fname ="";

  File savedFile=null ;
  File fullFile=null ;

  DiskFileUpload u1 = new DiskFileUpload();
  if(request.getContentLength()>0)
  {
    List items1 = u1.parseRequest(request);
    List items2= items1;

    Iterator itr1 = items1.iterator();

    List items = items1;
    Iterator itr = items.iterator();

    FileItem item =null;// (FileItem) itr.next();
    String fieldName="";
    FileItem item1=null;
    while(itr1.hasNext())
    {
      item1 = (FileItem) itr1.next();
      fieldName = item1.getFieldName();
      if(fieldName.equals("bcomid")) {
        ccomid = item1.getString();
      }
      else if(fieldName.equals("bsubject")){
        csubject = item1.getString();
      }
      else if(fieldName.equals("bdate")){
        cdate = item1.getString();
      }
      else if(fieldName.equals("bfolio")){
        cfolio = item1.getString();
      }
      else if(fieldName.equals("bshare")){
        cshareholder = item1.getString();
      }
      else if(fieldName.equals("bcomplaint")){
        ccomplaint = item1.getString();
      }
      else if(fieldName.equals("bassign")){
        cassigned = item1.getString();
      }
      else if(fieldName.equals("btime")){
        ctime = item1.getString();
      }
      else if(fieldName.equals("bstatus")){
        cstatus = item1.getString();
      }
      else if(fieldName.equals("bcomment")){
        ccomment = item1.getString();
      }
      else if(fieldName.equals("filepath")){
        CurrentFile = item1.getName();
        fullFile  = new File(item1.getName());
        fname =  fullFile.getName();
        item = item1;
        FilePath =fullFile.getPath();
      }
      else if(fieldName.equals("AttComment")){
        AComments = item1.getString();
        if (String.valueOf(AComments).equals("null"))
        {
          AComments = "";
        }
      }
       else if(fieldName.equals("rowcounter1")){
        rowcounter = item1.getString();
        if (String.valueOf(rowcounter).equals("null"))
        {
          rowcounterInt = 0;
        }
        else{
          rowcounterInt=Integer.parseInt(rowcounter);
        }
      }

    }

    if(rowcounterInt>0){
      //Integer icount=1;
      //int test=icount.intValue()
      String DeleteQuery ="";

    itr1 = items2.iterator();
    items = items2;
    itr = items.iterator();

    item =null;// (FileItem) itr.next();
    fieldName="";
    item1=null;
    int icount=1;
    int Comp_Id=0;
    String gFieldId="";
    String NStr="";

    while(itr1.hasNext())
    {
      item1 = (FileItem) itr1.next();
      fieldName = item1.getFieldName();
      for(icount=1;icount<rowcounterInt+1;icount++){
        NStr=(String) Integer.toString(icount);
        gFieldId="selectingbox" + NStr;
        if(fieldName.equals(gFieldId)) {
          Comp_Id =Integer.valueOf( item1.getString()).intValue();//Integer.parseInt(item1.getString());
          DeleteQuery = "Delete FROM COMP_ATTACH where ATTACH_ID="+Comp_Id;
          //System.out.println(DeleteQuery);
          try
          {
            cm.queryExecute(DeleteQuery);
          }
          catch (Exception e)
          {

          }
        }
      }

    }


    }


  }

     // ccomid=cm.replace(ccomid,"'","''");
      csubject=cm.replace(csubject,"'","''");
      cdate=cm.replace(cdate,"'","''");
      cfolio=cm.replace(cfolio,"'","''");
      cshareholder=cm.replace(cshareholder,"'","''");
      ccomplaint=cm.replace(ccomplaint,"'","''");
      cassigned=cm.replace(cassigned,"'","''");
      ctime=cm.replace(ctime,"'","''");
      cstatus=cm.replace(cstatus,"'","''");
      ccomment=cm.replace(ccomment,"'","''");


  String ucom = "call UPDATE_COMPLAINT('" + ccomid + "','" + cdate + "','" + csubject + "', '" + cfolio + "','" + cshareholder + "','" + ccomplaint + "','" + cassigned + "','" + ctime + "','" + cstatus + "','" + ccomment + "')";
  try
  {
    boolean b = cm.procedureExecute(ucom);

  }
  catch (Exception e)
    {

    }
//For File Attatchment

  if(fname.length()>1){
    int MaxAttachId =0;
    String FindMaxAttachId = "SELECT nvl(MAX(ATTACH_ID),0) AS ATTACH_ID FROM COMP_ATTACH";
    try
    {
      cm.queryExecute(FindMaxAttachId);
      while(cm.toNext())
      {
        MaxAttachId = cm.getColumnI("ATTACH_ID");
      }
    }
    catch (Exception e)
    {

    }
    MaxAttachId=MaxAttachId+1;
    Connection conn1=null;
    conn1=cm.getConnection();
    // It's faster when auto commit is off
    try
    {
      conn1.setAutoCommit (false);
      Statement stmt = conn1.createStatement ();
      try {
        stmt.execute ("INSERT INTO COMP_ATTACH VALUES ("+MaxAttachId+","+ccomid+",EMPTY_BLOB(),'"+fname+"','"+AComments+"')");
      }
      catch (Exception e)
      {
      }
      // Select the BLOB
      ResultSet rset = stmt.executeQuery ("select ATTACHMENTS from COMP_ATTACH where ATTACH_ID="+MaxAttachId);
      if (rset.next ())
      {
        // Get the BLOB locator from the table
        BLOB blob = ((OracleResultSet)rset).getBLOB ("ATTACHMENTS");

        // Declare a file handler for the john.gif file
        //File binaryFile = new File (FilePath);
        // Create a FileInputStream object to read the contents of the GIF file
        FileInputStream istream = new FileInputStream (fullFile);
        // Create an OutputStram object to write the BLOB as a stream
        OutputStream ostream = blob.getBinaryOutputStream();
        // Create a tempory buffer
        byte[] buffer = new byte[1024];
        int length = 0;

        // Use the read() method to read the GIF file to the byte
        // array buffer, then use the write() method to write it to
        // the BLOB.
        while ((length = istream.read(buffer)) != -1)
        ostream.write(buffer, 0, length);
        // Close the inputstream and outputstream
        istream.close();
        ostream.close();

        // Check the BLOB size
        //System.out.println ("Number of bytes written  = "+blob.length());
      }
      conn1.setAutoCommit (true);
      // Close all resources
      rset.close();
      conn1.close();
    }
    catch (Exception e)
    {

    }
  }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Edited Complaint')";
  try{
    boolean ub = cm.procedureExecute(ulog);
  }
  catch (Exception e)
  {
  }
 if (isConnect)
  {
    try{
      cm.takeDown();
    }
    catch (Exception e)
    {
    }
  }
%>
<script language="javascript">
 location = "<%=request.getContextPath()%>/jsp/Complaint/ComplaintbyReference.jsp";
 //history.go(-2)
</script>
</body>
</html>
