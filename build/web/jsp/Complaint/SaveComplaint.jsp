<script>
/*
'******************************************************************************************************************************************
'Script Author : Mohammad Ashraful Islam
'Creation Date : November 2006
'Page Purpose  : Save Complaint.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<%@ page language="java" import="oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*,org.apache.commons.fileupload.*,java.io.*,java.util.*,java.util.List,java.util.Vector,java.util.Date,java.text.SimpleDateFormat" %>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Complaint</title>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String CompSubject ="";// String.valueOf(request.getParameter("csubject"));
  String LodgeDate ="";// String.valueOf(request.getParameter("decdate"));
  String FolioNo ="";// String.valueOf(request.getParameter("foliono"));
  String ShareholderName ="";// String.valueOf(request.getParameter("shname"));
  String ComplaintBody ="";// String.valueOf(request.getParameter("compbody"));
  String AssignedPerson ="";// String.valueOf(request.getParameter("apname"));
  String TimeLine ="";// String.valueOf(request.getParameter("tline"));
  String CompStatus ="";// String.valueOf(request.getParameter("declaredby"));
  String Comments ="";// String.valueOf(request.getParameter("commentsbody"));
  String AComments ="";
  String FilePath="";
  String CurrentFile = "";
  String fname ="";

  File savedFile=null ;
  File fullFile=null ;

  DiskFileUpload u1 = new DiskFileUpload();
  if(request.getContentLength()>0)
  {
    List items1 = u1.parseRequest(request);

    Iterator itr1 = items1.iterator();

    List items = items1;
    Iterator itr = items.iterator();

    FileItem item =null;// (FileItem) itr.next();
    String fieldName="";
    FileItem item1=null;
    while(itr1.hasNext())
    {
      item1 = (FileItem) itr1.next();
      fieldName = item1.getFieldName();
      if(fieldName.equals("csubject")) {
        CompSubject = item1.getString();
      }
      else if(fieldName.equals("decdate")){
        LodgeDate = item1.getString();
      }
      else if(fieldName.equals("foliono")){
        FolioNo = item1.getString();
      }
      else if(fieldName.equals("shname")){
        ShareholderName = item1.getString();
      }
      else if(fieldName.equals("compbody")){
        ComplaintBody = item1.getString();
      }
      else if(fieldName.equals("apname")){
        AssignedPerson = item1.getString();
      }
      else if(fieldName.equals("tline")){
        TimeLine = item1.getString();
      }
      else if(fieldName.equals("declaredby")){
        CompStatus = item1.getString();
      }
      else if(fieldName.equals("commentsbody")){
        Comments = item1.getString();
      }
      else if(fieldName.equals("filepath")){
        CurrentFile = item1.getName();
        fullFile  = new File(item1.getName());
        fname =  fullFile.getName();
        item = item1;
        FilePath =fullFile.getPath();
      }
      else if(fieldName.equals("AttComment")){
        AComments = item1.getString();
        if (String.valueOf(AComments).equals("null"))
        {
          AComments = "";
        }
      }
    }
  }

  CompSubject=cm.replace(CompSubject,"'","''");
  LodgeDate=cm.replace(LodgeDate,"'","''");
  FolioNo=cm.replace(FolioNo,"'","''");
  ShareholderName=cm.replace(ShareholderName,"'","''");
  ComplaintBody=cm.replace(ComplaintBody,"'","''");
  AssignedPerson=cm.replace(AssignedPerson,"'","''");
  TimeLine=cm.replace(TimeLine,"'","''");
  CompStatus=cm.replace(CompStatus,"'","''");
  Comments=cm.replace(Comments,"'","''");

  String FindLastId = "SELECT MAX(COM_ID) AS TOP_ID FROM COMPLAINTS";
  try
  {
    cm.queryExecute(FindLastId);
  }
  catch (Exception e)
  {

  }
  int lastid =0;
  try
  {
    while(cm.toNext())
    {
      lastid = cm.getColumnI("TOP_ID");
    }
  }
  catch (Exception e)
  {

  }
   lastid=lastid+1;

  String CompId1 = String.valueOf(lastid);

  String comp = "call ADD_COMPLAINT('" + CompId1 + "','" + LodgeDate + "', '" + CompSubject + "', '" + FolioNo + "', '" + ShareholderName + "', '" + ComplaintBody + "', '" + AssignedPerson + "','" + TimeLine + "','" + CompStatus + "','" + AComments + "')";
  try
  {
    boolean b = cm.procedureExecute(comp);
  }
  catch (Exception e)
  {

  }
  //For File Attatchment

  if(fname.length()>1){
    int MaxAttachId =0;
    String FindMaxAttachId = "SELECT nvl(MAX(ATTACH_ID),0) AS ATTACH_ID FROM COMP_ATTACH";
    try
    {
      cm.queryExecute(FindMaxAttachId);
      while(cm.toNext())
      {
        MaxAttachId = cm.getColumnI("ATTACH_ID");
      }
    }
    catch (Exception e)
    {

    }
    MaxAttachId=MaxAttachId+1;
    Connection conn1=null;
    conn1=cm.getConnection();
    // It's faster when auto commit is off
    try
    {
      conn1.setAutoCommit (false);
      Statement stmt = conn1.createStatement ();
      stmt.execute ("INSERT INTO COMP_ATTACH VALUES ("+MaxAttachId+","+lastid+",EMPTY_BLOB(),'"+fname+"','"+AComments+"')");
      // Select the BLOB
      ResultSet rset = stmt.executeQuery ("select ATTACHMENTS from COMP_ATTACH where ATTACH_ID="+MaxAttachId);
      if (rset.next ())
      {
        // Get the BLOB locator from the table
        BLOB blob = ((OracleResultSet)rset).getBLOB ("ATTACHMENTS");

        // Declare a file handler for the john.gif file
        //File binaryFile = new File (FilePath);
        // Create a FileInputStream object to read the contents of the GIF file
        FileInputStream istream = new FileInputStream (fullFile);
        // Create an OutputStram object to write the BLOB as a stream
        OutputStream ostream = blob.getBinaryOutputStream();
        // Create a tempory buffer
        byte[] buffer = new byte[1024];
        int length = 0;

        // Use the read() method to read the GIF file to the byte
        // array buffer, then use the write() method to write it to
        // the BLOB.
        while ((length = istream.read(buffer)) != -1)
        ostream.write(buffer, 0, length);
        // Close the inputstream and outputstream
        istream.close();
        ostream.close();

        // Check the BLOB size
        //System.out.println ("Number of bytes written  = "+blob.length());
      }
      // Close all resources
      rset.close();
      conn1.close();
    }
    catch (Exception e)
    {

    }

  }


  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added Complaint Information')";
  try
  {
    boolean ub = cm.procedureExecute(ulog);
  }
  catch (Exception e)
  {

  }
  if (isConnect)
  {
    try
    {
      cm.takeDown();
    }
    catch (Exception e)
    {

    }
  }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Complaint/ComplaintbyReference.jsp";
</script>
</body>
</html>
