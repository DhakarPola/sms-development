<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : January 2006
'Page Purpose  : Declares Bonus Share.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Bonus Share Declaration</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function canwesplit()
{
  if (document.forms[0].cansplit.checked)
  {
    document.all.punits.style.display = ''
  }
  else
  {
    document.all.punits.style.display = 'none'
  }
}

function formconfirm()
{
  if (confirm("Do you want to declare the Bonus Share?"))
  {
    return true;
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('decdate','- Date of Declaration (Option must be entered)');
  SelectValidate('declaredby','- Declared By (Option must be entered)');
  BlankValidate('recdate','- Record Date (Option must be entered)');
  BlankValidate('ratio1','- Share Holder Ratio (Option must be entered)');
  BlankValidate('ratio2','- Bonus Share Ratio (Option must be entered)');

  if (document.forms[0].cansplit.checked)
  {
    BlankValidate('proposedunits','- Proposed Unit (Option must be entered)');
  }

  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
%>

<form action="SaveTempBonus.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Bonus Share Declaration</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date of Declaration</div></th>
        <td>:</td>
        <td><div align="left">
         <input name="decdate" type=text id="decdate" value="" maxlength="10" class="SL2TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('decdate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Declared By</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="declaredby" class="SL1TextFieldListBox">
            <option>--- Please Select ---</option>
            <option>Board</option>
            <option>AGM</option>
            <option>EGM</option>
         </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Record Date</div></th>
        <td>:</td>
        <td><div align="left">
         <input name="recdate" type=text id="recdate" value="" maxlength="10" class="SL2TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('recdate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Share Holder : Bonus Share Ratio</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style8">
          <input type="text" name="ratio1" class="SL69TextField" onkeypress="keypressOnNumberFld()"><b>&nbsp; : &nbsp;</b>
          <input type="text" name="ratio2" class="SL69TextField" onkeypress="keypressOnNumberFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;
          <input type="checkbox" name="cansplit" value="checkbox" onclick="canwesplit()">
          Split Cumulated Fraction Shares&nbsp;
          </div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left" class="style8">
          <SPAN id="punits" style="display:none">
            <input type="text" name="proposedunits" class="SL2TextField" onkeypress="keypressOnNumberFld()">
            &nbsp;<b>(Proposed Unit)</b>
          </span>
            </div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="10" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <td width="50%" align="right">
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
      </td>
        <td width="50%">
	          <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
      </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
