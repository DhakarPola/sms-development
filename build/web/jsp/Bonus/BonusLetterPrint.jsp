<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : January 2006
'Page Purpose  : Sets Criteria for Bonus Share Letter Printing.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.util.Date,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Bonus Share Letter</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function openfoliospans()
{
  var radio_choice;
  var radio_choice1;
  var counter;

  for (counter = 0; counter < document.forms[0].printscope.length; counter++)
   {
    if (document.forms[0].printscope[counter].checked)
    {
      radio_choice = document.forms[0].printscope[counter].value;
    }
   }

  for (counter = 0; counter < document.forms[0].bonustype.length; counter++)
   {
    if (document.forms[0].bonustype[counter].checked)
    {
      radio_choice1 = document.forms[0].bonustype[counter].value;
    }
   }

  if (radio_choice == "printsingle")
  {
   if (radio_choice1 == "SMStype")
   {
     document.all.foliospan1.style.display = '';
     document.all.foliospan2.style.display = '';
     document.all.foliospan3.style.display = '';
     document.all.foliospan4.style.display = 'none';
   }
   else if (radio_choice1 == "CDBLtype")
   {
     document.all.foliospan1.style.display = 'none';
     document.all.foliospan2.style.display = '';
     document.all.foliospan3.style.display = '';
     document.all.foliospan4.style.display = '';
   }
  }
}

function closefoliospans()
{
 document.all.foliospan1.style.display = 'none';
 document.all.foliospan2.style.display = 'none';
 document.all.foliospan3.style.display = 'none';
 document.all.foliospan4.style.display = 'none';
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  SelectValidate('dateselect','- Declaration Date (Option must be entered)');

  var radio_choice;
  var radio_choice1;
  var counter;

  for (counter = 0; counter < document.forms[0].printscope.length; counter++)
   {
    if (document.forms[0].printscope[counter].checked)
    {
      radio_choice = document.forms[0].printscope[counter].value;
    }
   }

  for (counter = 0; counter < document.forms[0].bonustype.length; counter++)
   {
    if (document.forms[0].bonustype[counter].checked)
    {
      radio_choice1 = document.forms[0].bonustype[counter].value;
    }
   }

  if (radio_choice == "printsingle")
  {
    if (radio_choice1 == "SMStype")
    {
      BlankValidate('foliono','- Folio No. (Option must be entered)');
    }
    else if (radio_choice1 == "CDBLtype")
    {
      BlankValidate('foliono','- BO ID. (Option must be entered)');
    }
  }

  if (count == 0)
  {
    document.forms[0].submit();
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
%>

<form action="<%=request.getContextPath()%>/jsp/Bonus/CheckBonusLetter.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Bonus Share Letter</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Type</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left" class="style9">
            <input name="bonustype" type="radio" value="SMStype" onclick="openfoliospans()" checked="checked">&nbsp;SMS<br>
            <input name="bonustype" type="radio" value="CDBLtype" onclick="openfoliospans()">&nbsp;CDBL
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date of Declaration</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="dateselect" class="SL1TextFieldListBox">
            <option>--- Please Select ---</option>
            <%
              String thedate = "";

              String query1 = "SELECT DISTINCT DATE_DEC FROM BONUSDECLARATION_VIEW ORDER BY DATE_DEC DESC";
              cm.queryExecute(query1);

              while(cm.toNext())
              {
                 thedate = cm.getColumnDT("DATE_DEC");
                 %>
                   <option value="<%=thedate%>"><%=thedate%></option>
                 <%
              }
            %>
         </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Print Scope</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left" class="style9">
            <input name="printscope" type="radio" value="printsingle" onclick="openfoliospans()">&nbsp;Single<br>
            <input name="printscope" type="radio" value="printall" onclick="closefoliospans()" checked="checked">&nbsp;All
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8"><SPAN id="foliospan1" style="display:none">&nbsp;&nbsp;&nbsp;&nbsp;Folio No.</span>
         <SPAN id="foliospan4" style="display:none">&nbsp;&nbsp;&nbsp;&nbsp;BO ID.</span>
        </div></th>
        <td><SPAN id="foliospan2" style="display:none">:</span></td>
        <td><div align="left">
          <SPAN id="foliospan3" style="display:none"><input type="text" name="foliono" class="SL2TextField"></span>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Name of Letter Templete&nbsp;
          </div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left" class="style9">
            Bonus Letter
            </div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="50%" scope="row">
            <div align="right">
	      <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
            </div></th>
        <td width="50%" align="left">
 	      <img name="B1" src="<%=request.getContextPath()%>/images/btnOK.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOKOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOK.gif'">
      </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
