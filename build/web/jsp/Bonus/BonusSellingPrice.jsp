<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : January 2006
'Page Purpose  : Form for Entering Selling Price of Fractional Bonus Shares.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.util.Date,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Selling Price of Bonus Fraction Shares</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function setbankvalues(value1)
{
  var val1 = value1;
  var val2 = value1.indexOf("%");
  var val3 = val1.substring(0,val2);
  var val4 = value1.indexOf("$");
  var val5 = val1.substring(val2 + 1,val4);
  var val6 = value1.indexOf("?");
  var val7 = val1.substring(val4 + 1,val6);
  var val8 = val1.substring(val6 + 1,val1.length);

  document.all["bchname"].value = val5;
  document.all["bnkaddress"].value = val7;
  document.all["bankidentification"].value = val8;
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('priceamount','- Sales Amount (Option must be entered)');
  BlankValidate('bnkname','- Bank Name (Option must be entered)');
  BlankValidate('bchname','- Bank Branch (Option must be entered)');
  BlankValidate('bnkaddress','- Bank Address (Option must be entered)');
  BlankValidate('bnkacc','- Account No. (Option must be entered)');

  if (count == 0)
  {
    document.forms[0].submit();
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   String DecDate = request.getParameter("declarationdate");

   String sellingprice = "";
   String bankname = "";
   String bankbranch = "";
   String bankaddress = "";
   String accountno = "";
   String bankid = "";
   String bankid1 = "";

   String query21 = "SELECT * FROM BONUSDECLARATION_VIEW WHERE DATE_DEC = TO_DATE('" + DecDate + "','DD/MM/YYYY')";
   cm.queryExecute(query21);

   while(cm.toNext())
     {
       sellingprice = cm.getColumnS("SELLING_PRICE");
       bankname = cm.getColumnS("BANK_NAME");
       bankbranch = cm.getColumnS("BANK_BRANCH");
       bankaddress = cm.getColumnS("BANK_ADDRESS");
       accountno = cm.getColumnS("ACCOUNT_NO");
       bankid = cm.getColumnS("BANK_ID");

       if (String.valueOf(sellingprice).equals("null"))
         sellingprice = "";
       if (String.valueOf(bankname).equals("null"))
         bankname = "";
       if (String.valueOf(bankbranch).equals("null"))
         bankbranch = "";
       if (String.valueOf(bankaddress).equals("null"))
         bankaddress = "";
       if (String.valueOf(bankid).equals("null"))
         bankid = "";
       if (String.valueOf(accountno).equals("null"))
         accountno = "";
     }
%>

<form action="<%=request.getContextPath()%>/jsp/Bonus/SaveBonusSellingPrice.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Selling Price of Bonus Fraction Shares</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Selling Price (Tk/Share)</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <input type="text" name="priceamount" class="SL2TextField" onkeypress="keypressOnDoubleFld()" value="<%=sellingprice%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Name</div></th>
        <td>:</td>
        <td><div align="left">
         <select name="bnkname" class="SL4TextFieldListBox" onchange="setbankvalues(document.all.bnkname.value)">
            <option>--- Please Select ---</option>
            <%
              String bname = "";
              String bbranch = "";
              String baddress = "";
              String bid = "";

              String query2 = "SELECT * FROM BANK_VIEW ORDER BY UPPER(BANK_NAME)";
              cm.queryExecute(query2);

              while(cm.toNext())
              {
                 bname = cm.getColumnS("BANK_NAME");
                 bbranch = cm.getColumnS("BANK_BRANCH");
                 baddress = cm.getColumnS("BANK_ADDRESS");
                 bid = cm.getColumnS("BANK_ID");
                 String combo = bname + "%" + bbranch + "$" + baddress + "?" + bid;

                 if (bankid.equalsIgnoreCase(bid))
                 {
                  bankid1 = bid;
                 %>
                   <option value="<%=combo%>" selected="selected"><%=bname%></option>
                 <%
                 }
                 else
                 {
                 %>
                   <option value="<%=combo%>"><%=bname%></option>
                 <%
                 }
              }
            %>
         </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Branch Name</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="bchname" class="SL71TextField" onfocus="this.blur()" value="<%=bankbranch%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Address</div></th>
        <td valign="top">:</td>
        <td><div align="left">
          <textarea name="bnkaddress" cols="20" rows="3" wrap="VIRTUAL" id="bnkaddress" class="ML12TextField" onfocus="this.blur()"><%=bankaddress%></textarea>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Account No.</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="bnkacc" class="SL71TextField" value="<%=accountno%>">
        </div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="34%" scope="row">
            <div align="right">
                   <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="history.go(-1)" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
            </div></th>
        <td width="33%" align="center">
                  <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
        </td>
        <td width="33%">
	          <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
        </td>
      </tr>
</table>

<!--HIDDEN FIELDS-->
<input type="hidden" name="bankidentification" value="<%=bankid1%>">
<input type="hidden" name="decdate" value="<%=DecDate%>">
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
