<script>
/*
'******************************************************************************************************************************************
'Script Author : Mohammad Ashraful Islam
'Creation Date : January 2007
'Page Purpose  : Calculate Dividend Report Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Dividend Return Calculation</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String decdate = String.valueOf(request.getParameter("dateselect"));
  String lid = String.valueOf(request.getParameter("letter"));
  String c=request.getParameter("counter");
  int count=Integer.parseInt(c);

  decdate=cm.replace(decdate,"'","''");

  String cat[];
  cat=new String[count+1];

  for(int i=1;i<=count;i++)
  {
  cat[i]=request.getParameter("AStatus"+i);
  }

  int j=1;

  String query3="call DELETE_DIVIDEND_RETURN()";
  boolean ub2=cm.procedureExecute(query3);

  int h=1;
  for(h=1;h<=count;h++)
  {

    if(!String.valueOf(cat[h]).equals("null"))
    {

    String cq="select category from category where code='"+cat[h]+"'";
    cm.queryExecute(cq);
    cm.toNext();
    String categ=cm.getColumnS("category");

    String query2="call ADD_DIVIDEND_RETURN('"+decdate+"','"+cat[h]+"','"+categ+"')";
    boolean ub1=cm1.procedureExecute(query2);

    String query4="call ADD_BODIVIDEND_RETURN('"+decdate+"','"+cat[h]+"','"+categ+"')";
    boolean ub3=cm1.procedureExecute(query4);
  }
  }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Calculated Dividend Return Information')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
     cm1.takeDown();
   }
%>

<script language="javascript">
  location = "<%=request.getContextPath()%>/DivReturnReport.jsp?letterid=<%=lid%>";
</script>
</body>
</html>
