<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : August 2005
'Page Purpose  : Saves update of Earning Partial information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update Earning Partial Information</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

  String oldYear = String.valueOf(request.getParameter("oldYear"));
  String dyear = String.valueOf(request.getParameter("dyear"));
  String amount = String.valueOf(request.getParameter("amount"));
  double amt=Double.parseDouble(amount);

  String upart= "call UPDATE_EARNING_PARTIAL('" + oldYear + "','" + dyear + "'," + amt + ")";
  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Edited Earning Partial Information')";
  try
  {
    boolean b = cm.procedureExecute(upart);
    boolean ub = cm.procedureExecute(ulog);
  }catch(Exception e){}

 if (isConnect)
  {
    cm.takeDown();
  }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Dividend/AllEarningPartial.jsp";
</script>
</body>
</html>
