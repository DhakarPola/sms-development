<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : December 2006
'Page Purpose  : Passes Parameters for Bank Report.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

 boolean isConnect = cm.connect();
 if(isConnect==false)
 {
   %>
    <jsp:forward page="ErrorMsg.jsp" >
    <jsp:param name="ErrorTitle" value="Connection Failure" />
    <jsp:param name="ErrorHeading" value="Connection Problem" />
    <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
    </jsp:forward>
  <%
 }

%>

<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<%@page
import="java.util.Date,java.text.*,com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
 Bank Report
</title>
</head>
<body bgcolor="#ffffff">
<h1>
</h1>
<%
    //Value of parameter get from page SetBankReport.jsp

  String account = String.valueOf(request.getParameter("accountno"));
  String accounttype=String.valueOf(request.getParameter("accounttype"));
  %>
  Account number=<%=account%>
  Account Type=<%=accounttype%>
  <%
  if (String.valueOf(account).equals("null"))
   account = "";
   if( String.valueOf(accounttype).equals("null"))
   accounttype="";

  String reporturl = "";
  String markprinted = "";
  boolean mp;

  if(accounttype.equals("Unrefunded"))
  {
    //add report
    reporturl="";
  }
  else
  {
    //add report
    reporturl="";
  }


  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Printed AGM Attemdance Report')";
  boolean ub = cm.procedureExecute(ulog);

  if (!reporturl.equals("")) {
    session.setAttribute("reportSource", null);
  }

Object reportSource = session.getAttribute("reportSource");
if (reportSource == null)
{
try{
  IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
  reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
  session.setAttribute("reportSource", reportSource);
}catch (Exception e) {}
}
  CrystalReportViewer viewer= new CrystalReportViewer();
  try{
    viewer.setReportSource(reportSource);
  }catch (Exception e) {}
  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  //viewer.setPrintMode(CrPrintMode.PDF);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setReuseParameterValuesOnRefresh(true);
// Passing parameters

ParameterField param1 = new ParameterField();
param1.setReportName("");
param1.setName("accountno");

Values vals1 = new Values();
ParameterFieldDiscreteValue val1 = new ParameterFieldDiscreteValue();
val1.setValue(account);
vals1.add(val1);

param1.setCurrentValues(vals1);
//param2.setCurrentValues(vals2);

Fields fields = new Fields();
fields.add(param1);
//fields.add(param2);

viewer.setParameterFields(fields);
viewer.refresh();

try
{
viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);
}
catch(Exception e)
{
  System.out.println(e.getMessage());
}

  if (isConnect)
  {
   cm.takeDown();
  }

%>
</body>
</html>

