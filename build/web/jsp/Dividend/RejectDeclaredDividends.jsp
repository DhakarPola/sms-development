<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : October 2005
'Page Purpose  : Deletes a Dividend.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Delete User</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String ddiv = "call DELETE_DIVIDENDDECLARATION_T()";
  boolean b = cm.procedureExecute(ddiv);

  ddiv = "SELECT MIN(WARRANT_NO) WMIN FROM DIVIDEND_T";
  cm.queryExecute(ddiv);
  cm.toNext();
  int min_warrant = cm.getColumnI("WMIN");
  min_warrant--;
  ddiv = "UPDATE STATUS SET WARRANT_NO = '" + min_warrant + "'";
  cm.queryExecute(ddiv);

  ddiv = "call DELETE_DIVIDEND_T()";
  b = cm.procedureExecute(ddiv);

  ddiv = "call DELETE_BODIVIDEND_T()";
  b = cm.procedureExecute(ddiv);

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Rejected Declared Dividend')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Dividend/AcceptDividend.jsp";
</script>
</body>
</html>
