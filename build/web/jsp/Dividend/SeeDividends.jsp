<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : November 2005
'Page Purpose  : See Dividend Information.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.util.Date,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<jsp:useBean id="cal" class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>

<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.imagehand{
  cursor:hand;
  color:#FFFFFF;
 }

-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit Dividend</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function showfields(id1,id2)
{
  if (document.all[id1].style.display=='none')
  {
   document.all[id1].style.display = '';
   document.all[id2].src = '<%=request.getContextPath()%>/images/twisteeDown.gif';
  }
  else
  {
   document.all[id1].style.display = 'none';
   document.all[id2].src = '<%=request.getContextPath()%>/images/twisteeUp.gif';
  }
}

function setbankvalues(value1)
{
  var val1 = value1;
  var val2 = value1.indexOf("%");
  var val3 = val1.substring(0,val2);
  var val4 = value1.indexOf("$");
  var val5 = val1.substring(val2 + 1,val4);
  var val6 = val1.substring(val4 + 1,val1.length);

  document.all["bchname"].value = val5;
  document.all["bnkaddress"].value = val6;
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769; font-weight: bold;}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   String DecDate = request.getParameter("declarationdate");
   String divtype = "";
   double divperc = 0;
   String PeriodFrom = "";
   String PeriodTo = "";
   double indrestax = 0;
   double indnonrestax = 0;
   double comprestax = 0;
   double compnonrestax = 0;
   double sptax = 0;
   String Ssptax = "";
   double indtfree = 0;
   String Sindtfree = "";
   double comptfree = 0;
   String Scomptfree = "";
   double deduction1 = 0;
   String Sdeduction1 = "";
   double deduction2 = 0;
   String Sdeduction2 = "";
   String ded1reason = "";
   String ded1criteria = "";
   String ded2reason = "";
   String ded2criteria = "";
   String qualdate = "";
   String paydate = "";
   String finyear = "";
   String bnkname = "";
   String bchname = "";
   String bnkaddress = "";
   String bnkacc = "";
   double indrestaxtin = 0;
   double indnonrestaxtin = 0;
   double comprestaxtin = 0;
   double compnonrestaxtin = 0;

   String query21 = "SELECT * FROM DIVIDENDDECLARATION_VIEW WHERE DATE_DEC = TO_DATE('" + DecDate + "','DD/MM/YYYY')";
   cm.queryExecute(query21);

   while(cm.toNext())
     {
       divtype = cm.getColumnS("DIV_TYPE");
       divperc = cm.getColumnF("DEC_DIV");
       PeriodFrom = cm.getColumnDT("PERIOD_FROM");
       PeriodTo = cm.getColumnDT("PERIOD_TO");
       indrestax = cm.getColumnF("TAX_IND_RES");
       indnonrestax = cm.getColumnF("TAX_IND_NONRES");
       comprestax = cm.getColumnF("TAX_COMP_RES");
       compnonrestax = cm.getColumnF("TAX_COMP_NONRES");
       sptax = cm.getColumnF("SPECIAL_TAX");
       indtfree = cm.getColumnF("IND_TAX_FREE");
       comptfree = cm.getColumnF("COMP_TAX_FREE");
       deduction1 = cm.getColumnF("DED1");
       ded1reason = cm.getColumnS("DED1REASON");
       ded1criteria = cm.getColumnS("DED1CRITERIA");
       deduction2 = cm.getColumnF("DED2");
       ded2reason = cm.getColumnS("DED2REASON");
       ded2criteria = cm.getColumnS("DED2CRITERIA");
       qualdate = cm.getColumnDT("QUAL_DATE");
       paydate = cm.getColumnDT("PAYABLE_DATE");
       finyear = cm.getColumnDT("FINANCIAL_YEAR");
       bnkname = cm.getColumnS("BANK_NAME");
       bchname = cm.getColumnS("BANK_BRANCH");
       bnkaddress = cm.getColumnS("BANK_ADDRESS");
       bnkacc = cm.getColumnS("ACCOUNT_NO");
	   
	   indrestaxtin = cm.getColumnF("TAX_IND_RES_TIN");
       indnonrestaxtin = cm.getColumnF("TAX_IND_NONRES_TIN");
       comprestaxtin = cm.getColumnF("TAX_COMP_RES_TIN");
       compnonrestaxtin = cm.getColumnF("TAX_COMP_NONRES_TIN");

       if (String.valueOf(ded1reason).equals("null"))
         ded1reason = "";
       if (String.valueOf(ded1criteria).equals("null"))
         ded1criteria = "";
       if (String.valueOf(ded2reason).equals("null"))
         ded2reason = "";
       if (String.valueOf(ded2criteria).equals("null"))
         ded2criteria = "";

       bnkaddress = cm.replace(bnkaddress,"\r\n","<br>");
       bnkaddress = cm.replace(bnkaddress,"  ","&nbsp;&nbsp;");

       if (sptax > 0)
       {
         Ssptax = String.valueOf(sptax);
       }
       if (deduction1 > 0)
       {
         Sdeduction1 = String.valueOf(deduction1);
       }
       if (deduction2 > 0)
       {
         Sdeduction2 = String.valueOf(deduction2);
       }
     }

     int warrantmax = 0;
     int warrantmin = 0;
     int bowarrantmax = 0;
     int bowarrantmin = 0;

     String grpquery = "SELECT MAX(to_Number(WARRANT_NO)) as WARMAX FROM DIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DecDate + "','DD/MM/YYYY')";
     cm.queryExecute(grpquery);
     cm.toNext();
     warrantmax = cm.getColumnI("WARMAX");

     grpquery = "SELECT MIN(to_Number(WARRANT_NO)) as WARMIN FROM DIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DecDate + "','DD/MM/YYYY')";
     cm.queryExecute(grpquery);
     cm.toNext();
     warrantmin = cm.getColumnI("WARMIN");

     grpquery = "SELECT MAX(to_Number(WARRANT_NO)) as BOWARMAX FROM BODIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DecDate + "','DD/MM/YYYY')";
     cm.queryExecute(grpquery);
     cm.toNext();
     bowarrantmax = cm.getColumnI("BOWARMAX");

     grpquery = "SELECT MIN(to_Number(WARRANT_NO)) as BOWARMIN FROM BODIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DecDate + "','DD/MM/YYYY')";
     cm.queryExecute(grpquery);
     cm.toNext();
     bowarrantmin = cm.getColumnI("BOWARMIN");

%>

<form action="UpdateTempDividend.jsp" method="post" name="FileForm">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Dividend Information</td></tr>
  <tr bgcolor="#005FFB">
    <td width="5%" height="20">&nbsp;&nbsp;
    <img name="DivInfoimg" id="DivInfo" class="DivInfoimg" src="<%=request.getContextPath()%>/images/twisteeUp.gif" onclick="showfields('DivInfospan','DivInfoimg');">
    <strong>
    <a class="imagehand" onclick="showfields('DivInfospan','DivInfoimg');">&nbsp;&nbsp;Dividend Information</a>
    </strong>
    </td>
  </tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <div align="left">
 <SPAN id="DivInfospan" name="DivInfospan" style="display:none">
  <br>
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="45%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Dividend Type</div></th>
        <td width="5%">:</td>
        <td width="55%"><div align="left" class="style8">
          <%=divtype%>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Declared Dividend (%)</div></th>
        <td width="1%" valign="top">:</td>
        <td><div align="left" class="style8">
          <%=divperc%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Declaration Date</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=DecDate%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Period From</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=PeriodFrom%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Period to</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=PeriodTo%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Record Date</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=qualdate%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Payable Date</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=paydate%>
        </div></td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Warrant No.</div></th>
        <td valign="top">:</td>
        <td valign="top"><div align="left" class="style8">
          SMS : <%=warrantmin%> - <%=warrantmax%><br><br>
          CDBL: <%=bowarrantmin%> - <%=bowarrantmax%>
        </div></td>
      </tr>
  </table>
  <br>
  </span>


  <tr bgcolor="#005FFB">
    <td width="5%" height="20">&nbsp;&nbsp;
    <img name="TaxInfoimg" id="TaxInfo" class="TaxInfoimg" src="<%=request.getContextPath()%>/images/twisteeUp.gif" onclick="showfields('TaxInfospan','TaxInfoimg');">
    <strong>
    <a class="imagehand" onclick="showfields('TaxInfospan','TaxInfoimg');">&nbsp;&nbsp;Tax Information</a>
    </strong>
    </td>
  </tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <div align="left">
 <SPAN id="TaxInfospan" name="TaxInfospan" style="display:none">
  <br>
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="45%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Individual Res. Tax w/out TIN (%)</div></th>
        <td width="5%">:</td>
        <td width="55%"><div align="left" class="style8">
          <%=indrestax%>
        </div></td>
      </tr>
	  <tr>
        <th scope="row" width="45%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Individual Res. Tax with TIN (%)</div></th>
        <td width="5%">:</td>
        <td width="55%"><div align="left" class="style8">
          <%=indrestaxtin%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Individual Non-Res. Tax w/out TIN (%)</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=indnonrestax%>
        </div></td>
      </tr>
	  <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Individual Non-Res. Tax with TIN (%)</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=indnonrestaxtin%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Company Res. Tax w/out TIN (%)</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=comprestax%>
        </div></td>
      </tr>
	  <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Company Res. Tax with TIN (%)</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=comprestaxtin%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Company Non-Res. Tax w/out TIN (%)</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=compnonrestax%>
        </div></td>
      </tr>
	  <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Company Non-Res. Tax with TIN (%)</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=compnonrestaxtin%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Special Tax (%)</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=Ssptax%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Individual Tax Free Max. Dividend (Tk)</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=indtfree%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Company Tax Free Max. Dividend (Tk)</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=comptfree%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Deduction 1 (%)</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=Sdeduction1%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp; Reason</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=ded1reason%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- &nbsp;&nbsp;Criteria</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=ded1criteria%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Deduction 2 (%)</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=Sdeduction2%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp; Reason</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=ded2reason%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- &nbsp;&nbsp;Criteria</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=ded2criteria%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Financial Year</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=finyear%>
        </div></td>
      </tr>
  </table>
  <br>
  </span>


  <tr bgcolor="#005FFB">
    <td width="5%" height="20">&nbsp;&nbsp;
    <img name="BankInfoimg" id="BankInfo" class="BankInfoimg" src="<%=request.getContextPath()%>/images/twisteeUp.gif" onclick="showfields('BankInfospan','BankInfoimg');">
    <strong>
    <a class="imagehand" onclick="showfields('BankInfospan','BankInfoimg');">&nbsp;&nbsp;Bank Information</a>
    </strong>
    </td>
  </tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
 <SPAN id="BankInfospan" name="BankInfospan" style="display:none">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="45%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Name</div></th>
        <td width="5%">:</td>
        <td width="55%"><div align="left" class="style8">
          <%=bnkname%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Branch Name</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=bchname%>
        </div></td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Address</div></th>
        <td valign="top">:</td>
        <td><div align="left" class="style8">
          <%=bnkaddress%>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Account No.</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <%=bnkacc%>
        </div></td>
      </tr>
  </table>
  <br>
  </span>

      </div>
      <table width="100%" BORDER=0  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
      <tr bgcolor="#E8F3FD">
        <th width="100%" scope="row" >
            <div align="center">
	      <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="history.go(-1)" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
            </div></th>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
