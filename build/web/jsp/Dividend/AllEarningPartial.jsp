<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : August 2007
'Page Purpose  : View of All Earning Partial.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Earning Partial</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function tonewbank()
{
  location = "<%=request.getContextPath()%>/jsp/Dividend/NewEarningPartial.jsp"
}
//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

    String decYear = "";
    String dest="";
    String amount = "";
    String query1 = "SELECT * FROM VIEW_EARNING_PARTIAL ORDER BY DEC_YEAR DESC";
    try
    {
      cm.queryExecute(query1);

%>
  <span class="style7">
  <form method="GET" action="">
  <img name="B4" src="<%=request.getContextPath()%>/images/btnNew.gif" onclick="tonewbank()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNewOn.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNew.gif'">
  <table width="100%" BORDER=1  cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Earning Per Share Information</center></td></tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="50%" class="style9"><div align="center">Year</div></td>
    <td width="50%"><div align="center" class="style9">
      <div align="center">Amount</div>
    </div></td>
  </tr>
  <div align="left">
     <%
    while(cm.toNext())
     {
       decYear = cm.getColumnS("DEC_YEAR");
       amount = cm.getColumnS("AMOUNT");

       dest = request.getContextPath() + "/jsp/Dividend/EditEarningPartial.jsp";

       if (!decYear.equals("null"))
       {
         if (String.valueOf(amount).equals("null"))
           amount = "0";
         %>
    </div>
  <tr bgcolor="#E8F3FD">
           <td><div align="center" class="style10">
             <div align="center" class="style10">
               <span class="style13">
               <label></label>
               <a HREF="<%=dest%>?dyear=<%=decYear%>"><%=decYear%></a>
               </span></div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">&nbsp;<%=amount%></div>
           </div></td>
         </tr>
         <div align="left" class="style13">
         <%
         }
     }
    }
    catch(Exception e){}
   if (isConnect)
   {
     try
     {
       cm.takeDown();
     }catch(Exception e){}
   }
  %>
         </div>
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
</body>
</html>
