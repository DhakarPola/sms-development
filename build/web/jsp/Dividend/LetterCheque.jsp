
<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page
import="java.util.Date,java.text.*,com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
Letter Cheque
</title>
</head>
<body bgcolor="#ffffff">
<h1>
</h1>
<%
  String DateDec = String.valueOf(request.getParameter("dateselect"));
  String DivType = String.valueOf(request.getParameter("dividendtype"));
  String DivOrder = String.valueOf(request.getParameter("orderby"));
  String PRecords = String.valueOf(request.getParameter("printrecords"));
  String FStart = String.valueOf(request.getParameter("foliostart"));
  String FEnd = String.valueOf(request.getParameter("folioend"));
  String WStart = String.valueOf(request.getParameter("warrantstart"));
  String WEnd = String.valueOf(request.getParameter("warrantend"));
  String PrintDup = String.valueOf(request.getParameter("printduplicate"));
  String IncludeGrpMem = String.valueOf(request.getParameter("includegroupmembers"));
  String SignatureInfo = String.valueOf(request.getParameter("sigselect"));

  if (String.valueOf(FStart).equals("null"))
   FStart = "";
  if (String.valueOf(FEnd).equals("null"))
   FEnd = "";
  if (String.valueOf(WStart).equals("null"))
   WStart = "";
  if (String.valueOf(WEnd).equals("null"))
   WEnd = "";
  if (String.valueOf(PrintDup).equals("null"))
   PrintDup = "";
  if (String.valueOf(IncludeGrpMem).equals("null"))
   IncludeGrpMem = "";


  String reporturl = "";

  if (DivType.equalsIgnoreCase("SMStype"))
  {
   if (DivOrder.equalsIgnoreCase("byname"))
   {
     reporturl = "/CR_Reports/FOLIO_Div_PostLists/FolioDivPostlist_Order_by_Name.rpt";
   }
   else if (DivOrder.equalsIgnoreCase("bydividend"))
   {
     reporturl = "/CR_Reports/FOLIO_Div_PostLists/FolioDivPostlist_Order_by_Dividend.rpt";
   }
   else if (DivOrder.equalsIgnoreCase("byfolio"))
   {
     reporturl = "/CR_Reports/FOLIO_Div_PostLists/FolioDivPostlist_Order_by_FolioNo.rpt";
   }
  }
  else if (DivType.equalsIgnoreCase("CDBLtype"))
  {
   if (DivOrder.equalsIgnoreCase("byname"))
   {
     reporturl = "/CR_Reports/BO_Div_PostLists/BO DivPostList_Order_By_Name.rpt";
   }
   else if (DivOrder.equalsIgnoreCase("bydividend"))
   {
     reporturl = "/CR_Reports/BO_Div_PostLists/BO DivPostList_Order_By_Dividend.rpt";
   }
   else if (DivOrder.equalsIgnoreCase("byfolio"))
   {
     reporturl = "/CR_Reports/BO_Div_PostLists/BO DivPostList_Order_By_BOID.rpt";
   }
  }

//System.out.println("----------- " + reporturl);

CrystalReportViewer crv = new CrystalReportViewer();

//

try
{
JPEReportSourceFactory jrsf = new JPEReportSourceFactory();
crv.setReportSource(jrsf.createReportSource(reporturl,request.getLocale()));
//System.out.println(request.getLocale());
}
catch(Exception e)
{
  System.out.println("*****"+e.getMessage());
}
  crv.setHasViewList(false);
  crv.setHasPrintButton(true);
  crv.setHasRefreshButton(true);
  crv.setHasExportButton(true);
  crv.setHasLogo(false);
  crv.setDisplayPage(true);
  crv.setDisplayGroupTree(false);
  crv.setEnableParameterPrompt(false);
  crv.setEnableDrillDown(false);
  crv.setHasSearchButton(false);
  crv.setHasGotoPageButton(false);
  crv.setHasPageNavigationButtons(true);
  crv.setHasToggleGroupTreeButton(false);
  crv.setEnableParameterPrompt(false);

  crv.setPrintMode(CrPrintMode.PDF);
  crv.setHasLogo(false);
  crv.setOwnPage(true);
  crv.setLeft(20);

  crv.refresh();

  // For displaying the toolbar
  crv.setDisplayToolbar(false);


// set credentials
/*
ConnectionInfo ci = new ConnectionInfo();
ci.setUserName("sms");
ci.setPassword("p");
ConnectionInfos connections = new ConnectionInfos();
connections.add(ci);
crv.setDatabaseLogonInfos(connections);
*/
//crv.setReportSource(jrsf.createReportSource("/Invoice2.rpt"));

// Passing parameters

/*
ParameterField param1 = new ParameterField();
param1.setReportName(""); // must always be set, even if just to ""
param1.setName("DivGreaterThan_Param");

ParameterField param2 = new ParameterField();
param2.setReportName("");
param2.setName("DivLessThan_Param");

ParameterField param3 = new ParameterField();
param3.setReportName("");
param3.setName("DecDate_Param");

Values vals1 = new Values();
ParameterFieldDiscreteValue val1 = new ParameterFieldDiscreteValue();
val1.setValue(Div_GT);
vals1.add(val1);

Values vals2 = new Values();
ParameterFieldDiscreteValue val2 = new ParameterFieldDiscreteValue();
val2.setValue(Div_LT);
vals2.add(val2);

Values vals3 = new Values();
ParameterFieldDiscreteValue val3 = new ParameterFieldDiscreteValue();



try {
DateFormat df = new SimpleDateFormat("dd/mm/yyyy");
Date d = df.parse(DateDec);
String expDate = new SimpleDateFormat("dd/mm/yyyy").format(d);
System.out.println("Wasif: " + expDate);
val3.setValue(d);
vals3.add(val3);
} catch(Exception e) {
e.printStackTrace();
}

param1.setCurrentValues(vals1);
param2.setCurrentValues(vals2);
param3.setCurrentValues(vals3);

Fields fields = new Fields();
fields.add(param1);
fields.add(param2);
fields.add(param3);

crv.setParameterFields(fields);

try
{
  crv.processHttpRequest(request, response, application,null);
}
catch(Exception e)
{
  System.out.println(e.getMessage());
}
*/
%>
</body>
</html>

