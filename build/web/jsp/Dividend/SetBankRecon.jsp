<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Updated By    : Renad Hakim
'Updated By    : Rahat Uddin
'Update Date   : August 2007
'Creation Date : December 2006
'Page Purpose  : Selects Account Number for Bank Reconciliation.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Bank Reconciliation</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>

<script language="javascript">


<!--
//Execute while click on Submit
function SubmitThis() {
  count = 0;
  SelectValidate('account','- Account select (Option must be entered)');

  if (count == 0)
  {
    document.forms[0].submit();
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
function gosearch(account)
{
  var list =  document.forms[0].account;
  location = list.options[list.selectedIndex].value;
}
function refresh()
{
  location="<%=request.getContextPath()%>/jsp/Dividend/SetBankRecon.jsp?account=0";
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
      }
      String account=request.getParameter("account");
      if(account.equals("null"))
      account="0";
      %>

<form action="ShowBankReconciliation.jsp?accountselect=<%=account%>" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Bank Reconciliation</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Account No.</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="account" class="SL1TextFieldListBox" onchange="gosearch()">
            <option value="<%=request.getContextPath()%>/jsp/Dividend/SetBankRecon.jsp?account=0">--- Please Select ---</option>
            <%
              //String query1 = "SELECT DISTINCT ACCOUNT_NO FROM DIVIDENDDECLARATION_VIEW ORDER BY ACCOUNT_NO DESC";
              String query1 = "SELECT DISTINCT * FROM BANK_ACCOUNTS_VIEW";
              cm.queryExecute(query1);
              String unclaimed_account="";
              String current_account ="";
              while(cm.toNext())
              {
                //String account_no = cm.getColumnS("ACCOUNT_NO");
                current_account = cm.getColumnS("CURRENT_ACCOUNT");
                unclaimed_account = cm.getColumnS("UNCLAIMED_ACCOUNT");

                  %>
                  <option value="<%=request.getContextPath()%>/jsp/Dividend/SetBankRecon.jsp?account=<%=current_account%>" <%if(current_account.equals(account)){%>Selected<%}%>><%=current_account%>&nbsp;(Current Account)</option>
                  <option value="<%=request.getContextPath()%>/jsp/Dividend/SetBankRecon.jsp?account=<%=unclaimed_account%>" <%if(unclaimed_account.equals(account)){%>Selected<%}%>><%=unclaimed_account%>&nbsp;(Unclaimed Account)</option>
                  <%
              }
            %>
         </select>
        </div></td>
      </tr>
      <%
      if(!account.equals("0"))
      {
      %>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date of Dividend Declaration</div></th>
        <td valign="top">:</td>

        <td valign="top"><div align="left">

          <table>
          <%
          //String qdate="select DATE_DEC from DIVIDENDDECLARATION_VIEW where ACCOUNT_NO='"+ account +"' order by DATE_DEC";

          String date="";
          if(!account.equalsIgnoreCase(unclaimed_account)){
          String qdate="select DATE_DEC  from DIVIDENDDECLARATION_VIEW where upper(ACC_MERGED)!='Y' or ACC_MERGED is null order by DATE_DEC";
          cm.queryExecute(qdate);
          while(cm.toNext())
          {
            date=cm.getColumnDT("DATE_DEC");
            %>
            <tr>
              <th scope="row"><div align="left" class="style8"><%=date%></div></th>
            </tr>
            <%}
          }
          else{
            String qdate="select DATE_DEC from DIVIDENDDECLARATION_VIEW where upper(ACC_MERGED)='Y' order by DATE_DEC";
            cm.queryExecute(qdate);
            String All_Divi_date="";

            while(cm.toNext())
          {
            date=cm.getColumnDT("DATE_DEC");
            if(All_Divi_date.length()>0){
              All_Divi_date=All_Divi_date+", "+date;
            }
            else{
              All_Divi_date=date;
            }
          }
            %>
            <tr>

              <th scope="row"><div align="left" class="style8"><%=All_Divi_date%></div>
                <input type="hidden" name="unclaimed_account" id="unclaimed_account" value="Y"></th>
            </tr>
            <%
          }
            %>
          </table>
</div></td>
      </tr>
      <%
         }
      %>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="50%" scope="row">
            <div align="right">
	      <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="refresh()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
            </div></th>
        <td width="50%" align="left">
 	      <img name="B1" src="<%=request.getContextPath()%>/images/btnOK.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOKOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOK.gif'">
      </td>
      </tr>
</table>
<input type="hidden" name="StartValue" value="1">
<input type="hidden" name="EndValue" value="0">
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
