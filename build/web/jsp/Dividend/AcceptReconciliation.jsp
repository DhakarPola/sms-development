<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : December 2005
'Page Purpose  : Marks the Collected Warrant Numbers.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Mark Selected Warrants</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String rowcounter = String.valueOf(request.getParameter("rowcounter1"));
  String DivDate = String.valueOf(request.getParameter("ddate"));
  String DivType = String.valueOf(request.getParameter("dtype"));
  String SMSType = String.valueOf(request.getParameter("smstype1"));
  String StartingValue = String.valueOf(request.getParameter("svalue"));
  String EndingValue = String.valueOf(request.getParameter("evalue"));
  String ColDate = String.valueOf(request.getParameter("mrecondate"));
  String Fldname = "";
  int irowcounter = Integer.parseInt(rowcounter);

  for (int i=1;i<irowcounter+1;i++)
  {
    Fldname = "selectingbox" + String.valueOf(i);
    String BoxValue = String.valueOf(request.getParameter(Fldname));

    BoxValue=cm.replace(BoxValue,"'","''");

    if (String.valueOf(BoxValue).equals("null"))
    {
      BoxValue = "";
    }
    if (!BoxValue.equalsIgnoreCase(""))
    {
      String marksel = "";
      String markcoll = "";
      if (DivType.equalsIgnoreCase("SMS"))
      {
        if (SMSType.equalsIgnoreCase("Group"))
        {
          marksel = "call MARK_SELECTED_DIVIDEND_GROUP('" + BoxValue + "','" + ColDate + "','" + DivDate + "')";
          markcoll = "call MARK_COLLECTED_DIVIDEND_GROUP('" + DivDate + "')";
        }
        else if (SMSType.equalsIgnoreCase("Individual"))
        {
          marksel = "call MARK_SELECTED_DIVIDEND('" + BoxValue + "','" + ColDate + "','" + DivDate + "')";
          markcoll = "call MARK_COLLECTED_DIVIDEND()";
        }
      }
      else if (DivType.equalsIgnoreCase("CDBL"))
      {

        if (SMSType.equalsIgnoreCase("Group"))
        {
          // System.out.print("the val is here-----"+BoxValue+"-"+ColDate+"-"+DivDate);
         marksel = "call MARK_SELECTED_OMNIBUS_GROUP('" + BoxValue + "','" + ColDate + "','" + DivDate + "')";
         markcoll = "call MARK_COLLECTED_OMNIBUS_GROUP('" + BoxValue + "'," +"'" + DivDate + "')";
        }

        else if (SMSType.equalsIgnoreCase("Individual"))
        {
          marksel = "call MARK_SELECTED_BODIVIDEND('" + BoxValue + "','" + ColDate + "','" + DivDate + "')";
          markcoll = "call MARK_COLLECTED_BODIVIDEND()";
        }
      }

      boolean b = cm.procedureExecute(marksel);
      b = cm.procedureExecute(markcoll);
    }
  }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Reconciled Dividends')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  alert("Reconciliation Done!");
  location = "<%=request.getContextPath()%>/jsp/Dividend/AllReconciliation.jsp?dateselect=<%=DivDate%>&orderby=<%=DivType%>&StartValue=<%=StartingValue%>&EndValue=<%=EndingValue%>&dividendby=<%=SMSType%>&collectiondate=<%=ColDate%>";
</script>
</body>
</html>
