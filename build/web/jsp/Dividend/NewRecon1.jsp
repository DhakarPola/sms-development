<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : September 2005
'Page Purpose  : Form to add new Bank Reconciliation Entry.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Bank Reconciliation</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function canwesplit()
{
  if (document.forms[0].cansplit.checked)
  {
    document.all.punits.style.display = ''
  }
  else
  {
    document.all.punits.style.display = 'none'
  }
}

function formconfirm()
{
  if (confirm("Do you want to declare the Right Share?"))
  {
    return true;
  }
}

function calsharevalue()
{
  var var1 = trim(window.document.forms[0].hiddenvalue.value);
  var var2 = trim(window.document.forms[0].sharevalue.value);
  var var3 = trim(window.document.forms[0].premdiscvalue.value);
  var var4 = trim(window.document.forms[0].taxpershare.value);

  var var5 = 0;
  var5 = Number(var5);

  if (var2 == "")
  {
    var2 = 0;
  }
  if (var3 == "")
  {
    var3 = 0;
  }
  if (var4 == "")
  {
    var4 = 0;
  }

  var2 = Number(var2);
  var3 = Number(var3);
  var4 = Number(var4);

  if (var1 == "p")
  {
   var5 = var2 + var3 + var4;
  }
  else if (var1 == "d")
  {
   var5 = var2 - var3 + var4;
  }

  document.all["totalsharevalue"].value = var5;
}

function givevalue1()
{
  document.all["hiddenvalue"].value = 'p';
}

function givevalue2()
{
  document.all["hiddenvalue"].value = 'd';
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('decdate','- Date of Declaration (Option must be entered)');
  SelectValidate('declaredby','- Declared By (Option must be entered)');
  BlankValidate('recdate','- Record Date (Option must be entered)');
  BlankValidate('ratio1','- Share Holder Ratio (Option must be entered)');
  BlankValidate('ratio2','- Right Share Ratio (Option must be entered)');
  BlankValidate('sharevalue','- Share Value (Option must be entered)');
  BlankValidate('premdiscvalue','- Premium/Discount (Option must be entered)');
  BlankValidate('taxpershare','- Tax Per Share (Option must be entered)');

  if (document.forms[0].cansplit.checked)
  {
    BlankValidate('proposedunits','- Proposed Unit (Option must be entered)');
  }

  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769; font-weight: bold;}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF" onload="calsharevalue()">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
%>

<form action="SaveTempRight.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Bank Reconciliation</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Account No.</div></th>
        <td width="1%" valign="top">:</td>
        <td><div align="left" class="style8">
          SVAC123456-78
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date of Dividend Declaration</div></th>
        <td width="1%" valign="top">:</td>
        <td><div align="left" class="style8">
          21/06/2006
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Entry Date</div></th>
        <td>:</td>
        <td><div align="left">
         <input name="entdate" type=text id="entdate" value="" maxlength="10" class="SL2TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('entdate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Particulars</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="particulars" class="SL68TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Amount</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="foliono" class="SL2TextField">
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Debit/Credit</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="declaredby" class="SL1TextFieldListBox">
            <option>--- Please Select ---</option>
            <option>Board</option>
            <option>AGM</option>
            <option>EGM</option>
         </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Refunded</div></th>
        <td>:</td>
        <td><div align="left" class="style9">
         <input name="AStatus" id="AStatus" type="checkbox" value="T">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Refund Date</div></th>
        <td>:</td>
        <td><div align="left">
         <input name="rfndate" type=text id="rfndate" value="" maxlength="10" class="SL2TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('rfndate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="10" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <td width="50%" align="right">
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
      </td>
        <td width="50%">
	          <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
      </td>
      </tr>
</table>
   <input type="hidden" name="hiddenvalue" value="p">
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
