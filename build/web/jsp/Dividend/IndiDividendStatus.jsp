<script>
/*
'******************************************************************************************************************************************
'Script Author : Rahat
'Creation Date : July 2011
'Page Purpose  : Supply BO/ FOLIO number for dividend Report.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Bank Report</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>

<script language="javascript">


<!--
//Execute while click on Submit
var bo_folio_no="";

function SubmitThis() {
  count = 0;
  BlankValidate('bo_folio_no','- BO / FOLIO Number (Option must be entered)');

  if (count == 0)
  {

    document.forms[0].action="<%=request.getContextPath()%>/IndiDividendReport.jsp";
    document.forms[0].submit();
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}

function refresh()
{
  location="<%=request.getContextPath()%>/jsp/Dividend/IndiDividendStatus.jsp";
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
  boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
      /*String account=request.getParameter("account");
      String account_no="";
      if(account.equals("null"))
      account="0";
      */
      %>

<form action="<%=request.getContextPath()%>/AccountReport.jsp" method="post" name="FileForm" onsubmit="SubmitThis()">
  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7" width="100%" colspan="3">Dividend Reports for Individual Shareholder</td></tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;BO / FOLIO Number</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <Input type="text" id="bo_folio_no" name="bo_folio_no" class="SL1TextFieldListBox" value="">
        </div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="50%" scope="row">
            <div align="right">
	      <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="refresh()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
            </div></th>
        <td width="50%" align="left">
 	      <img name="B1" src="<%=request.getContextPath()%>/images/btnOK.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOKOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOK.gif'">
      </td>
      </tr>
</table>

</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
