<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : December 2006
'Page Purpose  : Selects Criteria for Letter to Bank.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Letter to Bank</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
//Execute while click on Submit
function SubmitThis() {
  count = 0;
  SelectValidate('letter','- Letter name (Option must be entered)');
  SelectValidate('bank','- Bank name (Option must be entered)');
  SelectValidate('sig','- Signature (Option must be entered)');
  SelectValidate('account','- Account select (Option must be entered)');

  if (count == 0)
  {
    document.forms[0].submit();

  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
function gosearch(account)
{
  var list =  document.forms[0].account;
  location = list.options[list.selectedIndex].value;
}
function gosearch1(bank)
{
  var list =  document.forms[0].bank;
  location = list.options[list.selectedIndex].value;

}
function gosearch2(letter)
{
  var list =  document.forms[0].letter;
  location = list.options[list.selectedIndex].value;

}
function gosearch3(sig)
{
  var list =  document.forms[0].sig;
  location = list.options[list.selectedIndex].value;
}
function resetnow()
{
  location="<%=request.getContextPath()%>/jsp/Dividend/SetBankLetter.jsp?account=0&sig=0&letter=0&bank=0";
}
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

     String account=String.valueOf(request.getParameter("account"));
     System.out.println("Account="+account);
     String bankname=String.valueOf(request.getParameter("bank"));
     System.out.println("Bank="+bankname);
     String lettername=String.valueOf(request.getParameter("letter"));
     System.out.println("Letter="+lettername);
     String signature=String.valueOf(request.getParameter("sig"));
     System.out.println("Signature="+signature);
     String sig_id=String.valueOf(request.getParameter("sigid"));

%>

<form action="<%=request.getContextPath()%>/BankLetter.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Letter to Bank</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
  <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Name of Bank</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="bank" class="SL3TextFieldListBox" onchange="gosearch1()">
            <option value="<%=request.getContextPath()%>/jsp/Dividend/SetBankLetter.jsp?account=<%=account%>&bank=0&letter=<%=lettername%>&sig=<%=signature%>&sigid=<%=sig_id%>">--- Please Select ---</option>
            <%
              String queryb = "SELECT DISTINCT BANK_NAME FROM BANK_VIEW ORDER BY BANK_NAME";
              System.out.println("queryb= "+queryb);
              try
              {
                cm.queryExecute(queryb);
                while(cm.toNext())
                {
                  String bank = cm.getColumnS("BANK_NAME");
                  System.out.println("bank=" +bank);
                  %>
                  <option value="<%=request.getContextPath()%>/jsp/Dividend/SetBankLetter.jsp?account=<%=account%>&bank=<%=bank%>&letter=<%=lettername%>&sig=<%=signature%>&sigid=<%=sig_id%>" <%if(bank.equals(bankname)){%>Selected<%}%>><%=bank%></option>
                  <%
                  }
                }catch(Exception e){}
            %>
         </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Name of Letter</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="letter" class="SL3TextFieldListBox" onchange="gosearch2()">
            <option value="<%=request.getContextPath()%>/jsp/Dividend/SetBankLetter.jsp?account=<%=account%>&bank=<%=bankname%>&letter=0&sig=<%=signature%>&sigid=<%=sig_id%>">--- Please Select ---</option>
            <%
              String letter = "";
              String query1 = "SELECT DISTINCT LETTER_NAME, LETTER_ID FROM LETTER_VIEW ORDER BY LETTER_NAME";
              cm.queryExecute(query1);
              while(cm.toNext())
              {
                 letter = cm.getColumnS("LETTER_NAME");
                 String letter_id=cm.getColumnS("LETTER_ID");
                 //letter=letter.trim();
                 %>
                   <option value="<%=request.getContextPath()%>/jsp/Dividend/SetBankLetter.jsp?account=<%=account%>&bank=<%=bankname%>&letter=<%=letter%>&sig=<%=signature%>&sigid=<%=sig_id%>" <%if(letter.equals(lettername)){%>Selected<%}%>><%=letter%></option>
                 <%
              }
            %>
         </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Signature</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="sig" class="SL3TextFieldListBox" onchange="gosearch3()">
          <option value="<%=request.getContextPath()%>/jsp/Dividend/SetBankLetter.jsp?account=<%=account%>&bank=<%=bankname%>&letter=<%=lettername%>&sig=0&sigid=<%=sig_id%>">--- Please Select ---</option>
           <%
             String signame = "";
             String sigid = "";
             String sigrank = "";
             String temp="";

             String query2 = "SELECT * FROM SIGNATURE_VIEW ORDER BY SIG_ID";
             cm.queryExecute(query2);
             while(cm.toNext())
             {
               signame = cm.getColumnS("NAME");
               sigid = cm.getColumnS("SIG_ID");
               sigrank = cm.getColumnS("RANK");
               temp=signame+" ("+sigrank+")";
               %>
               <option value="<%=request.getContextPath()%>/jsp/Dividend/SetBankLetter.jsp?account=<%=account%>&bank=<%=bankname%>&letter=<%=lettername%>&sig=<%=temp%>&sigid=<%=sigid%>" <%if(temp.equals(signature)){%>Selected<%}%>><%=temp%></option>
               <%
              }
            %>
         </select>
        </div></td>
      </tr>
       <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Account No.</div></th>
        <td width="1%" valign="top">:</td>
        <td valign="top"><div align="left">
         <select name="account" class="SL3TextFieldListBox" onchange="gosearch()">
            <option value="<%=request.getContextPath()%>/jsp/Dividend/SetBankLetter.jsp?account=0&bank=<%=bankname%>&letter=<%=lettername%>&sig=<%=signature%>&sigid=<%=sig_id%>">--- Please Select ---</option>
            <%
              //String querya = "SELECT DISTINCT ACCOUNT_NO FROM DIVIDENDDECLARATION_VIEW ORDER BY ACCOUNT_NO DESC";

              String querya = "SELECT DISTINCT * FROM BANK_ACCOUNTS_VIEW";
              cm.queryExecute(querya);
              String unclaimed_account="";
              String current_account ="";
              while(cm.toNext())
              {
                //String account_no = cm.getColumnS("ACCOUNT_NO");
                current_account = cm.getColumnS("CURRENT_ACCOUNT");
                unclaimed_account = cm.getColumnS("UNCLAIMED_ACCOUNT");

                  %>
                  <option value="<%=request.getContextPath()%>/jsp/Dividend/SetBankLetter.jsp?account=<%=current_account%>&bank=<%=bankname%>&letter=<%=lettername%>&sig=<%=signature%>&sigid=<%=sig_id%>" <%if(current_account.equals(account)){%>Selected<%}%>><%=current_account%>&nbsp;(Current Account)</option>
                  <option value="<%=request.getContextPath()%>/jsp/Dividend/SetBankLetter.jsp?account=<%=unclaimed_account%>&bank=<%=bankname%>&letter=<%=lettername%>&sig=<%=signature%>&sigid=<%=sig_id%>" <%if(unclaimed_account.equals(account)){%>Selected<%}%>><%=unclaimed_account%>&nbsp;(Unclaimed Account)</option>
                  <%
              }
            %>
         </select>
        </div></td>
      </tr>
      <%if(!account.equals("0")){ %>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date of Dividend Declaration</div></th>
        <td valign="top">:</td>

        <td><div align="left">

          <table>
          <%
          //String qdate="select DATE_DEC from DIVIDENDDECLARATION_VIEW where ACCOUNT_NO='"+ account +"' order by DATE_DEC";

          String date="";
          if(!account.equalsIgnoreCase(unclaimed_account)){
          String qdate="select DATE_DEC  from DIVIDENDDECLARATION_VIEW where upper(ACC_MERGED)!='Y' or ACC_MERGED is null order by DATE_DEC";
          cm.queryExecute(qdate);
          while(cm.toNext())
          {
            date=cm.getColumnDT("DATE_DEC");
            %>
            <tr>
              <th scope="row"><div align="left" class="style8"><%=date%></div></th>
            </tr>
            <%}
          }
          else{
            String qdate="select DATE_DEC from DIVIDENDDECLARATION_VIEW where upper(ACC_MERGED)='Y' order by DATE_DEC";
            cm.queryExecute(qdate);
            String All_Divi_date="";

            while(cm.toNext())
          {
            date=cm.getColumnDT("DATE_DEC");
            if(All_Divi_date.length()>0){
              All_Divi_date=All_Divi_date+", "+date;
            }
            else{
              All_Divi_date=date;
            }
          }
            %>
            <tr>

              <th scope="row"><div align="left" class="style8"><%=All_Divi_date%></div>
                <input type="hidden" name="unclaimed_account" id="unclaimed_account" value="Y"></th>
            </tr>
            <%
          }
            %>
          </table>
</div></td>

      </tr>
      <%}%>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="50%" scope="row">
            <div align="right">
	      <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="resetnow()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
            </div></th>
        <td width="50%" align="left">
 	      <img name="B1" src="<%=request.getContextPath()%>/images/btnOK.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOKOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOK.gif'">
      </td>
      </tr>
</table>
<input type="hidden" name="bankselect" value="<%=bankname%>" />
<input type="hidden" name="letterselect" value="<%=lettername%>"/>
<input type="hidden" name="sigselect" value="<%=signature%>"/>
<input  type="hidden" name="accountselect" value="<%=account%>"/>
<input type="hidden" name="sid" value="<%=sig_id%>" />
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
