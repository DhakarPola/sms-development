<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : December 2005
'Page Purpose  : Warrant Details.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

int Chunk = 20;
%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
.style19 {
	color: #0A2769;
	font-weight: bold;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Warrant Details</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
var check1 = 0;

function goPrevious(pdate,ptype,pstart,pend)
{
  if (check1 > 0)
  {
    count = 0;
    BlankValidate('mstopdate','- Stop Date (Option must be entered)');

    if (count == 0)
    {
      var thisurl = "<%=request.getContextPath()%>";
      thisurl = thisurl + "/jsp/Dividend/WarrantDetails.jsp?dateselect=" + pdate;
      thisurl = thisurl + "&orderby=" + ptype;
      thisurl = thisurl + "&StartValue=" + pstart;
      thisurl = thisurl + "&EndValue=" + pend;
      thisurl = thisurl + "&stoppingdate=" + document.all.mstopdate.value;

      document.forms[0].pstatus.value = thisurl;
      document.forms[0].lstatus.value = "previous";
      document.forms[0].action = "MarkLocked.jsp";
      document.forms[0].submit();
    }
    else
    {
     ShowAllAlertMsg();
     return false;
    }
  }
  else
  {
   var thisurl = "<%=request.getContextPath()%>";
   thisurl = thisurl + "/jsp/Dividend/WarrantDetails.jsp?dateselect=" + pdate;
   thisurl = thisurl + "&orderby=" + ptype;
   thisurl = thisurl + "&StartValue=" + pstart;
   thisurl = thisurl + "&EndValue=" + pend;
   thisurl = thisurl + "&stoppingdate=" + document.all.mstopdate.value;

   document.forms[0].pstatus.value = thisurl;
   document.forms[0].lstatus.value = "previous";
   document.forms[0].action = "MarkLocked.jsp";
   document.forms[0].submit();
  }
}

function goNext(ndate,ntype,nstart,nend)
{
  if (check1 > 0)
  {
    count = 0;
    BlankValidate('mstopdate','- Stop Date (Option must be entered)');

    if (count == 0)
    {
     var thisurl = "<%=request.getContextPath()%>";
     thisurl = thisurl + "/jsp/Dividend/WarrantDetails.jsp?dateselect=" + ndate;
     thisurl = thisurl + "&orderby=" + ntype;
     thisurl = thisurl + "&StartValue=" + nstart;
     thisurl = thisurl + "&EndValue=" + nend;
     thisurl = thisurl + "&stoppingdate=" + document.all.mstopdate.value;

     document.forms[0].lstatus.value = "next";
     document.forms[0].pstatus.value = thisurl;
     document.forms[0].action = "MarkLocked.jsp";
     document.forms[0].submit();
    }
    else
    {
     ShowAllAlertMsg();
     return false;
    }
  }
  else
  {
   var thisurl = "<%=request.getContextPath()%>";
   thisurl = thisurl + "/jsp/Dividend/WarrantDetails.jsp?dateselect=" + ndate;
   thisurl = thisurl + "&orderby=" + ntype;
   thisurl = thisurl + "&StartValue=" + nstart;
   thisurl = thisurl + "&EndValue=" + nend;
   thisurl = thisurl + "&stoppingdate=" + document.all.mstopdate.value;

   document.forms[0].lstatus.value = "next";
   document.forms[0].pstatus.value = thisurl;
   document.forms[0].action = "MarkLocked.jsp";
   document.forms[0].submit();
  }
}

function lockwarrants(ndate,ntype,nstart,nend)
{
  if (confirm("Do you want to Lock the selected Warrants?"))
  {
    count = 0;
    BlankValidate('mstopdate','- Stop Date (Option must be entered)');

    if (count == 0)
    {
      var thisurl = "<%=request.getContextPath()%>";
      thisurl = thisurl + "/jsp/Dividend/WarrantDetails.jsp?dateselect=" + ndate;
      thisurl = thisurl + "&orderby=" + ntype;
      thisurl = thisurl + "&StartValue=" + nstart;
      thisurl = thisurl + "&EndValue=" + nend;
      thisurl = thisurl + "&stoppingdate=" + document.all.mstopdate.value;

      document.forms[0].pstatus.value = thisurl;
      document.forms[0].lstatus.value = "lock";
      document.forms[0].action = "MarkLocked.jsp";
      document.forms[0].submit();
    }
    else
    {
     ShowAllAlertMsg();
     return false;
    }
  }
}

function newwarrantno(sdate,stype,sstart,send)
{
  if (confirm("Do you want to assign New Warrant No. to Locked Warrants?"))
  {
    var thisurl = "<%=request.getContextPath()%>";
    thisurl = thisurl + "/jsp/Dividend/WarrantDetails.jsp?dateselect=" + sdate;
    thisurl = thisurl + "&orderby=" + stype;
    thisurl = thisurl + "&StartValue=" + sstart;
    thisurl = thisurl + "&EndValue=" + send;
    thisurl = thisurl + "&stoppingdate=" + document.all.mstopdate.value;

    document.forms[0].pstatus.value = thisurl;
    document.forms[0].action = "AssignNewWarrantNo.jsp";
    document.forms[0].submit();
  }
}

function stoppaymentletterprint(ndate,ntype,nstart,nend)
{
  if (confirm("Do you want to Issue Stop Payment Letter to Bank?"))
  {
    var thisurl = "<%=request.getContextPath()%>";
    thisurl = thisurl + "/jsp/Dividend/WarrantDetails.jsp?dateselect=" + ndate;
    thisurl = thisurl + "&orderby=" + ntype;
    thisurl = thisurl + "&StartValue=" + nstart;
    thisurl = thisurl + "&EndValue=" + nend;
    thisurl = thisurl + "&stoppingdate=" + document.all.mstopdate.value;

    document.forms[0].pstatus.value = thisurl;
    document.forms[0].action = "<%=request.getContextPath()%>/DividendStopPaymentLetter.jsp";
    document.forms[0].submit();
  }
}

function warrantprint(ndate,ntype)
{
  if (confirm("Do you want to Print the Warrant Letter(s)?"))
  {
    location = "<%=request.getContextPath()%>" + "/DividendLetters.jsp?pagename=warrantletter&divdate=" + ndate + "&divtype=" + ntype + "&stoppingdate=" + document.all.mstopdate.value;
  }
}

function bankpaymentletterprint(ndate,ntype,nstart,nend)
{
  if (confirm("Do you want to Print Payment Letter to Bank?"))
  {
    var thisurl = "<%=request.getContextPath()%>";
    thisurl = thisurl + "/jsp/Dividend/WarrantDetails.jsp?dateselect=" + ndate;
    thisurl = thisurl + "&orderby=" + ntype;
    thisurl = thisurl + "&StartValue=" + nstart;
    thisurl = thisurl + "&EndValue=" + nend;
    thisurl = thisurl + "&stoppingdate=" + document.all.mstopdate.value;

    document.forms[0].pstatus.value = thisurl;
    document.forms[0].action = "<%=request.getContextPath()%>/DividendBankPaymentLetter.jsp";
    document.forms[0].submit();
  }
}

function gosearch(sdate,stype,sstart,send)
{
  if (document.all.searchWarrant.value.length > 0)
  {
    if (check1 > 0)
    {
      count = 0;
      BlankValidate('mstopdate','- Stop Date (Option must be entered)');

      if (count == 0)
      {
       var thisurl = "<%=request.getContextPath()%>";
       thisurl = thisurl + "/jsp/Dividend/WarrantDetails.jsp?dateselect=" + sdate;
       thisurl = thisurl + "&orderby=" + stype;
       thisurl = thisurl + "&StartValue=" + sstart;
       thisurl = thisurl + "&EndValue=" + send;
       thisurl = thisurl + "&stoppingdate=" + document.all.mstopdate.value;

       document.forms[0].pstatus.value = thisurl;
       document.forms[0].action = "MarkLocked.jsp";
       document.forms[0].submit();
      }
      else
      {
       ShowAllAlertMsg();
       return false;
      }
    }
    else
    {
      var thisurl = "<%=request.getContextPath()%>";
      thisurl = thisurl + "/jsp/Dividend/WarrantDetails.jsp?dateselect=" + sdate;
      thisurl = thisurl + "&orderby=" + stype;
      thisurl = thisurl + "&StartValue=" + sstart;
      thisurl = thisurl + "&EndValue=" + send;
      thisurl = thisurl + "&stoppingdate=" + document.all.mstopdate.value;

      document.forms[0].pstatus.value = thisurl;
      document.forms[0].action = "MarkLocked.jsp";
      document.forms[0].submit();
    }
  }
}

 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

 function putstopdate()
 {
   check1++;
 }

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style17 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String DivDate = String.valueOf(request.getParameter("dateselect"));
    String DivType = String.valueOf(request.getParameter("orderby"));
    String SStartingValue = String.valueOf(request.getParameter("StartValue"));
    String SEndingValue = String.valueOf(request.getParameter("EndValue"));
    String SStopDate = String.valueOf(request.getParameter("stoppingdate"));

    if (String.valueOf(SStopDate).equals("null"))
      SStopDate = "";

    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    EndingValue = StartingValue + Chunk - 1;

    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;

    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }

    if (DivType.equalsIgnoreCase("SMS"))
    {
    String query1cc = "SELECT * FROM DIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' ORDER BY SUBSTR(FOLIO_NO,3,6)";
    int fcc = cm.queryExecuteCount(query1cc);

    if (nStartingValue > (fcc-Chunk+1))
    {
      nStartingValue = fcc - Chunk + 1;
    }
    if (nEndingValue > fcc)
    {
      nEndingValue = fcc;
    }
    if (nStartingValue < 1)
    {
      nStartingValue = 1;
    }
    if (nEndingValue < Chunk)
    {
      nEndingValue = Chunk;
    }

    String query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM DIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' ORDER BY SUBSTR(FOLIO_NO,3,6)) div where rownum <= " + EndingValue + ") where rnum >= " + StartingValue + "";
    cm.queryExecute(query1);

    String foliono = "";
    String warrantno = "";
    String isCollected = "";
    String isLocked = "";
    String stopdate = "";
    String letterissued = "";
    String warrantissued = "";
    String remarks = "";
    String paymentletter = "";
    String paymentletterdate = "";
    int rowcounter1 = 0;
%>
  <span class="style7">
  <form method="GET" action="MarkLocked.jsp">
  <SPAN id="dprint">
    <img name="B4" src="<%=request.getContextPath()%>/images/btnLock.gif" onclick="lockwarrants('<%=DivDate%>','<%=DivType%>',<%=StartingValue%>,<%=EndingValue%>)" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnLockR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnLock.gif'">
    <img name="B6" src="<%=request.getContextPath()%>/images/btnIssueStopPaymentLetter.gif" onclick="stoppaymentletterprint('<%=DivDate%>','<%=DivType%>',<%=StartingValue%>,<%=EndingValue%>)" onMouseOver="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnIssueStopPaymentLetterR.gif'" onMouseOut="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnIssueStopPaymentLetter.gif'">
    <img name="B7" src="<%=request.getContextPath()%>/images/btnNewWarrantNo.gif" onclick="newwarrantno('<%=DivDate%>','<%=DivType%>',<%=StartingValue%>,<%=EndingValue%>)" onMouseOver="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnNewWarrantNoR.gif'" onMouseOut="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnNewWarrantNo.gif'">
    <img name="B8" src="<%=request.getContextPath()%>/images/btnPrintLetterWarrant.gif" onclick="warrantprint('<%=DivDate%>','<%=DivType%>')" onMouseOver="document.forms[0].B8.src = '<%=request.getContextPath()%>/images/btnPrintLetterWarrantR.gif'" onMouseOut="document.forms[0].B8.src = '<%=request.getContextPath()%>/images/btnPrintLetterWarrant.gif'">
    <img name="B9" src="<%=request.getContextPath()%>/images/btnPaymentLetterBank.gif" onclick="bankpaymentletterprint('<%=DivDate%>','<%=DivType%>',<%=StartingValue%>,<%=EndingValue%>)" onMouseOver="document.forms[0].B9.src = '<%=request.getContextPath()%>/images/btnPaymentLetterBankR.gif'" onMouseOut="document.forms[0].B9.src = '<%=request.getContextPath()%>/images/btnPaymentLetterBank.gif'">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
  </SPAN>
  <table width="100%" BORDER=0  cellpadding="0" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td height="30" bgcolor="#0044B0" class="style17" valign="middle"><center>Dividend Warrant Details - SMS [<%=DivDate%>]</center></td></tr>
  </table>
  <table width="100%"  border="0" cellpadding="0" cellspacing="0" bordercolor="#0044B0">
      <tr bgcolor="#E8F3FD" valign="middle">
	<td width="12%" valign="middle" align="center" style="border-left: solid 1px #0044B0;"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goPrevious('<%=DivDate%>','<%=DivType%>',<%=pStartingValue%>,<%=pEndingValue%>)"></td>
	<td width="8%" valign="middle"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNext('<%=DivDate%>','<%=DivType%>',<%=nStartingValue%>,<%=nEndingValue%>)"></td>
	<td width="24%" class="style19" valign="middle">
         Stop Date :&nbsp;
         <input name="mstopdate" type=text id="mstopdate" value="<%=SStopDate%>" class="SL75TextField" onfocus="this.blur()">
         &nbsp
         <a href="javascript:NewCal('mstopdate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
          &nbsp;
        </td>
	<td width="25%" height="35" class="style19" align="right" valign="middle">
            <input name="forw" type="radio" value="folio">&nbsp;Folio No.
            &nbsp;<input name="forw" type="radio" value="warrant" checked="checked">&nbsp;Warrant No.&nbsp;
        </td>
	<td width="19%" valign="middle" align="center"><input name="searchWarrant" type="text" class="rhakim" onkeypress="keypressOnNumberFld()"></td>
	<td width="12%" valign="middle" align="left" style="border-right: solid 1px #0044B0;"><img src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="gosearch('<%=DivDate%>','<%=DivType%>',<%=StartingValue%>,<%=EndingValue%>)"></td>
      </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="15%" class="style9"><div align="center">Folio No.</div></td>
    <td width="14%" class="style9"><div align="center">Date</div></td>
    <td width="14%" class="style9"><div align="center">Warrant No.</div></td>
    <td width="10%" class="style9"><div align="center">Lock</div></td>
    <td width="15%"><div align="center" class="style9">
      <div align="center">Stop Date</div>
    </div></td>
    <td width="17%"><div align="center" class="style9">
      <div align="center">Bank Payment Letter</div>
    </div></td>
    <td width="15%"><div align="center" class="style9">
      <div align="center">New Warrant No.</div>
    </div></td>
  </tr>
  <div align="left">
     <%
    while(cm.toNext())
     {
       rowcounter1++;
       foliono = cm.getColumnS("FOLIO_NO");
       warrantno = cm.getColumnS("WARRANT_NO");
       isLocked = cm.getColumnS("LOCKED");
       isCollected = cm.getColumnS("COLLECTED");
       stopdate = cm.getColumnDT("STOP_DATE");
       letterissued = cm.getColumnS("ISSUED_LETTER");
       warrantissued = cm.getColumnS("ISSUED_WARRANT");
       remarks = cm.getColumnS("REMARKS");
       paymentletter = cm.getColumnS("PAYMENT_LETTER");
       paymentletterdate = cm.getColumnDT("PAYMENT_LETTER_DATE");

       if (String.valueOf(foliono).equals("null"))
         foliono = "";
       if (String.valueOf(warrantno).equals("null"))
         warrantno = "";
       if (String.valueOf(isLocked).equals("null"))
         isLocked = "";
       if (String.valueOf(isCollected).equals("null"))
         isCollected = "";
       if (String.valueOf(stopdate).equals("null"))
         stopdate = "";
       if (String.valueOf(letterissued).equals("null"))
         letterissued = "";
       if (String.valueOf(warrantissued).equals("null"))
         warrantissued = "";
       if (String.valueOf(remarks).equals("null"))
         remarks = "";
       if (String.valueOf(paymentletter).equals("null"))
         paymentletter = "";
       if (String.valueOf(paymentletterdate).equals("null"))
         paymentletterdate = "";

       String uletterissued = "";
       String uwarrantissued = "";
       if (letterissued.equalsIgnoreCase("T"))
       {
         uletterissued = "Yes";
       }
       else if (letterissued.equalsIgnoreCase("F"))
       {
         uletterissued = "No";
       }
       if (warrantissued.equalsIgnoreCase("T"))
       {
         uwarrantissued = remarks;
       }
       else if (warrantissued.equalsIgnoreCase("F"))
       {
         uwarrantissued = "---";
       }
     %>
    </div>
  <tr bgcolor="#E8F3FD">
            <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=foliono%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=DivDate%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=warrantno%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">
             <%
               if (isLocked.equalsIgnoreCase("T"))
               {
               %>
                 <input type="checkbox" name="selectingbox<%=rowcounter1%>" value="<%=warrantno%>" checked="checked">
               <%
               }
               else if (isLocked.equalsIgnoreCase("F"))
               {
               %>
                 <input type="checkbox" name="selectingbox<%=rowcounter1%>" value="<%=warrantno%>" onclick="putstopdate()">
               <%
               }
             %>
             </div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">
                 <%=stopdate%>
             </div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=paymentletterdate%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=uwarrantissued%>&nbsp;</div>
           </div></td>
         </tr>
         <div align="left" class="style13">
             <%
         }
         %>
           <!--HIDDEN FIELDS-->
            <input type="hidden" name="rowcounter1" value="<%=rowcounter1%>">
            <input type="hidden" name="ddate" value="<%=DivDate%>">
            <input type="hidden" name="dtype" value="<%=DivType%>">
            <input type="hidden" name="svalue" value="<%=StartingValue%>">
            <input type="hidden" name="evalue" value="<%=EndingValue%>">
            <input type="hidden" name="pstatus" value="close">
            <input type="hidden" name="lstatus" value="something">
            <input type="hidden" name="pchunk" value="<%=Chunk%>">
         <%
    }
    else if (DivType.equalsIgnoreCase("CDBL"))
    {
      String query1cc = "SELECT * FROM BODIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' ORDER BY BO_ID";
      int fcc = cm.queryExecuteCount(query1cc);

      if (nStartingValue > (fcc-Chunk+1))
      {
        nStartingValue = fcc - Chunk + 1;
      }
      if (nEndingValue > fcc)
      {
        nEndingValue = fcc;
      }
      if (nStartingValue < 1)
      {
        nStartingValue = 1;
      }
      if (nEndingValue < Chunk)
      {
        nEndingValue = Chunk;
      }

    String query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM BODIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' ORDER BY BO_ID) div where rownum <= " + EndingValue + ") where rnum >= " + StartingValue + "";
    cm.queryExecute(query1);

    String boid = "";
    String warrantno = "";
    String isCollected = "";
    String isLocked = "";
    String stopdate = "";
    String letterissued = "";
    String warrantissued = "";
    String remarks = "";
    String paymentletter = "";
    String paymentletterdate = "";
    int rowcounter1 = 0;
%>
  <span class="style7">
  <form method="GET" action="MarkLocked.jsp">
  <SPAN id="dprint">
    <img name="B4" src="<%=request.getContextPath()%>/images/btnLock.gif" onclick="lockwarrants('<%=DivDate%>','<%=DivType%>',<%=StartingValue%>,<%=EndingValue%>)" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnLockR.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnLock.gif'">
    <img name="B6" src="<%=request.getContextPath()%>/images/btnIssueStopPaymentLetter.gif" onclick="stoppaymentletterprint('<%=DivDate%>','<%=DivType%>',<%=StartingValue%>,<%=EndingValue%>)" onMouseOver="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnIssueStopPaymentLetterR.gif'" onMouseOut="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnIssueStopPaymentLetter.gif'">
    <img name="B7" src="<%=request.getContextPath()%>/images/btnNewWarrantNo.gif" onclick="newwarrantno('<%=DivDate%>','<%=DivType%>',<%=StartingValue%>,<%=EndingValue%>)" onMouseOver="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnNewWarrantNoR.gif'" onMouseOut="document.forms[0].B7.src = '<%=request.getContextPath()%>/images/btnNewWarrantNo.gif'">
    <img name="B8" src="<%=request.getContextPath()%>/images/btnPrintLetterWarrant.gif" onclick="warrantprint('<%=DivDate%>','<%=DivType%>')" onMouseOver="document.forms[0].B8.src = '<%=request.getContextPath()%>/images/btnPrintLetterWarrantR.gif'" onMouseOut="document.forms[0].B8.src = '<%=request.getContextPath()%>/images/btnPrintLetterWarrant.gif'">
    <img name="B9" src="<%=request.getContextPath()%>/images/btnPaymentLetterBank.gif" onclick="bankpaymentletterprint('<%=DivDate%>','<%=DivType%>',<%=StartingValue%>,<%=EndingValue%>)" onMouseOver="document.forms[0].B9.src = '<%=request.getContextPath()%>/images/btnPaymentLetterBankR.gif'" onMouseOut="document.forms[0].B9.src = '<%=request.getContextPath()%>/images/btnPaymentLetterBank.gif'">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
  </SPAN>
  <table width="100%" BORDER=0  cellpadding="0" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td height="30" bgcolor="#0044B0" class="style17" valign="middle"><center>Dividend Warrant Details - CDBL [<%=DivDate%>]</center></td></tr>
  </table>
  <table width="100%"  border="0" cellpadding="0" cellspacing="0" bordercolor="#0044B0">
      <tr bgcolor="#E8F3FD">
	<td width="12%" align="center" style="border-left: solid 1px #0044B0;"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goPrevious('<%=DivDate%>','<%=DivType%>',<%=pStartingValue%>,<%=pEndingValue%>)"></td>
	<td width="8%" align="left"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNext('<%=DivDate%>','<%=DivType%>',<%=nStartingValue%>,<%=nEndingValue%>)"></td>
	<td width="24%" class="style19" valign="middle">
         Stop Date :&nbsp;
         <input name="mstopdate" type=text id="mstopdate" value="<%=SStopDate%>" class="SL75TextField" onfocus="this.blur()">
         &nbsp
         <a href="javascript:NewCal('mstopdate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
          &nbsp;
        </td>
	<td width="25%" height="35" class="style19" align="right">
            <input name="forw" type="radio" value="folio">&nbsp;BO ID
            &nbsp;<input name="forw" type="radio" value="warrant" checked="checked">&nbsp;Warrant No.&nbsp;
        </td>
	<td width="19%" align="center"><input name="searchWarrant" type="text" class="rhakim" onkeypress="keypressOnNumberFld()"></td>
	<td width="12%" align="left" style="border-right: solid 1px #0044B0;"><img src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="gosearch('<%=DivDate%>','<%=DivType%>',<%=StartingValue%>,<%=EndingValue%>)"></td>
      </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="15%" class="style9"><div align="center">BO ID</div></td>
    <td width="14%" class="style9"><div align="center">Date</div></td>
    <td width="15%" class="style9"><div align="center">Warrant No.</div></td>
    <td width="10%" class="style9"><div align="center">Lock</div></td>
    <td width="12%"><div align="center" class="style9">
      <div align="center">Stop Date</div>
    </div></td>
    <td width="17%"><div align="center" class="style9">
      <div align="center">Bank Payment Letter</div>
    </div></td>
    <td width="17%"><div align="center" class="style9">
      <div align="center">New Warrant No.</div>
    </div></td>
  </tr>
  <div align="left">
     <%
    while(cm.toNext())
     {
       rowcounter1++;
       boid = cm.getColumnS("BO_ID");
       warrantno = cm.getColumnS("WARRANT_NO");
       isLocked = cm.getColumnS("LOCKED");
       isCollected = cm.getColumnS("COLLECTED");
       stopdate = cm.getColumnDT("STOP_DATE");
       letterissued = cm.getColumnS("ISSUED_LETTER");
       warrantissued = cm.getColumnS("ISSUED_WARRANT");
       remarks = cm.getColumnS("REMARKS");
       paymentletter = cm.getColumnS("PAYMENT_LETTER");
       paymentletterdate = cm.getColumnDT("PAYMENT_LETTER_DATE");

       if (String.valueOf(boid).equals("null"))
         boid = "";
       if (String.valueOf(warrantno).equals("null"))
         warrantno = "";
       if (String.valueOf(isLocked).equals("null"))
         isLocked = "";
       if (String.valueOf(isCollected).equals("null"))
         isCollected = "";
       if (String.valueOf(stopdate).equals("null"))
         stopdate = "";
       if (String.valueOf(letterissued).equals("null"))
         letterissued = "";
       if (String.valueOf(warrantissued).equals("null"))
         warrantissued = "";
       if (String.valueOf(remarks).equals("null"))
         remarks = "";
       if (String.valueOf(paymentletter).equals("null"))
         paymentletter = "";
       if (String.valueOf(paymentletterdate).equals("null"))
         paymentletterdate = "";

       String uletterissued = "";
       String uwarrantissued = "";
       if (letterissued.equalsIgnoreCase("T"))
       {
         uletterissued = "Yes";
       }
       else if (letterissued.equalsIgnoreCase("F"))
       {
         uletterissued = "No";
       }
       if (warrantissued.equalsIgnoreCase("T"))
       {
         uwarrantissued = remarks;
       }
       else if (warrantissued.equalsIgnoreCase("F"))
       {
         uwarrantissued = "---";
       }
     %>
    </div>
  <tr bgcolor="#E8F3FD">
            <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=boid%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=DivDate%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=warrantno%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center">
             <%
               if (isLocked.equalsIgnoreCase("T"))
               {
               %>
                 <input type="checkbox" name="selectingbox<%=rowcounter1%>" value="<%=warrantno%>" checked="checked">
               <%
               }
               else if (isLocked.equalsIgnoreCase("F"))
               {
               %>
                 <input type="checkbox" name="selectingbox<%=rowcounter1%>" value="<%=warrantno%>" onclick="putstopdate()">
               <%
               }
             %>
             </div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=stopdate%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=paymentletterdate%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center">&nbsp;<%=uwarrantissued%>&nbsp;</div>
           </div></td>
         </tr>
         <div align="left" class="style13">
             <%
         }
         %>
           <!--HIDDEN FIELDS-->
            <input type="hidden" name="rowcounter1" value="<%=rowcounter1%>">
            <input type="hidden" name="ddate" value="<%=DivDate%>">
            <input type="hidden" name="dtype" value="<%=DivType%>">
            <input type="hidden" name="svalue" value="<%=StartingValue%>">
            <input type="hidden" name="evalue" value="<%=EndingValue%>">
            <input type="hidden" name="pstatus" value="close">
            <input type="hidden" name="lstatus" value="something">
            <input type="hidden" name="pchunk" value="<%=Chunk%>">
         <%
    }

  if (isConnect)
   {
     cm.takeDown();
   }
  %>
         </div>
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>

</body>
</html>
