<script>
/*
'******************************************************************************************************************************************
'Script Author : Md.Kamruzzaman
'Creation Date : August 2007
'Page Purpose  : View of all Account list
****************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style19 {color: #0A2769; font-weight: bold; }
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Date</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--

function goNextPrevious(pstart,pend,fdate)
{
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Dividend/MergeAccounts.jsp?StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;
  thisurl = thisurl + "&searchDate="+fdate;
  location = thisurl;
}


//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   int com_state=0;
   int COUNT;
   int MAX_RECORDS=20;
   String sdate=request.getParameter("searchDate");
 //System.out.println(sdate);


   String com_no="";
   String csubject = "";
   String cdate = "";
   String cfolio = "";
   String cshareholder = "";
   String ccomplaint = "";
   String cassigned = "";
   String ctime = "";
   String cstatus="";
   String ccomment="";
   String dest="";
   String mergeDate="";
   String decDate="";
   String merged="";
 //Used for NEXT/PREVIOUS
    String SStartingValue = String.valueOf(request.getParameter("StartValue"));
    String SEndingValue = String.valueOf(request.getParameter("EndValue"));


    if(SStartingValue == null || SStartingValue.equals("") || SStartingValue.equals("null")) SStartingValue="1";
    if(SEndingValue == null || SEndingValue.equals("") || SEndingValue.equals("null")) SEndingValue="0";

    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    int Chunk = MAX_RECORDS;
    EndingValue = StartingValue + Chunk - 1;
    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;

    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }

    if (String.valueOf(sdate).equals("null"))
      sdate = "";
   String query1="";

     if (sdate.length()<=0)
     {
       query1 = "SELECT DATE_DEC,MERGED_DATE,ACC_MERGED FROM (SELECT div.*, rownum rnum FROM(select DATE_DEC,MERGED_DATE,ACC_MERGED from DIVIDENDDECLARATION_VIEW order by DATE_DEC DESC) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;

     }
     else
     {
       query1 = "SELECT DATE_DEC,MERGED_DATE,ACC_MERGED FROM (SELECT div.*, rownum rnum FROM(select DATE_DEC,MERGED_DATE,ACC_MERGED from DIVIDENDDECLARATION_VIEW where DATE_DEC<=TO_DATE('" + sdate + "','DD/MM/YYYY')  order by DATE_DEC DESC) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;
     }

  //System.out.println(query1);

   cm.queryExecute(query1);
   COUNT=0;
   while(cm.toNext())
   {
     COUNT++;
   }
   cm.queryExecute(query1);


%>
  <span class="style7">
  <form method="GET" action="MergeAccounts.jsp">
  <table width="100%" BORDER=1  cellpadding="0" bordercolor="#06689E" style="border-collapse: collapse" bordercolor="#06689E">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7" height="30"><center>Merge Accounts</center></td></tr>
  <tr>
  	<td>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0">
			<tr bgcolor="#E8F3FD">
				<td width="10%"></td>
				<td width="10%"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goNextPrevious(<%=pStartingValue%>,<%=pEndingValue%>,'<%=sdate%>')"></td>
				<td width="10%"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNextPrevious(<%=nStartingValue%>,<%=nEndingValue%>,'<%=sdate%>')"></td>
				<td width="12%" class="style19"> Date :</td>
				<td width="25%">
                                  <div align="left">
                                          <input name="searchDate" type=text id="searchDate" value="" maxlength="10" class="SL2TextField" onFocus="this.blur();">
                                              &nbsp
                                          <a href="javascript:NewCal('searchDate','ddmmyyyy',false,12)">
                                            <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
                                           </a>
                                    </div>
                                        <input type="hidden" name="hdate" value="<%=sdate%>"/>
                                </td>
				<td width="20%"><img src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="SubmitThis()"></td>
			</tr>
                </table>
	</td>
  </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="0044B0">
  <tr bgcolor="#0044B0">
    <td width="45%" class="style9" align="center">Dividend Declaration Date</td>
    <td width="45%" class="style9" align="center">Merge Date</td>
    <td width="10%" class="style9" align="center">Merged</td>
  </tr>
  <div align="left">
     <%
     com_state=0;
     int counter1=0;
    while(cm.toNext() && COUNT!=0)
     {
       com_state++;
       counter1++;
       decDate=cm.getColumnDT("DATE_DEC");
       mergeDate =cm.getColumnDT("MERGED_DATE");
       merged = cm.getColumnS("ACC_MERGED");


       dest = request.getContextPath() + "/jsp/Dividend/EditMerge.jsp";

       if (!decDate.equals("null"))
       {
         if (String.valueOf(mergeDate).equals("null"))
           mergeDate = "";
         if (String.valueOf(merged).equals("null"))
           merged = "N";
         %>
    </div>
    <tr bgcolor="#E8F3FD">
           <td ><div align="center" class="style10">
             <div align="center" class="style10">
               <span class="style13">
                  <a HREF="<%=dest%>?decdate=<%=decDate%>"><%=decDate%></a>
               &nbsp;
               <%
               if(com_state==COUNT)
               {
               %>
                 <input type="hidden" name="svalue" value="<%=StartingValue%>">
                 <input type="hidden" name="evalue" value="<%=EndingValue%>">
              <%}%>
               </span></div>
             </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center"><%=mergeDate %></div>
           </div></td>
            <td class="style13"><div align="center" class="style12">
            <div align="center">
            <%
              if (merged.equalsIgnoreCase("Y"))
              {
               %>
                 <img name="B2" src="<%=request.getContextPath()%>/images/icoCheck.gif" alt="">
               <%
               }
               else
               {
                %>
                <img name="B3" src="<%=request.getContextPath()%>/images/icoCross.gif">
                <%
                }
                %>
                </div></div></td>
         </tr>
         <div align="left" class="style13">
             <%
         }
     }

   if (isConnect)
   {
     try
     {
       cm.takeDown();
     }
     catch(Exception e){}
   }
  %>
         </div>
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD" ><center>

</body>
</html>
