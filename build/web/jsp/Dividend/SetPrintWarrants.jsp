<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : December 2005
'Page Purpose  : Sets Criteria for Printing Warrants.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Print Warrants</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function canrangebyfolio()
{
 document.all.warrantspan1.style.display = 'none';
 document.all.warrantspan2.style.display = 'none';
 document.all.warrantspan3.style.display = 'none';
 document.all.warrantspan4.style.display = 'none';
 document.all.warrantspan5.style.display = 'none';
 document.all.warrantspan6.style.display = 'none';
 document.all.foliospan1.style.display = '';
 document.all.foliospan2.style.display = '';
 document.all.foliospan3.style.display = '';
 document.all.foliospan4.style.display = '';
 document.all.foliospan5.style.display = '';
 document.all.foliospan6.style.display = '';
}

function canrangebywarrant()
{
 document.all.foliospan1.style.display = 'none';
 document.all.foliospan2.style.display = 'none';
 document.all.foliospan3.style.display = 'none';
 document.all.foliospan4.style.display = 'none';
 document.all.foliospan5.style.display = 'none';
 document.all.foliospan6.style.display = 'none';
 document.all.warrantspan1.style.display = '';
 document.all.warrantspan2.style.display = '';
 document.all.warrantspan3.style.display = '';
 document.all.warrantspan4.style.display = '';
 document.all.warrantspan5.style.display = '';
 document.all.warrantspan6.style.display = '';
}

function closerangespans()
{
 document.all.foliospan1.style.display = 'none';
 document.all.foliospan2.style.display = 'none';
 document.all.foliospan3.style.display = 'none';
 document.all.foliospan4.style.display = 'none';
 document.all.foliospan5.style.display = 'none';
 document.all.foliospan6.style.display = 'none';
 document.all.warrantspan1.style.display = 'none';
 document.all.warrantspan2.style.display = 'none';
 document.all.warrantspan3.style.display = 'none';
 document.all.warrantspan4.style.display = 'none';
 document.all.warrantspan5.style.display = 'none';
 document.all.warrantspan6.style.display = 'none';
}

function closesinglespans()
{
 document.all.sspan1.style.display = 'none';
 document.all.sspan2.style.display = 'none';
 document.all.sspan3.style.display = 'none';
}

function closesinglewspans()
{
 document.all.swspan1.style.display = 'none';
 document.all.swspan2.style.display = 'none';
 document.all.swspan3.style.display = 'none';
}

function opensinglespans()
{
 document.all.sspan1.style.display = '';
 document.all.sspan2.style.display = '';
 document.all.sspan3.style.display = '';
}

function opensinglewspans()
{
 document.all.swspan1.style.display = '';
 document.all.swspan2.style.display = '';
 document.all.swspan3.style.display = '';
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  SelectValidate('dateselect','- Declaration Date (Option must be entered)');
  SelectValidate('sigselect','- Signature (Option must be entered)');

  var radio_choice;
  var counter;

  for (counter = 0; counter < document.forms[0].printrecords.length; counter++)
   {
    if (document.forms[0].printrecords[counter].checked)
    {
      radio_choice = document.forms[0].printrecords[counter].value;
    }
   }

  if (radio_choice == "rangebyfolio")
  {
    BlankValidate('foliostart','- Folio Range From (Option must be entered)');
    BlankValidate('folioend','- Folio Range To (Option must be entered)');
  }
  else if (radio_choice == "rangebywarrant")
  {
    BlankValidate('warrantstart','- Warrant Range From (Option must be entered)');
    BlankValidate('warrantend','- Warrant Range To (Option must be entered)');
  }
  else if (radio_choice == "singlebyfolio")
  {
    BlankValidate('sfoliono','- Folio No./BO ID (Option must be entered)');
  }
  else if (radio_choice == "singlebywarrant")
  {
    BlankValidate('swarrantno','- Warrant No. (Option must be entered)');
  }

  if (count == 0)
  {
    document.forms[0].submit();
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
%>

<form action="<%=request.getContextPath()%>/DividendWarrantLetter.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Print Warrants</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Type</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left" class="style9">
            <input name="dividendtype" type="radio" value="SMStype" checked="checked">&nbsp;SMS<br>
            <input name="dividendtype" type="radio" value="CDBLtype">&nbsp;CDBL
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date of Declaration</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="dateselect" class="SL1TextFieldListBox">
            <option>--- Please Select ---</option>
            <%
              String thedate = "";

              String query1 = "SELECT DISTINCT ISSUE_DATE FROM DIVIDEND_VIEW ORDER BY ISSUE_DATE DESC";
              cm.queryExecute(query1);

              while(cm.toNext())
              {
                 thedate = cm.getColumnDT("ISSUE_DATE");
                 %>
                   <option value="<%=thedate%>"><%=thedate%></option>
                 <%
              }
            %>
         </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Order By</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left" class="style9">
            <input name="orderby" type="radio" value="byname" checked="checked">&nbsp;Name<br>
            <input name="orderby" type="radio" value="byfolio">&nbsp;Folio/BO ID<br>
            <input name="orderby" type="radio" value="bydividend">&nbsp;Dividend
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Print Records</div></th>
        <td width="1%" valign="top">:</td>
        <td width="55%"><div align="left" class="style9">
            <input name="printrecords" type="radio" value="singlebyfolio" onclick="closerangespans(),opensinglespans(),closesinglewspans()">&nbsp;Single by Folio/BO<br>
            <input name="printrecords" type="radio" value="singlebywarrant" onclick="closerangespans(),closesinglespans(),opensinglewspans()">&nbsp;Single by Warrant<br>
            <input name="printrecords" type="radio" value="notprinted" checked="checked" onclick="closerangespans(),closesinglespans(),closesinglewspans()">&nbsp;All Not Printed<br>
            <input name="printrecords" type="radio" value="rangebyfolio" onclick="canrangebyfolio(),closesinglespans(),closesinglewspans()">&nbsp;Range by Folio<br>
            <input name="printrecords" type="radio" value="rangebywarrant" onclick="canrangebywarrant(),closesinglespans(),closesinglewspans()">&nbsp;Range by Warrant
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8"><SPAN id="sspan1" style="display:none">&nbsp;&nbsp;&nbsp;&nbsp;Folio No./BO ID</span>
          </div></th>
        <td><SPAN id="sspan2" style="display:none">:</span></td>
        <td><div align="left">
          <SPAN id="sspan3" style="display:none"><input type="text" name="sfoliono" class="SL2TextField" onkeypress="keypressOnNumberFld()"></span>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8"><SPAN id="swspan1" style="display:none">&nbsp;&nbsp;&nbsp;&nbsp;Warrant No.</span>
          </div></th>
        <td><SPAN id="swspan2" style="display:none">:</span></td>
        <td><div align="left">
          <SPAN id="swspan3" style="display:none"><input type="text" name="swarrantno" class="SL2TextField" onkeypress="keypressOnNumberFld()"></span>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8"><SPAN id="foliospan1" style="display:none">&nbsp;&nbsp;&nbsp;&nbsp;Folio Range</SPAN></div></th>
        <td width="1%" valign="top"><SPAN id="foliospan2" style="display:none">:</span></td>
        <td width="55%"><div align="left">
         <table width="75%"  border="0" cellpadding="2">
           <tr>
            <td width="30%" align="left"><div align="left" class="style8">
              <b><SPAN id="foliospan3" style="display:none">From&nbsp;&nbsp;</span></b></div>
            </td>
            <td width="70%" align="left"><div align="left" class="style8">
              <SPAN id="foliospan4" style="display:none">
              <input type="text" name="foliostart" class="SL67TextField" onkeypress="keypressOnNumberFld()"></span></div>
            </td>
           </tr>
           <tr>
            <td width="30%" align="left"><div align="left" class="style8">
              <b><SPAN id="foliospan5" style="display:none">To&nbsp;&nbsp;</span></b></div>
            </td>
            <td width="70%" align="left"><div align="left" class="style8">
              <SPAN id="foliospan6" style="display:none"><input type="text" name="folioend" class="SL67TextField" onkeypress="keypressOnNumberFld()"></span></div>
            </td>
           </tr>
        </table>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%" valign="top"><div align="left" class="style8"><SPAN id="warrantspan1" style="display:none">&nbsp;&nbsp;&nbsp;&nbsp;Warrant Range</SPAN></div></th>
        <td width="1%" valign="top"><SPAN id="warrantspan2" style="display:none">:</span></td>
        <td width="55%"><div align="left">
         <table width="75%"  border="0" cellpadding="2">
           <tr>
            <td width="30%" align="left"><div align="left" class="style8">
              <b><SPAN id="warrantspan3" style="display:none">From&nbsp;&nbsp;</span></b></div>
            </td>
            <td width="70%" align="left"><div align="left" class="style8">
              <SPAN id="warrantspan4" style="display:none">
              <input type="text" name="warrantstart" class="SL67TextField" onkeypress="keypressOnNumberFld()"></span></div>
            </td>
           </tr>
           <tr>
            <td width="30%" align="left"><div align="left" class="style8">
              <b><SPAN id="warrantspan5" style="display:none">To&nbsp;&nbsp;</span></b></div>
            </td>
            <td width="70%" align="left"><div align="left" class="style8">
              <SPAN id="warrantspan6" style="display:none"><input type="text" name="warrantend" class="SL67TextField" onkeypress="keypressOnNumberFld()"></span></div>
            </td>
           </tr>
        </table>
        </div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Signature</div></th>
        <td width="1%">:</td>
        <td><div align="left">
         <select name="sigselect" class="SL3TextFieldListBox">
            <option>--- Please Select ---</option>
            <%
              String signame = "";
              String sigid = "";
              String sigrank = "";

              String query2 = "SELECT * FROM SIGNATURE_VIEW ORDER BY SIG_ID";
              cm.queryExecute(query2);

              while(cm.toNext())
              {
                 signame = cm.getColumnS("NAME");
                 sigid = cm.getColumnS("SIG_ID");
                 sigrank = cm.getColumnS("RANK");
                 %>
                   <option value="<%=sigid%>"><%=signame%> (<%=sigrank%>)</option>
                 <%
              }
            %>
         </select>
        </div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="50%" scope="row">
            <div align="right">
	      <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
            </div></th>
        <td width="50%" align="left">
 	      <img name="B1" src="<%=request.getContextPath()%>/images/btnOK.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOKOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnOK.gif'">
      </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
