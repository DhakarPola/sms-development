<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : December 2005
'Page Purpose  : Marks the selected Warrant Numbers.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Mark Selected Warrants</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%

   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String rowcounter = String.valueOf(request.getParameter("rowcounter1"));
  String Destination = String.valueOf(request.getParameter("pstatus"));
  String DivDate = String.valueOf(request.getParameter("ddate"));
  String DivType = String.valueOf(request.getParameter("dtype"));
  String SMSType = String.valueOf(request.getParameter("smstype1"));
  String Warrantno = String.valueOf(request.getParameter("searchWarrant"));
  String Chunk = String.valueOf(request.getParameter("pchunk"));
  String ForW = String.valueOf(request.getParameter("forw"));
  String ColDate = String.valueOf(request.getParameter("mrecondate"));
  // System.out.println("---------------SMS--------------------------------------------------------------"+SMSType);
  int iChunk = Integer.parseInt(Chunk);

  Warrantno=cm.replace(Warrantno,"'","''");
  if (String.valueOf(Warrantno).equals("null"))
  {
    Warrantno = "";
  }
  if (String.valueOf(ColDate).equals("null"))
  {
    ColDate = "";
  }

  if (!Warrantno.equalsIgnoreCase(""))
  {
      String ifwarexists = "";
      String countfolios = "";



     System.out.println("Marked page ");
      System.out.println(DivType);

      if (DivType.equalsIgnoreCase("SMS"))
      {


      //  System.out.println("---------------SMS--------------------------------------------------------------");

        if (SMSType.equalsIgnoreCase("Group"))
        {
          ifwarexists = "SELECT * FROM DIVIDEND_GROUP_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' AND WARRANT_NO='"+ Warrantno+"'";
          countfolios = "SELECT * FROM DIVIDEND_GROUP_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F'";
        }
        else if (SMSType.equalsIgnoreCase("Individual"))
        {
          if (ForW.equalsIgnoreCase("folio"))
          {
            ifwarexists = "SELECT * FROM DIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' AND FOLIO_NO='"+ Warrantno+"'";
          }
          else if (ForW.equalsIgnoreCase("warrant"))
          {
            ifwarexists = "SELECT * FROM DIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' AND WARRANT_NO='"+ Warrantno+"'";
          }

          countfolios = "SELECT * FROM DIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F'";
        }
      }



       else if (DivType.equalsIgnoreCase("CDBL"))
      {
        System.out.println("CDBL");
        System.out.println("SMSType--"+SMSType);
        if (SMSType.equalsIgnoreCase("Group"))
        {
           System.out.println("Group");
          //ifwarexists = "SELECT * FROM DIVIDEND_GROUP_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' AND WARRANT_NO='"+ Warrantno+"'";
          ifwarexists = "select * from OMNIBUS_BO_DIVIDEND o,BODIVIDEND d,omnibus_config_dividend,omnibus_config con where d.ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') and o.warrent_no= d.warrant_no and omnibus_config_dividend.rec_id= o.omnibus_div_id and con.rec_id=o.omnibus_ac_id and omnibus_config_dividend.status='ACCEPTED' and d.COLLECTED = 'F' AND d.LOCKED = 'F' AND d.WARRANT_NO='"+ Warrantno+"'";
		  System.out.println(ifwarexists);
		  //countfolios = "SELECT * FROM DIVIDEND_GROUP_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F'";
          countfolios = "select * from OMNIBUS_BO_DIVIDEND o,BODIVIDEND d,omnibus_config_dividend,omnibus_config con where d.ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') and o.warrent_no= d.warrant_no and omnibus_config_dividend.rec_id= o.omnibus_div_id and con.rec_id=o.omnibus_ac_id and omnibus_config_dividend.status='ACCEPTED' and d.COLLECTED = 'F' AND d.LOCKED = 'F'";
		 System.out.println(countfolios);
          }




      else if (SMSType.equalsIgnoreCase("Individual"))
      {
      // System.out.println("---------------Individual");
       if (ForW.equalsIgnoreCase("folio"))
       {
        ifwarexists = "SELECT * FROM BODIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' AND BO_ID = '"+ Warrantno+"' AND warrant_no not in(Select OMNIBUS_BO_DIVIDEND.warrent_no from BODiVIDEND,OMNIBUS_BO_DIVIDEND, omnibus_config_dividend where BODiVIDEND.warrant_no=OMNIBUS_BO_DIVIDEND.warrent_no and omnibus_config_dividend.rec_id=OMNIBUS_BO_DIVIDEND.omnibus_div_id and OMNIBUS_BO_DIVIDEND.ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') UNION Select OMNIBUS_BO_DIVIDEND_T.warrant_no from BODiVIDEND,OMNIBUS_BO_DIVIDEND_T, omnibus_config_dividend  where BODiVIDEND.warrant_no=OMNIBUS_BO_DIVIDEND_T.warrant_no and omnibus_config_dividend.rec_id=OMNIBUS_BO_DIVIDEND_T.omnibus_div_id and OMNIBUS_BO_DIVIDEND_T.ISSUE_DATE =  TO_DATE('" + DivDate + "','DD/MM/YYYY'))";
       }
       else if (ForW.equalsIgnoreCase("warrant"))
       {
         String s="SELECT * FROM BODIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' AND WARRANT_NO = '"+ Warrantno+"' AND warrant_no not in(Select OMNIBUS_BO_DIVIDEND.warrent_no from BODiVIDEND,OMNIBUS_BO_DIVIDEND, omnibus_config_dividend where BODiVIDEND.warrant_no=OMNIBUS_BO_DIVIDEND.warrent_no and omnibus_config_dividend.rec_id=OMNIBUS_BO_DIVIDEND.omnibus_div_id and OMNIBUS_BO_DIVIDEND.ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') UNION Select OMNIBUS_BO_DIVIDEND_T.warrant_no from BODiVIDEND,OMNIBUS_BO_DIVIDEND_T, omnibus_config_dividend  where BODiVIDEND.warrant_no=OMNIBUS_BO_DIVIDEND_T.warrant_no and omnibus_config_dividend.rec_id=OMNIBUS_BO_DIVIDEND_T.omnibus_div_id and OMNIBUS_BO_DIVIDEND_T.ISSUE_DATE =  TO_DATE('" + DivDate + "','DD/MM/YYYY'))";
        ifwarexists =s ;
       // System.out.println("---------------warrant>"+s);
      }

        countfolios = "SELECT * FROM BODIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' AND warrant_no not in(Select OMNIBUS_BO_DIVIDEND.warrent_no from BODiVIDEND,OMNIBUS_BO_DIVIDEND, omnibus_config_dividend where BODiVIDEND.warrant_no=OMNIBUS_BO_DIVIDEND.warrent_no and omnibus_config_dividend.rec_id=OMNIBUS_BO_DIVIDEND.omnibus_div_id and OMNIBUS_BO_DIVIDEND.ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') UNION Select OMNIBUS_BO_DIVIDEND_T.warrant_no from BODiVIDEND,OMNIBUS_BO_DIVIDEND_T, omnibus_config_dividend  where BODiVIDEND.warrant_no=OMNIBUS_BO_DIVIDEND_T.warrant_no and omnibus_config_dividend.rec_id=OMNIBUS_BO_DIVIDEND_T.omnibus_div_id and OMNIBUS_BO_DIVIDEND_T.ISSUE_DATE =  TO_DATE('" + DivDate + "','DD/MM/YYYY'))";
      }
      }

    int cWAR = cm.queryExecuteCount(ifwarexists);

    if (cWAR > 0)
    {
      Destination = request.getContextPath() + "/jsp/Dividend/AllReconciliation.jsp?dateselect=";
      Destination = Destination + DivDate + "&collectiondate=" + ColDate + "&orderby=" + DivType + "&StartValue=";

      int tcount = cm.queryExecuteCount(countfolios);

      String getWarrant = "";
      if (DivType.equalsIgnoreCase("SMS"))
      {
       if (SMSType.equalsIgnoreCase("Group"))
       {
         getWarrant = "SELECT rnum FROM (SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM DIVIDEND_GROUP_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' ORDER BY NOMINEE_NO) div where rownum <= "+tcount+") where rnum >= 1) WHERE WARRANT_NO = '"+Warrantno+"'";
       }
       else if (SMSType.equalsIgnoreCase("Individual"))
       {
         if (ForW.equalsIgnoreCase("folio"))
         {
           getWarrant = "SELECT rnum FROM (SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM DIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' ORDER BY SUBSTR(FOLIO_NO,3,6)) div where rownum <= "+tcount+") where rnum >= 1) WHERE FOLIO_NO = '"+Warrantno+"'";
         }
         else if (ForW.equalsIgnoreCase("warrant"))
         {
           getWarrant = "SELECT rnum FROM (SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM DIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' ORDER BY SUBSTR(FOLIO_NO,3,6)) div where rownum <= "+tcount+") where rnum >= 1) WHERE WARRANT_NO = '"+Warrantno+"'";
         }

         //getWarrant = "SELECT rnum FROM (SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM DIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' ORDER BY SUBSTR(FOLIO_NO,3,6)) div where rownum <= "+tcount+") where rnum >= 1) WHERE WARRANT_NO = '"+Warrantno+"'";
       }
      }
















       else if (DivType.equalsIgnoreCase("CDBL"))
      {
        //System.out.println("2nd>>>>>>>>>>"+SMSType);
      if (SMSType.equalsIgnoreCase("Group"))
      {
         getWarrant = "SELECT rnum FROM (SELECT * FROM (SELECT div.*, rownum rnum FROM (select * from OMNIBUS_BO_DIVIDEND o,BODIVIDEND d,omnibus_config_dividend,omnibus_config con where d.ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') and o.warrent_no= d.warrant_no and omnibus_config_dividend.rec_id= o.omnibus_div_id and con.rec_id=o.omnibus_ac_id and omnibus_config_dividend.status='ACCEPTED' and d.COLLECTED = 'F' AND d.LOCKED = 'F') div where rownum <= "+tcount+") where rnum >= 1) WHERE WARRANT_NO = '"+Warrantno+"'";
		// System.out.println("getWarrant-"+getWarrant);
      }

      else if (SMSType.equalsIgnoreCase("Individual"))
      {
       if (ForW.equalsIgnoreCase("folio"))
       {
         getWarrant = "SELECT rnum FROM (SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM BODIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' AND warrant_no not in(Select OMNIBUS_BO_DIVIDEND.warrent_no from BODiVIDEND,OMNIBUS_BO_DIVIDEND, omnibus_config_dividend where BODiVIDEND.warrant_no=OMNIBUS_BO_DIVIDEND.warrent_no and omnibus_config_dividend.rec_id=OMNIBUS_BO_DIVIDEND.omnibus_div_id and OMNIBUS_BO_DIVIDEND.ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') UNION Select OMNIBUS_BO_DIVIDEND_T.warrant_no from BODiVIDEND,OMNIBUS_BO_DIVIDEND_T, omnibus_config_dividend  where BODiVIDEND.warrant_no=OMNIBUS_BO_DIVIDEND_T.warrant_no and omnibus_config_dividend.rec_id=OMNIBUS_BO_DIVIDEND_T.omnibus_div_id and OMNIBUS_BO_DIVIDEND_T.ISSUE_DATE =  TO_DATE('" + DivDate + "','DD/MM/YYYY')) ORDER BY BO_ID) div where rownum <= "+tcount+") where rnum >= 1) WHERE BO_ID = '"+Warrantno+"'";
       }
       else if (ForW.equalsIgnoreCase("warrant"))
       {
        getWarrant = "SELECT rnum FROM (SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM BODIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' AND warrant_no not in(Select OMNIBUS_BO_DIVIDEND.warrent_no from BODiVIDEND,OMNIBUS_BO_DIVIDEND, omnibus_config_dividend where BODiVIDEND.warrant_no=OMNIBUS_BO_DIVIDEND.warrent_no and omnibus_config_dividend.rec_id=OMNIBUS_BO_DIVIDEND.omnibus_div_id and OMNIBUS_BO_DIVIDEND.ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') UNION Select OMNIBUS_BO_DIVIDEND_T.warrant_no from BODiVIDEND,OMNIBUS_BO_DIVIDEND_T, omnibus_config_dividend  where BODiVIDEND.warrant_no=OMNIBUS_BO_DIVIDEND_T.warrant_no and omnibus_config_dividend.rec_id=OMNIBUS_BO_DIVIDEND_T.omnibus_div_id and OMNIBUS_BO_DIVIDEND_T.ISSUE_DATE =  TO_DATE('" + DivDate + "','DD/MM/YYYY')) ORDER BY BO_ID) div where rownum <= "+tcount+") where rnum >= 1) WHERE WARRANT_NO = '"+Warrantno+"'";
       }
        //getWarrant = "SELECT rnum FROM (SELECT * FROM (SELECT div.*, rownum rnum FROM (SELECT * FROM BODIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DivDate + "','DD/MM/YYYY') AND COLLECTED = 'F' AND LOCKED = 'F' ORDER BY BO_ID) div where rownum <= "+tcount+") where rnum >= 1) WHERE WARRANT_NO = '"+Warrantno+"'";
       }
	 }
      cm.queryExecute(getWarrant);
      cm.toNext();
      String srr = cm.getColumnS("rnum");
      int rr = Integer.parseInt(srr);
      int err = rr + iChunk;
      Destination = Destination + rr + "&EndValue=" + err + "&dividendby=" + SMSType;
    }
  }

  String Fldname = "";
  int irowcounter = Integer.parseInt(rowcounter);

  for (int i=1;i<irowcounter+1;i++)
  {
    Fldname = "selectingbox" + String.valueOf(i);
    String BoxValue = String.valueOf(request.getParameter(Fldname));

    BoxValue=cm.replace(BoxValue,"'","''");

    if (String.valueOf(BoxValue).equals("null"))
    {
      BoxValue = "";
    }
    if (!BoxValue.equalsIgnoreCase(""))
    {
      String marksel = "";
      if (DivType.equalsIgnoreCase("SMS"))
      {
        if (SMSType.equalsIgnoreCase("Group"))
        {
          marksel = "call MARK_SELECTED_DIVIDEND_GROUP('" + BoxValue + "','" + ColDate + "','" + DivDate + "')";
        }
        else if (SMSType.equalsIgnoreCase("Individual"))
        {
          marksel = "call MARK_SELECTED_DIVIDEND('" + BoxValue + "','" + ColDate + "','" + DivDate + "')";
        }
      }
      else if (DivType.equalsIgnoreCase("CDBL"))
      {
	   if (SMSType.equalsIgnoreCase("Group"))
        {
		  marksel = "call MARK_SELECTED_BODIVIDEND('" + BoxValue + "','" + ColDate + "','" + DivDate + "')";
	     }
	    else if (SMSType.equalsIgnoreCase("Individual"))
        {
        marksel = "call MARK_SELECTED_BODIVIDEND('" + BoxValue + "','" + ColDate + "','" + DivDate + "')";
		}
      }
      boolean b = cm.procedureExecute(marksel);
    }
  }

   if (isConnect)
   {
     cm.takeDown();
   }

  if (Destination.equalsIgnoreCase("close"))
  {
   %>
     <script language="javascript">
       location = "<%=request.getContextPath()%>/jsp/Home.jsp";
    </script>
   <%
  }
  else
  {
   %>
     <script language="javascript">
       location = "<%=Destination%>";
    </script>
   <%
  }
%>
</body>
</html>
