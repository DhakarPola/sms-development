<script>
    /*
     '******************************************************************************************************************************************
     'Script Author : Renad Hakim
     'Creation Date : October 2005
     'Page Purpose  : View of all the users.
     '******************************************************************************************************************************************
     */
</script>

<%@page import="batbsms.UserModel"%>
<%@page import="java.util.List"%>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="UserDAO"  class="batbsms.UserDAO"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if (String.valueOf(session.getAttribute("UserName")).equals("null")) {
%>
<script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
    }%>

<html>
    <style type="text/css">
        <!--
        .style9 {
            color: #FFFFFF;
            font-weight: bold;
        }
        .style12 {
            color: #000066;
            font-size: 11px;
        }
        -->
    </style>
    <head>
        <style type="text/css">
            <!--
            body {
                background-color: #FFFFFF;
            }
            body,td,th {
                color: #000000;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 12px;
            }
            -->
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>All Users</title>

        <SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
        <LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
        <script language="javascript">
<!--
            function tonewuser()
            {
                location = "<%=request.getContextPath()%>/jsp/Users/NewUser.jsp"
            }

            function todeleteuser()
            {
                location = "<%=request.getContextPath()%>/jsp/Users/DeleteUser.jsp?"
            }

            function askdelete()
            {
                if (confirm("Do you want to Delete the User?"))
                {
                    document.forms[0].submit();
                }
            }

//Execute while click on Submit
            function SubmitThis() {
                count = 0;
                if (count == 0) {
                    document.forms[0].submit();
                } else {
                    ShowAllAlertMsg();
                    return false;
                }
            }
//-->
</script>
<style type="text/css">
            <!--
            .style7 {
                color: #FFFFFF;
                font-weight: bold;
                font-size: 13px;
            }
            .style10 {color: #FF0000;}
            .style11 {
                color: #06689E;
                font-size: 11px;
            }
            .style13 {font-size: 11px}
            .style14 {color: #000066}
            -->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
            <%
                List<UserModel> allUser = UserDAO.getAllUser();

                String username = "";
                String userloginid = "";
                String userrole = "";
                String dest = "";

                int userid = 0;
            %>
        <span class="style7">
            <form method="GET" action="DeleteUser.jsp">
                <img name="B4" src="<%=request.getContextPath()%>/images/btnNew.gif" onclick="tonewuser()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNewOn.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNew.gif'">
                <img name="B6" src="<%=request.getContextPath()%>/images/btnDelete.gif" onclick="askdelete()" onMouseOver="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnDeleteOn.gif'" onMouseOut="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnDelete.gif'">
                <table width="100%" BORDER=1  cellpadding="5" bordercolor="#0044B0" style="border-collapse: collapse">
                    <!--DWLayoutTable-->
                    <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Users</center></td></tr>
                </table>
                <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
                    <tr bgcolor="#0044B0">
                        <td width="20%" class="style9"><div align="center">User Name</div></td>
                        <td width="20%"><div align="center" class="style9">
                                <div align="center">Login Name</div>
                            </div></td>
                        <td width="20%" class="style9"><div align="center">User Role</div></td>
                        <td width="20%" class="style9"><div align="center">Locked Out</div></td>
                        <td width="20%" class="style9"><div align="center">Disabled</div></td>
                    </tr>
                    <div align="left">
                        <%
                            for (UserModel singleUser : allUser) {

                                username = singleUser.getName();
                                userloginid = singleUser.getLoginId();
                                userrole = singleUser.getUserRole();
                                userid = singleUser.getUserId();
                                
                                dest = request.getContextPath() + "/jsp/Users/EditUser.jsp";

                                if (!username.equals("null")) {
                                    if (String.valueOf(userloginid).equals("null")) {
                                        userloginid = "";
                                    }
                                    if (String.valueOf(userrole).equals("null")) {
                                        userrole = "";
                                    }
                        %>
                    </div>
                    <tr bgcolor="#E8F3FD">
                        <td><div align="center" class="style10">
                                <div align="left" class="style10">
                                    <span class="style13">
                                        <label><input type="radio" checked="checked" name="duser" value="<%=userloginid %>"></label>
                                        &nbsp;&nbsp;&nbsp<a HREF="<%=dest%>?uname1=<%=userloginid %>&uid1=<%=singleUser.getUserId() %>"><%=username%></a>
                                        &nbsp;
                                    </span></div>
                            </div></td>
                        <td class="style13"><div align="center" class="style12">
                                <div align="center"><%=userloginid%>&nbsp;</div>
                            </div></td>
                        <td class="style12"><div align="center">&nbsp;&nbsp;&nbsp;<%=userrole%>&nbsp;</div></td>
                        <td class="style12"><div align="center">&nbsp;&nbsp;&nbsp;<%=singleUser.getLockOut()%><br>&nbsp;</div></td>
                        <td class="style12"><div align="center">&nbsp;&nbsp;&nbsp;<%=singleUser.getDisabled()%><br>&nbsp;</div></td>
                    </tr>
                    <div align="left" class="style13">
                        <%
                                }
                            }

                        %>
                    </div>
                </table>    
            </form>
            <tr bgcolor="#E8F3FD" >
                <td height="100%" bgcolor="#E8F3FD"  ><center>
            </center></tr>
        </span>            
</body>
</html>
