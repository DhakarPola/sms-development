<jsp:useBean id="ad" scope="session" class="batbsms.MSADConnection"/>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<% if((String.valueOf(session.getAttribute("UserName")).equals("null")) || (String.valueOf(session.getAttribute("UserRole")).equals("Operator")) || (String.valueOf(session.getAttribute("UserRole")).equals("Supervisor"))){
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

  boolean isConnected2AD = ad.connect();
  //try to connect if failed, redirect to error page
  if (isConnected2AD == false) {
    if (ad.isNetworkExist) {
      ad.takeDown();
    }
%>
<jsp:forward page="ErrorMsg.jsp">
  <jsp:param name="ErrorTitle" value="Connection Failure"/>
  <jsp:param name="ErrorHeading" value="Connection Problem"/>
  <jsp:param name="ErrorMsg" value="Error : Network Connection Failed, Please contact the System Administrator"/>
</jsp:forward>
<%
  }

 boolean ismembers=ad.loadMembers();
 boolean isGroups=ad.loadGroups();
 ad.takeDown();

%>
<HTML>
<HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
/*
	'******************************************************************************************************************************************
	'Script Author :Renad Hakim
        'Creation Date:February 2005
	'Page Purpose: Role Selection document, will load users from SQL table.
	'******************************************************************************************************************************************
*/
var FldName = null;
var FieldName ='';
var SelectedField;
//Open the Select User child window
function loadUserList(SelectedField){
    FieldName = SelectedField
    FldName = "RequestedBy"; //global variable
   var TU = "<%=request.getContextPath()%>/jsp/Users/SelectUser.jsp?";
   var width = 750;
   var height = 440;
   ONW('SelectUserss',TU, width, height, 0, 0, 0);
}
var nArray = new Array();
var SAdminVal
var SAdminText
var count=0;
var msg="";
//Generate String with list options value and text, passed in the SaveRoleSelection.jsp.
function GenerateListOptions(FieldName){
  var k = 0;
  var SeperaterVal="";
  var TotalStr="";
  var f=document.forms[0];

  for (k=0; k<f.elements[FieldName].options.length;k++){
    FieldVal = f.elements[FieldName].options[k].value;
    FieldText = f.elements[FieldName].options[k].text;
    if (TotalStr!=""){
        SeperaterVal="#";
      }
      TotalStr=TotalStr+SeperaterVal+(FieldVal+"|"+FieldText);
    }if(FieldName=='SAdmin'){
      f.HSAdmin.value=TotalStr
        }else if(FieldName=='Supervisor'){
          f.HSupervisor.value=TotalStr
        }else if(FieldName=='Operators'){
           f.HOperator.value=TotalStr
        }
    }
//validates and submits
function SubmitThis() {
   count = 0;
   var FieldName='SAdmin';
   GenerateListOptions(FieldName);
   FieldName='Supervisor';
   GenerateListOptions(FieldName);
   FieldName='Operators';
   GenerateListOptions(FieldName);

   ListBoxValidate('SAdmin',"Administrator field cannot be left blank.")
   ListBoxValidate('Supervisor',"Supervisor field cannot be left blank.")
   ListBoxValidate('Operators',"Operator field cannot be left blank.")

        if (count == 0){
		document.forms[0].submit();
	}
	else {
         ShowAllAlertMsg();
		return false;
		}
}//End of Submit

</Script></HEAD>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Untitled Document</title>
<link href="file:///I|/Test/Test.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {color: #0066FF}
-->
.Listbox {
	width: 250px;
	height: 80px;
	border:solid #0066FF 1px;
}
</style>

</head>
<body>
<form name="form0" method="post" action="<%=request.getContextPath()%>/jsp/Users/SaveAdUserSelection.jsp">
 <%
   String SAdminName = request.getParameter("SAdmin");
String SAminNameVal="";
boolean b =  cm.connect();
if(b) // if 1, means connection established
{
  int c = cm.queryExecuteCount("SELECT name FROM AD_ROLE_VIEW");
  if(c!=0) //  means record found, show the edit form
  {
  String SQL="";
  SQL = "SELECT login_id, name, user_role"
              +" FROM AD_ROLE_VIEW WHERE user_role = 'Administrator'";
cm.queryExecute(SQL);
  }
 }
 %>
   <table border="2" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#06689E" width="100%" id="AutoNumber1">
  <tr BGCOLOR="#E8F3FD">
    <td width="100%">&nbsp;<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber2">
      <tr>
        <td width="3%">&nbsp;</td>
        <td width="30%" valign="top"><b><font face="Arial, Helvetica, sans-serif" size="-1" color="#0A2769"> &nbsp;&nbsp;Administrator&nbsp;</font></b></td>
        <td width="3%">&nbsp;</td>
        <td width="29%">&nbsp;&nbsp;&nbsp;&nbsp;<SELECT NAME="SAdmin" multiple="multiple" class="Listbox">
           <% while(cm.toNext()){%>
          <OPTION VALUE="<%=cm.getColumnS("login_id").trim()%>"><%=cm.getColumnS("name").trim()%>
     <%}%>
        </td>
        <td width="24%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;
		<img name="Button1" src="<%=request.getContextPath()%>/images/btnSelect.gif"  onclick="loadUserList('SAdmin')" onMouseOver="document.forms[0].Button1.src = '<%=request.getContextPath()%>/images/btnSelectOn.gif'" onMouseOut="document.forms[0].Button1.src = '<%=request.getContextPath()%>/images/btnSelect.gif'">
		</td>
      </tr>
      <tr>
        <td width="3%">&nbsp;</td>
        <td width="18%">&nbsp;</td>
        <td width="3%">&nbsp;</td>
        <td width="29%">&nbsp;</td>
        <td width="24%">&nbsp;</td>
      </tr>
      <tr>
        <td width="3%">&nbsp;</td>
        <td width="18%" valign="top"><b><font face="Arial, Helvetica, sans-serif" size="-1" color="#0A2769">&nbsp;&nbsp;Supervisor</font></b></td>
        <td width="3%">&nbsp;</td>
        <% String SupervisorSQL="";
        SupervisorSQL = "SELECT login_id, name, user_role"
              +" FROM AD_ROLE_VIEW WHERE user_role = 'Supervisor'";
              cm.queryExecute(SupervisorSQL);
        %>
        <td width="29%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;<SELECT NAME="Supervisor" multiple="multiple" class="Listbox">
          <% while(cm.toNext()){%>
          <OPTION VALUE="<%=cm.getColumnS("login_id").trim()%>"><%=cm.getColumnS("name").trim()%>
            <%}%>
            </td>
            <td width="24%" valign="top" >&nbsp;&nbsp;&nbsp;&nbsp;
 		    <img name="Button2" src="<%=request.getContextPath()%>/images/btnSelect.gif"  onclick="loadUserList('Supervisor')" onMouseOver="document.forms[0].Button2.src = '<%=request.getContextPath()%>/images/btnSelectOn.gif'" onMouseOut="document.forms[0].Button2.src = '<%=request.getContextPath()%>/images/btnSelect.gif'">
			</td>
      </tr>
      <tr>
        <td width="3%">&nbsp;</td>
        <td width="18%">&nbsp;</td>
        <td width="3%">&nbsp;</td>
        <td width="29%">&nbsp;</td>
        <td width="24%">&nbsp;</td>
      </tr>
      <tr>
        <td width="3%">&nbsp;</td>
        <td width="18%" valign="top"><b><font face="Arial, Helvetica, sans-serif" size="-1" color="#0A2769">&nbsp;&nbsp;Operator</font></b></td>
        <td width="3%">&nbsp;</td>
        <% String OperatorsQuery="";
        OperatorsQuery = "SELECT login_id, name, user_role"
              +" FROM AD_ROLE_VIEW WHERE user_role = 'Operator'";
              cm.queryExecute(OperatorsQuery);
        %>
        <td width="29%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;<select name="Operators" multiple="multiple" class="Listbox">
         <% while(cm.toNext()){%>
         <OPTION VALUE="<%=cm.getColumnS("login_id").trim()%>"><%=cm.getColumnS("name").trim()%>
            <%}%>
           </td>
          <td width="20%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;
 		  <img name="Button5" src="<%=request.getContextPath()%>/images/btnSelect.gif"  onclick="loadUserList('Operators')" onMouseOver="document.forms[0].Button5.src = '<%=request.getContextPath()%>/images/btnSelectOn.gif'" onMouseOut="document.forms[0].Button5.src = '<%=request.getContextPath()%>/images/btnSelect.gif'">
		  </td>
         </tr>
      <tr>
        <td width="3%">&nbsp;</td>
        <td width="18%">&nbsp;</td>
        <td width="3%">&nbsp;</td>
        <td width="29%">&nbsp;</td>
        <td width="24%">&nbsp;</td>
      </tr>
       <tr>
        <td width="3%">&nbsp;</td>
        <td width="18%">&nbsp;</td>
        <td width="3%">&nbsp;</td>
        <td width="29%">
        <center>
 		<img name="Button6" src="<%=request.getContextPath()%>/images/btnSubmit.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].Button6.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].Button6.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
		</td>
        </center>
         <td width="3%">&nbsp;</td>
         <td width="45%">&nbsp;</td>
      </tr>
    </table>
    <p>

</table>
<input name="HSAdmin" type="hidden" value="">
<input name="HSupervisor" type="hidden" value="">
<input name="HOperator" type="hidden" value="">
</form>
</body>
</html>
