<script>
    /*
     '******************************************************************************************************************************************
     'Script Author : Renad Hakim
     'Creation Date : October 2005
     'Page Purpose  : View of all the users.
     '******************************************************************************************************************************************
     */
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if (String.valueOf(session.getAttribute("UserName")).equals("null")) {
%>
<script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
    }%>

<html>
    <style type="text/css">
        <!--
        .style9 {
            color: #FFFFFF;
            font-weight: bold;
        }
        .style12 {
            color: #000066;
            font-size: 11px;
        }
        -->
    </style>
    <head>
        <style type="text/css">
            <!--
            body {
                background-color: #FFFFFF;
            }
            body,td,th {
                color: #000000;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 12px;
            }
            -->
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>All Users</title>

        <SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
        <LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
        <script language="javascript">
            <!--
        function tonewuser()
            {
                location = "<%=request.getContextPath()%>/jsp/Users/NewUser.jsp"
            }

            function todeleteuser()
            {
                location = "<%=request.getContextPath()%>/jsp/Users/DeleteUser.jsp?"
            }

            function askdelete()
            {
                if (confirm("Do you want to Delete the User?"))
                {
                    document.forms[0].submit();
                }
            }

            //Execute while click on Submit
            function SubmitThis() {
                count = 0;
                if (count == 0) {
                    document.forms[0].submit();
                } else {
                    ShowAllAlertMsg();
                    return false;
                }
            }
            //-->
        </script>
        <style type="text/css">
            <!--
            .style7 {
                color: #FFFFFF;
                font-weight: bold;
                font-size: 13px;
            }
            .style10 {color: #FF0000;}
            .style11 {
                color: #06689E;
                font-size: 11px;
            }
            .style13 {font-size: 11px}
            .style14 {color: #000066}
            -->
        </style>
    </head>

    <body TEXT="000000" BGCOLOR="FFFFFF">
        <%
            boolean isConnect = cm.connect();
            if (isConnect == false) {
        %>
        <jsp:forward page="ErrorMsg.jsp" >
            <jsp:param name="ErrorTitle" value="Connection Failure" />
            <jsp:param name="ErrorHeading" value="Connection Problem" />
            <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
        </jsp:forward>
        <%
            }
        %>
        <span class="style7">
            <form method="GET" action="DeleteUser.jsp">

                <table width="100%" BORDER=1  cellpadding="5" bordercolor="#0044B0" style="border-collapse: collapse">
                    <!--DWLayoutTable-->
                    <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Configuration</center></td></tr>
                </table>
                <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
                    <tr bgcolor="#0044B0">
                        <td width="33%"><div align="center" class="style9">
                                <div align="center">Particular Name</div>
                            </div></td>
                        <td width="33%" class="style9"><div align="center">Configuration Values</div></td>
                    </tr>
                    <div align="left">

                    </div>
                    <tr bgcolor="#E8F3FD">
                        <td><div align="center" class="style10">
                                <div align="left" class="style10">
                                    <span class="style13">
                                        
                                        &nbsp;&nbsp;&nbsp<a HREF="ConfigurationEdit.jsp">Unsuccessful try</a>
                                        &nbsp;
                                    </span></div>
                            </div></td>
                        <td class="style12"><div align="center">&nbsp;&nbsp;&nbsp;10 times&nbsp;</div></td>
                    </tr>
                    <tr bgcolor="#E8F3FD">
                        <td><div align="center" class="style10">
                                <div align="left" class="style10">
                                    <span class="style13">
                                        
                                        &nbsp;&nbsp;&nbsp<a HHREF="ConfigurationEdit.jsp">Active session time</a>
                                        &nbsp;
                                    </span></div>
                            </div></td>
                        <td class="style12"><div align="center">&nbsp;&nbsp;&nbsp;20 minute&nbsp;</div></td>
                    </tr>       
                    <tr bgcolor="#E8F3FD">
                        <td><div align="center" class="style10">
                                <div align="left" class="style10">
                                    <span class="style13">
                                        
                                        &nbsp;&nbsp;&nbsp<a HREF="ConfigurationEdit.jsp">Maximum inactivity Time</a>
                                        &nbsp;
                                    </span></div>
                            </div></td>
                        <td class="style12"><div align="center">&nbsp;&nbsp;&nbsp;45 days &nbsp;</div></td>
                    </tr>     
                    </tr>       
                    <tr bgcolor="#E8F3FD">
                        <td><div align="center" class="style10">
                                <div align="left" class="style10">
                                    <span class="style13">
                                        
                                        &nbsp;&nbsp;&nbsp<a HREF="ConfigurationEdit.jsp">Password Change</a>
                                        &nbsp;
                                    </span></div>
                            </div></td>
                        <td class="style12"><div align="center">&nbsp;&nbsp;&nbsp;90 days&nbsp;</div></td>
                    </tr>    
                    <tr bgcolor="#E8F3FD">
                        <td><div align="center" class="style10">
                                <div align="left" class="style10">
                                    <span class="style13">
                                        
                                        &nbsp;&nbsp;&nbsp<a HREF="ConfigurationEdit.jsp">Password History Matching</a>
                                        &nbsp;
                                    </span></div>
                            </div></td>
                        <td class="style12"><div align="center">&nbsp;&nbsp;&nbsp;10 &nbsp;</div></td>
                    </tr>     
                    <tr bgcolor="#E8F3FD">
                        <td><div align="center" class="style10">
                                <div align="left" class="style10">
                                    <span class="style13">
                                        
                                        &nbsp;&nbsp;&nbsp<a HREF="ConfigurationEdit.jsp">Password Complexity</a>
                                        &nbsp;
                                    </span></div>
                            </div></td>
                        <td class="style12"><div align="center">&nbsp;&nbsp;&nbsp;5&nbsp;</div></td>
                    </tr>    
                    <tr bgcolor="#E8F3FD">
                        <td><div align="center" class="style10">
                                <div align="left" class="style10">
                                    <span class="style13">
                                        
                                        &nbsp;&nbsp;&nbsp<a HREF="ConfigurationEdit.jsp">Password length</a>
                                        &nbsp;
                                    </span></div>
                            </div></td>
                        <td class="style12"><div align="center">&nbsp;&nbsp;&nbsp;5&nbsp;</div></td>
                    </tr>                      
                    <div align="left" class="style13">

                    </div>
                </table>
            </form>
            <%
                if (isConnect) {
                    cm.takeDown();
                }
            %>
            <tr bgcolor="#E8F3FD" >
                <td height="100%" bgcolor="#E8F3FD"  ><center>

                </body>
                </html>
