<script>
    /*
     '******************************************************************************************************************************************
     'Script Author : Renad Hakim
     'Creation Date : October 2005
     'Page Purpose  : Edits User Information.
     '******************************************************************************************************************************************
     */
</script>

<%@page import="batbsms.UserDAO"%>
<%@page import="batbsms.UserModel"%>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if (String.valueOf(session.getAttribute("UserName")).equals("null")) {
%>
<script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
    }
%>
<% 
   String username = request.getParameter("uname1");
   UserModel SingleUser = new UserDAO().getUserByName(username);
%>
<html>
    <head>
        <style type="text/css">
            <!--
            body {
                background-color: #FFFFFF;
            }
            body,td,th {
                color: #000000;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 12px;
            }
                      
            -->
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Edit User</title>

        <SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>

        <script language="javascript">
        <!--
        function gotouser(){
            location = "<%=request.getContextPath()%>/jsp/Users/AllUsers.jsp";
        }

        function gotodelete(){
            if (confirm("Do you want to delete the User?")){
                location = "<%=request.getContextPath()%>/jsp/Users/DeleteUser.jsp?cusid2=<%=SingleUser.getUserId() %>";
            }
        }

        function formconfirm()        {
            if (confirm("Do you want to change the User Information?"))            {
                return true;
            }
        }

        //Execute while click on Submit
        function SubmitThis() {
            count = 0;
            BlankValidate('userFullName', '- Name (Option must be entered)');
            BlankValidate('userLoginId', '- Login ID (Option must be entered)');
            BlankValidate('userCurrentPassword', '- Password (Option must be entered)');
            BlankValidate('userCurrentPassword', '- Confirm Password (Option must be entered)');

            if (document.all.userCurrentPassword.value != document.all.userCurrentPassword.value)            {
                count++;
                nArray[count] = '- Passwords do no match!';
            }

            if (count > 0){
                ShowAllAlertMsg();
                return false;
            } else if( count === 0) {
                if (formconfirm())                {
                    document.forms[0].submit();
                }
            }
        }
                        //-->
        </script>
        <style type="text/css">
            <!--
            .style7 {
                color: #FFFFFF;
                font-weight: bold;
                font-size: 13px;
            }
            .style8 {color: #0A2769}
            .style9 {color: #0A2769; font-weight: bold; }
            -->
        </style>
    </head>

    <body TEXT="000000" BGCOLOR="FFFFFF">
        <form action="UpdateUser.jsp" method="get" enctype="multipart/form-data" name="FileForm"  id="edit-form" data-parsley-validate="">

            <span class="style7">
                <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
                    <!--DWLayoutTable-->
                    <tr><td bgcolor="#0044B0" class="style7">User Information</td></tr>
                    <tr bgcolor="#E8F3FD" >
                        <td height="100%" bgcolor="#E8F3FD"  ><center>
                        <br>
                        <div align="left">
                            <table width="80%"  border="0" cellpadding="5">
                                <tr>
                                    <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Name</div></th>
                                    <td width="1%">:</td>
                                    <td width="55%">
                                        <div align="left">
                                            <input type="text" name="userFullName" value="<%=SingleUser.getName()%>" class="SL2TextField" required="">
                                        </div></td>
                                </tr>
                                <tr>
                                    <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Login ID</div></th>
                                    <td>:</td>
                                    <td><div align="left">
                                            <input type="text" name="userLoginId" value="<%=SingleUser.getLoginId()%>" class="SL2TextField" onfocus="this.blur()">
                                        </div></td>
                                </tr>
                                <tr>
                                    <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;User Role</div></th>
                                    <td>:</td>
                                    <td><div align="left">
                                            <select name="urole" class="SL1TextFieldListBox">
                                                <%
                                                    if (SingleUser.getUserRole().equalsIgnoreCase("Administrator")) {
                                                %>
                                                <option selected="selected"> Administrator </option>
                                                <option> Supervisor </option>
                                                <option> Operator </option>
                                                <%
                                                } else if (SingleUser.getUserRole().equalsIgnoreCase("Supervisor")) {
                                                %>
                                                <option> Administrator </option>
                                                <option selected="selected"> Supervisor </option>
                                                <option> Operator </option>
                                                <%
                                                } else if (SingleUser.getUserRole().equalsIgnoreCase("Operator")) {
                                                %>
                                                <option> Administrator </option>
                                                <option> Supervisor </option>
                                                <option selected="selected"> Operator </option>
                                                <%
                                                    }
                                                %>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                               <tr>
                                    <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Login Attempt</div></th>
                                    <td>:</td>
                                    <td>
                                        <div align="left"><%=SingleUser.getLoginAttempt() %></div>
                                    </td>
                                </tr>                                 
                                <tr>
                                    <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Password</div></th>
                                    <td>:</td>
                                    <td><div align="left">
                                            <input type="password" name="userCurrentPassword" value="" class="SL2TextField">
                                        </div></td>
                                </tr>
                                <tr>
                                    <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Confirm Password</div></th>
                                    <td>:</td>
                                    <td><div align="left">
                                            <input type="password" name="userNewPassword" value="" class="SL2TextField">
                                        </div>
                                    </td>
                                </tr>                             
                                 <tr>
                                    <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Locked Out</div></th>
                                    <td>:</td>
                                    <td><div align="left">
                                            <input type="checkbox" name="userLockedOut" value="<%=SingleUser.getLockOut() %>" class="SL2TextField">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;User Disabled</div></th>
                                    <td>:</td>
                                    <td><div align="left">
                                            <input type="checkbox" name="userDisabled" value="<%=SingleUser.getDisabled() %>" class="SL2TextField">
                                        </div>
                                    </td>
                                </tr>
                              <tr>
                                    <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;first Time Login</div></th>
                                    <td>:</td>
                                    <td>
                                        <div align="left"><%=SingleUser.getFirstTimeLogin() %></div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Logged In </div></th>
                                    <td>:</td>
                                    <td>
                                        <div align="left"><%=SingleUser.getLoggedIn() %></div>
                                    </td>
                                </tr>                                
                                <tr>
                                    <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Lockout End Time</div></th>
                                    <td>:</td>
                                    <td><div align="left">
                                            <input type="datetime" name="lockOutEndTime" value="<%=SingleUser.getLockOutEndTime() %>" class="SL2TextField">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Inactivity End Time</div></th>
                                    <td>:</td>
                                    <td><div align="left">
                                            <input type="datetime" name="inactivityEndTime" value="<%=SingleUser.getInactivityEndTime() %>" class="SL2TextField">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Password Expiration TIme</div></th>
                                    <td>:</td>
                                    <td><div align="left">
                                            <input type="datetime" name="passwordExpireTime" value="<%=SingleUser.getPasswordExpireTime()  %>" class="SL2TextField">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Login Time</div></th>
                                    <td>:</td>
                                    <td><div align="left">
                                            <input type="datetime" name="userLoginTime" value="<%=SingleUser.getLoginTime()%>" class="SL2TextField">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;logout Time</div></th>
                                    <td>:</td>
                                    <td><div align="left">
                                            <input type="datetime" name="userLogoutTime" value="<%=SingleUser.getLogoutTime() %>" class="SL2TextField">
                                        </div>
                                    </td>
                                </tr>
  
                                                                
                            </table>

                            <!--HIDDEN FIELDS-->
                            <input type="hidden" name="userLgoinId" value="<%=SingleUser.getLoginId() %>">

                            <br>
                        </div>
                        <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
                            <tr>
                                <th width="43%" scope="row">
                                    <div align="right">
                                        <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
                                    </div></th>
                                <td width="57%">
                                    <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="gotouser()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
                                </td>
                            </tr>
                        </table>
                </table>
            </span>                    
        </form>
    </body>
</html>
