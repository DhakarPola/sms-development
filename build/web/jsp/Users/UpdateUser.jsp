<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : October 2005
'Page Purpose  : Saves update of user information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.sql.*,java.util.Vector,java.security.MessageDigest,java.util.Collections,java.io.*"%>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update User</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String UserName       = String.valueOf(request.getParameter("uname"));
    String UserLoginID    = String.valueOf(request.getParameter("uloginid"));
    String UserRole       = String.valueOf(request.getParameter("urole"));
    String UserPassword   = String.valueOf(request.getParameter("upass"));
    String UserID         = String.valueOf(request.getParameter("fuserid"));

    UserName              =cm.replace(UserName,"'","''");
    UserLoginID           =cm.replace(UserLoginID,"'","''");
    UserRole              =cm.replace(UserRole,"'","''");
    UserPassword          =cm.replace(UserPassword,"'","''");

    try {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(UserPassword.getBytes());
        byte[] bytes = md.digest();
        StringBuilder sb = new StringBuilder();
        for(int i=0; i< bytes.length ;i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        UserPassword = sb.toString();
    }catch (Exception e){
        e.printStackTrace();
    }  


  String uuser = "call UPDATE_USER('" + UserID + "','" + UserName + "', '" + UserLoginID + "','" + UserRole + "','" + UserPassword + "')";
  boolean b = cm.procedureExecute(uuser);

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Updated Stand-alone User Information')";
  boolean ub = cm.procedureExecute(ulog);

 if (isConnect)
  {
    cm.takeDown();
  }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Users/AllUsers.jsp";
</script>
</body>
</html>
