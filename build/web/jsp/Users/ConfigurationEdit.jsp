
<script>
    /*
     '******************************************************************************************************************************************
     'Script Author : Renad Hakim
     'Creation Date : October 2005
     'Page Purpose  : View of all the users.
     '******************************************************************************************************************************************
     */
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<%@page import="batbsms.ConfigurationDAO"%>
<%@page import="batbsms.ConfigurationModel"%>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if (String.valueOf(session.getAttribute("UserName")).equals("null")) {
%>
<script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
    }%>

<html>
    <style type="text/css">
        <!--
        .style9 {
            color: #FFFFFF;
            font-weight: bold;
        }
        .style12 {
            color: #000066;
            font-size: 11px;
        }
        -->
    </style>
    <head>
        <style type="text/css">
            <!--
            body {
                background-color: #FFFFFF;
            }
            body,td,th {
                color: #000000;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 12px;
            }
            -->
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>All Users</title>

        <SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
        <LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
        <script language="javascript">
            <!--
        function tonewuser()
            {
                location = "<%=request.getContextPath()%>/jsp/Users/NewUser.jsp"
            }

            function todeleteuser()
            {
                location = "<%=request.getContextPath()%>/jsp/Users/DeleteUser.jsp?"
            }

            function askdelete()
            {
                if (confirm("Do you want to Delete the User?"))
                {
                    document.forms[0].submit();
                }
            }

            //Execute while click on Submit
            function SubmitThis() {
                count = 0;
                if (count == 0) {
                    document.forms[0].submit();
                } else {
                    ShowAllAlertMsg();
                    return false;
                }
            }
            //-->
        </script>
        <style type="text/css">
            <!--
            .style7 {
                color: #FFFFFF;
                font-weight: bold;
                font-size: 13px;
            }
            .style10 {color: #FF0000;}
            .style11 {
                color: #06689E;
                font-size: 11px;
            }
            .style13 {font-size: 11px}
            .style14 {color: #000066}
            -->
        </style>
    </head>

    <body TEXT="000000" BGCOLOR="FFFFFF">
        <%
            boolean isConnect = cm.connect();
            if (isConnect == false) {
        %>
        <jsp:forward page="ErrorMsg.jsp" >
            <jsp:param name="ErrorTitle" value="Connection Failure" />
            <jsp:param name="ErrorHeading" value="Connection Problem" />
            <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
        </jsp:forward>
        <%
            }

        
        

        %>
        <% ConfigurationModel configuration = new ConfigurationDAO().getConfiguration(); %>
        <span class="style7">
            <form method="GET" action="ConfigurationUpdate.jsp">

                <table width="100%" BORDER=1  cellpadding="5" bordercolor="#0044B0" style="border-collapse: collapse">
                    <!--DWLayoutTable-->
                    <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Configuration</center></td></tr>
                </table>
                <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
                    <tr bgcolor="#0044B0">
                        <td width="33%"><div align="center" class="style9">
                                <div align="center">Particular Name</div>
                            </div></td>
                        <td width="33%" class="style9"><div align="center">Configuration Values</div></td>
                    </tr>
                    <div align="left">

                    </div>
                    <tr bgcolor="#E8F3FD">
                        <td><div align="center" class="style10">
                                <div align="left" class="style10">
                                    <span class="style13">
                                        &nbsp;&nbsp;&nbsp<a HREF="">Unsuccessful try</a>
                                        &nbsp;
                                    </span></div>
                            </div></td>
                            <td class="style12"><div align="center"><input type="text" name="UNSUCCESSFUL_TRY" value="<%= configuration.getUnsuccessfulTry()%>"></div></td>
                    </tr>
                    <tr bgcolor="#E8F3FD">
                        <td><div align="center" class="style10">
                                <div align="left" class="style10">
                                    <span class="style13">
                                        
                                        &nbsp;&nbsp;&nbsp<a HREF="">Active session time</a>
                                        &nbsp;
                                    </span></div>
                            </div></td>
                            <td class="style12"><div align="center"> <input type="text" name="SESSION_TIMEOUT" value="<%= configuration.getSessionTimeOut()%>"></div></td>
                    </tr>       
                    <tr bgcolor="#E8F3FD">
                        <td><div align="center" class="style10">
                                <div align="left" class="style10">
                                    <span class="style13">
                                        
                                        &nbsp;&nbsp;&nbsp<a HREF="">Inactivity Time</a>
                                        &nbsp;
                                    </span></div>
                            </div></td>
                        <td class="style12"><div align="center"><input type="text" name="INACTIVITY_TIME" value="<%= configuration.getInactivityTime()%>"></div></td>
                    </tr>     
                    </tr>       
                    <tr bgcolor="#E8F3FD">
                        <td><div align="center" class="style10">
                                <div align="left" class="style10">
                                    <span class="style13">
                                        
                                        &nbsp;&nbsp;&nbsp<a HREF="">Password Change</a>
                                        &nbsp;
                                    </span></div>
                            </div></td>
                        <td class="style12"><div align="center"><input type="text" name="PASSWORD_CHANGE" value="<%= configuration.getPasswordChange()%>"></div></td>
                    </tr>    
                    <tr bgcolor="#E8F3FD">
                        <td><div align="center" class="style10">
                                <div align="left" class="style10">
                                    <span class="style13">
                                        
                                        &nbsp;&nbsp;&nbsp<a HREF="">Password History Matching</a>
                                        &nbsp;
                                    </span></div>
                            </div></td>
                        <td class="style12"><div align="center"><input type="text" name="PASSWORD_CHANGE_HISTORY" value="<%= configuration.getPasswordChangeHistory()%>"></div></td>
                    </tr>     
                    <tr bgcolor="#E8F3FD">
                        <td><div align="center" class="style10">
                                <div align="left" class="style10">
                                    <span class="style13">
                                       
                                        &nbsp;&nbsp;&nbsp<a HREF="">Password Complexity</a>
                                        &nbsp;
                                    </span></div>
                            </div></td>
                        <td class="style12"><div align="center">
                                <input type="checkbox" name="PASSWORD_COMPLEXITY_SPCHARACTER" value="<%= configuration.isPasswordComplexitySpeical() %>" <%= configuration.isPasswordComplexitySpeical() ? "checked" : ""  %>> Special Character 
                                <input type="checkbox" name="PASSWORD_COMPLEXITY_CAPITAL" value="<%= configuration.isPasswordComplexityCapital()%>" <%= configuration.isPasswordComplexityCapital() ? "checked" : ""  %>> Capital Letter 
                                <input type="checkbox" name="PASSWORD_COMPLEXITY_NUMBER" value="<%= configuration.isPasswordComplexityNumber()%>" <%= configuration.isPasswordComplexityCapital() ? "checked" : ""  %>> Number </div></td>
                    </tr>    
                    <tr bgcolor="#E8F3FD">
                        <td><div align="center" class="style10">
                                <div align="left" class="style10">
                                    <span class="style13">
                                        
                                        &nbsp;&nbsp;&nbsp<a HREF="">Password length</a>
                                        &nbsp;
                                    </span></div>
                            </div></td>
                        <td class="style12"><div align="center"><input type="text" name="PASSWORD_LENGTH_ADMIN" value="<%= configuration.getPasswordLengthAdmin()%>">
                                <input type="text" name="PASSWORD_LENGTH_USER" value="<%= configuration.getPasswordLengthUser()%>"></div></td>
                    </tr>                      
                    <div align="left" class="style13">

                    </div>
                </table>
                        <table width="100%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
                            <tr>
                                <th width="43%" scope="row">
                                    <div align="right">
                                        <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
                                    </div></th>
                                <td width="57%">
                                    <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="gotouser()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
                                </td>
                            </tr>
                        </table>                    
            </form>
            <%
                if (isConnect) {
                    cm.takeDown();
                }
            %>
            <tr bgcolor="#E8F3FD" >
                <td height="100%" bgcolor="#E8F3FD"  ><center>

                </body>
                </html>
