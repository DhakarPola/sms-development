<script>
/*
'******************************************************************************************************************************************
'Script Author : Md.Kamruzzaman
'Creation Date : December 2006
'Page Purpose  : Exports Shareholding Report to Excel Format.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Shareholding Report</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<%@ page language="java" import="org.apache.poi.hssf.usermodel.*,java.util.Vector,java.io.*,java.util.*,java.util.List,java.util.Vector" %>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

           String totalshare=request.getParameter("total_share");
           //System.out.println("totalshare="+totalshare);
           String perS=String.valueOf(request.getParameter("percent"));
           //System.out.println("perS= "+perS);
           String Name="";
           String share="";
           String percentage="";
           String testtwo="";
           String tempshares="";
           String finalshare="";
           String t="";
           char c;



           HSSFWorkbook wb = new HSSFWorkbook();
           HSSFSheet sheet = wb.createSheet("Shareholding Report");
          // Create a row and put some cells in it. Rows are 0 based.
           int i=1;
           i++;
           HSSFRow row = sheet.createRow((short)i);

           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue("Shareholdings");
           i++;

           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("");
           i++;

           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue("The names of the Shareholders along with the position of their shares are listed");
           i++;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue("bellow:");
           i+=2;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue("         NAME OF SHAREHOLDER     ");
           row.createCell((short)6).setCellValue("SHARES");
           row.createCell((short)8).setCellValue("% OF");
           i++;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue("");
           row.createCell((short)6).setCellValue("HELD");
           row.createCell((short)8).setCellValue("HOLDING");
           i+=2;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue(" i) Parent/Subsidiary/associate/related parties:");
           row.createCell((short)6).setCellValue("");
           row.createCell((short)8).setCellValue("");
           i++;
           String q1="select * from SHAREHOLDING_REPORT where CATEGORY='Parent'";
           cm.queryExecute(q1);
           cm.toNext();
           Name=cm.getColumnS("NAME");
           share=cm.getColumnS("SHARES");

           // for add comma in Shares held
           finalshare="";
           tempshares=share;
           while(tempshares.length()>3)
           {
             t=tempshares.substring((tempshares.length()-3),tempshares.length());
             tempshares=tempshares.substring(0,(tempshares.length()-3));
             finalshare=","+t+finalshare;
           }
           finalshare=tempshares+finalshare;
           share=finalshare;
           ///////////////////////////////////////

           percentage=cm.getColumnS("PERCENTAGE");
           //to reverce the string
	   for (int j=0; j<percentage.length(); j++)
           {
	      testtwo = percentage.charAt(j) + testtwo;
	   }
           //to add 0 ifone decimal place is found
           c=testtwo.charAt(2);
           if(c!='.')
           percentage=percentage+"0";

           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue("    "+Name);
           row.createCell((short)6).setCellValue(share);
           row.createCell((short)8).setCellValue(percentage+"%");
           i+=2;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue("ii) Director/CEO/CS/CFO/Audit Head");
           row.createCell((short)6).setCellValue("");
           row.createCell((short)8).setCellValue("");
           i++;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue("   and their spouses and minor children");
           row.createCell((short)6).setCellValue("nil");
           row.createCell((short)8).setCellValue("nil");
           i+=2;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue("iii) Executives(Head of Function)");
           row.createCell((short)6).setCellValue("nil");
           row.createCell((short)8).setCellValue("nil");
           i+=2;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue("iv) Shareholders, who holds "+ perS +"% or more");
           row.createCell((short)6).setCellValue("");
           row.createCell((short)8).setCellValue("");
            i++;

           String q2="select * from SHAREHOLDING_REPORT where CATEGORY='Greater then 10' order by SHARES DESC";
           cm.queryExecute(q2);
           while(cm.toNext())
           {
           Name=cm.getColumnS("NAME");
           share=cm.getColumnS("SHARES");

           // for add comma in Shares held
           finalshare="";
           tempshares=share;
           while(tempshares.length()>3)
           {
             t=tempshares.substring((tempshares.length()-3),tempshares.length());
             tempshares=tempshares.substring(0,(tempshares.length()-3));
             finalshare=","+t+finalshare;
           }
           finalshare=tempshares+finalshare;
           share=finalshare;
           ///////////////////////////////////////

           percentage=cm.getColumnS("PERCENTAGE");

           //to reverce the string
	   for (int j=0; j<percentage.length(); j++)
           {
	      testtwo = percentage.charAt(j) + testtwo;
	   }
           //to add 0 ifone decimal place is found
           c=testtwo.charAt(2);
           if(c!='.')
           percentage=percentage+"0";

           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue("    "+Name);
           row.createCell((short)6).setCellValue(share);
           row.createCell((short)8).setCellValue(percentage+"%");
           i++;
           }
           i++;
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue("v) Other Shareholders, who holds less than " + perS +"%");
           row.createCell((short)6).setCellValue("");
           row.createCell((short)8).setCellValue("");
           i+=2;

           String q3="select * from SHAREHOLDING_REPORT where CATEGORY='Less then 10' and Name !='Others' order by SHARES DESC";
           cm.queryExecute(q3);
           while(cm.toNext())
           {
           Name=cm.getColumnS("NAME");
           share=cm.getColumnS("SHARES");
           // for add comma in Shares held
           finalshare="";
           tempshares=share;
           while(tempshares.length()>3)
           {
             t=tempshares.substring((tempshares.length()-3),tempshares.length());
             tempshares=tempshares.substring(0,(tempshares.length()-3));
             finalshare=","+t+finalshare;
           }
           finalshare=tempshares+finalshare;
           share=finalshare;
           ///////////////////////////////////////

           percentage=cm.getColumnS("PERCENTAGE");

           //to reverce the string
	   for (int j=0; j<percentage.length(); j++)
           {
	      testtwo = percentage.charAt(j) + testtwo;
	   }

           //to add 0 ifone decimal place is found
           c=testtwo.charAt(2);
           if(c!='.')
           percentage=percentage+"0";

           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue("   "+Name);
           row.createCell((short)6).setCellValue(share);
           row.createCell((short)8).setCellValue(percentage+"%");
           i++;
           }
           String q4="select * from SHAREHOLDING_REPORT where CATEGORY='Less then 10' and Name ='Others' order by SHARES DESC";
           cm.queryExecute(q4);
           cm.toNext();
           Name=cm.getColumnS("NAME");
           share=cm.getColumnS("SHARES");
           // for add comma in Shares held
           finalshare="";
           tempshares=share;
           while(tempshares.length()>3)
           {
             t=tempshares.substring((tempshares.length()-3),tempshares.length());
             tempshares=tempshares.substring(0,(tempshares.length()-3));
             finalshare=","+t+finalshare;
           }
           finalshare=tempshares+finalshare;
           share=finalshare;
           ///////////////////////////////////////

           percentage=cm.getColumnS("PERCENTAGE");

           //to reverce the string
	   for (int j=0; j<percentage.length(); j++)
           {
	      testtwo = percentage.charAt(j) + testtwo;
	   }

           //to add 0 ifone decimal place is found
           c=testtwo.charAt(2);
           if(c!='.')
           percentage=percentage+"0";
           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue("   "+Name);
           row.createCell((short)6).setCellValue(share);
           row.createCell((short)8).setCellValue(percentage+"%");

           i+=2;

           // for add comma in Shares held
           finalshare="";
           tempshares=totalshare;
           while(tempshares.length()>3)
           {
             t=tempshares.substring((tempshares.length()-3),tempshares.length());
             tempshares=tempshares.substring(0,(tempshares.length()-3));
             finalshare=","+t+finalshare;
           }
           finalshare=tempshares+finalshare;
           totalshare=finalshare;
           ///////////////////////////////////////

           row=sheet.createRow((short)i);
           row.createCell((short)0).setCellValue("");
           row.createCell((short)1).setCellValue(" Total");
           row.createCell((short)6).setCellValue(totalshare);
           row.createCell((short)8).setCellValue("100.00%");

          // Write the output to a file
             String savepath="";
             ServletContext sc = getServletConfig().getServletContext();

             //Code for creating a folder
           String path=getServletContext().getRealPath("/");
           File f1 = new File(path + "/Excel Export");
           if(!f1.exists())
           {
                 f1.mkdir();
           }
           savepath = sc.getRealPath("/") +"Excel Export/"+"ShareholdingReport.xls";

                FileOutputStream fileOut = new FileOutputStream(savepath);
                wb.write(fileOut);
                fileOut.close();
                fileOut=null;
                sheet=null;
                wb=null;
         //End Excel Export
        String tempURL =  request.getContextPath() +"/Excel Export/"+"ShareholdingReport.xls";
        %>
 <script language="javascript">
     showPrintPreview6('<%=tempURL%>');
    location = "<%=request.getContextPath()%>/jsp/Reports/ExportShareholding.jsp";
 </script>

<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
