<script>
/*
'******************************************************************************************************************************************
'Script Author : Mohammad Ashraful Islam
'Creation Date : December 2006
'Page Purpose  : Calculations for SEC Compliance Report.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}
%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Bank</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
String DivType="";
int DivYear=0;
int DivRate=0;
String DateDec="";
String Dateafter60="";
String CurrentDate="";
String DivSource="";

String DateSelect = String.valueOf(request.getParameter("dateselect"));
DateSelect=cm.replace(DateSelect,"'","''");

String query1 = "SELECT INITCAP(DIV_TYPE) TYPE,TO_NUMBER(TO_CHAR(PERIOD_FROM,'yyyy'),9999) DYEAR,DEC_DIV/10 RATE,DATE_DEC,DATE_DEC+60 AFTER60,SYSDATE FROM DIVIDENDDECLARATION_VIEW WHERE DATE_DEC=TO_DATE('" + DateSelect + "','DD/MM/YYYY')";
cm.queryExecute(query1);
cm.toNext();

DivType = cm.getColumnS("TYPE");
DivYear = cm.getColumnI("DYEAR");
DivRate = cm.getColumnI("RATE");
DateDec = cm.getColumnDT("DATE_DEC");
Dateafter60 = cm.getColumnDT("AFTER60");
CurrentDate = cm.getColumnDT("SYSDATE");

if (String.valueOf(DivType).equals("null"))
    DivType = "";
if (String.valueOf(DateDec).equals("null"))
    DateDec = "";
if (String.valueOf(Dateafter60).equals("null"))
    Dateafter60 = "";
if (String.valueOf(CurrentDate).equals("null"))
    CurrentDate = "";

DivType=cm.replace(DivType,"'","''");
DateDec=cm.replace(DateDec,"'","''");
Dateafter60=cm.replace(Dateafter60,"'","''");
CurrentDate=cm.replace(CurrentDate,"'","''");

DivType=DivType.trim();

if (DivType.equalsIgnoreCase("FINAL"))
{
DivSource="Board Meeting and AGM";
}
else
{
DivSource="Board Meeting";
}

//FOR NO. 4 AND 7

String query2="SELECT SUM(DIVIDEND) T1 FROM DIVIDEND_VIEW WHERE ISSUE_DATE=TO_DATE('" + DateSelect + "','DD/MM/YYYY')";
cm.queryExecute(query2);
cm.toNext();

int total1=0;
total1=cm.getColumnI("T1");

String query3="SELECT SUM(DIVIDEND) T2 FROM BODIVIDEND_VIEW WHERE ISSUE_DATE=TO_DATE('" + DateSelect + "','DD/MM/YYYY')";
cm.queryExecute(query3);
cm.toNext();

int total2=0;
total2=cm.getColumnI("T2");

int total=total1+total2;

//FOR NO. 8

String query4="SELECT SUM(DIVIDEND) T3 FROM DIVIDEND_VIEW WHERE ISSUE_DATE=TO_DATE('" + DateSelect + "','DD/MM/YYYY') AND ISSUED_WARRANT='T'";
cm.queryExecute(query4);
cm.toNext();

int total3=0;
total3=cm.getColumnI("T3");

String query5="SELECT SUM(DIVIDEND) T4 FROM BODIVIDEND_VIEW WHERE ISSUE_DATE=TO_DATE('" + DateSelect + "','DD/MM/YYYY') AND ISSUED_WARRANT='T'";
cm.queryExecute(query5);
cm.toNext();

int total4=0;
total4=cm.getColumnI("T4");

int totalamount=total3+total4;

//FOR NO. 10

String query6="SELECT SUM(DIVIDEND) X FROM VIEW_FOLIO_UNCLAIMED";
cm.queryExecute(query6);
cm.toNext();

int valueX=0;
valueX=cm.getColumnI("X");

String query7="SELECT SUM(DIVIDEND) Y FROM VIEW_BO_UNCLAIMED";
cm.queryExecute(query7);
cm.toNext();

int valueY=0;
valueY=cm.getColumnI("Y");

String query8="SELECT SUM(DIVIDEND) A FROM VIEW_FOLIO_UNCLAIMED WHERE ISSUE_DATE=TO_DATE('" + DateSelect + "','DD/MM/YYYY')";
cm.queryExecute(query8);
cm.toNext();

int valueA=0;
valueA=cm.getColumnI("A");

String query9="SELECT SUM(DIVIDEND) B FROM VIEW_BO_UNCLAIMED WHERE ISSUE_DATE=TO_DATE('" + DateSelect + "','DD/MM/YYYY')";
cm.queryExecute(query9);
cm.toNext();

int valueB=0;
valueB=cm.getColumnI("B");

int valuePREV=(valueX+valueY)-(valueA+valueB);
int valueCUR=(valueA+valueB);

String addtable = "call CREATE_SEC_COMPLIANCE(UPPER('" + DivType + "'),'" + DivYear + "','" + DateDec + "','" + DivRate + "','" + total + "',TO_DATE('" + DateSelect + "','DD/MM/YYYY'),TO_DATE('" + Dateafter60 + "','DD/MM/YYYY'),'" + total + "','" + totalamount + "',TO_DATE('" + CurrentDate + "','DD/MM/YYYY'),'" + 0 + "','" + 0 + "','" + valuePREV + "','" + valueCUR + "','" + DivSource + "')";
boolean ub = cm.procedureExecute(addtable);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/SECComplianceReport.jsp?dateselect=<%=DateSelect%>";
</script>
</body>
</html>
