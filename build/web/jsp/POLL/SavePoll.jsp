<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : August 2007
'Page Purpose  : Save new Poll Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>

<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Poll</title>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();

   if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
     String boid="";
     String Srname="";
     String Shname="";
     String Address="";
     String agm ="";
     int c=0;

     String polldate = String.valueOf(request.getParameter("polldate"));
     String agenda1=String.valueOf(request.getParameter("check1"));
     String agenda2=String.valueOf(request.getParameter("check2"));
     String agenda3=String.valueOf(request.getParameter("check3"));
     String agenda4=String.valueOf(request.getParameter("check4"));

     if(agenda1.equals("1")) agenda1="T"; else agenda1="F";
     if(agenda2.equals("2")) agenda2="T"; else agenda2="F";
     if(agenda3.equals("3")) agenda3="T"; else agenda3="F";
     if(agenda4.equals("4")) agenda4="T"; else agenda4="F";

     //System.out.println("polldate= "+polldate);
     //System.out.println("agenda1= "+agenda1);
     //System.out.println("agenda2= "+agenda2);
     //System.out.println("agenda3= "+agenda3);
     //System.out.println("agenda4= "+agenda4);

     String q1 = "SELECT POLL_DATE FROM POLL_DECLARE_VIEW where POLL_DATE >= TO_DATE('" + polldate + "','DD/MM/YYYY')";
     try
     {
       c=cm.queryExecuteCount(q1);
     }catch(Exception e){}

     if (c!=0)
     {
       %>
       <script language="javascript">
       alert("POLL entry already exists on or after this date.\nTry a later date.");
       location = "<%=request.getContextPath()%>/jsp/POLL/NewPoll.jsp";
       </script>
       <%
     }
     else
    {
    String poll_config="call ADD_POLL_CONFIG('" + polldate + "', '" + agenda1 + "', '" + agenda2 + "', '" + agenda3 + "', '" + agenda4 + "')";
    //System.out.println(poll_config);
    try
    {
      boolean q=cm.procedureExecute(poll_config);
    }catch(Exception e){}
     String query1 = "SELECT * FROM AGMAttendance_view where agmdate=TO_DATE('" + polldate + "','DD/MM/YYYY') order by BOID";
     //System.out.println(query1);
     try{
       cm.queryExecute(query1);
       while(cm.toNext())
       {
         boid = cm.getColumnS("BOID");
         Shname = cm.getColumnS("NAME");
         Address = cm.getColumnS("ADDRESS");

         if (String.valueOf(boid).equals("null"))
           boid = "";
         if (String.valueOf(Shname).equals("null"))
           Shname = "";
         if (String.valueOf(Address).equals("null"))
           Address = "";

         Shname=cm.replace(Shname,"'","''");
         Address=cm.replace(Address,"'","''");
         String insert_poll = "call ADD_POLL_DECLARE('" + polldate + "', '" + boid + "', '" + Shname + "', '" + Address + "')";
         boolean b = cm1.procedureExecute(insert_poll);
        }
     }
     catch(Exception ex){

     }

  }
    String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added POLL Information')";
    try{
      boolean ub = cm.procedureExecute(ulog);
      if (isConnect)
      {
        cm.takeDown();
        cm1.takeDown();
      }
    }
    catch(Exception ex){
    }

%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/POLL/GeneratePollList.jsp";
  </script>
</body>
</html>
