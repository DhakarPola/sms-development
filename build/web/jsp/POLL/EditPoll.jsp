<script>
/*
'******************************************************************************************************************************************
'Script Author : Md.Kamruzzaman
'Creation Date : August 2007
'Page Purpose  : Set Vote.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.io.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*"%>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<jsp:useBean id="util" class="batbsms.Utility"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css">
</LINK><%if (String.valueOf(session.getAttribute("UserName")).equals("null")) {%>
<script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%}%>
<html>
<head>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datepicker.js"></SCRIPT>
<script language="javascript">
<!--
function CLClose()
{
  //location = "<//%=request.getContextPath()%>/jsp/AGM/AGMAttendance.jsp";
  history.back();
}
function UpdateTable()
{
  document.forms[0].submit();
}

function showAgenda(agenda)
{
  var mes="";
  if(agenda=='1')
  {
    mes="Resolved that the Reports of Directors and the Balance sheet and Profit and Loss Account for the year ended 31 December 2006, and the notes thereto and Report of the Directors and Auditors\u2019 Reports thereon,  be and are hereby adopted.";
  }
  else if(agenda=='2')
  {
    mes="That a Dividend of Tk.3.00 per share of Tk.10.00 each, on the issued and paid up ordinary shares of the Company, as recommended by the Directors, be approved and that the Directors and/or the Secretary of the Company be and are hereby authorised to pay the same accordingly, to the shareholders whose names appear on the Share Register or/and Depository Resister of the Company at 10 May 2007.";
  }
  else if(agenda=='3')
  {
    mes="That Messrs. Md. Ziaul Haque Khondker and Mr. M. A. Mokaddem retire from the Board by rotation, Mr. Nicholas S. Hales and Md. Mahbubur Rahman, who were appointed to the Board since the last Annual General Meeting, also retires. Messrs. Md. Ziaul Haque Khondker, Mr. M. A. Mokaddem, Mr. Nicholas S. Hales and Md. Mahbubur Rahman, being eligible, be and are hereby elected Directors of the Company. In accordance with the Articles of Association, the term of office of Mr. M. A. Mokaddem will be upto the conclusion of the next Annual General Meeting, since he have reached an age beyond 65";
  }
  else if(agenda=='4')
  {
    mes="That Messrs. Rahman Rahman Huq be and are hereby appointed Auditors for the Company's Financial Year 2007 at a remuneration of Tk. 650,000/=(Taka Six Lac  fifty thousand) plus VAT";
  }
  count=0;
  if(mes!="")
  {
    count++;
    nArray[count]=mes;
  }
  if(count!=0)
  {
    ShowAgenda('<%=request.getContextPath()%>');
  }
}
//-->
</script>
<%
  boolean isConnect = cm.connect();
  if (isConnect == false) {
%>
<jsp:forward page="ErrorMsg.jsp">
  <jsp:param name="ErrorTitle" value="Connection Failure"/>
  <jsp:param name="ErrorHeading" value="Connection Problem"/>
  <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator"/>
</jsp:forward>
<%
  }
  String sboid = request.getParameter("fno");
  String polldate=request.getParameter("polldate");
  String foundS=request.getParameter("found");
  //System.out.println(foundS);
  int foundI=Integer.parseInt(foundS);
  //System.out.println("sboid= "+sboid);
  //System.out.println("polldate= "+polldate);

  String boid = sboid;
  String shareholdername = "";
  String address = "";
  String surname="";

  String VStatus ="";
  String queryFolio;
  String selectQuery="";
   byte[] imgData1=null;
  Connection conn1=null;

 queryFolio = "SELECT * FROM POLL_DECLARE_VIEW WHERE boid = '" + sboid+"' and POLL_DATE=TO_DATE('" + polldate + "','DD/MM/YYYY')";
 cm.queryExecute(queryFolio);

  while (cm.toNext()) {
    //boid  = cm.getColumnS("BOID");
    shareholdername = cm.getColumnS("NAME");
    address = cm.getColumnS("ADDRESS");
    //surname = cm.getColumnS("SURNAME");
	//VStatus = cm.getColumnS("IS_PRESENT");

    if (String.valueOf(shareholdername).equals("null"))
          shareholdername = "";
    if (String.valueOf(address).equals("null"))
          address = "";

   //***********************************************
  }

%>
<style type="text/css">
  <!--
    body {
    background-color: #FFFFFF;
    }
    body,td,th {
    color: #000000;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
    }
  -->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit Vote</title>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css">
</LINK><script language="javascript">
<!--


function closedocument()
{
	location = "<%=request.getContextPath()%>/jsp/POLL/AllVoter.jsp";
}


function formconfirm()
{
  if (confirm("Do you want to change the Folio Information?"))
  {
    return true;
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
   if(document.forms[0].foliono.value.length == 8)
  {
    if (count == 0)
    {
      if (formconfirm())
      {
        document.forms[0].submit();
      }
    }
    else{
      ShowAllAlertMsg();
      return false;
    }
  }
  else
  {
    alert("Folio Number should be 8 characters long");
  }
}

//-->
</script>
<style type="text/css">
  <!--
    .style7 {
    color: #FFFFFF;
    font-weight: bold;
    font-size: 13px;
    }
    .style8 {color: #0A2769;
            font-weight: bold;}
    .style9 {color: #0A2769; font-weight: bold; }
    .link{cursor:hand;
           color:#0A2769;
         }
  -->
</style>
</head>
<body TEXT="000000" BGCOLOR="FFFFFF">
<form action="saveVote.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Poll Management</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Folio/BOID</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=sboid%></div></td>

      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Name</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=shareholdername%></div></td>
      </tr>
	  <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Address</div></th>
        <td>:</td>
        <td><div align="left" class="style9"><%=address%></div></td>
      </tr>
      <%
      String con="select * from poll_config_view where POLL_DATE=TO_DATE('" + polldate + "','DD/MM/YYYY')";
      String a1="";
      String a2="";
      String a3="";
      String a4="";
      int count_agenda=0;
      try
      {
       cm.queryExecute(con);
       cm.toNext();
        a1=cm.getColumnS("AGENDA1");
        a2=cm.getColumnS("AGENDA2");
        a3=cm.getColumnS("AGENDA3");
        a4=cm.getColumnS("AGENDA4");
      }
      catch(Exception e){}

      String isAgenda1="";
      String isAgenda2="";
      String isAgenda3="";
      String isAgenda4="";

     if(foundI>0)
     {
       String IsVote="select * from POLL_Vote_VIEW where poll_date=TO_DATE('" + polldate + "','DD/MM/YYYY') and BOID='" + boid + "'";
       try
       {
         cm.queryExecute(IsVote);
         cm.toNext();
         isAgenda1=cm.getColumnS("AGENDA1");
         isAgenda2=cm.getColumnS("AGENDA2");
         isAgenda3=cm.getColumnS("AGENDA3");
         isAgenda4=cm.getColumnS("AGENDA4");
       }
       catch(Exception e){}
     }

     //System.out.println("isAgenda1= "+isAgenda1);
    // System.out.println("isAgenda2= "+isAgenda2);
    // System.out.println("isAgenda3= "+isAgenda3);
    // System.out.println("isAgenda4= "+isAgenda4);

      if(a1.equals("T"))
      {
        %>
        <tr>
          <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;<a onclick="showAgenda('1')" class="link"><u>Agenda 1</u></a></div></th>
          <td valign="top">:</td>
          <td valign="top">
          <div align="left" class="style9">
          <table>
          <tr>
           <td valign="top"><input type="radio" name="agenda1" value="yes" <%if(isAgenda1.equals("yes")){%>checked<%}%>/>Yes</td>
          </tr>
          <tr>
           <td valign="top"><input type="radio" name="agenda1" value="no" <%if(isAgenda1.equals("no")){%>checked<%}%>/>No</td>
          </tr>
          <tr>
           <td valign="top"><input type="radio" name="agenda1" value="nu" <%if(isAgenda1.equals("nu")){%>checked<%}%>/>Neutral</td>
          </tr>
          </table>
          </div>
          </td>
        </tr>
        <%
        }
       if(a2.equals("T"))
       {
         %>
         <tr>
           <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;<a onclick="showAgenda('2')" class="link"><u>Agenda 2</u></a></div></th>
           <td valign="top">:</td>
           <td valign="top"><div align="left" class="style9"><table>
          <tr>
           <td valign="top"><input type="radio" name="agenda2" value="yes" <%if(isAgenda2.equals("yes")){%>checked<%}%>/>Yes</td>
          </tr>
          <tr>
           <td valign="top"><input type="radio" name="agenda2" value="no" <%if(isAgenda2.equals("no")){%>checked<%}%>/>No</td>
          </tr>
          <tr>
           <td valign="top"><input type="radio" name="agenda2" value="nu" <%if(isAgenda2.equals("nu")){%>checked<%}%>/>Neutral</td>
          </tr>
          </table></div></td>
         </tr>
         <%
         }
         if(a3.equals("T"))
         {
         %>
         <tr>
           <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;<a onclick="showAgenda('3')" class="link"><u>Agenda 3</u></a></div></th>
           <td valign="top">:</td>
           <td valign="top"><div align="left" class="style9"><table>
          <tr>
           <td valign="top"><input type="radio" name="agenda3" value="yes" <%if(isAgenda3.equals("yes")){%>checked<%}%>/>Yes</td>
          </tr>
          <tr>
           <td valign="top"><input type="radio" name="agenda3" value="no" <%if(isAgenda3.equals("no")){%>checked<%}%>/>No</td>
          </tr>
          <tr>
           <td valign="top"><input type="radio" name="agenda3" value="nu" <%if(isAgenda3.equals("nu")){%>checked<%}%>/>Neutral</td>
          </tr>
          </table></div></td>
         </tr>
         <%
         }
         if(a4.equals("T"))
         {
           %>
           <tr>
           <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;<a onclick="showAgenda('4')" class="link"><u>Agenda 4</u></a></div></th>
           <td valign="top">:</td>
           <td valign="top"><div align="left" class="style9">
         <table>
          <tr>
           <td valign="top"><input type="radio" name="agenda4" value="yes" <%if(isAgenda4.equals("yes")){%>checked<%}%>/>Yes</td>
          </tr>
          <tr>
           <td valign="top"><input type="radio" name="agenda4" value="no" <%if(isAgenda4.equals("no")){%>checked<%}%>/>No</td>
          </tr>
          <tr>
           <td valign="top"><input type="radio" name="agenda4" value="nu" <%if(isAgenda4.equals("nu")){%>checked<%}%>/>Neutral</td>
          </tr>
          </table></div></td>
         </tr>
         <%
         }%>
       </tr>
      </table>
      <br>
      </div>

      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="50%" scope="row">
            <div align="right">
	      <img name="B2" src="<%=request.getContextPath()%>/images/btnSubmit.gif" onclick="UpdateTable()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
            </div></th>
        <td width="50%" align="left">
              <img name="B3" src="<%=request.getContextPath()%>/images/btnClose.gif" onclick="CLClose()" onMouseOver="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B3.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
              <input type="hidden" name="boid" value="<%=sboid%>">
              <input type="hidden" name="polldate" value="<%=polldate%>">
              <input type="hidden" name="found" value="<%=foundS%>"/>
      </td>
      </tr>
   </table>
</form>
<%
  if (isConnect) {
    cm.takeDown();
  }
%>
</body>
</html>

