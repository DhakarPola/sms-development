<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : August 2007
'Page Purpose  : Save new Vote Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>

<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Poll</title>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();

   if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

     String agenda1="";
     String agenda2="";
     String agenda3="";
     String agenda4="";

     String agen1="";
     String agen2="";
     String agen3="";
     String agen4="";

     String polldate = String.valueOf(request.getParameter("polldate"));
     String boid=String.valueOf(request.getParameter("boid"));
     String found=String.valueOf(request.getParameter("found"));

     //System.out.println("found="+found);

     int foundI=Integer.parseInt(found);

     //System.out.println("foundI="+foundI);

     agenda1=String.valueOf(request.getParameter("agenda1"));
     agenda2=String.valueOf(request.getParameter("agenda2"));
     agenda3=String.valueOf(request.getParameter("agenda3"));
     agenda4=String.valueOf(request.getParameter("agenda4"));

     agen1=agenda1;
     agen2=agenda2;
     agen3=agenda3;
     agen4=agenda4;

     String con="select * from poll_config_view where POLL_DATE=TO_DATE('" + polldate + "','DD/MM/YYYY')";
     String a1="";
     String a2="";
     String a3="";
     String a4="";
     int count_agenda=0;
      try
      {
       cm.queryExecute(con);
       cm.toNext();
        a1=cm.getColumnS("AGENDA1");
        a2=cm.getColumnS("AGENDA2");
        a3=cm.getColumnS("AGENDA3");
        a4=cm.getColumnS("AGENDA4");
      }
      catch(Exception e){}

     if(a1.equals("F"))
     {
       agen1="X";
     }
      if(a2.equals("F"))
     {
       agen2="X";
     }
     if(a3.equals("F"))
     {
       agen3="X";
     }
     if(a4.equals("F"))
     {
       agen4="X";
     }

     if(a1.equals("T")&&(agenda1.equals("")||agenda1.equals("null")))
     agen1="nu";
     if(a2.equals("T")&&(agenda2.equals("")||agenda2.equals("null")))
     agen2="nu";
     if(a3.equals("T")&&(agenda3.equals("")||agenda3.equals("null")))
     agen3="nu";
     if(a4.equals("T")&&(agenda4.equals("")||agenda4.equals("null")))
     agen4="nu";

     String insert="";

     if(foundI>0)
     {
      insert="call UPDATE_POLL_VOTE('" + boid + "','" + polldate + "','" + agen1 + "','" + agen2 + "','" + agen3 + "','" + agen4 + "')";
     }
     else
     insert="call ADD_POLL_VOTE('" + boid + "','" + polldate + "','" + agen1 + "','" + agen2 + "','" + agen3 + "','" + agen4 + "')";

     //System.out.println("insert= "+insert);

     String ulog = "call ADD_USER_LOG('" + boid + "','Voter','Voted')";
    try{
      boolean b=cm.procedureExecute(insert);
      boolean ub = cm.procedureExecute(ulog);
      if (isConnect)
      {
        cm.takeDown();
        cm1.takeDown();
      }
    }
    catch(Exception ex){
    }

%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/POLL/AllVoter.jsp?polldate=<%=polldate%>&searchFolio=<%=boid%>";
  </script>
</body>
</html>
