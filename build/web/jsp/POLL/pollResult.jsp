<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. kamruzzaman
'Creation Date : August 2007
'Page Purpose  : Generating Poll Result.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>AGM History</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function tonewdata()
{
  location = "<%=request.getContextPath()%>/jsp/POLL/NewPoll.jsp"
}

//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
.style15{

        }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
      if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

     String decdate = "";

     String query = "SELECT DISTINCT POLL_DATE FROM POLL_DECLARE_VIEW ORDER BY POLL_DATE DESC";
     try
     {
     cm.queryExecute(query);
   %>

     <span class="style7">
     <form name="FormToSubmit" method="GET" action="NewPoll.jsp">
     <img name="B4" src="<%=request.getContextPath()%>/images/btnNew.gif" onclick="tonewdata()" onMouseOver="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNewOn.gif'" onMouseOut="document.forms[0].B4.src = '<%=request.getContextPath()%>/images/btnNew.gif'">
     <table width="100%" BORDER=1  cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
     <!--DWLayoutTable-->
     <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>POLL Result History</center></td></tr>
     </table>
     <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
     <tr bgcolor="#0044BD">
     <td width="12%" class="style9"><div align="center">POLL Date</div></td>
     <td width="22%"><div align="center" class="style9">
     <div align="center">Agenda 1</div>
     </div>
     </td>
     <td width="22%"><div align="center" class="style9">
     <div align="center">Agenda 2</div>
     </div>
     </td>
     <td width="22%"><div align="center" class="style9">
     <div align="center">Agenda 3</div>
     </div>
     </td>
     <td width="22%"><div align="center" class="style9">
     <div align="center">Agenda 4</div>
     </div>
     </td>
     </tr>
     <tr bgcolor="#0044F0">
     <td width="12%" class="style9"><div align="center"></div></td>
     <td width="22%"><div align="center" class="style9">
     <div align="center"></div>
     </div>
     <table width="100%" border="0" cellpadding="5" style="border-collapse: collapse" bordercolor="#ffffff">
     <tr>
     <th>Yes</th>
     <th>No</th>
     <th>Neutral</th>
     </tr>
     </table>
     </td>
     <td width="22%"><div align="center" class="style9">
     <div align="center"></div>
     </div>
    <table width="100%" border="0" cellpadding="5" style="border-collapse: collapse" bordercolor="#ffffff">
     <tr>
     <th>Yes</th>
     <th>No</th>
     <th>Neutral</th>
     </tr>
     </table>
     </td>
     <td width="22%"><div align="center" class="style9">
     <div align="center"></div>
     </div>
    <table width="100%" border="0" cellpadding="5" style="border-collapse: collapse" bordercolor="#ffffff">
     <tr>
     <th>Yes</th>
     <th>No</th>
     <th>Neutral</th>
     </tr>
     </table>
     </td>
     <td width="22%"><div align="center" class="style9">
     <div align="center"></div>
     </div>
     <table width="100%" border="0" cellpadding="5" style="border-collapse: collapse" bordercolor="#ffffff">
     <tr>
     <th>Yes</th>
     <th>No</th>
     <th>Neutral</th>
     </tr>
     </table>
     </td>
     </tr>
     <div align="left">
     <%
     while (cm.toNext())
     {
     decdate = cm.getColumnDT("POLL_DATE");
     String query1 = "SELECT * FROM POLL_CONFIG_VIEW WHERE POLL_DATE = TO_DATE('"+decdate+"','DD/MM/YYYY')";
     cm1.queryExecute(query1);
     cm1.toNext();
     String agenda1=cm1.getColumnS("AGENDA1");
     String agenda2=cm1.getColumnS("AGENDA2");
     String agenda3=cm1.getColumnS("AGENDA3");
     String agenda4=cm1.getColumnS("AGENDA4");

     int a1y=0;
     int a1n=0;
     int a1ne=0;

     int a2y=0;
     int a2n=0;
     int a2ne=0;

     int a3y=0;
     int a3n=0;
     int a3ne=0;

     int a4y=0;
     int a4n=0;
     int a4ne=0;


     /*if(agenda1.equals("T"))
     {
     String vote_count="select q1.a1y as a1,q2.a1n as a2,q3.a1ne as a3 from (select count(BOID) as a1y  from poll_vote where POLL_DATE=TO_DATE('" + decdate +"','DD/MM/YYYY') and agenda1='yes' ) q1,(select count(BOID) as a1n  from poll_vote where POLL_DATE=TO_DATE('" + decdate +"','DD/MM/YYYY') and agenda1='no' ) q2,(select count(BOID) as a1ne  from poll_vote where POLL_DATE=TO_DATE('" + decdate +"','DD/MM/YYYY') and agenda1='nu' ) q3";
    // System.out.println(vote_count);
     try
     {
       cm1.queryExecute(vote_count);
       cm1.toNext();
       a1y=cm1.getColumnI("a1");
       a1n=cm1.getColumnI("a2");
       a1ne=cm1.getColumnI("a3");
     }
     catch(Exception e){}
     }

     if(agenda2.equals("T"))
     {
     String vote_count1="select q1.a1y as a1,q2.a1n as a2,q3.a1ne as a3 from (select count(BOID) as a1y  from poll_vote where POLL_DATE=TO_DATE('" + decdate +"','DD/MM/YYYY') and agenda2='yes' ) q1,(select count(BOID) as a1n  from poll_vote where POLL_DATE=TO_DATE('" + decdate +"','DD/MM/YYYY') and agenda2='no' ) q2,(select count(BOID) as a1ne  from poll_vote where POLL_DATE=TO_DATE('" + decdate +"','DD/MM/YYYY') and agenda2='nu' ) q3";
     //System.out.println(vote_count1);
     try
     {
       cm1.queryExecute(vote_count1);
       cm1.toNext();
       a2y=cm1.getColumnI("a1");
       a2n=cm1.getColumnI("a2");
       a2ne=cm1.getColumnI("a3");
     }
     catch(Exception e){}
     }

     if(agenda3.equals("T"))
     {
     String vote_count2="select q1.a1y as a1,q2.a1n as a2,q3.a1ne as a3 from (select count(BOID) as a1y  from poll_vote where POLL_DATE=TO_DATE('" + decdate +"','DD/MM/YYYY') and agenda3='yes' ) q1,(select count(BOID) as a1n  from poll_vote where POLL_DATE=TO_DATE('" + decdate +"','DD/MM/YYYY') and agenda3='no' ) q2,(select count(BOID) as a1ne  from poll_vote where POLL_DATE=TO_DATE('" + decdate +"','DD/MM/YYYY') and agenda3='nu' ) q3";
     //System.out.println(vote_count2);
     try
     {
       cm1.queryExecute(vote_count2);
       cm1.toNext();
       a3y=cm1.getColumnI("a1");
       a3n=cm1.getColumnI("a2");
       a3ne=cm1.getColumnI("a3");
     }
     catch(Exception e){}
     }

     if(agenda4.equals("T"))
     {
     String vote_count3="select q1.a1y as a1,q2.a1n as a2,q3.a1ne as a3 from (select count(BOID) as a1y  from poll_vote where POLL_DATE=TO_DATE('" + decdate +"','DD/MM/YYYY') and agenda4='yes' ) q1,(select count(BOID) as a1n  from poll_vote where POLL_DATE=TO_DATE('" + decdate +"','DD/MM/YYYY') and agenda4='no' ) q2,(select count(BOID) as a1ne  from poll_vote where POLL_DATE=TO_DATE('" + decdate +"','DD/MM/YYYY') and agenda4='nu' ) q3";
     //System.out.println(vote_count3);
     try
     {
       cm1.queryExecute(vote_count3);
       cm1.toNext();
       a4y=cm1.getColumnI("a1");
       a4n=cm1.getColumnI("a2");
       a4ne=cm1.getColumnI("a3");
     }
     catch(Exception e){}
     }
   */
    String q_vote="(select q.boid, q.agenda1 as a1,q.agenda2 as a2,q.agenda3 as a3,q.agenda4 as a4, s.ts as ts from (select FOLIO_NO,TOTAL_SHARE as ts from Shareholder_View) s,(select BOID,AGENDA1,AGENDA2,AGENDA3,agenda4 from POLL_VOTE_VIEW where POLL_DATE=TO_DATE('" + decdate + "','DD/MM/YYYY') ) q where q.boid=s.folio_no)union ";
    q_vote=q_vote+"(select q.boid, q.agenda1 as a1,q.agenda2 as a2,q.agenda3 as a3,q.agenda4 as a4, s.ts as ts from(select BOID,CURRBAL as ts from BOHOLDING_VIEW) s,(select BOID,AGENDA1,AGENDA2,AGENDA3,agenda4 from POLL_VOTE_VIEW where POLL_DATE=TO_DATE('" + decdate + "','DD/MM/YYYY') ) q where q.boid=s.boid)";

    //System.out.println(q_vote);

    try
    {
      cm1.queryExecute(q_vote);
      while(cm1.toNext())
      {
        String a1=cm1.getColumnS("a1");
        String a2=cm1.getColumnS("a2");
        String a3=cm1.getColumnS("a3");
        String a4=cm1.getColumnS("a4");
        int tv=cm1.getColumnI("TS");

        if(a1.equalsIgnoreCase("yes")) a1y=a1y+tv;
        if(a1.equalsIgnoreCase("no"))  a1n=a1n+tv;
        if(a1.equalsIgnoreCase("nu"))  a1ne=a1ne+tv;

        if(a2.equalsIgnoreCase("yes")) a2y=a2y+tv;
        if(a2.equalsIgnoreCase("no"))  a2n=a2n+tv;
        if(a2.equalsIgnoreCase("nu"))  a2ne=a2ne+tv;

        if(a3.equalsIgnoreCase("yes")) a3y=a3y+tv;
        if(a3.equalsIgnoreCase("no"))  a3n=a3n+tv;
        if(a3.equalsIgnoreCase("nu"))  a3ne=a3ne+tv;

        if(a4.equalsIgnoreCase("yes")) a4y=a4y+tv;
        if(a4.equalsIgnoreCase("no"))  a4n=a4n+tv;
        if(a4.equalsIgnoreCase("nu"))  a4ne=a4ne+tv;
      }
    }
    catch(Exception e){}

     %>
     </div>
     <tr bgcolor="#E8F3FD">
     <td class="style13"><div align="left" class="style12">
     <div align="center"><%=decdate%>&nbsp;</div>
     </div></td>
     <td width="20%"><div align="center" class="style9">
     <div align="center"><%if(agenda1.equals("T"))
     {
     %>
     <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#ffffff">
     <tr>
       <td align="center"><div align="left" class="style12"><div align="center"><%=a1y%></div></div></td>
       <td align="center"><div align="left" class="style12"><div align="center"><%=a1n%></div></div></td>
       <td align="center"><div align="left" class="style12"><div align="center"><%=a1ne%></div></div></td>
     </tr>
     </table>
     <%
     }
     else
     {
       %>
       <img name="B3" src="<%=request.getContextPath()%>/images/icoCross.gif">
       <%
     }
     %>
     </div>
     </div></td>
     <td width="20%"><div align="center" class="style9">
     <div align="center">
       <%
       if(agenda2.equals("T"))
       {
       %>
     <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#ffffff">
     <tr>
     <td align="center"><div align="left" class="style12"><div align="center"><%=a2y%></div></div></td>
       <td align="center"><div align="left" class="style12"><div align="center"><%=a2n%></div></div></td>
       <td align="center"><div align="left" class="style12"><div align="center"><%=a2ne%></div></div></td>
     </tr>
     </table>
     <%
   }
   else
   {
     %>
     <img name="B3" src="<%=request.getContextPath()%>/images/icoCross.gif">
     <%
   }
   %>
     </div>
     </div></td>
     <td width="20%"><div align="center" class="style9">
     <div align="center">
     <%if(agenda3.equals("T"))
       {
     %>
     <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#ffffff">
     <tr>
     <td align="center"><div align="left" class="style12"><div align="center"><%=a3y%></div></div></td>
       <td align="center"><div align="left" class="style12"><div align="center"><%=a3n%></div></div></td>
       <td align="center"><div align="left" class="style12"><div align="center"><%=a3ne%></div></div></td>
     </tr>
     </table>
     <%
   }
   else
   {
     %>
      <img name="B3" src="<%=request.getContextPath()%>/images/icoCross.gif">
     <%
   }
   %>
     </div>
     </div></td>
     <td width="20%"><div align="center" class="style9">
     <div align="center">
       <%
       if(agenda4.equals("T"))
       {
         %>
     <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#ffffff">
     <tr>
       <td align="center"><div align="left" class="style12"><div align="center"><%=a4y%></div></div></td>
       <td align="center"><div align="left" class="style12"><div align="center"><%=a4n%></div></div></td>
       <td align="center"><div align="left" class="style12"><div align="center"><%=a4ne%></div></div></td>
     </tr>
     </table>
     <%
   }
   else
     {
       %>
       <img name="B3" src="<%=request.getContextPath()%>/images/icoCross.gif">
       <%
     }
     %>
     </div>
     </div></td>
     </tr>
     <%
     }
    }catch(Exception e){}
     %>
    </form>
    <%
     if (isConnect)
   {
     try
     {
       cm.takeDown();
       cm1.takeDown();
     }catch(Exception e){}
   }
   %>
</body>
</html>
