<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Updated By    : Renad Hakim
'Creation Date : January 2005
'Page Purpose  : View for Certificate Lock Information
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Folios</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

function goNextPrevious(pstart,pend,certificateno)
{
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Certificate/CertificateLock.jsp?StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;
  thisurl = thisurl + "&searchCertificate="+certificateno;
  location = thisurl;
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}

//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   int certificate_state=0;
   int COUNT=0;
   int MAX_RECORDS=25;
   String certificate_no=request.getParameter("searchCertificate");
   String name = "";
   String hFolio="";
   String comment="";
   String dest = "";
   String islocked = "";

 //Used for NEXT/PREVIOUS
    String SStartingValue = String.valueOf(request.getParameter("StartValue"));
    String SEndingValue = String.valueOf(request.getParameter("EndValue"));


    if(SStartingValue == null || SStartingValue.equals("") || SStartingValue.equals("null")) SStartingValue="1";
    if(SEndingValue == null || SEndingValue.equals("") || SEndingValue.equals("null")) SEndingValue="0";



    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    int Chunk = MAX_RECORDS;
    EndingValue = StartingValue + Chunk - 1;
    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;

    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }


   if(certificate_no==null)
   {
     certificate_no="000000000";
   }

   certificate_no = certificate_no.trim();

   String query1;


     query1 =  "SELECT * FROM (SELECT div.*, rownum rnum FROM  ";
     query1 += "(SELECT CERTIFICATE_NO,HOLDER_FOLIO,\"LOCK\" AS IFLOCKED,\"COMMENT\" AS COMM FROM CERTIFICATE_VIEW WHERE CERTIFICATE_NO >= '"+certificate_no+"' ORDER BY CERTIFICATE_NO) div";
     query1 += " WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;

    COUNT = cm.queryExecuteCount(query1);
    cm.queryExecute(query1);
%>
  <span class="style7">
  <form method="GET" action="CertificateLock.jsp">
  <SPAN id="dprint">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
  </SPAN>
  <table width="100%" BORDER=1  cellpadding="0"  style="border-collapse: collapse" bordercolor="#06689E">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7" height="30"><center>Lock Certificates</center></td></tr>
  <tr>
  	<td>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0">
			<tr bgcolor="#E8F3FD">
				<td width="6%"></td>
				<td width="8%"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goNextPrevious(<%=pStartingValue%>,<%=pEndingValue%>,<%=certificate_no%>)"></td>
				<td width="9%"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNextPrevious(<%=nStartingValue%>,<%=nEndingValue%>,<%=certificate_no%>)"></td>
				<td width="15%" align="right" class="style12"><b>Enter Certificate No:</b></td>
				<td width="20%" align="center"><input name="searchCertificate" type="text" >
                                <input type="hidden" name="hcertificate" value="<%=certificate_no%>"/>              </td>
				<td width="15%"><img src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="SubmitThis()"></td>
			</tr>
                </table>
	</td>
  </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="0044B0">
  <tr bgcolor="#0044B0">
    <td width="13%" class="style9" align="center">Certificate No</td>
    <td width="12%"><div align="center" class="style9">
      <div align="center">Holder Folio</div>
    </div></td>
    <td width="12%"><div align="center" class="style9">
      <div align="center">Locked</div>
    </div></td>
    <td width="63%" align="center" class="style9">
      Comment of Lock
    </td>
  </tr>
  <div align="left">
     <%
     certificate_state=0;
    while(cm.toNext() && COUNT!=0)
     {
       islocked = "";
       certificate_state++;
       certificate_no=cm.getColumnS("CERTIFICATE_NO");
       hFolio = cm.getColumnS("HOLDER_FOLIO");
       comment = cm.getColumnS("COMM");
       islocked = cm.getColumnS("IFLOCKED");

       dest = request.getContextPath() + "/jsp/Certificate/CertificateLockEdit.jsp";

       if (!certificate_no.equals("null"))
       {
         comment = cm.replace(comment,"\r\n","<br>");
         comment = cm.replace(comment,"  ","&nbsp;&nbsp;");

         if (String.valueOf(name).equals("null"))
           name = "";
         if (String.valueOf(hFolio ).equals("null"))
           hFolio = "";
         if (String.valueOf(comment).equals("null"))
           comment = "";

         if(islocked.equalsIgnoreCase("T"))
         {
           islocked="YES";
         }
         else if (String.valueOf(islocked).equals("null"))
         {
           islocked = "NO";
         }
         else if (islocked.equalsIgnoreCase(""))
         {
           islocked = "NO";
         }
         else if(islocked.equalsIgnoreCase("F"))
         {
           islocked="NO";
         }
         %>
    </div>
    <tr bgcolor="#E8F3FD">
           <td valign="top"><div align="center" class="style10">
             <div align="center" class="style10">
               <span class="style12">
               <a HREF="<%=dest%>?certificateno=<%=certificate_no%>"><%=certificate_no%></a>
               &nbsp;
               <%
               if(certificate_state==COUNT)
               {
               %>
                 <input type="hidden" name="svalue" value="<%=StartingValue%>">
                 <input type="hidden" name="evalue" value="<%=EndingValue%>">
              <%}%>
               </span></div>
             </div></td>
           <td class="style13" valign="top"><div align="center" class="style12">
             <div align="center"><%=hFolio%>&nbsp;</div>
           </div></td>
           <td class="style13" valign="top"><div align="center" class="style12">
             <div align="center"><%=islocked%>&nbsp;</div>
           </div></td>
          <td class="style13"><div align="left" class="style12">
             <div align="left"><%=comment%></div>
           </div></td>
    </tr>
         <div align="left" class="style13">
             <%
         }
     }

   if (isConnect)
   {
     cm.takeDown();
   }
  %>
         </div>
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD" ><center>

</body>
</html>
