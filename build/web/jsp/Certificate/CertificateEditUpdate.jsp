<html>
<script>
/*
'******************************************************************************************************************************************
'Script Author : Rahat
'Updated By    :
'Creation Date : June 2005
'Page Purpose  : To Edit Certificates
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="util"  class="batbsms.Utility" scope="session"/>
<jsp:useBean id="cert"  class="batbsms.Certificate" scope="session"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>


<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.imagehand{
  cursor:hand;
  color:#FFFFFF;
 }

-->
</style>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<%@page language="java" import="java.io.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*"%>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>

<script type="text/javascript">
<!--
//Execute while click on Submit
function SubmitThis() {
 count = 0;
 BlankValidate('Certificate_No','- Certificate No (Option must be entered)');
 BlankValidate('Dist_From1','- Dist. From (Option must be entered)');
 BlankValidate('Dist_To1','- Dist. To (Option must be entered)');

	if (count == 0)
    {
        document.forms[0].submit();
    }
    else
    {
      ShowAllAlertMsg();
      return false;
    }
}

function changeCertificatePopup(oldCertificate)
{
  var url="CertificateEditPopup.jsp";
  if(document.forms[0].oldCertificate.value=="")
    alert("Please search for the certificate first.");
  else
    window.open("CertificateEditPopup.jsp","","HEIGHT=200,WIDTH=500");
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>
<body>
<%
boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

    String EditFoliono = String.valueOf(request.getParameter("Cfoliono"));
    String EditCertificateNo = String.valueOf(request.getParameter("CCertificateNo"));

    String nameFromDB="";
    String cert_distFromDB="";
    String cert_distToDB="";
    String vFoliono = "";
    String vName= "";
    String vAddress= "";
    String oldCertificate= "";
    String newCertificate= "";

   String FolioNo="";
   String CertificateNo="";
   String DistFrom1="";
   String DistTo1="";
   String DistFrom2="";
   String DistTo2="";
   String DistFrom3="";
   String DistTo3="";
   String DistFrom4="";
   String DistTo4="";
   String FolioName="";
   String FolioAddress="";
   String CertificateIssueDate="";
   String CertificateValidity="";
   String CertificateCollected="";
   String IsLocked="";
   String IsVerified="";
   String CComments="";
   String SelectQuery1="";

    SelectQuery1= "Select CV.FOLIO_NO,CV.CERTIFICATE_NO,CV.DIST_FROM,CV.DIST_TO ,CV.NAME,CV.ADDRESS1,";
    SelectQuery1=SelectQuery1 + "CV.CERTIFICATE_ISSUE_DATE,CV.VALIDITY,CV.LETTER_PRINTED,CV.COLLECTED,CV.LOCKED,CV.VERIFIED,CV.COMMENTS from CERTIFICATEEDIT_VIEW CV ";
    SelectQuery1=SelectQuery1 + "where CV.FOLIO_NO='" + EditFoliono + "' and CV.CERTIFICATE_NO='" + EditCertificateNo+"'";

  try{
    cm.queryExecute(SelectQuery1);
   String PreviousFolio="";
   String PreviousCertificate="";
    if(cm.toNext())
     {

       FolioNo=cm.getColumnS("FOLIO_NO");
        CertificateNo=cm.getColumnS("CERTIFICATE_NO");
        DistFrom1=cm.getColumnS("DIST_FROM");
        DistTo1=cm.getColumnS("DIST_TO");
        FolioName=cm.getColumnS("NAME");
		FolioName=FolioName.trim();
        FolioAddress=cm.getColumnS("ADDRESS1");
		FolioAddress=FolioAddress.trim();
        CertificateIssueDate=cm.getColumnDT("CERTIFICATE_ISSUE_DATE");
        CertificateValidity=cm.getColumnS("VALIDITY");
        CertificateCollected=cm.getColumnS("COLLECTED");
        IsLocked=cm.getColumnS("Locked");
        IsVerified=cm.getColumnS("Verified");
        CComments=cm.getColumnS("Comments");
        IsLocked=IsLocked.trim();
        IsVerified=IsVerified.trim();
        CertificateCollected=CertificateCollected.trim();
        if (String.valueOf(CComments).equals("null"))
      		CComments = "";

     }
	 if(cm.toNext())
     {

       	DistFrom2=cm.getColumnS("DIST_FROM");
        DistTo2=cm.getColumnS("DIST_TO");
     }
	 if(cm.toNext())
     {

       	DistFrom3=cm.getColumnS("DIST_FROM");
        DistTo3=cm.getColumnS("DIST_TO");
     } if(cm.toNext())
     {

       	DistFrom4=cm.getColumnS("DIST_FROM");
        DistTo4=cm.getColumnS("DIST_TO");
     }


    }
    catch(Exception ex)
    {
    }
   %>
<form action="CertificateEditSave.jsp" method="get" name="frmCertificateEditUpdate">
<table width="100%" border="1" cellspacing="0" cellpadding="0" style="border-collapse:collapse" bordercolor="#0044B0" bgcolor="#E8F3FD">
  <tr>
    <td>
	<table width="100%" border="0" cellspacing="0" cellpadding="1" >
      <tr>
        <td bgcolor="#0044B0" class="style7" height="30">Edit Certificate </td>
      </tr>
      <tr>
        <td><br>&nbsp;<table width="100%" border="0" cellspacing="0" cellpadding="5">
          <tr>
            <td  width="21%" class="style9">&nbsp;&nbsp;&nbsp;&nbsp;Folio</td>
            <td  width="3%" align="center">:</td>
            <td colspan="4"><%=FolioNo%></td>
            </tr>
			<tr>
            <td  width="21%" class="style9">&nbsp;&nbsp;&nbsp;&nbsp;Name</td>
            <td  width="3%" align="center">:</td>
            <td colspan="4"><input name="FolioName" type="text" id="FolioName" maxlength="10" class="SL70TextFieldDub" value="<%=FolioName%>" ></td>
            </tr>
			<tr>
            <td  width="21%" valign="top" class="style9">&nbsp;&nbsp;&nbsp;&nbsp;Address</td>
            <td  width="3%" align="center" valign="top">:</td>
            <td colspan="4"><textarea name="FolioAddress" wrap="VIRTUAL"  class="SL70TextFieldDub"><%=FolioAddress.trim()%></textarea></td>
            </tr>
			<tr>
            <td  width="21%" class="style9">&nbsp;&nbsp;&nbsp;&nbsp;Certificate No </td>
            <td  width="3%" align="center">:</td>
            <td colspan="4"><input name="Certificate_No" type="text" id="Certificate_No" maxlength="10" class="SL70TextFieldDub" value="<%=CertificateNo%>" onkeypress="keypressOnNumberFld();"></td>
            </tr>
			<tr>
            <td  width="21%" class="style9">&nbsp;&nbsp;&nbsp;&nbsp;Certificate Issue Date </td>
            <td  width="3%" align="center">:</td>
            <td colspan="4" valign="top"><input name="Certificate_Issue_Date" type=text value="<%=CertificateIssueDate%>" maxlength="10" class="SL70TextFieldDub" onFocus="this.blur();">&nbsp;
                    <a href="javascript:NewCal('Certificate_Issue_Date','ddmmyyyy',false,12)">
                      <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
                    </a></td>
            </tr>
			<tr>
            <td  width="21%" class="style9">&nbsp;&nbsp;&nbsp;&nbsp;Certificate Validity </td>
            <td  width="3%" align="center">:</td>
            <td colspan="4" valign="top"><select name="Certificate_Validity"  class="SL70TextFieldDub" onFocus="this.blur();">
				<option value="T" <%if(CertificateValidity.equalsIgnoreCase("T")){%>Selected <%}%>>T</option>
				<option value="F" <%if(CertificateValidity.equalsIgnoreCase("F")){%>Selected <%}%>>F</option>
				</select>
			</td>
            </tr>


			<tr>
            <td  width="21%" class="style9">&nbsp;&nbsp;&nbsp;&nbsp;Certificate Collected</td>
            <td  width="3%" align="center">:</td>
            <td colspan="4" valign="top"><select name="Certificate_Collected"  class="SL70TextFieldDub" onFocus="this.blur();">
				<option value="T" <%if(CertificateCollected.equalsIgnoreCase("T")){%>Selected <%}%>>T</option>
				<option value="F" <%if(CertificateCollected.equalsIgnoreCase("F")){%>Selected <%}%>>F</option>
				</select>
			</td>
            </tr>
			<tr>
            <td  width="21%" class="style9">&nbsp;&nbsp;&nbsp;&nbsp;Certificate Locked </td>
            <td  width="3%" align="center">:</td>
            <td colspan="4" valign="top"><select name="Is_Locked"  class="SL70TextFieldDub" onFocus="this.blur();">
				<option value="T" <%if(IsLocked.equalsIgnoreCase("T")){%>Selected <%}%>>T</option>
				<option value="F" <%if(IsLocked.equalsIgnoreCase("F")){%>Selected <%}%>>F</option>
				</select>
			</td>
            </tr>
			<tr>
            <td  width="21%" class="style9">&nbsp;&nbsp;&nbsp;&nbsp;Certificate Verified </td>
            <td  width="3%" align="center">:</td>
            <td colspan="4" valign="top"><select name="Is_Verified"  class="SL70TextFieldDub" onFocus="this.blur();">
				<option value="T" <%if(IsVerified.equalsIgnoreCase("T")){%>Selected <%}%>>T</option>
				<option value="F" <%if(IsVerified.equalsIgnoreCase("F")){%>Selected <%}%>>F</option>
				</select>
			</td>
            </tr>
			<tr>
            <td  width="21%" valign="top" class="style9">&nbsp;&nbsp;&nbsp;&nbsp;Certificate Comments</td>
            <td  width="3%" align="center" valign="top">:</td>
            <td colspan="4" valign="top"><textarea name="C_Comments" wrap="VIRTUAL"  class="SL70TextFieldDub"><%=CComments%></textarea>
			</td>
            </tr>

			<tr>
            <td  width="21%" class="style9">&nbsp;&nbsp;&nbsp;&nbsp;Dist. From 1</td>
            <td  width="3%" align="center">:</td>
            <td   width="16%"><input name="Dist_From1" type="text" id="Dist_From1" onfocus="this.blur()" class="SLC707TextField" value="<%=DistFrom1%>" onkeypress="keypressOnNumberFld();"></td>
			<td  width="10%" class="style9">&nbsp;Dist. To 1 </td>
            <td  width="1%" align="center">:</td>
            <td   width="49%"><input name="Dist_To1" type="text" id="Dist_To1" onfocus="this.blur()" class="SLC707TextField" value="<%=DistTo1%>" onkeypress="keypressOnNumberFld();"></td>
            </tr>
			<tr>
            <td  width="21%" class="style9">&nbsp;&nbsp;&nbsp;&nbsp;Dist. From 2 </td>
            <td  width="3%" align="center">:</td>
            <td   width="16%"><input name="Dist_From2" type="text" id="Dist_From2" onfocus="this.blur()" class="SLC707TextField" value="<%=DistFrom2%>" onkeypress="keypressOnNumberFld();"></td>
			<td  width="10%" class="style9">&nbsp;Dist. To 2</td>
            <td  width="1%" align="center">:</td>
            <td   width="49%"><input name="Dist_To2" type="text" id="Dist_To2" onfocus="this.blur()" class="SLC707TextField" value="<%=DistTo2%>" onkeypress="keypressOnNumberFld();"></td>
            </tr>
			<tr>
            <td  width="21%" class="style9">&nbsp;&nbsp;&nbsp;&nbsp;Dist. From 3 </td>
            <td  width="3%" align="center">:</td>
            <td   width="16%"><input name="Dist_From3" type="text" id="Dist_From3" onfocus="this.blur()" class="SLC707TextField" value="<%=DistFrom3%>" onkeypress="keypressOnNumberFld();"></td>
			<td  width="10%" class="style9">&nbsp;Dist. To 3</td>
            <td  width="1%" align="center">:</td>
            <td   width="49%"><input name="Dist_To3" type="text" id="Dist_To3" onfocus="this.blur()" class="SLC707TextField" value="<%=DistTo3%>" onkeypress="keypressOnNumberFld();"></td>
            </tr>
			<tr>
            <td  width="21%" class="style9">&nbsp;&nbsp;&nbsp;&nbsp;Dist. From 4</td>
            <td  width="3%" align="center">:</td>
            <td   width="16%"><input name="Dist_From4" type="text" id="Dist_From4" onfocus="this.blur()" class="SLC707TextField" value="<%=DistFrom4%>" onkeypress="keypressOnNumberFld();"></td>
			<td  width="10%" class="style9">&nbsp;Dist. To 4</td>
            <td  width="1%" align="center">:</td>
            <td   width="49%"><input name="Dist_To4" type="text" id="Dist_To4" onfocus="this.blur()" class="SLC707TextField" value="<%=DistTo4%>" onkeypress="keypressOnNumberFld();"></td>
            </tr>
        </table></td>
      </tr>

      <tr>
        <td align="center"><img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      </tr>

    </table>
<!-- Start of the View Search Results section-->
	</td>
  </tr>
</table>

</td>
  </tr>
</table>

	</td>
  </tr>
</table>
<input name="Filio_No" type="hidden" id="Filio_No"  class="SL70TextFieldDub" value="<%=FolioNo%>">
<input name="Certificate_No_old" type="hidden" id="Certificate_No_old"  class="SL70TextFieldDub" value="<%=CertificateNo%>">
<input name="Certificate_Issue_DT_old" type="hidden" id="Certificate_Issue_DT_old" maxlength="8" class="SL70TextFieldDub" value="<%=CertificateIssueDate%>">
<input name="Dist_From1_old" type="hidden" id="Dist_From1_old" maxlength="8" class="SL0TextField" value="<%=DistFrom1%>">
<input name="Dist_To1_old" type="hidden" id="Dist_To1_old" maxlength="8" class="SL0TextField" value="<%=DistTo1%>">
<input name="Dist_From2_old" type="hidden" id="Dist_From2_old" maxlength="8" class="SL0TextField" value="<%=DistFrom2%>">
<input name="Dist_To2_old" type="hidden" id="Dist_To2_old" maxlength="8" class="SL0TextField" value="<%=DistTo2%>">
<input name="Dist_From3_old" type="hidden" id="Dist_From3_old" maxlength="8" class="SL0TextField" value="<%=DistFrom3%>">
<input name="Dist_To3_old" type="hidden" id="Dist_To3_old" maxlength="8" class="SL0TextField" value="<%=DistTo3%>">
<input name="Dist_From4_old" type="hidden" id="Dist_From4_old" maxlength="8" class="SL0TextField" value="<%=DistFrom4%>">
<input name="Dist_To4_old" type="hidden" id="Dist_To4_old" maxlength="8" class="SL0TextField" value="<%=DistTo4%>">

<input name="FolioName_old" type="hidden" id="FolioName_old" class="SL0TextField" value="<%=FolioName%>">
<input name="CertificateIssueDateold" type="hidden" id="CertificateIssueDateold"  class="SL0TextField" value="<%=CertificateIssueDate%>">
<input name="CertificateValidityold" type="hidden" id="CertificateValidityold"  class="SL0TextField" value="<%=CertificateValidity%>">
<input name="CertificateCollectedold" type="hidden" id="CertificateCollectedold"  class="SL0TextField" value="<%=CertificateCollected%>">
<input name="IsLockedold" type="hidden" id="IsLockedold"  class="SL0TextField" value="<%=IsLocked%>">
<input name="IsVerifiedold" type="hidden" id="IsVerifiedold"  class="SL0TextField" value="<%=IsVerified%>">

</form>
</body>
</html>
