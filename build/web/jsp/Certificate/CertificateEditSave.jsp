<html>
<script>
/*
'******************************************************************************************************************************************
'Script Author : Rahat
'Updated By    :
'Creation Date : June 2005
'Page Purpose  : To Update Certificate Information
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="util"  class="batbsms.Utility" scope="session"/>
<jsp:useBean id="cert"  class="batbsms.Certificate" scope="session"/>
<%@page language="java" import="java.io.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*"%>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>


<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.imagehand{
  cursor:hand;
  color:#FFFFFF;
 }

-->
</style>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>

<script type="text/javascript">
<!--
//Execute while click on Submit
function SubmitThis() {
 count = 0;
 BlankValidate('Certificate_No','- Certificate No (Option must be entered)');
 BlankValidate('Dist_From1','- Dist. From (Option must be entered)');
 BlankValidate('Dist_To1','- Dist. To (Option must be entered)');

	if (count == 0)
    {
        document.forms[0].submit();
    }
    else
    {
      ShowAllAlertMsg();
      return false;
    }
}

function changeCertificatePopup(oldCertificate)
{
  var url="CertificateEditPopup.jsp";
  if(document.forms[0].oldCertificate.value=="")
    alert("Please search for the certificate first.");
  else
    window.open("CertificateEditPopup.jsp","","HEIGHT=200,WIDTH=500");
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>
<body>
<%
boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

	String EditFoliono = String.valueOf(request.getParameter("Filio_No"));
	String CertificateNoOld = String.valueOf(request.getParameter("Certificate_No_old"));
	String CertificateNoCur = String.valueOf(request.getParameter("Certificate_No"));
	String CertificateIssueDateOld=String.valueOf(request.getParameter("Certificate_Issue_DT_old"));
	String CertificateIssueDate=String.valueOf(request.getParameter("Certificate_Issue_Date"));
	String CertificateValidity=String.valueOf(request.getParameter("Certificate_Validity"));

	String DistFrom1old = String.valueOf(request.getParameter("Dist_From1_old"));
	String DistFrom1 = String.valueOf(request.getParameter("Dist_From1"));
	String DistTo1old = String.valueOf(request.getParameter("Dist_To1_old"));
	String DistTo1 = String.valueOf(request.getParameter("Dist_To1"));

	String DistFrom2old = String.valueOf(request.getParameter("Dist_From2_old"));
	String DistFrom2 = String.valueOf(request.getParameter("Dist_From2"));
	String DistTo2old = String.valueOf(request.getParameter("Dist_To2_old"));
	String DistTo2 = String.valueOf(request.getParameter("Dist_To2"));

	String DistFrom3old = String.valueOf(request.getParameter("Dist_From3_old"));
	String DistFrom3 = String.valueOf(request.getParameter("Dist_From3"));
	String DistTo3old = String.valueOf(request.getParameter("Dist_To3_old"));
	String DistTo3 = String.valueOf(request.getParameter("Dist_To3"));

	String DistFrom4old = String.valueOf(request.getParameter("Dist_From4_old"));
	String DistFrom4 = String.valueOf(request.getParameter("Dist_From4"));
	String DistTo4old = String.valueOf(request.getParameter("Dist_To4_old"));
	String DistTo4 = String.valueOf(request.getParameter("Dist_To4"));

	String FolioName=String.valueOf(request.getParameter("FolioName"));
	String FolioAddress=String.valueOf(request.getParameter("FolioAddress"));
	//String CertificateIssueDate=String.valueOf(request.getParameter("CERTIFICATE_ISSUE_DATE"));
	//String CertificateValidity=String.valueOf(request.getParameter("Certificate_Validity"));
	String CertificateCollected=String.valueOf(request.getParameter("Certificate_Collected"));
	String IsLocked=String.valueOf(request.getParameter("Is_Locked"));
	String IsVerified=String.valueOf(request.getParameter("Is_Verified"));
	String CComments=String.valueOf(request.getParameter("C_Comments"));

	String FolioName_old=String.valueOf(request.getParameter("FolioName_old"));
	String CertificateIssueDateold=String.valueOf(request.getParameter("CertificateIssueDateold"));
	String CertificateValidityold=String.valueOf(request.getParameter("CertificateValidityold"));
	String CertificateCollectedold=String.valueOf(request.getParameter("CertificateCollectedold"));
	String IsLockedold=String.valueOf(request.getParameter("IsLockedold"));
	String IsVerifiedold=String.valueOf(request.getParameter("IsVerifiedold"));

	boolean ub=true;
	String ulog="";
        String uContent="";

        String UpdateQuery="";

	if(DistFrom4.length()>0){
          if(!DistFrom4.equalsIgnoreCase(DistFrom4old)){
            UpdateQuery="Update Scripts set DIST_FROM="+DistFrom4+", Dist_To="+DistTo4+" where CERTIFICATE_NO='" + CertificateNoOld+"' and DIST_FROM="+DistFrom4old;
            try{
              cm.queryExecute(UpdateQuery);
			  uContent="Update on Scripts Table CeN="+CertificateNoOld+",DistFrom1old="+DistFrom4old+",DistTo1old="+DistTo4old;
			  ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','"+uContent+"')";
  			  ub = cm.procedureExecute(ulog);
            }
            catch(Exception ex)
            {
            }
          }
        }

        if(DistFrom3.length()>0){
          if(!DistFrom3.equalsIgnoreCase(DistFrom3old)){
            UpdateQuery="Update Scripts set DIST_FROM="+DistFrom3+", Dist_To="+DistTo3+" where CERTIFICATE_NO='" + CertificateNoOld+"' and DIST_FROM="+DistFrom3old;
            try{
              cm.queryExecute(UpdateQuery);
			  uContent="Update on Scripts Table CeN="+CertificateNoOld+",DistFrom1old="+DistFrom3old+",DistTo1old="+DistTo3old;
			  ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','"+uContent+"')";
  			  ub = cm.procedureExecute(ulog);
            }
            catch(Exception ex)
            {
            }
          }
        }

        if(DistFrom2.length()>0){
          if(!DistFrom2.equalsIgnoreCase(DistFrom2old)){
            UpdateQuery="Update Scripts set DIST_FROM="+DistFrom2+", Dist_To="+DistTo2+" where CERTIFICATE_NO='" + CertificateNoOld+"' and DIST_FROM="+DistFrom2old;
            try{
              cm.queryExecute(UpdateQuery);
			  uContent="Update on Scripts Table CeN="+CertificateNoOld+",DistFrom1old="+DistFrom2old+",DistTo1old="+DistTo2old;
			  ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','"+uContent+"')";
  			  ub = cm.procedureExecute(ulog);
            }
            catch(Exception ex)
            {
            }
          }
        }

        if(DistFrom1.length()>0){
          if(!DistFrom1.equalsIgnoreCase(DistFrom1old)){
            UpdateQuery="Update Scripts set DIST_FROM="+DistFrom1+", Dist_To="+DistTo1+" where CERTIFICATE_NO='" + CertificateNoOld+"' and DIST_FROM="+DistFrom1old;
            try{
              cm.queryExecute(UpdateQuery);
			  uContent="Update on Scripts Table CeN="+CertificateNoOld+",DistFrom1old="+DistFrom1old+",DistTo1old="+DistTo1old;
			  ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','"+uContent+"')";
  			  ub = cm.procedureExecute(ulog);
            }
            catch(Exception ex)
            {
            }
          }
        }



		FolioName=FolioName.trim();
		FolioAddress=FolioAddress.trim();
		CComments=CComments.trim();

        if(CertificateNoCur.length()>0){
          if(!CertificateNoCur.equalsIgnoreCase(CertificateNoOld)){
            UpdateQuery="Update Certificate set CERTIFICATE_NO='"+CertificateNoCur+"',CERTIFICATE_ISSUE_DATE=TO_DATE('"+CertificateIssueDate+"', 'DD/MM/YYYY'),VALIDITY='"+CertificateValidity+"', COLLECTED='"+CertificateCollected +"',\"LOCK\"='"+IsLocked+"', VERIFY='"+IsVerified +"', \"COMMENT\"='"+CComments+"' where CERTIFICATE_NO='" + CertificateNoOld+"'";
            try{
              cm.queryExecute(UpdateQuery);
              UpdateQuery="Update Shareholder set NAME='"+FolioName+"', ADDRESS1='"+FolioAddress+"' where FOLIO_NO='" + EditFoliono+"'";
              cm.queryExecute(UpdateQuery);

                uContent="Update on Certificate Table oldCeN="+CertificateNoOld+", IsD="+CertificateIssueDateold+",Valid="+CertificateValidityold+",colle"+CertificateCollectedold+",Lock="+IsLockedold+",Vari="+IsVerifiedold;
                ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','"+uContent+"')";
                ub = cm.procedureExecute(ulog);

                uContent="Update on ShareHolder Table Na="+FolioName_old+",FOLIO_NO=" + EditFoliono;
                ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','"+uContent+"')";
                ub = cm.procedureExecute(ulog);

              UpdateQuery="Update Scripts set CERTIFICATE_NO='"+CertificateNoCur+"' where CERTIFICATE_NO='" + CertificateNoOld+"'";
              cm.queryExecute(UpdateQuery);

              uContent="Update on Scripts Table CertificateNoOld="+CertificateNoOld;
			  ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','"+uContent+"')";
  			  ub = cm.procedureExecute(ulog);

            }
            catch(Exception ex)
            {
            }
          }
          else
          {
             UpdateQuery="Update Certificate set CERTIFICATE_ISSUE_DATE=TO_DATE('"+CertificateIssueDate+"', 'DD/MM/YYYY'),VALIDITY='"+CertificateValidity+"', COLLECTED='"+CertificateCollected +"',\"LOCK\"='"+IsLocked+"', VERIFY='"+IsVerified +"', \"COMMENT\"='"+CComments+"' where CERTIFICATE_NO='" + CertificateNoOld+"'";

             cm.queryExecute(UpdateQuery);

             UpdateQuery="Update Shareholder set NAME='"+FolioName+"', ADDRESS1='"+FolioAddress+"' where FOLIO_NO='" + EditFoliono+"'";
              try{
                cm.queryExecute(UpdateQuery);
                  UpdateQuery="Update Shareholder set NAME='"+FolioName+"', ADDRESS1='"+FolioAddress+"' where FOLIO_NO='" + EditFoliono+"'";
                  cm.queryExecute(UpdateQuery);

                uContent="Update on Certificate Table CeN="+CertificateNoOld+",Na="+FolioName_old+",IsD="+CertificateIssueDateold+",Valid="+CertificateValidityold+",colle"+CertificateCollectedold+",LOCK="+IsLockedold+",Vari="+IsVerifiedold;
                ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','"+uContent+"')";
                ub = cm.procedureExecute(ulog);

                ub = cm.procedureExecute(ulog);
                uContent="Update on ShareHolder Table Na="+FolioName_old+",FOLIO_NO=" + EditFoliono;
                ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','"+uContent+"')";
                ub = cm.procedureExecute(ulog);

              }
            catch(Exception ex)
            {
            }
          }
        }
%>
<form action="CertificateEditSave.jsp" method="get" name="frmCertificateEditSave">
<script language="javascript">
alert('Data has been updated.');
location = "<%=request.getContextPath()%>/jsp/Certificate/CertificateEditView.jsp?searchFoliono=<%=EditFoliono%>";
</script>

</form>
</body>
</html>
