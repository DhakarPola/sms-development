<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Updated By    : Renad Hakim
'Creation Date : January 2005
'Page Purpose  : View of all the Non- Printed Certificate Information
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Folios</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--

 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

function goNextPrevious(pstart,pend,certificateno)
{

  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Certificate/NonPrintedCertificatesView.jsp?StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;
  thisurl = thisurl + "&searchCertificate="+certificateno;
  location = thisurl;

}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
function toPrintCertificate()
{
  location = "<%=request.getContextPath()%>/jsp/Certificate/PrintCertificate.jsp?"
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
.style21 {
	color: #0044B0;
	font-size: 11px;
	font-weight:bold;
}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   String certmax = "";
   String certmin = "";
   int icertmax = 0;
   int icertmin = 0;
   int certlength = 9;

   String certqry = "SELECT  MAX(to_Number(CERTIFICATE_NO)) as CERTMAX FROM CERTIFICATE_VIEW WHERE LETTER_PRINTED = 'F'";
   cm.queryExecute(certqry);
   cm.toNext();
   icertmax = cm.getColumnI("CERTMAX");

   certmax = String.valueOf(icertmax);

   if (certmax.length() < certlength)
   {
     int sizedifferance = certlength - certmax.length();
     for (int ib=0;ib<sizedifferance;ib++)
     {
      certmax = "0" + certmax;
     }
   }

   certqry = "SELECT  MIN(to_Number(CERTIFICATE_NO)) as CERTMIN FROM CERTIFICATE_VIEW WHERE LETTER_PRINTED = 'F'";
   cm.queryExecute(certqry);
   cm.toNext();
   icertmin = cm.getColumnI("CERTMIN");

   certmin = String.valueOf(icertmin);

   if (certmin.length() < certlength)
   {
     int sizedifferance1 = certlength - certmin.length();
     for (int ib1=0;ib1<sizedifferance1;ib1++)
     {
      certmin = "0" + certmin;
     }
   }

   int certificate_state=0;
   int COUNT;
   int MAX_RECORDS=20;
   String certificate_no=request.getParameter("searchCertificate");
   String name = "";
   String hFolio="";
   String sFolio="";
   String dest = "";

 //Used for NEXT/PREVIOUS
    String SStartingValue = String.valueOf(request.getParameter("StartValue"));
    String SEndingValue = String.valueOf(request.getParameter("EndValue"));


    if(SStartingValue == null || SStartingValue.equals("") || SStartingValue.equals("null")) SStartingValue="1";
    if(SEndingValue == null || SEndingValue.equals("") || SEndingValue.equals("null")) SEndingValue="0";



    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    int Chunk = MAX_RECORDS;
    EndingValue = StartingValue + Chunk - 1;
    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;

    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }


   if(certificate_no==null)
   {
     certificate_no="000000000";
   }


   String query1;


     query1 =  "SELECT * FROM (SELECT div.*, rownum rnum FROM  ";
     query1 += "(select c.CERTIFICATE_NO CERT, s.NAME NAME, c.HOLDER_FOLIO AS HFOLIO, t.SELLER_FOLIO SFOLIO";
     query1 += " from TRANSFER t, CERTIFICATE c,SHAREHOLDER s ";
     query1 += " WHERE (c.CERTIFICATE_NO = t.NEW_CERTIFICATE_NO(+)  AND c.HOLDER_FOLIO = s.FOLIO_NO AND c.VALIDITY='T' AND c.LETTER_PRINTED='F' AND c.CERTIFICATE_NO >='"+certificate_no+"') ";
     query1 += " ORDER BY c.CERTIFICATE_NO ) div";
     query1 += " WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;

  cm.queryExecute(query1);

   COUNT=0;
   while(cm.toNext())
   {
     COUNT++;
   }
   cm.queryExecute(query1);


%>
  <span class="style7">
  <form method="GET" action="NonPrintedCertificatesView.jsp">
  <SPAN id="dprint">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
  </SPAN>
  <div align="right" class="style21"><b>Not Printed Certificates : <%=certmin%> - <%=certmax%></b></div>
  <table width="100%" BORDER=1  cellpadding="0" bordercolor="0044B0" style="border-collapse: collapse" bordercolor="#06689E">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7" height="30"><center>Not Printed Certificate Information</center></td></tr>
  <tr>
  	<td>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0">
			<tr bgcolor="#E8F3FD">
				<td width="6%"></td>
				<td width="8%"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goNextPrevious(<%=pStartingValue%>,<%=pEndingValue%>,<%=certificate_no%>)"></td>
				<td width="9%"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNextPrevious(<%=nStartingValue%>,<%=nEndingValue%>,<%=certificate_no%>)"></td>
				<td width="9%">&nbsp;</td>
				<td width="18%">&nbsp;</td>
				<td width="15%" align="left" class="style12"><b>Enter Certificate No:</b></td>
				<td width="20%"><input name="searchCertificate" type="text" >
                                        <input type="hidden" name="hcertificate" value="<%=certificate_no%>"/>              </td>
				<td width="15%"><img src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="SubmitThis()"></td>
			</tr>
                </table>
	</td>
  </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="0044B0">
  <tr bgcolor="#0044B0">
    <td width="20%" class="style9" align="center">Certificate No</td>
    <td width="50%"><div align="center" class="style9">
      <div align="center">Holder's Name</div>
    </div></td>
    <td width="15%"><div align="center" class="style9">
      <div align="center">Holder Folio</div>
    </div></td>
    <td width="15%" align="center" class="style9">
      Seller's Folio
    </td>
  </tr>
  <div align="left">
     <%
     certificate_state=0;
    while(cm.toNext() && COUNT!=0)
     {
       certificate_state++;
       certificate_no=cm.getColumnS("CERT");

       name = cm.getColumnS("NAME");
       hFolio = cm.getColumnS("HFOLIO");
       sFolio = cm.getColumnS("SFOLIO");


       dest = request.getContextPath() + "/jsp/Folio/EditFolio.jsp";

       if (!certificate_no.equals("null"))
       {
         if (String.valueOf(name).equals("null"))
           name = "";
         if (String.valueOf(hFolio ).equals("null"))
           hFolio = "";
         if (String.valueOf(sFolio ).equals("null"))
           sFolio = "";
         %>
    </div>
    <tr bgcolor="#E8F3FD">
           <td ><div align="center" class="style10">
             <div align="center" class="style10">
               <span class="style12">
                  <%=certificate_no%>
               &nbsp;
               <%
               if(certificate_state==COUNT)
               {
               %>
                 <input type="hidden" name="svalue" value="<%=StartingValue%>">
                 <input type="hidden" name="evalue" value="<%=EndingValue%>">
              <%}%>
               </span></div>
             </div></td>
           <td class="style13"><div align="left" class="style12">
             <div>&nbsp;&nbsp;&nbsp;<%=name%>&nbsp;</div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=hFolio%>&nbsp;</div>
           </div></td>
          <td class="style13"><div align="center" class="style12">
             <div align="center"><%=sFolio%>&nbsp;</div>
           </div></td>
    </tr>
         <div align="left" class="style13">
             <%
         }
     }

   if (isConnect)
   {
     cm.takeDown();
   }
  %>
         </div>
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD" ><center>

</body>
</html>
