<html>
<script>
/*
'******************************************************************************************************************************************
'Script Author : Rahat
'Updated By    :
'Creation Date : January 2005
'Page Purpose  : View to Edit Certificates
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="util"  class="batbsms.Utility" scope="session"/>
<jsp:useBean id="cert"  class="batbsms.Certificate" scope="session"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>


<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.imagehand{
  cursor:hand;
  color:#FFFFFF;
 }

-->
</style>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>

<script type="text/javascript">
<!--
function goNextPrevious(pstart,pend,foliono)
{

  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Certificate/CertificateEditView.jsp?StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;
  thisurl = thisurl + "&searchFoliono="+foliono;
  location = thisurl;

}
//Execute while click on Submit
function SubmitThis() {
 count = 0;
 //BlankValidate('searchFoliono','- Folio No (Option must be entered)');
/* BlankValidate('searchName','- Name (Option must be entered)');
 BlankValidate('distFrom','- Dist. From (Option must be entered)');
 BlankValidate('distTo','- Dist. To (Option must be entered)');

 if(document.forms[0].searchFoliono.value.length == 8)
 { */
   if (count == 0)
    {
        document.forms[0].submit();
    }
    else
    {
      ShowAllAlertMsg();
      return false;
    }
 /* }
  else  alert("Folio Number should be 8 characters long");*/
}

function changeCertificatePopup(oldCertificate)
{
  var url="CertificateEditPopup.jsp";
  if(document.forms[0].oldCertificate.value=="")
    alert("Please search for the certificate first.");
  else
    window.open("CertificateEditPopup.jsp","","HEIGHT=200,WIDTH=500");
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.TableData {
	font-size: 11px;
	font-family:arial;
	vertical-align:text-top;
	vertical-align:top;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>
<body>
<%
boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
	int COUNT;
        int MAX_RECORDS=15;

    String searchFoliono = util.changeIfNull(String.valueOf(request.getParameter("searchFoliono")));
	String searchCertificateNo = util.changeIfNull(String.valueOf(request.getParameter("searchCertificateNo")));
	if (String.valueOf(searchFoliono).equals("null"))
          searchFoliono = "";
	
    String nameFromDB="";
    String cert_distFromDB="";
    String cert_distToDB="";
    String vFoliono = "";
    String vName= "";
    String vAddress= "";
    String oldCertificate= "";
    String newCertificate= "";

    String FolioNo="";
   String CertificateNo="";
   String DistForm="";
   String DistTo="";
   String FolioName="";
   String FolioAddress="";
   String CertificateIssueDate="";
   String CertificateValidity="";
   String LetterPrinted="";
   String Collected="";
   String IsLocked="";
   String Verified="";
   String Comments="";
   //Used for NEXT/PREVIOUS
    String SStartingValue = String.valueOf(request.getParameter("StartValue"));
    String SEndingValue = String.valueOf(request.getParameter("EndValue"));


    if(SStartingValue == null || SStartingValue.equals("") || SStartingValue.equals("null")) SStartingValue="1";
    if(SEndingValue == null || SEndingValue.equals("") || SEndingValue.equals("null")) SEndingValue="0";



    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    int Chunk = MAX_RECORDS;
    EndingValue = StartingValue + Chunk - 1;
    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;

    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }

   String SelectQuery="";
	if(searchCertificateNo.length()>0 && searchFoliono.length()>0){
		SelectQuery= "SELECT * FROM (SELECT div.*, rownum rnum FROM (Select CV.FOLIO_NO,CV.CERTIFICATE_NO,CV.DIST_FROM,CV.DIST_TO ,CV.NAME,CV.ADDRESS1,";
		SelectQuery=SelectQuery + "CV.CERTIFICATE_ISSUE_DATE,CV.VALIDITY,CV.LETTER_PRINTED,CV.COLLECTED,CV.LOCKED,CV.VERIFIED,CV.COMMENTS from CERTIFICATEEDIT_VIEW CV ";
		SelectQuery=SelectQuery + "where CV.FOLIO_NO='" + searchFoliono.trim() + "' and CV.CERTIFICATE_NO='" + searchCertificateNo.trim() + "' order By CV.CERTIFICATE_NO) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;
	}
	else if(searchCertificateNo.length()>0){
		SelectQuery= "SELECT * FROM (SELECT div.*, rownum rnum FROM (Select CV.FOLIO_NO,CV.CERTIFICATE_NO,CV.DIST_FROM,CV.DIST_TO ,CV.NAME,CV.ADDRESS1,";
		SelectQuery=SelectQuery + "CV.CERTIFICATE_ISSUE_DATE,CV.VALIDITY,CV.LETTER_PRINTED,CV.COLLECTED,CV.LOCKED,CV.VERIFIED,CV.COMMENTS from CERTIFICATEEDIT_VIEW CV ";
		SelectQuery=SelectQuery + "where CV.CERTIFICATE_NO='" + searchCertificateNo.trim() + "' order By CV.CERTIFICATE_NO) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;
	}
	else{
		SelectQuery= "SELECT * FROM (SELECT div.*, rownum rnum FROM (Select CV.FOLIO_NO,CV.CERTIFICATE_NO,CV.DIST_FROM,CV.DIST_TO ,CV.NAME,CV.ADDRESS1,";
		SelectQuery=SelectQuery + "CV.CERTIFICATE_ISSUE_DATE,CV.VALIDITY,CV.LETTER_PRINTED,CV.COLLECTED,CV.LOCKED,CV.VERIFIED,CV.COMMENTS from CERTIFICATEEDIT_VIEW CV ";
		SelectQuery=SelectQuery + "where CV.FOLIO_NO='" + searchFoliono.trim() + "' order By CV.CERTIFICATE_NO) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;
	}
	
	 int fcc = cm.queryExecuteCount(SelectQuery);

    if (nStartingValue > (fcc-Chunk+1))
    {
      nStartingValue = fcc - Chunk + 1;
    }
    if (nEndingValue > fcc)
    {
      nEndingValue = fcc;
    }
    if (nStartingValue < 1)
    {
      nStartingValue = 1;
    }
    if (nEndingValue < Chunk)
    {
      nEndingValue = Chunk;
    }
	
	
%>
<form action="CertificateEditView.jsp" method="get" name="frmCertificateEditView">
<table width="100%" border="1" cellspacing="0" cellpadding="0" style="border-collapse:collapse" bordercolor="#0044B0" bgcolor="#E8F3FD">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" >
      <tr>
        <td bgcolor="#0044B0" class="style7" height="30">Find Certificate </td>
      </tr>
      <tr>
        <td></td>
      </tr>
    </table>
	</td>
  </tr>
  <tr>
    <td><br><table width="100%" border="0" cellspacing="0" cellpadding="5">	
          <tr>
            <td width="22%" class="style9" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;Folio No. </td>
            <td width="2%" align="center" valign="middle">:</td>
            <td width="76%" valign="middle">
				<input name="searchFoliono" type="text" id="searchFoliono" maxlength="8" class="SL70TextField" value="<%=searchFoliono%>">
			</td>
            </tr>
			<tr>
            <td width="22%" class="style9" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;Certificate No.</td>
            <td width="2%" align="center" valign="middle">:</td>
            <td width="76%" valign="middle">
				<input name="searchCertificateNo" type="text" id="searchCertificateNo" maxlength="10" class="SL70TextField" value="">
			</td>
            </tr>
			<tr>
			<td valign="middle" colspan="3" align="center"><br><img name="B1" src="<%=request.getContextPath()%>/images/btnSearch.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSearchR.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnSearch.gif'"><br>&nbsp;</td>
            </tr>
        </table>
	</td>
  </tr>
  <tr>
    <td>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
		<td bgcolor="#0044B0" class="style7" height="25">Content of the Record</td>
		<td width="6%" bgcolor="#0044B0" class="style7"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goNextPrevious(<%=pStartingValue%>,<%=pEndingValue%>,'<%=searchFoliono%>')"></td>
		<td width="6%" align="right" bgcolor="#0044B0" class="style7"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNextPrevious(<%=nStartingValue%>,<%=nEndingValue%>,'<%=searchFoliono%>')">&nbsp;&nbsp;</td>
	  </tr>
	</table>
	</td>
  </tr>
  <tr>
    <td>
	<table width="100%" border="0" cellspacing="0" cellpadding="4">
           <%
  try{
    cm.queryExecute(SelectQuery);
    String PreviousFolio="";
	String FirstRec="T";
    String PreviousCertificate="";
    while(cm.toNext())
     {

       FolioNo=cm.getColumnS("FOLIO_NO");
        CertificateNo=cm.getColumnS("CERTIFICATE_NO");
        DistForm=cm.getColumnS("DIST_FROM");
        DistTo=cm.getColumnS("DIST_TO");
        FolioName=cm.getColumnS("NAME");
        FolioAddress=cm.getColumnS("ADDRESS1");
        CertificateIssueDate=cm.getColumnDT("CERTIFICATE_ISSUE_DATE");
        CertificateValidity=cm.getColumnS("VALIDITY");
        LetterPrinted=cm.getColumnS("VALIDITY");
	   Collected=cm.getColumnS("COLLECTED");
	   IsLocked=cm.getColumnS("LOCKED");
	   Verified=cm.getColumnS("VERIFIED");
	   Comments=cm.getColumnS("COMMENTS");
	   if (String.valueOf(Comments).equals("null"))
      		Comments = "";
		if(FirstRec.equalsIgnoreCase("T")){
			FirstRec="F";
                %>
			<Tr><td colspan="9">
			<table width=100% cellpadding="5">
			<tr>
			  <td width="17%" align="left" class="style9">&nbsp;&nbsp;&nbsp;&nbsp;Folio No.</td>
                  <td width="2%" align="center" class="style9">:</td>
                  <td width="80%"  align="left" class="TableData"><%=FolioNo%></td>
            </tr>
			<tr>
			  <td width="17%" align="left" class="style9">&nbsp;&nbsp;&nbsp;&nbsp;Name</td>
                  <td width="2%" align="center" class="style9">:</td>
                  <td width="80%"  align="left" class="TableData"><%=FolioName%></td>
            </tr>
			<tr>
			  <td width="17%" align="left" class="style9">&nbsp;&nbsp;&nbsp;&nbsp;Address</td>
                  <td width="2%" align="center" class="style9">:</td>
                  <td width="80%"  align="left" class="TableData"><%=FolioAddress%></td>
            </tr>
			</table></td></tr>
			 <tr bgcolor="#0044B0">
                  <td width="10%" align="center" class="style7">Cert. No.</td>
                  <td width="10%" align="center" class="style7">Issue&nbsp;Date</td>
                  <td width="8%" align="center" class="style7">Validity</td>
                  <td width="10%" align="center" class="style7">Collected</td>
                  <td width="6%" align="center" class="style7">Lock</td>
                  <td width="7%" align="center" class="style7">Verify</td>
                  <td width="7%" align="center" class="style7">Printed</td>
                  <td width="22%" align="center" class="style7">Comments</td>
                  <td width="20%" align="center" class="style7">Distinction No.</td>
            </tr>

			<%


		}
        if(PreviousFolio.equalsIgnoreCase(FolioNo)){
          if(PreviousCertificate.equalsIgnoreCase(CertificateNo)){%>
                 <tr>
                  <td width="10%" align="center" class="TableData"></td>
                  <td width="10%" align="center" class="TableData"></td>
                  <td width="8%" align="center" class="TableData"></td>
                  <td width="10%" align="center" class="TableData"></td>
                  <td width="6%" align="center" class="TableData"></td>
                  <td width="7%" align="center" class="TableData"></td>
				  <td width="7%" align="center" class="TableData"></td>
				  <td width="22%" align="center" class="TableData"></td>
                  <td width="20%" align="center" class="TableData"><%=DistForm+" - "+DistTo%></td>
                 </tr>
          <%
          PreviousCertificate=CertificateNo;
          }else{
          %>
          <tr>
		  <td width="10%" align="center" class="TableData"><a href="CertificateEditUpdate.jsp?Cfoliono=<%=FolioNo%>&CCertificateNo=<%=CertificateNo%>"><%=CertificateNo%></a></td>
		  <td width="10%" align="center" class="TableData"><%=CertificateIssueDate%></td>
		  <td width="8%" align="center" class="TableData"><%=CertificateValidity%></td>
		  <td width="10%" align="center" class="TableData"><%=Collected%></td>
		  <td width="6%" align="center" class="TableData"><%=IsLocked%></td>
		  <td width="7%" align="center" class="TableData"><%=Verified%></td>
		  <td width="7%" align="center" class="TableData"><%=LetterPrinted%></td>
		  <td width="22%" align="center" class="TableData"><%=Comments%></td>
		  <td width="20%" align="center" class="TableData"><%=DistForm+" - "+DistTo%></td>

          </tr>
          <%
          PreviousCertificate=CertificateNo;
          }
          PreviousFolio=FolioNo;
          }
          else{%>
          <tr>
		  <td width="10%" align="center" class="TableData"><a href="CertificateEditUpdate.jsp?Cfoliono=<%=FolioNo%>&CCertificateNo=<%=CertificateNo%>"><%=CertificateNo%></a></td>
		  <td width="10%" align="center" class="TableData"><%=CertificateIssueDate%></td>
		  <td width="8%" align="center"  class="TableData"><%=CertificateValidity%></td>
		  <td width="10%" align="center"  class="TableData"><%=Collected%></td>
		  <td width="6%" align="center"  class="TableData"><%=IsLocked%></td>
		  <td width="7%" align="center" class="TableData"><%=Verified%></td>
		  <td width="7%" align="center" class="TableData"><%=LetterPrinted%></td>
		  <td width="22%" align="center" class="TableData"><%=Comments%></td>
		  <td width="20%" align="center" class="TableData"><%=DistForm+" - "+DistTo%></td>
          </tr>
          <%
            PreviousFolio=FolioNo;
            PreviousCertificate=CertificateNo;
          }
     }
    }
    catch(Exception ex)
    {
    }
            %>
          </table>
	</td>
  </tr>
  <tr>
    <td>
	</td>
  </tr>
  <input type="hidden" name="svalue" value="<%=StartingValue%>">
  <input type="hidden" name="evalue" value="<%=EndingValue%>">
</table>
</form>
</body>
</html>
