<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Updated By    : Renad Hakim
'Creation Date : January 2006
'Page Purpose  : Unlocks certificate.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@ page language="java" import="oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*,org.apache.commons.fileupload.*,java.io.*,java.util.*,java.util.List,java.util.Vector,java.util.Date,java.text.SimpleDateFormat,java.util.Vector,java.math.*" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>

<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
     Date d = new Date();
     int thisyear = d.getYear();
     thisyear = thisyear + 1900;
     int thismonth = d.getMonth();
     thismonth = thismonth + 1;
     int thisday = d.getDate();
     int thisday1 = d.getDay();
     String wday = "";
     String thismonth1 = "";
     if (thisday1 == 1)
       wday = "Monday";
     else if (thisday1 == 2)
       wday = "Tuesday";
     else if (thisday1 == 3)
       wday = "Wednesday";
     else if (thisday1 == 4)
       wday = "Thursday";
     else if (thisday1 == 5)
       wday = "Friday";
     else if (thisday1 == 6)
       wday = "Saturday";
     else if (thisday1 == 0)
       wday = "Sunday";

     if (thismonth == 1)
       thismonth1 = "January";
     else if (thismonth == 2)
       thismonth1 = "February";
     else if (thismonth == 3)
       thismonth1 = "March";
     else if (thismonth == 4)
       thismonth1 = "April";
     else if (thismonth == 5)
       thismonth1 = "May";
     else if (thismonth == 6)
       thismonth1 = "June";
     else if (thismonth == 7)
       thismonth1 = "July";
     else if (thismonth == 8)
       thismonth1 = "August";
     else if (thismonth == 9)
       thismonth1 = "September";
     else if (thismonth == 10)
       thismonth1 = "October";
     else if (thismonth == 11)
       thismonth1 = "November";
     else if (thismonth == 12)
       thismonth1 = "December";

     String formdate = wday + ", " + thismonth1 + " " + thisday + ", " + thisyear;

     String lockedby = "Unlocked By: " + String.valueOf(session.getAttribute("FullName"));
     lockedby = lockedby + "\r\nDate: " + formdate;

  String updateCommentQuery="";
  String certificateno = String.valueOf(request.getParameter("certificateno"));
  certificateno=cm.replace(certificateno,"'","''");
  String comments = String.valueOf(request.getParameter("comments"));
  comments=cm.replace(comments,"'","''");
  if (String.valueOf(comments).equals("null"))
   comments = "";

  lockedby = lockedby + "\r\nComments: " + comments;

  String checkifCertExistQuery = "SELECT * FROM CERTIFICATE WHERE \"LOCK\"='T' AND CERTIFICATE_NO='"+certificateno+"'";
  cm.queryExecute(checkifCertExistQuery);

  cm.toNext();
  String excomments = cm.getColumnS("COMMENT");

  if (String.valueOf(excomments).equals("null"))
   excomments = "";

  comments = lockedby + "\r\n" + excomments;

  updateCommentQuery="CALL UPDATE_CERTIFICATE_COMMENT('"+certificateno +"','"+comments+"','F')";

//  updateCommentQuery="UPDATE CERTIFICATE SET \"LOCK\" = 'F' WHERE CERTIFICATE_NO='"+ certificateno +"'";
  boolean uCQR = cm.procedureExecute(updateCommentQuery);
  %>
  <script type="text/javascript">
    alert("Certificate Unlocked");
  </script>
  <%
/*
'******************************************************************************************************************************************
'Updated By    : Renad Hakim
'Date          : March 2005
'******************************************************************************************************************************************
*/

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Unlocked Certificate')";
  boolean ub = cm.procedureExecute(ulog);

 if (isConnect)
  {
    cm.takeDown();
  }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/jsp/Certificate/CertificateLock.jsp";
</script>
</body>
</html>
