
<%@ page language="java" import="oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*,org.apache.commons.fileupload.*,java.io.*,java.util.*,java.util.List,java.util.Vector,java.util.Date,java.text.SimpleDateFormat" %>
<html>
<title>Upload Signature</title>
<head>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function formconfirm()
{
  if (confirm("Do you want to attatch this image?"))
  {
    return true;
  }
}

//Execute while click on Submit
function SubmitThis() {


  if(document.forms[0].signaturepath.value == "")
  {
    alert("Please attatch an image");
  }
  else
  {
    if (count == 0)
    {
        document.forms[0].submit();
    }
    else{
      ShowAllAlertMsg();
      return false;
    }
  }
}

function closeSig()
{

  var regexp = /\\/g;

    if (formconfirm())
    {
      window.opener.document.forms[0].signature.value = document.forms[0].path.value.replace(regexp,"\\\\");
    }
  self.close();
}

//-->
</script>

<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}

.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {
        color: #0A2769;
        font-weight: bold;
	font-size: 13px;
        }
.style12 {color: #FFFFFF}
-->
</style>
</head>
<body bgcolor="#ffffff">
<form action="UploadNewSignature.jsp" method="post" enctype="multipart/form-data"  name="newUploadSignatureForm">
  <table width="100%"  border="0">
    <tr>
      <td><table width="100%"  border="0" cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
        <tr bgcolor="#E8F3FD">
          <td colspan="2" bgcolor="#0044B0" class="style7">Upload User Signature </td>
          </tr>
       <tr bgcolor="#E8F3FD" >
          <td class="style8">Upload Signature: </td>
          <td><input name="signaturepath" type="file" id="signaturepath"></td>
        </tr>

      <%
        String CurrentFile = "";
        String fname ="";

        String signaturepath=null;


        DiskFileUpload u1 = new DiskFileUpload();


        if(request.getContentLength()>0)
        {
          List items1 = u1.parseRequest(request);

          Iterator itr1 = items1.iterator();

          File savedFile=null ;
          File fullFile=null ;

          List items = items1;
          Iterator itr = items.iterator();

          FileItem item = (FileItem) itr.next();

          while(itr1.hasNext())
          {
            FileItem item1 = (FileItem) itr1.next();
            String fieldName = item1.getFieldName();
            if(fieldName.equals("signaturepath"))
            {
              CurrentFile = item1.getName();
              fullFile  = new File(item1.getName());
              fname =  fullFile.getName();
              item = item1;
              signaturepath =fullFile.getPath();

            }
          }//end of while
        }
        if(signaturepath !=null)
        {
        %>
        <tr bgcolor="#E8F3FD">
		<td class="style8">Preview: </td>
        <td colspan="2"><img name="imgsignature" alt="" src="<%=signaturepath%>"/>
        </td>
        </tr>
        <%
            }
        %>
        <tr bgcolor="#E8F3FD">
          <td colspan="2" align="center">
          <input type="hidden" name="path" value="<%=signaturepath%>"/>
            <img name="B1" src="<%=request.getContextPath()%>/images/btnAttach.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnAttachR.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnAttach.gif'">
            &nbsp;&nbsp;
            <img name="B2" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="closeSig()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
          </td>
        </tr>

        <tr bgcolor="#E8F3FD">
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
    </tr>
  </table>
</form>
</body>
</html>
