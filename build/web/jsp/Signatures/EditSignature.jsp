<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim/Fahmi Sarker
'Creation Date : October 2005
'Page Purpose  : Edits Signature Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.io.*,oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*"%>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}
%>

<html>
<head>
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   String signame = request.getParameter("sname1");
   String Ssigid = request.getParameter("sid1");
   int sigid = Integer.parseInt(Ssigid);
   String sigrank = "";
   String sigsign = "";

   String query1 = "SELECT * FROM SIGNATURE_VIEW WHERE SIG_ID = " + sigid + " AND NAME = '"+signame+ "'";
   cm.queryExecute(query1);

   while(cm.toNext())
     {
       sigrank = cm.getColumnS("RANK");
       sigsign = cm.getColumnS("SIGN");

       if (String.valueOf(sigrank).equals("null"))
         sigrank = "";
       if (String.valueOf(sigsign).equals("null"))
         sigsign = "";
     }
%>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit Signature</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function gotosig()
{
  location = "<%=request.getContextPath()%>/jsp/Signatures/AllSignatures.jsp";
}

function editSignatureWindow()
{
  	window.open("EditSignaturePopup.jsp","","HEIGHT=350,WIDTH=500")
}

function gotodelete()
{
  if (confirm("Do you want to delete the Signature?"))
  {
    location = "<%=request.getContextPath()%>/jsp/Signatures/DeleteSignature.jsp?sigid2=<%=Ssigid%>";
  }
}

function formconfirm()
{
  if (confirm("Do you want to change the Signature Information?"))
  {
    return true;
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('sname','- Name (Option must be entered)');
  BlankValidate('srank','- Rank (Option must be entered)');

  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">

<form action="UpdateSignature.jsp" method="get" enctype="multipart/form-data" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Signature Information</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Name</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="sname" value="<%=signame%>" class="SL2TextField">
</div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Designation</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="srank" value="<%=sigrank%>" class="SL2TextField"">
        </div></td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Signature</div></th>
        <td valign="top">:</td>
        <%
                 String selectQuery="SELECT SIGN FROM SIGNATURE WHERE SIG_ID = "+Ssigid;
                  byte[] imgData =  cm.getPhoto( cm.getConnection(), selectQuery  );

        %>
        <td valign="top">
        <table width="149">
                  <tr><td width="41">
                  <%
                     if(imgData.length !=0)
                     {
                  %>
                    <img alt="Signature" src="image.jsp?selectQuery=<%=selectQuery%>" /></td>
                    <%
                     }
                     else
                     {
                     %>
                     <b>(Signature Unavailable)</b>
                       <%
                       }
                     %>
                       <td width="96" valign="top">
                      <!-- <input type="button" name="ButtonSignature" value="Edit" onClick="editSignatureWindow()">-->
                      &nbsp;&nbsp;<img name="B6" src="<%=request.getContextPath()%>/images/btnEdit.gif"  onclick="editSignatureWindow()" onMouseOver="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnEditR.gif'" onMouseOut="document.forms[0].B6.src = '<%=request.getContextPath()%>/images/btnEdit.gif'">
                       <input type="hidden" name="signature">
                       <input type="hidden" name="signaturestatus" value="edit"/>
                                </td>
                  </tr></table>
        </td>
      </tr>
  </table>

  <!--HIDDEN FIELDS-->
  <input type="hidden" name="fsigid" value="<%=Ssigid%>">

  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="43%" scope="row">
            <div align="right">
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
            </div></th>
        <td width="57%">
	          <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="gotosig()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
        </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
