<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : May 2007
'Page Purpose  : Processes barcode
'******************************************************************************************************************************************
*/
</script>
<%@page errorPage="CommonError.jsp"%>
<%@page language="java" import="oracle.jdbc.driver.*,oracle.sql.BLOB,java.sql.*,org.apache.commons.fileupload.*,java.io.*,java.util.*,java.util.List,java.util.Vector,java.util.Date,java.text.SimpleDateFormat,oracle.jdbc.driver.*"%>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<jsp:useBean id="util" class="batbsms.Utility"/>
<jsp:useBean id="cert" class="batbsms.Certificate"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css">
</LINK><%if (String.valueOf(session.getAttribute("UserName")).equals("null")) {%>
<script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%}%>
<html>
<head>
<style type="text/css">
  <!--
    body {
    background-color: #FFFFFF;
    }
    body,td,th {
    color: #000000;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
    }
  -->
</style>
<script type="text/javascript">
function showComplete()
{
    document.all.spanEnd.style.display = '';
    document.all.spanStart.style.display = 'none';
}
</script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Bar Code</title>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>
<body TEXT="000000" BGCOLOR="FFFFFF">
<%
  boolean isConnect = cm.connect();
  if (isConnect == false) {
%>
<jsp:forward page="ErrorMsg.jsp">
  <jsp:param name="ErrorTitle" value="Connection Failure"/>
  <jsp:param name="ErrorHeading" value="Connection Problem"/>
  <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator"/>
</jsp:forward>
<%
  }
  String certificateno = util.changeIfNullnTrim(String.valueOf(request.getParameter("txtCertificate")));
  String certificateFrom =  util.changeIfNullnTrim(String.valueOf(request.getParameter("txtCertificateFrom")));
  String certificateTo =  util.changeIfNullnTrim(String.valueOf(request.getParameter("txtCertificateTo")));
  String printStyle =   util.changeIfNullnTrim(String.valueOf(request.getParameter("rdoPrintStyle")));


  String path=application.getRealPath(request.getContextPath());

  cert.deleteBarcodes();

  try
  {
    %>

        <span id="spanStart">
            <h2 align="center" >Processing Image...</h2>
        </span>
    <%

        if(printStyle.equals("on"))
          cert.insertBarCode(certificateno,path);
        else
          if(printStyle.equals("range"))
            cert.insertBarCodeRangeRemat(certificateFrom,certificateTo,"");


        %>
        <span id="spanEnd" style="display:none">
            <h2 align="center">Generating Report...</h2>
        </span>
        <script type="text/javascript">
          showComplete();
        </script>

        <%
        if (isConnect) {
          cm.takeDown();
        }
  }
  catch (Exception ex) {
    ex.printStackTrace();
  }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/PrintRematCertificate.jsp?certificateno=<%=certificateno%>&certificateFrom=<%=certificateFrom%>&certificateTo=<%=certificateTo%>&printStyle=<%=printStyle%>";
</script>
</body>
</html>
