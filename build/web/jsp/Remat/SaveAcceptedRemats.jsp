<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : May 2007
'Page Purpose  : Saves Accepted Remats.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@ page language="java" import="java.sql.*,java.util.Vector,java.util.Date,java.text.SimpleDateFormat,java.math.*" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Save Accepted Remats</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

    String FolioNo = "";
    String BOID = "";
    String DNRStart = "";
    String DNREnd = "";
    int RQuantity = 0;
    String RRN1 = "";
    String TraceNo = "";
    String RTA_Date = "";
    String RRF1 = "";
    String RStatus = "";
    String BalanceType = "";
    String CertificateNo = "";
    int distfrom = 0;
    int distto = 0;

    int totalrequests = 0;

    String query1 = "SELECT * FROM REMAT_TEMP_VIEW";
    cm.queryExecute(query1);

    String dlastremat = "call DELETE_LAST_REMAT()";
    boolean b1 = cm1.procedureExecute(dlastremat);

    while(cm.toNext())
     {
       totalrequests++;

       FolioNo = cm.getColumnS("FOLIO_NO");
       BOID = cm.getColumnS("BO_ID");
       RQuantity = cm.getColumnI("REMAT_QTY");
       DNRStart = cm.getColumnS("DNR_START");
       DNREnd = cm.getColumnS("DNR_END");
       RRN1 = cm.getColumnS("RRN");
       TraceNo = cm.getColumnS("TRACE_NO");
       RTA_Date = cm.getColumnDT("RTA_CON_DATE");
       RRF1 = cm.getColumnS("RRF");
       RStatus = cm.getColumnS("REMAT_STATUS");
       BalanceType = cm.getColumnS("BALANCE_TYPE");
       CertificateNo = cm.getColumnS("CERTIFICATE_NO");

       if (String.valueOf(RRN1).equals("null"))
         RRN1 = "";
       if (String.valueOf(TraceNo).equals("null"))
         TraceNo = "";
       if (String.valueOf(RTA_Date).equals("null"))
         RTA_Date = "";
       if (String.valueOf(RRF1).equals("null"))
         RRF1 = "";
       if (String.valueOf(RStatus).equals("null"))
         RStatus = "";
       if (String.valueOf(BalanceType).equals("null"))
         BalanceType = "";

       String qdistmax = "SELECT MAX(to_Number(DIST_TO)) as DISTMAX FROM SCRIPTS_VIEW";
       cm1.queryExecute(qdistmax);
       cm1.toNext();
       distfrom = cm1.getColumnI("DISTMAX");

       distfrom = distfrom + 1;
       distto = distfrom + RQuantity - 1;

       String addremat = "call ADD_REMAT('" + RRN1 + "', '" + TraceNo + "', '" + RTA_Date + "', '" + FolioNo + "', '" + BOID + "','" + CertificateNo + "','" + RRF1 + "','" + RStatus + "','" + BalanceType + "','" + DNRStart + "','" + DNREnd + "'," + RQuantity + ")";
       boolean b7 = cm1.procedureExecute(addremat);

       String addlastremat = "call ADD_LAST_REMAT('" + RRN1 + "', '" + TraceNo + "', '" + RTA_Date + "', '" + FolioNo + "', '" + BOID + "','" + CertificateNo + "','" + RRF1 + "','" + RStatus + "','" + BalanceType + "','" + DNRStart + "','" + DNREnd + "'," + RQuantity + ")";
       boolean b10 = cm1.procedureExecute(addlastremat);

       String adddist = "call ADD_DISTINCTION(" + distfrom + ", " + distto + ", '" + CertificateNo + "')";
       boolean b9 = cm1.procedureExecute(adddist);

       String sbal = "call ADD_SHARE_BALANCE('" + FolioNo + "', " + RQuantity + ")";
       boolean sb = cm1.procedureExecute(sbal);
   }

  String delallremats = "DELETE REMAT_TEMP";
  cm.queryExecute(delallremats);

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Accepted Remat Request(s)')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm1.takeDown();
     cm.takeDown();
   }
%>
<script language="javascript">
  alert(<%=totalrequests%> + " Remat Request(s) Accepted!");
  location = "<%=request.getContextPath()%>/jsp/Remat/AcceptRemats.jsp";
</script>
</body>
</html>
