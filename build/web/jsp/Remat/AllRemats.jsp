<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : May 2007
'Page Purpose  : View for All Accepted Remats.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Accepted Remats</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
.style21 {
	color: #0044B0;
	font-size: 11px;
	font-weight:bold;
}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
    String grpquery = "SELECT SUM(REMAT_QTY) as TLCK FROM REMAT_VIEW";
    cm.queryExecute(grpquery);
    cm.toNext();
    int totalremat = cm.getColumnI("TLCK");

    String query1 = "SELECT * FROM REMAT_VIEW ORDER BY REMAT_ACC_DATE DESC";
    cm.queryExecute(query1);

    String foliono = "";
    String boid = "";
    String sname = "";
    int nofshares = 0;
    String certificateno = "";
    String dnrstart = "";
    String dnrend = "";
    int distfrom = 0;
    int distto = 0;
    String raccdate = "";

    String dest = "";

    int rowcounter = 0;
%>
  <span class="style7">
  <form name="FormToSubmit" method="GET" action="RejectPendingRemats.jsp">
  <SPAN id="dprint">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
  </SPAN>
  <div align="right" class="style21"><b>Total Rematerialized Shares = <%=totalremat%></b></div>
  <table width="100%" BORDER=1  cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Accepted Remats</center></td></tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
  <tr bgcolor="#0044B0">
    <td width="8%" class="style9"><div align="center">Folio No.</div></td>
    <td width="13%"><div align="center" class="style9">
      <div align="center">BO ID</div>
    </div></td>
    <td width="23%"><div align="center" class="style9">
      <div align="center">Name</div>
    </div></td>
    <td width="9%"><div align="center" class="style9">
      <div align="center">Certificate</div>
    </div></td>
    <td width="15%"><div align="center" class="style9">
      <div align="center">DNR</div>
    </div></td>
    <td width="17%"><div align="center" class="style9">
      <div align="center">Distinctions</div>
    </div></td>
    <td width="7%"><div align="center" class="style9">
      <div align="center">Shares</div>
    </div></td>
    <td width="8%"><div align="center" class="style9">
      <div align="center">Date</div>
    </div></td>
  </tr>
  <div align="left">
     <%
    while(cm.toNext())
     {
       rowcounter++;
       foliono = cm.getColumnS("FOLIO_NO");
       boid = cm.getColumnS("BO_ID");
       sname = cm.getColumnS("NAME");
       nofshares = cm.getColumnI("REMAT_QTY");
       certificateno = cm.getColumnS("CERTIFICATE_NO");
       dnrstart = cm.getColumnS("DNR_START");
       dnrend = cm.getColumnS("DNR_END");
       distfrom = cm.getColumnI("DIST_FROM");
       distto = cm.getColumnI("DIST_TO");
       raccdate = cm.getColumnDT("REMAT_ACC_DATE");

       if (String.valueOf(sname).equals("null"))
         sname = "";

       dest = request.getContextPath() + "/jsp/Remat/SeeRemat.jsp?status1=all";

      %>
    </div>
  <tr bgcolor="#E8F3FD">
           <td class="style13"><div align="left" class="style12">
             <div align="center" class="style10">
               <span class="style13">
               <a HREF="<%=dest%>&rid1=<%=certificateno%>"><%=foliono%></a>
               </span></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="center" class="style10">
               <span class="style13">
               <a HREF="<%=dest%>&rid1=<%=certificateno%>"><%=boid%></a>
               </span></div>
           </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="left" class="style10">
               <span class="style13">
               <a HREF="<%=dest%>&rid1=<%=certificateno%>"><%=sname%></a>
               </span></div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center" class="style10">
               <span class="style13">
               <a HREF="<%=dest%>&rid1=<%=certificateno%>"><%=certificateno%></a>
               </span></div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=dnrstart%> - <%=dnrend%></div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=distfrom%> - <%=distto%></div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=nofshares%></div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=raccdate%></div>
           </div></td>
         </tr>
             <%
     }
     %>
       </form>
     <%
   if (isConnect)
   {
     cm.takeDown();
   }
  %>
</body>
</html>
