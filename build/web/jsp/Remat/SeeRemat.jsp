<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : May 2007
'Page Purpose  : See Remat Info.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit Remat Info.</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('folio_no','- Folio No. (Option must be entered)');
  BlankValidate('bo_id','- BO ID (Option must be entered)');
  BlankValidate('dnr_start','- DNR Start (Option must be entered)');
  BlankValidate('dnr_end','- DNR End (Option must be entered)');
  BlankValidate('remat_qty','- Remat Quantity (Option must be entered)');

  if (count == 0)
  {
    document.forms[0].submit();
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

    String FolioNo = "";
    String BOID = "";
    String DNRStart = "";
    String DNREnd = "";
    int RQuantity = 0;
    String RRN1 = "";
    String TraceNo = "";
    String RTA_Date = "";
    String RRF1 = "";
    String RStatus = "";
    String BalanceType = "";
    String raccdate = "";
    String sname = "";
    int distfrom = 0;
    int distto = 0;

    String certificateno = String.valueOf(request.getParameter("rid1"));
    String statusis = String.valueOf(request.getParameter("status1"));

    String query1 = "";

    if (statusis.equalsIgnoreCase("all"))
    {
      query1 = "SELECT * FROM REMAT_VIEW WHERE LOWER(CERTIFICATE_NO) = LOWER('" + certificateno + "')";
    }
    else if (statusis.equalsIgnoreCase("last"))
    {
      query1 = "SELECT * FROM LAST_REMAT_VIEW WHERE LOWER(CERTIFICATE_NO) = LOWER('" + certificateno + "')";
    }

    cm.queryExecute(query1);

    while(cm.toNext())
     {
       FolioNo = cm.getColumnS("FOLIO_NO");
       BOID = cm.getColumnS("BO_ID");
       RQuantity = cm.getColumnI("REMAT_QTY");
       DNRStart = cm.getColumnS("DNR_START");
       DNREnd = cm.getColumnS("DNR_END");
       RRN1 = cm.getColumnS("RRN");
       TraceNo = cm.getColumnS("TRACE_NO");
       RTA_Date = cm.getColumnDT("RTA_CON_DATE");
       RRF1 = cm.getColumnS("RRF");
       RStatus = cm.getColumnS("REMAT_STATUS");
       BalanceType = cm.getColumnS("BALANCE_TYPE");
       raccdate = cm.getColumnDT("REMAT_ACC_DATE");
       sname = cm.getColumnS("NAME");
       distfrom = cm.getColumnI("DIST_FROM");
       distto = cm.getColumnI("DIST_TO");

       if (String.valueOf(RRN1).equals("null"))
         RRN1 = "";
       if (String.valueOf(TraceNo).equals("null"))
         TraceNo = "";
       if (String.valueOf(RTA_Date).equals("null"))
         RTA_Date = "";
       if (String.valueOf(RRF1).equals("null"))
         RRF1 = "";
       if (String.valueOf(RStatus).equals("null"))
         RStatus = "";
       if (String.valueOf(BalanceType).equals("null"))
         BalanceType = "";
     }

%>

<form action="UpdateTempRemat.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Remat Information</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Shareholder Name</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style9"><%=sname%></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Folio No.</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style9"><%=FolioNo%></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;BO ID</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style9"><%=BOID%></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Certificate No.</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style9"><%=certificateno%></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;DNR</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style9"><%=DNRStart%> - <%=DNREnd%></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Distinctions</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style9"><%=distfrom%> - <%=distto%></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Remat Quantity</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style9"><%=RQuantity%></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;RRN</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style9"><%=RRN1%></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Trace No.</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style9"><%=TraceNo%></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;RTA Confirmation Date</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style9"><%=RTA_Date%></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;RRF</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style9"><%=RRF1%></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Status</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style9"><%=RStatus%></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Balance Type</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style9"><%=BalanceType%></div></td>
      </tr>
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Remat Acceptance Date</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left" class="style9"><%=raccdate%></div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="10" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="100%" scope="row">
            <div align="center">
                   <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="history.go(-1)" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
            </div></th>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
