select (nvl(uptoval,'')||To_CHAR(issue_date,'DD/MM/YYYY')) as issue_date,ClaimedAmount,UnClaimedAmount from (
(select uptoval,Cl.issue_date,ClaimedAmount,UnClaimedAmount from 
(Select issue_date,Sum(DIVIDEND) ClaimedAmount from DIVIDEND where COLLECTED='T' and remarks is null   and issue_date in (Select date_dec from dividenddeclaration where (account_no)=TO_CHAR({?myaccno})) group by issue_date) CL,
(Select  '' as uptoval,issue_date,Sum(DIVIDEND) UnClaimedAmount from DIVIDEND where COLLECTED='F' and remarks is null  and issue_date in (Select date_dec from dividenddeclaration where (account_no)=TO_CHAR({?myaccno})) group by issue_date) UCL where CL.issue_date=UCL.issue_date)
Union 
(SELECT  'Up to ' as uptoval,To_date('31/12/1988','DD/MM/YYYY') issue_date,0 ClaimedAmount,amount   from OFFSETAMOUNT where isactive='T') order by issue_date desc) 