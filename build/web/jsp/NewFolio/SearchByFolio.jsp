<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker-----
'Updated By    : Renad Hakim
'Creation Date : November 2005
'Page Purpose  : View of all the Permanent Folios
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>


<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<style type="text/css">
<!--
.style19 {color: #0A2769; font-weight: bold; }
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
-->
</style>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Folios</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--

function goNextPrevious(pstart,pend,foliono)
{
  var thisurl = "<%=request.getContextPath()%>";
  thisurl = thisurl + "/jsp/Folio/AllPermanentFolios.jsp?StartValue=" + pstart;
  thisurl = thisurl + "&EndValue=" + pend;
  thisurl = thisurl + "&searchFolio="+foliono;
  location = thisurl;
}

 function confirmprint()
 {
  if (confirm("Do you want to Print the Document?"))
  {
    document.all.dprint.style.display = 'none'
    window.print();
    document.all.dprint.style.display = '';
  }
 }

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  if (count == 0){
    document.forms[0].submit();
    }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   int folio_state=0;
   int COUNT;
   int MAX_RECORDS=20;
   String folio_no=request.getParameter("searchFolio");
   String folioname = "";
   String folioaddress = "";
   String folioaddress2 = "";
   String folioaddress3 = "";
   String folioaddress4 = "";
   String dest = "";
   int totalshares = 0;

 //Used for NEXT/PREVIOUS
    String SStartingValue = String.valueOf(request.getParameter("StartValue"));
    String SEndingValue = String.valueOf(request.getParameter("EndValue"));


    if(SStartingValue == null || SStartingValue.equals("") || SStartingValue.equals("null")) SStartingValue="1";
    if(SEndingValue == null || SEndingValue.equals("") || SEndingValue.equals("null")) SEndingValue="0";

    int StartingValue = Integer.parseInt(SStartingValue);
    int EndingValue = Integer.parseInt(SEndingValue);
    int Chunk = MAX_RECORDS;
    EndingValue = StartingValue + Chunk - 1;
    int pStartingValue = StartingValue - Chunk;
    int pEndingValue = EndingValue - Chunk;
    int nStartingValue = StartingValue + Chunk;
    int nEndingValue = EndingValue + Chunk;

    if (pStartingValue < 1)
    {
      pStartingValue = 1;
    }
    if (pEndingValue < Chunk)
    {
      pEndingValue = Chunk;
    }

    if (String.valueOf(folio_no).equals("null"))
      folio_no = "1";

   String query1;

/*   if(folio_no==null)
   {
     folio_no="1";
   }
*/

   if (folio_no.length() < 8)
   {
     query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (select * from shareholder_view order by SUBSTR(folio_no,3,6)) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;
   }
   else
   {
     query1 = "SELECT * FROM (SELECT div.*, rownum rnum FROM (select * from shareholder_view where (SUBSTR(folio_no,3,6) >= '"+folio_no.substring(2,8)+"') order by SUBSTR(folio_no,3,6)) div WHERE rownum<=" + EndingValue + ") WHERE  rnum>="+StartingValue;
   }

   cm.queryExecute(query1);

   COUNT=0;
   while(cm.toNext())
   {
     COUNT++;
   }
   cm.queryExecute(query1);


%>
  <span class="style7">
  <form method="GET" action="AllPermanentFolios.jsp">
  <SPAN id="dprint">
    <img name="B2" src="<%=request.getContextPath()%>/images/btnPrint.gif"  onclick="confirmprint();" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrintOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnPrint.gif'">
  </SPAN>
  <table width="100%" BORDER=1  cellpadding="0" bordercolor="0044B0" style="border-collapse: collapse" bordercolor="#06689E">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7" height="30"><center>Folio Information</center></td></tr>
  <tr>
  	<td>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0">
			<tr bgcolor="#E8F3FD">
				<td width="10%"></td>
				<td width="10%"><img src="<%=request.getContextPath()%>/images/btnPrev.gif" onclick="goNextPrevious(<%=pStartingValue%>,<%=pEndingValue%>,<%=folio_no%>)"></td>
				<td width="29%"><img src="<%=request.getContextPath()%>/images/btnNext.gif" onclick="goNextPrevious(<%=nStartingValue%>,<%=nEndingValue%>,<%=folio_no%>)"></td>
				<td width="12%" class="style19">Enter Folio No:</td>
				<td width="19%"><input name="searchFolio" type="text" >
                                        <input type="hidden" name="hfolio" value="<%=folio_no%>"/>
                                </td>
				<td width="20%"><img src="<%=request.getContextPath()%>/images/btnSearch.gif" onclick="SubmitThis()"></td>
			</tr>
                </table>
	</td>
  </tr>
  </table>
  <table width="100%" border="1" cellpadding="5" style="border-collapse: collapse" bordercolor="0044B0">
  <tr bgcolor="#0044B0">
    <td width="8%" class="style9" align="center">Folio No</td>
    <td width="24%"><div align="center" class="style9">
      <div align="center">Name</div>
    </div></td>
    <td width="32%"><div align="center" class="style9">
      <div align="center">Address</div>
    </div></td>
    <td width="11%"><div align="center" class="style9">
      <div align="center">Total Shares</div>
    </div></td>
  </tr>
  <div align="left">
     <%
     folio_state=0;
    while(cm.toNext() && COUNT!=0)
     {
       folio_state++;
       folio_no=cm.getColumnS("FOLIO_NO");



       folioname = cm.getColumnS("NAME");
       folioaddress = cm.getColumnS("ADDRESS1");
       folioaddress2 = cm.getColumnS("ADDRESS2");
       folioaddress3 = cm.getColumnS("ADDRESS3");
       folioaddress4 = cm.getColumnS("ADDRESS4");
       totalshares = cm.getColumnI("TOTAL_SHARE");

       dest = request.getContextPath() + "/jsp/Folio/EditFolio.jsp";

       if (!folio_no.equals("null"))
       {
         if (String.valueOf(folioname).equals("null"))
           folioname = "";
         if (String.valueOf(folioaddress).equals("null"))
           folioaddress = "";
         if (String.valueOf(folioaddress2).equals("null"))
           folioaddress2 = "";
         if (String.valueOf(folioaddress3).equals("null"))
           folioaddress3 = "";
         if (String.valueOf(folioaddress4).equals("null"))
           folioaddress4 = "";

         folioaddress = folioaddress + folioaddress2 + folioaddress3 + folioaddress4;
         %>
    </div>
    <tr bgcolor="#E8F3FD">
           <td ><div align="center" class="style10">
             <div align="center" class="style10">
               <span class="style13">
                  <a HREF="<%=dest%>?fno=<%=folio_no%>&dbstatus=actual"><%=folio_no%></a>
               &nbsp;
               <%
               if(folio_state==COUNT)
               {
               %>
                 <input type="hidden" name="svalue" value="<%=StartingValue%>">
                 <input type="hidden" name="evalue" value="<%=EndingValue%>">
              <%}%>
               </span></div>
             </div></td>
           <td class="style13"><div align="left" class="style12">
             <div align="left">&nbsp;&nbsp;<%=folioname%></div>
           </div></td>
           <td class="style13"><div align="justify" class="style12">
             <div align="left"><%=folioaddress%></div>
           </div></td>
           <td class="style13"><div align="center" class="style12">
             <div align="center"><%=totalshares%></div>
           </div></td>
    </tr>
         <div align="left" class="style13">
             <%
         }
     }

   if (isConnect)
   {
     cm.takeDown();
   }
  %>
         </div>
  </form>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD" ><center>

</body>
</html>
