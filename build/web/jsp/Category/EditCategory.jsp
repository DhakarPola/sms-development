<script>
/*
'******************************************************************************************************************************************
'Script Author : Fahmi Sarker
'Creation Date : November 2005
'Page Purpose  : Edits Category Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}
%>

<html>
<head>
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

  String category = "";
  String phonecode = "";
  String code = String.valueOf(request.getParameter("cid"));

  category=cm.replace(category,"'","''");
  phonecode=cm.replace(phonecode,"'","''");
  code=cm.replace(code,"'","''");

   String editCategoryQuery = "SELECT * FROM CATEGORY_VIEW WHERE CODE = '" + code + "'";
   cm.queryExecute(editCategoryQuery);
   while(cm.toNext())
     {
       category = cm.getColumnS("CATEGORY");
       phonecode = cm.getColumnS("POSTAL_CODE");

       if (String.valueOf(category).equals("null"))
         category = "";
       if (String.valueOf(phonecode).equals("null"))
         phonecode = "";
     }
%>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit Category</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function gotocategory()
{
  location = "<%=request.getContextPath()%>/jsp/Category/AllCategories.jsp";
}

function gotodelete()
{
  if (confirm("Do you want to delete the Category Information?"))
  {
    location = "<%=request.getContextPath()%>/jsp/Category/DeleteCategory.jsp?cid=<%=category%>";
  }
}

function formconfirm()
{
  if (confirm("Do you want to change the Category Information?"))
  {
    return true;
  }
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('category','- Category Name (Option must be entered)');
  BlankValidate('code','- Category Name (Option must be entered)');


  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">

<form action="UpdateCategory.jsp" method="get" enctype="multipart/form-data" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Category Information</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="80%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Category</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="category" class="SL2TextField" value="<%=category%>">
        </div></td>
      </tr>
     <tr>
        <th scope="row" width="44%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Code</div></th>
        <td width="1%">:</td>
        <td width="55%"><div align="left">
          <input type="text" name="code" class="SL2TextField" value="<%=code%>">
        </div></td>
      </tr>

  </table>

  <!--HIDDEN FIELDS-->
  <input type="hidden" name="fcid" value="<%=code%>">

  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="43%" scope="row">
            <div align="right">
            <!--  onclick="SubmitThis()"    -->
 		      <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
            </div></th>
        <td width="57%">
	          <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="gotocategory()" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
        </td>
      </tr>
</table>
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
