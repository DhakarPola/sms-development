<script>
/*
'******************************************************************************************************************************************
'Script Author : Mohammad Rahat Uddin
'Creation Date : April 2010
'Page Purpose  : Update Validate Certificate info
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>

<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>


<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<%@ page language="java" import="java.lang.Math.*,org.apache.poi.hssf.usermodel.*,java.util.Vector,java.io.*,java.util.*,java.util.List,java.util.Vector,org.apache.poi.hssf.usermodel.HSSFSheet,org.apache.poi.hssf.usermodel.HSSFCell,org.apache.poi.hssf.usermodel.HSSFRow,org.apache.poi.poifs.filesystem.POIFSFileSystem,java.util.Date,java.text.SimpleDateFormat;" %>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>

</head>

<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }%>

<%
	String DMAT_DT =String.valueOf(request.getParameter("DMAT_DT"));
	String CERTIFICATE_NO =String.valueOf(request.getParameter("CERTIFICATE_NO"));
	String NO_SHARES =String.valueOf(request.getParameter("NO_SHARES"));
	String DIS_NO_FM =String.valueOf(request.getParameter("DIS_NO_FM"));  
	String DIS_NO_TO =String.valueOf(request.getParameter("DIS_NO_TO")); 
	String SP_DATE =String.valueOf(request.getParameter("SP_DATE"));
	String recordno =String.valueOf(request.getParameter("recordno"));
	String updateecertificate="";
	try{
        updateecertificate="update  TMP_FOLIO_CERTI set CERTIFICATE_NO="+CERTIFICATE_NO+", NO_SHARES="+NO_SHARES+", DIS_NO_FM="+DIS_NO_FM+", DIS_NO_TO="+DIS_NO_TO+", SP_DATE=TO_DATE('" + SP_DATE + "','DD/MM/YYYY'),DMAT_DT=TO_DATE('" + DMAT_DT + "','DD/MM/YYYY') where RECORD_NO="+recordno;
		System.out.print(updateecertificate);
        cm.queryExecute(updateecertificate);
        if (isConnect)
        {
          cm.takeDown();
        }
        response.sendRedirect("ValidateCertificate.jsp");
      }
      catch(Exception ei){
        ei.printStackTrace();
      }
  %>
