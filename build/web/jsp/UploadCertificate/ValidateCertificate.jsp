<script>
/*
'******************************************************************************************************************************************
'Script Author : Mohammad Rahat Uddin
'Creation Date : April 2010
'Page Purpose  : Validate Certificate info
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>

<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>All Folios</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<%@ page language="java" import="java.lang.Math.*,org.apache.poi.hssf.usermodel.*,java.util.Vector,java.io.*,java.util.*,java.util.List,java.util.Vector,org.apache.poi.hssf.usermodel.HSSFSheet,org.apache.poi.hssf.usermodel.HSSFCell,org.apache.poi.hssf.usermodel.HSSFRow,org.apache.poi.poifs.filesystem.POIFSFileSystem,java.util.Date,java.text.SimpleDateFormat;" %>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
function openPhotoWindow()
{
  	window.open("UploadPhoto.jsp","","HEIGHT=350,WIDTH=500")
}
function Submitme(callfrom)
{
if(callfrom==1)
  document.forms[0].action="ValidateCertificateSave.jsp";
else if(callfrom==2)
	document.forms[0].action="FinalUpload.jsp";
  document.forms[0].submit();
}
//-->
</script>
<style type="text/css">
<!--
.style19 {color: #0A2769; font-weight: bold; }
.style9 {
	color: #FFFFFF;
	font-weight: bold;
}
.style12 {
	color: #000066;
	font-size: 11px;
}
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
	text-align:center;
}
.style10 {color: #FF0000;}
.style11 {
	color: #06689E;
	font-size: 11px;
}
.style13 {font-size: 11px}
.style14 {color: #000066}
-->
</style>
</head>

<body BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }%>
     <form action="ValidateCertificateSave.jsp" method="post">
	<table width="100%" BORDER=1  cellpadding="5" style="border-collapse: collapse" bordercolor="#0044B0">
     <!--DWLayoutTable-->
     <tr><td background="<%=request.getContextPath()%>/images/tableTopBG.gif" class="style7"><center>Validate New Certificate</center></td></tr>
     </table>
    <table width="100%" border="1" cellpadding="3" style="border-collapse: collapse" bordercolor="#FFFFFF">
     <%
      int recno=0;
	  int haserror=0;

      try {

      String folioName="";
      String companyname="";
      Date dmatdate = null;
      String folioNo="";
      String oldcertNo = "";
      String shareno="";
      String olddistFrom = "";
      String olddistTo = "";
      Date dateValue = null;
      int a=0;
      int b=0;
      int c=0;
      int e=0;
      int f=0;
      int actual_total_share=0;
      int current_total_share=0;
      String dmatdatestr="";
      String dateValuestr="";
      String nCertificate ="";
      String dest = "";
      String HOLDERFOLIO="";
      int blankcount=0;
      cm.queryExecute("select nvl(max(RECORD_NO),0)+1 recno,HOLDER_FOLIO from TMP_FOLIO_CERTI group by HOLDER_FOLIO");
      if(cm.toNext())
      {
        recno= cm.getColumnI("recno");
        HOLDERFOLIO=cm.getColumnS("HOLDER_FOLIO");
        //System.out.print("recno="+recno+" rows="+rows);
      }

      if(recno<=1){ //No Data for validation
      %>
      <tr><td colspan="12"  bgcolor="#0044B0"class="style7" >No Data for validation</td></tr>
      <%
      }
      else{
        cm.queryExecute("select distinct TMP_FOLIO_CERTI.* from TMP_FOLIO_CERTI,DUPLICATE_CERTIFICATE where TMP_FOLIO_CERTI.CERTIFICATE_NO=DUPLICATE_CERTIFICATE.CERTIFICATE_NO and DUPLICATE_CERTIFICATE.recno>1 order by TMP_FOLIO_CERTI.CERTIFICATE_NO");
        //System.out.println("print 1="+"select distinct TMP_FOLIO_CERTI.* from TMP_FOLIO_CERTI,DUPLICATE_CERTIFICATE where TMP_FOLIO_CERTI.CERTIFICATE_NO=DUPLICATE_CERTIFICATE.CERTIFICATE_NO and DUPLICATE_CERTIFICATE.recno>1 order by TMP_FOLIO_CERTI.CERTIFICATE_NO");
        recno=0;
        %>
        <tr bgcolor="#0044B0">
          <td class="style7">Delete</td>
          <td class="style7">Record No.</td>
          <td class="style7">Follo Name</td>
          <td class="style7">Company Name</td>
          <td class="style7">Dmat Date</td>
          <td class="style7">Holder Follo</td>
          <td class="style7">Certificate</td>
          <td class="style7">Share No.</td>
          <td class="style7">Dist. From</td>
          <td class="style7">Dist. To</td>
          <td class="style7">Sp Date</td>
          <td class="style7">Action</td>
        </tr>
          <tr>
                <td bgcolor="#E8F3FD" colspan="12" width="100%"><b>Duplicate Certificate,Distiction Number From and TO</b> </td>
          </tr>
        <%
        //For duplicate certificate,dist_no_fm,dist_no_to
        while(cm.toNext())
        {
          recno++;
		  haserror++;
          //System.out.print(oldcertNo);
          dest = request.getContextPath() + "/jsp/UploadCertificate/EditCertificate.jsp";
          if(cm.getColumnS("CERTIFICATE_NO").equalsIgnoreCase(oldcertNo)){
            %>
            <tr bgcolor="#E8F3FD">
              <td><input type="checkbox" id="selectrecbox<%=recno%>" name="selectrecbox<%=recno%>" value="<%=cm.getColumnI("RECORD_NO")%>"></td>
                <td><%=cm.getColumnI("RECORD_NO")%></td>
                <td><%=cm.getColumnS("FOLIO_NAME")%></td>
                <td><%=cm.getColumnS("COMPANY_NAME")%></td>
                <td><%=cm.getColumnDT("DMAT_DT")%></td>
                <td><%=cm.getColumnS("HOLDER_FOLIO")%></td>
                <td><%=cm.getColumnS("CERTIFICATE_NO")%></td>
                <td><%=cm.getColumnS("NO_SHARES")%></td>
                <td><%=cm.getColumnS("DIS_NO_FM")%></td>
                <td><%=cm.getColumnS("DIS_NO_TO")%></td>
                <td><%=cm.getColumnDT("SP_DATE")%></td>
                <td><a href="<%=dest%>?RECORD_NO=<%=cm.getColumnI("RECORD_NO")%>">Edit</a></td>
            </tr>
            <% //System.out.print("select distinct TMP_FOLIO_CERTI.* from TMP_FOLIO_CERTI,DUPLICATE_CERTIFICATE where TMP_FOLIO_CERTI.CERTIFICATE_NO=DUPLICATE_CERTIFICATE.CERTIFICATE_NO and DUPLICATE_CERTIFICATE.recno>1");
            }

            oldcertNo=cm.getColumnS("CERTIFICATE_NO");
        }
        //For Duplicate dist_no_fm
        if(recno==0){
          cm.queryExecute("select distinct TMP_FOLIO_CERTI.* from TMP_FOLIO_CERTI,DUPLICATE_DIS_NO_FM where TMP_FOLIO_CERTI.dis_no_fm=DUPLICATE_DIS_NO_FM.dis_no_fm and DUPLICATE_DIS_NO_FM.recno>1 order by TMP_FOLIO_CERTI.dis_no_fm");
          //System.out.println("Print 2= "+"select distinct TMP_FOLIO_CERTI.* from TMP_FOLIO_CERTI,DUPLICATE_DIS_NO_FM where TMP_FOLIO_CERTI.dis_no_fm=DUPLICATE_DIS_NO_FM.dis_no_fm and DUPLICATE_DIS_NO_FM.recno>1 order by TMP_FOLIO_CERTI.dis_no_fm");
           %>
          <tr bgcolor="#E8F3FD">
                <td colspan="12" width="100%"><b>Duplicate Distiction Number Form </b></td>
          </tr>
         <%
          while(cm.toNext())
          {
            recno++;
			haserror++;
            dest = request.getContextPath() + "/jsp/UploadCertificate/EditCertificate.jsp";
            if(cm.getColumnS("DIS_NO_FM").equalsIgnoreCase(olddistFrom)){
              %>
              <tr bgcolor="#E8F3FD">
                <td><input type="checkbox" id="selectrecbox<%=recno%>" name="selectrecbox<%=recno%>" value="<%=cm.getColumnI("RECORD_NO")%>"></td>
                 <td><%=cm.getColumnI("RECORD_NO")%></td>
                  <td><%=cm.getColumnS("FOLIO_NAME")%></td>
                  <td><%=cm.getColumnS("COMPANY_NAME")%></td>
                  <td><%=cm.getColumnDT("DMAT_DT")%></td>
                  <td><%=cm.getColumnS("HOLDER_FOLIO")%></td>
                  <td><%=cm.getColumnS("CERTIFICATE_NO")%></td>
                  <td><%=cm.getColumnS("NO_SHARES")%></td>
                  <td><%=cm.getColumnS("DIS_NO_FM")%></td>
                  <td><%=cm.getColumnS("DIS_NO_TO")%></td>
                  <td><%=cm.getColumnDT("SP_DATE")%></td>
                  <td><a href="<%=dest%>?RECORD_NO=<%=cm.getColumnI("RECORD_NO")%>">Edit</a></td>
              </tr>
              <% //System.out.print("select distinct TMP_FOLIO_CERTI.* from TMP_FOLIO_CERTI,DUPLICATE_CERTIFICATE where TMP_FOLIO_CERTI.CERTIFICATE_NO=DUPLICATE_CERTIFICATE.CERTIFICATE_NO and DUPLICATE_CERTIFICATE.recno>1");
              }

              olddistFrom=cm.getColumnS("DIS_NO_FM");
            }
        }

        //For Duplicate dist_no_to
        if(recno==0){
          cm.queryExecute("select distinct TMP_FOLIO_CERTI.* from TMP_FOLIO_CERTI,DUPLICATE_DIS_NO_TO where TMP_FOLIO_CERTI.dis_no_TO=DUPLICATE_DIS_NO_TO.dis_no_TO and DUPLICATE_DIS_NO_TO.recno>1 order by TMP_FOLIO_CERTI.dis_no_TO");
          %>
          <tr bgcolor="#E8F3FD">
                <td colspan="12" width="100%"><b>Duplicate Distiction Number TO</b></td>
          </tr>
         <%
          while(cm.toNext())
          {
            recno++;
			haserror++;
            dest = request.getContextPath() + "/jsp/UploadCertificate/EditCertificate.jsp";
            if(cm.getColumnS("DIS_NO_TO").equalsIgnoreCase(olddistTo)){
              %>
              <tr bgcolor="#E8F3FD">
                <td><input type="checkbox" id="selectrecbox<%=recno%>" name="selectrecbox<%=recno%>" value="<%=cm.getColumnI("RECORD_NO")%>"></td>
                  <td><%=cm.getColumnI("RECORD_NO")%></td>
                  <td><%=cm.getColumnS("FOLIO_NAME")%></td>
                  <td><%=cm.getColumnS("COMPANY_NAME")%></td>
                  <td><%=cm.getColumnDT("DMAT_DT")%></td>
                  <td><%=cm.getColumnS("HOLDER_FOLIO")%></td>
                  <td><%=cm.getColumnS("CERTIFICATE_NO")%></td>
                  <td><%=cm.getColumnS("NO_SHARES")%></td>
                  <td><%=cm.getColumnS("DIS_NO_FM")%></td>
                  <td><%=cm.getColumnS("DIS_NO_TO")%></td>
                  <td><%=cm.getColumnDT("SP_DATE")%></td>
                  <td><a href="<%=dest%>?RECORD_NO=<%=cm.getColumnI("RECORD_NO")%>">Edit</a></td>
              </tr>
              <% //System.out.print("select distinct TMP_FOLIO_CERTI.* from TMP_FOLIO_CERTI,DUPLICATE_CERTIFICATE where TMP_FOLIO_CERTI.CERTIFICATE_NO=DUPLICATE_CERTIFICATE.CERTIFICATE_NO and DUPLICATE_CERTIFICATE.recno>1");
              }

              olddistTo=cm.getColumnS("DIS_NO_TO");
            }
        }
		%>
          <tr bgcolor="#E8F3FD">
                <td colspan="12" width="100%"><b>Total Share</b></td>
          </tr>

          <tr bgcolor="#E8F3FD">
                <td colspan="12" width="100%"><b>Duplicate Distiction Number TO</b></td>
          </tr>
         <%
         //For Total Share
          cm.queryExecute("select TOTAL_SHARE,folio_no from shareholder where folio_no='"+HOLDERFOLIO+"' and active='T'");
          if(cm.toNext())
          {
            actual_total_share=cm.getColumnI("TOTAL_SHARE");
          }
          cm.queryExecute("select sum(NO_SHARES) TOTAL_SHARE from TMP_FOLIO_CERTI");
          if(cm.toNext())
          {
            current_total_share=cm.getColumnI("TOTAL_SHARE");
          }

            %>
              <tr bgcolor="#E8F3FD">
                <td colspan="12" width="100%">
                  <table width="100%">
                    <tr>
                      <td width="10%">&nbsp;</td>
                      <td width="20%">Previous Shares</td>
                      <td width="2%">:</td>
                      <td width="10%" align=left><b><%=actual_total_share%></b></td>
                      <td width="5%">&nbsp;</td>
                      <td width="20%">Current Total Share</td>
                      <td width="2%">:</td>
                      <td align="left"><b><%=current_total_share%></b></td>
                    </tr>
                  </table>
                 </td>
              </tr>
              <%

      }

    } catch(Exception ioe) {
    ioe.printStackTrace();
}
try{
  if (isConnect)
      {
        cm.takeDown();
      }

    }
    catch(Exception ei){
    ei.printStackTrace();
  }
  %>
		<tr bgcolor="#E8F3FD">
               <td colspan="12" width="100%">
			   <table width="100%" BORDER=0  cellpadding="15" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <td width="100%" scope="row" align="center">
            <div align="center">
			 <% if(recno<=0){%>
	      	<img name="B2" src="<%=request.getContextPath()%>/images/btnUpload.gif" onclick="Submitme(2)" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnUploadOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnUpload.gif'">
		   <%} else {%>
		   	<img name="B2" src="<%=request.getContextPath()%>/images/btnSubmit.gif" onclick="Submitme(1)" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnSubmitOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnSubmit.gif'">
		    <%}%>
            </div></td>
      </tr>
   </table>
        </td>
          </tr>
  </table>
  <input type="hidden" name="recno" id="recno" value="<%=recno%>"/>
  </form>
</body>
</html>
