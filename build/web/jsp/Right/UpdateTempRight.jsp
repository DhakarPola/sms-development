<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : February 2006
'Page Purpose  : Updates Temporary Right Share Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.util.Date,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update Temporary Right Share</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String DecDate = String.valueOf(request.getParameter("decdate"));
  String RecDate = String.valueOf(request.getParameter("recdate"));
  String HolderRatio = String.valueOf(request.getParameter("ratio1"));
  String RightRatio = String.valueOf(request.getParameter("ratio2"));
  String ShareValue = String.valueOf(request.getParameter("sharevalue"));
  String PremiumValue = String.valueOf(request.getParameter("premdiscvalue"));
  String PorD = String.valueOf(request.getParameter("premdisc"));
  String DecBy = String.valueOf(request.getParameter("declaredby"));
  String Tax = String.valueOf(request.getParameter("taxpershare"));
  String SProposedUnits = String.valueOf(request.getParameter("proposedunits"));
  String sbankid = String.valueOf(request.getParameter("bankid"));
  String accountnumber = String.valueOf(request.getParameter("accountno"));

  int bankid = Integer.parseInt(sbankid);

  if (String.valueOf(SProposedUnits).equals("null"))
   SProposedUnits = "";
  if (String.valueOf(DecBy).equals("null"))
   DecBy = "";

  int HRatio = Integer.parseInt(HolderRatio);
  int RRatio = Integer.parseInt(RightRatio);
  double iShareValue = Double.parseDouble(ShareValue);
  double iTax = Double.parseDouble(Tax);
  iTax = bcal.roundtovalue(iTax,4);
  double iPremiumValue = 0;
  double iDiscountValue = 0;

  if (PorD.equalsIgnoreCase("premium"))
  {
    iPremiumValue = Double.parseDouble(PremiumValue);
    iPremiumValue = bcal.roundtovalue(iPremiumValue,4);
  }
  else if (PorD.equalsIgnoreCase("discount"))
  {
    iDiscountValue = Double.parseDouble(PremiumValue);
    iDiscountValue = bcal.roundtovalue(iDiscountValue,4);
  }

  iShareValue = bcal.roundtovalue(iShareValue,4);

  int errorcounter = 0;

  String query1 = "SELECT * FROM RIGHTDECLARATION_VIEW WHERE DATE_DEC = TO_DATE('" + DecDate + "','DD/MM/YYYY')";
  int dupprocessed = cm.queryExecuteCount(query1);

  if (dupprocessed > 0)
  {
     errorcounter++;
     %>
       <script language="javascript">
        alert("A Right Share has already been Declared on this Date!");
        history.go(-1);
       </script>
     <%
  }

  String folio_no = "";
  String boid = "";
  int totalshare = 0;
  int indtotalshare = 0;
  int totalrightshares = 0;
  int indrightshares = 0;
  double totalfractionshares = 0;
  double indfractionshares = 0;
  BigDecimal temp1;
  BigDecimal temp2 = BigDecimal.valueOf(HRatio);
  BigDecimal temp3 = BigDecimal.valueOf(RRatio);
  BigDecimal temp4;
  String temp5 = "";
  String temp6 = "";
  String temp7 = "";

  if (errorcounter == 0)
  {
    String dright1 = "call DELETE_RIGHT_T()";
    boolean db1 = cm.procedureExecute(dright1);

    dright1 = "call DELETE_BORIGHT_T()";
    db1 = cm.procedureExecute(dright1);

    String findtotalshares = "SELECT SUM(TOTAL_SHARE) as TSHARE FROM SHAREHOLDER_VIEW WHERE ACTIVE = 'T'";
    cm.queryExecute(findtotalshares);
    cm.toNext();
    int ttotalshare = cm.getColumnI("TSHARE");
    totalshare = ttotalshare;

    findtotalshares = "SELECT SUM(CURRBAL) as TBAL FROM BOHOLDINGONLY_VIEW";
    cm.queryExecute(findtotalshares);
    cm.toNext();
    ttotalshare = cm.getColumnI("TBAL");
    totalshare = totalshare + ttotalshare;

    totalrightshares = (totalshare * RRatio)/ HRatio;

    String query3 = "SELECT * FROM SHAREHOLDER_VIEW WHERE ACTIVE = 'T' ORDER BY FOLIO_NO";
    cm.queryExecute(query3);

   while(cm.toNext())
   {
    folio_no = cm.getColumnS("FOLIO_NO");
    indtotalshare = cm.getColumnI("TOTAL_SHARE");
    temp1 = BigDecimal.valueOf(indtotalshare);
    temp4 = temp1.multiply(temp3);
    temp4 = temp4.divide(temp2,2,BigDecimal.ROUND_HALF_UP);

    temp5 = String.valueOf(temp4);
    StringTokenizer RightSTK = new StringTokenizer(temp5,".");
    temp6 = RightSTK.nextToken();
    temp7 = RightSTK.nextToken();
    temp7 = "0." + temp7;

    indrightshares = Integer.parseInt(temp6);
    indfractionshares = Double.parseDouble(temp7);

    totalfractionshares = totalfractionshares + indfractionshares;

    String sdividend = "call ADD_RIGHT_T('" + folio_no + "', '" + DecDate + "', " + indtotalshare + ", " + indrightshares + ", " + indfractionshares + ")";
    boolean bB1 = cm1.procedureExecute(sdividend);
   }

//    query3 = "SELECT * FROM BOHOLDINGONLY_VIEW ORDER BY BOID";
    query3 = "SELECT * FROM BOHOLDING_VIEW ORDER BY BOID";
    cm.queryExecute(query3);

   while(cm.toNext())
   {
     boid = cm.getColumnS("BOID");
     indtotalshare = cm.getColumnI("CURRBAL");
     temp1 = BigDecimal.valueOf(indtotalshare);
     temp4 = temp1.multiply(temp3);
     temp4 = temp4.divide(temp2,2,BigDecimal.ROUND_HALF_UP);

     temp5 = String.valueOf(temp4);
     StringTokenizer RightSTK1 = new StringTokenizer(temp5,".");
     temp6 = RightSTK1.nextToken();
     temp7 = RightSTK1.nextToken();
     temp7 = "0." + temp7;

     indrightshares = Integer.parseInt(temp6);
     indfractionshares = Double.parseDouble(temp7);

     totalfractionshares = totalfractionshares + indfractionshares;

     String sdividend1 = "call ADD_BORIGHT_T('" + boid + "', '" + DecDate + "', " + indtotalshare + ", " + indrightshares + ", " + indfractionshares + ")";
     boolean bB2 = cm1.procedureExecute(sdividend1);
   }

   totalfractionshares = bcal.roundtovalue(totalfractionshares,0);
   int itotalfractionshares = (int)totalfractionshares;

   String ndividend = "call UPDATE_RIGHTDECLARATION_T('" + DecDate + "','" + RecDate + "'," + HRatio + "," + RRatio + "," + totalshare + "," + totalrightshares + "," + totalfractionshares + "," + iShareValue + "," + iPremiumValue + "," + iDiscountValue + ",'" + DecBy + "'," + iTax + ",'" + SProposedUnits + "'," + bankid + ",'" + accountnumber + "')";
   boolean b = cm.procedureExecute(ndividend);
  }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Edited Declared Right Share Information')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
     cm1.takeDown();
   }
%>
<script language="javascript">
  alert("Right Share Information has been Updated!");
  location = "<%=request.getContextPath()%>/jsp/Right/AcceptRight.jsp";
</script>
</body>
</html>
