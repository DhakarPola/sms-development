<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : March 2006
'Page Purpose  : Updates Fractional and Unsold Right Share Sales Information.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.util.Date,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="cm1"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Update Fractional and Unsold Right Share Price</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   boolean isConnect1 = cm1.connect();
   if((isConnect==false) || (isConnect1==false)){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }
  String SPrice = String.valueOf(request.getParameter("priceamount"));
  String DecDate = String.valueOf(request.getParameter("decdate"));

  String nright = "call UPDATE_RIGHT_FRACTION_INFO('" + DecDate + "', " + SPrice + ")";
  boolean b = cm.procedureExecute(nright);

  String forbno = "";
  double fractionshare = 0;
  double fractionamount = 0;
  int rightshares = 0;
  int boughtshares = 0;
  double sellingprice = Double.parseDouble(SPrice);
  double shareprice = 0;
  double premium = 0;
  double tax = 0;
  double discount = 0;
  String warrantno = "";

  String query1aa = "SELECT * FROM RIGHTDECLARATION_VIEW WHERE DATE_DEC = TO_DATE('" + DecDate + "','DD/MM/YYYY')";
  cm.queryExecute(query1aa);

   while(cm.toNext())
   {
     shareprice = cm.getColumnF("PRICE");
     premium = cm.getColumnF("PREMIUM");
     tax = cm.getColumnF("TAX");
     discount = cm.getColumnF("DISCOUNT");
   }

  String query1 = "SELECT * FROM RIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DecDate + "','DD/MM/YYYY') AND RECONCILIATED = 'T'";
  cm.queryExecute(query1);

   while(cm.toNext())
   {
     forbno = cm.getColumnS("FOLIO_NO");
     fractionshare = cm.getColumnF("FRACTION_SHARE");
     rightshares = cm.getColumnI("RIGHT_SHARE");
     boughtshares = cm.getColumnI("BOUGHT");

     if (rightshares == boughtshares)
     {
//       warrantno = cm1.LastWarrant();
       fractionamount = fractionshare * (sellingprice - (shareprice + premium + tax - discount));
       fractionamount = bcal.roundtovalue(fractionamount,2);

       if (fractionamount > 0)
       {
        warrantno = cm1.LastWarrant();
        nright = "call UPDATE_RIGHT_FRACTION_AMOUNT('" + DecDate + "','" + forbno + "'," + fractionamount + ",'" + warrantno + "')";
       }
       else
       {
        nright = "call ADD_FRACTION_RIGHT(" + fractionamount + ",'" + forbno + "','" + DecDate + "')";
       }

       b = cm1.procedureExecute(nright);
     }
   }

  query1 = "SELECT * FROM BORIGHT_VIEW  WHERE DATE_DEC = TO_DATE('" + DecDate + "','DD/MM/YYYY') AND RECONCILIATED = 'T'";
  cm.queryExecute(query1);

   while(cm.toNext())
   {
     forbno = cm.getColumnS("BO_ID");
     fractionshare = cm.getColumnF("FRACTION_SHARE");
     rightshares = cm.getColumnI("RIGHT_SHARE");
     boughtshares = cm.getColumnI("BOUGHT");

     if (rightshares == boughtshares)
     {
//      warrantno = cm1.LastWarrant();
      fractionamount = fractionshare * (sellingprice - (shareprice + premium + tax - discount));
      fractionamount = bcal.roundtovalue(fractionamount,2);

      if (fractionamount > 0)
      {
       warrantno = cm1.LastWarrant();
       nright = "call UPDATE_BORIGHT_FRACTION_AMOUNT('" + DecDate + "','" + forbno + "'," + fractionamount + ",'" + warrantno + "')";
      }
      else
      {
       nright = "call ADD_FRACTION_BORIGHT(" + fractionamount + ",'" + forbno + "','" + DecDate + "')";
      }

      b = cm1.procedureExecute(nright);
     }
   }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Added Information Regarding Selling Price of Fractional and Unsold Right Shares')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
     cm1.takeDown();
   }
%>
<script language="javascript">
  alert("Right Sharing Sales Information Updated!");
  location = "<%=request.getContextPath()%>/jsp/Right/SellingPriceInfo.jsp";
</script>
</body>
</html>
