<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : February 2006
'Page Purpose  : Edits Temporary Right Share.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="bcal" class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/datetimepicker.js"></SCRIPT>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Edit Right Share</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script language="javascript">
<!--
function canwesplit()
{
  if (document.forms[0].cansplit.checked)
  {
    document.all.punits.style.display = ''
  }
  else
  {
    document.all.punits.style.display = 'none'
  }
}

function formconfirm()
{
  if (confirm("Do you want to Edit the Right Share Information?"))
  {
    return true;
  }
}

function setbankvalues(value1)
{
  var val1 = value1;
  var val2 = value1.indexOf("%");
  var val3 = val1.substring(0,val2);
  var val4 = value1.indexOf("$");
  var val5 = val1.substring(val2 + 1,val4);
  var val7 = value1.indexOf("~");
  var val8 = val1.substring(val4 + 1,val7);
  var val6 = val1.substring(val7 + 1,val1.length);

  document.all["bchname"].value = val5;
  document.all["bnkaddress"].value = val8;
  document.all["bankid"].value = val6;
}

//Execute while click on Submit
function SubmitThis() {
  count = 0;
  BlankValidate('decdate','- Date of Declaration (Option must be entered)');
  SelectValidate('declaredby','- Declared By (Option must be entered)');
  BlankValidate('recdate','- Record Date (Option must be entered)');
  BlankValidate('ratio1','- Share Holder Ratio (Option must be entered)');
  BlankValidate('ratio2','- Right Share Ratio (Option must be entered)');
  BlankValidate('sharevalue','- Share Value (Option must be entered)');
  BlankValidate('premdiscvalue','- Premium/Discount (Option must be entered)');
  BlankValidate('taxpershare','- Tax Per Share (Option must be entered)');
  BlankValidate('bnkname','- Bank Name (Option must be entered)');
  BlankValidate('bchname','- Branch Name (Option must be entered)');
  BlankValidate('bnkaddress','- Bank Address (Option must be entered)');
  BlankValidate('accountno','- Account No. (Option must be entered)');

  if (document.forms[0].cansplit.checked)
  {
    BlankValidate('proposedunits','- Proposed Unit (Option must be entered)');
  }

  if (count == 0)
  {
    if (formconfirm())
    {
      document.forms[0].submit();
    }
  }
  else{
   ShowAllAlertMsg();
   return false;
   }
}
//-->
</script>
<style type="text/css">
<!--
.style7 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 13px;
}
.style8 {color: #0A2769}
.style9 {color: #0A2769; font-weight: bold; }
-->
</style>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

   String DecDate = request.getParameter("declarationdate");
   String RecDate = "";
   String decby = "";
   String Sproposedunit = "";
   int hratio = 0;
   int rratio = 0;
   double price = 0;
   double premium = 0;
   double discount = 0;
   double tax = 0;
   int totalshares = 0;
   int totalrightshares = 0;
   int totalfractionshares = 0;
   int bankid = 0;
   String accountnumber = "";

   String query21 = "SELECT * FROM RIGHTDECLARATION_T_VIEW WHERE DATE_DEC = TO_DATE('" + DecDate + "','DD/MM/YYYY')";
   cm.queryExecute(query21);

   while(cm.toNext())
     {
       decby = cm.getColumnS("DEC_BY");
       RecDate = cm.getColumnDT("REC_DATE");
       hratio = cm.getColumnI("HOLDER_RATIO");
       rratio = cm.getColumnI("RIGHT_RATIO");
       price = cm.getColumnF("PRICE");
       premium = cm.getColumnF("PREMIUM");
       discount = cm.getColumnF("DISCOUNT");
       tax = cm.getColumnF("TAX");
       totalshares = cm.getColumnI("TOTAL_SHARES");
       totalrightshares = cm.getColumnI("TOTAL_RIGHT_SHARES");
       totalfractionshares = cm.getColumnI("TOTAL_FRACTION_SHARES");
       Sproposedunit = cm.getColumnS("PROPOSED_UNITS");
       bankid = cm.getColumnI("BANK_ID");
       accountnumber = cm.getColumnS("ACCOUNT_NO");

       if (String.valueOf(Sproposedunit).equals("null"))
         Sproposedunit = "";
       if (String.valueOf(decby).equals("null"))
         decby = "";

       price = bcal.roundtovalue(price,4);
       premium = bcal.roundtovalue(premium,4);
       discount = bcal.roundtovalue(discount,4);
       tax = bcal.roundtovalue(tax,4);
     }
%>

<form action="UpdateTempRight.jsp" method="post" name="FileForm">

  <span class="style7">
  <table width="100%" BORDER=1  cellpadding="5"  style="border-collapse: collapse" bordercolor="#0044B0">
  <!--DWLayoutTable-->
  <tr><td bgcolor="#0044B0" class="style7">Right Share Declaration</td></tr>
  <tr bgcolor="#E8F3FD" >
    <td height="100%" bgcolor="#E8F3FD"  ><center>
      <br>
      <div align="left">
  <table width="100%"  border="0" cellpadding="5">
      <tr>
        <th scope="row" width="32%"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Date of Declaration</div></th>
        <td width="1%">:</td>
        <td width="67%"><div align="left">
         <input name="decdate" type=text id="decdate" value="<%=DecDate%>" maxlength="10" class="SL2TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('decdate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Declared By</div></th>
        <td>:</td>
        <td><div align="left">
         <select name="declaredby" class="SL2TextFieldListBox"">
            <option>--- Please Select ---</option>
            <%
              if (decby.equalsIgnoreCase("Board"))
              {
               %>
                 <option selected="selected">Board</option>
                 <option>AGM</option>
                 <option>EGM</option>
               <%
              }
              else if (decby.equalsIgnoreCase("AGM"))
              {
               %>
                 <option>Board</option>
                 <option selected="selected">AGM</option>
                 <option>EGM</option>
               <%
              }
              else if (decby.equalsIgnoreCase("EGM"))
              {
               %>
                 <option>Board</option>
                 <option>AGM</option>
                 <option selected="selected">EGM</option>
               <%
              }
            %>
         </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Record Date</div></th>
        <td>:</td>
        <td><div align="left">
         <input name="recdate" type=text id="recdate" value="<%=RecDate%>" maxlength="10" class="SL68TextField" onFocus="this.blur();">
         &nbsp
         <a href="javascript:NewCal('recdate','ddmmyyyy',false,12)">
           <img src="<%=request.getContextPath()%>/images/cal.gif" width="16" height="16" border="0" alt="Pick a date">
         </a>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Share Holder : Right Share Ratio</div></th>
        <td>:</td>
        <td><div align="left" class="style8">
          <input type="text" name="ratio1" class="SL2TextField" onkeypress="keypressOnNumberFld()" value="<%=hratio%>"><b>&nbsp; : &nbsp;</b>
          <input type="text" name="ratio2" class="SL2TextField" onkeypress="keypressOnNumberFld()" value="<%=rratio%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Share Value</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="sharevalue" class="SL68TextField" value="<%=price%>" onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
      <%
         if (discount > 0)
         {
           %>
           <tr>
             <th scope="row" valign="top"><div align="left" class="style8">
              &nbsp;&nbsp;<input name="premdisc" type="radio" value="premium">&nbsp;&nbsp;Premium<br>
              &nbsp;&nbsp;<input name="premdisc" type="radio" value="discount" checked="checked">&nbsp;&nbsp;Discount
             </div></th>
            <td valign="top">:</td>
            <td valign="top"><div align="left" class="style9">
             <input type="text" name="premdiscvalue" class="SL68TextField" value="<%=discount%>" onkeypress="keypressOnDoubleFld()">
            </div></td>
           </tr>
           <%
         }
         else
         {
           %>
           <tr>
             <th scope="row" valign="top"><div align="left" class="style8">
              &nbsp;&nbsp;<input name="premdisc" type="radio" value="premium" checked="checked">&nbsp;&nbsp;Premium<br>
              &nbsp;&nbsp;<input name="premdisc" type="radio" value="discount">&nbsp;&nbsp;Discount
             </div></th>
            <td valign="top">:</td>
            <td valign="top"><div align="left" class="style9">
             <input type="text" name="premdiscvalue" class="SL68TextField" value="<%=premium%>" onkeypress="keypressOnDoubleFld()">
            </div></td>
           </tr>
           <%
         }
      %>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Tax Per Share (Tk)</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="taxpershare" class="SL68TextField" value="<%=tax%>" onkeypress="keypressOnDoubleFld()">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Name</div></th>
        <td>:</td>
        <td><div align="left">
         <select name="bnkname" class="SL2TextFieldListBox" onchange="setbankvalues(document.all.bnkname.value)">
            <option>--- Please Select ---</option>
            <%
              String bname = "";
              String bbranch = "";
              String baddress = "";
              int bid = 0;

              String dbranch = "";
              String daddress = "";

              String query1 = "SELECT * FROM BANK_VIEW ORDER BY UPPER(BANK_NAME)";
              cm.queryExecute(query1);

              while(cm.toNext())
              {
                 bname = cm.getColumnS("BANK_NAME");
                 bbranch = cm.getColumnS("BANK_BRANCH");
                 baddress = cm.getColumnS("BANK_ADDRESS");
                 bid = cm.getColumnI("BANK_ID");
                 String combo = bname + "%" + bbranch + "$" + baddress + "~" + String.valueOf(bid);
                 if (bankid == bid)
                 {
                   dbranch = bbranch;
                   daddress = baddress;
                   %>
                     <option value="<%=combo%>" selected="selected"><%=bname%></option>
                   <%
                 }
                 else
                 {
                   %>
                     <option value="<%=combo%>"><%=bname%></option>
                   <%
                 }
              }
            %>
         </select>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Branch Name</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="bchname" value="<%=dbranch%>" class="SL68TextField" onfocus="this.blur()">
        </div></td>
      </tr>
      <tr>
        <th scope="row" valign="top"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Bank Address</div></th>
        <td valign="top">:</td>
        <td><div align="left">
          <textarea name="bnkaddress" cols="20" rows="3" wrap="VIRTUAL" id="bnkaddress" class="ML10TextField" onfocus="this.blur()"><%=daddress%></textarea>
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;&nbsp;&nbsp;Account No.</div></th>
        <td>:</td>
        <td><div align="left">
          <input type="text" name="accountno" class="SL68TextField" value="<%=accountnumber%>">
        </div></td>
      </tr>
      <tr>
        <th scope="row"><div align="left" class="style8">&nbsp;&nbsp;
          <%
            if (Sproposedunit.equalsIgnoreCase(""))
            {
            %>
            <input type="checkbox" name="cansplit" value="checkbox" onclick="canwesplit()">
            <%
            }
            else
            {
            %>
            <input type="checkbox" name="cansplit" value="checkbox" onclick="canwesplit()" checked="checked">
            <%
            }
          %>
          Split Cumulated Fraction Shares&nbsp;
          </div></th>
        <td valign="top">:</td>
        <td><div align="left" class="style8">
          <%
            if (Sproposedunit.equalsIgnoreCase(""))
            {
            %>
            <SPAN id="punits" style="display:none">
            <%
            }
            else
            {
            %>
            <SPAN id="punits">
            <%
            }
          %>
            <input type="text" name="proposedunits" class="SL2TextField" onkeypress="keypressOnNumberFld()" value="<%=Sproposedunit%>">
            &nbsp;<b>(Proposed Unit)</b>
          </span>
            </div></td>
      </tr>
  </table>
  <br>
      </div>
      <table width="50%" BORDER=0  cellpadding="10" style="border-collapse: collapse" bordercolor="#EAC06B">
      <tr>
        <th width="34%" scope="row">
            <div align="right">
                   <img name="B5" src="<%=request.getContextPath()%>/images/btnClose.gif"  onclick="history.go(-1)" onMouseOver="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnCloseOn.gif'" onMouseOut="document.forms[0].B5.src = '<%=request.getContextPath()%>/images/btnClose.gif'">
            </div></th>
        <td width="33%" align="center">
                  <img name="B1" src="<%=request.getContextPath()%>/images/btnUpdate.gif"  onclick="SubmitThis()" onMouseOver="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdateOn.gif'" onMouseOut="document.forms[0].B1.src = '<%=request.getContextPath()%>/images/btnUpdate.gif'">
        </td>
        <td width="33%">
	          <img name="B2" src="<%=request.getContextPath()%>/images/btnRefresh.gif" onclick="reset()" onMouseOver="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = '<%=request.getContextPath()%>/images/btnRefresh.gif'">
        </td>
      </tr>
</table>
   <input type="hidden" name="bankid" value="0">
</form>
<%
   if (isConnect)
   {
     cm.takeDown();
   }
%>
</body>
</html>
