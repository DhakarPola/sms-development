<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : February 2006
'Page Purpose  : Checks Right Share Letter to CDBL.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.util.Date,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<jsp:useBean id="cm"  class="batbsms.conBean"/>
<jsp:useBean id="bcal"  class="batbsms.batCalculations"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}%>

<html>
<head>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
body,td,th {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Check Right Share Letter to CDBL</title>

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
</head>

<body TEXT="000000" BGCOLOR="FFFFFF">
<%
   boolean isConnect = cm.connect();
   if(isConnect==false){
     %>
     <jsp:forward page="ErrorMsg.jsp" >
       <jsp:param name="ErrorTitle" value="Connection Failure" />
       <jsp:param name="ErrorHeading" value="Connection Problem" />
       <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
     </jsp:forward>
     <%
     }

  String DateDec = String.valueOf(request.getParameter("dateselect"));

  String query3 = "SELECT * FROM LETTER_VIEW WHERE lower(LETTER_NAME)=lower('Right Share Letter to CDBL')";
  int letterexists = cm.queryExecuteCount(query3);

  if (letterexists < 1)
  {
   %>
    <script language="javascript">
     alert("Right Share Letter to CDBL Template does not Exist!\nFirst make a Template named Right Share Letter to CDBL.\nThen, try Printing the Letter Again.");
     location = "<%=request.getContextPath()%>/jsp/Others/OthersLetterEdit.jsp";
    </script>
   <%
  }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Printed Right Share Letter to CDBL')";
  boolean ub = cm.procedureExecute(ulog);

   if (isConnect)
   {
     cm.takeDown();
   }
%>
<script language="javascript">
  location = "<%=request.getContextPath()%>/RightCDBLLetter.jsp?dateselect=<%=DateDec%>";
</script>
</body>
</html>
