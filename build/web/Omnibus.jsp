<script>
/*
'******************************************************************************************************************************************
'Script Author : Hasanul Azaz Aman
'Creation Date : 2013
'Page Purpose  :  Report.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null")) { %>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<% } 
 boolean isConnect = cm.connect();
 if(isConnect==false) { %>
    <jsp:forward page="ErrorMsg.jsp" >
    <jsp:param name="ErrorTitle" value="Connection Failure" />
    <jsp:param name="ErrorHeading" value="Connection Problem" />
    <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
    </jsp:forward>
  <%
 }
%>

<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<%@page
import="java.util.Date,java.text.*,com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
 Account Report
</title>
</head>
<body bgcolor="#ffffff">
<%
    //Value of parameter get from page SetBankReport.jsp

  String omnibusDate 	= String.valueOf(request.getParameter("dateselect"));
  String bo_folio_no 	= String.valueOf(request.getParameter("omnibusName"));
  String sigselect  	= String.valueOf(request.getParameter("sigselect"));


  System.out.print("the Id-----"+bo_folio_no+"-----omnibusDate-----"+omnibusDate+"------sigselect----"+sigselect+"----------");

    // bo_folio_no=omnibusName;
    // bo_folio_no="'"+bo_folio_no+"'";
     // omnibusDate="'"+omnibusDate+"'";
  // System.out.print("the Date -----"+omnibusDate);

  String reporturl = "";
  String markprinted = "";
  boolean mp;

    //add report
    // reporturl="/CR_Reports/Div_Collection/DividentReportIndividual_2.rpt";
     reporturl="/CR_Reports/Div_Collection/Omnibus.rpt";



  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Printed Individual Divident Report')";
  boolean ub = cm.procedureExecute(ulog);

  if (!reporturl.equals("")) {
    //session.setAttribute("reportSource", null);
    session.setAttribute("reportSource", null);
    session.setAttribute("refreshed", null);
  }

Object reportSource = session.getAttribute("reportSource");
if (reportSource == null)
{
try{
  IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
  reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
  session.setAttribute("reportSource", reportSource);
}catch (Exception e) {}
}
  CrystalReportViewer viewer= new CrystalReportViewer();
  try{
    viewer.setReportSource(reportSource);
  }catch (Exception e) {}
  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  //viewer.setPrintMode(CrPrintMode.PDF);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setReuseParameterValuesOnRefresh(true);
// Passing parameters

ParameterField param1 = new ParameterField();
param1.setReportName("");
param1.setName("bo_folio");

ParameterField param2 = new ParameterField();
param2.setReportName("");
param2.setName("mdate");

ParameterField param3 = new ParameterField();
param3.setReportName("");
param3.setName("signid");


//ParameterField param2 = new ParameterField();
//param2.setReportName("");
//param2.setName("myaccnotype");

Values vals1 = new Values();
ParameterFieldDiscreteValue val1 = new ParameterFieldDiscreteValue();
val1.setValue(bo_folio_no);
vals1.add(val1);

Values vals2 = new Values();
ParameterFieldDiscreteValue val2 = new ParameterFieldDiscreteValue();
val2.setValue(omnibusDate);
vals2.add(val2);

Values vals3 = new Values();
ParameterFieldDiscreteValue val3 = new ParameterFieldDiscreteValue();
val3.setValue(sigselect);
vals3.add(val3);

param1.setCurrentValues(vals1);
param2.setCurrentValues(vals2);
param3.setCurrentValues(vals3);

Fields fields = new Fields();
fields.add(param1);
fields.add(param2);
fields.add(param3);

viewer.setParameterFields(fields);
//viewer.refresh();
//System.out.println("The Viewer is here--------------------------"+viewer);
if (session.getAttribute("refreshed") == null)
{
viewer.refresh();
session.setAttribute("refreshed", "true");
}

//viewer.refresh();

try
{
viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);
}
catch(Exception e)
{
  System.out.println(e.getMessage());
}

  if (isConnect)
  {
   cm.takeDown();
  }

%>
</body>
</html>
