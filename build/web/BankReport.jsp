<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : December 2006
'Page Purpose  : Passes Parameters for Bank Report.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

 boolean isConnect = cm.connect();
 if(isConnect==false)
 {
   %>
    <jsp:forward page="ErrorMsg.jsp" >
    <jsp:param name="ErrorTitle" value="Connection Failure" />
    <jsp:param name="ErrorHeading" value="Connection Problem" />
    <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
    </jsp:forward>
  <%
 }

%>

<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<%@page
import="java.util.Date,java.text.*,com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
 Bank Report
</title>
</head>
<body bgcolor="#ffffff">
<h1>
</h1>
<%
    //Value of parameter get from page SetBankReport.jsp

  String account = String.valueOf(request.getParameter("accountno"));
  String accounttype=String.valueOf(request.getParameter("accounttype"));
  String startDate=String.valueOf(request.getParameter("startDate"));
  String endDate=String.valueOf(request.getParameter("endDate"));
  //System.out.println("startDate= "+startDate);
  //System.out.println("endDate= "+endDate);

   if (String.valueOf(account).equals("null"))
   account = "";
   if( String.valueOf(accounttype).equals("null"))
   accounttype="";

  String reporturl = "";

  if(accounttype.equals("Unrefunded"))
  {
    //add report
      reporturl="/CR_Reports/Bank_Recon/Unrefunded.rpt";
  }
  else if(accounttype.equals("All"))
  {
    //add report
    reporturl="/CR_Reports/Bank_Recon/All.rpt";
  }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Printed AGM Attemdance Report')";
  boolean ub = cm.procedureExecute(ulog);

  if (!reporturl.equals("")) {
    //session.setAttribute("reportSource", null);
    session.setAttribute("reportSource", null);
    session.setAttribute("refreshed", null);
  }

Object reportSource = session.getAttribute("reportSource");
if (reportSource == null)
{
try{
  IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
  reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
  session.setAttribute("reportSource", reportSource);
}catch (Exception e) {}
}
  CrystalReportViewer viewer= new CrystalReportViewer();
  try{
    viewer.setReportSource(reportSource);
  }catch (Exception e) {}
  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  //viewer.setPrintMode(CrPrintMode.PDF);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setReuseParameterValuesOnRefresh(true);
// Passing parameters

ParameterField param1 = new ParameterField();
param1.setReportName("");
param1.setName("account");

Values vals1 = new Values();
ParameterFieldDiscreteValue val1 = new ParameterFieldDiscreteValue();
val1.setValue(account);
vals1.add(val1);

ParameterField param2 = new ParameterField();
param2.setReportName("");
param2.setName("startDate");

Values vals2 = new Values();
ParameterFieldDiscreteValue val2 = new ParameterFieldDiscreteValue();
val2.setValue(startDate);
vals2.add(val2);
try {
 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
 Date d = df.parse(startDate);
 val2.setValue(d);
 vals2.add(val2);
} catch(Exception e) {
e.printStackTrace();
}

ParameterField param3 = new ParameterField();
param3.setReportName("");
param3.setName("endDate");

Values vals3 = new Values();
ParameterFieldDiscreteValue val3 = new ParameterFieldDiscreteValue();
val3.setValue(startDate);
vals3.add(val3);
try {
 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
 Date d = df.parse(endDate);
 val3.setValue(d);
 vals3.add(val3);
} catch(Exception e) {
e.printStackTrace();
}

param1.setCurrentValues(vals1);
param2.setCurrentValues(vals2);
param3.setCurrentValues(vals3);

Fields fields = new Fields();
fields.add(param1);
fields.add(param2);
fields.add(param3);


viewer.setParameterFields(fields);
viewer.refresh();

try
{
viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);
}
catch(Exception e)
{
  System.out.println(e.getMessage());
}

  if (isConnect)
  {
  try
  {
    cm.takeDown();
  }catch(Exception e){}
  }

%>
</body>
</html>

