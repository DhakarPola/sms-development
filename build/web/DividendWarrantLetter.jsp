<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : March 2006
'Page Purpose  : Passes Parameters for Dividend Warrant and Letter Printing.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

 boolean isConnect = cm.connect();
 if(isConnect==false)
 {
   %>
    <jsp:forward page="ErrorMsg.jsp" >
    <jsp:param name="ErrorTitle" value="Connection Failure" />
    <jsp:param name="ErrorHeading" value="Connection Problem" />
    <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
    </jsp:forward>
  <%
 }

%>

<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<%@page
import="java.util.Date,java.text.*,com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
Dividend Warrant and Letter
</title>
</head>
<body bgcolor="#ffffff">
<h1>
</h1>
<%
  String DateDec = String.valueOf(request.getParameter("dateselect"));
  String DivType = String.valueOf(request.getParameter("dividendtype"));
  String DivOrder = String.valueOf(request.getParameter("orderby"));
  String PRecords = String.valueOf(request.getParameter("printrecords"));
  String ForBNo = String.valueOf(request.getParameter("sfoliono"));
  String SWarrantNo = String.valueOf(request.getParameter("swarrantno"));
  String FStart = String.valueOf(request.getParameter("foliostart"));
  String FEnd = String.valueOf(request.getParameter("folioend"));
  String WStart = String.valueOf(request.getParameter("warrantstart"));
  String WEnd = String.valueOf(request.getParameter("warrantend"));
  String PrintDup = String.valueOf(request.getParameter("printduplicate"));
//  String IncludeGrpMem = String.valueOf(request.getParameter("includegroupmembers"));
  String SignatureInfo = String.valueOf(request.getParameter("sigselect"));

  if (String.valueOf(FStart).equals("null"))
   FStart = "";
  if (String.valueOf(FEnd).equals("null"))
   FEnd = "";
  if (String.valueOf(WStart).equals("null"))
   WStart = "";
  if (String.valueOf(WEnd).equals("null"))
   WEnd = "";
  if (String.valueOf(PrintDup).equals("null"))
   PrintDup = "";
//  if (String.valueOf(IncludeGrpMem).equals("null"))
//   IncludeGrpMem = "";
  if (String.valueOf(ForBNo).equals("null"))
   ForBNo = "";
  if (String.valueOf(SWarrantNo).equals("null"))
   SWarrantNo = "";

  String reporturl = "";
  String markprinted = "";
  boolean mp;

  if (DivType.equalsIgnoreCase("SMStype"))
  {
   if (PRecords.equalsIgnoreCase("singlebyfolio"))
   {
     String query1 = "SELECT * FROM DIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DateDec + "','DD/MM/YYYY') AND FOLIO_NO = '" + ForBNo + "'";
     int fcc1 = cm.queryExecuteCount(query1);

     if (fcc1 < 1)
     {
     %>
      <script language="javascript">
       alert("Folio No. does not exist!");
       location = "<%=request.getContextPath()%>/jsp/Dividend/SetLetterCheque.jsp";
      </script>
     <%
     }
     else if (fcc1 > 0)
     {
       if (DivOrder.equalsIgnoreCase("byname"))
       {
         reporturl = "/CR_Reports/DividendWarrants/SMS/ByName/Folio_Div_Warrant_&_LetterOrderByName_ParamFolio.rpt";
       }
       else if (DivOrder.equalsIgnoreCase("byfolio"))
       {
         reporturl = "/CR_Reports/DividendWarrants/SMS/ByFolio/Folio_Div_Warrant_&_LetterOrderByFolio_ParamFolio.rpt";
       }
       else if (DivOrder.equalsIgnoreCase("bydividend"))
       {
        reporturl = "/CR_Reports/DividendWarrants/SMS/ByDividend/Folio_Div_Warrant_&_LetterOrderByDividend_ParamFolio.rpt";
       }
     }
   }
   else if (PRecords.equalsIgnoreCase("singlebywarrant"))
   {
     String query2 = "SELECT * FROM DIVIDEND_VIEW WHERE WARRANT_NO = '" + SWarrantNo + "'";
     int fcc2 = cm.queryExecuteCount(query2);

     if (fcc2 < 1)
     {
     %>
      <script language="javascript">
       alert("Warrant does not exist!");
       location = "<%=request.getContextPath()%>/jsp/Dividend/SetLetterCheque.jsp";
      </script>
     <%
     }
     else if (fcc2 > 0)
     {
       if (DivOrder.equalsIgnoreCase("byname"))
       {
         reporturl = "/CR_Reports/DividendWarrants/SMS/ByName/Folio_Div_Warrant_&_LetterOrderByName_ParamWarrant.rpt";
       }
       else if (DivOrder.equalsIgnoreCase("byfolio"))
       {
         reporturl = "/CR_Reports/DividendWarrants/SMS/ByFolio/Folio_Div_Warrant_&_LetterOrderByFolio_ParamWarrant.rpt";
       }
       else if (DivOrder.equalsIgnoreCase("bydividend"))
       {
        reporturl = "/CR_Reports/DividendWarrants/SMS/ByDividend/Folio_Div_Warrant_&_LetterOrderByDividend_ParamWarrant.rpt";
       }
     }
   }
   else if (PRecords.equalsIgnoreCase("notprinted"))
   {
     if (DivOrder.equalsIgnoreCase("byname"))
     {
         reporturl = "/CR_Reports/DividendWarrants/SMS/ByName/Folio_Div_Warrant_&_LetterOrderByName_All.rpt";
     }
     else if (DivOrder.equalsIgnoreCase("byfolio"))
     {
         reporturl = "/CR_Reports/DividendWarrants/SMS/ByFolio/Folio_Div_Warrant_&_LetterOrderByFolio_All.rpt";
     }
     else if (DivOrder.equalsIgnoreCase("bydividend"))
     {
        reporturl = "/CR_Reports/DividendWarrants/SMS/ByDividend/Folio_Div_Warrant_&_LetterOrderByDividend_All.rpt";
     }
   }
   else if (PRecords.equalsIgnoreCase("rangebyfolio"))
   {
     if (DivOrder.equalsIgnoreCase("byname"))
     {
         reporturl = "/CR_Reports/DividendWarrants/SMS/ByName/Folio_Div_Warrant_&_LetterOrderByName_RangeFolio.rpt";
     }
     else if (DivOrder.equalsIgnoreCase("byfolio"))
     {
         reporturl = "/CR_Reports/DividendWarrants/SMS/ByFolio/Folio_Div_Warrant_&_LetterOrderByFolio_RangeFolio.rpt";
     }
     else if (DivOrder.equalsIgnoreCase("bydividend"))
     {
        reporturl = "/CR_Reports/DividendWarrants/SMS/ByDividend/Folio_Div_Warrant_&_LetterOrderByDividend_RangeFolio.rpt";
     }
   }
   else if (PRecords.equalsIgnoreCase("rangebywarrant"))
   {
     if (DivOrder.equalsIgnoreCase("byname"))
     {
         reporturl = "/CR_Reports/DividendWarrants/SMS/ByName/Folio_Div_Warrant_&_LetterOrderByName_RangeWarrant.rpt";
     }
     else if (DivOrder.equalsIgnoreCase("byfolio"))
     {
         reporturl = "/CR_Reports/DividendWarrants/SMS/ByFolio/Folio_Div_Warrant_&_LetterOrderByFolio_RangeWarrant.rpt";
     }
     else if (DivOrder.equalsIgnoreCase("bydividend"))
     {
        reporturl = "/CR_Reports/DividendWarrants/SMS/ByDividend/Folio_Div_Warrant_&_LetterOrderByDividend_RangeWarrant.rpt";
     }
   }
  }
  else if (DivType.equalsIgnoreCase("CDBLtype"))
  {
   if (PRecords.equalsIgnoreCase("singlebyfolio"))
   {
     String query1 = "SELECT * FROM BODIVIDEND_VIEW WHERE ISSUE_DATE = TO_DATE('" + DateDec + "','DD/MM/YYYY') AND BO_ID = '" + ForBNo + "'";
     int fcc1 = cm.queryExecuteCount(query1);

     if (fcc1 < 1)
     {
     %>
      <script language="javascript">
       alert("BO ID does not exist!");
       location = "<%=request.getContextPath()%>/jsp/Dividend/SetLetterCheque.jsp";
      </script>
     <%
     }
     else if (fcc1 > 0)
     {
       if (DivOrder.equalsIgnoreCase("byname"))
       {
         reporturl = "/CR_Reports/DividendWarrants/CDBL/By_Name/BO_Div_Warrant_&_LetterOrderByName_ParamBO.rpt";
       }
       else if (DivOrder.equalsIgnoreCase("byfolio"))
       {
         reporturl = "/CR_Reports/DividendWarrants/CDBL/By_BOID/BO_Div_Warrant_&_LetterOrderByBO_ParamBO.rpt";
       }
       else if (DivOrder.equalsIgnoreCase("bydividend"))
       {
         reporturl = "/CR_Reports/DividendWarrants/CDBL/ByDividend/BO_Div_Warrant_&_LetterOrderByDividend_ParamBO.rpt";
       }
     }
   }
   else if (PRecords.equalsIgnoreCase("singlebywarrant"))
   {
     String query2 = "SELECT * FROM BODIVIDEND_VIEW WHERE WARRANT_NO = '" + SWarrantNo + "'";
     int fcc2 = cm.queryExecuteCount(query2);

     if (fcc2 < 1)
     {
     %>
      <script language="javascript">
       alert("Warrant does not exist!");
       location = "<%=request.getContextPath()%>/jsp/Dividend/SetLetterCheque.jsp";
      </script>
     <%
     }
     else if (fcc2 > 0)
     {
       if (DivOrder.equalsIgnoreCase("byname"))
       {
         reporturl = "/CR_Reports/DividendWarrants/CDBL/By_Name/BO_Div_Warrant_&_LetterOrderByName_ParamWarrant.rpt";
       }
       else if (DivOrder.equalsIgnoreCase("byfolio"))
       {
         reporturl = "/CR_Reports/DividendWarrants/CDBL/By_BOID/BO_Div_Warrant_&_LetterOrderByBO_ParamWarrant.rpt";
       }
       else if (DivOrder.equalsIgnoreCase("bydividend"))
       {
         reporturl = "/CR_Reports/DividendWarrants/CDBL/ByDividend/BO_Div_Warrant_&_LetterOrderByDividend_ParamWarrant.rpt";
       }
     }
   }
   else if (PRecords.equalsIgnoreCase("notprinted"))
   {
     if (DivOrder.equalsIgnoreCase("byname"))
     {
         reporturl = "/CR_Reports/DividendWarrants/CDBL/By_Name/BO_Div_Warrant_&_LetterOrderByName_All.rpt";
     }
     else if (DivOrder.equalsIgnoreCase("byfolio"))
     {
         reporturl = "/CR_Reports/DividendWarrants/CDBL/By_BOID/BO_Div_Warrant_&_LetterOrderByBO_All.rpt";
     }
     else if (DivOrder.equalsIgnoreCase("bydividend"))
     {
         reporturl = "/CR_Reports/DividendWarrants/CDBL/ByDividend/BO_Div_Warrant_&_LetterOrderByDividend_All.rpt";
     }
   }
   else if (PRecords.equalsIgnoreCase("rangebyfolio"))
   {
     if (DivOrder.equalsIgnoreCase("byname"))
     {
         reporturl = "/CR_Reports/DividendWarrants/CDBL/By_Name/BO_Div_Warrant_&_LetterOrderByName_ParamRangeBO.rpt";
     }
     else if (DivOrder.equalsIgnoreCase("byfolio"))
     {
         reporturl = "/CR_Reports/DividendWarrants/CDBL/By_BOID/BO_Div_Warrant_&_LetterOrderByBO_ParamRangeBO.rpt";
     }
     else if (DivOrder.equalsIgnoreCase("bydividend"))
     {
         reporturl = "/CR_Reports/DividendWarrants/CDBL/ByDividend/BO_Div_Warrant_&_LetterOrderByDividend_ParamRangeBO.rpt";
     }
   }
   else if (PRecords.equalsIgnoreCase("rangebywarrant"))
   {
     if (DivOrder.equalsIgnoreCase("byname"))
     {
         reporturl = "/CR_Reports/DividendWarrants/CDBL/By_Name/BO_Div_Warrant_&_LetterOrderByName_RangeWarrant.rpt";
     }
     else if (DivOrder.equalsIgnoreCase("byfolio"))
     {
         reporturl = "/CR_Reports/DividendWarrants/CDBL/By_BOID/BO_Div_Warrant_&_LetterOrderByBO_RangeWarrant.rpt";
     }
     else if (DivOrder.equalsIgnoreCase("bydividend"))
     {
         reporturl = "/CR_Reports/DividendWarrants/CDBL/ByDividend/BO_Div_Warrant_&_LetterOrderByDividend_RangeWarrant.rpt";
     }
   }
  }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Printed Dividend Warrant and Letter')";
  boolean ub = cm.procedureExecute(ulog);

  if (!reporturl.equals("")) {
    session.setAttribute("reportSource", null);
  }

Object reportSource = session.getAttribute("reportSource");
if (reportSource == null)
{
try{
  IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
  reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
  session.setAttribute("reportSource", reportSource);
}catch (Exception e) {}
}
  CrystalReportViewer viewer= new CrystalReportViewer();
  try{
    viewer.setReportSource(reportSource);
  }catch (Exception e) {}
  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  //viewer.setPrintMode(CrPrintMode.PDF);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setReuseParameterValuesOnRefresh(true);

// Passing parameters

ParameterField param1 = new ParameterField();
param1.setReportName("");
param1.setName("ddate");

Values vals1 = new Values();
ParameterFieldDiscreteValue val1 = new ParameterFieldDiscreteValue();
val1.setValue(DateDec);
vals1.add(val1);

ParameterField param2 = new ParameterField();
param2.setReportName("");
param2.setName("ParamWarrant");

Values vals2 = new Values();
ParameterFieldDiscreteValue val2 = new ParameterFieldDiscreteValue();
val2.setValue(SWarrantNo);
vals2.add(val2);

ParameterField param3 = new ParameterField();
param3.setReportName("");
param3.setName("WarrantGreaterThan");

Values vals3 = new Values();
ParameterFieldDiscreteValue val3 = new ParameterFieldDiscreteValue();
val3.setValue(WStart);
vals3.add(val3);

ParameterField param4 = new ParameterField();
param4.setReportName("");
param4.setName("WarrantLessThan");

Values vals4 = new Values();
ParameterFieldDiscreteValue val4 = new ParameterFieldDiscreteValue();
val4.setValue(WEnd);
vals4.add(val4);

ParameterField param5 = new ParameterField();
param5.setReportName("");
param5.setName("ParamID");

Values vals5 = new Values();
ParameterFieldDiscreteValue val5 = new ParameterFieldDiscreteValue();
val5.setValue(ForBNo);
vals5.add(val5);

ParameterField param6 = new ParameterField();
param6.setReportName("");
param6.setName("FolioGreaterThan");

Values vals6 = new Values();
ParameterFieldDiscreteValue val6 = new ParameterFieldDiscreteValue();
val6.setValue(FStart);
vals6.add(val6);

ParameterField param7 = new ParameterField();
param7.setReportName("");
param7.setName("FolioLessThan");

Values vals7 = new Values();
ParameterFieldDiscreteValue val7 = new ParameterFieldDiscreteValue();
val7.setValue(FEnd);
vals7.add(val7);

try {
 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
 Date d = df.parse(DateDec);
 val1.setValue(d);
 vals1.add(val1);
} catch(Exception e) {
e.printStackTrace();
}

param1.setCurrentValues(vals1);
param2.setCurrentValues(vals2);
param3.setCurrentValues(vals3);
param4.setCurrentValues(vals4);
param5.setCurrentValues(vals5);
param6.setCurrentValues(vals6);
param7.setCurrentValues(vals7);

Fields fields = new Fields();
fields.add(param1);
fields.add(param2);
fields.add(param3);
fields.add(param4);
fields.add(param5);
fields.add(param6);
fields.add(param7);

viewer.setParameterFields(fields);
viewer.refresh();

try
{
viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);

  if (DivType.equalsIgnoreCase("SMStype"))
  {
   if (PRecords.equalsIgnoreCase("singlebyfolio"))
   {
       markprinted = "call MARK_DIVIDEND_PRINTED_FOLIO('" + ForBNo + "','" + DateDec + "')";
       mp = cm.procedureExecute(markprinted);
   }
   else if (PRecords.equalsIgnoreCase("singlebywarrant"))
   {
      markprinted = "call MARK_DIVIDEND_PRINTED('" + SWarrantNo + "')";
      mp = cm.procedureExecute(markprinted);
   }
   else if (PRecords.equalsIgnoreCase("notprinted"))
   {
     markprinted = "call MARK_DIVIDEND_PRINTED_ALL('" + DateDec + "')";
     mp = cm.procedureExecute(markprinted);
   }
   else if (PRecords.equalsIgnoreCase("rangebyfolio"))
   {
     markprinted = "call MARK_DIVIDEND_PRINTED_FRANGE('" + FStart + "','" + FEnd + "','" + DateDec + "')";
     mp = cm.procedureExecute(markprinted);
   }
   else if (PRecords.equalsIgnoreCase("rangebywarrant"))
   {
     markprinted = "call MARK_DIVIDEND_PRINTED_WRANGE('" + WStart + "','" + WEnd + "','" + DateDec + "')";
     mp = cm.procedureExecute(markprinted);
   }
  }
  else if (DivType.equalsIgnoreCase("CDBLtype"))
  {
   if (PRecords.equalsIgnoreCase("singlebyfolio"))
   {
     markprinted = "call MARK_BODIVIDEND_PRINTED_BO('" + ForBNo + "','" + DateDec + "')";
     mp = cm.procedureExecute(markprinted);
   }
   else if (PRecords.equalsIgnoreCase("singlebywarrant"))
   {
     markprinted = "call MARK_BODIVIDEND_PRINTED('" + SWarrantNo + "')";
     mp = cm.procedureExecute(markprinted);
   }
   else if (PRecords.equalsIgnoreCase("notprinted"))
   {
     markprinted = "call MARK_BODIVIDEND_PRINTED_ALL('" + DateDec + "')";
     mp = cm.procedureExecute(markprinted);
   }
   else if (PRecords.equalsIgnoreCase("rangebyfolio"))
   {
     markprinted = "call MARK_BODIVIDEND_PRINTED_BRANGE('" + FStart + "','" + FEnd + "','" + DateDec + "')";
     mp = cm.procedureExecute(markprinted);
   }
   else if (PRecords.equalsIgnoreCase("rangebywarrant"))
   {
     markprinted = "call MARK_BODIVIDEND_PRINTED_WRANGE('" + WStart + "','" + WEnd + "','" + DateDec + "')";
     mp = cm.procedureExecute(markprinted);
   }
  }

}
catch(Exception e){System.out.println(e.getMessage());}

  if (isConnect)
  {
   cm.takeDown();
  }

%>
</body>
</html>

