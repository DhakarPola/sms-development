<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : January 2006
'Page Purpose  : Passes Parameters for Right Certificate Printing.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<jsp:useBean id="cm1" class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

 boolean isConnect = cm.connect();
 boolean isConnect1 = cm1.connect();
 if(isConnect==false)
 {
   %>
    <jsp:forward page="ErrorMsg.jsp" >
    <jsp:param name="ErrorTitle" value="Connection Failure" />
    <jsp:param name="ErrorHeading" value="Connection Problem" />
    <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
    </jsp:forward>
  <%
 }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Printed Right Share Certificates')";
  boolean ub = cm.procedureExecute(ulog);

%>

<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<%@page
import="java.util.Date,java.text.*,com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
Print Right Certificates
</title>
</head>
<body bgcolor="#ffffff">
<h1>
</h1>
<%
  String DateDec = String.valueOf(request.getParameter("dateselect"));
  String PrintScope = String.valueOf(request.getParameter("printscope"));
  String CertificateNo = String.valueOf(request.getParameter("certificateno"));

  if (String.valueOf(CertificateNo).equals("null"))
   CertificateNo = "";

  String reporturl = "";
  String ms = "";
  boolean b;

  if (PrintScope.equalsIgnoreCase("printsingle"))
  {
   reporturl = "/CR_Reports/Right_Certificate_Print/ByCertificateNum.rpt";
  }
  else if (PrintScope.equalsIgnoreCase("printall"))
  {
   reporturl = "/CR_Reports/Right_Certificate_Print/All.rpt";
  }

    if (!reporturl.equals("")) {
    session.setAttribute("reportSource", null);
    session.setAttribute("refreshed", null);
  }

Object reportSource = session.getAttribute("reportSource");
if (reportSource == null)
{
try{
  IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
  reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
  session.setAttribute("reportSource", reportSource);
}catch (Exception e) {}
}
  CrystalReportViewer viewer= new CrystalReportViewer();
  try{
    viewer.setReportSource(reportSource);
  }catch (Exception e) {}

  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setOwnForm(true);

// Passing parameters

ParameterField param1 = new ParameterField();
param1.setReportName("");
param1.setName("ParamDeclareDate");

Values vals1 = new Values();
ParameterFieldDiscreteValue val1 = new ParameterFieldDiscreteValue();
val1.setValue(DateDec);
vals1.add(val1);

ParameterField param2 = new ParameterField();
param2.setReportName("");
param2.setName("ParamCertNo");

Values vals2 = new Values();
ParameterFieldDiscreteValue val2 = new ParameterFieldDiscreteValue();
val2.setValue(CertificateNo);
vals2.add(val2);


try {
 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
 Date d = df.parse(DateDec);
 val1.setValue(d);
 vals1.add(val1);
} catch(Exception e) {
e.printStackTrace();
}

param1.setCurrentValues(vals1);
param2.setCurrentValues(vals2);

Fields fields = new Fields();
fields.add(param1);
fields.add(param2);

viewer.setParameterFields(fields);

if (session.getAttribute("refreshed") == null)
{
viewer.refresh();
session.setAttribute("refreshed", "true");
}


try
{
viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);

  if (PrintScope.equalsIgnoreCase("printsingle"))
  {
    ms = "call MARK_CERTIFICATE_PRINTED('" + CertificateNo + "')";
    b = cm.procedureExecute(ms);
  }
  else if (PrintScope.equalsIgnoreCase("printall"))
  {
    ms = "SELECT * FROM  CERTIFICATE_RIGHT_VIEW WHERE DATE_DEC = TO_DATE('" + DateDec + "','DD/MM/YYYY')";
    cm.queryExecute(ms);

    String certificateno = "";
    String ms1 = "";

    while(cm.toNext())
     {
       certificateno = cm.getColumnS("CERTIFICATE_NO");

       ms1 = "call MARK_CERTIFICATE_PRINTED('" + certificateno + "')";
       b = cm1.procedureExecute(ms1);
     }
  }
}
catch(Exception e){System.out.println(e.getMessage());}

  if (isConnect)
  {
   cm.takeDown();
   cm1.takeDown();
  }


%>
</body>
</html>

