
<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page
import="java.util.Date,java.text.*,com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
Bonus Allotment List
</title>
</head>
<body bgcolor="#ffffff">
<h1>
</h1>
<%
  String DateDec = String.valueOf(request.getParameter("dateselect"));
  String DivType = String.valueOf(request.getParameter("dividendtype"));
  String DivOrder = String.valueOf(request.getParameter("orderby"));

  String reporturl = "";


  if (DivType.equalsIgnoreCase("SMStype"))
  {
   if (DivOrder.equalsIgnoreCase("byname"))
   {
     reporturl = "/CR_Reports/Folio_Bonus_Allotment/Folio_BonusAllotmentOrderByName.rpt";
   }
   else if (DivOrder.equalsIgnoreCase("byfolio"))
   {
     reporturl = "/CR_Reports/Folio_Bonus_Allotment/Folio_BonusAllotmentOrderByFolio.rpt";
   }
  }
  else if (DivType.equalsIgnoreCase("CDBLtype"))
  {
   if (DivOrder.equalsIgnoreCase("byname"))
   {
     reporturl = "/CR_Reports/BO_Bonus_Allotment/BO_BonusAllotmentOrdeByName.rpt";
   }
   else if (DivOrder.equalsIgnoreCase("byfolio"))
   {
     reporturl = "/CR_Reports/BO_Bonus_Allotment/BO_BonusAllotmentOrderByBO.rpt";
   }
  }

  if (!reporturl.equals("")) {
    session.setAttribute("reportSource", null);
    session.setAttribute("refreshed", null);
  }

Object reportSource = session.getAttribute("reportSource");
if (reportSource == null)
{
try{
  IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
  reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
  session.setAttribute("reportSource", reportSource);
}catch (Exception e) {}
}
  CrystalReportViewer viewer= new CrystalReportViewer();
  try{
    viewer.setReportSource(reportSource);
  }catch (Exception e) {}
  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setOwnForm(true);

// Passing parameters

ParameterField param1 = new ParameterField();
param1.setReportName("");
param1.setName("ParamDeclareDate");

Values vals1 = new Values();
ParameterFieldDiscreteValue val1 = new ParameterFieldDiscreteValue();
val1.setValue(DateDec);
vals1.add(val1);

try {
 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
 Date d = df.parse(DateDec);
 val1.setValue(d);
 vals1.add(val1);
} catch(Exception e) {
e.printStackTrace();
}

param1.setCurrentValues(vals1);

Fields fields = new Fields();
fields.add(param1);

viewer.setParameterFields(fields);
if (session.getAttribute("refreshed") == null)
{
viewer.refresh();
session.setAttribute("refreshed", "true");
}


try
{
viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);
}
catch(Exception e){System.out.println(e.getMessage());}

%>
</body>
</html>

