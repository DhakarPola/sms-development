package batbsms;

import java.io.ByteArrayInputStream;
import java.sql.*;
import java.sql.SQLException;
import java.io.*;
import java.io.BufferedReader;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.lang.Object.*;
import java.lang.Number;
import java.lang.Long;
import java.lang.Integer;
import java.lang.Short;

import java.math.*;
import java.math.BigInteger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import oracle.jdbc.pool.OracleDataSource;
//import oracle.jdbc.driver.OracleResultSet;
import oracle.jdbc.OracleResultSet;
import oracle.jdbc.driver.*;
import oracle.sql.BLOB;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

//~--- non-JDK imports --------------------------------------------------------

/*import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;*/

//~--- JDK imports ------------------------------------------------------------
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.text.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class conBean {

    Connection con;
    ResultSet rs;
    Statement stmt;

    /*for test purpose */
    //private java.sql.Connection  con = null;
    //  private final String url = "";
    private String serverName;
    private String userName;
    private String password;
    //private final String databaseName= "lcexport";

    // Informs the driver to use server a side-cursor,
    // which permits more than one active statement
    // on a connection.
    private final String selectMethod = "cursor";

    //---------------------------------------------------
    public conBean() {

    }

    public Connection getConnection() {
        return con;
    }

    public boolean conn() {

        try {
            Context ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:/batbsms");
            Connection conn = ds.getConnection();
            return true;
        } catch (NamingException ex) {
            System.out.println("NamingException");
            System.out.println(ex.getMessage());
            return false;
        } catch (SQLException ex) {
            System.out.println("SQLException");
            System.out.println(ex.getMessage());
            return false;
        }
    }

    //*****************For connection, returns boolean******************************
    public boolean connect() {
        try {

            // Open a file of the given name.
            File file = new File("smstest.ini");

            // Get the size of the opened file.
            int size = (int) file.length();

            // Set to zero a counter for counting the number of
            // characters that have been read from the file.
            int chars_read = 0;

            // Create an input reader based on the file, so we can read its data.
            FileReader in = new FileReader(file);

            // Create a character array of the size of the file,
            // to use as a data buffer, into which we will read
            // the text data.
            char[] data = new char[size];

            // Read all available characters into the buffer.
            while (in.ready()) {
                // Increment the count for each character read,
                // and accumulate them in the data buffer.
                chars_read += in.read(data, chars_read, size - chars_read);
            }
            in.close();
            String info = new String(data, 0, chars_read);
            info = info.substring(info.indexOf("\n"), info.length());
            String[] values = info.split(",");

            serverName = values[4].substring(values[4].indexOf("=") + 1,
                    values[4].length());
            userName = values[5].substring(values[5].indexOf("=") + 1,
                    values[5].length());
            password = values[6].substring(values[6].indexOf("=") + 1,
                    values[6].length());

        } catch (FileNotFoundException e) {
            //System.out.println("File not found.");
        } catch (IOException e) {
            //System.out.println("IO exception occured while reading file.");
        }

        try {

            Class.forName("oracle.jdbc.driver.OracleDriver");

            String url = "jdbc:oracle:thin:@" + serverName;

            //System.out.println("Server Name: " + serverName);
            con = DriverManager.getConnection(url, userName, password);

            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);

            return true;
        } catch (ClassNotFoundException cnfex) {
            cnfex.printStackTrace();
            return false;
        } catch (SQLException cnfex) {
            cnfex.printStackTrace();
            return false;
        }
    }

    //-----------------Start: Test Module----------------------------
    //  private String getConnectionUrl(){
    //          return url+serverName+";databaseName="+databaseName+";selectMethod="+selectMethod+";";
    //  }

    /*
         Display the driver properties, database details
     */
    public void connAutoCommit(boolean tf) {

        try {
            con.setAutoCommit(tf);
        } catch (SQLException SQLEx) {

        }
    }

    public void connCommit() {
        try {
            con.commit();
        } catch (SQLException SQLEx) {

        }
    }

    private void closeConnection() {
        try {
            if (con != null) {
                con.close();
            }
            con = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //-----------------End: Test Module----------------------------

    //******************************************************************************
    //*****************For connection, returns boolean******************************
    //*****************************For executing query******************************
    public void queryExecute(String s) throws Exception {
        rs = stmt.executeQuery(s);
        //stmt.execute(s);

    }

    public boolean procedureExecute(String s) throws Exception {
        return stmt.execute(s);
    }

    //******************************************************************************
    //*****************************For executing query****************************//
    //*************For executing query and check if row is found or not*************
    public boolean queryExecuteCheckRow(String s) throws Exception {
        rs = stmt.executeQuery(s);
        if (toNext() == true) {
            return true;
        } else {
            return false;
        }
    }

    //*************For executing query and check if row is found or not***********//
    //****************************For counting rows*********************************
    public int queryExecuteCount(String s) throws Exception {
        int i = 0;
        rs = stmt.executeQuery(s);
        //while(toNext()==true){
        //	i+=1;}
        rs.last();
        //	return i;
        return rs.getRow();
    }

    //***************************For counting rows********************************//
    /*
       public String getCallID(){
         String CallID = "0000000000";

         if(con==null){
           if(connect()){
             try{
      queryExecute("SELECT MAX(CallID)+1 as ID FROM Call_History_Info");
               toNext();
               CallID = "0000000000" + String.valueOf(getColumnI("ID"));
      CallID = CallID.substring(CallID.length() - 10, CallID.length());

             }catch(Exception e){

             }
           }
         }
         return CallID;
       }
     */

    //***********************Execute query and returns a resultset******************
    public ResultSet queryExecuteGetResultSet(String s) throws Exception {
        rs = stmt.executeQuery(s);

        return rs;
    }
    //******************************************************************************
    //***********************Execute query and returns a resultset****************//

    //***********************Insert in database ******************
    public int insertRow(String s) throws Exception {
        int done = stmt.executeUpdate(s);
        return done;
    }
    //******************************************************************************
    //***********************Execute query and returns a resultset****************//

    //For next resultSet
    public boolean toNext() throws Exception {
        return rs.next();
    }

    //For reading text
    public String getColumnS(String s) {
        String val = "";
        try {
            val = rs.getString(s);
        } catch (Exception e) {
            val = "";
        }
        return val;
    }

    //For reading datetime
    public String getColumnDT(String s) {
        //Date d = new Date();
        String val = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            val = sdf.format(rs.getDate(s));
        } catch (Exception e) {
            val = "";
        }
        return val;
    }

    //For reading int
    public int getColumnI(String s) throws Exception {
        return rs.getInt(s);
    }

    public float getColumnF(String s) throws Exception {
        return rs.getFloat(s);
    }

    public BigDecimal getColumnD(String s) throws Exception {
        BigDecimal num = new BigDecimal(rs.getString(s));
        return num;
    }

    public String replaceSingleQuote(String s) {
        s = replace(s, "'", "''");
        s = replace(s, "<br><br>", "<br>");
        return s;
    }

    /*
        public String replaceSingleQuote(String s) {
             if (s != null) {
               StringBuffer sb = new StringBuffer(s);
               for (int i=0; i < sb.length(); i++) {
                 if (sb.charAt(i) == '\'') {
                  sb.insert(i+1, '\'');
                   i++;
                 }else if (sb.charAt(i) == '<br>') {
                  sb.insert(i, '\n');
                   i++;
                 }

               }
               s = sb.toString();
             }
             return s;
        }
     */

 /* Replace all instances of a String in a String.
      *   @param  s  String to alter.
      *   @param  f  String to look for.
      *   @param  r  String to replace it with, or null to just remove it.
     */
    public String replace(String s, String f, String r) {
        if (s == null) {
            return s;
        }
        if (f == null) {
            return s;
        }
        if (r == null) {
            r = "";
        }

        int index01 = s.indexOf(f);
        while (index01 != -1) {
            s = s.substring(0, index01) + r + s.substring(index01 + f.length());
            index01 += r.length();
            index01 = s.indexOf(f, index01);
        }
        return s;
    }

    //converts datetime from "yyyy-MM-dd HH:mm:ss" to "dd/mm/yyyy hh:mm:ss a/p"
    public String getDateTime4User(String dt) {
        if (dt.equals("") || dt.equals("null")) {
            return "";
        }

        //   System.out.println(dt);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");

        String[] vals1 = dt.split(" ");
        String dateOnly = vals1[0];
        String timeOnly = vals1[1];

        String[] vals2 = dateOnly.split("-");
        int dd = Integer.parseInt(vals2[2]);
        int mm = Integer.parseInt(vals2[1]);
        int yyyy = Integer.parseInt(vals2[0]);

        String[] vals3 = timeOnly.split(":");
        int hh = Integer.parseInt(vals3[0]);
        int mi = Integer.parseInt(vals3[1]);
        int ss = Integer.parseInt(vals3[2]);

        //   System.out.println(String.valueOf(mm) + "/" + String.valueOf(dd) + "/" + String.valueOf(yyyy) + " "+ timeOnly) ;
        //System.out.println(dt);
        //   System.out.println("ff="+sdf.format(new Date(yyyy - 1900,mm,dd,hh,mi,ss)));
        return sdf.format(new Date(yyyy - 1900, mm - 1, dd, hh, mi, ss));
    }

    //converts datetime from "dd/mm/yyyy hh:mm:ss a/p" to "yyyy-MM-dd HH:mm:ss"
    public String getDateTime4db(String dt) {
        if (dt.equals("") || dt.equals("null")) {
            return "";
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String[] vals1 = dt.split(" ");
        String dateOnly = vals1[0];
        String timeOnly = vals1[1];
        String ampm = vals1[2];

        String[] vals2 = dateOnly.split("/");
        String dd = vals2[0];
        String mm = vals2[1];
        String yyyy = vals2[2];

        String strDateTime = mm + "/" + dd + "/" + yyyy + " " + timeOnly + " "
                + ampm;
        //   System.out.println("strDateTime="+strDateTime);
        return sdf.format(new Date(strDateTime));
    }

    //For closing connection
    public void print(String msg) throws Exception {
        System.out.println(msg);

    }

    //For closing connection
    public void takeDown() throws Exception {
        if (stmt != null) {
            stmt.close();
        }
        if (con != null) {
            con.close();
        }
        con = null;

    }

    public String importDematConf(String from, String to, String tblName, String comments, String status, Date entryDate, String filePath) throws Exception {
        String returnStr = "";
        try {
            /*Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            String filename = filePath;
            String database = "jdbc:odbc:Driver={Microsoft Access Driver (*.mdb)};DBQ=";
            database += filename.trim() + ";DriverID=22;READONLY=true}"; // add on to the end
             */

 /* 
                Md. Zahidul Islam 2017 July 07 
             */
            Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
            String filename = filePath;
            String database = "jdbc:ucanaccess://" + filename.trim();

            // now we can get the connection from the DriverManager
            Connection conn = DriverManager.getConnection(database, "", "");
            // try and create a java.sql.Statement so we can run queries
            Statement s = conn.createStatement();
            s.execute("select * from " + tblName); // select the data from the table
            ResultSet mdbrs = s.getResultSet(); // get any ResultSet that came from our query
            int count = 0;
            //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            //java.sql.Date pfDate=java.sql.Date.valueOf(sdf.format(dt));
            Date dt = new Date();
            String certStatus = "";
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat simD = new SimpleDateFormat("dd-MMM-yyyy");
            java.sql.Date pfDate = null;
            String CertNo = "000000000";
            String pfDtStr = "";
            int totlal = 0;
            CallableStatement cstmt = con.prepareCall("{call ADD_BODEMATCONFIRMATIONTEMP(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
            if (mdbrs != null) // if rs == null, then there is no ResultSet to view
            {
                while (mdbrs.next()) { // this will step through our data row-by-row
                    Date tempDate = null;
                    tempDate = simD.parse(mdbrs.getString("PF_BDATE"));
                    pfDtStr = sdf.format(tempDate);
                    pfDate = java.sql.Date.valueOf(pfDtStr);
                    //System.out.println("pfDate="+pfDate);
                    //pfDate=mdbrs.getDate("PF_BDATE");
                    if (count == 0) {
                        totlal = queryExecuteCount("SELECT * FROM BODEMATCONFIRMATION_VIEW WHERE to_char(PF_BDATE,'YYYY-MM-DD')='" + pfDtStr + "'");
                        if (totlal > 0) {
                            sdf.applyPattern("dd/MM/yyyy");
                            cstmt.close();
                            mdbrs.close();
                            s.close(); // close the Statement to let the database know we're done with it
                            conn.close(); // close the Connection to let the database know we're done with it
                            return "Imported-" + sdf.format(pfDate);
                        }
                    }
                    cstmt.setDouble(1, mdbrs.getDouble("GH2_DE_SEQ_NUMB"));
                    cstmt.setString(2, mdbrs.getString("GH2_DE_DRF_NUMB"));
                    cstmt.setString(3, mdbrs.getString("GH2_BO_ID"));
                    cstmt.setString(4, mdbrs.getString("GH2_BO_SHR_NAME"));
                    cstmt.setDouble(5, mdbrs.getDouble("GH2_DE_ACCEPT_QTY"));
                    cstmt.setDouble(6, mdbrs.getDouble("GH2_DE_REJ_QTY"));
                    CertNo = "000000000";
                    CertNo = "000000000"
                            + mdbrs.getString("GH4_DE_CERTIFICATE_NUMB");
                    CertNo = CertNo.substring(CertNo.length() - 9,
                            CertNo.length());
                    cstmt.setString(7, CertNo);
                    cstmt.setString(8, mdbrs.getString("GH4_DE_REG_FOLIO_NUMB"));
                    cstmt.setString(9,
                            mdbrs.getString("GH4_DE_CERTIFICATE_STAT"));
                    cstmt.setDouble(10,
                            mdbrs.getDouble("GH4_Sum_DE_CERTIFICATE_QTY"));
                    cstmt.setDouble(11, mdbrs.getDouble("DE_DE_START_DIST_NUMB"));
                    cstmt.setDouble(12, mdbrs.getDouble("DE_DE_END_DIST_NUMB"));
                    cstmt.setDate(13, pfDate);
                    cstmt.setString(14, mdbrs.getString("GH2_LCK_STAT"));
                    cstmt.setDouble(15, mdbrs.getDouble("GH2_DRF_QTY"));
                    cstmt.addBatch();
                    count++;
                }
            }
            try {
                con.setAutoCommit(false);
                CallableStatement cstmt1 = con.prepareCall(
                        "{call DELETE_bodematconfirmationtemp()}");
                cstmt1.execute();
                cstmt1.close();
                int[] updateCounts = cstmt.executeBatch();
                cstmt.close();
                cstmt = con.prepareCall(
                        "{call ADD_IMPORT(?, ?, ?, ?, ?, ?, ?)}");
                cstmt.setString(1, from);
                cstmt.setString(2, to);
                cstmt.setString(3, tblName);
                cstmt.setString(4, comments);
                cstmt.setString(5, status);
                sdf.applyPattern("yyyy-MM-dd");
                java.sql.Date ed = java.sql.Date.valueOf(sdf.format(entryDate));
                cstmt.setDate(6, ed);
                cstmt.setDate(7, pfDate);
                cstmt.execute();
                con.commit();
                con.setAutoCommit(true);
            } catch (SQLException exSQL) {
                //exSQL.printStackTrace();
                returnStr = "ImportFailed";
                //System.out.println("kamrul");
            }
            cstmt.close();
            mdbrs.close();
            s.close(); // close the Statement to let the database know we're done with it
            conn.close(); // close the Connection to let the database know we're done with it
        } catch (ClassNotFoundException cnf) {
            //cnf.printStackTrace();
            //System.out.println("Could not find class: " + cnf);
            returnStr = "ConnectionError";

        } catch (SQLException sqle) {
            if (sqle.getErrorCode() == -1305) {
                returnStr = "TableNotFound-" + tblName;
            } else {
                returnStr = "ImportFailed";

            }
        }
        return returnStr;
    }

    //******************************************************************** kamruzzaman ***********************/
    public String importDematRequest(String from, String to, String tblName, String comments, String status, Date entryDate, String filePath) throws Exception {
        String returnStr = "";
        //System.err.println("net.ucanaccess.jdbc.UcanaccessDriver");
        try {
            /*Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
           String filename = filePath;
           String database = "jdbc:odbc:Driver={Microsoft Access Driver (*.mdb)};DBQ=";
           database+= filename.trim() + ";DriverID=22;READONLY=true}"; // add on to the end
             */

 /* 
                Md. Zahidul Islam 2017 July 07 
             */
            //System.err.println("net.ucanaccess.jdbc.UcanaccessDriver");
            Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
            String filename = filePath;
            //System.err.println(filename);
            String database = "jdbc:ucanaccess://" + filename.trim();
            //System.err.println(database);
            // now we can get the connection from the DriverManager
            Connection conn = DriverManager.getConnection(database, "", "");
            // try and create a java.sql.Statement so we can run queries
            Statement s = conn.createStatement();
            //s.execute("select * from " + tblName); // select the data from the table
            s.execute("select * from Zahid08062017");
            ResultSet mdbrs = s.getResultSet(); // get any ResultSet that came from our query
            System.out.println(mdbrs.toString());
            int count = 0;
            //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            //java.sql.Date pfDate=java.sql.Date.valueOf(sdf.format(dt));
            //String maxRfId="select nvl(max(REF_NO),0)as refId from DEMAT_REQUEST_VIEW";
            // ResultSet rstemp=stmt.executeQuery(maxRfId);
            // rstemp.next();
            // int refId=rstemp.getInt("refId");
            Date dt = new Date();
            String certStatus = "";
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat simD = new SimpleDateFormat("dd-MMM-yyyy");
            java.sql.Date pfDate = null;
            String CertNo = "000000000";
            String pfDtStr = "";
            int total = 0;
            CallableStatement cstmt = con.prepareCall("{call ADD_DEMAT_REQUEST(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
            if (mdbrs != null) // if rs == null, then there is no ResultSet to view
            {
                while (mdbrs.next()) { // this will step through our data row-by-row
                    System.err.println("database 111");
                    Date tempDate = null;
                    tempDate = simD.parse(mdbrs.getString("PF_BDATE"));
                    pfDtStr = sdf.format(tempDate);
                    pfDate = java.sql.Date.valueOf(pfDtStr);

                    if (count == 0) {
                        String q = "SELECT * FROM DEMAT_REQUEST_VIEW WHERE PF_BDATE=TO_DATE('" + pfDtStr + "','yyyy-MM-dd')";
                        System.err.println("database 112");
                        total = queryExecuteCount(q);
                        if (total > 0) {
                            sdf.applyPattern("dd/MM/yyyy");
                            cstmt.close();
                            mdbrs.close();
                            s.close(); // close the Statement to let the database know we're done with it
                            conn.close(); // close the Connection to let the database know we're done with it
                            return "Imported-" + sdf.format(pfDate);
                        }
                    }
                    int refId = 0;
                    cstmt.setInt(1, refId);
                    //cstmt.setDouble(1,mdbrs.getDouble("GH2_DE_SEQ_NUMB"));
                    //if(mdbrs.getString("GH3_BO_ID") == ""){
                    cstmt.setString(2, "GH3_BO_ID");
                    //}else{
                    //      cstmt.setString(2, mdbrs.getString("GH3_BO_ID"));
                    //}
                    //if(mdbrs.getString("GH3_BO_SHR_NAME1") == ""){
                    cstmt.setString(3, "GH3_BO_SHR_NAME1");
                    //}else{
                    //      cstmt.setString(3, mdbrs.getString("GH3_BO_SHR_NAME1"));
                    //}              
                    //cstmt.setString(3, mdbrs.getString("GH3_BO_SHR_NAME1"));
                    cstmt.setDouble(4, mdbrs.getDouble("GH4_Sum_DE_CERTIFICATE_QTY"));
                    cstmt.setString(5, mdbrs.getString("GH4_DE_REG_FOLIO_NUMB"));
                    CertNo = "000000000";
                    CertNo = "000000000" + mdbrs.getString("GH4_DE_CERTIFICATE_NUMB");
                    CertNo = CertNo.substring(CertNo.length() - 9, CertNo.length());
                    cstmt.setString(6, CertNo);
                    cstmt.setDouble(7, mdbrs.getDouble("DE_DE_START_DIST_NUMB"));
                    cstmt.setDouble(8, mdbrs.getDouble("DE_DE_END_DIST_NUMB"));
                    cstmt.setDate(9, pfDate);

                    //if(mdbrs.getString("GH3_DRF_QTY") == ""){
                    //      cstmt.setString(3, "GH3_DRF_QTY");
                    //}else{
                    //      cstmt.setString(3, mdbrs.getString("GH3_DRF_QTY"));
                    //}               
                    cstmt.setDouble(10, 1646.090);
                    cstmt.setString(11, "String");
                    cstmt.setString(12, "GH3_DE_DRF_NUMB");
                    cstmt.addBatch();
                }
            }
            try {
                con.setAutoCommit(false);
                //CallableStatement cstmt1 = con.prepareCall("{call DELETE_bodematconfirmationtemp()}");
                //cstmt1.execute();
                //cstmt1.close();
                System.err.println("database 113");
                int[] updateCounts = cstmt.executeBatch();
                cstmt.close();
                cstmt = con.prepareCall("{call ADD_IMPORT(?, ?, ?, ?, ?, ?, ?)}");
                cstmt.setString(1, from);
                cstmt.setString(2, to);
                cstmt.setString(3, tblName);
                cstmt.setString(4, comments);
                cstmt.setString(5, status);
                sdf.applyPattern("yyyy-MM-dd");
                java.sql.Date ed = java.sql.Date.valueOf(sdf.format(entryDate));
                cstmt.setDate(6, ed);
                cstmt.setDate(7, pfDate);
                cstmt.execute();
                con.commit();
                con.setAutoCommit(true);
            } catch (SQLException exSQL) {
                System.err.println(exSQL);
                returnStr = "ImportFailed";

            }
            cstmt.close();
            mdbrs.close();
            s.close(); // close the Statement to let the database know we're done with it
            conn.close(); // close the Connection to let the database know we're done with it
        } catch (ClassNotFoundException cnf) {
            System.err.println("database 114");
            returnStr = "ConnectionError";
            //System.out.print("k="+returnStr);
        } catch (Exception sqle) {
            sqle.printStackTrace();
            //if (sqle.getErrorCode()==-1305) {
            //    returnStr="TableNotFound-"+tblName;
            //}else{
            returnStr = "ImportFailed";
            // System.out.println("kamruzzaman");
            //}
        }
        return returnStr;
    }
    //********************************************************************************************************/

    public String importBOHolding(String from, String to, String tblName, String comments, String status, Date entryDate, String filePath) throws Exception {

        String returnStr = "";
        try {
            /*
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            String filename = filePath;
            String database = "jdbc:odbc:Driver={Microsoft Access Driver (*.mdb)};DBQ=";
            database+= filename.trim() + ";DriverID=22;READONLY=true}"; // add on to the end
                
             */

 /* 
                Md. Zahidul Islam 2017 July 07 
             */
            Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
            String filename = filePath;
            String database = "jdbc:ucanaccess://" + filename.trim();

            //  Connection from the DriverManager
            Connection conn = DriverManager.getConnection(database, "", "");
            // Creating a java.sql.Statement to run queries
            Statement s = conn.createStatement();
            s.execute("select * from " + tblName); // selecting  data from the table
            ResultSet mdbrs = s.getResultSet(); // get any ResultSet that came from our query
            Date dt = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat simD = new SimpleDateFormat("dd-MMM-yyyy");
            java.sql.Date pfDate = java.sql.Date.valueOf(sdf.format(dt));
            CallableStatement cstmt = con.prepareCall("{call ADD_BOHOLDING(?, ?, ?, ?)}");
            if (mdbrs != null) // if rs == null, then there is no ResultSet to view
            {
                while (mdbrs.next()) // this will step  through  data row-by-row
                {
                    Date tempDate = null;
                    tempDate = simD.parse(mdbrs.getString("PF_BDATE"));
                    pfDate = java.sql.Date.valueOf(sdf.format(tempDate));
                    cstmt.setDouble(1, mdbrs.getDouble("DE_CURRBAL"));
                    cstmt.setString(2, mdbrs.getString("DE_BOID"));
                    cstmt.setString(3, mdbrs.getString("DE_BOSHORTNAME"));
                    cstmt.setDate(4, pfDate);
                    cstmt.addBatch();
                }
            }
            try {
                con.setAutoCommit(false);
                CallableStatement cstmt1 = con.prepareCall("{call DELETE_BOHOLDING()}");
                cstmt1.execute();
                cstmt1.close();
                int[] updateCounts = cstmt.executeBatch();
                cstmt.close();
                cstmt = con.prepareCall("{call ADD_IMPORT(?, ?, ?, ?, ?, ?, ?)}");
                cstmt.setString(1, from);
                cstmt.setString(2, to);
                cstmt.setString(3, tblName);
                cstmt.setString(4, comments);
                cstmt.setString(5, status);
                sdf.applyPattern("yyyy-MM-dd");
                java.sql.Date ed = java.sql.Date.valueOf(sdf.format(entryDate));
                cstmt.setDate(6, ed);
                cstmt.setDate(7, pfDate);
                cstmt.execute();
                con.commit();
                con.setAutoCommit(true);
            } catch (SQLException exSQL) {
                returnStr = "ImportFailed";
            }
            cstmt.close();
            mdbrs.close();
            s.close(); // close the Statement to let the database know we're done with it
            conn.close(); // close the Connection to let the database know we're done with it
            //return returnStr;
        } catch (ClassNotFoundException cnf) {
            returnStr = "ConnectionError";
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            if (sqle.getErrorCode() == -1305) {
                returnStr = "TableNotFound." + tblName;
            } else {
                returnStr = "ImportFailed";
            }

        }
        return returnStr;
    }

    //////////////////// Maruf
    //public String importBOAddressForXLS(String textfileName, String tblName,  Date entryDate, String filePath) throws Exception,NumberFormatException{//07.01.2012 jahir
    public String importBOAddressForXLS(BufferedReader readerText1, String tblName, Date entryDate, String filePath) throws Exception, NumberFormatException {
        //System.out.println("Aman ---------->0");
        String returnstr = "";
        //System.out.println("-------------textfileName1------->"+textfileName+"<----------filePath------->"+filePath+"<----------tblName------->"+tblName);
        try {

            //////////////  Database Connection...................
            Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");

            CallableStatement cstmt1 = con.prepareCall("{call DELETE_BOADDRESSTEMP()}");
            cstmt1.execute();
            cstmt1.close();

            String filename = filePath;
            Date dt = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat simD = new SimpleDateFormat("dd-MMM-yyyy");
            java.sql.Date pfDate = java.sql.Date.valueOf(sdf.format(dt));
            CallableStatement cstmt = con.prepareCall("{call ADD_BOHOLDING(?, ?, ?, ?)}");

            //BufferedReader readerText = new BufferedReader(new FileReader(textfileName));//07.01.2012 jahir
            BufferedReader readerText = readerText1;
            String line = null;
            Boolean flag = false;
            java.sql.Date ed = java.sql.Date.valueOf(sdf.format(entryDate));
            int count = 0;
            while ((line = readerText.readLine()) != null) {
                String[] valueOfShareHolder = new String[50];
                String splitletter = "~";
                valueOfShareHolder = line.split(splitletter);

                count++;
                //////////// ShareHolder Name
                valueOfShareHolder[3] = valueOfShareHolder[3].replaceAll("'", " ");

                //03.06.2013 Jahir
                //Father Name
                String FatherName = "";
                //FatherName=valueOfShareHolder[22]; 30.06.2014
                FatherName = valueOfShareHolder[21];
                if (FatherName.equalsIgnoreCase(null)) {
                    FatherName = "";
                }
                FatherName = FatherName.replaceAll("'", " ");
                //System.out.println("-------------FatherName----------"+FatherName+"-----");
                //Mother Name
                String MotherName = "";
                //MotherName=valueOfShareHolder[23]; 30.06.2014
                MotherName = valueOfShareHolder[22];
                if (MotherName.equalsIgnoreCase(null)) {
                    MotherName = "";
                }
                MotherName = MotherName.replaceAll("'", " ");
                //System.out.println("-------------MotherName----------"+MotherName+"-----");
                //End 03.06.2013 Jahir

                /////////////////  Address Concated
                String Address = "";
                //Address = valueOfShareHolder[24] + valueOfShareHolder[25] + valueOfShareHolder[26]; 30.06.2014
                Address = valueOfShareHolder[23] + valueOfShareHolder[24] + valueOfShareHolder[25];
                Address = Address.replaceAll("'", " ");

                /////////////////  Phone Concated
                String Phone = "";
                /* 30.06.2014
                  if(valueOfShareHolder[30]==null)
                     Phone = valueOfShareHolder[31];
                 else if(valueOfShareHolder[31]==null)
                     Phone = valueOfShareHolder[30];

                 else if(valueOfShareHolder[30]!=null && valueOfShareHolder[31]!=null)
                     Phone = valueOfShareHolder[30] +", " +  valueOfShareHolder[31];

                 */

                if (valueOfShareHolder[29] == null) {
                    Phone = valueOfShareHolder[30];
                } else if (valueOfShareHolder[30] == null) {
                    Phone = valueOfShareHolder[29];
                } else if (valueOfShareHolder[30] != null && valueOfShareHolder[29] != null) {
                    Phone = valueOfShareHolder[29] + ", " + valueOfShareHolder[30];
                } else {
                    Phone = "";
                }

                Phone = Phone.replaceAll("'", " ");
                /*
                 valueOfShareHolder[27] = valueOfShareHolder[27].replaceAll("'"," ");
                 valueOfShareHolder[28] = valueOfShareHolder[28].replaceAll("'"," ");
                 valueOfShareHolder[29] = valueOfShareHolder[29].replaceAll("'"," ");
                 valueOfShareHolder[35] = valueOfShareHolder[35].replaceAll("'"," ");
                 valueOfShareHolder[36] = valueOfShareHolder[36].replaceAll("'"," ");
                 valueOfShareHolder[37] = valueOfShareHolder[37].replaceAll("'"," ");
                 */
                valueOfShareHolder[26] = valueOfShareHolder[26].replaceAll("'", " ");
                valueOfShareHolder[27] = valueOfShareHolder[27].replaceAll("'", " ");
                valueOfShareHolder[28] = valueOfShareHolder[28].replaceAll("'", " ");
                valueOfShareHolder[34] = valueOfShareHolder[34].replaceAll("'", " ");
                valueOfShareHolder[35] = valueOfShareHolder[35].replaceAll("'", " ");
                valueOfShareHolder[36] = valueOfShareHolder[36].replaceAll("'", " ");
                valueOfShareHolder[37] = valueOfShareHolder[37].replaceAll("'", " "); // routing number

                String national = "Bangladeshi";

                /*
                                   String insertQuery = "call ADD_BOADDRESSTEMP('"+valueOfShareHolder[2]+"','"+valueOfShareHolder[3]+"','"+"','"+Address+"','"+valueOfShareHolder[27]+"','"+valueOfShareHolder[27]+"','"
                                                       +valueOfShareHolder[29]+"','"+valueOfShareHolder[28]+"','"+Phone+"','','',to_date('"+ed+"', 'yyyy/mm/dd'),"+0.0+",'T','F','T','"+national+"','A','"+valueOfShareHolder[35]+"','"
                                                       +valueOfShareHolder[36]+"','"+valueOfShareHolder[37]+"','"+FatherName+"','"+MotherName+"')";
// with father's name and mother's name
 String insertQuery = "call ADD_BOADDRESSTEMP('"+valueOfShareHolder[2]+"','"+valueOfShareHolder[3]+"','"+"','"+Address+"','"+valueOfShareHolder[26]+"','"+valueOfShareHolder[26]+"','"
                                       +valueOfShareHolder[28]+"','"+valueOfShareHolder[27]+"','"+Phone+"','','',to_date('"+ed+"', 'yyyy/mm/dd'),"+0.0+",'T','F','T','"+national+"','A','"+valueOfShareHolder[34]+"','"
                                       +valueOfShareHolder[35]+"','"+valueOfShareHolder[36]+"','"+FatherName+"','"+MotherName+"')";


                 */
                String insertQuery = "call ADD_BOADDRESSTEMP('" + valueOfShareHolder[2] + "','" + valueOfShareHolder[3] + "','" + "','" + Address + "','" + valueOfShareHolder[26] + "','" + valueOfShareHolder[26] + "','"
                        + valueOfShareHolder[28] + "','" + valueOfShareHolder[27] + "','" + Phone + "','','',to_date('" + ed + "', 'yyyy/mm/dd')," + 0.0 + ",'T','F','T','" + national + "','A','" + valueOfShareHolder[34] + "','"
                        + valueOfShareHolder[35] + "','" + valueOfShareHolder[36] + "','" + valueOfShareHolder[37] + "')";

                try {
                    int b = insertRow(insertQuery);
                } catch (Exception e) {
                    System.out.println(" Problem for insert data in BO ADDRESS TEMP");
                }

            }
        } catch (Exception e) {
            System.out.println("importBOAddressForXLS---------->File Data read problem");
            e.printStackTrace();
        }
        return returnstr;
    }
    /////////////////// End maruf

    public String importBOAddress(String from, String to, String tblName, String comments, String status, Date entryDate, String filePath) throws Exception {
        System.out.println("Aman ---------->1");
        String returnStr = "";

        try {
            /*
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            String filename = filePath;
            String database = "jdbc:odbc:Driver={Microsoft Access Driver (*.mdb)};DBQ=";
            database+= filename.trim() + ";DriverID=22;READONLY=true}"; // add on to the end
             */

 /* 
                Md. Zahidul Islam 2017 July 07 
             */
            Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
            String filename = filePath;
            String database = "jdbc:ucanaccess://" + filename.trim();

            //  Connection from the DriverManager
            Connection conn = DriverManager.getConnection(database, "", "");
            // Creating a java.sql.Statement to run queries
            Statement s = conn.createStatement();
            s.execute("select * from " + tblName); // selecting  data from the table

            ResultSet mdbrs = s.getResultSet(); // get any ResultSet that came from our query
            Date dt = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            java.sql.Date pfDate = java.sql.Date.valueOf(sdf.format(dt));
            String pfDtStr = "";

            int count = 0;
            int totlal = 0;

            CallableStatement cstmt1 = con.prepareCall("{call DELETE_BOADDRESSTEMP()}");
            cstmt1.execute();
            cstmt1.close();

            CallableStatement cstmt = con.prepareCall("{call ADD_BOADDRESSTEMP(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            if (mdbrs != null) // if rs == null, then there is no ResultSet to view
            {
                while (mdbrs.next()) { // this will step  through  data row-by-row

                    Date tempDate = new Date(mdbrs.getString("PF_BDATE"));
                    pfDate = java.sql.Date.valueOf(sdf.format(tempDate));

                    // System.out.println("------ pfDate --------" + pfDate);
                    /////////////  Get data
                    String DE_BO_ID = mdbrs.getString("DE_BO_ID");
                    System.out.println("------ DE_BO_ID --------" + DE_BO_ID);
                    String DE_Name_FirstHolder = mdbrs.getString(
                            "DE_Name_FirstHolder");
                    //System.out.println("------ DE_Name_FirstHolder --------"+DE_Name_FirstHolder);
                    String DE_Contact_Person_Name = mdbrs.getString(
                            "DE_Contact_Person_Name");
                    //System.out.println("------ DE_Contact_Person_Name --------"+DE_Contact_Person_Name);
                    String DE_Address = mdbrs.getString("DE_Address");
                    //System.out.println("------ DE_Address --------"+DE_Address);
                    String DE_City = mdbrs.getString("DE_City");
                    // System.out.println("------ DE_City --------"+DE_City);
                    String DE_State = mdbrs.getString("DE_State");
                    // System.out.println("------ DE_State --------"+DE_State);
                    String DE_Postal_Code = mdbrs.getString("DE_Postal_Code");
                    //System.out.println("------ DE_Postal_Code --------"+DE_Postal_Code);
                    String DE_Country = mdbrs.getString("DE_Country");
                    //System.out.println("------ DE_Country --------"+DE_Country);
                    String DE_Phone = mdbrs.getString("DE_Phone");
                    //System.out.println("------ DE_Phone --------"+DE_Phone);
                    String DE_Fax = mdbrs.getString("DE_Fax");
                    //System.out.println("------ DE_Fax --------"+DE_Fax);
                    String DE_BO_Email = mdbrs.getString("DE_BO_Email");
                    // System.out.println("------ DE_BO_Email --------"+DE_BO_Email);
                    //// String pfDateN
                    Double tax_free_share = 0.0;
                    String LIABLETOTAX = "T";
                    String SPECIALTAX = "F";
                    String RESIDENT = "T";
                    String NATIONALITY = "Bangladeshi";
                    String BOCLASS = "A";

                    DE_Name_FirstHolder = replace(DE_Name_FirstHolder, "'", " ");
                    // this.replaceSingleQuote()
                    DE_Contact_Person_Name = replace(DE_Contact_Person_Name,
                            "'", " ");
                    DE_Address = replace(DE_Address, "'", "");
                    DE_City = replace(DE_City, "'", "");
                    DE_State = replace(DE_State, "'", "");
                    DE_Postal_Code = replace(DE_Postal_Code, "'", "");
                    DE_Country = replace(DE_Country, "'", "");
                    DE_Phone = replace(DE_Phone, "'", "");
                    DE_Fax = replace(DE_Fax, "'", "");
                    DE_BO_Email = replace(DE_BO_Email, "'", "");

                    /* String add_query_temp="call ADD_BOADDRESSTEMP('"+DE_BO_ID+"','"+DE_Name_FirstHolder+"','"+DE_Contact_Person_Name+"','"+DE_Address+"','"+DE_City+"','"+DE_State+"','"+DE_Postal_Code+"','"+DE_Country+"','"+DE_Phone+"','"+DE_Fax+"','"+DE_BO_Email+"',TO_DATE('"+pfDate+"','yyyy-MM-dd'),"+tax_free_share+",'"+LIABLETOTAX+"','"+SPECIALTAX+"','"+RESIDENT+"','"+NATIONALITY+"','"+BOCLASS+"')";
                     //System.out.println("add_query_temp-"+count+"="+add_query_temp);

                                 try
                                 {
                        procedureExecute(add_query_temp);
                                 }
                                 catch(Exception e)
                                 {
                        System.out.print("---error------->"+e);
                                 }*/
                    // System.out.println("------ the de-bo-id --------"+mdbrs.getString("DE_BO_ID"));
                    //System.out.println("------ the DE_Name_FirstHolder --------"+mdbrs.getString("DE_Name_FirstHolder"));

                    /* cstmt.setString(1,mdbrs.getString("DE_BO_ID"));
                     System.out.println("------ the de-bo-id1 --------"+mdbrs.getString("DE_BO_ID"));

                     cstmt.setString(2,mdbrs.getString("DE_Name_FirstHolder"));
                     System.out.println("------ the DE_Name_FirstHolder1 --------"+mdbrs.getString("DE_Name_FirstHolder"));
                     cstmt.setString(3, mdbrs.getString("DE_Contact_Person_Name"));
                     System.out.println("------ the DE_Contact_Person_Name --------"+mdbrs.getString("DE_Contact_Person_Name"));
                     cstmt.setString(4, mdbrs.getString("DE_Address"));
                     System.out.println("------ the DE_Address --------"+mdbrs.getString("DE_Address"));
                     cstmt.setString(5, mdbrs.getString("DE_City"));
                     System.out.println("------ the DE_City --------"+mdbrs.getString("DE_City"));
                     cstmt.setString(6, mdbrs.getString("DE_State"));
                     System.out.println("------ the DE_State --------"+mdbrs.getString("DE_State"));
                     cstmt.setString(7, mdbrs.getString("DE_Postal_Code"));
                     System.out.println("------ the de-bo-id --------"+mdbrs.getString("DE_Postal_Code"));
                     cstmt.setString(8, mdbrs.getString("DE_Country"));
                     System.out.println("------ the DE_Country --------"+mdbrs.getString("DE_Country"));
                     cstmt.setString(9, mdbrs.getString("DE_Phone"));
                     System.out.println("------ the DE_Phone --------"+mdbrs.getString("DE_Phone"));
                     cstmt.setString(10, mdbrs.getString("DE_Fax"));
                     System.out.println("------ the DE_Fax --------"+mdbrs.getString("DE_Fax"));
                     cstmt.setString(11, mdbrs.getString("DE_BO_Email"));
                     System.out.println("------ the DE_BO_Email --------"+mdbrs.getString("DE_BO_Email"));

                     cstmt.setDate  (12, pfDate);
                     cstmt.setDouble(13,0);
                     cstmt.setString(14,"T");
                     cstmt.setString(15, "F");
                     cstmt.setString(16, "T");
                     cstmt.setString(17, "Bangladeshi");
                     cstmt.setString(18, "A");

                     cstmt.addBatch();*/
                    cstmt.setString(1, DE_BO_ID);

                    cstmt.setString(2, DE_Name_FirstHolder);

                    cstmt.setString(3, DE_Contact_Person_Name);

                    cstmt.setString(4, DE_Address);

                    cstmt.setString(5, DE_City);

                    cstmt.setString(6, DE_State);

                    cstmt.setString(7, DE_Postal_Code);

                    cstmt.setString(8, DE_Country);

                    cstmt.setString(9, DE_Phone);

                    cstmt.setString(10, DE_Fax);

                    cstmt.setString(11, DE_BO_Email);

                    cstmt.setDate(12, pfDate);
                    cstmt.setDouble(13, 0);
                    cstmt.setString(14, "T");
                    cstmt.setString(15, "F");
                    cstmt.setString(16, "T");
                    cstmt.setString(17, "Bangladeshi");
                    cstmt.setString(18, "A");

                    cstmt.addBatch();

                    System.out.println("-------- count --------" + count);

                    if (count >= 1000) {
                        try {
                            con.setAutoCommit(false);
                            //CallableStatement cstmt1 = con.prepareCall("{call DELETE_BOADDRESSTEMP()}");
                            // cstmt1.execute();
                            // cstmt1.close();
                            int[] updateCounts = cstmt.executeBatch();
                            cstmt.close();
                            cstmt = con.prepareCall(
                                    "{call ADD_IMPORT(?, ?, ?, ?, ?, ?, ?)}");
                            cstmt.setString(1, from);
                            cstmt.setString(2, to);
                            cstmt.setString(3, tblName);
                            cstmt.setString(4, comments);
                            cstmt.setString(5, status);
                            sdf.applyPattern("yyyy-MM-dd");
                            java.sql.Date ed = java.sql.Date.valueOf(sdf.format(
                                    entryDate));
                            cstmt.setDate(6, ed);
                            cstmt.setDate(7, pfDate);
                            cstmt.execute();
                            con.commit();
                            con.setAutoCommit(true);
                            count = 0;
                            cstmt = con.prepareCall(
                                    "{call ADD_BOADDRESSTEMP(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                        } catch (SQLException exSQL) {
                            //con.setAutoCommit(true);
                            //exSQL.printStackTrace();
                            returnStr = "ImportFailed";
                            System.out.println("----********************---");
                        }

                    }
                    count++;
                }

                if (count != 0) {
                    try {
                        con.setAutoCommit(false);
                        //CallableStatement cstmt1 = con.prepareCall("{call DELETE_BOADDRESSTEMP()}");
                        // cstmt1.execute();
                        // cstmt1.close();
                        int[] updateCounts = cstmt.executeBatch();
                        cstmt.close();
                        cstmt = con.prepareCall(
                                "{call ADD_IMPORT(?, ?, ?, ?, ?, ?, ?)}");
                        cstmt.setString(1, from);
                        cstmt.setString(2, to);
                        cstmt.setString(3, tblName);
                        cstmt.setString(4, comments);
                        cstmt.setString(5, status);
                        sdf.applyPattern("yyyy-MM-dd");
                        java.sql.Date ed = java.sql.Date.valueOf(sdf.format(
                                entryDate));
                        cstmt.setDate(6, ed);
                        cstmt.setDate(7, pfDate);
                        cstmt.execute();
                        con.commit();
                        con.setAutoCommit(true);
                        count = 0;
                        cstmt = con.prepareCall(
                                "{call ADD_BOADDRESSTEMP(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                    } catch (SQLException exSQL) {
                        //con.setAutoCommit(true);
                        //exSQL.printStackTrace();
                        returnStr = "";
                        System.out.println("----********************---");
                    }

                }
                //System.out.println("in the function - 7." + count);
                cstmt.close();
                mdbrs.close();
                s.close(); // close the Statement to let the database know we're done with it
                conn.close(); // close the Connection to let the database know we're done with it
                //return returnStr;
            }
        } catch (ClassNotFoundException cnf) {
            //cnf.printStackTrace();
            //System.out.println("Could not find class: " + cnf);
            returnStr = "ConnectionError";
        } catch (SQLException sqle) {
            if (sqle.getErrorCode() == -1305) {
                returnStr = "TableNotFound." + tblName;
            } else {
                System.out.println("----=========================---" + sqle);
                returnStr = "ImportFailed";
            }

        }
        System.out.println("----rtn value---" + returnStr);

        return returnStr;
    }

    public String LastCertificate() throws Exception {
        String qcert = "SELECT * FROM STATUS_VIEW";
        queryExecute(qcert);
        toNext();
        String Slastcertificate = getColumnS("CERTIFICATE_NO");

        String qlastcer = "SELECT MAX(to_Number(CERTIFICATE_NO)) as CERTIFICATENO1 FROM STATUS_VIEW";
        queryExecute(qlastcer);
        toNext();
        int lastcertificate = getColumnI("CERTIFICATENO1");
        if (String.valueOf(lastcertificate).equals("null")) {
            lastcertificate = 0;
        }
        lastcertificate++;
        String newcert = String.valueOf(lastcertificate);
        if (newcert.length() < 9) {
            int lendiff = 9 - newcert.length();
            for (int ii = 0; ii < lendiff; ii++) {
                newcert = "0" + newcert;
            }
        }

        String upcert = "call UPDATE_STATUS_CERTIFICATE('" + Slastcertificate + "', '" + newcert + "')";
        boolean b = procedureExecute(upcert);

        return newcert;
    }

    public String LastWarrant() throws Exception {
        String qwar = "SELECT * FROM STATUS_VIEW";
        queryExecute(qwar);
        toNext();
        String Slastwarrant = getColumnS("WARRANT_NO");

        String qlastwar1 = "SELECT MAX(to_Number(WARRANT_NO)) as WARRANTNO1 FROM STATUS_VIEW";
        queryExecute(qlastwar1);
        toNext();
        int lastwarrant1 = getColumnI("WARRANTNO1");
        lastwarrant1++;

        String newwarr = String.valueOf(lastwarrant1);

        String upwar = "call UPDATE_STATUS_WARRANT('" + Slastwarrant + "', '" + newwarr + "')";
        boolean b = procedureExecute(upwar);

        return newwarr;
    }

    public int LastDistinction() throws Exception {
        String qlastdist1 = "SELECT MAX(DIST_TO) as DISTTO1 FROM SCRIPTS_VIEW";
        queryExecute(qlastdist1);
        toNext();
        int lastdist1 = getColumnI("DISTTO1");
        lastdist1++;

        return lastdist1;
    }

    public void viewBlob(String path, String ID) throws Exception {
        Blob blob;
        System.out.println(path);
        rs = stmt.executeQuery("SELECT sign FROM Signature WHERE SIG_ID ='" + ID + "'");
        while (rs.next()) {
            blob = rs.getBlob("sign");
            byte[] image1 = blob.getBytes(1, (int) blob.length());
            FileOutputStream fos = new FileOutputStream(path);
            fos.write(image1);
        }
    }

    public void InsertBlob(File sfile, String name, String rank, String ID) throws Exception {
        BLOB blob;
        PreparedStatement pstmt;
        try {
            pstmt = con.prepareStatement("INSERT INTO Signature VALUES( ?, ?, ?, empty_blob() )");
            pstmt.setString(1, ID);
            pstmt.setString(2, name);
            pstmt.setString(3, rank);
            pstmt.execute();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        pstmt = con.prepareStatement("SELECT * FROM Signature where SIG_ID = ? for update");
        pstmt.setString(1, ID);
        con.setAutoCommit(false);

        ResultSet rset = pstmt.executeQuery();
        rset.next();

        //Use the OracleDriver resultset, we take the blob locator
        blob = ((OracleResultSet) rset).getBLOB("sign");
        OutputStream os = blob.getBinaryOutputStream();

        //Read the file by chuncks and insert them in the Blob. The chunk size come from the blob
        byte[] chunk = new byte[blob.getChunkSize()];
        int i = -1;

        FileInputStream is = new FileInputStream(sfile);
        while ((i = is.read(chunk)) != -1) {
            os.write(chunk, 0, i); //Write the chunk
            System.out.print('.'); // print progression
        }

        is.close();
        os.close();
        //Close the statement and commit
        pstmt.close();
        con.commit();
    }

    public static byte[] getPhoto(java.sql.Connection conn, String querystring) throws Exception, SQLException {
        String req = "";
        Blob img = null;
        byte[] imgData = null;

        Statement stmt = conn.createStatement();
        // Query
        req = querystring;
        ResultSet rset = stmt.executeQuery(req);
        while (rset.next()) {
            img = rset.getBlob(1);
            imgData = img.getBytes(1, (int) img.length());
        }
        rset.close();
        stmt.close();
        return imgData;
    }
}
