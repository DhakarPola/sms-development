/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batbsms;

/**
 *
 * @author User
 */
public class ConfigurationModel {
    private int unsuccessfulTry;                                                    // 10 times
    private int sessionTimeOut;                                                     // 20 minute
    private int inactivityTime;                                                     // 45 days 
    private int passwordChange;                                                     // 90 days 
    private int passwordChangeHistory;                                              // 10 times
    private boolean passwordComplexitySpeical;                                      // true false 
    private boolean passwordComplexityCapital;
    private boolean passwordComplexityNumber; 
    private int passwordLengthAdmin;
    private int passwordLengthUser; 

    public int getUnsuccessfulTry() {
        return unsuccessfulTry;
    }

    public void setUnsuccessfulTry(int unsuccessfulTry) {
        this.unsuccessfulTry = unsuccessfulTry;
    }

    public int getSessionTimeOut() {
        return sessionTimeOut;
    }

    public void setSessionTimeOut(int sessionTimeOut) {
        this.sessionTimeOut = sessionTimeOut;
    }

    public int getInactivityTime() {
        return inactivityTime;
    }

    public void setInactivityTime(int inactivityTime) {
        this.inactivityTime = inactivityTime;
    }

    public int getPasswordChange() {
        return passwordChange;
    }

    public void setPasswordChange(int passwordChange) {
        this.passwordChange = passwordChange;
    }

    public int getPasswordChangeHistory() {
        return passwordChangeHistory;
    }

    public void setPasswordChangeHistory(int passwordChangeHistory) {
        this.passwordChangeHistory = passwordChangeHistory;
    }

    public boolean isPasswordComplexitySpeical() {
        return passwordComplexitySpeical;
    }

    public void setPasswordComplexitySpeical(boolean passwordComplexitySpeical) {
        this.passwordComplexitySpeical = passwordComplexitySpeical;
    }

    public boolean isPasswordComplexityCapital() {
        return passwordComplexityCapital;
    }

    public void setPasswordComplexityCapital(boolean passwordComplexityCapital) {
        this.passwordComplexityCapital = passwordComplexityCapital;
    }

    public boolean isPasswordComplexityNumber() {
        return passwordComplexityNumber;
    }

    public void setPasswordComplexityNumber(boolean passwordComplexityNumber) {
        this.passwordComplexityNumber = passwordComplexityNumber;
    }

    public int getPasswordLengthAdmin() {
        return passwordLengthAdmin;
    }

    public void setPasswordLengthAdmin(int passwordLengthAdmin) {
        this.passwordLengthAdmin = passwordLengthAdmin;
    }

    public int getPasswordLengthUser() {
        return passwordLengthUser;
    }

    public void setPasswordLengthUser(int passwordLengthUser) {
        this.passwordLengthUser = passwordLengthUser;
    }
        
}
