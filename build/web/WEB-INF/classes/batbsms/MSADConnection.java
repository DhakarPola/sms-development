package batbsms;
/*
LDAP code for accessing the Active Directory
*/
import javax.naming.*;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Collections;
import javax.naming.directory.*;
import java.io.*;
import java.net.ConnectException;

public class MSADConnection {
  public String AllUserGroup="";
  public String HDSupportGroup="";
  public String Level2UserGroup="";
  public String Level3UserGroup="";
  public int UserLevel=0;
  public boolean isNetworkExist=false;

  static String LDAP_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
  private String HOST;
  private String USER_DN;
  private String BASE_DN;
  private String DN;
  private String user;
  private String password;
  static InitialDirContext ctx = null;


  public MSADConnection() {
    try {

      // Open a file of the given name.
      File file = new File("batsms.ini");

      // Get the size of the opened file.
      int size = (int)file.length();

      // Set to zero a counter for counting the number of
      // characters that have been read from the file.
      int chars_read = 0;

      // Create an input reader based on the file, so we can read its data.
      FileReader in = new FileReader(file);

      // Create a character array of the size of the file,
      // to use as a data buffer, into which we will read
      // the text data.
      char[] data = new char[size];

      // Read all available characters into the buffer.
      while(in.ready()) {
        // Increment the count for each character read,
        // and accumulate them in the data buffer.
        chars_read += in.read(data, chars_read, size - chars_read);
      }
      in.close();
      String info = new String(data, 0, chars_read);
      info = info.substring(info.indexOf("\n"), info.length());
      String[] values = info.split(",");

      HOST = "ldap://"+values[0].substring(values[0].indexOf("=") + 1, values[0].length());
      BASE_DN=values[1].substring(values[1].indexOf("=") + 1, values[1].length());

      //USER_DN = ",cn=users,dc="+BASE_DN;
      user = values[2].substring(values[2].indexOf("=") + 1, values[2].length());

      //DN = "cn="+user+",cn=users,dc="+BASE_DN;
      password = values[3].substring(values[3].indexOf("=") + 1, values[3].length());

    }
     catch(FileNotFoundException e) {
       //System.out.println("File not found.");
    }
     catch(IOException e) {
       //System.out.println("IO exception occured while reading file.");
    }
  }

  public boolean connect(){
    //System.out.println("-----------------Host="+HOST);
    Hashtable env = new Hashtable();
    env.put(Context.INITIAL_CONTEXT_FACTORY, LDAP_FACTORY);
    env.put(Context.PROVIDER_URL, HOST );
    env.put(Context.SECURITY_PRINCIPAL, user + "@" + BASE_DN);
    env.put(Context.SECURITY_CREDENTIALS, password);

    boolean valid = false;
    try{
      ctx = new InitialDirContext(env);
//      System.out.println("Administrator authenticated successfully");
      valid = true;
      isNetworkExist=true;
    } catch (CommunicationException ce) {
      valid = false;
      isNetworkExist=false;
    } catch (AuthenticationException ae) {
      valid = false;
      isNetworkExist=true;
    } catch (NamingException ne) {
      valid = false;
      isNetworkExist=true;
    }finally{
      return valid;
    }
  }

  private Vector member;
  private Vector groups;

  public Vector getMembers(String ListType){

    if (ListType.equals("Group"))
      return groups;
    else
      return member;
  }

  public boolean loadMembers() {
    boolean loaded = false;
    member = new Vector();
      try{
        member=getUsers();
        Collections.sort(member);
         if(!member.isEmpty()){
           loaded = true;
         }
     }catch(Exception e){
       loaded = false;
     }
     return loaded;
   }

   public boolean loadGroups() {
     boolean loaded = false;
     groups = new Vector();

       try{
         groups=getGroups();
         Collections.sort(groups);
          if(!groups.isEmpty()){
            loaded = true;
          }
      }catch(Exception e){
        loaded = false;
      }
      return loaded;
   }

   public boolean isValidUser(String username, String password){
     boolean valid = false;
     InitialDirContext ctx = null;

     try {
         Hashtable env = new Hashtable();

         env.put(Context.INITIAL_CONTEXT_FACTORY, LDAP_FACTORY);
         env.put(Context.PROVIDER_URL, HOST );
         env.put(Context.SECURITY_PRINCIPAL, username + "@" + BASE_DN);
         env.put(Context.SECURITY_CREDENTIALS, password);

         ctx = new InitialDirContext(env);

         if (ctx != null) {
//          System.out.println(username+" is a Valid User");
           valid = true;
         }
         ctx.close();

       } catch(AuthenticationException aex) {

       } catch(NamingException nex) {
        nex.printStackTrace();
       valid = false;
     }finally {
     return valid;
   }

  }

  public String get(String need, String user)throws NamingException,NullPointerException{

      String DN="";
      String vals = "";

      SearchControls constraints = new SearchControls();
      constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
      constraints.setReturningAttributes(new String[]{"memberOf"});
      String filter = "(sAMAccountName=" + user + ")";

      String[] BASE_DN_Arr=BASE_DN.split("[.]");
      if (BASE_DN.indexOf(".")==-1) {
        DN= "DC="+BASE_DN;
      } else {
        for(int i=0;i<BASE_DN_Arr.length;i++) {
          if (DN.equals(""))
            DN ="DC=" + BASE_DN_Arr[i];
          else
            DN =DN + ",DC=" + BASE_DN_Arr[i];
        }
      }

      NamingEnumeration results = ctx.search(DN,filter, constraints);

      try{
        while (results != null && results.hasMore()) {
          SearchResult si = (SearchResult) results.next();
          Attributes attrs = si.getAttributes();
          String temp;
          if (attrs == null) {
            //System.out.println("No attributes");
          }
          else {
            for (NamingEnumeration ne = attrs.getAll(); ne.hasMore(); ) {
              Attribute at = (Attribute) ne.next();
              String attrID = at.getID();
              if (attrID.equalsIgnoreCase(need)) {
                NamingEnumeration e = at.getAll();
                while (e != null && e.hasMore()) {
                  temp = e.next().toString();
                  if (temp != null) {
                    vals += temp + "~";
                    if (!need.equals("memberOf")) {
                      if (!vals.equals("")) {
                        vals = vals.substring(0, vals.length() - 1);
                      }
                      return vals;
                    }
                  }
                }
                if (!vals.equals("")) {
                  vals = vals.substring(0, vals.length() - 1);
                  return vals;
                }
              }
            }
          }

        }
        return vals;
      } catch(NamingException ne){
       //ne.printStackTrace();
       return "";
     }

  }

//For closing connection
  public void takeDown() throws Exception
   {
      ctx.close();
      ctx = null;
   }

   /**
    * Get all users from the authentication service
    */
   public Vector getUsers() throws ConnectException {
     String DN="";
     Vector uvec = new Vector();
     String[] users = null;
     String cn="";
     String AccountName="";

     SearchControls constraints = new SearchControls();
     constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
     constraints.setReturningAttributes(new String[]{"cn","sAMAccountName"});
     String filter = "(&(objectclass=user)(objectcategory=user))";

     String[] BASE_DN_Arr=BASE_DN.split("[.]");
     if (BASE_DN.indexOf(".")==-1) {
       DN= "DC="+BASE_DN;
     } else {
       for(int i=0;i<BASE_DN_Arr.length;i++) {
         if (DN.equals(""))
           DN ="DC=" + BASE_DN_Arr[i];
         else
           DN =DN + ",DC=" + BASE_DN_Arr[i];
       }
     }


     try {
       NamingEnumeration results = ctx.search(DN, filter, constraints);

      // Print the users
      while (results.hasMore()) {
        SearchResult sr = (SearchResult)results.next();
        Attributes attrs = sr.getAttributes();
        cn=attrs.get("cn").toString();
        cn=cn.substring(4,cn.length());
        AccountName=attrs.get("sAMAccountName").toString();
        AccountName=AccountName.substring(16,AccountName.length());
        uvec.add(cn+"~|~"+AccountName);
      }

//       users = new String[uvec.size()];
//       for (int i = 0; i < uvec.size(); i++) {
//         users[i] = (String) uvec.elementAt(i);
//       }

       return uvec;
     }
     catch (NamingException ne) {
       //ne.printStackTrace();
       return uvec;
    }
  }

  public Vector getGroups() throws ConnectException {
    String DN="";
    Vector uvec = new Vector();
    String[] groups = null;
    String cn="";
    String AccountName="";

    SearchControls constraints = new SearchControls();
    constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
    constraints.setReturningAttributes(new String[]{"cn","distinguishedName"});
    String filter = "(&(objectclass=group)(objectcategory=group))";

    String[] BASE_DN_Arr=BASE_DN.split("[.]");
   if (BASE_DN.indexOf(".")==-1) {
     DN= "DC="+BASE_DN;
   } else {
     for(int i=0;i<BASE_DN_Arr.length;i++) {
       if (DN.equals(""))
         DN ="DC=" + BASE_DN_Arr[i];
       else
         DN =DN + ",DC=" + BASE_DN_Arr[i];
     }
   }


    try {
      NamingEnumeration results = ctx.search(DN, filter, constraints);

     // Print the users
     while (results.hasMore()) {
       SearchResult sr = (SearchResult)results.next();
       Attributes attrs = sr.getAttributes();
       cn=attrs.get("cn").toString();
       cn=cn.substring(4,cn.length());
       AccountName=attrs.get("distinguishedName").toString();
       AccountName=AccountName.substring(19,AccountName.length());
       if (AccountName.indexOf("CN=Builtin")==-1)
         uvec.add(cn+"~|~"+AccountName);
     }

//      groups = new String[uvec.size()];
//      for (int i = 0; i < uvec.size(); i++) {
//        groups[i] = (String) uvec.elementAt(i);

      return uvec;
    } catch (NamingException ne) {
      //ne.printStackTrace();
      return uvec;
   }
  }

   /**
   * Get the groups for a particular user from the authentication service
   */
  public String[] getGroupsOfUser(String foruser) throws ConnectException {
    String DN="";

    Vector uvec = new Vector();
    String[] groups = null;

    SearchControls constraints = new SearchControls();
    constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
    constraints.setReturningAttributes(new String[]{"memberOf"});
    String filter = "(sAMAccountName=" + foruser + ")";
//    System.out.println(BASE_DN);
//    System.out.println(BASE_DN_Arr.length);
    String[] BASE_DN_Arr=BASE_DN.split("[.]");
    if (BASE_DN.indexOf(".")==-1) {
      DN= "DC="+BASE_DN;
    } else {
      for(int i=0;i<BASE_DN_Arr.length;i++) {
        if (DN.equals(""))
          DN ="DC=" + BASE_DN_Arr[i];
        else
          DN =DN + ",DC=" + BASE_DN_Arr[i];
      }
    }
//    System.out.println(DN);
    try {
      NamingEnumeration results = ctx.search(DN, filter, constraints);

      while (results != null && results.hasMore()) {
        SearchResult si = (SearchResult) results.next();
        Attributes attrs = si.getAttributes();
        if (attrs == null) {
          //System.out.println("No attributes");
        }
        else {
          for (NamingEnumeration ne = attrs.getAll(); ne.hasMore(); ) {
            Attribute at = (Attribute) ne.next();
            String attrID = at.getID();
            if (attrID.equalsIgnoreCase("memberOf")) {
              NamingEnumeration e = at.getAll();
              groups = new String[uvec.size()];
              while (e != null && e.hasMore()) {
                uvec.addElement(e.next().toString());

              }
            }
          }
        }
      }

      groups = new String[uvec.size()];
      for (int i = 0; i < uvec.size(); i++) {
        groups[i] = (String) uvec.elementAt(i);
      }
      return groups;
    }
    catch (NamingException ne) {
      //ne.printStackTrace();
      return groups;
    }
  }
}
