package batbsms;

import java.io.*;

public class fileDelete
  {
    public static void kill(String nameoffile)
    {
        String fileName= new String(nameoffile);
        File dir=new File(fileName);
        if (!dir.exists())
        {
          System.out.println(fileName + " does not exist");
          return;
        }

        String[] info = dir.list();
        int totalFile=dir.list().length;
        for (int i = 0; i <totalFile; i++)
          {
//          System.out.println(info[i]);
            File n = new File(fileName + dir.separator + info[i]);
            if (!n.isFile())
              { // skip ., .., other directories, etc.
                System.out.println("file: "+n.getName()+" is not a file" );
                continue;
              }
            System.out.println("removing " + n.getPath());
            if (!n.delete())
              {
                System.err.println("Couldn't remove " + n.getPath());
              }
          }

         if(!dir.delete())
          {
            System.out.println("Can't delete the root directory");
          }
     }
  }
