package batbsms;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author : Jahir / Date: 8.12.2012
 * @version 1.0
 */
import java.util.*;
import java.math.*;

public class NumToWords {





    int i=0;
    String string="";
    String st1[] = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven",
                    "Eight", "Nine", };
    //String st2[] = { "Hundred", "Thousand", "Lakh", "Crore" };
    String st2[] = { "Hundred", "Thousand", "Million", "Billion", "Trillion", "Quadrillion", "Quintillion", "Sextillion", "Septillion", "Octillion" };
    String st3[] = { "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen",
                    "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Ninteen", };
    String st4[] = { "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy",
                    "Eighty", "Ninty" };

    public String convert(String num) {
            //System.out.println("----------String for convert----"+num);

            String check="E";
            boolean checked=num.contains(check);
            //System.out.println("---------checked------"+checked);

            double d=0.0;
            long convint = 0l;
            String strnumber = null;

            if(checked==true){
                d = Double.valueOf(num).doubleValue();
                //System.out.println("---------d------"+d);
                convint = Math.round(d);
                strnumber = Long.toString(convint);
                //System.out.println("---------strnumber------"+strnumber);
            }else{
                strnumber = num;
            }


            i=0;
            String valueInWords="";
            String partOne="";
            String partTwo="";
            String[] parts = strnumber.split("\\.");
            partOne=parts[0];
            if(parts.length>1){
                partTwo = parts[1];
                //System.out.println("<------"+"<---parts[1]--->"+parts[1]);
            }
            //System.out.println("----------number for convert----partOne--->"+partOne+"<---And partTwo--->"+partTwo+"<----"+parts.length);
            //if(num.equalsIgnoreCase("00.00"))
            try{
                if (partOne != null) {
                    valueInWords = convertedValue(partOne);
                    if (valueInWords != null && !valueInWords.equalsIgnoreCase(""))
                        valueInWords = valueInWords + " Taka";
                }
                if (!partTwo.equalsIgnoreCase("") && Integer.parseInt(partTwo) > 0)
                    valueInWords = valueInWords + " " + convertedPointValue(partTwo) + " Paisa";
                if (valueInWords != null && !valueInWords.equalsIgnoreCase(""))
                    valueInWords = valueInWords + " Only";
            }
            catch(Exception e){}
            //System.out.println("-------------------------final Convert-------->> "+valueInWords+"-----");

            return valueInWords;
    }


   public String convertedPointValue(String num) {
       //string="";
       if(num.length()==1)
           num=num+"0";

       try{
           string=convertedValue(num);
       }
       catch(Exception e){}
       //System.out.println("<---string--->"+string);
       return string;
   }

   public String convertedValue(String num) {
                //System.out.println("-----------num--num.length()--"+num.length()+"----------num Convert---"+num+"-----");
                //string="";
                try{
                    if (num.length() == 1)
                        string = lengthOne(num);
                    if (num.length() == 2)
                        string = lengthTwo(num);
                    if (num.length() == 3)
                        string = lengthThree(num);
                    if (num.length() > 3 && num.length() < 7)
                        string = lengthGTThreeLTSeven(num);
                    if (num.length() > 6 && num.length() < 10)
                        string = lengthGTSixLTTen(num);
                    if (num.length() > 9 && num.length() < 13)
                        string = lengthGTNineLTTherteen(num);
                    if (num.length() > 12 && num.length() < 16)
                        string = lengthGTTwelveLTSixteen(num);
                    if (num.length() > 15 && num.length() < 19)
                        string = lengthGTFifteenLTNineteen(num);
                    if (num.length() > 18 && num.length() < 22)
                        string = lengthGTEightteenLTTwentyTwo(num);
                    if (num.length() > 21 && num.length() < 25)
                        string = lengthGTTwentyOneLTTwentyFive(num);
                    if (num.length() > 24 && num.length() < 28)
                        string = lengthGTTwentyFourLTTwentyEight(num);
                    if (num.length() > 27 && num.length() < 31)
                        string = lengthGTTwentySevenLTThertyOne(num);
                }
                catch(Exception e){}
                //System.out.println("-------------------convertedValue---"+string+"-----");

            return string;
    }


    public String lengthOne(String num){
        //System.out.println("------lengthOne-----num--"+num);
        String stringValue="";
        try{
            if(Integer.parseInt(num)>0){
                stringValue = st1[Integer.parseInt(num)];
            }
        }
        catch(Exception e){}
        //System.out.println("-----lengthOne------stringValue--"+stringValue);
        return stringValue;
    }

    public String lengthTwo(String num){
        //System.out.println("------lengthTwo-----num--"+num);
        String stringValue="";
        String[] parts = num.split("");
        int num1=Integer.parseInt(parts[1]);
        int num2=Integer.parseInt(parts[2]);
        try{
            if (num1 == 0)
                stringValue = lengthOne(String.valueOf(num2));
            if (num1 == 1)
                stringValue = st3[num2];
            if (num1 > 1)
                stringValue = st4[num1 - 2] + " " + lengthOne(String.valueOf(num2));
        }
        catch(Exception e){}
        //System.out.println("------lengthTwo-----stringValue--"+stringValue);
        return stringValue;
    }


    public String lengthThree(String num){
        //System.out.println("------lengthThree-----num--"+num);
        String stringValue="";
        String[] parts = num.split("");
        int num1=Integer.parseInt(parts[1]);
        String eve=num.substring(1);
        try{
            if (num1 > 0)
                stringValue = lengthOne(parts[1]) + " " + st2[0] + " ";
            stringValue = stringValue + lengthTwo(eve);
        }
        catch(Exception e){}
        //System.out.println("-----lengthThree------stringValue--"+stringValue);
        return stringValue;
    }


    public String lengthGTThreeLTSeven(String num){

        //System.out.println("------lengthGTThreeLTSeven-----num--"+num);
        String stringValuef="";
        String cut1 = num.substring(0, num.length() - 3);
        String cut2 = num.substring(num.length() - 3);
        try{
            if(Integer.parseInt(cut1)>0){
                stringValuef = convertedValue(cut1) + " " + st2[1] + " " + lengthThree(cut2);
            }else{
                stringValuef = convertedValue(cut1) + " " + lengthThree(cut2);
            }
        }
        catch(Exception e){}
        //System.out.println("-----lengthGTThreeLTSeven------stringValue--"+stringValue);
        return stringValuef;
    }


    public String lengthGTSixLTTen(String num){

        //System.out.println("------lengthGTSixLTTen-----num--"+num);
        String stringValuef="";
        String cut1 = num.substring(0, num.length() - 6);
        String cut2 = num.substring(num.length() - 6);
        try{
            if(Integer.parseInt(cut1)>0){
                stringValuef = convertedValue(cut1) + " " + st2[2] + " " + lengthGTThreeLTSeven(cut2);
            }else{
                stringValuef = convertedValue(cut1) + " " + lengthGTThreeLTSeven(cut2);
            }

        }
        catch(Exception e){}
        //System.out.println("-----lengthGTSixLTTen------stringValue--"+stringValue);
        return stringValuef;
    }


    public String lengthGTNineLTTherteen(String num){

        //System.out.println("------lengthGTNineLTTherteen-----num--"+num);
        String stringValuef="";
        String cut1 = num.substring(0, num.length() - 9);
        String cut2 = num.substring(num.length() - 9);
        try{
            if(Integer.parseInt(cut1)>0){
                stringValuef = convertedValue(cut1) + " " + st2[3] + " " + lengthGTSixLTTen(cut2);
            }else{
                stringValuef = convertedValue(cut1) + " " + lengthGTSixLTTen(cut2);
            }
        }
        catch(Exception e){}
        //System.out.println("-----lengthGTNineLTTherteen------stringValue--"+stringValue);
        return stringValuef;
    }


    public String lengthGTTwelveLTSixteen(String num){

        //System.out.println("------lengthGTTwelveLTSixteen-----num--"+num);
        String stringValuef="";
        String cut1 = num.substring(0, num.length() - 12);
        String cut2 = num.substring(num.length() - 12);
        try{
            if(Integer.parseInt(cut1)>0){
                stringValuef = convertedValue(cut1) + " " + st2[4] + " " + lengthGTNineLTTherteen(cut2);
            }else{
                stringValuef = convertedValue(cut1) + " " + lengthGTNineLTTherteen(cut2);
            }
        }
        catch(Exception e){}
        //System.out.println("-----lengthGTTwelveLTSixteen------stringValue--"+stringValue);
        return stringValuef;
    }


    public String lengthGTFifteenLTNineteen(String num){

        //System.out.println("------lengthGTFifteenLTNineteen-----num--"+num);
        String stringValuef="";
        String cut1 = num.substring(0, num.length() - 15);
        String cut2 = num.substring(num.length() - 15);
        try{
            if(Integer.parseInt(cut1)>0){
                stringValuef = convertedValue(cut1) + " " + st2[5] + " " + lengthGTTwelveLTSixteen(cut2);
            }else{
                stringValuef = convertedValue(cut1) + " " + lengthGTTwelveLTSixteen(cut2);
            }
        }
        catch(Exception e){}
        //System.out.println("-----lengthGTFifteenLTNineteen------stringValue--"+stringValue);
        return stringValuef;
    }


    public String lengthGTEightteenLTTwentyTwo(String num){

        //System.out.println("------lengthGTEightteenLTTwentyTwo-----num--"+num);
        String stringValuef="";
        String cut1 = num.substring(0, num.length() - 18);
        String cut2 = num.substring(num.length() - 18);
        try{
            if(Integer.parseInt(cut1)>0){
                stringValuef = convertedValue(cut1) + " " + st2[6] + " " + lengthGTFifteenLTNineteen(cut2);
            }else{
                stringValuef = convertedValue(cut1) + " " + lengthGTFifteenLTNineteen(cut2);
            }
        }
        catch(Exception e){}
        //System.out.println("-----lengthGTEightteenLTTwentyTwo------stringValue--"+stringValue);
        return stringValuef;
    }


    public String lengthGTTwentyOneLTTwentyFive(String num){

        //System.out.println("------lengthGTTwentyOneLTTwentyFive-----num--"+num);
        String stringValuef="";
        String cut1 = num.substring(0, num.length() - 21);
        String cut2 = num.substring(num.length() - 21);
        try{
            if(Integer.parseInt(cut1)>0){
                stringValuef = convertedValue(cut1) + " " + st2[7] + " " + lengthGTEightteenLTTwentyTwo(cut2);
            }else{
                stringValuef = convertedValue(cut1) + " " + lengthGTEightteenLTTwentyTwo(cut2);
            }
        }
        catch(Exception e){}
        //System.out.println("-----lengthGTTwentyOneLTTwentyFive------stringValue--"+stringValue);
        return stringValuef;
    }


    public String lengthGTTwentyFourLTTwentyEight(String num){

        //System.out.println("------lengthGTTwentyFourLTTwentyEight-----num--"+num);
        String stringValuef="";
        String cut1 = num.substring(0, num.length() - 24);
        String cut2 = num.substring(num.length() - 24);
        try{
            if(Integer.parseInt(cut1)>0){
                stringValuef = convertedValue(cut1) + " " + st2[8] + " " + lengthGTTwentyOneLTTwentyFive(cut2);
            }else{
                stringValuef = convertedValue(cut1) + " " + lengthGTTwentyOneLTTwentyFive(cut2);
            }
        }
        catch(Exception e){}
        //System.out.println("-----lengthGTTwentyFourLTTwentyEight------stringValue--"+stringValue);
        return stringValuef;
    }


    public String lengthGTTwentySevenLTThertyOne(String num){

        //System.out.println("------lengthGTTwentySevenLTThertyOne-----num--"+num);
        String stringValuef="";
        String cut1 = num.substring(0, num.length() - 27);
        String cut2 = num.substring(num.length() - 27);
        try{
            if(Integer.parseInt(cut1)>0){
                stringValuef = convertedValue(cut1) + " " + st2[9] + " " + lengthGTTwentyFourLTTwentyEight(cut2);
            }else{
                stringValuef = convertedValue(cut1) + " " + lengthGTTwentyFourLTTwentyEight(cut2);
            }
        }
        catch(Exception e){}
        //System.out.println("-----lengthGTTwentySevenLTThertyOne------stringValue--"+stringValue);
        return stringValuef;
    }












}
