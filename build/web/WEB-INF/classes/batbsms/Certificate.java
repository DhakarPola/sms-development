/*
'******************************************************************************************************************************************
'Author : Fahmi Sarker
'Creation Date : January 2005
'Purpose  : Implements Certificate related functions
'******************************************************************************************************************************************
*/
//Which tables should be updated for changing certificate?

package batbsms;

import java.util.Iterator;
import java.util.Vector;
import barcode.*;
import java.io.*;
import javax.servlet.http.*;
import oracle.jdbc.driver.*;
import java.sql.*;
import oracle.sql.BLOB;
import oracle.jdbc.OracleResultSet;

public class Certificate
{

   conBean con;
   boolean validDN;
   String address;
   Utility util;

   public Certificate()
   {
       try
       {
           con = new conBean();
           validDN = false;
           address="";
           util = new Utility();
       }
       catch(Exception ex)
       {
           System.out.println(ex.getMessage());
       }
   }

   public boolean getValidDN()
   {
       return validDN;
   }
   public String getAddress()
   {
       return address;
   }

   //Checks if the given folio no exists in database
   public boolean folioNoExists(String foliono)
   {

       foliono = foliono.trim();
       String checkIfFolioExistQuery ="";
       int no_of_folios=0;
       boolean isConnect=false;
       try
       {
           isConnect = con.connect();
           checkIfFolioExistQuery = "SELECT * FROM SHAREHOLDER_VIEW WHERE  FOLIO_NO = '"+ foliono +"'";
           no_of_folios = con.queryExecuteCount(checkIfFolioExistQuery);
        }
        catch(Exception ex)
        {
            System.out.println("Could not connect : "+ ex.getMessage() );
        }
        finally
        {
            if (isConnect)
            {   try{
                    con.takeDown();
                }
                catch(Exception ex){;}
            }
        }
        if(no_of_folios>0)
            return true;
        else
            return false;
   }

   //Retrieves shareholder's name from folio no
   public String getName(String foliono)
   {
             String queryName="";
             String name="";

             foliono = foliono.trim();
             boolean isConnect=false;
             try
             {
                 isConnect = con.connect();
                 queryName="SELECT NAME,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4  FROM SHAREHOLDER_VIEW WHERE FOLIO_NO= '"+foliono+"'";
                 con.queryExecute(queryName);
                 while(con.toNext())
                 {
                     name = con.getColumnS("NAME");
                     address = con.getColumnS("ADDRESS1").trim()+"\n" +con.getColumnS("ADDRESS2").trim()+"\n"+con.getColumnS("ADDRESS3").trim()+"\n"+con.getColumnS("ADDRESS4").trim();
                 }
              }
              catch(Exception ex)
              {
                  System.out.println("Could not connect : "+ ex.getMessage() );
              }
              finally
              {
                  if (isConnect)
                  {   try{
                          con.takeDown();
                      }
                      catch(Exception ex){;}
                  }
              }
              return name;

   }

   //Retrieves certificate from Dist. number
   public String getCertificateDN(String dist,String fieldName)
   {

      String queryName="";

      String certificate="";
      dist = dist.trim();

      boolean isConnect=false;
      try
      {
          isConnect = con.connect();

          queryName="SELECT CERTIFICATE_NO FROM SCRIPTS WHERE " + fieldName +"='" + dist +"'";
          con.queryExecute(queryName);
          while(con.toNext())
          {
              certificate = con.getColumnS("CERTIFICATE_NO");
          }
       }
       catch(Exception ex)
       {
           System.out.println("Could not connect : "+ ex.getMessage() );
       }
       finally
       {
           if (isConnect)
           {   try{
                   con.takeDown();
               }
               catch(Exception ex){;}
           }
       }
       return certificate;
   }

   //Retrieves certificate number
   public String getCertificate(String foliono, String suggestCertificate)
   {
       String queryGetCert="";
       String tempCertificate="";
       String oldCertificate="";
       Vector certificateCollection = new Vector();
       boolean isExists=false;
       boolean isConnect = false;
       Object obj;

       foliono = foliono.trim();
       suggestCertificate = suggestCertificate.trim();

       try
       {
           isConnect = con.connect();
           queryGetCert="SELECT CERTIFICATE_NO  FROM CERTIFICATE WHERE HOLDER_FOLIO= '"+foliono+"'";

           con.queryExecute(queryGetCert);
           while(con.toNext())
           {
               certificateCollection.add(con.getColumnS("CERTIFICATE_NO"));
           }

           Iterator it  = certificateCollection.iterator();

           while(it.hasNext())
           {
               tempCertificate = String.valueOf(it.next());
               if(suggestCertificate.equals(tempCertificate))
               {
                   validDN = true;
                   oldCertificate = suggestCertificate;
                   break;
               }
           }
        }
        catch(Exception ex)
        {
            System.out.println("Could not connect : "+ ex.getMessage() );
        }
        finally
              {
                  if (isConnect)
                  {   try{
                          con.takeDown();
                      }
                      catch(Exception ex){;}
                  }
              }


        return oldCertificate;
   }

   public boolean replaceCertificateNo(String newCertificate,String oldCertificate) throws Exception
   {
     String updateCertificateNoQuery = "CALL REPLACE_CERTIFICATE('"+newCertificate+"', '"+ oldCertificate+"')";
     boolean isConnect = false;
     boolean result = false;

     try
     {
         isConnect = con.connect();
         result = con.procedureExecute(updateCertificateNoQuery);
     }
     catch(Exception ex)
      {
          System.out.println("Error generated from method replacCertificateNo() : "+ ex.getMessage() );
      }
      finally
      {
          if (isConnect)
          {   try{
                  con.takeDown();
              }
              catch(Exception ex){;}
          }
      }
      return result;
   }

   public void insertBarCode(String certificateno,String path)
   {
       Barcode barcode = null;
       String queryAddBarcode = "CALL ADD_BARCODE('" + certificateno + "',null)";


       String filename= "c:\\Barcode"+certificateno+".jpg";

       try{
           con.connect();
           con.procedureExecute(queryAddBarcode);// adds empty barcode in database

           FileOutputStream fos = new FileOutputStream(filename);

           barcode = BarcodeFactory.createPDF417(certificateno);
           BarcodeImageHandler.outputBarcodeAsJPEGImage(barcode, fos);

           conBean cm = new conBean();
           //Insert Blob into database
            BLOB blob;
            PreparedStatement pstmt;
            cm.connect();
            Connection connection = cm.getConnection() ;
            String selectQuery="SELECT * FROM BARCODE WHERE CERTIFICATE_NO = '"+ certificateno+"' for update";
            pstmt = connection.prepareStatement(selectQuery);
            connection.setAutoCommit(false);
            ResultSet rset=pstmt.executeQuery();
            rset.next();
            //Use the OracleDriver resultset, we take the blob locator
            blob = ((OracleResultSet) rset).getBLOB("BARCODE_IMG");
            OutputStream os = blob.getBinaryOutputStream();
            //Read the file by chuncks and insert them in the Blob. The chunk size come from the blob
            byte[] chunk = new byte[blob.getChunkSize()];
            int i = -1;

            File fullFile = new File(filename);
            FileInputStream is = new FileInputStream(fullFile);

            while ((i = is.read(chunk)) != -1) {
                os.write(chunk, 0, i);
            }
            is.close();
            os.close();
            fos.close();

            pstmt.close();
            connection.commit();
            connection.setAutoCommit(true);

            boolean success = (new File(filename)).delete();
            if (!success)
            {
              System.out.println("Deletion failed!");
            }
       }
       catch(Exception ex)
       {
           ex.printStackTrace();
       }
   }

   public void insertBarCodeRange(String certificateFrom,String certificateTo,String path)
   {
      Barcode barcode = null;
      Vector certificateCollection = new Vector();
      String certificateno="";
      boolean isConnect = false;
      conBean cm = new conBean();
      conBean cm1 = new conBean();

      cm.connect();
      cm1.connect();

      String querySelectCertRange = "SELECT CERTIFICATE_NO FROM CERTIFICATE WHERE CERTIFICATE_NO BETWEEN '"+certificateFrom +"' AND '"+certificateTo+"'";
      try{

          isConnect = con.connect();
          con.queryExecute(querySelectCertRange);
          while(con.toNext())
          {
                certificateCollection.add(con.getColumnS("CERTIFICATE_NO"));
          }

           Iterator it  = certificateCollection.iterator();

           while(it.hasNext())
           {
               certificateno =util.changeIfNullnTrim(String.valueOf(it.next()));

               String queryAddBarcode = "CALL ADD_BARCODE('" + certificateno + "',null)";



               cm1.procedureExecute(queryAddBarcode);// adds empty barcode in database

               String filename = "c:\\Barcode" + certificateno + ".jpg";

               FileOutputStream fos = new FileOutputStream(filename);

               barcode = BarcodeFactory.createPDF417(certificateno);
               BarcodeImageHandler.outputBarcodeAsJPEGImage(barcode, fos);


               //Insert Blob into database
               BLOB blob;
               PreparedStatement pstmt;

               Connection connection = cm.getConnection();
               String selectQuery = "SELECT * FROM BARCODE WHERE CERTIFICATE_NO = '" +
                                    certificateno + "' for update";
               pstmt = connection.prepareStatement(selectQuery);
               connection.setAutoCommit(false);
               ResultSet rset = pstmt.executeQuery();
               rset.next();
               //Use the OracleDriver resultset, we take the blob locator
               blob = ((OracleResultSet) rset).getBLOB("BARCODE_IMG");
               OutputStream os = blob.getBinaryOutputStream();
               //Read the file by chuncks and insert them in the Blob. The chunk size come from the blob
               byte[] chunk = new byte[blob.getChunkSize()];
               int i = -1;

               File fullFile = new File(filename);
               FileInputStream is = new FileInputStream(fullFile);

               while ((i = is.read(chunk)) != -1) {
                   os.write(chunk, 0, i);
               }
               is.close();
               os.close();
               fos.close();

               pstmt.close();
               connection.commit();
               connection.setAutoCommit(true);

               boolean success = (new File(filename)).delete();
               if (!success) {
                   System.out.println("Deletion failed!");
               }
           }
       }
       catch(Exception ex)
       {
           ex.printStackTrace();
       }
       finally
       {
           try
           {
               if (isConnect)
                   con.takeDown();
               if (isConnect)
                   cm.takeDown();
               if (isConnect)
                   cm1.takeDown();
           }
           catch(Exception ex){;}
       }
   }

   public void insertBarCodeRangeRemat(String certificateFrom,String certificateTo,String path)
  {
     Barcode barcode = null;
     Vector certificateCollection = new Vector();
     String certificateno="";
     boolean isConnect = false;
     conBean cm = new conBean();
     conBean cm1 = new conBean();

     cm.connect();
     cm1.connect();

     String querySelectCertRange = "SELECT CERTIFICATE_NO FROM REMAT WHERE CERTIFICATE_NO BETWEEN '"+certificateFrom +"' AND '"+certificateTo+"'";
     try{

         isConnect = con.connect();
         con.queryExecute(querySelectCertRange);
         while(con.toNext())
         {
               certificateCollection.add(con.getColumnS("CERTIFICATE_NO"));
         }

          Iterator it  = certificateCollection.iterator();

          while(it.hasNext())
          {
              certificateno =util.changeIfNullnTrim(String.valueOf(it.next()));

              String queryAddBarcode = "CALL ADD_BARCODE('" + certificateno + "',null)";



              cm1.procedureExecute(queryAddBarcode);// adds empty barcode in database

              String filename = "c:\\Barcode" + certificateno + ".jpg";

              FileOutputStream fos = new FileOutputStream(filename);

              barcode = BarcodeFactory.createPDF417(certificateno);
              BarcodeImageHandler.outputBarcodeAsJPEGImage(barcode, fos);


              //Insert Blob into database
              BLOB blob;
              PreparedStatement pstmt;

              Connection connection = cm.getConnection();
              String selectQuery = "SELECT * FROM BARCODE WHERE CERTIFICATE_NO = '" +
                                   certificateno + "' for update";
              pstmt = connection.prepareStatement(selectQuery);
              connection.setAutoCommit(false);
              ResultSet rset = pstmt.executeQuery();
              rset.next();
              //Use the OracleDriver resultset, we take the blob locator
              blob = ((OracleResultSet) rset).getBLOB("BARCODE_IMG");
              OutputStream os = blob.getBinaryOutputStream();
              //Read the file by chuncks and insert them in the Blob. The chunk size come from the blob
              byte[] chunk = new byte[blob.getChunkSize()];
              int i = -1;

              File fullFile = new File(filename);
              FileInputStream is = new FileInputStream(fullFile);

              while ((i = is.read(chunk)) != -1) {
                  os.write(chunk, 0, i);
              }
              is.close();
              os.close();
              fos.close();

              pstmt.close();
              connection.commit();
              connection.setAutoCommit(true);

              boolean success = (new File(filename)).delete();
              if (!success) {
                  System.out.println("Deletion failed!");
              }
          }
      }
      catch(Exception ex)
      {
          ex.printStackTrace();
      }
      finally
      {
          try
          {
              if (isConnect)
                  con.takeDown();
              if (isConnect)
                  cm.takeDown();
              if (isConnect)
                  cm1.takeDown();
          }
          catch(Exception ex){;}
      }
  }


   public void insertBarCodeNonPrinted()
   {
       Barcode barcode = null;
       Vector certificateCollection = new Vector();
       String certificateno="";
       boolean isConnect = false;
       conBean cm = new conBean();
       conBean cm1 = new conBean();

       cm.connect();
       cm1.connect();

       String querySelectCertRange = "SELECT CERTIFICATE_NO FROM NON_PRINTED_CERTS_VIEW ";

       try{

           isConnect = con.connect();
           con.queryExecute(querySelectCertRange);
           while(con.toNext())
           {
                 certificateCollection.add(con.getColumnS("CERTIFICATE_NO"));
           }

            Iterator it  = certificateCollection.iterator();

            while(it.hasNext())
            {
                certificateno =util.changeIfNullnTrim(String.valueOf(it.next()));

                String queryAddBarcode = "CALL ADD_BARCODE('" + certificateno + "',null)";



                cm1.procedureExecute(queryAddBarcode);// adds empty barcode in database

                String filename = "c:\\Barcode" + certificateno + ".jpg";

                FileOutputStream fos = new FileOutputStream(filename);

                barcode = BarcodeFactory.createPDF417(certificateno);
                BarcodeImageHandler.outputBarcodeAsJPEGImage(barcode, fos);


                //Insert Blob into database
                BLOB blob;
                PreparedStatement pstmt;

                Connection connection = cm.getConnection();
                String selectQuery = "SELECT * FROM BARCODE WHERE CERTIFICATE_NO = '" +
                                     certificateno + "' for update";
                pstmt = connection.prepareStatement(selectQuery);
                connection.setAutoCommit(false);
                ResultSet rset = pstmt.executeQuery();
                rset.next();
                //Use the OracleDriver resultset, we take the blob locator
                blob = ((OracleResultSet) rset).getBLOB("BARCODE_IMG");
                OutputStream os = blob.getBinaryOutputStream();
                //Read the file by chuncks and insert them in the Blob. The chunk size come from the blob
                byte[] chunk = new byte[blob.getChunkSize()];
                int i = -1;

                File fullFile = new File(filename);
                FileInputStream is = new FileInputStream(fullFile);

                while ((i = is.read(chunk)) != -1) {
                    os.write(chunk, 0, i);
                }
                is.close();
                os.close();
                fos.close();

                pstmt.close();
                connection.commit();
                connection.setAutoCommit(true);

                boolean success = (new File(filename)).delete();
                if (!success) {
                    System.out.println("Deletion failed!");
                }
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            try
            {
                if (isConnect)
                    con.takeDown();
                if (isConnect)
                    cm.takeDown();
                if (isConnect)
                    cm1.takeDown();
            }
            catch(Exception ex){;}
        }

   }
   public void deleteBarcodes()
   {
       String updateCertificateNoQuery = "DELETE FROM BARCODE";
       boolean isConnect = false;
       boolean result = false;

       try
       {
           isConnect = con.connect();
           result = con.procedureExecute(updateCertificateNoQuery);
       }
       catch(Exception ex)
        {
            System.out.println("Error generated from method deleteBarcodes() : "+ ex.getMessage() );
        }
        finally
        {
            if (isConnect)
            {   try{
                    con.takeDown();
                }
                catch(Exception ex){;}
            }
        }

   }

}
