package barcode.twod.pdf417;

import barcode.*;
import barcode.output.AbstractOutput;
import barcode.output.GraphicsOutput;

import java.awt.image.BufferedImage;
import java.awt.*;

/**
 * Implementation of the PDF417 two dimensional barcode format.
 *
 * <p/>Contributed by Alex Ferrer <alex@ftconsult.com>
 *
 * @author <a href="mailto:ian@thoughtworks.net">Ian Bourke</a>
 */
public class PDF417Barcode extends Barcode {
	private PDF417Module module;
	
	/**
	 * Constructs a new new PDF417 barcode with the specified data.
	 * @param data The data to encode
	 * @throws BarcodeException If the data to be encoded is invalid
	 */
	public PDF417Barcode(String data) throws BarcodeException {
		super(data);
		this.barWidth = 1;
		this.drawingText = false;
	}

	/**
	 * Does nothing. Text is never drawn for PDF417.
	 * @param drawingText Ignored
	 */
	public void setDrawingText(boolean drawingText) {
		// Do nothing - we never draw the text for PDF417
	}

	/**
	 * Does nothing. Fixed width for PDF417.
	 * @param barWidth Ignored
	 */
	public void setBarWidth(double barWidth) {
		// Fixed width
	}

	/**
	 * Does nothing. Fixed height for PDF417.
	 * @param barHeight Ignored
	 */
	public void setBarHeight(double barHeight) {
		// Fixed height
	}

	/**
	 * Does nothing. Fixed resolution for PDF417.
	 * @param resolution Ignored
	 */
	public void setResolution(int resolution) {
		// Fixed res
	}

	/**
	 * Returns the barcode width for the given resolution.
	 * @param resolution The output resolution
	 * @return The barcode width
	 */
	protected double getBarcodeWidth(int resolution) {
		initBarcode(data);
		return module.getBarcodeWidth();
	}

	/**
	 * Returns the minimum allowed height for the barcode for the given resolution.
	 * @param resolution The output resolution
	 * @return The minimum allowed barcode height
	 */
	protected double calculateMinimumBarHeight(int resolution) {
		initBarcode(data);
		return module.getBarcodeHeight();
	}

	/**
	 * Returns the encoded data for the barcode.
	 * @return An array of modules that represent the data as a barcode
	 */
	protected Module[] encodeData() {
		initBarcode(data);
		return new Module[] {new PDF417Module(data)};
	}

	/**
	 * Returns the checksum for the barcode, pre-encoded as a Module.
	 * @return A blank module
	 */
	protected Module calculateChecksum() {
		return new BlankModule(0);
	}

	/**
	 * Returns the pre-amble for the barcode.
	 * @return A blank module
	 */
	protected Module getPreAmble() {
		return new BlankModule(0);
	}

	/**
	 * Returns the post-amble for the barcode.
	 * @return A blank module
	 */
	protected Module getPostAmble() {
		return new BlankModule(0);
	}
	
	private void initBarcode(String data) {
		if (module == null) {
			this.module = new PDF417Module(data);
			AbstractOutput params = new GraphicsOutput(
					(Graphics2D) new BufferedImage(1000, 1000,
					                 BufferedImage.TYPE_BYTE_GRAY).getGraphics(),
					null, barWidth, barHeight, false, Color.black, Color.white);
			module.draw(params, 0, 0);
		}
	}
}
