package barcode;
import barcode.output.AbstractOutput;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CompositeModule extends Module {
        private final List modules;

        /**
         * Constructs a new Composite module that is initially empty.
         */
        public CompositeModule() {
                super(new int[0]);
                modules = new ArrayList();
        }

        /**
         * Adds the given module to this composite module.
         * @param module The module to add
         */
        public void add(Module module) {
                modules.add(module);
        }

        /**
         * Returns the number of modules currently contained within this composite module.
         * @return The number of child modules
         */
        public int size() {
                return modules.size();
        }

        /**
         * Returns the child module at the specified index.
         * @param index The module index
         * @return The module at the given index
         */
        public Module getModule(int index) {
                return (Module) modules.get(index);
        }

        /**
         * Returns the symbol group encoded by this module. This is actually a concatenation
         * of the symbols encoded by each child module.
         * @return The symbol encoded by this composite module
         */
        public String getSymbol() {
                StringBuffer buf = new StringBuffer();
                for (Iterator iterator = modules.iterator(); iterator.hasNext();) {
                        Module module = (Module) iterator.next();
                        buf.append(module.getSymbol());
                }
                return buf.toString();
        }

        /**
         * Draws the module to the given outputter at the specified origin. This actually
         * draws each child module in turn.
         * @param output The outputter to draw to
         * @param x The X component of the origin
         * @param y The Y component of the origin
         * @return The total width drawn
         */
        protected double draw(AbstractOutput output, double x, double y) {
                double sum = 0;
                double currentX = x;

                for (Iterator iterator = modules.iterator(); iterator.hasNext();) {
                        Module module = (Module) iterator.next();
                        double result = module.draw(output, currentX, y);
                        currentX += result;
                        sum += result;
                }
                return sum;
        }

}
