package barcode.env;

public class EnvironmentFactory {

        private static Environment env;
        private static Environment defaultEnvironment;

        ///CLOVER:OFF
        /** Cannot construct directly */
        protected EnvironmentFactory() {
        }
        ///CLOVER:ON

        /**
         * Returns the current operating environment.
         * @return The current environment
         */
        public static Environment getEnvironment() {
                if (env == null) {
                        determineCurrentEnvironment();
                }
                return env;
        }

        /**
         * Forces the factory to assume headless mode, regardless of whether
         * this is actually true or not.
         */
        public static void setHeadlessMode() {
                env = new HeadlessEnvironment();
        }

        /**
         * Forces the factory to use the environment that does not access the AWT.
         */
        public static void setNonAWTMode() {
                env = new NonAWTEnvironment(HeadlessEnvironment.DEFAULT_RESOLUTION);
        }

        /**
         * Forces the factory to use the environment that does not access the AWT.
         * @param resolution The desired (or current) screen/output resolution
         */
        public static void setNonAWTMode(int resolution) {
                env = new NonAWTEnvironment(resolution);
        }

        /**
         * Sets the factory to use the default (discovered) environment.
         */
        public static void setDefaultMode() {
                determineCurrentEnvironment();
        }

        /**
         * Sets the default environment for the factory.
         * Use this to set your own environment implementation.
         * @param newEnv The new default environment
         */
        public static void setDefaultEnvironment(Environment newEnv) {
                env = null;
                defaultEnvironment = newEnv;
        }

        private static void determineCurrentEnvironment() {
                Environment current;
                if (defaultEnvironment != null) {
                        current = defaultEnvironment;
                } else {
                        current = new DefaultEnvironment();
                }
                try {
                        // Try to get the res, this will fail in headless mode
                        current.getResolution();
                } catch (UnsupportedOperationException e) {
                        current = new HeadlessEnvironment();
                } catch (InternalError e) {
                        current = new HeadlessEnvironment();
                }
                env = current;
        }
}
