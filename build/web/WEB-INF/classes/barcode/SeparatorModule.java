package barcode;
import barcode.output.AbstractOutput;
public class SeparatorModule extends Module {

        /**
         * Constructs a new SeparatorModule with a width of 1.
         */
        public SeparatorModule() {
                super(new int[] {1});
        }

        /**
         * Constructs a new SeparatorModule with the specified width.
         * @param width The width of the separator in bar widths.
         */
        public SeparatorModule(int width) {
                super(new int[] {width});
        }

        /**
         * Draws the module to the specified output.
         * @param output The barcode output to draw to
         * @param x The starting X co-ordinate
         * @param y The starting Y co-ordinate
         * @return The total width drawn
         */
        protected double draw(AbstractOutput output, double x, double y) {
                double w = bars[0] * output.getBarWidth();
                output.drawBar((int) x, (int) y, (int) w, (int) output.getBarHeight(), false);
                return w;
        }

        /**
         * Returns the symbol that this module encodes.
         * @return A blank string
         */
        public String getSymbol() {
                return "";
        }
}
