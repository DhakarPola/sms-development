package barcode.linear.code128;

import barcode.Module;

/**
 * Specific module implemetation that represents the Code 128 code shift character.
 *
 * @author <a href="mailto:ian@thoughtworks.net">Ian Bourke</a>
 */
class ShiftModule extends Module {

	/**
	 * Constructs a shift module with the specified bars.
	 * @param bars The bars to draw to encode the shift
	 */
	public ShiftModule(int[] bars) {
		super(bars);
	}
}
