package barcode.linear.codebar;

import barcode.Barcode;
import barcode.Module;
import barcode.BlankModule;
import barcode.SeparatorModule;
import barcode.BarcodeException;

import java.text.StringCharacterIterator;
import java.text.CharacterIterator;
import java.util.List;
import java.util.ArrayList;


public class CodabarBarcode extends Barcode {
	/** The default codabar start character */
	public static final String DEFAULT_START = "A";
	/** The default codabar stop character */
	public static final String DEFAULT_STOP = "C";

	private int width = 0;
	private String label;

	/**
	 * Constructs a new Codabar barcode with thte specified data.
	 * @param data The data to encode
	 * @throws BarcodeException If the data is invalid
	 */
	public CodabarBarcode(String data) throws BarcodeException {
		super(data);
		this.label = data;
		validateData();
	}

	/**
	 * Returns the text label to be displayed underneath the barcode.
	 * @return The barcode label
	 */
	protected String getLabel() {
		return label;
	}

	/**
	 * Returns the total width of the barcode in pixels at the given resolution.
	 * @param resolution The resolution to calculate the width for
	 * @return The width of the barcode in pixels
	 */
	protected double getBarcodeWidth(int resolution) {
		encodeData();
		return barWidth * width;
	}

	/**
	 * Encodes the data of the barcode into bars.
	 * @return The encoded bar data
	 */
	protected Module[] encodeData() {
		width = 0;
		List modules = new ArrayList();
		for (int i = 0; i < data.length(); i++) {
			if (i > 0) {
				modules.add(new SeparatorModule(1));
				width += 1;
			}
			char c = data.charAt(i);
			Module module = ModuleFactory.getModule(String.valueOf(c));
			modules.add(module);
			width += module.widthInBars();
		}
		return (Module[]) modules.toArray(new Module[0]);
	}

	/**
	 * Calculates the check sum digit for the barcode.
	 * @return Null - Codabar has no checksum
	 */
	protected Module calculateChecksum() {
		return null; // No checksum - return null
	}

	/**
	 * Returns the pre-amble for the barcode.
	 * @return A BlankModule
	 */
	protected Module getPreAmble() {
		return new BlankModule(0);
	}

	/**
	 * Returns the post-amble for the barcode.
	 * @return A BlankModule
	 */
	protected Module getPostAmble() {
		return new BlankModule(0);
	}

	private void validateData() throws BarcodeException {
		replaceTraditionalStartStopWithModern();
		addDefaultStartStopIfRequired();
		int index = 0;
		StringBuffer buf = new StringBuffer();
		StringCharacterIterator iter = new StringCharacterIterator(data);
		for (char c = iter.first(); c != CharacterIterator.DONE; c = iter.next()) {
			if (!Character.isWhitespace(c)) {
				if (!ModuleFactory.isValid(String.valueOf(c))) {
					throw new BarcodeException(c
			                  + " is not a valid character for Codabar encoding");
				}
				checkStartStop(c, index);
				buf.append(c);
			}
			index += 1;
		}
		data = buf.toString();
	}

	private void addDefaultStartStopIfRequired() {
		StringBuffer newData = new StringBuffer();
		if (!Character.isLetter(data.charAt(0))) {
			newData.append(DEFAULT_START);
		}
		newData.append(data);
		if (!Character.isLetter(data.charAt(data.length() - 1))) {
			newData.append(DEFAULT_STOP);
		}
		data = newData.toString();
	}

	private void replaceTraditionalStartStopWithModern() {
		data = data.replace('a', 'A');
		data = data.replace('t', 'A');
		data = data.replace('b', 'B');
		data = data.replace('n', 'B');
		data = data.replace('c', 'C');
		data = data.replace('*', 'C');
		data = data.replace('d', 'D');
		data = data.replace('e', 'D');
	}

	private void checkStartStop(char c, int index) throws BarcodeException {
		if (Character.isLetter(c) && index > 0 && index < data.length() - 1) {
			throw new BarcodeException(c
	                  + " is only allowed as the first and last characters for Codabar barcodes");
		}
	}
}
