package barcode.output;
import java.io.BufferedWriter;
import java.io.IOException;
import java.awt.*;
public class SVGOutput extends AbstractOutput {

        private String mUnits;
        private StringBuffer mSB;
        private final java.io.Writer writer;
    private static final String DEFAULT_FAMILY = "Arial";
    private static final int DEFAULT_SIZE = 20;

    /**
         * Creates a new instance of SVGOutput.
         * @param writer The Writer to output the SVG text to
         * @param font The font for text rendering (only if Barcode has drawText set to true)
         * @param barHeight The height of the barcode
         * @param fgColor Foreground color
         * @param bgColor Background color
         * @param scalar The scalar value to convert to units.  if barWidth is 1, and you want the
         * smallest bar to be 1/128 of an inch, this should be set to 1.0/128, and units
         * should be set to "in"
         * @param units The units for the scalar, above.  "in", "cm", "mm", "px" are acceptable values.
         */
        public SVGOutput(java.io.Writer writer, Font font, double barHeight,
                                         Color fgColor, Color bgColor, double scalar, String units) {
                super(font, 1.0, barHeight, true, scalar, fgColor, bgColor);
                this.writer = new BufferedWriter(writer);
                mUnits = units;
                mSB = new StringBuffer();
        }

        /**
         * From AbstractOutput - sets up the SVG output.
         * @param width The output width (in pixels) of the barcode
         * @param height The output height (in pixels) of the barcode.
         */
        public void beginDraw(double width, double height) {
                mSB.setLength(0);
                mSB.append("<svg ");
                mSB.append("xmlns=\"http://www.w3.org/2000/svg\" ");
                mSB.append("width=\"" + getScaledDimension(width) + "\" ");
                mSB.append("height=\"" + getScaledDimension(height) + "\"");
                mSB.append(">\n");
                try {
                        writer.write(mSB.toString());
                } catch (java.io.IOException ex) {
                        System.err.println("IO Exception writing SVG prologue: " + ex.toString());
                }
        }

        /**
         * From AbstractOutput - finished up the SVG output.
         */
        public void endDraw() {
                mSB.setLength(0);
                mSB.append("</svg>\n");
                try {
                        writer.write(mSB.toString());
                } catch (java.io.IOException ex) {
                        System.err.println("IO Exception writing svg closing tag: " + ex.toString());
                } finally {
                        try {
                                writer.flush();
                                writer.close();
                        } catch (IOException e) {
                                System.err.println("IO Exception closing SVG stream: " + e.toString());
                        }
                }
        }

        /**
         * From AbstractOutput - outputs the correct rectangle to the SVG output.
         * @param x the x coordinate
         * @param y the y coordinate
         * @param width the width
         * @param height the height
         * @param paintWithForegroundColor if true, use the foreground color, otherwise use the background color
         */
        public void drawBar(int x, int y, int width, int height, boolean paintWithForegroundColor) {
                mSB.setLength(0);
                mSB.append(" <rect ");
                mSB.append("x=\"" + getScaledDimension(x) + "\" ");
                mSB.append("y=\"" + getScaledDimension(y) + "\" ");
                mSB.append("width=\"" + getScaledDimension(width) + "\" ");
                mSB.append("height=\"" + getScaledDimension(height) + "\" ");
                mSB.append("style=\"fill:");
                mSB.append(paintWithForegroundColor ? getColorAsCSS(foregroundColour) : getColorAsCSS(backgroundColour));
                mSB.append(";\"");
                mSB.append("/>\n");

                try {
                        writer.write(mSB.toString());
                } catch (java.io.IOException ex) {
                        System.err.println("Exception Writing Module:" + ex.toString());
                }
        }

        /**
         * From AbstractOutput - Draw the specified text to the SVG output.
         * @param text The text to draw
         * @param x The x position
         * @param y The y position
         * @param width The width of barcode (total)
         * @return The height of this text
         */
        public double drawText(String text, double x, double y, double width) {
                double startX = x;
                double startY = y; // default case, we return 0.
                startY = y + barHeight;

        String family = DEFAULT_FAMILY;
        int size = DEFAULT_SIZE;
        int style = Font.PLAIN;

        if (font != null) {
            family = font.getFamily();
            size = font.getSize();
            style = font.getStyle();
        }

                mSB.setLength(0);
                mSB.append(" <text ");
                mSB.append("x=\"" + getScaledDimension(startX) + "\" ");
                mSB.append("y=\"" + getScaledDimension(startY) + "\" ");
                mSB.append("style=\"");
        mSB.append("font-family: " + family + "; ");
        mSB.append("font-size: " + size + "pt; ");

        switch(style) {
                        case Font.PLAIN: mSB.append("font-style: normal; "); break;
                        case Font.BOLD: mSB.append("font-weight: bold; "); break;
                        case Font.ITALIC: mSB.append("font-style: italic; "); break;
                        default:
                                System.err.println("Unknown Font Style: " + font.getStyle());
                                mSB.append("font-style: normal; ");
                                break;
                }
                mSB.append("\"");
                mSB.append(">");

                // now the text...
                mSB.append(text);

                // and the close element
                mSB.append("</text>\n");

                try {
                        writer.write(mSB.toString());
                } catch (java.io.IOException ex) {
                        System.err.println("IO Exception writing text: " + ex.toString());
                }

                return startY - y;
        }

        private String getScaledDimension(double value) {
                return "" + value * scalar + mUnits;
        }

        private String getColorAsCSS(Color c) {
                StringBuffer sb = new StringBuffer("#");
                float [] components = c.getColorComponents(null);
                for (int ii = 0; ii < components.length; ii++) {
                        String s = Integer.toHexString((int) (255 * components[ii])).toUpperCase();
                        if (s.length() == 1) {
                                sb.append('0');
                        }
                        sb.append(s);
                }
                return sb.toString();
        }
}
