package barcode;
import barcode.output.AbstractOutput;
import java.util.Arrays;
public class Module {
        /** The specification of bars that makes up this module, in a list of bar widths in on, off order) */
        protected final int[] bars;
        private String symbol;

        /**
         * Constructs a new Module with the given bar specification.
         * @param bars The bar specification
         */
        public Module(int[] bars) {
                this.bars = bars;
        }

        /**
         * Returns the symbol being encoded by this module.
         * @return The symbol encoded by this module
         */
        public String getSymbol() {
                return symbol;
        }

        /**
         * Sets the symbol that this module encodes.
         * @param symbol The symbol being encoded by this module
         */
        public void setSymbol(String symbol) {
                this.symbol = symbol;
        }

        /**
         * Returns the underlying total width of the bars from the bar
         * specification (that is, the sum of original bar widths in base
         * bar units).
         * @return The total width of bars in base (unscaled) units
         */
        public int widthInBars() {
                int sum = 0;
                for (int i = 0; i < bars.length; i++) {
                        sum += bars[i];
                }
                return sum;
        }

        /**
         * Draws the module to the given outputter at the specified origin.
         * @param output The outputter to draw to
         * @param x The X component of the origin
         * @param y The Y component of the origin
         * @return The total width drawn
         * @todo integers here, not doubles
         */
        protected double draw(AbstractOutput output, double x, double y) {
        double sum = 0;

        for (int i = 0; i < bars.length; i++) {
            int bar = bars[i];
            double w = bar * output.getBarWidth();
            sum += w;
                        output.drawBar((int) x, (int) y, (int) w, (int) output.getBarHeight(), (i % 2 == 0));
            x += w;
        }
        return sum;
    }

        /**
         * Indicates whether some other object is "equal to" this one.
         * <p>
         * The <code>equals</code> method implements an equivalence relation:
         * <ul>
         * <li>It is <i>reflexive</i>: for any reference value <code>x</code>,
         *     <code>x.equals(x)</code> should return <code>true</code>.
         * <li>It is <i>symmetric</i>: for any reference values <code>x</code> and
         *     <code>y</code>, <code>x.equals(y)</code> should return
         *     <code>true</code> if and only if <code>y.equals(x)</code> returns
         *     <code>true</code>.
         * <li>It is <i>transitive</i>: for any reference values <code>x</code>,
         *     <code>y</code>, and <code>z</code>, if <code>x.equals(y)</code>
         *     returns  <code>true</code> and <code>y.equals(z)</code> returns
         *     <code>true</code>, then <code>x.equals(z)</code> should return
         *     <code>true</code>.
         * <li>It is <i>consistent</i>: for any reference values <code>x</code>
         *     and <code>y</code>, multiple invocations of <tt>x.equals(y)</tt>
         *     consistently return <code>true</code> or consistently return
         *     <code>false</code>, provided no information used in
         *     <code>equals</code> comparisons on the object is modified.
         * <li>For any non-null reference value <code>x</code>,
         *     <code>x.equals(null)</code> should return <code>false</code>.
         * </ul>
         * <p>
         * The <tt>equals</tt> method for class <code>Object</code> implements
         * the most discriminating possible equivalence relation on objects;
         * that is, for any reference values <code>x</code> and <code>y</code>,
         * this method returns <code>true</code> if and only if <code>x</code> and
         * <code>y</code> refer to the same object (<code>x==y</code> has the
         * value <code>true</code>).
         *
         * @param   o   the reference object with which to compare.
         * @return  <code>true</code> if this object is the same as the obj
         *          argument; <code>false</code> otherwise.
         * @see     Boolean#hashCode()
         */
        public boolean equals(Object o) {
                if (this == o) {
                        return true;
                }

                if (!(o instanceof Module)) {
                        return false;
                }

                final Module module = (Module) o;

                if (!Arrays.equals(bars, module.bars)) {
                        return false;
                }

                return true;
        }



        /**
         * Returns a hash code value for the object. This method is
         * supported for the benefit of hashtables such as those provided by
         * <code>java.util.Hashtable</code>.
         * <p>
         * The general contract of <code>hashCode</code> is:
         * <ul>
         * <li>Whenever it is invoked on the same object more than once during
         *     an execution of a Java application, the <tt>hashCode</tt> method
         *     must consistently return the same integer, provided no information
         *     used in <tt>equals</tt> comparisons on the object is modified.
         *     This integer need not remain consistent from one execution of an
         *     application to another execution of the same application.
         * <li>If two objects are equal according to the <tt>equals(Object)</tt>
         *     method, then calling the <code>hashCode</code> method on each of
         *     the two objects must produce the same integer result.
         * <li>It is <em>not</em> required that if two objects are unequal
         *     according to the {@link Object#equals(Object)}
         *     method, then calling the <tt>hashCode</tt> method on each of the
         *     two objects must produce distinct integer results.  However, the
         *     programmer should be aware that producing distinct integer results
         *     for unequal objects may improve the performance of hashtables.
         * </ul>
         * <p>
         * As much as is reasonably practical, the hashCode method defined by
         * class <tt>Object</tt> does return distinct integers for distinct
         * objects. (This is typically implemented by converting the internal
         * address of the object into an integer, but this implementation
         * technique is not required by the
         * Java<font size="-2"><sup>TM</sup></font> programming language.)
         *
         * @return  a hash code value for this object.
         * @see     Object#equals(Object)
         */
        public int hashCode() {
                int sum = 0;
                for (int i = 0; i < bars.length; i++) {
                        sum += (i + 1) * bars[i];
                }
                return sum;
        }

        /**
         * Returns a string representation of the object. In general, the
         * <code>toString</code> method returns a string that
         * "textually represents" this object. The result should
         * be a concise but informative representation that is easy for a
         * person to read.
         * It is recommended that all subclasses override this method.
         * <p>
         * The <code>toString</code> method for class <code>Object</code>
         * returns a string consisting of the name of the class of which the
         * object is an instance, the at-sign character `<code>@</code>', and
         * the unsigned hexadecimal representation of the hash code of the
         * object. In other words, this method returns a string equal to the
         * value of:
         * <blockquote>
         * <pre>
         * getClass().getName() + '@' + Integer.toHexString(hashCode())
         * </pre></blockquote>
         *
         * @return  a string representation of the object.
         */
        public String toString() {
                StringBuffer buf = new StringBuffer();
                for (int i = 0; i < bars.length; i++) {
                        if (i > 0) {
                                buf.append(", ");
                        }
                        buf.append(bars[i]);
                }
                return buf.toString();
        }
}
