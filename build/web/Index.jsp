<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : October 2005
'Page Purpose  : Prompts the user to Login.
'******************************************************************************************************************************************
*/
</script>

<%@ page errorPage="CommonError.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<title>Welcome to Share Management System</title>
<style type="text/css">
<!--
.style2 {color: #0044B0}
-->
</style>
</head>

<body  onkeypress="PressEnter()" onload="document.forms[0].UserID.focus();">
&nbsp;
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>

<script LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></script>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<script LANGUAGE="JavaScript">

function PressEnter(){
  if (document.layers)
	document.captureEvents(Event.KEYPRESS);
	function backhome(e){
		if (document.layers){
			if (e.which==13)

			document.forms[0].submit();
		}
		else if (document.all){
			if (event.keyCode==13)

			document.forms[0].submit();
		}
	}

document.onkeypress=backhome;
}

function SubmitThis(){

	count = 0;

//USER ID AND PASSWORD VALIDATION

	BlankValidate('UserID','- User ID');
	BlankValidate('Password',' - Password');

	if(count==0){

		document.forms[0].submit();
	}
	else{
		ShowAllAlertMsg();
		return;
	}
}

</script>
</p>
<p></p>
<p></p>
<form name="form1" method="POST" action="<%=request.getContextPath()%>/jsp/login.jsp" onsubmit="SubmitThis();return false;">
    <center>
    <table border="0" cellpadding="4" style="border-collapse: collapse" bordercolor="#111111" width="31%" id="AutoNumber3" >
      <tr>
        <td width="100%">
        <p align="right"><IMG src="images/BATBLogo.gif" width="100" height="82" border="0"></td>
      </tr>
    </table>

    <table border="1" width="34%" id="AutoNumber1" cellpadding="4" bordercolor="#0044B0" style="border-collapse: collapse" cellspacing="0" bgcolor="#E8F3FD">
     <tr>
      <td width="100%" bgcolor="#0044B0"><b><font color="#FFFFFF" face="Verdana" size="2">Login</font></b></td>
     </tr>
     <tr>
      <td width="100%" bgcolor="#FFF7E8" bordercolor="#C0C0C0">

        <table border="0" cellspacing="0" width="99%" id="AutoNumber2" cellpadding="4" style="border-collapse: collapse" bgcolor="#FFF7E8">
          <tr>
            <td width="56"><span class="style2"><font face="Verdana" size="2">User ID</font></span></td>
            <td width="9"><font face="Verdana" size="2" color="#FF0000">*</font></td>
            <td width="180"><font face="Verdana" size="2">
            <input type="text"  name="UserID" tabindex="1" class="SL3TextField" size="30"></font></td>
          </tr>
          <tr>
            <td width="56"><span class="style2"><font face="Verdana" size="2">Password</font></span></td>
            <td width="9"><font face="Verdana" size="2" color="#FF0000">*</font></td>
            <td width="180"><font face="Verdana" size="2"><input name="Password" type="password" tabindex="1" size="30" class="SL3TextField">
            </font></td>
          </tr>
          <tr>
            <td width="56" nowrap><span class="style2"><font face="Verdana" size="2">User </font></span></td>
            <td width="9"><font face="Verdana" size="2" color="#FF0000"></font></td>
            <td width="180"><span class="style2"><font face="Verdana" size="2">
            <input name="UserType" type="radio" class="" tabindex="1" value="NetworkUser">Network
            <input name="UserType" type="radio" class="" tabindex="1" value="User" checked="checked">Stand-alone
	    	</font></span></td>
          </tr>
        </table>

      </td>
    </tr>
    <tr>
      <td width="100%" bgcolor="#FFF7E8">
        <p align="center"><font face="Verdana">
		<img name="B1" src="images/btnLogin.gif" onClick="SubmitThis()" onMouseOver="document.forms[0].B1.src = 'images/btnLoginOn.gif'" onMouseOut="document.forms[0].B1.src = 'images/btnLogin.gif'">
		<img name="B2" src="images/btnRefresh.gif" onClick="reset()" onMouseOver="document.forms[0].B2.src = 'images/btnRefreshOn.gif'" onMouseOut="document.forms[0].B2.src = 'images/btnRefresh.gif'">

        </font>
    </tr>
  </table>
    </center>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
</form>
</body>
</html>

