<script>
/*
'******************************************************************************************************************************************
'Script Author : Renad Hakim
'Creation Date : March 2006
'Page Purpose  : Passes Parameters for Group Dividend Warrant and Letter Printing.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

 boolean isConnect = cm.connect();
 if(isConnect==false)
 {
   %>
    <jsp:forward page="ErrorMsg.jsp" >
    <jsp:param name="ErrorTitle" value="Connection Failure" />
    <jsp:param name="ErrorHeading" value="Connection Problem" />
    <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
    </jsp:forward>
  <%
 }

%>

<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<%@page
import="java.util.Date,java.text.*,com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
Dividend Warrant and Letter
</title>
</head>
<body bgcolor="#ffffff">
<h1>
</h1>
<%
  String DateDec = String.valueOf(request.getParameter("dateselect"));
  String PRecords = String.valueOf(request.getParameter("printrecords"));
  String SWarrantNo = String.valueOf(request.getParameter("swarrantno"));
  String WStart = String.valueOf(request.getParameter("warrantstart"));
  String WEnd = String.valueOf(request.getParameter("warrantend"));
  String SignatureInfo = String.valueOf(request.getParameter("sigselect"));

  if (String.valueOf(WStart).equals("null"))
   WStart = "";
  if (String.valueOf(WEnd).equals("null"))
   WEnd = "";
  if (String.valueOf(SWarrantNo).equals("null"))
   SWarrantNo = "";

  String reporturl = "";
  String markprinted = "";
  boolean mp;

   if (PRecords.equalsIgnoreCase("singlebywarrant"))
   {
     String query2 = "SELECT * FROM DIVIDEND_GROUP_VIEW WHERE WARRANT_NO = '" + SWarrantNo + "'";
     int fcc2 = cm.queryExecuteCount(query2);

     if (fcc2 < 1)
     {
     %>
      <script language="javascript">
       alert("Warrant does not exist!");
       location = "<%=request.getContextPath()%>/jsp/Dividend/SetGroupLetterCheque.jsp";
      </script>
     <%
     }
     else
     {
      reporturl = "/CR_Reports/Group_Warrant/Folio_Div_Warrant_&_LetterOrderByDividend_ParamWarrant.rpt";
     }
   }
   else if (PRecords.equalsIgnoreCase("notprinted"))
   {
      reporturl = "/CR_Reports/Group_Warrant/Folio_Div_Warrant_&_LetterOrderByDividend_All.rpt";
   }
   else if (PRecords.equalsIgnoreCase("rangebywarrant"))
   {
      reporturl = "/CR_Reports/Group_Warrant/Folio_Div_Warrant_&_LetterOrderByDividend_RangeWarrant.rpt";
   }

  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','Printed Dividend Warrant and Letter')";
  boolean ub = cm.procedureExecute(ulog);

  if (!reporturl.equals("")) {
    session.setAttribute("reportSource", null);
  }

Object reportSource = session.getAttribute("reportSource");
if (reportSource == null)
{
try{
  IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
  reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
  session.setAttribute("reportSource", reportSource);
}catch (Exception e) {}
}
  CrystalReportViewer viewer= new CrystalReportViewer();
  try{
    viewer.setReportSource(reportSource);
  }catch (Exception e) {}
  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  //viewer.setPrintMode(CrPrintMode.PDF);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setReuseParameterValuesOnRefresh(true);

// Passing parameters

ParameterField param1 = new ParameterField();
param1.setReportName("");
param1.setName("ddate");

Values vals1 = new Values();
ParameterFieldDiscreteValue val1 = new ParameterFieldDiscreteValue();
val1.setValue(DateDec);
vals1.add(val1);

ParameterField param2 = new ParameterField();
param2.setReportName("");
param2.setName("ParamWarrant");

Values vals2 = new Values();
ParameterFieldDiscreteValue val2 = new ParameterFieldDiscreteValue();
val2.setValue(SWarrantNo);
vals2.add(val2);

ParameterField param3 = new ParameterField();
param3.setReportName("");
param3.setName("WarrantGreaterThan");

Values vals3 = new Values();
ParameterFieldDiscreteValue val3 = new ParameterFieldDiscreteValue();
val3.setValue(WStart);
vals3.add(val3);

ParameterField param4 = new ParameterField();
param4.setReportName("");
param4.setName("WarrantLessThan");

Values vals4 = new Values();
ParameterFieldDiscreteValue val4 = new ParameterFieldDiscreteValue();
val4.setValue(WEnd);
vals4.add(val4);

try {
 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
 Date d = df.parse(DateDec);
 val1.setValue(d);
 vals1.add(val1);
} catch(Exception e) {
e.printStackTrace();
}

param1.setCurrentValues(vals1);
param2.setCurrentValues(vals2);
param3.setCurrentValues(vals3);
param4.setCurrentValues(vals4);

Fields fields = new Fields();
fields.add(param1);
fields.add(param2);
fields.add(param3);
fields.add(param4);

viewer.setParameterFields(fields);
viewer.refresh();

try
{
viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);

   if (PRecords.equalsIgnoreCase("singlebywarrant"))
   {
     String query2a = "SELECT * FROM DIVIDEND_GROUP_VIEW WHERE WARRANT_NO = '" + SWarrantNo + "'";
     int fcc2a = cm.queryExecuteCount(query2a);

     String nomineeno = "";
     String foliono = "";

     while(cm.toNext())
     {
       nomineeno = cm.getColumnS("NOMINEE_NO");
     }

     if (String.valueOf(nomineeno).equals("null"))
      nomineeno = "";

     if (!nomineeno.equalsIgnoreCase(""))
     {
       markprinted = "call MARK_PRINTED_DIVIDEND_GROUP('" + nomineeno + "','" + DateDec + "')";
       mp = cm.procedureExecute(markprinted);
     }
   }
   else if (PRecords.equalsIgnoreCase("notprinted"))
   {
     String query2a = "SELECT * FROM DIVIDEND_GROUP_VIEW WHERE PRINTED = 'F' AND ISSUE_DATE = TO_DATE('" + DateDec + "','DD/MM/YYYY')";
     int fcc2a = cm.queryExecuteCount(query2a);

     String nomineeno = "";

     while(cm.toNext())
     {
       nomineeno = cm.getColumnS("NOMINEE_NO");

       if (String.valueOf(nomineeno).equals("null"))
        nomineeno = "";

       if (!nomineeno.equalsIgnoreCase(""))
       {
         markprinted = "call MARK_PRINTED_DIVIDEND_GROUP('" + nomineeno + "','" + DateDec + "')";
         mp = cm.procedureExecute(markprinted);
       }
     }
   }
   else if (PRecords.equalsIgnoreCase("rangebywarrant"))
   {
     String query2a = "SELECT * FROM DIVIDEND_GROUP_VIEW WHERE PRINTED = 'F' AND ISSUE_DATE = TO_DATE('" + DateDec + "','DD/MM/YYYY') AND (WARRANT_NO BETWEEN '" + WStart + "' AND '" + WEnd + "')";
     int fcc2a = cm.queryExecuteCount(query2a);

     String nomineeno = "";

     while(cm.toNext())
     {
       nomineeno = cm.getColumnS("NOMINEE_NO");

       if (String.valueOf(nomineeno).equals("null"))
        nomineeno = "";

       if (!nomineeno.equalsIgnoreCase(""))
       {
         markprinted = "call MARK_PRINTED_DIVIDEND_GROUP('" + nomineeno + "','" + DateDec + "')";
         mp = cm.procedureExecute(markprinted);
       }
     }
   }
}
catch(Exception e){System.out.println(e.getMessage());}

  if (isConnect)
  {
   cm.takeDown();
  }

%>
</body>
</html>

