<script>
/*
'******************************************************************************************************************************************
'Script Author : Md. Kamruzzaman
'Creation Date : May 2007
'Page Purpose  : Passes Parameters for Remat Certificate View.
'******************************************************************************************************************************************
*/
</script>
<%@ page errorPage="CommonError.jsp" %>
<jsp:useBean id="cm" class="batbsms.conBean"/>
<LINK REL=stylesheet HREF="<%=request.getContextPath()%>/js/common.css" TYPE="text/css"></LINK>
<% if(String.valueOf(session.getAttribute("UserName")).equals("null"))
   {
%>
 <script>top.location = "<%=request.getContextPath()%>/Index.jsp"</script>
<%
}

 boolean isConnect = cm.connect();
 if(isConnect==false)
 {
   %>
    <jsp:forward page="ErrorMsg.jsp" >
    <jsp:param name="ErrorTitle" value="Connection Failure" />
    <jsp:param name="ErrorHeading" value="Connection Problem" />
    <jsp:param name="ErrorMsg" value="Error : Connection Failed, Please contact the System Administrator" />
    </jsp:forward>
  <%
 }

%>

<%@ taglib uri="/crystal-tags-reportviewer.tld" prefix="crviewer" %>
<%@page language="java" import="java.util.Vector,java.util.StringTokenizer,java.text.SimpleDateFormat, java.lang.Object.*, java.math.*"%>
<%@page
import="java.util.Date,java.text.*,com.crystaldecisions.reports.reportengineinterface.*,
com.crystaldecisions.report.web.viewer.*,
com.crystaldecisions.sdk.occa.report.data.*,
com.crystaldecisions.sdk.occa.report.reportsource.*
"
contentType="text/html;charset=UTF-8"
%>
<html>
<head>
<title>
Remat Cirtificate List
</title>
</head>
<body bgcolor="#ffffff">
<h1>
</h1>
<%
    //Value of parameter get from page SetAttendanceList.jsp
  String orderby = String.valueOf(request.getParameter("orderby"));

  if (String.valueOf(orderby).equals("null"))
   orderby = "";

  String reporturl = "";

  if(orderby.equalsIgnoreCase("byfolio"))
  {
    reporturl="";
    reporturl ="/CR_Reports/Remat_certificate/remat_certificat_list_folio.rpt";
  }
  else if(orderby.equalsIgnoreCase("bycertificate"))
  {
    reporturl="";
    reporturl ="/CR_Reports/Remat_certificate/remat_certificat_list_Certificate.rpt";
  }


  String ulog = "call ADD_USER_LOG('" + String.valueOf(session.getAttribute("UserName")) + "','" + String.valueOf(session.getAttribute("UserRole")) + "','View Remat Cirtificate List')";
  boolean ub = cm.procedureExecute(ulog);

  if (!reporturl.equals("")) {
    //session.setAttribute("reportSource", null);
    session.setAttribute("reportSource", null);
    session.setAttribute("refreshed", null);
  }

Object reportSource = session.getAttribute("reportSource");
if (reportSource == null)
{
try{
  IReportSourceFactory2 rptSrcFactory = new JPEReportSourceFactory();
  reportSource = rptSrcFactory.createReportSource(reporturl, request.getLocale());
  session.setAttribute("reportSource", reportSource);
}catch (Exception e) {}
}
  CrystalReportViewer viewer= new CrystalReportViewer();
  try{
    viewer.setReportSource(reportSource);
  }catch (Exception e) {}
  viewer.setHasViewList(false);
  viewer.setDisplayGroupTree(false);
  viewer.setEnableParameterPrompt(false);
  viewer.setEnableDrillDown(false);
  viewer.setHasToggleGroupTreeButton(false);
  //viewer.setPrintMode(CrPrintMode.PDF);
  viewer.setPrintMode(CrPrintMode.ACTIVEX);
  viewer.setLeft(10);
  viewer.setOwnPage(true);
  viewer.setReuseParameterValuesOnRefresh(true);
// Passing parameters

if (session.getAttribute("refreshed") == null)
{
viewer.refresh();
session.setAttribute("refreshed", "true");
}

try
{
viewer.processHttpRequest(request, response, getServletConfig().getServletContext(), null);
}
catch(Exception e)
{
  System.out.println(e.getMessage());
}

  if (isConnect)
  {
   cm.takeDown();
  }

%>
</body>
</html>

